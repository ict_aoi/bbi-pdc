<?php

return [
    'avatar'     => storage_path() . '/app/avatar',
    'nagai'      => storage_path() . '/app/nagai',
    'jurnal'     => storage_path() . '/app/jurnal',
    'data_entry' => storage_path() . '/app/data_entry',
];
