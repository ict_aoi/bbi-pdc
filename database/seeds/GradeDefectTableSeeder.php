<?php

use Illuminate\Database\Seeder;
use App\Models\GradeDefect;

class GradeDefectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GradeDefect::Create([
            'name'  => 'no defect',
            'color' => 'green'
        ]);
        GradeDefect::Create([
            'name'  => '1 minor defect',
            'color' => 'yellow'
        ]);
        GradeDefect::Create([
            'name'  => '1 Major or 2 Minors',
            'color' => 'red'
        ]);

    }
}
