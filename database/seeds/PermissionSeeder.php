<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-line',
                'display_name' => 'menu line',
                'description' => 'menu line'
            ],
            [
                'name' => 'menu-process',
                'display_name' => 'menu process',
                'description' => 'menu process'
            ],
            [
                'name' => 'menu-workflow',
                'display_name' => 'menu workflow',
                'description' => 'menu workflow'
            ],
            [
                'name' => 'menu-mesin',
                'display_name' => 'menu mesin',
                'description' => 'menu mesin'
            ],
            [
                'name' => 'menu-daily-line',
                'display_name' => 'menu daily line',
                'description' => 'menu daily line'
            ],
            
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
