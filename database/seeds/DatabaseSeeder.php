<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //DB::table('permissions')->truncate();
        //DB::table('roles')->truncate();
        //DB::table('menus')->truncate();
        //DB::table('factories')->truncate();
        //DB::table('users')->truncate();
        // DB::table('lines')->truncate();
       /*  DB::table('grade_defects')->truncate();
        DB::table('defects')->truncate();
        DB::table('machines')->truncate(); */
        DB::table('working_hours')->truncate();
        //$this->call(PermissionSeeder::class);
        //$this->call(RoleSeeder::class);
        //$this->call(FactoryTableSeeder::class);
        //$this->call(UserSeeder::class);
        // $this->call(LinesTableSeeder::class);
        /* $this->call(GradeDefectTableSeeder::class);
        $this->call(DefectTableSeeder::class);
        $this->call(MachineTableSeeder::class); */
        $this->call(WorkingHoursTableSeeder::class);
        
    }
}
