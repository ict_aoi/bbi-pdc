<?php

use Illuminate\Database\Seeder;
use App\Models\Factory;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $role_admin_icts                  = Role::Select('id')->where('name','admin_ict')->get();
        $bbis                             = Factory::where('code','bbis')->first();
        $admin_ict_bbi                    = new User();
        $admin_ict_bbi->name              = 'admin ict';
        $admin_ict_bbi->nik               = '22222222';
        $admin_ict_bbi->sex               = 'perempuan';
        $admin_ict_bbi->department_name   = 'ict';
        $admin_ict_bbi->factory_id        = $bbis->id;
        $admin_ict_bbi->email             = 'admin_bbi_ict@aoi.co.id';
        $admin_ict_bbi->email_verified_at = carbon::now();
        $admin_ict_bbi->password          =  bcrypt('password1');
        if($admin_ict_bbi->save()) $admin_ict_bbi->attachRoles($role_admin_icts); 
    }
}
