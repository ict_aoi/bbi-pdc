<?php

use Illuminate\Database\Seeder;
use App\Models\GradeDefect;
use App\Models\Defect;

class DefectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $green  = GradeDefect::where('name','no defect')->first();
        $yellow = GradeDefect::where('name','1 minor defect')->first();
        $red    = GradeDefect::where('name','1 Major or 2 Minors')->first();

        $defects = [
            ['grade_defect_id' => $red->id,'name'=>'Color Defects ( shading within one garment or garments, migration/crocking/bleeding)'],
            ['grade_defect_id' => $red->id,'name'=>'Fabric defect (crease mark, Pilling, contamination)'],
            ['grade_defect_id' => $red->id,'name'=>'Incorrect stitch density ( eg. Button hole, seam SPI, Embroidery)'],
            ['grade_defect_id' => $red->id,'name'=>'Broken stitch'],
            ['grade_defect_id' => $red->id,'name'=>'Skipped stitches – All types of stitches'],
            ['grade_defect_id' => $red->id,'name'=>'Uneven/ wavy top & cover stitching'],
            ['grade_defect_id' => $red->id,'name'=>'Open seam/run off'],
            ['grade_defect_id' => $red->id,'name'=>'Seam puckering / Sewn-in pleats'],
            ['grade_defect_id' => $red->id,'name'=>'Roping, Twisted, wavy, uneven body/hem'],
            ['grade_defect_id' => $red->id,'name'=>'Needle cuts and needle holes'],
            ['grade_defect_id' => $red->id,'name'=>'Fabric raw edge/excess fabric'],
            ['grade_defect_id' => $red->id,'name'=>'Back tack/ bar tack missing or misplaced'],
            ['grade_defect_id' => $red->id,'name'=>'De-lamination and bubbling due to incorrect fusing'],
            ['grade_defect_id' => $red->id,'name'=>'Insecure and harmful accessories attachment'],
            ['grade_defect_id' => $red->id,'name'=>'Incorrect 3-stripe quality, position and measurements.'],
            ['grade_defect_id' => $red->id,'name'=>'Construction detail not symmetric'],
            ['grade_defect_id' => $red->id,'name'=>'Scissor cut'],
            ['grade_defect_id' => $red->id,'name'=>'Measurement out of tolerance'],
            ['grade_defect_id' => $yellow->id,'name'=>'Oil spots/ Soil/ Any kind of Stain/ Glue mark'],
            ['grade_defect_id' => $yellow->id,'name'=>'Untrimmed sewing threads or loose threads'],
            ['grade_defect_id' => $yellow->id,'name'=>'Chalk marks or pen/pencil marks are visble'],
            ['grade_defect_id' => $red->id,'name'=>'Incorrect size, color, shape, position, appearance on artwork'],
            ['grade_defect_id' => $red->id,'name'=>'Poor print / heat transfer quality ( Cracking, Sticky, Smell, Peel off and poor coverage)'],
            ['grade_defect_id' => $red->id,'name'=>'Faulty stitching on embroidery'],
            ['grade_defect_id' => $red->id,'name'=>'Uneven gathering on elastic construction or design'],
            ['grade_defect_id' => $red->id,'name'=>'Wrong Accessories/ missing / uncompleted'],
            ['grade_defect_id' => $red->id,'name'=>'Poor pressing'],
            ['grade_defect_id' => $red->id,'name'=>'Wrongly packed or wrong size'],
            ['grade_defect_id' => $red->id,'name'=>'Incorrect / missing information on polybag or carton box/wrong label'],
            ['grade_defect_id' => $red->id,'name'=>'Miscellaneous (please specify)'],
            ['grade_defect_id' => $green->id,'name'=>'No Defect']
        ];
        foreach ($defects as $key => $defect) {
            Defect::create([
                'name'            => $defect['name'],
                'grade_defect_id' => $defect['grade_defect_id']
            ]);
        }
    }
}
