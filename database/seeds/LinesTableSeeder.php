<?php

use Illuminate\Database\Seeder;
use App\Models\Factory;
use App\Models\Line;

class LinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = Factory::where('code','bbis')->whereNull('delete_at')->first();
        $lines = [
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 1', 'code' => '1','order' => '1'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 2', 'code' => '2','order' => '2'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 3', 'code' => '3','order' => '3'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 4', 'code' => '4','order' => '4'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 5', 'code' => '5','order' => '5'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 6', 'code' => '6','order' => '6'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 7', 'code' => '7','order' => '7'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 8', 'code' => '8','order' => '8'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 9', 'code' => '9','order' => '9'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 10', 'code' => '10','order' => '10'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 11', 'code' => '11','order' => '11'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line 12', 'code' => '12','order' => '12'],
            ['factory_id' => $factory->id, 'buyer' => 'nagai', 'name' => 'line cpa', 'code' => 'cpa','order' => '13'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 20', 'code' => '20','order' => '20'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 21', 'code' => '21','order' => '21'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 22', 'code' => '22','order' => '22'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 23', 'code' => '23','order' => '23'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 24', 'code' => '24','order' => '24'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 25', 'code' => '25','order' => '25'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 26', 'code' => '26','order' => '26'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 27', 'code' => '27','order' => '27'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 28', 'code' => '28','order' => '28'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 28a', 'code' => '28a','order' => '29'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 29', 'code' => '29','order' => '30'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 29a', 'code' => '29a','order' => '31'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 30', 'code' => '30','order' => '32'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 31', 'code' => '31','order' => '33'],
            ['factory_id' => $factory->id, 'buyer' => 'other', 'name' => 'line 32', 'code' => '32','order' => '34'],
            ['factory_id' => $factory->id, 'buyer' => '-', 'name' => '-', 'code' => 'all','order' => '0'],
        ];
        foreach ($lines as $key => $line) {
            Line::Create([
                'factory_id' => $line['factory_id'],
                'code'       => $line['code'],
                'buyer'      => $line['buyer'],
                'name'       => $line['name'],
                'order'      => $line['order']
            ]);  
        }
        
    }
}
