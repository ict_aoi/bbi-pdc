<?php

use Illuminate\Database\Seeder;
use App\Models\Factory;
use App\Models\Machine;

class MachineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = Factory::where('code','bbis')->whereNull('delete_at')->first()->id;
        $machines = [
            ['factory_id' => $factory , 'code' =>'sn','name' => 'single needle'],
            ['factory_id' => $factory , 'code' =>'dn','name' => 'double needle'],
            ['factory_id' => $factory , 'code' =>'ol','name' => 'overlock'],
            ['factory_id' => $factory , 'code' =>'ovd','name' => 'overdeck'],
            ['factory_id' => $factory , 'code' =>'kns 3 stripes','name' => 'kansai 3 strip'],
            ['factory_id' => $factory , 'code' =>'kns wb','name' => 'kansai wb'],
            ['factory_id' => $factory , 'code' =>'bh','name' => 'button hole'],
            ['factory_id' => $factory , 'code' =>'bt','name' => 'bartack'],
            ['factory_id' => $factory , 'code' =>'bs','name' => 'button sewing'],
            ['factory_id' => $factory , 'code' =>'mh','name' => 'chainstitch'],
            ['factory_id' => $factory , 'code' =>'fs','name' => 'flatseam'],
            ['factory_id' => $factory , 'code' =>'reece','name' => 'reece'],
            ['factory_id' => $factory , 'code' =>'maikap','name' => 'maikap'],
            ['factory_id' => $factory , 'code' =>'snap','name' => 'snap button']
        ];
        foreach ($machines as $key => $m) {
            Machine::create([
                'factory_id' => $m['factory_id'],
                'code'       => $m['code'],
                'name'       => $m['name']
            ]);
        }
    }
}
