<?php

use Illuminate\Database\Seeder;
use App\Models\WorkingHours;

class WorkingHoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hours = [
            ['name'  => '07:00 - 08:00','start' =>'07:00:00','end' => '08:00:00'],
            ['name'  => '08:00 - 09:00','start' =>'08:00:00','end' => '09:00:00'],
            ['name'  => '09:00 - 10:00','start' =>'09:00:00','end' => '10:00:00'],
            ['name'  => '10:00 - 11:00','start' =>'10:00:00','end' => '11:00:00'],
            ['name'  => '11:00 - 12:00','start' =>'11:00:00','end' => '12:00:00'],
            ['name'  => '13:00 - 14:00','start' =>'12:00:00','end' => '14:00:00'],
            ['name'  => '14:00 - 15:00','start' =>'14:00:00','end' => '15:00:00'],
            ['name'  => '15:00 - 16:00','start' =>'15:00:00','end' => '16:00:00'],
            ['name'  => '16:00 - 17:00','start' =>'16:00:00','end' => '17:00:00'],
            ['name'  => '17:00 - 18:00','start' =>'17:00:00','end' => '18:00:00'],
            ['name'  => '18:00 - 19:00','start'  =>'18:00:00','end' => '19:00:00'],
            ['name'  => '19:00 - 20:00','start' =>'19:00:00','end' => '20:00:00'],
            ['name'  => '20:00 - 21:00','start' =>'20:00:00','end' => '21:00:00'],
        ];
        foreach ($hours as $key => $hour) {
            WorkingHours::Create([
                'name'  => $hour['name'],
                'start' => $hour['start'],
                'end'   => $hour['end']
            ]);  
        }
    }
}
