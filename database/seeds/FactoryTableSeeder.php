<?php

use Illuminate\Database\Seeder;
use App\Models\Factory;

class FactoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Factory::Create([
			'code'        => 'bbis',
			'name'        => 'pt .bina busana internusa semarang',
			'alamat'      =>'jl. tugu wijaya kusuma iv, kawasan industri wijaya kusuma, randugarut tugu semarang - jawa tengah',
			'description' => 'gudang bina busana internusa semarang',
			'wft'         =>'3'
        ]);
    }
}
