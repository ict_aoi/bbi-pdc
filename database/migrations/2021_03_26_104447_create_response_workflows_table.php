<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('response_workflows', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36)->nullable()->unsigned();
            $table->char('workflow_id',36)->nullable()->unsigned();
            $table->integer('nomor');
            $table->string('style');
            $table->text('message');
            $table->string('create_qc_nik');
            $table->string('create_qc_name');
            $table->string('status')->nullable();
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('workflow_id')->references('id')->on('workflows')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response_workflows');
    }
}