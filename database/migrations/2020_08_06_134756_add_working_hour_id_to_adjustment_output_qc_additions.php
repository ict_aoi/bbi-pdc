<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkingHourIdToAdjustmentOutputQcAdditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adjustment_output_qc_additions', function (Blueprint $table) {
            $table->char('working_hour_id',36)->nullable()->unsigned();
            
            $table->foreign('working_hour_id')->references('id')->on('working_hours')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adjustment_output_qc_additions', function (Blueprint $table) {
            //
        });
    }
}
