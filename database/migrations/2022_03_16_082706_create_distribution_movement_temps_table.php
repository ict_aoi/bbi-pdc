<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributionMovementTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_movement_temps', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('distribution_id',36)->unsigned()->nullable();
            $table->char('distribution_detail_id',36)->unsigned()->nullable();
            $table->char('line_id',36)->unsigned()->nullable();
            $table->string('barcode_id', 100);
            $table->string('ip_address');
            $table->timestamps();
            
            $table->foreign('distribution_id')->references('id')->on('distributions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('distribution_detail_id')->references('id')->on('distribution_details')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribution_movement_temps');
    }
}