<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleWorkflowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_workflow_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('cycle_time_id',36)->unsigned();
            $table->char('workflow_detail_id',36)->unsigned();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();

            $table->foreign('cycle_time_id')->references('id')->on('cycle_times')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('workflow_detail_id')->references('id')->on('workflow_details')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycle_workflow_details');
    }
}