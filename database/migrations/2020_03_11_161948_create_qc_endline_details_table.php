<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcEndlineDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_endline_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('qc_endline_id',36)->unsigned();
            $table->bigInteger('counter')->nullable()->default(0);
            $table->string('ip_address', 100)->nullable();
            $table->timestamps();
            $table->string('created_by_name')->nullable();
            $table->string('created_by_nik')->nullable();

            $table->foreign('qc_endline_id')->references('id')->on('qc_endlines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_endline_details');
    }
}
