<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcEndlineDefectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_endline_defects', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('qc_endline_detail_id',36)->unsigned();
            $table->char('qc_endline_id',36)->unsigned();
            $table->bigInteger('defect_id')->nullable()->default(0);
            $table->timestamp('update_defect_date')->nullable();
            $table->timestamps();

            $table->foreign('qc_endline_id')->references('id')->on('qc_endlines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('qc_endline_detail_id')->references('id')->on('qc_endline_details')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('defect_id')->references('id')->on('defects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_endline_defects');
    }
}
