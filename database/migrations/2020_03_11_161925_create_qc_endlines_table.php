<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcEndlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_endlines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('production_id',36)->unsigned();
            $table->char('production_size_id',36)->unsigned();
            $table->bigInteger('counter')->nullable()->default(0);
            $table->bigInteger('counter_day')->nullable()->default(0);
            $table->bigInteger('repairing')->nullable()->default(0);
            $table->bigInteger('total_repairing')->nullable()->default(0);
            $table->date('counter_day_at')->nullable();
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();

            $table->foreign('production_id')->references('id')->on('productions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('production_size_id')->references('id')->on('production_sizes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_endlines');
    }
}
