<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_lines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36)->unsigned();
            $table->string('style', 100);
            $table->string('change_over', 10);
            $table->double('smv', 15, 8);
            $table->integer('cumulative_day')->default(0);
            $table->boolean('job_status')->default(true);            
            $table->timestamps();
            $table->datetime('delete_at')->nullable();

            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_lines');
    }
}
