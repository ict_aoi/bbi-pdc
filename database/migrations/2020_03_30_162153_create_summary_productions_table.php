<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_productions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36)->unsigned();
            $table->string('name', 50);
            $table->string('style', 50);
            $table->string('poreference', 50)->nullable();
            $table->string('size', 50)->nullable();
            $table->string('article', 50)->nullable();
            $table->integer('output')->default(0);
            $table->time('start_hours');
            $table->time('end_hours');
            $table->date('date');
            $table->timestamps();

            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_productions');
    }
}
