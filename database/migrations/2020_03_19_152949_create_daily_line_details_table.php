<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyLineDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_line_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('daily_line_id',36)->unsigned();
            $table->char('daily_working_id',36)->unsigned();
            $table->integer('day')->default(0);
            $table->integer('present_sewer')->default(0);
            $table->integer('present_ipf')->nullable();
            $table->integer('present_folding')->nullable();
            $table->bigInteger('commitment_line')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->biginteger('create_user_id')->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('daily_line_id')->references('id')->on('daily_lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('daily_working_id')->references('id')->on('daily_workings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_line_details');
    }
}
