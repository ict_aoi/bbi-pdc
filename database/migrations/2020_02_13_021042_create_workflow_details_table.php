<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkflowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('workflow_id',36)->nullable()->unsigned();
            $table->char('process_sewing_id',36)->nullable()->unsigned();
            $table->boolean('last_process')->default(false);
            $table->timestamps();     

            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('workflow_id')->references('id')->on('workflows')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('process_sewing_id')->references('id')->on('process_sewings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_details');
    }
}
