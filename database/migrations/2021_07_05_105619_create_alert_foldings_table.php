<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertFoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_foldings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('production_id',36)->unsigned()->nullable();
            $table->char('production_size_id',36)->unsigned()->nullable();
            $table->integer('qc_output')->default(0);
            $table->string('create_user_name', 100);
            $table->string('create_user_nik', 100);
            $table->timestamps();

            $table->foreign('production_id')->references('id')->on('productions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('production_size_id')->references('id')->on('production_sizes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_foldings');
    }
}