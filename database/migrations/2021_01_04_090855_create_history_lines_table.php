<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_lines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36)->nullable()->unsigned();
            $table->string('name');
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->timestamps();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_lines');
    }
}
