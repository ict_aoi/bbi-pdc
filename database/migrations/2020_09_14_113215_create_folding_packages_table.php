<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoldingPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folding_packages', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('scan_id', 100)->nullable();
            $table->string('barcode_id', 100)->nullable();
            $table->integer('pkg_count')->default(0);
            $table->integer('item_qty')->default(0);
            $table->bigInteger('counter')->nullable()->default(0);
            $table->string('current_department', 100)->nullable();
            $table->string('current_status', 100)->nullable();
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folding_packages');
    }
}
