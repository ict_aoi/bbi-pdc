<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSewingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewing_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('sewing_id', 36)->unsigned();
            $table->bigInteger('counter')->default(0);
            $table->timestamps();
            $table->string('ip_address', 100)->nullable();
            $table->foreign('sewing_id')->references('id')->on('sewings')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewing_details');
    }
}
