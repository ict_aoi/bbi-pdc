<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcodeGarmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barcode_garments', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('style', 100)->nullable();
            $table->string('poreference', 100)->nullable();
            $table->string('article', 100)->nullable();
            $table->string('size', 100)->nullable();
            $table->string('barcode_id', 100)->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barcode_garments');
    }
}
