<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAndonSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('andon_supplies', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36)->unsigned();
            $table->bigInteger('balance')->default(0); 
            $table->bigInteger('target_line')->default(0); 
            $table->timestamp('start_time')->nullable();
            $table->timestamp('close_time')->nullable();
            $table->string('create_user_name', 100);
            $table->string('create_user_nik', 100);
            $table->string('close_user_name', 100)->nullable();
            $table->string('close_user_nik', 100)->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('andon_supplies');
    }
}