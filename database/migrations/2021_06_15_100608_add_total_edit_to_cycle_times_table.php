<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalEditToCycleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cycle_times', function (Blueprint $table) {
            $table->integer('total_update')->default(0);
            $table->boolean('is_copied')->default(false);
            $table->char('cycle_time_id_before',36)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cycle_times', function (Blueprint $table) {
            //
        });
    }
}