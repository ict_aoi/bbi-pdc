<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factories', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('code')->nullable();
            $table->string('name');
            $table->text('alamat');
            $table->text('description')->nullable();
            $table->bigInteger('wft')->default(0);
            $table->timestamps();
            $table->datetime('delete_at')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factories');
    }
}
