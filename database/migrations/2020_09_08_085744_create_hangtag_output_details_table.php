<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHangtagOutputDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hangtag_output_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('hangtag_output_id',36)->unsigned();
            $table->bigInteger('counter')->nullable()->default(0);
            $table->string('ip_address', 100)->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->string('created_by_name')->nullable();
            $table->string('created_by_nik')->nullable();

            $table->foreign('hangtag_output_id')->references('id')->on('hangtag_outputs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hangtag_output_details');
    }
}