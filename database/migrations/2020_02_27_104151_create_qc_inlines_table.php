<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcInlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_inlines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36)->unsigned();
            $table->char('production_id',36)->unsigned();
            $table->char('process_sewing_id',36)->unsigned();
            $table->char('machine_id',36)->unsigned();
            $table->integer('grade_defect_id')->unsigned();
            $table->integer('round');
            $table->string('sewer_name', 100);
            $table->string('sewer_nik', 100);
            $table->string('qc_name', 100);
            $table->string('qc_nik', 100);
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('production_id')->references('id')->on('productions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('process_sewing_id')->references('id')->on('process_sewings')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('machine_id')->references('id')->on('machines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('grade_defect_id')->references('id')->on('grade_defects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_inlines');
    }
}
