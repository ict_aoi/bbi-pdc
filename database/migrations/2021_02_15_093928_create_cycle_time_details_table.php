<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleTimeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_time_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('cycle_time_id',36)->unsigned();
            $table->time('time');
            $table->time('time_recorded');
            $table->float('duration');
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();

            $table->foreign('cycle_time_id')->references('id')->on('cycle_times')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycle_time_details');
    }
}