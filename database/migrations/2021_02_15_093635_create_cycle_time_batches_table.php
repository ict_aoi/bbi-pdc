<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleTimeBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_time_batches', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36)->unsigned();
            $table->char('line_id',36)->unsigned();
            $table->string('style', 50);
            $table->dateTime('start_job')->nullable();
            $table->dateTime('end_job')->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();

            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycle_time_batches');
    }
}