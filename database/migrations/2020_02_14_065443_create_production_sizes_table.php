<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_sizes', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('production_id',36)->nullable();
            $table->string('size')->nullable();
            $table->double('qty',15,8)->default(0);
            $table->string('created_user_by_nik')->nullable();
            $table->string('created_user_by_name')->nullable();
            $table->string('updated_user_by_nik')->nullable();
            $table->string('updated_user_by_name')->nullable();
            $table->string('deleted_user_by_nik')->nullable();
            $table->string('deleted_user_by_name')->nullable();
            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
            
            $table->foreign('production_id')->references('id')->on('productions')->onDelete('cascade')->onUpdate('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_sizes');
    }
}
