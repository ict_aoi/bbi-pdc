<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_times', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('cycle_time_batch_id',36)->unsigned();
            $table->char('workflow_detail_id',36)->unsigned();
            $table->bigInteger('wip')->nullable();
            $table->bigInteger('burst')->nullable();
            $table->string('sewer_name', 100);
            $table->string('sewer_nik', 100);
            $table->double('average', 15, 8)->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();

            $table->foreign('cycle_time_batch_id')->references('id')->on('cycle_time_batches')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('workflow_detail_id')->references('id')->on('workflow_details')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycle_times');
    }
}