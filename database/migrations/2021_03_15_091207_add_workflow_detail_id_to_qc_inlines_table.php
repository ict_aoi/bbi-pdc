<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkflowDetailIdToQcInlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qc_inlines', function (Blueprint $table) {
            $table->char('workflow_detail_id',36)->unsigned()->nullable();

            $table->foreign('workflow_detail_id')->references('id')->on('workflow_details')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qc_inlines', function (Blueprint $table) {
            //
        });
    }
}