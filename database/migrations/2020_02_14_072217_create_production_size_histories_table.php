<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionSizeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_size_histories', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('production_size_id',36)->nullable();
            $table->text('note')->nullable();
            $table->integer('qty_old')->default(0);
            $table->integer('qty_new')->default(0);
            $table->integer('qty_operator')->default(0);
            $table->timestamps();

            $table->foreign('production_size_id')->references('id')->on('production_sizes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_size_histories');
    }
}
