<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36)->unsigned()->nullable();
            $table->char('line_id',36)->unsigned()->nullable();
            $table->char('production_id',36)->unsigned()->nullable();
            $table->char('production_size_id',36)->unsigned()->nullable();
            $table->string('style', 100);
            $table->string('poreference', 100);
            $table->string('article', 100);
            $table->string('season', 100);
            $table->string('size', 100);
            $table->integer('cut_num');
            $table->integer('start_no');
            $table->integer('end_no');
            $table->bigInteger('qty')->default(0);
            $table->boolean('is_completed')->default(false);
            $table->boolean('is_movement')->default(false);
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->datetime('date_completed')->nullable();
            $table->datetime('date_movement')->nullable();
            
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('production_id')->references('id')->on('productions');
            $table->foreign('production_size_id')->references('id')->on('production_sizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributions');
    }
}