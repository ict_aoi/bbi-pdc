<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36)->nullable()->unsigned();
            $table->string('code');
            $table->string('buyer');
            $table->string('name');
            $table->integer('order')->default(1);
            $table->bigInteger('fast_react_id')->nullable();
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lines');
    }
}
