<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseWorkflowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('response_workflow_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('response_workflow_id',36)->nullable()->unsigned();
            $table->text('message');
            $table->string('status')->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('response_workflow_id')->references('id')->on('response_workflows')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response_workflow_details');
    }
}