<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryProcessSewingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_process_sewings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id', 36)->unsigned();
            $table->char('process_sewing_id', 36)->unsigned();
            $table->char('machine_id', 36)->unsigned();
            $table->char('line_id', 36)->unsigned();
            $table->string('sewer_name', 100);
            $table->string('sewer_nik', 100);
            $table->timestamps();

            $table->foreign('process_sewing_id')->references('id')->on('process_sewings')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('machine_id')->references('id')->on('machines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_process_sewings');
    }
}
