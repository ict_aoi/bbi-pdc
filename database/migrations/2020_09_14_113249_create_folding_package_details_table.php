<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoldingPackageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folding_package_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('folding_output_id',36)->unsigned();
            $table->char('folding_package_id',36)->unsigned();
            $table->char('folding_package_detail_size_id',36)->unsigned();
            $table->string('barcode_garment_id', 100)->nullable();
            $table->string('ip_address', 100)->nullable();
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->string('created_by_name')->nullable();
            $table->string('created_by_nik')->nullable();
            $table->foreign('barcode_garment_id')->references('id')->on('barcode_garments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('folding_output_id')->references('id')->on('folding_outputs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('folding_package_id')->references('id')->on('folding_packages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('folding_package_detail_size_id')->references('id')->on('folding_package_detail_sizes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folding_package_details');
    }
}
