<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSewingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sewings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id', 36)->unsigned();
            $table->char('line_id', 36)->unsigned();
            $table->string('style', 100);
            $table->string('article', 50);
            $table->bigInteger('counter')->default(0);
            $table->bigInteger('counter_day')->default(0);
            $table->string('status', 50)->nullable();
            $table->date('update_counter_day')->nullable();
            $table->timestamps();

            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sewings');
    }
}
