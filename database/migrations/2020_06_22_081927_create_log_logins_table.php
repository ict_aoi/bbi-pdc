<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_logins', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('line_id',36);
            $table->char('factory_id',36);
            $table->string('name', 100);
            $table->string('created_by_nik', 100);
            $table->string('created_by_name', 100);
            $table->string('subdept_name', 100);
            $table->dateTime('delete_at')->nullable();
            $table->dateTime('expired_date')->nullable();
            $table->timestamps();

            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_logins');
    }
}
