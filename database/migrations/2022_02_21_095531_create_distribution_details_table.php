<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('distribution_id',36)->unsigned()->nullable();
            $table->string('barcode_id', 100);
            $table->string('component_name', 100);
            $table->integer('style_id_cdms')->nullable();
            $table->string('create_user_nik', 100);
            $table->string('create_user_name', 100);
            $table->string('move_user_nik', 100)->nullable();
            $table->string('move_user_name', 100)->nullable();
            $table->boolean('is_movement')->default(false);
            $table->string('ip_address');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->datetime('date_movement')->nullable();
            
            $table->foreign('distribution_id')->references('id')->on('distributions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribution_details');
    }
}