<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryChangeStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_change_styles', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('style_id',36)->unsigned();
            $table->string('change', 100);            
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('edit_user_id')->nullable();

            $table->foreign('style_id')->references('id')->on('styles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('edit_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_change_styles');
    }
}
