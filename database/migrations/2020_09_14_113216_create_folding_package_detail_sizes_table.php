<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoldingPackageDetailSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folding_package_detail_sizes', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('folding_package_id',36)->unsigned();
            $table->string('style', 100)->nullable();
            $table->string('poreference', 100)->nullable();
            $table->string('article', 100)->nullable();
            $table->string('size', 100)->nullable();
            $table->integer('inner_pack')->nullable()->default(0);
            $table->timestamps();
            $table->datetime('delete_at')->nullable();
            $table->foreign('folding_package_id')->references('id')->on('folding_packages')->onDelete('cascade')->onUpdate('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folding_package_detail_sizes');
    }
}
