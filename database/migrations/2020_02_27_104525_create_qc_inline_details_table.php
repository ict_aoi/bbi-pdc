<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcInlineDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_inline_details', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('qc_inline_id',36)->unsigned();
            $table->integer('defect_id')->unsigned();
            $table->timestamps();
            $table->datetime('update_at')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('update_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('update_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('qc_inline_id')->references('id')->on('qc_inlines')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('defect_id')->references('id')->on('defects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_inline_details');
    }
}
