<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarkToWorkflowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflow_details', function (Blueprint $table) {
            $table->char('machine_id',36)->unsigned()->nullable();
            $table->double('smv',15,8)->nullable();
            $table->boolean('critical_process')->default(false);

            $table->foreign('machine_id')->references('id')->on('machines')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow_details', function (Blueprint $table) {
            //
        });
    }
}