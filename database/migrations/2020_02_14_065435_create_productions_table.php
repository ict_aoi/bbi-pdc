<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36)->nullable();
            $table->char('line_id',36)->nullable();
            $table->string('poreference')->nullable();
            $table->string('article')->nullable();
            $table->string('style')->nullable();
            $table->double('total_qty',15,8)->default(0);
            $table->string('created_user_by_nik')->nullable();
            $table->string('created_user_by_name')->nullable();
            $table->string('updated_user_by_nik')->nullable();
            $table->string('updated_user_by_name')->nullable();
            $table->string('deleted_user_by_nik')->nullable();
            $table->string('deleted_user_by_name')->nullable();
            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
            
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('line_id')->references('id')->on('lines')->onDelete('cascade')->onUpdate('cascade');
       

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
