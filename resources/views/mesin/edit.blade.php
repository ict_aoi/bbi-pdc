{!!
    Form::open([
        'role' => 'form',
        'url' => route('mesin.update',$data->id),
        'method' => 'post',
        'class' => 'form-horizontal',
        'id'=> 'form'
    ])
!!}
    
    @include('form.text', [
        'field' => 'code',
        'label' => 'Code',
        'default' => ucwords($data->code),
        'mandatory' => '*Wajib diisi',
        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
        'attributes' => [
            'id' => 'txtCode'
        ]
    ])
    @include('form.text', [
        'field' => 'name',
        'label' => 'Nama',
        'default' => ucwords($data->name),
        'mandatory' => '*Wajib diisi',
        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
        'attributes' => [
            'id' => 'txtNama'
        ]
    ])
    {!! Form::hidden('factory_id',$data->factory_id, array('id' => 'factory_id')) !!}
    <label for="is_special" class="control-label font-weight-bold col-md-2 col-lg-2 col-sm-12">
        Category Machine
    </label>
    <div class="checkbox checkbox-switchery switchery-lg switchery-double col-md-10 col-lg-10 col-sm-12">
        <label>
            Normal
            <input class="switchery" name="is_special" type="checkbox" {{ ($data->is_special)?"value='1' checked='true'":"value='0'"}}>
            Special
        </label>
    </div>
    <div class="text-right">
    <button type="submit" class="btn btn-primary legitRipple">update <i class="icon-arrow-right14 position-right"></i></button>
    </div>
{!! Form::close() !!}
<script src="{{ mix('js/switch.js') }}"></script>
<script>
     $('#form').submit(function(event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        $("#alert_success").trigger("click", 'Data berhasil disimpan.')
                        $('#edit_mesinModal').modal('hide');
                        $('#mesinTable').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                    }
                });
            }
        });
    });
</script>