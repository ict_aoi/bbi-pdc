@extends('layouts.app',['active' => 'mesin'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Mesin</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Data Mesin</li>
        </ul>
        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-gear position-left"></i>
                    Action
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    @if (Auth::user()->is_super_admin)
                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#filterFactoryModal"><i class="icon-office"></i>Factory</a></li>
                        <li class="divider"></li>
                    @endif
                    <li><a href="javascript:void(0)" onclick="return create()"><i class="icon-plus2"></i>Create</a></li>
                </ul>
            </li>
        </ul>
        
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
    </div>
    
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="mesinTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Category Machine</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
{!! Form::hidden('base_url',route('mesin.index'), array('id' => 'base_url')) !!}
@section('page-modal')
    @include('form.modal', [
        'name' => 'edit_mesin',
        'title' => 'Edit Mesin'
    ])
    @include('form.modal', [
        'name' => 'create_mesin',
        'title' => 'Create Mesin'
    ])
    @include('mesin.lov._lov_filter_factory')
@endsection
@section('page-js')
<script src="{{ mix('js/data_mesin.js') }}"></script>
@endsection