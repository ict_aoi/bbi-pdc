<script type="x-tmpl-mustache" id="items-table">
    {% #item %}
      <tr>
        <td width="50px" style="text-align:center;">{% no %}</td>
        <input type="hidden" value="{% id %}" data-id="{% _id %}" data-row="">
        <td>
            <div id="code_{% _id %}">{% code %}</div>
            <input type="text" class="form-control hidden input-sm " id="codeInput_{% _id %}" value="{% code %}" data-id="{% _id %}"
          >
        </td>
        <td>
            <div id="nama_{% _id %}">{% nama %}</div>
            <input type="text" class="form-control hidden input-sm " id="namaInput_{% _id %}" value="{% nama %}" data-id="{% _id %}"
          >
        </td>
        <td width="50px">
          <button type="button" id="editItem_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-item"><i class="icon-pencil5"></i></button>
          <button type="button" id="simpanItem_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-item hidden"><i class="icon-floppy-disk"></i></button>
          <button type="button" id="cancelItem_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-cancel-item hidden"><i class="icon-close2""></i></button>
          <button type="button" id="deleteItem_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-circle btn-delete-item"><i class="icon-trash"></i></button>
        </td>
      </tr>
    {%/item%}
  
     <tr>
      <td width="20px">
        #
      </td>
      <td>
        <input type="text" class="form-control input-sm" id="txtCode" autofocus>
      </td>
      <td>
        <input type="text" class="form-control input-sm" id="txtNama">
      </td>
      <td>
        <button type="button" id="btnAdd"  onclick="return addItem()" class="btn btn-default btn-icon-anim btn-circle btn-edit-item-description"><i class="icon-add"></i></button>
      </td>

    </tr>
  </script>