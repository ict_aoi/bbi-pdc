@extends('layouts.app',['active' => 'mesin'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Mesin</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('mesin.index') }}">Data Mesin</a></li>
            <li class="active">Baru</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="createLineTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody id="tbody-items">
                </tbody>
            </table>
        </div>
        {!!
            Form::open([
                'role'    => 'form',
                'url'     => route('mesin.store'),
                'method'  => 'post',
                'class'   => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'      => 'form'
            ])
        !!}
        
        {!! Form::hidden('items','[]', array('id' => 'items')) !!}
        <div class="text-right">
            <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@section('page-modal')
    @include('mesin._fields_mesin')
@endsection

@endsection

@section('page-js')
<script src="{{ mix('js/create_mesin.js') }}"></script>

@endsection