@extends('layouts.front')

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body" style="
    padding-bottom: 0px !important;
    padding-top: 2px !important;">
       <div class="row">
            <div class="col-xs-3 col-sm-4 line">
            <a href="{{ route('sewing.index',[$obj->factory,$obj->line]) }}">{{strtoupper($obj->line_name)}} - {{strtoupper($obj->factory)}}</a>
            </div>
            <div class="col-xs-4 col-sm-4">
                <label for="status">Status : </label>
                <label class="label label-warning hidden" id="process">Proses</label>
                <label class="label label-primary hidden" id="selesai">Selesai</label>
            </div>
            <div class="col-xs-5 col-sm-4 tanggal">
                <label>{{Carbon\Carbon::now()->format('d-M-Y')}}</label>
                <label id="time"></label>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
        
    <div class="panel-body" style="
    padding-bottom: 5px !important;
    padding-top: 2px !important;">
        <div class="col-xs-12 col-sm-12" align="center">
            <a href="javascript:void(0)" onclick="return selectStyle()" id="select_style" style="font-size:200%;">{{($obj->style?$obj->style."_".$obj->article:'Pilih Style')}}</a>
        </div>
        <div class="col-xs-12 col-sm-12"  align="center">
            <h1 style="font-size:400%;" id="counter_label">0</h1>
        </div>
        <button type="submit" id="plus" style="height: 70px;font-size:40px" class="btn bg-success-400 col-xs-12 legitRipple"><i class="icon-plus3 position-center"></i></button>
    </div>
</div>
{!!
    Form::open([
        'role'      => 'form',
        'url'       => route('sewing.store'),
        'method'    => 'post',
        'class'     => 'form-horizontal',
        'enctype'   => 'multipart/form-data',
        'id'        => 'form'
    ])
!!}
    {!! Form::hidden('line_id',$obj->line_id, array('id' => 'line_id')) !!}
    {!! Form::hidden('factory_id',$obj->factory_id, array('id' => 'factory_id')) !!}
    {!! Form::hidden('style',$obj->style, array('id' => 'style')) !!}
    {!! Form::hidden('article',$obj->article, array('id' => 'article')) !!}
    {!! Form::hidden('qtyIdle','0', array('id' => 'qtyIdle')) !!}
    {!! Form::hidden('sewing_id',$obj->sewing_id, array('id' => 'sewing_id')) !!}
    {!! Form::hidden('balance','', array('id' => 'balance')) !!}
    {!! Form::hidden('style_default',$obj->style_default, array('id' => 'style_default')) !!}
    <button type="submit" class="hidden">simpan</button>
{!! Form::close() !!}
  
@endsection
{!! Form::hidden('url_get_style',route('sewing.get_style'), array('id' => 'url_get_style')) !!}
{!! Form::hidden('url_select_style',route('sewing.select_style'), array('id' => 'url_select_style')) !!}  
{!! Form::hidden('url_get_counter',route('sewing.get_counter'), array('id' => 'url_get_counter')) !!}  
@section('page-modal')
    @include('form.modal', [
		'name'          => 'selectStyle',
		'title'         => 'Pilih Style',
		'placeholder'   => 'Cari berdasarkan style',
    ])
@endsection

@section('page-js')
    <script src="{{ mix('js/sewing.js') }}"></script>
    <script src="{{ mix('js/jquery.idle.min.js') }}"></script>
@endsection

