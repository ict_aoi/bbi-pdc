<div style="overflow:scroll; height:200px;">
	<table class="table table-striped table-hover table-responsive">
		<thead>
		  <tr>
			<th>Style</th>
			<th>Article</th>
			<th>Action</th>
		  </tr>
		</thead>
		
		<tbody>
			@foreach ($lists as $key => $list)
				<tr>
					<td>
						{{ $list->style }}
					</td>
					<td>
						{{ $list->article }}
					</td>
					<td>
						<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
							type="button" data-style="{{ $list->style }}" data-article="{{ $list->article }}"
							data-balance="{{ $list->balance }}" 
							>Pilih</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

{{-- {!! $productions->appends(Request::except('page'))->render() !!} --}}
