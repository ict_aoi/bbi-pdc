@extends('layouts.front')

@include('includes.front_navbar')
@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                {{-- <h1><i class="icon-grid5 position-left"></i> <span class="text-semibold">QC - ENDLINE (LINE 1)</span></h1> --}}
            </div>
            <div class="heading-elements">
                {{-- <h1 class="text-semibold no-margin">Hanna Dorman <small class="display-block">UX/UI designer</small></h1> --}}
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('endLine.index',[$obj->factory,$obj->line]) }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">QC - Endline</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    @if (isset($obj->nik))
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    @include('form.picklist', [
                        'label' 		=> ($obj->buyer=='other')?'Po Buyer':'No. LOT',
                        'field' 		=> 'poreference',
                        'name' 			=> 'poreference',
                        'placeholder' 	=> ($obj->buyer=='other')?'Silahkan pilih PO BUYER':'Silahkan pilih NO. LOT',
                        'title' 		=> ($obj->buyer=='other')?'Silahkan pilih PO BUYER':'Silahkan pilih NO. LOT',
                        'readonly'		=> true,
                    ])
                </div>
                <div class="col-lg-6">
                    @include('form.picklist', [
                        'label' 		=> 'Size',
                        'field' 		=> 'size',
                        'name' 			=> 'size',
                        'placeholder' 	=> 'Silahkan pilih size',
                        'title' 		=> 'Silahkan pilih size',
                        'readonly'		=> true,
                    ])
                </div>
                <button type="button" id="btn_cari" class="btn bg-teal-400 col-xs-12">Cari <i class="icon-search4 position-left"></i></button>
            </div>
        </div>
    </div>
    <div class="panel panel-flat">
                
        <div class="panel-body">
        <div class="col-md-12" align="right">
            <h1 class="no-margin text-semibold"><b><label id="time"></label></b></h1>
            
        </div>
        <br>
        <div class="row">
            <center>
                <div class="col-md-3"> <h1 class="no-margin text-semibold"><b>STYLE :</b> <span id="style_label">-</span></h1></div>
                <div class="col-md-3"> <h1 class="no-margin text-semibold"><b>{{ $obj->buyer=='other'?"PO BUYER":"NO LOT" }}</b>: <span id="poreference_label">-</span></h1></div>
                <div class="col-md-3"> <h1 class="no-margin text-semibold"><b>ARTICLE </b>: <span id="article_label">-</span></h1></div>
                <div class="col-md-3"> <h1 class="no-margin text-semibold"><b>SIZE </b>: <span id="size_label">-</span></h1></div>
            </center>
        </div>
        <br>
        <div class="col-md-12">
            <div class="col-md-12"></div>
            <table class="table table-striped table-bordered table-hover table-full-width">
                <thead>
                    <tr class="border-double">
                        <th width="200px" class="text-center">WIP</th>
                        <th class="text-center">PERBAIKAN</th>
                        <th class="text-center">TOTAL PERIKSA</th>
                        <th width="150px" class="text-center"><span id="balance-head">BALANCE</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="wip">0</span></td>
                        <td class="text-center" style="font-size: 30px"><a onclick="return repairing()" href="javascript:void(0)"><span class="repairing">0</span></a></td>
                        <td class="text-center" style="font-size: 30px"><span class="check_day">0</span></td>
                        <td class="text-center" rowspan="2" style="font-size: 50px"><span class="balance">0</span></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span class="total_repairing">0</span></td>
                        <td class="text-center"><span class="all">0</span></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button type="button" id="btnDefect" style="height: 75px;font-size:40px"  class="btn btn-danger btn-lg btn-block">Defect</button></td>
                        <td colspan="2"><button type="button" id="btnGood" style="height: 75px;font-size:40px"  class="btn btn-success btn-lg btn-block">Good</button></td>
                        
                    </tr>
                </tbody>
            </table>
            <a href="#" id="btnConfirmation"  data-toggle="modal" data-target="#confrimationDefectModal" class="btn btn-default hidden"><i class="icon-plus2 pull-right"></i> Create</a>
        </div>
        <br/>
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('endLine.store'),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'enctype'   => 'multipart/form-data',
                'id'        => 'form'
            ])
        !!}
            {!! Form::hidden('production_id','', array('id' => 'production_id')) !!}
            {!! Form::hidden('production_size_id','', array('id' => 'production_size_id')) !!}
            {!! Form::hidden('qc_endline_id','', array('id' => 'qc_endline_id')) !!}
            {!! Form::hidden('qtyIdle','0', array('id' => 'qtyIdle')) !!}
            {!! Form::hidden('flag_refresh','0', array('id' => 'flag_refresh')) !!}
            {!! Form::hidden('flag_commit','0', array('id' => 'flag_commit')) !!}
            {!! Form::hidden('server',$obj->ip_websocket, array('id' => 'server')) !!}
            {!! Form::hidden('port',$obj->port_websocket, array('id' => 'port')) !!}
    
            <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple hidden">Simpan <i class="icon-floppy-disk position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
    
    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
        <li>
            <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                <i class="fab-icon-open icon-grid3"></i>
                <i class="fab-icon-close icon-cross2"></i>
            </a>

            <ul class="fab-menu-inner">
                <li>
                    <div data-fab-label="Logout">
                        <a onclick="document.getElementById('qc_endline_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                            <i class="icon-switch2"></i>
                            <form id="qc_endline_logout" action="{{ route('endLine.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                {!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
                            {!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
                            </form>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Refresh">
                        <a data-toggle="modal" class="btn btn-primary btn-rounded btn-icon btn-float" onclick="return refresh()">
                            <i class="icon-spinner3"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Laporan Harian Qc Endline">
                        <a data-toggle="modal" class="btn btn-default btn-rounded btn-icon btn-float" onclick="return reportQcCheck()">
                            <i class="icon-cabinet"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Laporan Status Po Buyer">
                        <a data-toggle="modal" class="btn btn-default btn-rounded btn-icon btn-float" onclick="return status_pobuyer()" id="statuspo">
                            <i class="icon-newspaper2"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Shutdown">
                        <a href="#" data-toggle="modal" data-target="#shutdownModal" class="btn btn-danger btn-rounded btn-icon btn-float">
                            <i class="icon-power-cord"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
    <button type="button" class="hidden" id="btnReportDaily">ReportDail</button>
    <div class="hidden" onclick="getOutputHourly()" id="img_div">
        <img src="/images/warning.gif" width="150px" height="50px">
    </div>
    @endif
    {!! Form::hidden('buyer',($obj->buyer ? $obj->buyer : -1), array('id' => 'buyer')) !!}
    {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
    {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}
    {!! Form::hidden('line_name',($obj->line ? $obj->line : -1), array('id' => 'line_name')) !!}
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('start',($obj->start), array('id' => 'start')) !!}
    {!! Form::hidden('end',($obj->end), array('id' => 'end')) !!}
    {!! Form::hidden('style_merge','', array('id' => 'style_merge')) !!}
    {!! Form::hidden('style_daily',$obj->style_daily, array('id' => 'style_daily')) !!}
    {!! Form::hidden('commitment_line_jam',($obj->commitment_line_jam), array('id' => 'commitment_line_jam')) !!}
    {!! Form::hidden('commitment_line',($obj->commitment_line), array('id' => 'commitment_line')) !!}
    {!! Form::hidden('topic',($obj->topic), array('id' => 'topic')) !!}
    {!! Form::hidden('is_style_input',0, array('id' => 'is_style_input')) !!}
    {!! Form::hidden('commitment_line_style','', array('id' => 'commitment_line_style')) !!}
    {!! Form::hidden('balance_line','', array('id' => 'balance_line')) !!}
    {!! Form::hidden('url_poreference_picklist',route('endLine.poreferencePickList'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('url_size_picklist',route('endLine.sizePickList'), array('id' => 'url_size_picklist')) !!}
    {!! Form::hidden('url_get_data',route('endLine.get_data'), array('id' => 'url_get_data')) !!}
    {!! Form::hidden('url_get_defect',route('endLine.get_defect'), array('id' => 'url_get_defect')) !!}
    {!! Form::hidden('url_storeDefect',route('endLine.storeDefect'), array('id' => 'url_storeDefect')) !!}
    {!! Form::hidden('url_getRepairing',route('endLine.getRepairing'), array('id' => 'url_getRepairing')) !!}
    {!! Form::hidden('url_reportQcCheck',route('endLine.reportQcCheck'), array('id' => 'url_reportQcCheck')) !!}
    {!! Form::hidden('url_get_detail_defect',route('endLine.getDetailDefect'), array('id' => 'url_get_detail_defect')) !!}
    {!! Form::hidden('url_get_detail_output',route('endLine.getDetailOutput'), array('id' => 'url_get_detail_output')) !!}
    {!! Form::hidden('url_get_repairing_all',route('endLine.getRepairingAll'), array('id' => 'url_get_repairing_all')) !!}
    {!! Form::hidden('url_get_status_po',route('endLine.getStatusPo'), array('id' => 'url_get_status_po')) !!}
    {!! Form::hidden('url_getRepairingDataAll',route('endLine.getDataRepairingAll'), array('id' => 'url_getRepairingDataAll')) !!}
    {!! Form::hidden('url_get_commitment_line',route('endLine.get_commitment_line'), array('id' => 'url_get_commitment_line')) !!}
    {!! Form::hidden('url_get_alert_folding',route('endLine.get_alert_folding'), array('id' => 'url_get_alert_folding')) !!}
    {!! Form::hidden('url_check_andon',route('endLine.check_andon'), array('id' => 'url_check_andon')) !!}
    {!! Form::hidden('url_output_style',route('endLine.get_output_style'), array('id' => 'url_output_style')) !!}
    @endsection

@section('page-modal')
    @include('qc_endline.report._report_1_modal')
    @include('qc_endline._login')
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => ($obj->buyer=='other')?'Daftar List Po Buyer':'Daftar List NO. LOT',
		'placeholder'   => ($obj->buyer=='other')?'Cari berdasarkan Nama Po Buyer':'Cari berdasarkan Nama NO. LOT',
    ])
    @include('form.modal_picklist', [
		'name'          => 'size',
		'title'         => 'Daftar List Size',
		'placeholder'   => 'Cari berdasarkan Nama Size',
    ])
    @include('form.modal_picklist_c', [
		'name'          => 'defect',
		'title'         => 'Daftar List Defect',
		'placeholder'   => 'Cari berdasarkan Nama Defect/Kode Defect',
    ])
    @include('form.modal', [
		'name'          => 'repairing',
		'title'         => 'Daftar List Repairing',
		'placeholder'   => 'Cari berdasarkan Nama Size',
    ])
    @include('form.modal', [
		'name'          => 'reportDaily',
		'title'         => 'Laporan Harian Qc Endline',
    ])
    @include('form.modal', [
		'name'          => 'reportRepairingAll',
		'title'         => 'Laporan Repairing All',
    ])
    @include('form.modal', [
        'name'          => 'detail',
        'title'         => 'Laporan Detail Defect',
    ])
    @include('form.modal', [
        'name'          => 'detailOutput',
        'title'         => 'Laporan Output Perjam',
    ])
    @include('form.modal', [
        'name'          => 'statusPO',
        'title'         => 'Laporan Status Po Buyer',
    ])
    @include('form.modal', [
        'name'          => 'historyPo',
        'title'         => 'Laporan History Po Buyer',
    ])
    @include('qc_endline._shutdown')
    @include('qc_endline._defect_confirmation')
    @include('qc_endline._items_defect')
@endsection
{!! Form::hidden('url_ssh_reboot',route('endLine.ssh_reboot'), array('id' => 'url_ssh_reboot')) !!}
{!! Form::hidden('url_ssh_shutdown',route('endLine.ssh_shutdown'), array('id' => 'url_ssh_shutdown')) !!}
@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/keyboard.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/jquery.idle.min.js') }}"></script>
    <script src="{{ mix('js/mqttws31.js') }}"></script>
    <script src="{{ mix('js/qc_endline.js') }}"></script>
@endsection
