<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
	  	<th>{{$buyer=='other'?'PO BUYER':'NO LOT'}}</th>
		<th>STYLE</th>
		<th>ARTICLE</th>
		<th>ACTION</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->poreference) }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					{{ strtoupper($list->article) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-id="{{ $list->production_id }}"
					data-buyer="{{ $list->poreference }}" 
					data-style="{{ $list->style }}" 
					data-article="{{ $list->article }}" 
					>Pilih</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}