<div id="confrimationDefectModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Detail Defect Yang di Pilih</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="table-responsive">
                            <table class="table datatable-basic table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Defect</th>
                                        <th>Qty</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="defect_data_tbody">
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
            {!!
                Form::open([
                    'role' 		=> 'form_collection_defect',
                    'url' 		=> route('endLine.store_collection_defect'),
                    'method' 	=> 'store',
                    'class' 	=> 'form-horizontal',
                    'id'		=>	'form_collection_defect'
                ])
            !!}
            {!! Form::hidden('production_id_defect','', array('id' => 'production_id_defect')) !!}
            {!! Form::hidden('production_size_id_defect','', array('id' => 'production_size_id_defect')) !!}
            {!! Form::hidden('qc_endline_id_defect','', array('id' => 'qc_endline_id_defect')) !!}
            {!! Form::hidden('defect_confirmation_data','[]' , array('id' => 'defect_confirmation_data')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" id="btnCloseConfirmationDefect" data-dismiss="modal">Keluar <i class="icon-close2 position-right"></i></button>
					<button type="button" id="btnBackDefect" class="btn btn-warning legitRipple" data-dismiss="modal">Kembali <i class="glyphicon glyphicon-arrow-left position-right"></i></button>
					<button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</div>