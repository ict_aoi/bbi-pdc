<table class="table table-responsive">
	<thead>
	  <tr>
		@if ($buyer=='nagai')
			<th>Kode</th>
			<th>Posisi</th>
		@elseif($buyer=='other')
			<th>No</th>
		@endif
		<th>Nama</th>
	  </tr>
	</thead>
	
	<tbody>
		@php
			$no = 1;
		@endphp
		@foreach ($lists as $key => $list)
        <tr id="{{ $list->id_defect }}" style="background-color:#ffffff">
                
				@if ($buyer=='nagai')
					<td>{{ strtoupper($list->code) }}</td>
					<td>{{ strtoupper($list->position) }}</td>
				@elseif($buyer=='other')
					<td>{{ strtoupper($list->id_defect) }}</td>
				@endif
				<td>
					{{ strtoupper($list->name_defect) }}
				</td>
				<td>
					@if ($buyer=='other')
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->id_defect }}"
						data-name="{{ ucwords($list->name_defect) }}"
						data-grade="{{ $list->grade_defect_id }}" 
						>Select</button>
					@else
					<button class="btn btn-info btn-xs btn-choose" 
					type="button"
					id="{{ $list->id_defect }}_Select" 
					data-id="{{ $list->id_defect }}"
					data-name="{{ ucwords($list->position." ".$list->name_defect) }}"
					>Select</button>
					<button class="btn btn-info btn-xs btn-unchoose hidden" 
					type="button"
					id="{{ $list->id_defect }}_UnSelect" 
					data-id="{{ $list->id_defect }}"
					data-name="{{ ucwords($list->name_defect) }}"
					>UnSelect</button>
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
