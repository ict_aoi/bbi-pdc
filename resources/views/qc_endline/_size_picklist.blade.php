<table class="table table-responsive">
	<thead>
	  <tr>
		<th>Size</th>
		<th>Aksi</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr class="{{($list->qty==$list->counter)?"bg-success":''}}">
				<td>
					{{ strtoupper($list->size) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-id="{{ $list->id }}"
					data-size="{{ $list->size }}" 
					>Pilih</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}