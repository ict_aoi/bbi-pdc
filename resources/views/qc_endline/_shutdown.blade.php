<div id="shutdownModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p><i class="icon-power-cord"></i></h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="panel text-center">
									<div class="panel-body">
										<!-- Progress counter -->
										<div class="content-group-sm svg-center position-relative" id="hours-available-progress">
											<a href="#" onclick="shutdown()")">
												<svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(255, 0, 0);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(255, 0, 0); stroke: rgb(255, 0, 0);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(255, 0, 0); fill-opacity: 1;"></path></g>
												</svg>
												<i class="icon-switch text-danger-400 counter-icon" style="top: 22px"></i>
												<div>Daya Mati</div>
											</>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel text-center">
									<div class="panel-body">
										<!-- Progress counter -->
										<div class="content-group-sm svg-center position-relative" id="hours-available-progress">
											<a href="#" onclick="reboot()">
											<svg width="76" height="76">
												<g transform="translate(38,38)">
													<path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(0, 102, 51);"></path>
													<path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(0, 102, 51); stroke: rgb(0, 102, 51);"></path>
													<path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(0, 102, 51); fill-opacity: 1;"></path>
												</g>
											</svg>
											<i class="icon-spinner9 text-success-400 counter-icon" style="top: 22px"></i>
											<div>Mulai Ulang</div>
											</a>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
		</div>
	</div>
</div>
</div>