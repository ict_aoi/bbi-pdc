<script type="x-tmpl-mustache" id="defect_data_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td width="50px" style="text-align:center;">{% no %}</td>
			<td>{% defect_name %}</td>
			<td>
				<div class="input-group col-xs-8">
					<div id="qty_{% _id %}">{% qty_defect %}</div>
        			<input type="number" class="form-control input-sm hidden" id="qtyDefectInput_{% _id %}" value="{% qty_defect %}" data-id="{% _id %}" autocomplete="off">
				</div>
			</td>
			<td width="200px">
				<button type="button" id="editDefect_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-defect"><i class="icon-pencil5"></i></button>
				<button type="button" id="simpanDefect_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-defect hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancelDefect_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-cancel-defect hidden"><i class="icon-close2""></i></button>
				<button type="button" id="deleteDefect_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-circle btn-delete-defect"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
</script>