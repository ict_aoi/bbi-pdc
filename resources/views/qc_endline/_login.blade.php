<div id="modal_login_qc_endline" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content login-form width-400">
			{{-- <div class="panel-heading"> --}}
				{{-- <div class="panel text-center"> --}}
					<div class="panel-body">
						<div class="btn-group">
							<button type="button" class="btn bg-teal-400 btn-labeled dropdown-toggle legitRipple" data-toggle="dropdown"><b><i class="icon-power-cord"></i></b> SHUTDOWN <span class="caret"></span></button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#" onclick="shutdown()"><i class="icon-switch2"></i> Matikan Daya</a></li>
								<li><a href="#" onclick="reboot()"><i class="icon-reload-alt"></i> Mulai Ulang</a></li>
							</ul>
						</div>
					</div>
				{{-- </div> --}}
			{{-- </div> --}}
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="#qc-endline" data-toggle="tab"><h6>Qc Endline</h6></a></li>
			</ul>
			
			<div class="tab-content modal-body">
				<div class="tab-pane fade in active" id="qc-endline">
					{!!
						Form::open([
							'role' 		=> 'form',
							'url' 		=> route('endLine.login'),
							'method' 	=> 'post',
							'id'		=> 'login_qc_endline'
						])
					!!}	
						<div class="qc-endline-shade-screen" style="text-align: center;">
							<div class="qc-endline-shade-screen hidden">
								<img src="/images/ajax-loader.gif">
							</div>
						</div>
						<div id="qc-endline-body-login">
							<div class="form-group has-feedback has-feedback-left">
								
									<input type="text" class="form-control" placeholder="Masukan NIK anda" name="nik" id="nik_qc_endline" autocomplete="off" required="required">
									<div class="form-control-feedback">
										<i class="icon-user text-muted"></i>
									</div>
								
							</div>

							<div class="form-group">
								{!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>