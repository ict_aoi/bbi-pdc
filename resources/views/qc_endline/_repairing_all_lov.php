<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="listRepairingAllTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Style</th>
                        <th>Po Buyer</th>
                        <th>Article</th>
                        <th>Size</th>
                        <th width="50px">Kode Defect</th>
                        <th>Nama Defect</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>



<script>
    var url_getRepairingDataAll = $("#url_getRepairingDataAll").val();
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
        search: '<span>Filter:</span> _INPUT_',
        searchPlaceholder: 'Type to filter...',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    
    var listRepairingAllTable = $('#listRepairingAllTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        deferRender: true,
        ajax: {
           // type: 'GET',
            url: url_getRepairingDataAll
        },
        fnCreatedRow: function(row, data, index) {
            var info = listRepairingAllTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null,sortable: false,orderable: false,searchable: false},
            {
                data: 'tanggal',
                name: 'tanggal',
                searchable: true,
                visible: true,
                orderable: true
            },{
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: false
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: false,
                orderable: false
            }, {
                data: 'article',
                name: 'article',
                searchable: false,
                orderable: false
            }, {
                data: 'size',
                name: 'size',
                searchable: false,
                orderable: false
            }, {
                data: 'code',
                name: 'code',
                searchable: false,
                orderable: false
            }, {
                data: 'defect_name',
                name: 'defect_name',
                searchable: false,
                orderable: false
            },{
                data: 'aksi',
                name: 'aksi',
                visible: true,
                searchable: false,
                orderable: false
            }
        ],
    
    });
    
    var dtable = $('#listRepairingAllTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();
    function storeRepair(url) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'post',
            url: url,
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function() {
                $.unblockUI();
            },
            success: function(response) {
                $('#listRepairingAllTable').DataTable().ajax.reload();

            },
            error: function(response) {
                $.unblockUI();
                if (response['status'] == 500)
                    $("#alert_error").trigger("click", 'Please Contact ICT.');
                
                if (response['status'] == 422||response['status']==419)
                    $("#alert_error").trigger("click", response.responseJSON.message);
            }
        })  
    }
</script>
