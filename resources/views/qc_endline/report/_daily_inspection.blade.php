<div class="panel panel-flat">
    <div class="panel-heading">
        <br>
        <div class="heading-elements">
            <div class="col-md-12">
                <div class="form-group  col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                        <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
                    </div>
                </div>
                <div class="form-group  col-lg-6">
                    <div class="input-group">
                        <a href="javascript:void(0)" onclick="return detailOutput()" ><span class="input-group-addon"><i class="icon-database-time2"></i>Outout Perjam</span></a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-xlg text-nowrap">
            <tbody>
                <tr>
                    <td class="col-md-4">
                        
                        <div class="media-left media-middle">
                            <a href="javascript:void(0)" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-thumbs-up2"></i></a>
                        </div>

                        <div class="media-left">
                            <h1 class="text-semibold no-margin">
                                <span id="total_check">{{$qcEndline->counter_day}}</span> <small class="display-block no-margin">Total Cek</small>
                            </h1>
                        </div>
                    </td>
                    <td class="col-md-4">
                        <div class="media-left media-middle">
                            <a href="javascript:void(0)" onclick="return detailDefect()" class="btn border-danger-400 text-danger-400 btn-flat btn-rounded btn-xs btn-icon" data-popup="tooltip" title="klik Untuk Melihat Detail Defect" data-placement="bottom" data-original-title="klik Untuk Melihat Detail Defect"><i class="icon-thumbs-down2"></i></a>
                        </div>
                        <div class="media-left">
                            <h1 class="text-semibold no-margin">
                                <span id="total_defect">{{$qcEndline->total_defect_day}}</span> <small class="display-block no-margin">Total Defect</small>
                            </h1>
                        </div>
                    </td>
                    <td class="col-md-4">
                        <div class="media-left media-middle">
                            <a href="javascript:void(0)" onclick="return detailDefectAll()" class="btn border-danger-400 text-danger-400 btn-flat btn-rounded btn-xs btn-icon" data-popup="tooltip" title="klik Untuk Melihat Detail Defect" data-placement="bottom" data-original-title="klik Untuk Melihat Detail Defect"><i class="icon-hammer-wrench"></i></a>
                        </div>
                        <div class="media-left">
                            <h1 class="text-semibold no-margin">
                                <span id="total_defect">{{$repairing}}</span> <small class="display-block no-margin">Defect Belum di Perbaiki</small>
                            </h1>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>	
    </div>

    <div class="table-responsive">
        <table class="table table-basic table-condensed" id="listOutputQcTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Style</th>
                    @if ($buyer=='other')
                        <th>Po Buyer</th>
                    @else
                        <th>No. Lot</th>
                    @endif
                    <th>Article</th>
                    <th>Size</th>
                    <th>Qty Order</th>
                    <th>Total Cek</th>
                    <th>Total Cek Hari</th>
                    <th>Balance</th>
                    <th>Total Defect</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{!! Form::hidden('url_data_output_qc',route('endLine.getDataOutputQc'), array('id' => 'url_data_output_qc')) !!}
{!! Form::hidden('url_data_sum_qc',route('endLine.getDataSumQc'), array('id' => 'url_data_sum_qc')) !!}

<script>
    $('.pickadate-weekday').pickadate({
        firstDay: 1
    });
    var url_data_output_qc = $("#url_data_output_qc").val();
    var listOutputQcTable = $('#listOutputQcTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: url_data_output_qc,
            data: function(d) {
                    return $.extend({}, d, {
                        "date"         : $('#txtDate').val()
                    });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = listOutputQcTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null,sortable: false,orderable: false,searchable: false},
            {
                data: 'date',
                name: 'date',
                searchable: true,
                visible: true,
                orderable: true
            }, {
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: false
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: false,
                orderable: false
            }, {
                data: 'article',
                name: 'article',
                searchable: false,
                orderable: false
            }, {
                data: 'size',
                name: 'size',
                searchable: false,
                orderable: false
            }, {
                data: 'qty_order',
                name: 'qty_order',
                searchable: false,
                orderable: false
            }, {
                data: 'total_counter',
                name: 'total_counter',
                searchable: false,
                orderable: false
            }, {
                data: 'counter_day',
                name: 'counter_day',
                searchable: false,
                orderable: false
            }, {
                data: 'balance_per_day',
                name: 'balance_per_day',
                searchable: false,
                orderable: false
            }, {
                data: 'total_defect_day',
                name: 'total_defect_day',
                searchable: false,
                orderable: false
            }
        ],
    
    });
    
    var dtable = $('#listOutputQcTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    $("#txtDate").change(function (e) { 
        e.preventDefault();
        getDataSum();
        dtable.draw();
    });
    function getDataSum() {
        var url_data_sum_qc = $("#url_data_sum_qc").val();
        var txtDate = $("#txtDate").val();
        $.ajax({
            url: url_data_sum_qc,
            data: {
                date: txtDate,
            },
        })
        .done(function (data) {
            $("#total_check").text(data.counter_day);
            $("#total_defect").text(data.total_defect_day);
        });  
    }
    function detailOutput() {
        var url = $("#url_get_detail_output").val();
        $('#reportDailyModal').modal('toggle');
        $('#detailOutputModal').modal();
        lov('detailOutput',url);
    }
    function detailDefect() {
        var url = $("#url_get_detail_defect").val();
        $('#reportDailyModal').modal('toggle');
        $('#detailModal').modal();
        lov('detail',url);
    }
    function detailDefectAll() {
        var url = $("#url_get_repairing_all").val();
        $('#reportDailyModal').modal('toggle');
        $('#reportRepairingAllModal').modal();
        lov('reportRepairingAll',url);
    }
    function lov(name,url) {
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var date  = $('#cmbdate').val();


        function itemAjax() {
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');

            $.ajax({
                    url: url,
                    data:{
                        date:date
                    },
                })
                .done(function(data) {
                    $(table).html(data);
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                });
        }
        itemAjax();
    }
    function detailClose() {
        $('#detailModal').modal('toggle');
        $("#btnReportDaily").trigger("click");
    }
    function detailOutputClose() {
        $('#detailOutputModal').modal('toggle');
        $("#btnReportDaily").trigger("click");
    }
    function reportRepairingAllClose() {
        $('#reportRepairingAllModal').modal('toggle');
        $("#btnReportDaily").trigger("click");
        $("#btnReportDaily").trigger("click");
        $("#btn_cari").trigger("click");
    }
    dtable.draw();
</script>