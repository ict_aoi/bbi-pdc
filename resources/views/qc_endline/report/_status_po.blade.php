<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                @include('form.text', [
                    'field'       => 'poreference',
                    'label'       => ($buyer=='nagai') ? 'No. Lot' : 'Po Buyer',
                    'placeholder' => 'Silahkan Masukan Po Buyer',
                    'label_col'   => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'    => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes' => [
                        'id' => 'poreference'
                    ]
                ])
            </div>
            <div class="col-md-4">
                @include('form.select', [
                    'field' => 'status',
                    'label' => 'Status',
                    'options' => [
                        '' => '-- Pilih Status --',
                        'outstanding' => 'Kurang Output',
                        'balance' => 'Balance',
                        'over' => 'Lebih Output'
                    ],
                    'class' => 'select-search',
                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes' => [
                        'id' => 'select_status'
                    ]
                ])
            </div>
            <div class="col-md-2">
                <button type="button" id="btnTampil" class="btn btn-primary legitRipple">Tampilkan <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-basic table-condensed" id="listStatusPoTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Terakhir Output</th>
                    <th>Style</th>
                    @if ($buyer=='nagai')
                        <th>No. Lot</th>
                    @else
                        <th>Po Buyer</th>
                    @endif
                    <th>Article</th>
                    <th>Size</th>
                    <th>Qty Order</th>
                    <th>Total Output</th>
                    <th>Balance</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{!! Form::hidden('url_data_status_po',route('endLine.getDataStatusPo'), array('id' => 'url_data_status_po')) !!}

<script>
    
    var url_data_status_po = $("#url_data_status_po").val();
    var listStatusPoTable = $('#listStatusPoTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: url_data_status_po,
            data: function(d) {
                    return $.extend({}, d, {
                        "poreference"         : $('#poreference').val(),
                        "status"         : $('#select_status').val(),
                    });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = listStatusPoTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null,sortable: false,orderable: false,searchable: false},
            {
                data: 'date',
                name: 'date',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'style',
                name: 'style',
                searchable: true,
                orderable: false
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: true,
                orderable: false
            }, {
                data: 'article',
                name: 'article',
                searchable: true,
                orderable: false
            }, {
                data: 'size',
                name: 'size',
                searchable: true,
                orderable: false
            }, {
                data: 'qty_order',
                name: 'qty_order',
                searchable: false,
                orderable: false
            }, {
                data: 'total_output',
                name: 'total_output',
                searchable: false,
                orderable: false
            }, {
                data: 'balance',
                name: 'balance',
                searchable: false,
                orderable: false
            }, {
                data: 'status',
                name: 'status',
                searchable: false,
                orderable: false
            }, {
                data: 'aksi',
                name: 'aksi',
                searchable: false,
                orderable: false
            }
        ],
    
    });
    
    var dtable = $('#listStatusPoTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    $("#btnTampil").click(function (e) { 
        e.preventDefault();
        dtable.draw();
    });
    function historyOutput(url) {
        $('#statusPOModal').modal('toggle');
        $("#historyPoModal").modal();
        lov('historyPo', url);
    }
    function lov(name, url) {
        var modal         = '#' + name + 'Modal';
        var table         = '#' + name + 'Table';

        function itemAjax() {
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');

            $.ajax({
                    url: url,
                })
                .done(function(data) {
                    $(table).html(data);
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                });
        }
        
        itemAjax();

    }
    function statusPOClose() {
        $('#statusPOModal').modal('toggle');
    }
    // dtable.draw();
</script>