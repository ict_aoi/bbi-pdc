<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="listDetailOutputTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Style</th>
                        @foreach ($workingHours as $w)
                            <th width="300px">{{$w->name}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1;?>
                    @foreach ($data as $d)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $d->style }}</td>
                            <td>{{ $d->i }}</td>
                            <td>{{ $d->ii }}</td>
                            <td>{{ $d->iii }}</td>
                            <td>{{ $d->iv }}</td>
                            <td>{{ $d->v }}</td>
                            <td>{{ $d->vi }}</td>
                            <td>{{ $d->vii }}</td>
                            <td>{{ $d->viii }}</td>
                            <td>{{ $d->ix }}</td>
                            <td>{{ $d->x }}</td>
                            <td>{{ $d->xi }}</td>
                            <td>{{ $d->xii }}</td>
                            <td>{{ $d->xiii }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>