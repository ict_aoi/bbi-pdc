<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="listHistoryPoTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Style</th>
                        <th>Po Buyer</th>
                        <th>Article</th>
                        <th>Size</th>
                        <th>Qty Order</th>
                        <th>Total Cek</th>
                        <th>Total Cek Hari</th>
                        <th>Balance</th>
                        <th>Total Defect</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($data->get() as $d)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$d->date}}</td>
                            <td>{{$d->style}}</td>
                            <td>{{$d->poreference}}</td>
                            <td>{{$d->article}}</td>
                            <td>{{$d->size}}</td>
                            <td>{{$d->qty_order}}</td>
                            <td>{{$d->total_counter}}</td>
                            <td>{{$d->counter_day}}</td>
                            <td>{{$d->balance_per_day}}</td>
                            <td>{{$d->total_defect_day}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function historyPoClose() {
        $('#historyPoModal').modal('toggle');
        $("#statuspo").trigger("click");
    }
</script>