
<div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="panel bg-teal-400">
            <div class="panel-body">
                <h2 class="no-margin total_defect">0</h2>
                Total Defect Inline
            </div>

            <div class="container-fluid">
                <div id="members-online"></div>
            </div>
        </div>

    </div>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="panel bg-pink-400">
            <div class="panel-body">
                <h2 class="no-margin total_proses">0</h2>
                Total Proses
            </div>
            <div id="server-load"></div>
        </div>

    </div>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

        <div class="panel bg-blue-400">
            <div class="panel-body">

                <h2 class="no-margin wft_inline">0%</h2>
                WFT Inline
            </div>

            <div id="today-revenue"></div>
        </div>

    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="reportRoundTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Proses</th>
                        <th>Nik Sewer</th>
                        <th>Nama Sewer</th>
                        <th>Round 1</th>
                        <th>Round 2</th>
                        <th>Round 3</th>
                        <th>Round 4</th>
                        <th>Round 5</th>
                        
                        @if ($is_infinity_round=='t')
                            <th>Round 6</th>
                            <th>Round 7</th>
                            <th>Round 8</th>
                        @endif
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


{!! Form::hidden('line_id',$line_id , array('id' => 'line_id')) !!}
{!! Form::hidden('is_infinity_round',($is_infinity_round)?'1':'0' , array('id' => 'is_infinity_round')) !!}
{!! Form::hidden('url_report_sample_rounds',route('qc_inline.report_data_sample_round'), array('id' => 'url_report_sample_rounds')) !!}
<script>
    
    $(document).ready(function () {
        var url_report_sample_round = $("#url_report_sample_rounds").val();
        var is_infinity_round = $("#is_infinity_round").val();
        if(is_infinity_round==1){
            var columns = [
                {data: null,sortable: false,orderable: false,searchable: false},

                {
                    data: 'process_name',
                    name: 'process_name',
                    searchable: true,
                    visible: true,
                    orderable: true
                }, {
                    data: 'sewer_nik',
                    name: 'sewer_nik',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'sewer_name',
                    name: 'sewer_name',
                    searchable: true,
                    visible: true,
                    orderable: true
                },  {
                    data: 'round1',
                    name: 'round1',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'round2',
                    name: 'round2',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round3',
                    name: 'round3',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round4',
                    name: 'round4',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round5',
                    name: 'round5',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round6',
                    name: 'round6',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round7',
                    name: 'round7',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round8',
                    name: 'round8',
                    searchable: false,
                    orderable: false
                }
            ];
        }else{
            var columns = [
                {data: null,sortable: false,orderable: false,searchable: false},

                {
                    data: 'process_name',
                    name: 'process_name',
                    searchable: true,
                    visible: true,
                    orderable: true
                }, {
                    data: 'sewer_nik',
                    name: 'sewer_nik',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'sewer_name',
                    name: 'sewer_name',
                    searchable: true,
                    visible: true,
                    orderable: true
                },  {
                    data: 'round1',
                    name: 'round1',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'round2',
                    name: 'round2',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round3',
                    name: 'round3',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round4',
                    name: 'round4',
                    searchable: false,
                    orderable: false
                },{
                    data: 'round5',
                    name: 'round5',
                    searchable: false,
                    orderable: false
                }
            ];
        }
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
        
        var reportRoundTable = $('#reportRoundTable').DataTable({
            //dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            //pageLength: 20,
            //deferRender: true,
            //ajax: url_report_sample_round,
            ajax: {
            // type: 'GET',
                url: url_report_sample_round,
                data: {
                    'line_id': $('#line_id').val()
                },
            },
            fnCreatedRow: function(row, data, index) {
                var info = reportRoundTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            //  $('td', row).eq(4).css('width', '270px');
            },
            columns: columns,
        
        });
        
        var dtable = $('#reportRoundTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        
        
        dtable.draw();
        getSewer();
    });
    function getSewer() {
        var url = $("#url_dashboard_report").val(); 
        var line_id = $("#line_id").val(); 
        $.ajax({
            type: 'get',
            url: url+'?line_id='+line_id
        })
        .done(function(response){
            $(".total_defect").text(response.data.total_defect);
            $(".wft_inline").text(response.data.wft);
            $(".total_proses").text(response.data.total_proses);
        })
    }

</script>
