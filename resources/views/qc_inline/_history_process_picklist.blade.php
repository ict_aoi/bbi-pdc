<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Code</th>
		<th>Nama Mesin</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->code) }}
				</td>
				<td>
					{{ strtoupper($list->machine_name) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-id="{{ $list->machine_id }}"
					data-name="{{ $list->machine_name }}" 
					>Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
