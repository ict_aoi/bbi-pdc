<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		@if ($buyer=='nagai')
			<th>No Lot</th>
		@else
			<th>Po Buyer</th>
		@endif
		<th>Style</th>
		<th>Article</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->poreference) }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					{{ strtoupper($list->article) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-id="{{ $list->production_id }}"
					data-workflow_id="{{ $list->workflow_id }}"
					data-buyer="{{ $list->poreference }}" 
					data-style="{{ $list->style }}" 
					data-style_merge="{{ $list->style_merge }}" 
					data-article="{{ $list->article }}" 
					>Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
