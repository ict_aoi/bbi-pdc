@extends('layouts.front')

@section('content-css')
    <link href="{{ mix('css/keyboard.css') }}" rel="stylesheet" type="text/css">
@endsection

@include('includes.front_navbar') 
@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                {{-- <h1><i class="icon-grid5 position-left"></i> <span class="text-semibold">ADMIN LOADING (LINE {{ ucwords($obj->line) }})</span></h1> --}}
            </div>
            <div class="heading-elements">
                {{-- <h1 class="text-semibold no-margin">{{ ucwords($obj->name) }} <small class="display-block">{{ ucwords($obj->subdept_name) }}</small></h1> --}}
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Qc</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    @if(isset($obj->nik))
        <div class="panel panel-flat">
            <div class="panel-body">
                @if($obj->line == 'all')
                    @include('form.picklist', [
                        'label' 		=> 'Line',
                        'field' 		=> 'line',
                        'name' 			=> 'line',
                        'placeholder' 	=> 'Silahkan Pilih Line',
                        'title' 		=> 'Silahkan Pilih Line',
                        'readonly'		=> true,
                    ])
                @endif
                @include('form.picklist', [
                    'label' 		=> $obj->buyer=='nagai'?"No Lot":'Poreference',
                    'field' 		=> 'poreference',
                    'name' 			=> 'poreference',
                    'placeholder' 	=> $obj->buyer=='nagai'?'Silahkan Pilih No Lot':'Silahkan Pilih Po Buyer',
                    'title' 		=> $obj->buyer=='nagai'?'Silahkan Pilih No Lot':'Silahkan Pilih Po Buyer',
                    'readonly'		=> true,
                ])
            </div>
        </div>

        <div class="panel panel-flat">
                
                <div class="panel-body">
                
                <br>
                <div class="row">
                    <center>
                        <div class="col-md-4"> <h1 class="no-margin text-semibold">STYLE : <span id="style_label">-</span></h1></div>
                        <div class="col-md-4"> <h1 class="no-margin text-semibold">ARTICLE : <span id="article_label">-</span></h1></div>
                        <div class="col-md-4"> <h1 class="no-margin text-semibold">ROUND : <span id="round_label">-</span></h1></div>
                    </center>
                </div>
                <br/>
                <br/>
                <div class="col-md-12 col-lg-12 col-sm-12">
                    
                    <div class="form-group  col-md-12 col-lg-12 col-sm-12" id="">
                        <label class="control-label text-semibold col-md-2 col-lg-2 col-sm-12">NIK</label>
                        <div class="input-group  col-md-10 col-lg-10 col-sm-12">
                            <input type="text" class="form-control" id="nik_label" name="nik_label" placeholder="Ketik Nama / NIK Sewer">
                            <span class="input-group-btn">
                                <button class="btn btn-default legitRipple" type="button" id="sewerButtonLookup">
                                <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                        
                    </div>               
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                    @include('form.text', [
                        'field'     => 'name_label',
                        'label'     => 'Nama',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                        'default'   => '',
                        'attributes' => [
                            'id'        => 'name_label',
                            'readonly'  => '',
                        ]
                    ])
                </div>
                <br><br>
                <div class="col-md-12 col-lg-12 col-sm-12">
                    @include('form.picklist', [
                        'label' 		=> 'Proses',
                        'field' 		=> 'Process',
                        'name' 			=> 'component',
                        'placeholder' 	=> 'Silahkan Pilih Proses',
                        'title' 		=> 'Silahkan Pilih Proses',
                        'readonly'		=> true,
                    ])
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    @include('form.select', [
                        'field'     => 'machine',
                        'label'     => 'Mesin',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                        'options' => [
                            '' => '-- Pilih Jenis Mesin --',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id'       => 'select_machine',
                            'required' =>''
                        ]
                    ])
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="col-md-12"></div>
                    
                    <div class="media-left media-middle">
                        <span class="beforeround"></span>
                        <span class="listround"></span>
                        <a href="#" class="btn btn-rounded btn-icon btn-xs legitRipple" id="color">
                            <span class="letter-icon" id="round_icon">-</span>
                        </a>
                    </div>
                    <table class="table table-basic table-condensed" id="sampleTable">
                        <thead>
                            <tr class="border-double">
                                <th width="50px">No</th>
                                <th>Jenis Defect</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                            <tbody id="tbody-items">
                        </tbody>
                    </table>
                </div>
                <br/>
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       => route('qc_inline.store'),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form'
                    ])
                !!}
                    {!! Form::hidden('items','[]' , array('id' => 'items')) !!}
                    {!! Form::hidden('production_id','', array('id' => 'production_id')) !!}
                    {!! Form::hidden('machine_id','', array('id' => 'machine_id')) !!}
                    {!! Form::hidden('sewer_nik','', array('id' => 'sewer_nik')) !!}
                    {!! Form::hidden('sewer_name','', array('id' => 'sewer_name')) !!}
                    {!! Form::hidden('process_id','', array('id' => 'process_id')) !!}
                    {!! Form::hidden('last_process','', array('id' => 'last_process')) !!}
                    {!! Form::hidden('items_grade','', array('id' => 'items_grade')) !!}
                    {!! Form::hidden('grade_before','0', array('id' => 'grade_before')) !!}
                    {!! Form::hidden('grade','', array('id' => 'grade')) !!}
                    {!! Form::hidden('round','', array('id' => 'round')) !!}
                    {!! Form::hidden('style','', array('id' => 'style')) !!}
                    {!! Form::hidden('style_merge','', array('id' => 'style_merge')) !!}
                    {!! Form::hidden('workflow_detail_id','', array('id' => 'workflow_detail_id')) !!}
                    {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
                    {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}
                    <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                    {!! Form::close() !!}
            </div>
        </div>

        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
            <li>
                <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                    <i class="fab-icon-open icon-grid3"></i>
                    <i class="fab-icon-close icon-cross2"></i>
                </a>

                <ul class="fab-menu-inner">
                    <li>
                        <div data-fab-label="Logout">
                            <a onclick="document.getElementById('qc_inline_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-switch2"></i>
                                <form id="qc_inline_logout" action="{{ route('qc_inline.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
                                {!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
                                {!! Form::hidden('buyer',$obj->buyer , array('id' => 'buyer')) !!}
                                </form>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Laporan Qc Inline" class="lap_qc_inline hidden">
                        <a data-toggle="modal" href="#" onclick="return report_sample()" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-cabinet"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Gabung Proses Operator" class="join_process hidden">
                        <a data-toggle="modal" href="#" data-target="#JoinProcessModal" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-make-group"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Pindah Round" class="next_round hidden">
                        <a href="javascript:void(0)" onclick="return nextRound()"  class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="glyphicon glyphicon-step-forward"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Send Message" class="send_message hidden">
                        <a href="javascript:void(0)" onclick="return sendMessage()"  class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-mail-read"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    @else 
        {!! Form::hidden('items','[]' , array('id' => 'items')) !!}
    @endif
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('url_line_picklist',route('qc_inline.linePicklist'), array('id' => 'url_line_picklist')) !!}
    {!! Form::hidden('url_get_status',route('qc_inline.get_status'), array('id' => 'url_get_status')) !!}
    {!! Form::hidden('url_get_process',route('qc_inline.get_process'), array('id' => 'url_get_process')) !!}
    {!! Form::hidden('url_get_process_all',route('qc_inline.get_process_all'), array('id' => 'url_get_process_all')) !!}
    {!! Form::hidden('url_poreference_picklist',route('qc_inline.poreferencePickList'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('url_get_sewer',route('qc_inline.get_sewer'), array('id' => 'url_get_sewer')) !!}
    {!! Form::hidden('url_dashboard_report',route('qc_inline.get_dashboard_report'), array('id' => 'url_dashboard_report')) !!}
    {!! Form::hidden('url_get_defect',route('qc_inline.get_defect'), array('id' => 'url_get_defect')) !!}
    {!! Form::hidden('url_get_history_grade',route('qc_inline.get_history_grade'), array('id' => 'url_get_history_grade')) !!}
    {!! Form::hidden('url_get_grade_before',route('qc_inline.get_grade_before'), array('id' => 'url_get_grade_before')) !!}
    {!! Form::hidden('url_report_sample_round',route('qc_inline.report_sample_round'), array('id' => 'url_report_sample_round')) !!}
    {!! Form::hidden('url_get_history_process',route('qc_inline.get_history_process'), array('id' => 'url_get_history_process')) !!}
    {!! Form::hidden('url_destroy_process_sewing',route('qc_inline.destroy_process_sewing'), array('id' => 'url_destroy_process_sewing')) !!}
    {!! Form::hidden('url_store_history_process',route('qc_inline.store_history_process'), array('id' => 'url_store_history_process')) !!}
    {!! Form::hidden('url_get_component_process',route('qc_inline.get_component_process'), array('id' => 'url_get_component_process')) !!}
    {!! Form::hidden('url_store_switch_round',route('qc_inline.store_switch_round'), array('id' => 'url_store_switch_round')) !!}
    {!! Form::hidden('url_show_history_operator_sewer',route('qc_inline.show_history_operator_sewer'), array('id' => 'url_show_history_operator_sewer')) !!}
    {!! Form::hidden('url_get_notification',route('qc_inline.get_notification'), array('id' => 'url_get_notification')) !!}
    {!! Form::hidden('url_complain_workflow',route('qc_inline.complain_workflow_data'), array('id' => 'url_complain_workflow')) !!}
    {!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
    {!! Form::hidden('url_send_message',route('qc_inline.send_message'), array('id' => 'url_send_message')) !!}
    {!! Form::hidden('line_name',($obj->line ? $obj->line : -1), array('id' => 'line_name')) !!}
    {!! Form::hidden('workflow_id','', array('id' => 'workflow_id')) !!}
    {!! Form::hidden('count_component','', array('id' => 'count_component')) !!}
    {!! Form::hidden('data_process_from','[]' , array('id' => 'data_process_from')) !!}
    {!! Form::hidden('data_process_to','[]' , array('id' => 'data_process_to')) !!}
    {!! Form::hidden('buyer',$obj->buyer , array('id' => 'buyer')) !!}
    {!! Form::hidden('machines','[]' , array('id' => 'machines')) !!}
@endsection

@section('page-modal')
    @include('qc_inline._login')
    @include('qc_inline._fields_sample_round')
    @include('qc_inline.copy_process._data_process_to')
    @include('qc_inline.copy_process._data_process_from')
    
    
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar Line',
		'placeholder'   => 'Cari berdasarkan nomor line',
    ])
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => ($obj->buyer=='other')?'Daftar List Po Buyer':'Daftar List NO. LOT',
		'placeholder'   => ($obj->buyer=='other')?'Cari berdasarkan Nama Po Buyer':'Cari berdasarkan Nama NO. LOT',
    ])
    @include('form.modal_picklist_c', [
		'name'          => 'process',
		'title'         => 'Daftar Process',
		'placeholder'   => 'Cari berdasarkan Nama Proses',
    ])
    @include('form.modal_picklist', [
		'name'          => 'component',
		'title'         => 'Daftar Komponen',
		'placeholder'   => 'Cari berdasarkan Nama Komponen',
    ])
    @include('form.modal_picklist', [
		'name'          => 'historyProcess',
		'title'         => 'History Proses',
		'placeholder'   => 'Cari berdasarkan Nama Mesin',
    ])
    @include('form.modal_picklist', [
		'name'          => 'history',
		'title'         => 'History Mesin',
		'placeholder'   => 'Cari berdasarkan Nama Mesin',
    ])
    @include('form.modal_picklist', [
		'name'          => 'history_sewer',
		'title'         => 'History Sewer',
		'placeholder'   => 'Cari berdasarkan Nama NIK',
    ])
    @include('form.modal_picklist', [
		'name'          => 'defect',
		'title'         => 'Daftar Defect',
		'placeholder'   => 'Cari berdasarkan Nama Defect / Code Defect',
    ])
    @include('form.modal', [
        'name' => 'report_sample_round',
        'title' => 'Laporan Qc Inline'
    ])
    @include('form.modal', [
        'name' => 'send_message',
        'title' => 'Send Message'
    ])
    @include('form.modal_picklist', [
		'name'          => 'sewer',
		'title'         => 'Daftar History Sewer',
		'placeholder'   => 'Cari berdasarkan Nama Sewer',
    ])
    @include('form.modal_picklist', [
		'name'          => 'complainWorkflow',
		'title'         => 'Daftar Complain Workflow',
		'placeholder'   => 'Cari berdasarkan Status/Style',
    ])
    @include('qc_inline.copy_process._lov_join_operator')
@endsection

@section('page-js')
    <script src="{{ mix('js/keyboard.js') }}"></script>
    <script src="{{ mix('js/qc_inline.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
@endsection
