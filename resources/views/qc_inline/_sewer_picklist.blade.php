<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr >
		<th>Action</th>
		<th>NIK</th>
		<th>Nama Sewer</th>
		<th>Nama Proses</th>
		<th>Nama Mesin</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr class="{{ ($list->qc_check>0)?"bg-success":'' }}">
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-workflow_detail_id="{{ $list->workflow_detail_id }}"
					data-nik="{{ $list->sewer_nik }}"
					data-name="{{ $list->sewer_name }}"
					data-proses_name="{{ $list->process_name }}"
					data-machine="{{ $list->machine_id }}"
					data-machine_name="{{ $list->machine_name }}"
					data-last_proses="{{ ($list->last_process=='t')?'1':'0' }}"
					>Pilih</button>
				</td>
				<td>
					{{ strtoupper($list->sewer_nik) }}
				</td>
				<td>
					{{ strtoupper($list->sewer_name) }}
				</td>
				<td>
					{{ strtoupper($list->process_name) }}
				</td>
				<td>
					{{ strtoupper($list->machine_name) }}
				</td>
				
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
