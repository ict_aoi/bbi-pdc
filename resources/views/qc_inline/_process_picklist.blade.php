<table class="table table-responsive">
    <thead>
    <tr>
        <th style="width:50px">Aksi</th>
        <th>Nama Proses</th>
        <th>Nama Mesin</th>
    </tr>
    </thead>
    
    <tbody>
        @foreach ($listProcess as $key => $list)
            <tr class="{{$list->last_process?"bg-danger":($list->count_inline>0?"bg-success":'')}}">
                <td>
                    <button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
                    type="button" 
                    data-workflow_detail_id="{{ $list->workflow_detail_id }}"
                    data-machine_id="{{ $list->machine_id }}"
                    data-machine_name="{{ $list->machine_name }}"
                    data-proses_name="{{ $list->proses_name }}"
                    data-last="{{ $list->last_process?1:0 }}" 
                    >Pilih</button>
                </td>
                <td>
                    {{ strtoupper($list->proses_name) }}
                </td>
                <td>
                    {{ strtoupper($list->machine_name) }}
                </td>
                
            </tr>
        @endforeach
    </tbody>
</table>

{!! $listProcess->appends(Request::except('page'))->render() !!}