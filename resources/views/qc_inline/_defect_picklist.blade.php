<table class="table table-responsive">
	<thead>
	  <tr>
        <th>Action</th>
        @if ($buyer=='nagai')
			<th>Kode</th>
			<th>Posisi</th>
		@elseif($buyer=='other')
			<th>No</th>
		@endif
		<th>Nama</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
        <tr bgcolor="{{$buyer=='other'?$list->color:null}}">
				<td>
					@if ($buyer=='nagai')
						<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->id_defect }}"
						data-name="{{ ucwords($list->position." ".$list->name_defect) }}"
						data-grade="{{ $list->grade_defect_id }}" 
						>Select</button>
					@else
						<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->id_defect }}"
						data-name="{{ ucwords($list->name_defect) }}"
						data-grade="{{ $list->grade_defect_id }}" 
						>Select</button>
					@endif
				</td>
                @if ($buyer=='nagai')
					<td>{{ strtoupper($list->code) }}</td>
					<td>{{ strtoupper($list->position) }}</td>
				@elseif($buyer=='other')
					<td>{{ strtoupper($list->id_defect) }}</td>
				@endif
				<td>
					{{ strtoupper($list->name_defect) }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
