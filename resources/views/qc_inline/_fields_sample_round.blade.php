<script type="x-tmpl-mustache" id="items-table">

    {% #item %}
      <tr>
        <td style="text-align:center;">{% no %}</td>
        <td>
          <div id="item_{% _id %}">{% defect_code %}</div>
          <input type="text" class="form-control input-sm hidden" id="itemInputId_{% _id %}"
            value="{% id %}" data-id="{% _id %}"
          >
        </td>
  
        <td width="50px">
          <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="icon-trash"></i></button>
        </td>
      </tr>
    {%/item%}
    <tr>
      <td width="20px">
        #
      </td>
      <td colspan="2">
        <input type="hidden" id="defect_id">
        <br>
        <div class="row">
          <div class="col-xs-6">
            <button type="button" id="addGood" class="btn btn-success btn-lg btn-block btn-rounded legitRipple">GOOD</button>
          </div>
          <div class="col-xs-6">
            <button type="button" id="AddDefect" class="btn btn-danger btn-lg btn-block btn-rounded legitRipple">Defect</button>
          </div>
        </div>
        <br>
      </td>
    </tr>
  </script>