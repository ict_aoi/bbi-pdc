<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>No. Complain</th>
		<th>Line</th>
		<th>QC</th>
		<th>Style</th>
		<th>Pesan</th>
		<th>Response IE</th>
		<th>Status</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->nomor) }}
				</td>
				<td>
					{{ strtoupper($list->name) }}
				</td>
				<td>
					{{ strtoupper($list->create_qc_name)."-".$list->create_qc_nik }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					{{ strtoupper($list->message_ie) }}
				</td>
				<td>
					{{ $list->message_qc }}
				</td>
				<td>
					@if ($list->status=='open')
						<span class="label label-danger">Open</span>
					@elseif($list->status=='close')
						<span class="label label-flat border-success text-success-600 position-right">Close</span>
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
