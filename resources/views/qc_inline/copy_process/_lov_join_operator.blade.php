<div id="JoinProcessModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Copy Proses Operator</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-5">
							<div class="form-group  col-md-6">
                                <label for="nik" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                                Nik Asal
                                </label>
                                <div class="control-input col-md-10 col-lg-10 col-sm-12">
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" class="form-control" id="nik_awal" name="nik_awal">
                                        <div class="form-control-feedback">
                                            <i class="icon-search4 text-size-base"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @include('form.text', [
                                    'field'     => 'name_awal',
                                    'label'     => 'Nama Asal',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                                    'default'   => '',
                                    'attributes' => [
                                        'id'        => 'name_awal',
                                        'readonly'  => '',
                                    ]
                                ])
                            </div>
                            <div class="col-md-12">
                                <table class="table table-basic table-condensed">
                                    <thead>
                                        <tr class="border-double">
                                            <th width="50px">No</th>
                                            <th>Nama Proses</th>
                                            <th>Nama Mesin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                        <tbody id="data_process_from_tbody">
                                    </tbody>
                                </table>
                            </div>
						</div>
						<div class="col-md-2">
                            <br><br><br>
							<div class="form-group  col-md-6">
                                <button type="button" id="btnCopy" class="btn btn-success btn-raised legitRipple"><i class="icon-make-group position-left"></i> Copy Proses</button>
                            </div>
                            <br><br>
						</div>
						<div class="col-md-5">
							<div class="form-group  col-md-6">
                                <label for="nik" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                                Nik Tujuan
                                </label>
                                <div class="control-input col-md-10 col-lg-10 col-sm-12">
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" class="form-control" id="nik_akhir" name="nik_akhir">
                                        <div class="form-control-feedback">
                                            <i class="icon-search4 text-size-base"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @include('form.text', [
                                    'field'     => 'name_akhir',
                                    'label'     => 'Nama Tujuan',
                                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                                    'default'   => '',
                                    'attributes' => [
                                        'id'        => 'name_akhir',
                                        'readonly'  => '',
                                    ]
                                ])
                            </div>
                            <div class="col-md-12">
                                <table class="table table-basic table-condensed">
                                    <thead>
                                        <tr class="border-double">
                                            <th width="50px">No</th>
                                            <th>Nama Proses</th>
                                            <th>Nama Mesin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                        <tbody id="data_process_to_tbody">
                                    </tbody>
                                </table>
                            </div>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" id="btnCopyProsesClose" data-dismiss="modal">Close</button>
					<button type="button" id="btnSaveCopyProcess" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
		</div>
	</div>
</div>
</div>