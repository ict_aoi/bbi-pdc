<div id="modal_login_qc_inline" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content login-form width-400">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="#qc-inline" data-toggle="tab"><h6>Qc Inline</h6></a></li>
			</ul>

			<div class="tab-content modal-body">
				<div class="tab-pane fade in active" id="qc-inline">
					{!!
						Form::open([
							'role' 		=> 'form',
							'url' 		=> route('qc_inline.login'),
							'method' 	=> 'post',
							'id'		=> 'login_qc_inline'
						])
					!!}	
						<div class="qc-inline-shade-screen" style="text-align: center;">
							<div class="qc-inline-shade-screen hidden">
								<img src="/images/ajax-loader.gif">
							</div>
						</div>
						<div id="qc-inline-body-login">
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Masukan NIK anda" name="nik" id="nik_qc_inline" autocomplete="off" required="required">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								{!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
								{!! Form::hidden('buyer',$obj->buyer , array('id' => 'buyer')) !!}
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>