<script type="x-tmpl-mustache" id="breakdown_size_scan_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>{% noOrder %}</td>
			<td>{% size %}</td>
			<td>{% qty_order %}</td>
			<td>{% wip %}</td>
			<td>{% balance %}</td>
			<td>{% inner_pack %}</td>
			<td>{% qty_input %}</td>
			
			{%#scan_garment%}
			<td width="100px">
				<button type="button" id="simpanQty_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-qty"><i class="icon-floppy-disk"></i></button>
			</td>
			{%/scan_garment%}
			{%^scan_garment%}
			<td>
				<div class="input-group col-xs-8">
					<div id="qtyFolding_{% _id %}">{% qty %}</div>
        			<input type="text" class="form-control input-sm hidden" id="qtyFoldingInput_{% _id %}" value="{% qty_available %}" data-id="{% _id %}" autocomplete="off">
				</div>
			</td>
			<td width="200px">
				<button type="button" id="editQty_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-qty"><i class="icon-pencil5"></i></button>
				<button type="button" id="simpanQty_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-qty hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancelQty_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-cancel-qty hidden"><i class="icon-close2""></i></button>
				<button type="button" id="fullQty_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-full-qty"><i class="icon-checkmark4""></i></button>
			</td>
			{%/scan_garment%}
			
		</tr>
		{%#scan_garment%}
		<tr>
			
			<td colspan="8">
				<input type="text" class="form-control input-xl txt-scan-garment" id="scanGarment_{% _id %}" data-id="{% _id %}" autocomplete="off">
			</td>
		</tr>
		{%/scan_garment%}
	{%/list%}
</script>