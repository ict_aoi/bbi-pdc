<div class="panel panel-flat">
    {!!
        Form::open([
            'role' 		=> 'form_alert',
            'url' 		=> route('folding.store_alert_folding',[$factory,$line]),
            'method' 	=> 'store',
            'class' 	=> 'form-horizontal',
            'id'		=>	'form_alert'
        ])
    !!}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    @include('form.select', [
                        'field'     => 'no_order',
                        'label'     => 'Nomor Order',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                        'options' => [
                            '' => '-- Pilih Order --',
                        ]+$listStyle,
                        'class' => 'select-search',
                        'attributes' => [
                            'id'       => 'no_order',
                            'required' =>''
                        ]
                    ])
                    @include('form.select', [
                        'field'     => 'size',
                        'label'     => 'Size',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                        'options' => [
                            '' => '-- Pilih Size --',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id'       => 'select_size',
                            'required' =>''
                        ]
                    ])
                    
                    
                </div>
                </div>
        </div>
        
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
    {!! Form::close() !!}
</div>
{!! Form::hidden('url_get_size',route('folding.get_size'), array('id' => 'url_get_size')) !!}
<script>
    $('.select-search').select2();
    $("#no_order").change(function (e) { 
        e.preventDefault();
        let id = $(this).val();
        let url = $("#url_get_size").val();
        $.ajax({
            type: 'get',
            url: url,
            data:{
                id:id,
            }
        })
        .done(function(response) {
            var size = response.size;
            console.log(size);

            $("#select_size").empty();
            $("#select_size").append('<option value="">-- Pilih Size --</option>');

            $.each(size, function(size, id) {

                $("#select_size").append('<option value="' + id + '">' + size + '</option>');
            });
        })
    });
    $('#form_alert').submit(function (event){
        event.preventDefault();
        
        bootbox.confirm("Apakah anda yakin kirim pesan ini ?.", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: $('#form_alert').attr('action'),
					data:new FormData($("#form_alert")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Message berhasil dikirim.')
                        $('#infoQcModal').modal('toggle');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status != 200) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });
</script>