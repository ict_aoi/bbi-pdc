<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
        padding:10px 10px;
    }
</style>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="container-fluid col-12">
            <table class="table table-basic table-condensed fixed" id="listHistoryPoTable">
                
                <thead>
                    <tr>
                        <th width="10px"></th>
                        <th>Style</th>
                        <th>No. Lot</th>
                        <th>Article</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($data as $d)
                        <tr data-toggle="collapse" onclick='return detailSize("{{ $d->production_id }}")' data-target="#collapseTwo_{{ $d->production_id }}" class="accordion-toggle row_{{ $d->production_id }}" aria-expanded="false">
                            <td><button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></td>
                            <td>{{ $d->style }}</td>
                            <td>{{ $d->poreference }}</td>
                            <td>{{ $d->article }}</td>
                        </tr>
                        <tr>
                            <td colspan="12" class="hiddenRow">
                                <div class="accordian-body collapse" id="collapseTwo_{{ $d->production_id }}"> 
                                    <table class="table table-striped" id="size_detail_{{ $d->production_id }}">
                                        <thead>
                                            <tr class="info">
                                                <th width="10px">Size</th>
                                                <th width="120px">Informasi</th>
                                                <th>Karton</th>	
                                            </tr>
                                        </thead>	
                                        <tbody>
                                            <tr data-toggle="collapse"  class="accordion-toggle" data-target="#demo10">
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> 
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{!! $data->appends(Request::except('page'))->render() !!}
<script>
    
</script>