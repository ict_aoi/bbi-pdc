@extends('layouts.front')

@section('content-css')
    <link href="{{ mix('css/keyboard.css') }}" rel="stylesheet" type="text/css">
    
@endsection

@include('includes.front_navbar')
@section('page-header')
    @if(isset($obj->nik))
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    {{-- <h1><i class="icon-grid5 position-left"></i> <span class="text-semibold">FOLDING (LINE {{ ucwords($obj->line) }})</span></h1> --}}
                </div>
                <div class="heading-elements">
                    {{-- <h1 class="text-semibold no-margin">{{ ucwords($obj->name) }} <small class="display-block">{{ ucwords($obj->subdept_name) }}</small></h1> --}}
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="{{ route('folding.index',[$obj->factory,$obj->line]) }}"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Folding</li>
                </ul>
            </div>
        </div>
    @endif
@endsection

@section('page-content')
    @if(isset($obj->nik)&&isset($obj->method_packing))
        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
            <li>
                <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                    <i class="fab-icon-open icon-grid3"></i>
                    <i class="fab-icon-close icon-cross2"></i>
                </a>

                <ul class="fab-menu-inner">
                    <li>
                        <div data-fab-label="Logout">
                            <a onclick="document.getElementById('folding_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-switch2"></i>
                                <form id="folding_logout" action="{{ route('folding.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
                                </form>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Laporan Output Scan Folding">
                            <a data-toggle="modal" class="btn btn-default btn-rounded btn-icon btn-float" onclick="return reportScanFolding()">
                                <i class="icon-cabinet"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Info Qc">
                            <a data-toggle="modal" class="btn btn-default btn-rounded btn-icon btn-float" onclick="return infoQc()">
                                <i class="icon-info22"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Shutdown">
                            <a href="#" data-toggle="modal" data-target="#shutdownModal" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-power-cord"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        @if ($obj->method_packing=='1')
            @include('folding._scan_packing')
        @elseif($obj->method_packing=='0')
            @include('folding._collected')
        @endif
    @endif
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('method',$obj->method_packing, array('id' => 'method')) !!}
    {!! Form::hidden('scan_garment',$obj->scan_garment, array('id' => 'scan_garment')) !!}
    {!! Form::hidden('buyer',($obj->buyer ? $obj->buyer : -1), array('id' => 'buyer')) !!}
    {!! Form::hidden('url_poreference_picklist',route('folding.poreference_picklist'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('url_report_folding_scan',route('folding.report_folding_scan'), array('id' => 'url_report_folding_scan')) !!}
    {!! Form::hidden('url_detail_size',route('folding.detail_size'), array('id' => 'url_detail_size')) !!}
    @endsection

@section('page-modal')
    @include('folding._login')
    @include('folding._breakdown_size')
    @include('folding._breakdown_size_scan')
    @include('folding._select_method_packing')
    @include('qc_endline._shutdown')
    @include('form.modal_picklist', [
		'name'          => 'reportScanFolding',
		'title'         => 'Laporan Output Scan Folding',
        'placeholder'   => ($obj->buyer=='other')?'Cari berdasarkan Nama Po Buyer':'Cari berdasarkan Nama NO. LOT',
    ])
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => ($obj->buyer=='other')?'Daftar List Po Buyer':'Daftar List NO. LOT',
		'placeholder'   => ($obj->buyer=='other')?'Cari berdasarkan Nama Po Buyer':'Cari berdasarkan Nama NO. LOT',
    ])
    @include('form.modal', [
		'name'          => 'infoQc',
		'title'         => 'Info Qc',
    ])
@endsection
{!! Form::hidden('url_ssh_reboot',route('endLine.ssh_reboot'), array('id' => 'url_ssh_reboot')) !!}
{!! Form::hidden('url_ssh_shutdown',route('endLine.ssh_shutdown'), array('id' => 'url_ssh_shutdown')) !!}
@section('page-js')
    <script src="{{ mix('js/floating_button.js') }}"></script>
    <script src="{{ mix('js/jquery.idle.min.js') }}"></script>
    {{-- <script src="{{ mix('js/folding.js') }}"></script> --}}
    <script>
        method       = $("#method").val();
        scan_garment = $("#scan_garment").val();
        if (method == 0&&user_session==-1) {
            list_breakdown_size_data = JSON.parse($('#breakdown_size_data').val());
        }else if(method == 1&&user_session==-1){
            list_breakdown_size_data= JSON.parse($('#breakdown_size_scan').val());
            document.getElementById("txtScanBarcode").focus();
        }
        $(document).ready( function ()
        { 
        $("#txtScanBarcode").click(function (e) { 
            e.preventDefault();
            $(this).select();
        });
        if(method == 1){
            $(document).idle({
                onIdle: function(){   
                    if(list_breakdown_size_data.length>0){
                        list_breakdown_size_data.forEach(element => {
                            if(parseInt(element.qty)>0){
                                let url = $('#url_store_idle_scan_packing').val();
                                let data = $('#breakdown_size_scan').val();
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: {
                                        data :data
                                    },
                                    beforeSend: function () {
                                        element.qty = 0;
                                        render();
                                    },
                                    cache: false,
                                    complete: function () {
                                    },
                                    success: function (response) {
                                        let status = response.status;
                                        if (status==1) {
                                            $('#status_com_label').removeClass("hidden");
                                            $('#status_on_label').addClass('hidden');
                                            $('#status_label').addClass('hidden');
                                            document.getElementById("txtScanBarcode").focus();
                                        } else if(status==0){
                                            $('#status_on_label').removeClass("hidden");
                                            $('#status_label').addClass('hidden');
                                            $('#status_com_label').addClass('hidden');
                                        }
                                        checkFocus();
                                    },
                                    error: function (response) {
                                        $.unblockUI();
                                        
                                        if (response.status == 422) {
                                            $("#alert_warning").trigger("click",response.responseJSON.message);
                                        };
                                        if (response.status == 500) {
                                            $("#alert_warning").trigger("click",response.responseJSON.message);
                                        };
                                        checkFocus();
                                    }
                                });
                                return;
                            }
                        });
                        
                    }else{
                        console.log('tidak ada data')
                    }
                },
                idle: 3000
            });
        }
        
        var user_session= $('#user_session').val();
            $('#user_session').trigger('change');
            $("#btn_cari").click(function (e) { 
                var production_id = $("#prod_id").val();
                var url           = $('#url_breakdown_size_data').val();
                e.preventDefault();
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        production_id : production_id,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        list_breakdown_size_data = [];
                        render();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) 
                    {
                        for (i in response) 
                        {
                            var data  = response[i]
                            var input = {
                                'id'                  : data.id,
                                'production_id'       : data.production_id,
                                'production_size_id'  : data.production_size_id,
                                'qc_endline_id'       : data.qc_endline_id,
                                'size'                : data.size,
                                'qty_order'           : data.qty_order,
                                'total_output_qc'     : data.total_output_qc,
                                'total_output_hangtag': data.total_output_hangtag,
                                'wip'                 : data.wip,
                                'qty_input'           : 0,
                            }
                            list_breakdown_size_data.push(input);
                            
                        }
                        
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
            
                        if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
                    }
                })
                .done(function ()
                {
                    render();
                });
            });
            $("#txtScanBarcode").change(function (e) { 
                e.preventDefault();
                var barcode = $(this).val();
                var url     = $('#url_breakdown_size_scan').val();
                e.preventDefault();
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        barcode : barcode,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        $("#txtScanBarcode").val(' ');
                        list_breakdown_size_data = [];
                        render();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) 
                    {
                        var status = response.status;
                        $("#inner_pack_label").text(response.inner_pack);
                        for (i in response.data) 
                        {
                            var data  = response.data[i]
                            var input = {
                                'id'                            : data.id,
                                'barcode_id'                    : data.barcode_garment,
                                'production_id'                 : data.production_id,
                                'production_size_id'            : data.production_size_id,
                                'folding_package_id'            : data.folding_package_id,
                                'folding_package_detail_size_id': data.folding_package_detail_size_id,
                                'noOrder'                       : data.style+"|"+data.poreference+"|"+data.article,
                                'size'                          : data.size,
                                'qty_order'                     : data.qty_order,
                                'balance'                       : data.balance,
                                'wip'                           : data.wip,
                                'qty_available'                 : data.qty_available,
                                'inner_pack'                    : data.inner_pack,
                                'qty_input'                     : data.qty_input,
                                'qty'                           : 0,
                                'scan_garment'                  : data.scan_garment,
                                'qty_polibag'                   : data.qty_polibag,
                            }
                            list_breakdown_size_data.push(input);
                            
                        }
                        if (status==1) {
                            $('#status_com_label').removeClass("hidden");
                            $('#status_on_label').addClass('hidden');
                            $('#status_label').addClass('hidden');
                        } else if(status==0){
                            $('#status_on_label').removeClass("hidden");
                            $('#status_label').addClass('hidden');
                            $('#status_com_label').addClass('hidden');
                        }
                        
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
            
                        if (response['status'] == 422)  {
                            $("#alert_warning").trigger('click', response.responseJSON.message);
                            document.getElementById("txtScanBarcode").focus();
                        };
                    }
                })
                .done(function ()
                {
                    render();
                    checkFocus();
                });
            });
        });
        

        $('#user_session').on('change',function(){
            var value = $(this).val();
            if(value == -1)
            {
                $('#modal_login_folding').modal();
            }
        });

        $('#login_folding').submit(function (event) 
        {
            event.preventDefault();
            var nik 	= $('#nik_folding').val();
            
            if (!nik) 
            {
                $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
                return false;
            }

            $.ajax({
                type: "POST",
                url: $('#login_folding').attr('action'),
                data: $('#login_folding').serialize(),
                beforeSend: function () 
                {
                    $('#modal_login_folding').find('.folding-shade-screen').removeClass('hidden');
                    $('#modal_login_folding').find('#folding-body-login').addClass('hidden');
                },
                complete: function () 
                {
                    $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
                },
                success: function () 
                {
                    $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
                    $('#login_folding').trigger("reset");
                },
                error: function (response) 
                {
                    $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
                    
                    if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
                }
            })
            .done(function (request)
            {
                document.location.href = request;
            });
        });

        $('#login_adm_loading').submit(function (event) 
        {
            event.preventDefault();
            var nik 	= $('#nik_adm_loading').val();
            
            if (!nik) 
            {
                $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
                return false;
            }

            $.ajax({
                type: "POST",
                url: $('#login_adm_loading').attr('action'),
                data: $('#login_adm_loading').serialize(),
                beforeSend: function () 
                {
                    $('#modal_login_folding').find('.admin-loading-shade-screen').removeClass('hidden');
                    $('#modal_login_folding').find('#admin-loading-body-login').addClass('hidden');
                },
                complete: function () 
                {
                    $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
                },
                success: function () 
                {
                    $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
                    $('#login_adm_loading').trigger("reset");
                },
                error: function (response) 
                {
                    $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
                    $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
                    
                    if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
                }
            })
            .done(function (request)
            {
                document.location.href = request;
            });
        });
        $('#poreferenceButtonLookup').click(function () {
            var url = $('#url_poreference_picklist').val();
            $("#poreferenceModal").modal('show');
            poreferencePicklist('poreference',  url + '?');
        });

        $('#poreferenceName').click(function () {
            var url = $('#url_poreference_picklist').val();
            
            poreferencePicklist('poreference', url + '?');
        });
        function poreferencePicklist(name, url) 
        {
            var search 		= '#' + name + 'Search';
            var modal 		= '#' + name + 'Modal';
            var table 		= '#' + name + 'Table';
            var buttonSrc 	= '#' + name + 'ButtonSrc';
            var buyer = $("#buyer").val();
            $(search).val('');
            
            function itemAjax()
            {
                var q = $(search).val();
                
                $(table).addClass('hidden');
                $(modal).find('.shade-screen').removeClass('hidden');
                $(modal).find('.form-search').addClass('hidden');

                $.ajax({
                    url: url+ '&q=' + q+'&buyer='+buyer
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
                });
            }

            function chooseItem()
            {
                var production_id            = $(this).data('id');
                var poreference              = $(this).data('buyer');
                var style                    = $(this).data('style');
                var article                  = $(this).data('article');
                var url                      = $('#url_breakdown_size_data').val();
                $('#prod_id').val(production_id);
                $('#poreference_label').text(poreference);
                $('#style_label').text(style);
                $('#article_label').text(article);
                
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {
                        production_id : production_id,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        list_breakdown_size_data = [];
                        render();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) 
                    {
                        for (i in response) 
                        {
                            var data  = response[i]
                            var input = {
                                'id'                  : data.id,
                                'production_id'       : data.production_id,
                                'production_size_id'  : data.production_size_id,
                                'qc_endline_id'       : data.qc_endline_id,
                                'size'                : data.size,
                                'qty_order'           : data.qty_order,
                                'total_output_qc'     : data.total_output_qc,
                                'total_output_hangtag': data.total_output_hangtag,
                                'wip'                 : data.wip,
                                'qty_input'           : 0,
                            }
                            list_breakdown_size_data.push(input);
                        }
                        
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
            
                        if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
                    }
                })
                .done(function ()
                {
                    render();
                });

                
            }

            function pagination()
            {
                $(modal).find('.pagination a').on('click', function (e) {
                    var params = $(this).attr('href').split('?')[1];
                    url = $(this).attr('href') + (params == undefined ? '?' : '');

                    e.preventDefault();
                    itemAjax();
                });
            }

            $(buttonSrc).unbind();
            $(search).unbind();

            $(buttonSrc).on('click', itemAjax);

            $(search).on('keypress', function (e) {
                if (e.keyCode == 13)
                    itemAjax();
            });

            itemAjax();
        }
        function render() 
        {
            if (method == 0) {
                getIndex();
                $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
                var tmpl 		= $('#breakdown_size_data_table').html();
                Mustache.parse(tmpl);
                var data 		= { list: list_breakdown_size_data };
                var html 		= Mustache.render(tmpl, data);
                
                $('#breadown_size_data_tbody').html(html);
                bind();
            } else if(method == 1) {
                getIndex();
                $('#breakdown_size_scan').val(JSON.stringify(list_breakdown_size_data));
                var tmpl 		= $('#breakdown_size_scan_table').html();
                Mustache.parse(tmpl);
                var data 		= { list: list_breakdown_size_data };
                var html 		= Mustache.render(tmpl, data);
                
                $('#breadown_size_scan_tbody').html(html);
                bind();
            }
            
        }

        function getIndex() 
        {
            if (method == 0) {
                $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
                for (idx in list_breakdown_size_data) 
                {
                    list_breakdown_size_data[idx]['_id'] 	= idx;
                    list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
                }
            } else if (method == 1) {
                $('#breakdown_size_scan').val(JSON.stringify(list_breakdown_size_data));
                for (idx in list_breakdown_size_data) 
                {
                    list_breakdown_size_data[idx]['_id'] 	= idx;
                    list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
                }
            }
            
        }

        function bind() 
        {
            $('.btn-edit-qty').on('click', editQty);
            $('.btn-save-qty').on('click', saveQty);
            $('.btn-cancel-qty').on('click', cancelEdit);
            $('.btn-cancel-qty').on('click', cancelEdit);
            if (method=='1') {
                if (scan_garment=='1') {
                    $('.txt-scan-garment').on('change', scanGarment);
                }
                $('.btn-full-qty').on('click', fullQty);
            }
        }
        function checkFocus() {

            for (var i in list_breakdown_size_data) {
                var data = list_breakdown_size_data[i];
                if (data.inner_pack>data.qty_input) {
                    document.getElementById("scanGarment_"+i).focus();
                    return false;
                }
                
            }

            return true;
        }
        function editQty() {
            var i = $(this).data('id');
            $('#qtyFolding_' + i).addClass('hidden');

            //remove hidden textbox
            $('#qtyFoldingInput_' + i).removeClass('hidden');
            $('#qtyFoldingInput_' + i).select();

            $('#editQty_'+i).addClass("hidden");
            if (method=='packing') {
                $('.btn-full-qty').addClass("hidden");
            }
            $('#simpanQty_' + i).removeClass('hidden');
            $('#cancelQty_' + i).removeClass('hidden');
        }
        function saveQty() {
            if (method==0) {
                var i                  = $(this).data('id');
                var url                = $("#url_breakdown_store_hangtag").val();
                var qty                = $('#qtyFoldingInput_' + i).val();
                var data               = list_breakdown_size_data[i];
                var qty_new            = data.wip-qty;
                var production_id      = data.production_id;
                var production_size_id = data.production_size_id;
                var qc_endline_id      = data.qc_endline_id;
                
                if (!$.isNumeric(qty)) {
                    $("#alert_error").trigger("click", 'Qty harus angka');
                    $('#qtyFoldingInput_' + i).focus();
                    return false;
                }
                if (qty == 0) {
                    $("#alert_error").trigger("click", 'Qty Loading Tidak boleh 0');
                    $('#qtyFoldingInput_' + i).focus();
                    return false;
                }
                if (qty_new < 0) {
                    $("#alert_error").trigger("click", 'Qty yang dimasukan lebih dari qty wip');
                    $('#qtyFoldingInput_' + i).focus();
                    return false;
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        production_id     : production_id,
                        production_size_id: production_size_id,
                        qc_endline_id     : qc_endline_id,
                        qty               : qty,
                    },
                    cache: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        data.qty_input            = 0;
                        data.wip                  = qty_new;
                        data.total_output_hangtag = parseInt(data.total_output_hangtag)+parseInt(qty);
                        $('#qtyFoldingInput_' + i).addClass('hidden');
                        $('#qtyFolding_' + i).removeClass('hidden');

                        $('#editQty_'+i).removeClass("hidden");
                        $('#simpanQty_' + i).addClass('hidden');
                        $('#cancelQty_' + i).addClass('hidden');
                        render();
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                            $("#btn_cari").trigger("click");
                        };
                    }
                });
            } else if(method==1) {
                var i                              = $(this).data('id');
                var dataScan                       = list_breakdown_size_data[i];
                var url                            = $("#url_store_scan_packing").val();
                var qty                            = (scan_garment==0)?$('#qtyFoldingInput_' + i).val():dataScan.qty;
                var production_id                  = dataScan.production_id;
                var production_size_id             = dataScan.production_size_id;
                var folding_package_id             = dataScan.folding_package_id;
                var folding_package_detail_size_id = dataScan.folding_package_detail_size_id;
                var barcode_id                     = dataScan.barcode_id;
                var wip                            = parseInt(dataScan.wip)-parseInt(qty);
                var qty_available                  = parseInt(dataScan.qty_available)-parseInt(qty);
                var inner_pack                     = parseInt(dataScan.inner_pack);
                var qty_input                      = parseInt(dataScan.qty);
                
                
                if (scan_garment==0) {
                    if (wip<0) {
                        $("#alert_warning").trigger("click", "WIP tidak boleh 0, Info Qc Untuk outputs");
                        $(this).focus();
                        checkFocus();
                        return;
                    }
                    if (qty_available<0) {
                        $("#alert_warning").trigger("click", "Qty Input tidak boleh lebih dari Qty Inner Pack");
                        $(this).focus();
                        return;
                    }
                
                }else if(scan_garment==1){
                    if (dataScan.qty==0) {
                        $("#alert_warning").trigger("click", "Data Tidak bisa di simpan,Silahkan Scan Garment");
                        checkFocus();
                        return;
                    }
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        production_id                 : production_id,
                        production_size_id            : production_size_id,
                        folding_package_id            : folding_package_id,
                        folding_package_detail_size_id: folding_package_detail_size_id,
                        qty_input                     : qty,
                        barcode_id                    : barcode_id,
                        scan_garment                  : scan_garment,
                        full_inner                    : 0,
                    },
                    cache: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        dataScan.qty = 0;
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        var qty                = response.qty;
                        var status                = response.status;

                            if (scan_garment==0) {
                                dataScan.wip           = parseInt(dataScan.wip)-parseInt(qty);
                                dataScan.qty_available = parseInt(dataScan.qty_available)-parseInt(qty);
                                dataScan.balance       = parseInt(dataScan.balance)-parseInt(qty);
                                dataScan.qty_input     = parseInt(dataScan.qty_input)+parseInt(qty);
                            }
                            if (status==1) {
                                $('#status_com_label').removeClass("hidden");
                                $('#status_on_label').addClass('hidden');
                                $('#status_label').addClass('hidden');
                                document.getElementById("txtScanBarcode").focus();
                            } else if(status==0){
                                $('#status_on_label').removeClass("hidden");
                                $('#status_label').addClass('hidden');
                                $('#status_com_label').addClass('hidden');
                            }
                            
                        render();
                        if (scan_garment==1) {
                            var dataRender = list_breakdown_size_data[i];
                            let countRow   = dataRender.length;
                            if (parseInt(countRow)>1) {
                                var index = parseInt(i)+1;
                                if (parseInt(countRow<index)) {
                                    document.getElementById("txtScanBarcode").focus();
                                }else{
                                    checkFocus();
                                }
                                
                            }else{
                                if (dataRender.inner_pack>dataRender.qty_input) {
                                    document.getElementById("scanGarment_"+i).focus();
                                }else{
                                    document.getElementById("txtScanBarcode").focus();
                                }
                            }
                        }
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON.message);
                        };
                    }
                });
            }
            
        }
        function scanGarment(){
            let i             = $(this).data('id');
            let barcode_id    = $(this).val();
            let data          = list_breakdown_size_data[i];
            let qty_input     = data.qty_input;
            let qty_available = data.qty_available;
            let wip           = data.wip;
            let balance       = data.balance;
            let qty           = data.qty;
            let qty_polibag   = parseInt(data.qty_polibag);
            let cekitem       = checkBarcodeGarment(barcode_id,i);
            
            if(barcode_id.length>13){
                $("#alert_warning").trigger("click", "Silahkan cek kembali barcode garment");
                $(this).val('');
                $('#qtyFoldingInput_' + i).focus();
                return false;
            }
            if ((parseInt(wip)-qty_polibag)<0) {
                $("#alert_warning").trigger("click", "Wip Tidak boleh 0");
                $(this).val('');
                //$('#qtyFoldingInput_' + i).focus();
                checkFocus();
                return false;
            }
            if ((parseInt(qty_available)-qty_polibag)<0) {
                $("#alert_warning").trigger("click", "Qty Input tidak boleh lebih dari Qty Inner Pack");
                $(this).val('');
                var countRow = list_breakdown_size_data.length;
                if (parseInt(countRow)>qty_polibag) {
                    var index = parseInt(i)+qty_polibag;
                    if (parseInt(countRow<index)) {
                        document.getElementById("txtScanBarcode").focus();
                    }else{
                        checkFocus();
                    }
                    
                }else{
                    document.getElementById("txtScanBarcode").focus();
                }
                return;
            }
            if (data.barcode_id!=-1) {
                if (!cekitem) {
                    $("#alert_warning").trigger("click", "Barcode Salah");
                    
                    $(this).val('');
                    checkFocus();
                    return false;
                }
            }
            let cek_complete       = qty_available-qty_polibag;

            data.qty_available = cek_complete;
            data.barcode_id    = barcode_id;
            data.qty_input     = parseInt(qty_input)+qty_polibag;
            data.wip           = parseInt(wip)-qty_polibag;
            data.balance       = parseInt(balance)-qty_polibag;
            data.qty           = parseInt(qty)+qty_polibag;
            render();
            $(this).val('');
            checkFocus();
            if (cek_complete==0) {
                $("#simpanQty_"+i).trigger("click");
            }

            
        }
        function checkBarcodeGarment(barcode_id,index) {

            for (var i in list_breakdown_size_data) {
                var data = list_breakdown_size_data[index];
                if (data.barcode_id == barcode_id){
                    return true;
                }
                    
                    // console.log(data.barcode_id+"-"+barcode_id);
            }

            return false;
        }
        function cancelEdit() {
            var i = $(this).data('id');
            var qty  = $('#qtyFolding_' + i).val();
            var data = list_breakdown_size_data[i];
            $('#qtyLoadingInput_' + i).removeClass('hidden');

            //add hidden textbox
            $('#qtyFolding_' + i).addClass('hidden');

            $('#simpanQty_' + i).addClass('hidden');
            $('#cancelQty_' + i).addClass('hidden');
            $('#editQty_' + i).removeClass('hidden');
            qty = data.qty_input;
            render();
        }
        function fullQty() {
            var i                              = $(this).data('id');
            var data                           = list_breakdown_size_data[i];
            var url                            = $("#url_store_scan_packing").val();
            var production_id                  = data.production_id;
            var production_size_id             = data.production_size_id;
            var folding_package_id             = data.folding_package_id;
            var folding_package_detail_size_id = data.folding_package_detail_size_id;
            var wip                            = parseInt(data.wip);
            var qty_available                  = parseInt(data.qty_available);
            var inner_pack                     = parseInt(data.inner_pack);
            var qty_input                      = parseInt(data.qty_input);
            if ((wip-qty_available)<0) {
                $("#alert_warning").trigger("click", "WIP tidak boleh 0, Info Qc Untuk output");
                return;
            }
            if (qty_available<=0) {
                $("#alert_warning").trigger("click", "Qty Inner Pack sudah penuh");
                return;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    production_id                 : production_id,
                    production_size_id            : production_size_id,
                    folding_package_id            : folding_package_id,
                    folding_package_detail_size_id: folding_package_detail_size_id,
                    qty_input                 : qty_available,
                    full_inner                 : 1,
                },
                cache: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    var qty                = response.qty;
                    var status                = response.status;

                        data.wip           = parseInt(data.wip)-parseInt(qty);
                        data.qty_available = parseInt(data.qty_available)-parseInt(qty);
                        data.balance       = parseInt(data.balance)-parseInt(qty);
                        data.qty_input     = parseInt(data.qty_input)+parseInt(qty);
                        if (status==1) {
                            $('#status_com_label').removeClass("hidden");
                            $('#status_on_label').addClass('hidden');
                            $('#status_label').addClass('hidden');
                            document.getElementById("txtScanBarcode").focus();
                        } else if(status==0){
                            $('#status_on_label').removeClass("hidden");
                            $('#status_label').addClass('hidden');
                            $('#status_com_label').addClass('hidden');
                        }
                        
                    render();
                },
                error: function (response) {
                    $.unblockUI();
                    
                    if (response.status == 422) {
                        $("#alert_warning").trigger("click",response.responseJSON.message);
                    };
                }
            });
        }
        function reportScanFolding() {
            var url = $("#url_report_folding_scan").val();
            $("#reportScanFoldingModal").modal();
            lovScanFolding('reportScanFolding', url);
        }
        function lovScanFolding(name,url){
            var search = '#' + name + 'Search';
            var item_id = '#' + name + 'Id';
            var item_name = '#' + name + 'Name';
            var modal = '#' + name + 'Modal';
            var table = '#' + name + 'Table';
            var buttonSrc = '#' + name + 'ButtonSrc';
            $(search).val('');
        
            function itemAjax() {
        
                var q                   = $(search).val();
                var cycle_time_batch_id = $("#cycle_time_batch_id").val();
                $(table).addClass('hidden');
                $(modal).find('.shade-screen').removeClass('hidden');
                $(modal).find('.form-search').addClass('hidden');
        
                $.ajax({
                    url: url,
                    data: {
                        q                  : q
                    },
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');

                });
            }
            
        
            function pagination() {
                $(modal).find('.pagination a').on('click', function (e) {
                    var params = $(this).attr('href').split('?')[1];
                    url = $(this).attr('href') + (params == undefined ? '?' : '');
                    e.preventDefault();
                    itemAjax();
                });
            }
        
            $(buttonSrc).unbind();
            $(search).unbind();
        
            $(buttonSrc).on('click', itemAjax);
        
            $(search).on('keypress', function (e) {
                if (e.keyCode == 13)
                    itemAjax();
            });
        
            itemAjax();
        }
        function reportScanFoldingClose() {
            $('#reportScanFoldingModal').modal('toggle');
        }
        function lov(name, url) {
            var modal         = '#' + name + 'Modal';
            var table         = '#' + name + 'Table';

            function itemAjax() {
                $(table).addClass('hidden');
                $(modal).find('.shade-screen').removeClass('hidden');

                $.ajax({
                        url: url,
                    })
                    .done(function(data) {
                        $(table).html(data);
                        $(table).removeClass('hidden');
                        $(modal).find('.shade-screen').addClass('hidden');
                    });
            }
            
            itemAjax();

        }
        function reboot() {
            var url_ssh_reboot = $("#url_ssh_reboot").val();
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
                type: "post",
                url: url_ssh_reboot,
                beforeSend: function() {
                    $('#shutdownModal').modal('toggle');
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {

                },
                error: function(response) {
                    $.unblockUI();
                    if (response['status'] == 500)
                        $("#alert_error").trigger("click", response.responseJSON.message);
                    
                    if (response['status'] == 422||response['status']==419)
                        $("#alert_error").trigger("click", response.responseJSON.message);
                }
            })
            .done(function() {
            });
        }
        function shutdown() {
            var url_ssh_shutdown = $("#url_ssh_shutdown").val();
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
                type: "post",
                url: url_ssh_shutdown,
                beforeSend: function() {
                    $('#shutdownModal').modal('toggle');
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {

                },
                error: function(response) {
                    $.unblockUI();
                    if (response['status'] == 500)
                        $("#alert_error").trigger("click", response.responseJSON.message);
                    if (response['status'] == 422||response['status']==419)
                        $("#alert_error").trigger("click", response.responseJSON.message);
                }
            })
            .done(function() {
            });
        }
        function infoQc(){
            $("#infoQcModal").modal();
            var name ='infoQc';
            var search = '#' + name + 'Search';
            var modal = '#' + name + 'Modal';
            var table = '#' + name + 'Table';
            var buttonSrc = '#' + name + 'ButtonSrc';
            var url = $("#url_send_alert_qc").val();
            console.log(url);
            $(search).val('');

            function itemAjax() {
                var q = $(search).val();

                $(table).addClass('hidden');
                $(modal).find('.shade-screen').removeClass('hidden');
                $(modal).find('.form-search').addClass('hidden');

                $.ajax({
                        url: url 
                    })
                    .done(function (data) {
                        $(table).html(data);
                        $(search).focus();
                        $(table).removeClass('hidden');
                        $(modal).find('.shade-screen').addClass('hidden');
                        $(modal).find('.form-search').removeClass('hidden');
                        //$(table).find('.btn-choose').on('click', chooseItem);
                    });
            }
            itemAjax();
        }
        function infoQcClose() {
            $('#infoQcModal').modal('toggle');
        }
        function changeLine() {
            $("#line-class").removeClass("hidden");
        }
        function detailSize(production_id){
            let url = $('#url_detail_size').val();
            //let test = document.getElementsByClassName('row_'+production_id).getAttribute('aria-expanded');
            var n = document.getElementsByClassName('row_'+production_id);
            //$('#size_detail_'+production_id+'> tbody tr').remove();
            var l = $('.row_'+production_id).attr('aria-expanded');
            console.log(l);
            
            
            if(l=='false'){
                $('#size_detail_'+production_id+'> tbody').prepend(
                    $('<tr>').append(
                        $('<td colspan="3" align="center">').html("<img src='/images/ajax-loader.gif'>")
                    )
                )
                $.ajax({
                type: "GET",
                url: url,
                data: {
                    production_id : production_id,
                },
                
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) 
                {
                    $('#size_detail_'+production_id+'> tbody tr').remove();
                    const data = response.data;
                    for (i in response.data) 
                    {
                        const data  = response.data[i];
                        let isi ='';
                        let badge ='';
                        const details= data.folding_details;
                        var obj = jQuery.parseJSON(data.folding_details);
                        if(data.folding_output_id!= null){
                            $.each(obj, function(key,value) {
                                let badge = value.current_status=='onprogress'?"badge badge-flat border-danger text-danger-600 position-right":"badge badge-flat border-primary text-primary-600 position-right";

                                isi += "<div class='btn-group dropdown'><a href='#' class='"+badge+"' data-toggle='dropdown' aria-expanded='true'>"+value.total_scan+"<span class='caret'></span></a><ul class='dropdown-menu dropdown-menu-right'><li><a href='javascript:void(0)'><span class='status-mark position-left border-danger'></span>"+value.barcode_id+"</a></li><li><a href='javascript:void(0)'><span class='status-mark position-left border-info'></span> Pertama Scan - "+value.created_at+"</a></li><li><a href='javascript:void(0)'><span class='status-mark position-left border-primary'></span> Trakhir Scan - "+value.updated_at+"</a></li></ul></div>";
                            }); 
                        }
                        let scan_karton = data.total_scan?data.scan_karton:0;
                        let pkg_count = data.pkg_count?data.pkg_count:0;
                        $('#size_detail_'+production_id+'> tbody').prepend(
                            $('<tr>').append(
                                $('<td>').text(data.size)
                            ).append(
                                $('<td>').html("Qty.Loading: "+data.qty+"</br>Tot.Scan: "+scan_karton+" </br>Tot.Karton: "+scan_karton+"/"+pkg_count)
                            ).append(

                                $('<td>').html(isi)
                            )
                        )
                        
                        
                        
                    }
                    
                },
                error: function (response) 
                {
                    $.unblockUI();
                    
                    if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
        
                    if (response['status'] == 422)  {
                        
                    };
                }
                })
                .done(function ()
                {
                    
                });
            }else{
                $('#size_detail_'+production_id+'> tbody tr').remove();
            }
        }
        
    </script>
@endsection
