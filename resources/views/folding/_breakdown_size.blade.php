<script type="x-tmpl-mustache" id="breakdown_size_data_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>
				{% no %}. 
			</td>
			<td>{% size %}</td>
			<td>
				<div class="input-group col-xs-8">
					<div id="qty_order_{% _id %}">{% qty_order %}</div>
				</div>
			</td>
			<td>
				<div class="input-group col-xs-8">
					<div id="total_output_qc_{% _id %}">{% total_output_qc %}</div>
				</div>
			</td>
			<td>
				<div class="input-group col-xs-8">
					<div id="total_output_hangtag_{% _id %}">{% total_output_hangtag %}</div>
				</div>
			</td>
			
			<td>
				<div class="input-group col-xs-8">
					<div id="wip_{% _id %}">{% wip %}</div>
				</div>
			</td>
			
			<td>
				<div class="input-group col-xs-8">
					<div id="qtyFolding_{% _id %}">{% qty_input %}</div>
        			<input type="text" class="form-control input-sm hidden" id="qtyFoldingInput_{% _id %}" value="{% qty_outstanding %}" data-id="{% _id %}" autocomplete="off">
				</div>
			</td>
			<td width="200px">
				<button type="button" id="editQty_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-qty"><i class="icon-pencil5"></i></button>
				<button type="button" id="simpanQty_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-qty hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancelQty_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-cancel-qty hidden"><i class="icon-close2""></i></button>
			</td>
		</tr>
	{%/list%}
</script>