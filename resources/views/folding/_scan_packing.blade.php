<style>
    table td[class*=col-], table th[class*=col-] {
        position: static;
        display: table-cell;
        float: none;
    }
</style>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group col-lg-12 " id="">
            <label class="control-label text-semibold col-lg-2">Scan</label>
            <input id="txtScanBarcode" class="form-control"  placeholder="Silahkan Scan Barcode Karton di sini" autocomplete="off" name="scan_barcode" type="text" autofocus>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <center>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">Isi Karton <span id="inner_pack_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">Status <span class="label bg-danger hidden" id="status_on_label">On Proses</span><span class="label bg-blue hidden" id="status_com_label">Complete</span><span id="status_label">-</span></h1></div>
            </center>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-basic table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No. Order</th>
                            <th>Size</th>
                            <th>Qty Order</th>
                            <th>WIP</th>
                            <th>Balance</th>
                            <th>Inner Pack</th>
                            <th>Qty Input</th>
                            @if ($obj->method_packing==0)
                                <th>Input</th>
                            @endif
                            <th>Aksi</th>
                        </tr>
                        <tr>
                        </tr>
                    </thead>
                    <tbody id="breadown_size_scan_tbody">
                    </tbody>
                </table>
            </div>
        </div>

        <br/>
        {!! Form::hidden('breakdown_size_scan','[]' , array('id' => 'breakdown_size_scan')) !!}
    </div>
</div>
{!! Form::hidden('url_breakdown_size_scan',route('folding.breakdown_size_scan'), array('id' => 'url_breakdown_size_scan')) !!}
{!! Form::hidden('url_store_scan_packing',route('folding.store_packing',[$obj->factory,$obj->line]), array('id' => 'url_store_scan_packing')) !!}
{!! Form::hidden('url_store_idle_scan_packing',route('folding.store_idle_scan_packing',[$obj->factory,$obj->line]), array('id' => 'url_store_idle_scan_packing')) !!}
{!! Form::hidden('url_send_alert_qc',route('folding.send_alert_qc',[$obj->factory,$obj->line]), array('id' => 'url_send_alert_qc')) !!}
