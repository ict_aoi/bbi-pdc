<div id="modal_method_folding" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content width-400">
            <div class="panel-body">
                <div>
                    <a href="{{ route('folding.set_type_folding',[$obj->factory,$obj->line,'packing']) }}" style="height: 70px;font-size:40px;display: block" class="btn bg-primary-400 col-xs-12 legitRipple"
							onclick="event.preventDefault();
									document.getElementById('packing-form').submit();">
							Kirim Packing</a>
							<form id="packing-form" action="{{ route('folding.set_type_folding',[$obj->factory,$obj->line,'packing']) }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                </div>
                <div>
                    <a href="{{ route('folding.set_type_folding',[$obj->factory,$obj->line,'pooled']) }}" style="height: 70px;font-size:40px;margin-top:24px" class="btn bg-success-400 col-xs-12 legitRipple"
							onclick="event.preventDefault();
									document.getElementById('pooled-form').submit();">
							Dikumpulkan</a>
							<form id="pooled-form" action="{{ route('folding.set_type_folding',[$obj->factory,$obj->line,'pooled']) }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                </div>
                <div>
                    <div data-fab-label="Logout">
                            <a onclick="document.getElementById('folding_logout').submit();" style="height: 70px;font-size:40px;margin-top:24px" class="btn bg-danger-400 col-xs-12 legitRipple">
                                Log Out
                                <form id="folding_logout" action="{{ route('folding.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
                                </form>
                            </a>
                        </div>
                </div>
            </div>
		</div>
	</>
</div>