<div id="modal_login_folding" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content login-form width-400">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="btn-group">
							<button type="button" class="btn bg-teal-400 btn-labeled dropdown-toggle legitRipple" data-toggle="dropdown"><b><i class="icon-power-cord"></i></b> SHUTDOWN <span class="caret"></span></button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#" onclick="shutdown()"><i class="icon-switch2"></i> Matikan Daya</a></li>
								<li><a href="#" onclick="reboot()"><i class="icon-reload-alt"></i> Mulai Ulang</a></li>
								@if ($obj->buyer=='nagai')
									<li><a href="#" onclick="changeLine()"><i class="icon-cog3"></i> Ganti Line</a></li>
								@endif
							</ul>
						</div>
					</div>
					{!!
						Form::open([
							'role' 		=> 'form',
							'url' 		=> route('folding.login'),
							'method' 	=> 'post',
							'id'		=> 'login_folding'
						])
					!!}	
					<div class="col-md-8 hidden" id="line-class">
						@include('form.select', [
                                'field' => 'change_line',
                                'label' => '',
                                'options' => [
                                    ''       => '-- Pilih Line --',
                                ]+$lines,
                                'class' => 'select-search',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'change_line',
                                ]
                            ])
					</div>
				</div>
				
			</div>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="#folding" data-toggle="tab"><h6>Folding</h6></a></li>
				@if($obj->buyer == 'other')<li><a href="#admin-loading" data-toggle="tab"><h6>Admin Loading</h6></a></li>@endif
			</ul>

			<div class="tab-content modal-body">
				<div class="tab-pane fade in active" id="folding">
					
						<div class="folding-shade-screen" style="text-align: center;">
							<div class="folding-shade-screen hidden">
								<img src="/images/ajax-loader.gif">
							</div>
						</div>
						<div id="folding-body-login">
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control keyboard keyboard-numpad" placeholder="Masukan NIK anda" name="nik" id="nik_folding" autocomplete="off" required="required">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								{!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
								{!! Form::hidden('active_tab','folding', array('id' => 'active_tab')) !!}
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>

				<div class="tab-pane fade" id="admin-loading">
					{!!
						Form::open([
							'role' 		=> 'form',
							'url' 		=> route('folding.login'),
							'method' 	=> 'post',
							'id'		=> 'login_adm_loading'
						])
					!!}	

						<div class="admin-loading-shade-screen" style="text-align: center;">
							<div class="admin-loading-shade-screen hidden">
								<img src="/images/ajax-loader.gif">
							</div>
						</div>
						
						<div id="admin-loading-body-login">
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control keyboard form-control keyboard-numpad" placeholder="Masukan NIK anda" name="nik" id="nik_adm_loading">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>
							
							<div class="form-group">
								{!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
								{!! Form::hidden('active_tab','admin-loading', array('id' => 'active_tab')) !!}
								<button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>