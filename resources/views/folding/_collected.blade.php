<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                @include('form.picklist', [
                    'label' 		=> ($obj->buyer=='other')?'Po Buyer':'No. LOT',
                    'field' 		=> 'poreference',
                    'name' 			=> 'poreference',
                    'placeholder' 	=> ($obj->buyer=='other')?'Silahkan pilih PO BUYER':'Silahkan pilih NO. LOT',
                    'title' 		=> ($obj->buyer=='other')?'Silahkan pilih PO BUYER':'Silahkan pilih NO. LOT',
                    'readonly'		=> true,
                ])
            </div>
            <button type="button" id="btn_cari" class="btn bg-info-300 col-xs-12">Cari <i class="icon-search4 position-left"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <center>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">POREFERENCE <span id="poreference_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">ARTICLE <span id="article_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">STYLE <span id="style_label">-</span></h1></div>
            </center>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-basic table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Size</th>
                            <th>Qty Order</th>
                            <th>Tot. Qty Qc</th>
                            <th>Tot. Qty Hang Tag</th>
                            <th>WIP</th>
                            <th>Qty Input</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="breadown_size_data_tbody">
                    </tbody>
                </table>
            </div>
        </div>

        <br/>
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('adminLoading.store',[$obj->factory,$obj->line]),
                'class'     => 'form-horizontal',
                'enctype'   => 'multipart/form-data',
                'id'        => 'form'
            ])
        !!}
            {!! Form::hidden('production_id','', array('id' => 'production_id')) !!}
            {!! Form::hidden('production_size_id','', array('id' => 'production_size_id')) !!}
            {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
         {!! Form::close() !!}
    </div>
</div>
{!! Form::hidden('url_breakdown_size_data',route('folding.breakdown_size_data'), array('id' => 'url_breakdown_size_data')) !!}
{!! Form::hidden('url_breakdown_store_hangtag',route('folding.store_hangtag',[$obj->factory,$obj->line]), array('id' => 'url_breakdown_store_hangtag')) !!}
{!! Form::hidden('prod_id','', array('id' => 'prod_id')) !!}
