@extends('layouts.app',['active' => 'cycle-time'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cycle Time</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Cycle Time</li>
        </ul>
        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-gear position-left"></i>
                    Action
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    @if (Auth::user()->is_super_admin)
                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#filterFactoryModal"><i class="icon-office"></i>Factory</a></li>
                        <li class="divider"></li>
                    @endif
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#createHeaderCycleModal" onclick="return create()"><i class="icon-plus2"></i>Create</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="TableHistoryCycle">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Line</th>
                        <th>Style</th>
                        <th>Condition Day</th>
                        <th>NIK</th>
                        <th>Name</th>
                        <th>Date Start</th>
                        <th>Finish Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection
{!! Form::hidden('url_style_outstanding',route('alur_kerja.style_outstanding'), array('id' => 'url_style_outstanding')) !!}
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'style',
        'title'       => 'Daftar Style',
        'placeholder' => 'Cari berdasarkan Style',
    ])
    @include('cycle_time.lov._lov_create_header_cycle')
    @include('cycle_time.lov._lov_filter_factory')
@endsection
@section('page-js')
{{--  <script src="{{ mix('js/data_workflow.js') }}"></script>  --}}
<script>
    $(document).ready(function () {
        $('#day').keypress(function (e) {
            if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
                return false;
            }
            return true;
        });
        var TableHistoryCycle = $('#TableHistoryCycle').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 20,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/skill-matriks/cycle-time/data',
            },
            fnCreatedRow: function (row, data, index) {
                var info = TableHistoryCycle.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'line',
                name: 'line',
                visible: true,
                orderable: true
            },{
                data: 'style',
                name: 'style',
                visible: true,
                orderable: true
            }, {
                data: 'condition_day',
                name: 'condition_day',
                visible: true,
                orderable: true
            }, {
                data: 'nik',
                name: 'nik',
                visible: true,
                orderable: true
            }, {
                data: 'name',
                name: 'name',
                visible: true,
                orderable: true
            }, {
                data: 'start_job',
                name: 'start_job',
                visible: true,
                orderable: true
            }, {
                data: 'end_job',
                name: 'end_job',
                visible: true,
                orderable: true
            }, {
                data: 'status',
                name: 'status',
                visible: true,
                orderable: true
            }, {
                data: 'action',
                name: 'action',
                searchable: false,
                orderable: false
            }, ],
    
        });
    
        var dtable = $('#TableHistoryCycle').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;                                           
            });
        dtable.draw();
        $("#select_line").change(function (e) { 
            e.preventDefault();
            var value   = $(this).val();
            
            if (value) {
                $("#select_style").empty();
                $("#select_style").append('<option value="">-- Pilih Style --</option>');
                $.ajax({
                    type: 'get',
                    url: 'cycle-time/style-picklist',
                    data:{
                        line_id:value,
                    }
                })
                .done(function(response) {
                    var style = response.style;
                    $('#day').attr('readonly', true);
                    $('#day').val('');
                    $('#day_temp').val('');
                    $("#select_style").empty();
                    $("#select_style").append('<option value="">-- Pilih Style --</option>');
                
                    $.each(style, function(style, style) {
                
                        $("#select_style").append('<option value="' + style + '">' + style + '</option>');
                    });
                })
            } else {
    
                $("#select_style").empty();
                $("#select_style").append('<option value="">-- Pilih Style --</option>');
            }
        });
        $("#select_style").change(function (e) { 
            e.preventDefault();
            var value = $(this).val();  
            var line_id = $('#select_line').val();
            if(value==''){
                return;
            }
            $.ajax({
                type: 'get',
                url: 'cycle-time/get-day-header',
                data:{
                    line_id:line_id,
                    value:value
                },
                complete: function() {
                },
                success: function(response) {
                    if(response){
                        if(response.day!=1){
                            $('#day').removeAttr("readonly");
                        }
                        $('#day').val(response.day);
                        $('#day_temp').val(response.day);
                    }
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                }
            })
        });
        $("#day").change(function (e) {
            let day_temp = $('#day_temp').val();
            let day = $('#day').val();
            if(parseInt(day_temp)>parseInt(day)){
                $("#alert_warning").trigger("click", 'Day Tidak boleh lebih kecil dari sebelum nya');
                $('#day').val(day_temp);
                return false
            }
        });
        $('#form').submit(function(event) {
            event.preventDefault();
            var select_line  = $('#select_line').val();
            var select_style = $('#select_style').val();
            var day          = $('#day').val();
        
            if (!select_line) {
                $("#alert_warning").trigger("click", 'Line Belum di Pilih');
                return false
            }
        
            if (!select_style) {
                $("#alert_warning").trigger("click", 'Style Belum di Pilih');
                return false
            }
            if (!day) {
                $("#alert_warning").trigger("click", 'Day Belum di isi');
                return false
            }
        
            $('#createHeaderCycleModal').modal('hide');
            bootbox.confirm("Are you sure want to save this data ?.", function(result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data: $('#form').serialize(),
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
        
                            window.open(response, '_self');
        
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                            if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                        }
                    });
                }
            });
        });
        getDataLine();
    });
    function getDataLine(){
        let factory_id = $("#factory").val();
        console.log(factory_id)
        $.ajax({
            type: 'get',
            url: 'cycle-time/get-data-line',
            data:{
                factory_id:factory_id,
            }
        })
        .done(function(response) {
            var lines = response.lines;
            $('#day').attr('readonly', true);
            $("#select_line").empty();
            $("#select_line").append('<option value="">-- Pilih Line --</option>');
        
            $.each(lines, function(id, name) {
        
                $("#select_line").append('<option value="' + id + '">' + name + '</option>');
            });
        })
    }
</script>
@endsection