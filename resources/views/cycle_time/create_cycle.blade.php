@extends('layouts.app',['active' => 'cycle-time'])

@section('page-header')
<style>
    .isi{
        font-weight: 300;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .wrap{
        justify-content: center;
    align-items: center;
        min-width: 300px;
        min-height: 200px;
        padding: 5px 15px;
        border-radius: 8px;
        text-align: center;
    }
    #timer{
        position: relative;
        width: 250px;
        height: 250px;
        line-height: 250px;
        border-radius: 50%;
        font-size: 32px;
        margin: 0 auto;
        background-color: #fff;
    }
    #play,#lap,#reset{
        padding: 10px 20px;
        font-size: 20px;
        border-radius: 40px
    }
   
    .time-start{
        font-weight: 400;
        color: gray;
        padding: 30px 0;
    }
    .lap-label{
        position: relative;
        top:-100px;
        font-size: 16px;
        font-weight: 400;
        color: gray;
    }
    .option a{
        display: inline-block;
        margin: 40px 10px;
        text-decoration: none;
    }
    .table-picktime{
        margin-top: 0%
    }
    
    
    #play:active,#reset:active,#lap:active{
        transform: scale(0.95);
    }
    .pause .glyphicon-play,
    .glyphicon-pause{
        display: none;
    }
    .pause .glyphicon-pause{
        display: inline-block;
    }
    #timer svg{
        position: absolute;
        left: 0;
    }
    #animateCircle.addAnimation{
        stroke-dasharray: 770;
        stroke-dashoffset: -770;
        animation: animateCircle 60s linear infinite;
        animation-play-state: paused; /* default animation is pause */
    }
    @keyframes animateCircle {
        from{
            stroke-dashoffset: 0;
        }
    }
    .defaultCircle{
        opacity: .3;
    }
</style>

<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cycle Time</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('cycle_time.index') }}">Data Cycle Time</a></li>
            <li class="active">Input Cycle Time</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        @include('form.text', [
                'field'     => 'line',
                'label'     => 'Line',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $line,
                'attributes' => [
                    'id'        => 'line',
                    'readonly'  => '',
                ]
        ])
        @include('form.text', [
                'field'     => 'style',
                'label'     => 'Style',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $style,
                'attributes' => [
                    'id'        => 'style',
                    'readonly'  => '',
                ]
        ])
        @include('form.text', [
                'field'     => 'condition_day',
                'label'     => 'Condition Day',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $conditionalDay,
                'attributes' => [
                    'id'        => 'condition_day',
                    'readonly'  => '',
                ]
        ])
    </div>
</div>


<div class="panel panel-flat">
    <div class="panel-body">
        <br>
        <br/>
        <br/>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group  col-md-12 col-lg-12 col-sm-12" id="">
                <label class="control-label text-semibold col-md-2 col-lg-2 col-sm-12">NIK</label>
                <div class="input-group  col-md-10 col-lg-10 col-sm-12">
                    <input type="text" class="form-control" id="nik_label" name="nik_label" placeholder="Ketik Nama / NIK Sewer">
                    @if ($isCopied==1)
                        <span class="input-group-btn">
                            <button class="btn btn-default legitRipple" type="button" id="historyButtonLookup">
                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                            </button>
                        </span>
                    @endif
                </div>
                
            </div>
        </div><br>
        <div class="col-md-12 col-lg-12 col-sm-12">
            @include('form.text', [
                'field'     => 'name_label',
                'label'     => 'Nama',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => '',
                'attributes' => [
                    'id'        => 'name_label',
                    'readonly'  => '',
                ]
            ])
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group  col-md-12 col-lg-12 col-sm-12">
                <label for="style" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                Process</label>
                <div class="input-group  col-md-10 col-lg-10 col-sm-12">
                    <select multiple data-placeholder="" class="form-control select-search" name="processMerge[]" readonly id="select_process_merge">
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-default legitRipple" type="button" id="processButtonLookup">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group  col-md-12 col-lg-12 col-sm-12">
                <label for="style" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                Mesin</label>
                <div class="input-group  col-md-10 col-lg-10 col-sm-12">
                    <select multiple data-placeholder="" class="form-control select-search" name="mesinMerge[]" readonly id="select_mesin_merge">
                    </select>
                </div>
            </div>
        </div>
        {!!
            Form::open([
                'role' 		=> 'form',
                'url' 		=> route('cycle_time.store_detail_cycle','store'),
                'method' 	=> 'store',
                'class' 	=> 'form-horizontal',
                'id'		=>	'form'
            ])
        !!}
        <div class="col-md-12 col-lg-12 col-sm-12">
            @include('form.text', [
                'field'     => 'wip',
                'label'     => 'WIP',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => '',
                'attributes' => [
                    'id'        => 'wip',
                ]
            ])
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
            @include('form.text', [
                'field'     => 'stiches_burst',
                'label'     => 'Stitches Burst',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => '',
                'attributes' => [
                    'id'        => 'stiches_burst',
                ]
            ])
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group  col-lg-12">
                <label
                for="is_assembly" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                    Type WIP
                </label>
                <div class="checkbox checkbox-switchery switchery-lg switchery-double  col-md-10 col-lg-10 col-sm-12">
                    <label>
                        Pre Assembly
                        <input class="switchery" name="is_assembly" type="checkbox" value="0">
                        Assembly
                    </label>
                </div>
            </div>
        </div>
        <br>
        
        {!! Form::hidden('pick_time_data','[]' , array('id' => 'pick_time_data')) !!}
        {!! Form::hidden('process_pick_list','[]' , array('id' => 'process_pick_list')) !!}
        {!! Form::hidden('cycle_time_batch_id',$id, array('id' => 'cycle_time_batch_id')) !!}
        {!! Form::hidden('cycle_time_id','0', array('id' => 'cycle_time_id')) !!}
        {!! Form::hidden('cycle_time_id_before','0', array('id' => 'cycle_time_id_before')) !!}
        {!! Form::hidden('sewer_nik','', array('id' => 'sewer_nik')) !!}
        {!! Form::hidden('sewer_name','', array('id' => 'sewer_name')) !!}
        
        
        <button type="submit" class="hidden" id="btn-save">save</button>
        {!! Form::close() !!}
        <button type="button"  data-toggle="modal" id="pick-time" class="btn bg-teal-400 col-xs-12 legitRipple">Pitch Time</button>
    </div>
</div>
<ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
    <li>
        <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
            <i class="fab-icon-open icon-grid3"></i>
            <i class="fab-icon-close icon-cross2"></i>
        </a>

        <ul class="fab-menu-inner">
            <li>
                <div data-fab-label="Laporan Data Entry" class="data_entry">
                <a data-toggle="modal" href="#" onclick="return lapDataEntry()" class="btn btn-default btn-rounded btn-icon btn-float">
                    <i class="icon-cabinet"></i>
                </a>
                </div>
            </li>
            <li>
                <div data-fab-label="List Draft" class="listDraft">
                <a href="javascript:void(0)" onclick="return listDraft()"  class="btn btn-default btn-rounded btn-icon btn-float">
                        <i class="icon-file-text3 position-right"></i>
                    </a>
                </div>
            </li>
            <li>
                <div data-fab-label="Send Message" class="send_message">
                <a href="javascript:void(0)" onclick="return sendMessage()"  class="btn btn-default btn-rounded btn-icon btn-float">
                        <i class="icon-mail-read"></i>
                    </a>
                </div>
            </li>
            <li>
                <div data-fab-label="Lock Cycle" class="data_entry">
                <a onclick="document.getElementById('cycle_lock').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                    <i class="icon-lock2"></i>
                    <form id="cycle_lock" action="{{ route('cycle_time.lock_cycle') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        {!! Form::hidden('cycle_batch_id',$id, array('id' => 'cycle_batch_id')) !!}
                    </form>
                </a>
                </div>
            </li>
        </ul>
    </li>
</ul>
{!! Form::hidden('workflowId',$workflowId, array('id' => 'workflowId')) !!}
{!! Form::hidden('is_copied',0, array('id' => 'is_copied')) !!}
{!! Form::hidden('style',$style, array('id' => 'style')) !!}
{!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
{!! Form::hidden('url_draft_detail_cycle',route('cycle_time.store_detail_cycle','draft'), array('id' => 'url_draft_detail_cycle')) !!}
{!! Form::hidden('url_save_detail_cycle',route('cycle_time.store_detail_cycle','store'), array('id' => 'url_save_detail_cycle')) !!}
{!! Form::hidden('url_store_draft_cycle',route('cycle_time.store_draft_cycle'), array('id' => 'url_store_draft_cycle')) !!}
{!! Form::hidden('url_store_cycle_history',route('cycle_time.store_cycle_history'), array('id' => 'url_store_cycle_history')) !!}
{!! Form::hidden('url_get_component',route('cycle_time.get_component_process'), array('id' => 'url_get_component')) !!}
@endsection

@section('page-modal')
    @include('form.modal_picklist_c', [
        'name'          => 'process',
        'title'         => 'Daftar Proses',
        'placeholder'   => 'Cari berdasarkan nama proses/component',
    ])
    @include('form.modal_picklist_c', [
        'name'          => 'component',
        'title'         => 'Daftar Komponen',
        'placeholder'   => 'Cari berdasarkan nama Komponen',
    ])
    @include('form.modal_picklist_c', [
        'name'          => 'historyProcess',
        'title'         => 'Daftar Proses',
        'placeholder'   => 'Cari berdasarkan nama proses/component',
    ])
    @include('form.modal_picklist', [
		'name'          => 'style',
		'title'         => 'Daftar List Style',
		'placeholder'   => 'Cari berdasarkan Nama Style',
    ])
    @include('form.modal_picklist', [
		'name'          => 'dataEntry',
		'title'         => 'Laporan Data Entry',
		'placeholder'   => 'Cari berdasarkan Nama Sewer/NIK',
    ])
    @include('form.modal', [
        'name' => 'send_message',
        'title' => 'Send Message'
    ])
    @include('form.modal', [
        'name' => 'listDraft',
        'title' => 'List Draft'
    ])
    @include('form.modal_picklist', [
		'name'          => 'listDraft',
		'title'         => 'List Draft',
		'placeholder'   => 'Cari berdasarkan Nama Sewer / NIK',
    ])
    @include('cycle_time._lov_pick_time')
    @include('cycle_time._item_pick_time')
@endsection

@section('page-js')
<script src="{{ mix('js/floating_button.js') }}"></script>
<script src="{{ mix('js/switch.js') }}"></script>
<script src="{{ mix('js/create_cycle.js') }}"></script>
@endsection 
