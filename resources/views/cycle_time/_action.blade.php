
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if ($model->end_job==null)
                <li><a href="{{$edit}}" ><i class="icon-pencil6"></i> Lanjut</a></li>  
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Hapus</a></li>
            @else
                <li><a href="{{$report}}" ><i class="icon-file-excel"></i> Report Time Study</a></li>
            @endif
        </ul>
    </li>
</ul>
