<div id="pickTimeModal" data-backdrop="static" data-keyboard="false" class="modal fade" style="color:black;overflow: auto !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Pick Time</h5>
				</div>
				<div class="modal-body">
					<div class="row isi">
						<div class="wrap">
                            <div id="timer">
                                <svg width="260" height="260">
                                    <circle class="defaultCircle" cx="125" cy="125" r="120" stroke="#4661AF"
                                    stroke-width="10" fill="none" stroke-linecap="round" />
                                    <circle id="animateCircle" cx="125" cy="125" r="120" stroke="#566db0"
                                    stroke-width="10" fill="none" stroke-linecap="round" />
                                </svg>
                                {{--  <span id="minute">00</span> :
                                <span id="second">00</span> :
                                <span id="milisecond">00</span>  --}}
                                <span class="time" id="time">00:00:00</span>
                                
                            </div>
                            <div class="lap-label">
                                <span class="time" id="time_lap">00:00:00</span>
                            </div>
                            
                            <div class="option">
                                <a id="play" onclick="playFunc()" href="javascript:void(0)" class="btn btn-primary btn-icon btn-rounded legitRipple"><i class="glyphicon glyphicon-play"></i><i class="glyphicon glyphicon-pause"></i></a>
                                <a id="lap" onclick="LapFunc()" href="javascript:void(0)" class="btn btn-primary btn-icon btn-rounded legitRipple hidden"><i class="glyphicon glyphicon-flag"></i></a>
                                <a id="reset" onclick="resetFunc()" class="btn btn-primary btn-icon btn-rounded legitRipple hidden" href="javascript:void(0)"><i class="glyphicon glyphicon-refresh"></i></a>
                            </div>
                        </div>
					</div>
                    <div class="table-responsive pre-scrollable table-picktime">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Interval</th>
                                    <th>Total</th>
                                    <th>Time Recorded</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="pick_time_tbody">
                            </tbody>
                        </table>
                    </div>
				</div>
				<input type="hidden" id="flag">
				<div class="modal-footer">
					<button type="button" id="btn-close" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="button" id="btn-draft" class="btn btn-warning legitRipple">Draft <i class="icon-file-text3 position-right"></i></button>
					<button type="button" id="btn-save-pick" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
		</div>
	</div>
</div>
</div>