<div id="createHeaderCycleModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('cycle_time.store_header'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Buat Header Cycle</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@include('form.select', [
								'field'     => 'line',
								'label'     => 'Line',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
								'options' => [
									'' => '-- Pilih Line --',
								],
								'class' => 'select-search',
								'attributes' => [
									'id'       => 'select_line',
									'required' =>''
								]
							])
							@include('form.select', [
								'field'     => 'style',
								'label'     => 'Style',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
								'options' => [
									'' => '-- Pilih Style --',
								],
								'class' => 'select-search',
								'attributes' => [
									'id'       => 'select_style',
									'required' =>''
								]
							])
							@include('form.text', [
								'field' => 'day',
								'label' => 'Day',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'day',
									'readonly' => 'readonly'
								]
							])
                            
                            
						</div>
						</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
			{!! Form::hidden('day_temp','0', array('id' => 'day_temp')) !!}
		</div>
	</div>
</div>
</div>
