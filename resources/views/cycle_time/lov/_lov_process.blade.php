<table class="table table-responsive">
    <thead>
    <tr>
        <th style="width:50px">Aksi</th>
        <th>Nama Proses</th>
        <th>Component</th>
        <th>Nama Mesin</th>
    </tr>
    </thead>
    
    <tbody>
        @foreach ($listProcess as $key => $list)
            <tr id="{{ $list->workflow_detail_id }}" class="{{$list->last_process?"bg-danger":($list->count_inline>0?"bg-success":'')}}">
                
                <td>
                    @if ($list->machine_name==null)
                        <span>Mesin Kosong, Info IE Development</span>
                    @else
                        <button class="btn btn-info btn-xs btn-choose" 
                        type="button" 
                        id="{{ $list->workflow_detail_id }}_Select" 
                        data-workflow_detail_id="{{ $list->workflow_detail_id }}"
                        data-machine_id="{{ $list->machine_id }}"
                        data-machine_name="{{ $list->machine_name }}"
                        data-proses_name="{{ $list->proses_name }}"
                        data-last="{{ $list->last_process?1:0 }}" 
                        >Pilih</button>

                        <button class="btn btn-info btn-xs btn-unchoose hidden" 
                        type="button" 
                        id="{{ $list->workflow_detail_id }}_UnSelect"
                        data-workflow_detail_id="{{ $list->workflow_detail_id }}"
                        data-machine_id="{{ $list->machine_id }}"
                        data-machine_name="{{ $list->machine_name }}"
                        data-proses_name="{{ $list->proses_name }}"
                        data-last="{{ $list->last_process?1:0 }}" 
                        >UnSelect</button>
                    @endif
                    
                </td>
                <td>
                    {{ strtoupper($list->proses_name) }}
                </td>
                <td>
                    {{ strtoupper($list->component) }}
                </td>
                <td>
                    {{ strtoupper($list->machine_name) }}
                </td>
                
            </tr>
        @endforeach
    </tbody>
</table>
<div class="modal-footer">
	<button type="button" onclick="return backComponent()" class="btn btn-primary legitRipple">Back <i class="icon-arrow-left7 position-right"></i></button>
</div>
{!! $listProcess->appends(Request::except('page'))->render() !!}