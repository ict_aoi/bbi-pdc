<div class="panel panel-flat">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel bg-teal-400">
                    <div class="panel-body">
                        <h2 class="no-margin total_complain"><span id="active">{{ $totComplain }}</span></h2>
                        Complain Blm di Response
                    </div>
        
                    <div class="container-fluid">
                        <div id="members-online"></div>
                    </div>
                </div>
        
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel bg-danger-400">
                    <a href="javascript:void(0)" onclick="showComplainWorkflow()">
                        <div class="panel-body">
                            <h2 class="no-margin complain_not_read"><span id="outstanding">{{ $countComplainClose }}</span></h2>
                            Pesan Blm di Baca
                        </div>
            
                        <div class="container-fluid">
                            <div id="members-online"></div>
                        </div>
                    </a>
                </div>
        
            </div>
        </div>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form-send',
                'url' => route('cycle_time.store_message'),
                'method' => 'post',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'=> 'form-send'
            ])
        !!}
        
        @include('form.text', [
            'field' => 'style',
            'label' => 'Nama',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'default'=>$style,
            'attributes' => [
                'id' => 'style',
                'readonly' => 'readonly'
            ]
        ])
        @include('form.textarea', [
            'field' => 'message',
            'label' => 'Isi Pesan',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'message',
                'rows' => 5,
                'style' => 'resize: none;'
            ]
        ])
        {!! Form::hidden('line_id',$line_id, array('id' => 'line_id')) !!}
        {!! Form::hidden('nik',$nik, array('id' => 'nik')) !!}
        {!! Form::hidden('name',$name, array('id' => 'name')) !!}
        <div class="text-right">
            <button type="submit" class="btn btn-primary col-xs-12 legitRipple">Kirim Pesan <i class="icon-mail-read"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('.select-search').select2();
    $('#smv').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $("#name_process").change(function (e) { 
        e.preventDefault();
        $('#component').val("");
    });
    
    $('#form-send').submit(function (event){
        event.preventDefault();
        var style = $('#style').val();
        var message = $('#message').val();

        if(!style)
        {
            $("#alert_warning").trigger("click", 'Style Wajib di pilih');
            return false;
        }

        if(!message)
        {
            $("#alert_warning").trigger("click", 'Message Wajib diisi');
            return false
        }
        bootbox.confirm("Apakah anda yakin kirim pesan ini ?.", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: $('#form-send').attr('action'),
					data:new FormData($("#form-send")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Message berhasil dikirim.')
                        $('#send_messageModal').modal('toggle');
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });
</script>