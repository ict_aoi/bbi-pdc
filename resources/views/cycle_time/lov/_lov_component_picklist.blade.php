<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Action</th>
		<th>Nama Komponen</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
                <td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-name="{{ $list->component }}" 
					>Select</button>
				</td>
				<td>
					{{ strtoupper($list->component) }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
