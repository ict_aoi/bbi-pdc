<table class="table table-responsive">
    <thead>
    <tr>
        <th>Nama Sewer</th>
        <th>Process Name</th>
        <th>Machine Name</th>
        <th>SMV</th>
        <th>Cycle Time Study</th>
        <th>Target 90%</th>
        <th>Total</th>
        <th>Max</th>
        <th>Min</th>
        <th>Average</th>
        <th>Action</th>
    </tr>
    </thead>
    
    <tbody>
        @foreach ($listProcess as $key => $list)
            <tr class="{{$list->count_now>0?"bg-success":''}}">
                <td>
                    {{ strtoupper($list->sewer_name).'-'.$list->sewer_nik }}
                </td>
                <td>
                    {{ strtoupper($list->process_name) }}
                </td>
                <td>
                    {{ strtoupper($list->machine_name) }}
                </td>
                <td>
                    {{ round($list->smv,2) }}
                </td>
                <td>
                    @php
                        $cycles = explode(',', $list->duration);
                        foreach ($cycles as $c){
                            echo '<span class="label border-left-primary label-striped">'.$c.'</span>';
                        }
                    @endphp
                </td>
                <td>
                    {{ $list->total_target }}
                </td>
                <td>
                    {{ $list->total }}
                </td>
                <td>
                    {{ $list->max }}
                </td>
                <td>
                    {{ $list->min }}
                </td>
                <td>
                    {{ round($list->average,2) }}
                </td>
                <td width="50px">
                    <button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button" 
                        data-sewer_name="{{ $list->sewer_name }}"
                        data-sewer_nik="{{ $list->sewer_nik }}"
                        data-process="{{ $list->process_name }}"
                        data-machine_name="{{ $list->machine_name }}"
                        data-workflow_id="{{ $list->workflow_detail_id }}"
                        data-cylce_id="{{ $list->id }}"
                        data-workflow="{{ $list->workflow }}"
                    >Pilih</button>
                </td>
                
            </tr>
        @endforeach
    </tbody>
</table>

{!! $listProcess->appends(Request::except('page'))->render() !!}