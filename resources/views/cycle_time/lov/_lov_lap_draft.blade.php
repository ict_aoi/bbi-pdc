<table class="table table-responsive">
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Nama Sewer</th>
        <th>Process Name</th>
        <th>Machine Name</th>
        <th>SMV</th>
        <th>Cycle Time Study</th>
        <th>Target 90%</th>
        <th>Total</th>
        <th>Max</th>
        <th>Min</th>
        <th>Average</th>
        <th>Action</th>
    </tr>
    </thead>
    
    <tbody>
        @foreach ($listProcess as $key => $list)
            <tr>
                <td>
                    {{ $list->date }}
                </td>
                <td>
                    {{ strtoupper($list->sewer_name).'-'.$list->sewer_nik }}
                </td>
                <td>
                    {{ strtoupper($list->process_name) }}
                </td>
                <td>
                    {{ strtoupper($list->machine_name) }}
                </td>
                <td>
                    {{ $list->smv }}
                </td>
                <td>
                    @php
                        $cycles = explode(',', $list->duration);
                        foreach ($cycles as $c){
                            echo '<span class="label border-left-primary label-striped">'.$c.'</span>';
                        }
                    @endphp
                </td>
                <td>
                    {{ $list->total_target }}
                </td>
                <td>
                    {{ $list->total }}
                </td>
                <td>
                    {{ $list->max }}
                </td>
                <td>
                    {{ $list->min }}
                </td>
                <td>
                    {{ $list->average }}
                </td>
                <td>
                    <button type="button" data-dismiss="modal" data-id="{{ $list->id }}" class="btn btn-primary btn-icon-anim btn-circle btn-choose" data-id="{{ $list->id }}"
                        data-sewer="{{ $list->sewer_name }}" data-nik="{{ $list->sewer_nik }}" data-batch_id="{{ $list->cycle_time_batch_id }}"><i class="icon-select2"></i></button>
                    <button type="button" class="btn btn-danger btn-icon-anim btn-circle" onclick="return deleteDraft('{{ $list->id }}')"><i class="icon-trash"></i></button>
                    {{--  <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#" data-dismiss="modal" class="btn btn-xs btn-choose" type="button" data-id="{{ $list->id }}"
                                    data-sewer="{{ $list->sewer_name }}" data-nik="{{ $list->sewer_nik }}" data-batch_id="{{ $list->cycle_time_batch_id }}"><i class="icon-checkmark4"></i>Pilih</a></li>  
                                <li><a href="#" onclick="return deleteDraft('{{ $list->id }}')"><i class="icon-close2"></i> Hapus</a></li>
                            </ul>
                        </li>
                    </ul>  --}}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{!! $listProcess->appends(Request::except('page'))->render() !!}