<table class="table table-responsive">
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Nama Sewer</th>
        <th>Process Name</th>
        <th>Machine Name</th>
        <th>SMV</th>
        <th>Cycle Time Study</th>
        <th>Target 90%</th>
        <th>Total</th>
        <th>Max</th>
        <th>Min</th>
        <th>Average</th>
        <th>Action</th>
    </tr>
    </thead>
    
    <tbody>
        @foreach ($listProcess as $key => $list)
            <tr class="{{$list->total_update>0?"bg-success":''}}">
                <td>
                    {{ $list->date }}
                </td>
                <td>
                    {{ strtoupper($list->sewer_name).'-'.$list->sewer_nik }}
                </td>
                <td>
                    {{ strtoupper($list->process_name) }}
                </td>
                <td>
                    {{ strtoupper($list->machine_name) }}
                </td>
                <td>
                    {{ $list->smv }}
                </td>
                <td>
                    @php
                        $cycles = explode(',', $list->duration);
                        foreach ($cycles as $c){
                            echo '<span class="label border-left-primary label-striped">'.$c.'</span>';
                        }
                    @endphp
                </td>
                <td>
                    {{ $list->total_target }}
                </td>
                <td>
                    {{ $list->total }}
                </td>
                <td>
                    {{ $list->max }}
                </td>
                <td>
                    {{ $list->min }}
                </td>
                <td>
                    {{ round($list->average,2) }}
                </td>
                <td width="50px">
                    <button type="button" onclick="return deleteCyle('{{ $list->id }}')" class="btn btn-danger btn-delete-item"><i class="icon-trash"></i></button>
                </td>
                
            </tr>
        @endforeach
    </tbody>
</table>

{!! $listProcess->appends(Request::except('page'))->render() !!}