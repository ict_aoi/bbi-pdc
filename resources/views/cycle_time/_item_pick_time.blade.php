<script type="x-tmpl-mustache" id="pick_time_table">
	{% #list %}
		<tr id="pick_time_{% _id %}">
			<td width="50px" style="text-align:center;">{% no %}</td>
			<td>{% interval %}</td>
			<td>{% total %}</td>
			<td>{% time_recorded %}</td>
			<td>
				<button type="button" id="deleteDefect_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-circle btn-delete-defect"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
</script>