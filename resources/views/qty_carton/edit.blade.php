@extends('layouts.app',['active' => 'qty_karton'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ubah Qty Karton</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('qty_carton.index') }}">Data Line</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <center>
                        <div class="col-md-12"> <h1 class="no-margin text-semibold">STYLE <span id="style_label">{{ $style }}</span></h1></div>
                    </center>
                </div>
                <div class="heading-elements">
                    <a href="javascript:void(0)" onclick="return inputAll()" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Qty All</a>
                </div>
                <br/>
                <div class="row">
                    
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Size</th>
                                    <th>QTY/POLIBAG</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="breadown_size_data_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
        
                <br/>
                {!!
                    Form::open([
                        'role'      => 'form-store-carton',
                        'url'       => route('qty_carton.store_qty_carton'),
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form-store-carton'
                    ])
                !!}
                    {!! Form::hidden('style',$style, array('id' => 'style')) !!}
                    {!! Form::hidden('breakdown_size_data',json_encode($detail) , array('id' => 'breakdown_size_data')) !!}
        
                    <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-modal')
    @include('qty_carton._breakdown_size_data')
@endsection
@section('page-js')
    <script src="{{ mix('js/create_qty_carton.js') }}"></script>
@endsection