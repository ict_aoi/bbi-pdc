@extends('layouts.app',['active' => 'qty_karton'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-box position-left"></i> <span class="text-semibold">Qty Carton</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Qty Carton</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('qty_carton.create') }}" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Buat Baru</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="qtyCartonTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Style</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('page-js')
{{--  <script src="{{ mix('js/master_line.js') }}"></script>  --}}
<script>
    $(document).ready(function() {
        var qtyCartonTable = $('#qtyCartonTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 10,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/qty-karton/data',
            },
            fnCreatedRow: function(row, data, index) {
                var info = qtyCartonTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'style',
                name: 'style',
                searchable: true,
                visible: true,
                orderable: true
            }, {
                data: 'action',
                name: 'action',
                searchable: true,
                orderable: true
            } ],
    
        });
    
        var dtable = $('#qtyCartonTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();
    });
</script>
@endsection