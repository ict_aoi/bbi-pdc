@extends('layouts.app',['active' => 'adjustment-outputs-qc-subtraction'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Pengurangan Output Qc</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Data</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <a href="{{ route('adjustment_output_qc_subtraction.create') }}" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Buat Baru</a>
        </div>
    </div>
    
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="RevisiTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Code</th>
                        <th>Nama Line</th>
                        <th>Style</th>
                        <th>Po Buyer</th>
                        <th>Article</th>
                        <th>Size</th>
                        <th>Qty Revisi</th>
                        <th>Nama Qc</th>
                        <th>Alasan</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-modal')
    @include('form.modal', [
        'name' => 'edit_proses',
        'title' => 'Edit Proses'
    ])
@endsection
@section('page-js')
<script>
    var RevisiTable = $('#RevisiTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/adjustment/pengurangan-output-qc/data',
        },
        fnCreatedRow: function(row, data, index) {
            var info = RevisiTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'tanggal',
            name: 'tanggal',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'code',
            name: 'code',
            searchable: true,
            visible: false,
            orderable: true
        }, {
            data: 'line_name',
            name: 'line_name',
            searchable: true,
            visible: true
        }, {
            data: 'style',
            name: 'style',
            searchable: true,
            visible: true
        }, {
            data: 'poreference',
            name: 'poreference',
            searchable: true,
            visible: true
        }, {
            data: 'article',
            name: 'article',
            searchable: true,
            visible: true
        }, {
            data: 'size',
            name: 'size',
            searchable: true,
            visible: true
        }, {
            data: 'qty_revision',
            name: 'qty_revision',
            searchable: true,
            visible: true
        }, {
            data: 'qc_name',
            name: 'qc_name',
            searchable: true,
            visible: true
        }, {
            data: 'reason',
            name: 'reason',
            searchable: false,
            orderable: false
        }, ],
    
    });
    
    var dtable = $('#RevisiTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();
</script>
@endsection