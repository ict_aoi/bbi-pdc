@extends('layouts.app',['active' => 'adjustment-outputs-qc-subtraction'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Pengurangan Output Qc</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('adjustment_output_qc_subtraction.index') }}" >Data</a></li>
            <li class="active">Hapus Data Qc</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @include('form.date', [
                    'field' => 'date',
                    'label' => 'Tanggal',
                    'label_col'   => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'    => 'col-md-10 col-lg-10 col-sm-12',
                    'placeholder' => 'dd/mm/yyyy',
                    'attributes' => [
                        'id'        => 'date',
                    ]
                ])
            </div>
            <div class="col-lg-12">
                @include('form.picklist', [
                    'label' 		=> 'Line',
                    'field' 		=> 'line',
                    'name' 			=> 'line',
                    'placeholder' 	=> 'Silahkan Pilih Line',
                    'title' 		=> 'Silahkan Pilih Line',
                    'readonly'		=> true,
                ])
            </div>
            <button type="button" id="btn_cari" class="btn bg-teal-400 col-xs-12">Cari <i class="icon-search4 position-left"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="listOutputQcTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Line</th>
                        <th>Style</th>
                        @if (\Auth::user()->buyer=='other')
                            <th>Po Buyer</th>
                        @else
                            <th>No. Lot</th>
                        @endif
                        <th>Article</th>
                        <th>Size</th>
                        <th>Qty Order</th>
                        <th>Total Cek</th>
                        <th>Total Cek Hari</th>
                        <th>Balance</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar List Line',
		'placeholder'   => 'Cari berdasarkan Nama Line',
    ])
    @include('form.modal', [
		'name'          => 'editQty',
		'title'         => 'Edit Output Qc Endline',
		'placeholder'   => 'Cari berdasarkan Nama Line',
    ])
@endsection
@section('page-js')
<script src="{{ mix('js/adjustment_output.js') }}"></script>
@endsection