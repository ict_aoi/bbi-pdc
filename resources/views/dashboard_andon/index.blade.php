@extends('layouts.front')

@section('page-content')
<style type="text/css">
	#container {
		display: flex;                  /* establish flex container */
		flex-direction: row;            /* default value; can be omitted */
		flex-wrap: nowrap;              /* default value; can be omitted */
		justify-content: center; /* switched from default (flex-start, see below) */
		background-color: transparent;
	  }
	  #container > .con {
		width: 50px;
		height: 50px;
		margin-top: 40px;
		margin-right: 30px;
		margin-left: 50px;
	  }
	  #container > .text {
		  text-align: left;
		  margin-block:auto;
		  float:left;
		  width:150px;
	  }
	  
	.zoom {
		zoom: 0.65;
	}
	.modern-form__hr {
		border-color: #4daade;
		border-width: 2px;
		width: 121px;
	}
	.intro{
		max-width: 500px;
		margin: 0 auto;
	}
	marquee {
		margin-top: 5px;
		width: 100%;
	}
	.table{
		font-size:14px;

	}
	.active {
		height: 100%;
		width: 100%;
		border: 2px solid white;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.panel-body {
		padding: 10px 10px;
		padding-top: 0px;
		padding-bottom: 5px;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		padding: 2px 0px;
	}
	.runtext-container {
	height: 60px;
	overflow-x: hidden;
	overflow-y: visible;
	padding:0 3px 0 3px;
	}

	.main-runtext {margin: 0 auto;
	overflow: visible;
	position: relative;
	height: 35px;
	}

	.runtext-container .holder {
	position: relative;
	overflow: visible;
	display:inline;
	float:left;

	}

	.runtext-container .holder .text-container {
		display:inline;
	}

	.runtext-container .holder a{
		text-decoration: none;
		font-weight: bold;
		color:#0072CA;
		text-shadow:0 -1px 0 rgba(0,0,0,0.25);
		line-height: -0.5em;
		font-size:15px;
		height: 30px;
		line-height: 30px;
		text-align: center;
	}

	span {
		display: inline-block;
		vertical-align: middle;
		line-height: normal;
	}

	.runtext-container .holder a:hover{
		text-decoration: none;
		color:#6600ff;
	}
	
</style>
<div class="row">
	<br>
		<div class="row">
			<div class="col-lg-2">
				<img align="center" border="0" src="{{ asset('images/LOGO BINA BUSANA 2019 Large.png') }}" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 100%;" width="500px" />
			</div>
			<div class="col-lg-8">
				<div class="intro">
					<h1 class="text-center" style="padding: 0px 0px 0px;margin: 0;">ANDON SUPPLY CENTER</h1>
					<h1 class="text-center" style="padding: 0px 0px 0px;margin: 0;">{{ $buyer=='nagai'?'NAGAI':'NON NAGAI' }}</h1>
					<h1 class="text-center" style="padding: 0px 0px 0px;margin: 0;">{{Carbon\Carbon::now()->format('d-M-Y')}}</h1>
					{{--  <hr class="modern-form__hr" style="height: -10px;">  --}}
				</div>
			</div>
			<div class="col-lg-2">
				<h1 class="no-margin text-semibold" style="align:center"><b><label id="time"></label></b></h1>
			</div>
		</div>
		<br>
		<div class="container-fluid">
			<div class="row">
				<div class="isi"></div>
			</div>
		</div>
		<div class="container">
			<div id="container">
				<div class="con panel bg-success-700 has-bg-image"></div>
				<div class="text"><h2>Cukup Supply</h2></div>
				<div class="con panel bg-orange-700 has-bg-image"></div>
				<div class="text"><h2>Kurang Supply(<15menit)</h2></div>
				<div class="con panel bg-danger-700 has-bg-image"></div>
				<div class="text"><h2>Kurang Supply(>15menit)</h2></div>
			</div>
		</div>
</div>
{!! Form::hidden('flag_refresh',0, array('id' => 'flag_refresh')) !!}
{!! Form::hidden('buyer',$buyer, array('id' => 'buyer')) !!}
{!! Form::hidden('factory',$factory, array('id' => 'factory')) !!}
{!! Form::hidden('items','[]' , array('id' => 'items')) !!}
{!! Form::hidden('server',$ip_websocket, array('id' => 'server')) !!}
{!! Form::hidden('port',$port_websocket, array('id' => 'port')) !!}
{!! Form::hidden('topic',$topicTarget, array('id' => 'topic')) !!}
{!! Form::hidden('url_check_andon',route('endLine.check_andon'), array('id' => 'url_check_andon')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/mqttws31.js') }}"></script>
<script src="{{ mix('js/responsivevoice.js') }}"></script>
<script>
	let items= JSON.parse($('#items').val());
	connected_flag=0;
	let mqtt;
    let reconnectTimeout = 2000;
	
	$(function () {
		setInterval(function(){ 
			var weekday             = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			var today               = new Date();
			var d                   = weekday[today.getDay()];
			var h                   = today.getHours();
			var m                   = today.getMinutes();
			var s                   = today.getSeconds();
			if (s=='00') {
				renderCard();
				/*responsiveVoice.speak($('#text').val(),"Indonesian Female");
				//responsiveVoice.clickEvent();
				responsiveVoice.enableEstimationTimeout = true;
				var forgot_password_btn = document.querySelector("#pausebutton");
				
				forgot_password_btn.addEventListener("click", pausebutton);*/
			}
		},1000);
		
		/*setInterval(function(){ 
			$('#playbutton').trigger('click');
		},10000);*/
		/*playbutton.onclick = function() {
			
			responsiveVoice.speak($('#text').val(),"Indonesian Female");
			responsiveVoice.enableEstimationTimeout = false;

		};
		stopbutton.onclick = function() {

			responsiveVoice.cancel();
	
		};	*/
		function startTime() {
			today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			h = checkTime(h);
			$('#time').text(h + ":" + m + ":" + s);
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
			if (i < 10) {
				i = "0" + i
			}; // add zero in front of numbers < 10
			return i;
		}
		getSummaryDataEfficiency();
		MQTTconnect();
		render();
		startTime();
	});
	function render() {
		getIndex();
		$('#items').val(JSON.stringify(items));
		bind();
	}
	
	function bind() {
		
	}
	
	function getIndex() {
		for (idx in items) {
			items[idx]['_id'] = idx;
			items[idx]['no'] = parseInt(idx) + 1;
		
		}
	}
	function formatDate(date, format) {
		const map = {
			mm: date.getMonth() + 1,
			dd: date.getDate(),
			yy: date.getFullYear().toString().slice(-2),
			yyyy: date.getFullYear()
		}
	
		return format.replace(/mm|dd|yy|yyy/gi, matched => map[matched])
	}
	function getSummaryDataEfficiency()
	{
		let buyer = $("#buyer").val();
		let factory = $("#factory").val();
		$.ajax({
			type: 'get',
			url: '/andon/supply-data',
			data:{
				'buyer'  : buyer,
				'factory': factory,
			}
		})
		.done(function(response)
		{
			items=[];
			for (i in response.data) 
			{
				var data  = response.data[i];
				var input = {
					'andon_active'   : data.andon_active,
					'close_time'     : data.close_time,
					'balance'        : data.balance,
					'commitment_line': data.commitment_line,
					'factory_id'     : data.factory_id,
					'line_id'        : data.line_id,
					'name'           : data.name,
					'style'          : data.style,
					'count_daily'    : data.count_daily,
					'safety_stock'   : data.safety_stock,
					'alias'          : data.alias,
					//'history'        : data.history,
					//'count_history'  : data.count_history,
				}
				
				items.push(input);
			}
			render();
			setTimeout(function () {
				renderCard();
			}, 1000);
		});  
	}
	function renderCard(){
		$(".isi").empty();
		for (i in items) 
		{
			let data         = items[i];
			let dataStyle    = $.unique(JSON.parse(JSON.stringify(data.style)));
			let wip_style    = "";
			let countDaily   = "";
			let active_andon = '';
			let interval     = 0;
			let epoch        = '';
			
			let active_temp  = parseInt(data.safety_stock)>parseInt(data.balance);
			
			if(data.andon_active!=null && data.close_time==null){
				
				let date1            = new Date(data.andon_active);
				let date2            = new Date();
				let timeAgoInSeconds = Math.floor((date2 - date1) / 1000);
				let duration         = getDuration(timeAgoInSeconds);

				interval = duration.interval;
				epoch    = duration.epoch;
				
				active_andon= "<div class='active' id='bdiv_"+data.line_id+"' style='height:100%;width:100%;border:2px solid white;text-align: center'>Active : "+interval+" "+epoch+" </div>";
			}else if(active_temp&& data.andon_active==null){
				let now = new Date();
				var dateString = moment(now). format('YYYY-MM-DD HH:m:s');
				data.andon_active = dateString;
				checkAndon(data.line_id);
				active_andon= "<div class='active' id='bdiv_"+data.line_id+"' style='height:100%;width:100%;border:2px solid white;text-align: center'>Active : "+dateString+" Time: 0 Seconds </div>";
				render();
				let date1            = new Date(data.andon_active);
				let date2            = new Date();
				let timeAgoInSeconds = Math.floor((date2 - date1) / 1000);
				let duration         = getDuration(timeAgoInSeconds);

				interval = duration.interval;	
				epoch    = duration.epoch;
			}else{
				active_andon= "<div class='active' id='bdiv_"+data.line_id+"' style='height:100%;width:100%;border:2px solid white;margin-top:10px;margin-bottom:10px;text-align: center'>Active : - </div>";

			}

			let bgcolor       = (epoch=='second')?'bg-orange-700':(interval>15&&epoch=='Minute')?'bg-danger-700':(interval>0&&epoch!='Minute')?'bg-danger-700':(parseInt(data.safety_stock)>parseInt(data.balance))?'bg-orange-700':'bg-success-700';
			let table_wip     = "<table class='table' id='wipTable_"+data.line_id+"'><thead><tr><th>Style</th><th>WIP</th><th>TARGET</th></tr></thead><tbody></tbody></table>";
			setTimeout(function () {
				if(dataStyle.length>0){
					wip_style = "";
					let result = [];
					$.each(dataStyle, function (i, e) {
						var matchingItems = $.grep(result, function (item) {
							return item.style === e.style
						
						});
						if (matchingItems.length === 0){
							
							let color = (parseInt(e.safety_stock)>parseInt(e.balance_style))?'badge badge-flat border-danger text-white-600':'badge badge-flat border-grey text-white-600';
							$("#wipTable_"+data.line_id+" > tbody").prepend(
								$("<tr>").append(
									$("<td>").text(e.style)
								).append(
									$("<td>").text(e.balance_style)
								).append(
									$("<td>").text(e.commitment_line)
								)
							);
							result.push(e);
						}
					});
					
				}
			}, 100);
			
			if(data.count_daily>0){
				for (let index = data.count_daily; index <= data.count_daily; index++) {
					countDaily+="<br />"+" ";
					
				}
			}
			$(".isi").append("<div class='col-lg-2' style='height: 300px;overflow: auto;margin-top:10px;'><div class='panel "+bgcolor+" has-bg-image'><div class='panel-body'><div class='col-lg-3' style='border:2px solid white;margin-top:10px;margin-bottom:5px'><h1 style='text-align: center;'>"+data.name+"</h1></div><div class='col-lg-9'><table style='margin-top:10px;margin-bottom:5px;' class='table table-bordered table-striped' id='headerTable_"+data.line_id+"'><thead><tr><th style='text-align: center;'><h3>WIP</h3></th><th style='text-align: center;'><h3>MIN WIP</h3></th></tr></thead><tbody><tr><td style='text-align: center;'><h3>"+data.balance+" PCS</h3></td><td style='text-align: center;'><h3>"+data.safety_stock+" PCS</h3></td></tr></tbody></table></div>"+table_wip+countDaily+active_andon+"</div></div>");
			
		}
	}
	const epochs = [
		['Year', 31536000],
		['Month', 2592000],
		['Day', 86400],
		['Hour', 3600],
		['Minute', 60],
		['Second', 1]
	];
	const getDuration = (timeAgoInSeconds) => {
		for (let [name, seconds] of epochs) {
			const interval = Math.floor(timeAgoInSeconds / seconds);
			if (interval >= 1) {
				return {
					interval: interval,
					epoch: name
				};
			}
		}
	};
	function MQTTconnect() {
		var s = $("#server").val();
		var p = $("#port").val();;
		if (p!="")
		{
			console.log("ports");
			port=parseInt(p);
			console.log("port" +port);
		}
		if (s!="")
		{
			host=s;
			console.log("host");
		}
		console.log("connecting to "+ host +" "+ port);
		var x=Math.floor(Math.random() * 10000); 
		var cname="orderform-"+x;
		mqtt = new Paho.MQTT.Client(host,port,cname);
		//document.write("connecting to "+ host);
		var options = {
			timeout: 3,
			onSuccess: onConnect,
			onFailure: onFailure,
		};
		
		mqtt.onConnectionLost = onConnectionLost;
		mqtt.onMessageArrived = onMessageArrived;
			//mqtt.onConnected = onConnected;
	
		mqtt.connect(options);
		return false;
	}
	function onConnectionLost(){
		console.log("connection lost");
		location.reload();
		// document.getElementById("status").innerHTML = "Connection Lost";
		// document.getElementById("messages").innerHTML ="Connection Lost";
		connected_flag=0;
	}
	function onFailure(message) {
		console.log("Failed");
		// document.getElementById("messages").innerHTML = "Connection Failed- Retrying";
		setTimeout(MQTTconnect, reconnectTimeout);
	}
	function onMessageArrived(r_message){
		out_msg="Message received "+r_message.payloadString+"<br>";
		out_msg=out_msg+"Message received Topic "+r_message.destinationName;
		console.log(out_msg);
		let value = JSON.parse(r_message.payloadString);
		let topic = $("#topic").val();
		if (r_message.destinationName==topic) {
			if(value.type=='andon_update'){
				for (var i in items) {
					let data = items[i];
					if (data.line_id == value.payload.line_id){
						let new_balance = parseInt(data.balance)-parseInt(value.payload.qty);
						data.balance = new_balance;
						
						let dataStyle = data.style;
						for(var k in dataStyle){
							var dStyle = dataStyle[k];
							if(dStyle.style == value.payload.style){
								let new_balance_style = parseInt(dStyle.balance_style)-parseInt(value.payload.qty);
								dStyle.balance_style = new_balance_style;
							}
						}
					}
				}
			}else if(value.type=='andon_add'){
				getSummaryDataEfficiency();
			}else if(value.type=='refresh'){
				location.reload();
			}
			render();
			setTimeout(function () {
				renderCard();
			}, 100);
		}
	}
	function onConnected(recon,url){
		console.log(" in onConnected " +reconn);
	}
	function onConnect() {
		connected_flag=1
		console.log("on Connect "+connected_flag);
		sub_topics();
	}
	function sub_topics(){
		//document.getElementById("messages").innerHTML ="";
		if (connected_flag==0){
		out_msg="<b>Not Connected so can't subscribe</b>"
		//document.getElementById("messages").innerHTML = out_msg;
		return false;
		}
		let stopic= $("#topic").val();
		console.log("Subscribing to topic ="+stopic);
		mqtt.subscribe(stopic);
		return false;
	}
	function checkAndon(line_id) {
		let url         = $("#url_check_andon").val();
		$.ajax({
			url: url,
			data: {
				line_id: line_id
			},
		})
		.done(function (data) {
		});
	}
	
</script>
@endsection