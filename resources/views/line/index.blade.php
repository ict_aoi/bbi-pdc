@extends('layouts.app',['active' => 'line'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Line</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Line</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('master_line.create') }}" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Buat Baru</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lineTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Machine</th>
                        <th>Factory</th>
                        <th>Urut</th>
                        <th>Buyer</th>
                        <th>Nama</th>
                        <th>WFT</th>
                        <th>Alias</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/master_line.js') }}"></script>
@endsection