@extends('layouts.app',['active' => 'line'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ubah Line</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('master_line.index') }}">Data Line</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('master_line.store'),
                'method' => 'post',
                'class' => 'form-horizontal',
                'id'=> 'form'
            ])
        !!}
            @include('form.select', [
                'field' => 'factory',
                'label' => 'Factory',
                'mandatory' => '*Wajib diisi',
                'options' => [
                    '' => '-- Pilih Factory --',
                ]+$factory,
                'class' => 'select-search',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'factory',
                    'required' =>''
                ]
            ])
            @include('form.select', [
                'field' => 'jenis',
                'label' => 'Jenis',
                'options' => [
                    ''       => '-- Pilih Jenis --',
                    'other'      => 'Other',
                    'nagai'      => 'Nagai'
                ],
                'mandatory' => '*Wajib diisi',
                'class' => 'select-search',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'jenis',
                    'required' =>''
                ]
            ])

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'name',
                    'required' => ''
                ]
            ])
            @include('form.text', [
                'field' => 'code',
                'label' => 'No Machine',
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'code',
                    'required' => ''
                ]
            ])
            @include('form.text', [
                'field' => 'order',
                'label' => 'Urut',
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'order',
                    'required' => ''
                ]
            ])
            @include('form.text', [
                'field' => 'alias',
                'label' => 'Alias',
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'alias',
                    'required' => ''
                ]
            ])
            @include('form.text', [
                'field' => 'wft',
                'label' => 'WFT',
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'wft',
                    'required' => ''
                ]
            ])
            

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/master_line.js') }}"></script>

@endsection