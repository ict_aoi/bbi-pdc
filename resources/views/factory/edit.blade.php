@extends('layouts.app',['active' => 'line'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Perusahaan</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('master_factory.index') }}">Data Factory</a></li>
            <li class="active">Baru</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'   => 'form',
                'url'    => route('master_factory.update',$factory->id),
                'method' => 'post',
                'class'  => 'form-horizontal',
                'id'     => 'form'
            ])
        !!}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    @include('form.text', [
                        'field'      => 'code',
                        'label'      => 'Code',
                        'mandatory'  => '*Wajib diisi',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'default' => strtoupper($factory->code),
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes' => [
                            'id'       => 'code',
                            'readonly' => 'readonly'
                        ]
                    ])
                    @include('form.text', [
                        'field'      => 'name',
                        'label'      => 'Name',
                        'mandatory'  => '*Wajib diisi',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'default' => strtoupper($factory->name),
                        'attributes' => [
                            'id'       => 'name',
                            'required' =>''
                        ]
                    ])
                
                    @include('form.text', [
                        'field'      => 'address',
                        'label'      => 'Address',
                        'mandatory'  => '*Wajib diisi',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'default' => strtoupper($factory->alamat),
                        'attributes' => [
                            'id'       => 'address',
                            'required' =>''
                        ]
                    ])
                    @include('form.text', [
                        'field'      => 'description',
                        'label'      => 'Description',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'default' => strtoupper($factory->description),
                        'attributes' => [
                            'id'       => 'description'
                        ]
                    ])
                </div>
            </div>	
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
    {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/master_factory.js') }}"></script>
@endsection