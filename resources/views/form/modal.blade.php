<div id="{{ $name }}Modal" class="modal fade" data-backdrop="static" data-keyboard="false" style="color:black;overflow: auto !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" onclick='return {{ $name }}Close()'>&times;</button>
				<h5 class="modal-title">{{ $title }}</h5>
			</div>

			<div class="modal-body">
				<br/>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/images/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="{{ $name }}Table"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" onclick='return {{ $name }}Close()'>Keluar</button>
			</div>
		</div>
	</div>
</div>
