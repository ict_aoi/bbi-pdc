@extends('layouts.front')

@section('page-content')
<style type="text/css">
	.strip{
		width: 100%;
		height: 40px;
		margin-bottom: 30px;
		background: #1A2732;
	}
	.strip:last-child{
		margin-bottom: 0px;
	}
	.tri-strip{
		border: 0px solid #000;
		line-height: 180px;
	}
	.line{
		font-weight: bold;
		font-size: 90px;
		line-height: 180px;
		color:#1A2732;
		position: relative;
		left: 40px;
	}
	.row{
		padding: 10px;
	}
	.pt{
		text-align: center;
		width: 100%;
		border: 0px solid #000;
		font-size: 40px;
		font-weight: bold;
		position: fixed;
		left: 0px;
		color:#1A2732;
		bottom: 40px;
	}
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-4"></div>
        <div class="col-xs-3 text-center line">{{strtoupper($master_line->name)}}</span></div>
        <div class="col-xs-4 pull-right"></div>
    </div>
    <div class="pt">
        <div class="time"><?php echo date("H:i:s"); ?></div>
        <span>{{strtoupper($master_line->factory->name)}}</span>
        </div>
    </div>
    
</div>

@endsection

@section('page-js')
<script>
	$(document).ready(function () {
		function startTime() {
			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			h = checkTime(h);
			$('.time').text(h + ":" + m + ":" + s);
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
			if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
			return i;
		}
		setInterval(function(){ 
			location.reload();

		},20000);
		startTime()
	});
</script>
@endsection

