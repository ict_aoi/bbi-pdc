@extends('layouts.front')

@section('page-content')
<style type="text/css">
	.content{
        height: 100px;
    }
    .zoom {
		zoom: 0.65;
	}
</style>
<div class="row content" >
    <div class="panel panel-flat zoom">
        <div class="panel-body">
            <h1 class="text-center" style="color: #000000;font-size:60px"> <span class="title">ACHIEVEMENT PCS</span></h1>
            <h2 class="text-center" style="color: #000000;font-size:60px"> {{ strtoupper($data->line_name) }} </h2>
            <h2 class="text-center time" style="color: #000000;font-size:50px"> <?php echo date("d-M-Y H:i:s"); ?> </h2>
            <div id="chart1" height="100" width="300"></div><br>
            <div id="chart2" class="hidden" height="100" width="300"></div><br>
            <div id="chart3" class="hidden" height="100" width="300"></div>
        </div>
    </div>
</div>
{!! Form::hidden('flag_refresh',0, array('id' => 'flag_refresh')) !!}
{!! Form::hidden('url_line_performance',route('display.line_performance',[$data->factory,$data->line]), array('id' => 'url_line_performance')) !!}
@section('page-js')
<script>
    $(document).ready(function () {
        function startTime() {
			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			h = checkTime(h);
			$('.time').text(h + ":" + m + ":" + s);
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
			if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
			return i;
		}
        function getRand() {
			var min = 58,max = 60;
            var flag_refresh = $("#flag_refresh").val();
			var url_line_performance = $("#url_line_performance").val();
			var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
			// alert('Wait for ' + rand + ' seconds');
            if (flag_refresh==0) {
				$("#flag_refresh").val(1);
			} else {
				window.location.href=url_line_performance;
			}
			
			setTimeout(getRand, rand * 1000);
		}
		
        function slideChart(){
            setTimeout(() => {
                setTimeout(function(){ 
                    $(".title").text("EFFICIENCY RATE %");
                    $("#chart1").addClass("hidden");
                    $("#chart2").removeClass("hidden");
                    setTimeout(function(){ 
                        $(".title").text("REPAIR RATE %");
                        $("#chart2").addClass("hidden");
                        $("#chart3").removeClass("hidden");
    
                    },15000);
                },15000);
            }, 20000);
        }
        startTime();
        getRand();
        slideChart();
    });
    var target_output = [];
    var target_wft    = [];
    var target_eff    = [];
    var aktual_output = [];
    var aktual_eff    = [];
    var aktual_wft    = [];
 
    Highcharts.chart('chart1', {
        chart: {
            // width: 1320,
            height: 900,
            events: {
                load: function() {
                    Highcharts.each(this.series[1].points, function(p) {
                        aktual_output.push(p.y);
                        if (p.y > 8) {
                            p.update({
                                color: 'red'
                            });
                        }
                    });
                    Highcharts.each(this.series[2].points, function(p) {
                        target_output.push(p.y);
                    });

                    for(i = 0; i < target_output.length; i++) {
                        if(aktual_output[i] >= target_output[i]){
                            this.series[1].data[i].update({
                                color: '#28B463'
                                // 28B463 197740 color: calculateColor(this.series[0].color, min, max, severity[i])
                            }, false);   
                        }
                    }

                    this.redraw();
                }
            },
            backgroundColor: 'rgba(0,0,0,0)' 
            // '#FCFFC5'
        },
        title: {
            // enabled: false
            text: '',
            align:"left",
            style:{ 
                color:'#000000',
                fontSize:'50px'
            }
        },
        xAxis: {
            // categories: ['a','b','c','d','e','f']
            categories: [{{ $data->labelWorkingHours }}],
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '30px',
                    textOutline: false 
                }
            },
            title: {
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                    textOutline: false 
                }            
            }
        },
        yAxis: {
            title: {
                text: '',
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '45px',
                    textOutline: false 
                } 
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#000000',
                    fontSize:'20px',
                    textOutline: false 
                }
            },
            tickPixelInterval: 200
        },
        legend: {
            enabled: true,
            itemStyle: {
                color: '#000000',
                fontSize:'20px'
            }   
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                label: {
                    enabled: false,
                },
                borderWidth: 0,
                dataLabels: {
                    
                        allowOverlap:true,
                        enabled: true,
                        // color: '#000',
                        color: '#000000',
                        style: {fontWeight: 'bolder'},
                        formatter: function() {return this.y},
                        inside: true,
                        style: {
                            textOutline: false,
                            fontSize: '55px',
                            // borderWidth: 0
                        }
                        // rotation: 270
                    },
                    // pointPadding: 0.1,
                    // groupPadding: 0
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:f}</b> of total<br/>'
        },
        series: [{
            name: 'Target Tercapai',
            type: 'spline',
            data: [],
        }, {
            name: 'Target Tidak Tercapai',
            type: 'column',
            data: {{ $data->qc_output }}
        },{
            name: 'Target',
            type: 'spline',
            dashStyle: 'shortdot',
            data: [{{ $data->target_pcs }}],
            lineWidth: 10
        }],
        colors: ['#28B463', '#ED3528', '#750000']
        //  '#2E86C1', '#F1C40F', '#28B463']C0392B, D02510
    });
    Highcharts.chart('chart2', {
        chart: {
            // width: 1320,
            height: 900,
            events: {
                load: function() {
                    Highcharts.each(this.series[1].points, function(p) {
                        aktual_eff.push(p.y);
                        if (p.y > 8) {
                            p.update({
                                color: 'red'
                            });
                        }
                    });
                    Highcharts.each(this.series[2].points, function(p) {
                        target_eff.push(p.y);
                    });

                    for(i = 0; i < target_eff.length; i++) {
                        if(aktual_eff[i] >= target_eff[i]){
                            this.series[1].data[i].update({
                                color: '#28B463'
                                // 28B463 197740 color: calculateColor(this.series[0].color, min, max, severity[i])
                            }, false);   
                        }
                    }

                    this.redraw();
                }
            },
            backgroundColor: 'rgba(0,0,0,0)' 
            // '#FCFFC5'
        },
        title: {
            // enabled: false
            text: '',
            style:{ 
                color:'#000000',
                fontSize:'25px'
            }
        },
        xAxis: {
            // categories: ['a','b','c','d','e','f']
            categories: [{{ $data->labelWorkingHours }}],
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '30px',
                    textOutline: false 
                }
            },
            title: {
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                    textOutline: false 
                }            
            }
        },
        yAxis: {
            title: {
                text: '',
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '45px',
                    textOutline: false 
                } 
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#000000',
                    fontSize:'20px',
                    textOutline: false 
                }
            },
            tickPixelInterval: 200
        },
        legend: {
            enabled: true,
            itemStyle: {
                color: '#000000',
                fontSize:'20px'
            }   
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                label: {
                    enabled: false,
                },
                borderWidth: 0,
                dataLabels: {
                    
                        allowOverlap:true,
                        enabled: true,
                        // color: '#000',
                        color: '#000000',
                        // style: {fontWeight: 'bolder'},
                        formatter: function() {return this.y},
                        inside: true,
                        style: {
                            textOutline: false,
                            fontSize: '55px'
                            // borderWidth: 0
                        }
                        // rotation: 270
                    },
                    // pointPadding: 0.1,
                    // groupPadding: 0
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:f}</b> of total<br/>'
        },
        series: [{
            name: 'Target Tercapai',
            type: 'spline',
            data: [],
        }, {
            name: 'Target Tidak Tercapai',
            type: 'column',
            data: {{ $data->actual_eff }}
        },{
            name: 'Target',
            type: 'spline',
            dashStyle: 'shortdot',
            data: [{{ $data->target_eff }}],
            lineWidth: 10
        }],
        colors: ['#28B463', '#ED3528', '#750000']
        //  '#2E86C1', '#F1C40F', '#28B463']C0392B, D02510
    });
    Highcharts.chart('chart3', {
        chart: {
            // width: 1320,
            height: 900,
            events: {
                load: function() {
                    Highcharts.each(this.series[1].points, function(p) {
                        aktual_wft.push(p.y);
                        if (p.y > 8) {
                            p.update({
                                color: 'red'
                            });
                        }
                    });
                    Highcharts.each(this.series[2].points, function(p) {
                        target_wft.push(p.y);
                    });

                    for(i = 0; i < target_wft.length; i++) {
                        if(aktual_wft[i] <= target_wft[i]){
                            this.series[1].data[i].update({
                                color: '#28B463'
                                // 28B463 197740 color: calculateColor(this.series[0].color, min, max, severity[i])
                            }, false);   
                        }
                    }

                    this.redraw();
                }
            },
            backgroundColor: 'rgba(0,0,0,0)' 
            // '#FCFFC5'
        },
        title: {
            // enabled: false
            text: '',
            style:{ 
                color:'#000000',
                fontSize:'25px'
            }
        },
        xAxis: {
            // categories: ['a','b','c','d','e','f']
            categories: [{{ $data->labelWorkingHours }}],
            labels: {
                style: {
                    color: '#000000',
                    fontSize: '30px',
                    textOutline: false 
                }
            },
            title: {
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                    textOutline: false 
                }            
            }
        },
        yAxis: {
            title: {
                text: '',
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '45px',
                    textOutline: false 
                } 
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#000000',
                    fontSize:'20px',
                    textOutline: false 
                }
            },
            tickPixelInterval: 200
        },
        legend: {
            enabled: true,
            itemStyle: {
                color: '#000000',
                fontSize:'20px'
            }   
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                label: {
                    enabled: false,
                },
                borderWidth: 0,
                dataLabels: {
                    
                        allowOverlap:true,
                        enabled: true,
                        // color: '#000',
                        color: '#000000',
                        // style: {fontWeight: 'bolder'},
                        formatter: function() {return this.y},
                        inside: true,
                        style: {
                            textOutline: false,
                            fontSize: '55px'
                            // borderWidth: 0
                        }
                        // rotation: 270
                    },
                    // pointPadding: 0.1,
                    // groupPadding: 0
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:f}</b> of total<br/>'
        },
        series: [{
            name: 'Target Achieved',
            type: 'spline',
            data: [],
        }, {
            name: 'Target Achieved',
            type: 'column',
            data: {{ $data->wft_output }}
        },{
            name: 'Target Not Achieved',
            type: 'spline',
            dashStyle: 'shortdot',
            data: {{ $data->target_wft }},
            lineWidth: 10
        }],
        colors: ['#28B463', '#ED3528', '#750000']
        //  '#2E86C1', '#F1C40F', '#28B463']C0392B, D02510
    });
    
</script>
@endsection