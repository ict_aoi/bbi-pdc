@extends('layouts.front')

@section('page-content')
<style type="text/css">
	.zoom {
		zoom: 0.65;
	}
	.header{
		line-height: 38px;
		border-bottom: 1px solid #FFF;
		font-weight: bold;
		background:#e7e7e7;
		font-size: 40px;
	}
	.header-isi-atas{
		background-color: #e7e7e7;
		font-size: 40px; 
		border: 10px solid white;
		color:black;
		text-align: center;

	}
	.header-isi-kiri{
		background-color: #e7e7e7;
		vertical-align: middle; 
		font-size: 40px; 
		border: 10px solid white;

	}
	.header-bottom{
		border-top: 3px solid white; 
		border-bottom: 0px solid white; 
		border-left: 3px solid white; 
		border-right: 0px solid white;
		font-size: 40px; color:#125a49

	}
	.isi{
		background-color: #A4D3EE;
		text-align: center;
		font-size: 40px; 
		border: 10px solid white;
		color:black;
	}
	table.borderless td,table.borderless th{
		border: none !important;
	}

	/* CSS Document */

	marquee {
		margin-top: 5px;
		width: 100%;
	}

	.runtext-container {
	height: 60px;
	overflow-x: hidden;
	overflow-y: visible;
	padding:0 3px 0 3px;
	}

	.main-runtext {margin: 0 auto;
	overflow: visible;
	position: relative;
	height: 30px;
	}

	.runtext-container .holder {
	position: relative;
	overflow: visible;
	display:inline;
	float:left;

	}

	.runtext-container .holder .text-container {
		display:inline;
	}

	.runtext-container .holder a{
		text-decoration: none;
		font-weight: bold;
		color:#0072CA;
		text-shadow:0 -1px 0 rgba(0,0,0,0.25);
		line-height: -0.5em;
		font-size:53px;
		height: 30px;
		line-height: 30px;
		text-align: center;
	}

	span {
		display: inline-block;
		vertical-align: middle;
		line-height: normal;
	}

	.runtext-container .holder a:hover{
		text-decoration: none;
		color:#6600ff;
	}
</style>
<div class="row">
	<br>
    <div class="panel panel-flat zoom">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed">
                    <tr>
					<td bgcolor="#ccc8c8" rowspan="4" class="text-center" width="200px"><span style="font-size:75px;color: #1A2732"><b> {{$obj->line_name}} </b></span> <p class="time" style="font-size:45px;color: #1A2732"><b><?php echo date("H:i:s"); ?></b></p></td>
					</tr>
					<tr class="header">
						<td width="150px">Terakhir Update</td>
						<td width="150px"><span id="last_update"></span></td>
						<td width="150px">Target Harian</td>
						<td width="150px"><span style="font-size:45px;text-decoration: underline" id="commitment_line">0</span></td>
					</tr>
					<tr class="header">
						<td width="150px">Style</td>
						<td width="150px"><span id="style">-</span></td>
						<td width="150px">Cum Day</td>
						<td width="150px"><span id="cumday">0</span></td>
					</tr>
					<tr class="header">
						<td width="150px">SMV</td>
						<td width="150px"><span id="smv">0</span></td>
						<td width="150px">Man Power</td>
						<td width="150px"><span id="man_power">0</span></td>
					</tr>
					
				</table>
				<div class="row">
					<table class="table table-basic table-condensed">
						<tr>
							<th colspan="3" class="col-xs-4 text-center info" style="font-size: 30px; border: 10px solid white; background-color:#50b7ea">
								<div class="runtext-container">
									<div class="main-runtext">
										<marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">
		
											<div class="holder">
		
												<div class="text-container">
													<span>
														&nbsp; &nbsp; &nbsp; <a data-fancybox-group="gallery" class="fancybox" href="javascript:(0)" title="THE ELECTRIC LIGHTING ACT: section 35"><span id="runningText1"></span></a>
													</span>
												</div>
		
												
												<div class="text-container">
													<span>
													&nbsp; &nbsp; &nbsp; <a data-fancybox-group="gallery" class="fancybox" href="javascript:(0)" title="THE ELECTRIC LIGHTING ACT: section 35"><span id="runningText2"></span></a>
													</span>
												</div>
		
											</div>
		
										</marquee>
									</div>
								</div>
							</th>
						</tr>
						
					</table>
				</div>
				<div class="row">
					<table class="table table-basic table-condensed">
						<thead>
							<tr class="">
								<th class="col-xs-4 header-isi-atas"><b>OUTPUT (PCS)</b></th>
								<th class="col-xs-4 header-isi-atas"><b><span id="current"></span></b></th>
								<th class="col-xs-4 header-isi-atas"><b><span id="accumulation"></span></b></th>
								<!-- <th class="col-xs-4 text-center success" style="font-size: 20px;"><h3><b>KUMULATIF</b></h3></th> -->
							</tr>
						</thead>
						<tbody>
							<tr style="border: 10px solid white;">
								<td class="header-isi-kiri"><b>TARGET</b></td>
								<td class="isi"><b><span id="target_hourly"></span></b></td>
								<td class="isi"><b><span id="target_accumulation"></span></b></td>
								<!-- <td class="text-center success" style="vertical-align: middle; font-size: 30px;"><?php //echo //$today_pencapaian;?></td> -->
							</tr>
							<tr>
								<td class="header-isi-kiri"><b>ACTUAL SEWING</b></td>
								
								<td class="isi"><b><span id="outputsewingcurrent"></span></b></td>
								<td class="isi"><b><span id="outputsewingaccumulation"></span></b></td>
								<!-- <td class="text-center success" style="vertical-align: middle; font-size: 30px;"><?php //echo //$today_pencapaian;?></td> -->
							</tr>
							<tr>
								<td class="header-isi-kiri"><b>ACTUAL QC</b></td>
								<td class="isi"><b><span id="outputqccurrent"></span></b></td>
								<td class="isi"><b><span id="outputqaccumulation"></span></b></td>
								<!-- <td class="text-center" style="vertical-align: middle; font-size: 40px;"><?php //echo $cum_pencapaian;?></td> -->
							</tr>
						</tbody>
						
					</table>
				</div>
				<div class="row alert" style="background-color: #ccc8c8">
					<div class="col-lg-6">
						<table class="table table-basic table-condensed">
							<tr>
								<td colspan="2" class="text-center" width="700" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 45px; color:black"><b>EFFICIENCY</b></a>
								</td>
							</tr>
							<tr>
								<td width="700" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 0px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b>TARGET</b></a>
								</td>
								<td width="300" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 0px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b><span id="targeteff"></span></b></a>
								</td>
							</tr>
							
							<tr>
								<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 3px solid white; border-right: 0px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b>ACTUAL</b></a>
								</td>
								<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 0px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 40px; color:#ff2e42"><span id="actualeff"></span></a>
								</td>
							</tr>
							
						</table>
					</div>
					<div class="col-lg-6">
						<table class="table table-basic table-condensed">
							<tr>
								<td colspan="2" class="text-center" width="700" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 45px; color:black"><b>REPAIR ENDLINE</b></a>
								</td>
							</tr>
							<tr>
								<td width="700" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 3px solid white; border-right: 0px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b>TARGET</b></a>
								</td>
								<td width="300" style="border-top: 3px solid white; border-bottom: 0px solid white; border-left: 0px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b><span id="targetWft"></span></b></a>
								</td>
								
							</tr>
							<tr>
								<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 3px solid white; border-right: 0px solid white;">
									<a href='#' style="font-size: 40px; color:black"><b>ACTUAL</b></a>
								</td>
								<td style="border-top: 0px solid white; border-bottom: 3px solid white; border-left: 0px solid white; border-right: 3px solid white;">
									<a href='#' style="font-size: 40px;"><b><span id="wft"></span></b></a>
								</td>
							</tr>
						</table>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('flag_refresh',0, array('id' => 'flag_refresh')) !!}
{!! Form::hidden('line_id',$obj->line_id, array('id' => 'line_id')) !!}
{!! Form::hidden('line',$obj->line, array('id' => 'line')) !!}
{!! Form::hidden('factory_id',$obj->factory_id, array('id' => 'factory_id')) !!}
{!! Form::hidden('factory',$obj->factory, array('id' => 'factory')) !!}
{!! Form::hidden('url_get_data',route('display.get_data_performance'), array('id' => 'url_get_data')) !!}
{!! Form::hidden('url_graph_performance',route('display.graph_performance',[$obj->factory,$obj->line]), array('id' => 'url_graph_performance')) !!}
@endsection

@section('page-js')
<script>
	document.body.style.zoom = 1.25;
	$(document).ready(function () {
		function startTime() {
			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			h = checkTime(h);
			$('.time').text(h + ":" + m + ":" + s);
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
			if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
			return i;
		}
		function getRand() {
			var min = 50,max = 60;
			var flag_refresh = $("#flag_refresh").val();
			var url_graph_performance = $("#url_graph_performance").val();
			var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
			// alert('Wait for ' + rand + ' seconds');
			if (flag_refresh==0) {
				getDataDisplay(0);
				$("#flag_refresh").val(1);
			} else {
				window.location.href=url_graph_performance;
			}
			setTimeout(getRand, rand * 1000);
		}
		
		startTime();
		getRand();
	});
	function getDataDisplay(params) {
		var url_get_data = $("#url_get_data").val();
		var line_id      = $("#line_id").val();
		var line         = $("#line").val();
		var factory      = $("#factory").val();
		var factory_id   = $("#factory_id").val();
		$.ajax({
			url: url_get_data,
			data: {
				line_id   : line_id,
				factory_id: factory_id,
				line      : line,
				factory   : factory,
			},
			beforeSend: function() {
				if (params==1) {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				}
			},
			complete: function() {
				$.unblockUI();
			},
			success: function (response) {
				if (response.active==1) {
					$("#last_update").text(response.last_update);
					$("#style").text(response.style);
					$("#smv").text(response.smv);
					$("#commitment_line").text(response.commitment_line);
					$("#cumday").text(response.cumulative_day);
					$("#man_power").text(response.man_power);
					$("#accumulation").text(response.accumulation);
					$("#current").text(response.current);
					$("#outputsewingcurrent").text(response.outputCurrentSewing);
					$("#outputqccurrent").text(response.outputCurrentQc);
					$("#outputqaccumulation").text(response.outputAccumulationQc);
					$("#outputsewingaccumulation").text(response.outputAccumulationSewing);
					$("#target_hourly").text(response.targetHourly);
					$("#target_accumulation").text(response.targetAccumulation);
					$("#targeteff").text(response.targetEff);
					$("#actualeff").html(response.actualEff);
					$("#wft").html(response.wft);
					$("#targetWft").text(response.targetWft);
					$("#runningText1").html(response.runningText);
					$("#runningText2").html(response.runningText);
				} else {
					window.open(response.active, '_self');
				}
			}
		});
	}
</script>
@endsection