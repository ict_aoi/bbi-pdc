<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Style</th>
		<th>SMV</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			
			<tr>
				<td>
					{{ $list->style }}
				</td>
				<td>
					{{ ($list->smv?$list->smv:'-') }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-style="{{ $list->style }}" data-smv="{{ $list->smv }}" 
						>Pilih</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

