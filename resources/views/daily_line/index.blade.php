@extends('layouts.app',['active' => 'daily-line'])

@section('page-css')
<style>
    div.AnyTime-win {z-index:9999}
    .clockpicker{
        z-index: 999999
    }
</style>
    <link href="{{ mix('css/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content-css')
    
@endsection
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">HarianLine</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Data Harian Line</li>
        </ul>
        
        {{--  <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-gear position-left"></i>
                    Upload Harian Line
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{route('daily_line.import')}}"><i class="glyphicon glyphicon-upload"></i> Upload</a></li>
                </ul>
            </li>
        </ul>  --}}
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            
            @if (Auth::user()->is_super_admin)
                <div class="col-lg-12">
                    @include('form.select', [
                        'field' => 'factory',
                        'label' => 'Factory',
                        'mandatory' => '*Wajib diisi',
                        'default' => $factory_id,
                        'options' => [
                            '' => '-- Pilih Factory --',
                        ]+$factory,
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'factory',
                        ]
                    ])
                </div>
            @endif

            
            <div class="col-lg-12">
                @include('form.picklist', [
                    'label' 		=> 'Line',
                    'field' 		=> 'line',
                    'name' 			=> 'line',
                    'placeholder' 	=> 'Silahkan Pilih Line',
                    'title' 		=> 'Silahkan Pilih Line',
                    'readonly'		=> true,
                ])
            </div>
            <button type="button" id="btn_cari" class="btn bg-teal-400 col-xs-12">Cari <i class="icon-search4 position-left"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="panel-heading">
            <h6 class="panel-title">Jam Kerja</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="javascript:void(0)" onclick="return addDailyWorking()" id="add_hours"><i class="icon-database-time2"></i>Tambah Jam Kerja</a></li>
                            <li><a href="javascript:void(0)" class="hidden" onclick="return addDailyWorking()" id="edit"><i class="icon-database-time2"></i>Edit Jam Kerja</a></li>
                            <li><a href="javascript:void(0)" class="hidden" id="add_style" data-toggle="modal" data-target="#createDailyModal"><i class="icon-database-diff"></i>Tambah Style</a></li>
                            <li class="divider"></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-xlg text-nowrap">
                <tbody>
                    <tr>
                        <td class="col-md-4">
                            
                            <div class="media-left media-middle">
                                <a href="javascript:void(0)" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm"></i></a>
                            </div>
    
                            <div class="media-left">
                                <h1 class="text-semibold no-margin">
                                    <span id="start_hours">-</span> <small class="display-block no-margin">Jam Awal</small>
                                </h1>
                            </div>
                        </td>
                        <td class="col-md-4">
                            <div class="media-left media-middle">
                                <a href="javascript:void(0)" class="btn border-danger-400 text-danger-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm"></i></a>
                            </div>
                            <div class="media-left">
                                <h1 class="text-semibold no-margin">
                                    <span id="end_hours">-</span> <small class="display-block no-margin">Jam Akhir</small>
                                </h1>
                            </div>
                        </td>
                        <td class="col-md-4">
                            <div class="media-left media-middle">
                                <a href="javascript:void(0)" class="btn border-info-400 text-info-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm-check"></i></a>
                            </div>
                            <div class="media-left">
                                <h1 class="text-semibold no-margin">
                                    <span id="recess">-</span> <small class="display-block no-margin">Istirahat</small>
                                </h1>
                            </div>
                        </td>
                        <td class="col-md-4">
                            <div class="media-left media-middle">
                                <a href="javascript:void(0)" class="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm-check"></i></a>
                            </div>
                            <div class="media-left">
                                <h1 class="text-semibold no-margin">
                                    <span id="working_hours">-</span> <small class="display-block no-margin">Total Jam Kerja</small>
                                </h1>
                            </div>
                        </td>
                        
                    </tr>
                </tbody>
            </table>	
        </div>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="dailyLineTable">
                <thead>
                    <tr>
                        <th width="50px" class="text-center">NO.</th>
                        <th class="text-center">STYLE</th>
                        <th class="text-center">SMV</th>
                        <th width="100px" class="text-center">KOMITMEN LINE</th>
                        <th width="80px" class="text-center">CHANGE OVER</th>
                        <th width="80px" class="text-center">CUM DAY</th>
                        <th width="80px" class="text-center">PRESENT SEWER</th>
                        <th width="120px" class="text-center">ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('url_add_daily_working',route('daily_line.add_daily_working'), array('id' => 'url_add_daily_working')) !!}
{!! Form::hidden('server',$ip_websocket, array('id' => 'server')) !!}
{!! Form::hidden('port',$port_websocket, array('id' => 'port')) !!}
{!! Form::hidden('factory_id',$factory_id, array('id' => 'factory_id')) !!}
@endsection
@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar List Line',
		'placeholder'   => 'Cari berdasarkan Nama Line',
    ])
    @include('form.modal_picklist', [
		'name'          => 'style',
		'title'         => 'Daftar List Style',
		'placeholder'   => 'Cari berdasarkan Nama Style',
    ])
    @include('form.modal', [
		'name'          => 'edit',
		'title'         => 'Form Edit Harian Line',
    ])
    @include('form.modal', [
		'name'          => 'working_hours',
		'title'         => 'Form Jam Kerja',
    ])
    @include('daily_line._create')
@endsection
@section('page-js')
<script src="{{ mix('js/mqttws31.js') }}"></script>
<script src="{{ mix('js/daily_line.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/bootstrap-clockpicker.min.js') }}"></script>
@endsection