{!!
    Form::open([
        'role'   => 'form',
        'url'    => route('daily_line.update',$data->id),
        'method' => 'post',
        'class'  => 'form-horizontal',
        'id'     => 'form_update'
    ])
!!}
<div class="col-md-12">
    
    @include('form.text', [
        'field'     => 'style',
        'label'     => 'Style',
        'mandatory' => '*Wajib diisi',
        'default'   => $data->dailyLine->style,
        'label_col' => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
        'attributes' => [
            'id'       => 'style',
            'readonly' =>'readonly'
        ]
    ])
    @include('form.text', [
        'field'     => 'change_over',
        'label'     => 'Change Over',
        'mandatory' => '*Wajib diisi',
        'default'   => $data->dailyLine->change_over,
        'label_col' => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
        'attributes' => [
            'id'       => 'change_over',
            'readonly' =>'readonly'
        ]
    ])
    @include('form.text', [
        'field'      => 'smv',
        'label'      => 'SMV',
        'mandatory'  => '*Wajib diisi',
        'default'   => $data->dailyLine->smv,
        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
        'attributes' => [
            'id'       => 'smv',
            'required' =>''
        ]
    ])
    @include('form.text', [
        'field'      => 'commitment_line',
        'label'      => 'Target Line',
        'mandatory'  => '*Wajib diisi',
        'default'   => $data->commitment_line,
        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
        'attributes' => [
            'id'       => 'commitment_line',
            'required' =>''
        ]
    ])
    @include('form.text', [
        'field'       => 'present_sewer',
        'label'       => 'Present Sewer',
        'mandatory'   => '*Wajib diisi',
        'default'     => $data->present_sewer,
        'placeholder' => 'Masukan Jumlah Sewer yang Hadir',
        'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
        'attributes'  => [
            'id'       => 'present_sewer',
            'required' =>''
        ]
    ])
    @include('form.text', [
        'field'       => 'absent_sewer',
        'label'       => 'Absent Sewer',
        'mandatory'   => '*Wajib diisi',
        'placeholder' => 'Masukan Jumlah Sewer yang Tidak Hadir Hadir',
        'default'     => $data->absent_sewer,
        'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
        'attributes'  => [
            'id'       => 'absent_sewer',
            'required' =>''
        ]
    ])
    @include('form.text', [
        'field'       => 'present_ipf',
        'label'       => 'Present IPF',
        'placeholder' => 'Masukan Jumlah Ironing,Folding yang Hadir',
        'default'     => $data->present_ipf,
        'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
        'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
        'attributes'  => [
            'id'       => 'present_ipf'
        ]
    ])
    
</div>
{!! Form::hidden('line_id',$data->dailyLine->line_id, array('id' => 'line_id')) !!}
{!! Form::hidden('id',$data->id, array('id' => 'id')) !!}
<div class="modal-footer">
    <button type="submit" class="btn btn-primary legitRipple">Update <i class="icon-floppy-disk position-right"></i></button>
</div>
{!! Form::close() !!}
<script>
    $('#form_update').submit(function(event) {
        event.preventDefault();
        var line_id = $("#line_id").val();
        if (!line_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
            return false;
        }
        $('#editModal').modal('hide');
        bootbox.confirm("Apakah Anda Yakin Data Akan di update ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form_update').attr('action'),
                    data: $('#form_update').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function(response) {
                        $.ajax({
                            type: 'get',
                            url: '/daily-line/send-alert-andon',
                            data:{
                                'line_id': response.line_id,
                                'style'  : response.style
                            }
                        })
                        .done(function(data)
                        {
                            //let message = JSON.stringify(data.msg);
                            console.log(JSON.stringify(data.msg));
                            let message = '{"type":"commitment","payload":'+JSON.stringify(data.msg)+'}'
                            let message_andon = '{"type":"andon_add","payload":"update message"}';
                            send_message(data.topic,message);
                            send_message(data.topic_andon,message_andon);
                        });
                        $('#dailyLineTable').DataTable().ajax.reload();

                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                        if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                        $('#confirmationModal').modal();
                    }
                });
            }
        });
    });
</script>