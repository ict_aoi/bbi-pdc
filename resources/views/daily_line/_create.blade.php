<div id="createDailyModal" data-backdrop="static"  data-keyboard="false" class="modal fade" style="color:black;overflow: auto !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
                {!!
                    Form::open([
                        'role'   => 'form',
                        'url'    => route('daily_line.store'),
                        'method' => 'post',
                        'class'  => 'form-horizontal',
                        'id'     => 'form'
                    ])
                !!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Buat Harian Line Baru</h5>
				</div>
				<div class="modal-body">
					<div class="row">
                        <div class="col-md-12">
                            @include('form.picklist', [
                                'field'       => 'style',
                                'label'       => 'Pilih Style',
                                'name'        => 'style',
                                'mandatory'   => '*Wajib diisi',
                                'placeholder' => 'Silahkan Pilih Style',
                                'readonly'    => 'readonly',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes'  => [
                                    'id'       => 'style',
                                    'readonly' =>'readonly',
                                ]
                            ])
                            @include('form.select', [
                                'field' => 'change_over',
                                'label' => 'Change Over',
                                'options' => [
                                    ''       => '-- Pilih Change Over --',
                                    'a'      => 'A',
                                    'b'      => 'B',
                                    'c'      => 'C',
                                    'd'      => 'D',
                                    'repeat' => 'REPEAT',
                                ],
                                'class' => 'select-search',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id' => 'select_change_over',
                                    'required' =>''
                                ]
                            ])
                            @include('form.text', [
                                'field'      => 'smv',
                                'label'      => 'SMV',
                                'mandatory'  => '*Wajib diisi',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'smv',
                                ]
                            ])
                            @include('form.text', [
                                'field'      => 'commitment_line',
                                'label'      => 'Target Line',
                                'mandatory'  => '*Wajib diisi',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'commitment_line',
                                    'required' =>''
                                ]
                            ])
                        
                            @include('form.text', [
                                'field'      => 'present_sewer',
                                'label'      => 'Present Sewer',
                                'mandatory'  => '*Wajib diisi',
                                'placeholder'  => 'Masukan Jumlah Sewer yang Hadir',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'present_sewer',
                                    'required' =>''
                                ]
                            ])
                            @include('form.text', [
                                'field'       => 'absent_sewer',
                                'label'       => 'Absent Sewer',
                                'mandatory'   => '*Wajib diisi',
                                'placeholder' => 'Masukan Jumlah Sewer yang Tidak Hadir Hadir',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes'  => [
                                    'id'       => 'absent_sewer',
                                    'required' =>''
                                ]
                            ])
                            @include('form.text', [
                                'field'       => 'present_ipf',
                                'label'       => 'Present IPF',
                                'placeholder' => 'Masukan Jumlah Ironing,Folding yang Hadir',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes'  => [
                                    'id'       => 'present_ipf'
                                ]
                            ])
                            {{--  @include('form.text', [
                                'field'      => 'present_folding',
                                'label'      => 'Absensi Folding',
                                'mandatory'  => '*Wajib diisi',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'present_folding',
                                    'required' =>''
                                ]
                            ])  --}}
                        </div>
                    </div>	
				</div>
                {!! Form::hidden('line_id',null, array('id' => 'line_id')) !!}
                {!! Form::hidden('daily_working_id',null, array('id' => 'daily_working_id')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</div>