@extends('layouts.app',['active' => 'company'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Perusahaan</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Organisasi</li>
            <li><a href="{{ route('process.index') }}">Perusahaan</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('process.update',$process->id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'id'=> 'form'
            ])
        !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'default' => ucwords($process->name),
                'mandatory' => '*Wajib diisi',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'name',
                    'required' => ''
                ]
            ])
             @include('form.select', [
                'field' => 'subcontclass',
                'label' => 'Jenis Subcont',
                'default' => $process->subcont_class_id,
                'mandatory' => '*Wajib diisi',
                'options' => [
                    '' => '-- Pilih Jenis Subcont --',
                ]+$jenis,
                'class' => 'select-search',
                'attributes' => [
                    'id' => 'select_subcont',
                    'required'=>''
                ]
            ])
            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'default' => ucwords($process->description),
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'description',
                    'rows' => 5,
                    'style' => 'resize: none;'
                ]
            ])

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/process.js') }}"></script>

@endsection