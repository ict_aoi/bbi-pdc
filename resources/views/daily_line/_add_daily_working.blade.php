
{!!
    Form::open([
        'role'   => 'form',
        'url'    => route('daily_line.store_daily_working'),
        'method' => 'post',
        'class'  => 'form-horizontal',
        'id'     => 'form_update'
    ])
!!}
<div class="col-md-12">
    
    
    <div class="row">
        <div class="col-md-6"><label class="text-semibold control-label col-md-6 col-lg-6 col-sm-12">Jam Kerja : </label></div>
    </div>
    <div class="col-md-12">
        <label for="start" class="control-label font-weight-bold col-md-4 col-lg-4 col-sm-12">
            Mulai
        </label>
        
        <div class="input-group clockpicker col-md-8 col-lg-8 col-sm-12" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" name="start" class="form-control" value={{ date('g:i a',strtotime($obj->start)) }} id="start_edit">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
        <br><br>
        <label for="is_super_admin" class="control-label font-weight-bold col-md-4 col-lg-4 col-sm-12">
            Akhir
        </label>
        
        <div class="input-group clockpicker col-md-8 col-lg-8 col-sm-12" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" name="end" class="form-control" value={{ $obj->end }} id="end_edit">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
        <br><br>
        <label for="recess" class="control-label font-weight-bold col-md-4 col-lg-4 col-sm-12">
            Istirahat
        </label>
        
        <div class="input-group clockpicker col-md-8 col-lg-8 col-sm-12" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" name="recess" class="form-control" value={{ $obj->recess }}>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
            </span>
        </div>
        <br><br><br><br>
        <br><br>
    </div>
</div>

{!! Form::hidden('line_id',$obj->line_id, array('id' => 'line_id')) !!}
{!! Form::hidden('id',$obj->id, array('id' => 'id')) !!}

<div class="modal-footer">
    <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
</div>
{!! Form::close() !!}
<script>
    $('.pickatime-limits-integers').pickatime({
        
    })
    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        donetext: 'Done'
    });
    $('#form_update').submit(function(event) {
        event.preventDefault();
        var line_id = $("#line_id").val();
        if (!line_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
            return false;
        }
        $('#working_hoursModal').modal('hide');
        bootbox.confirm("Apakah Anda Yakin Data Akan di update ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form_update').attr('action'),
                    data: $('#form_update').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function(response) {
                        // $("#alert_success").trigger("click", response.responseJSON.message);
                        $("#add_hours").addClass('hidden');
                        $("#edit").removeClass('hidden');
                        $("#add_style").removeClass('hidden');
                        $("#start_hours").text(response.obj.start);
                        $("#end_hours").text(response.obj.end);
                        $("#working_hours").text(response.obj.working_hours);
                        $("#daily_working_id").val(response.obj.id);
                        $("#recess").val(response.obj.recess);
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                        if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                        $('#confirmationModal').modal();
                    }
                });
            }
        });
    });
</script>