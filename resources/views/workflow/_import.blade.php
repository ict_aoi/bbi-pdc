<script type="x-tmpl-mustache" id="items_upload">
	{% #item %}
		<tr {% #is_error %} style="background-color:#FF5722;color:#fff" {% /is_error %}>
			<td>{% no %}</td>
			<td>
				{% style %}
			</td>
			<td>
				{% nama_proses %}
			</td>
			<td>
				{% component %}
			</td>
			<td>
				{% nama_mesin %}
			</td>
			<td>
				{% smv %}
			</td>
			<td>
				{% proses_terakhir %}
			</td>		
			<td>
				{% critical_process %}
			</td>		
			<td>
				{% system_log %}
			</td>
		</tr>
	{%/item%}
</script>