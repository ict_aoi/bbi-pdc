@extends('layouts.app',['active' => 'work-flow'])

@section('page-header')


<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Buat Alur Kerja Sewing</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('alur_kerja.index') }}">Alur Kerja Sewing</a></li>
            <li class="active">Buat Alur Kerja Sewing</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row" id="">    
<div class="panel-body">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>                
        </div>
        <div class="panel-body">
        <a href="{{route('alur_kerja.import',$id)}}" class="btn btn-default space"><i class="icon-drawer-out position-left"></i>Import</a>
            <br><br><br>
            {!!
                Form::open([
                    'role'    => 'form',
                    'url'     => route('alur_kerja.simpan_detail'),
                    'method'  => 'post',
                    'class'   => 'form-horizontal',
                    'enctype' => 'multipart/form-data',
                    'id'      => 'form'
                ])
            !!}
            @include('form.text', [
                'field'     => 'style',
                'label'     => 'Style',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $data->style,
                'attributes' => [
                    'id'        => 'style',
                    'readonly'  => '',
                ]
            ])
            @include('form.text', [
                'field'     => 'smv',
                'label'     => 'SMV',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $data->smv,
                'attributes' => [
                    'id'        => 'smv',
                    'required'  =>''
                ]
            ])
        </div>
    </div>
</div>
</div>
<div class="row" id="">
    
    <div class="col-lg-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-striped table-hover" id="detailPanelTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Proses</th>
                                    <th>Komponen</th>
                                    <th>SMV</th>
                                    <th>Proses Terakhir</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-items">
                            </tbody>
                        </table>
                    </div>
                </div>
                <label for="lastprocess" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                    Proses Terakhir
                </label><br><div class="col-md-10 col-lg-10 col-sm-12"><span class=""><input type="checkbox" class="styled" name="lastprocess" id="cklastproses"></span></div>
                @include('form.select', [
                    'field'     => 'process',
                    'label'     => 'Proses Sewing',
                    'mandatory' => '*Wajib diisi',
                    'options'   => [
                        '' => '-- Pilih Jenis Proses --',
                    ],
                    'class'      => 'select-search',
                    'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes' => [
                        'id' => 'prosesName',
                        'onchange'=>'addProses()'
                    ]
                ])
                <div class="row">
                    <br/>
                    
                </div>
            
            </div>
        </div>
    </div>
    {!! Form::hidden('items',json_encode($detail), array('id' => 'items')) !!}
    {!! Form::hidden('id',$id, array('id' => 'id')) !!}
    <div class="text-right">
        <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
    </div>
    {!! Form::close() !!}

</div>

@endsection
@section('page-modal')
    @include('workflow._fields_item_workflow')
    @include('form.modal_picklist', [
        'name'        => 'style',
        'title'       => 'Daftar Style',
        'placeholder' => 'Cari berdasarkan Style',
    ])
@endsection

@section('page-js')
<script src="{{ mix('js/create_workflow.js') }}"></script>
@endsection
