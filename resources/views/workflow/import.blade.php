@extends('layouts.app',['active' => 'work-flow'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Request</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('alur_kerja.index') }}">Alur Kerja Sewing</a></li>
            <li><a href="{{ route('alur_kerja.create',$id) }}">Detail Alur Kerja Sewing</a></li>
            <li class="active">Import</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
                <a href="{{ route('alur_kerja.exportFormImport',$id) }}" class="btn btn-primary btn-icon"  data-popup="tooltip" title="download template upload" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				{!!
					Form::open([
                        'role'    => 'form',
                        'url'     => route('alur_kerja.uploadFormImport',$id),
                        'method'  => 'POST',
                        'id'      => 'upload_file_allocation',
                        'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table table-striped">
                <thead>

                    <tr>
                        <th>No</th>
                        <th>Style</th>
                        <th>Nama Proses</th>
                        <th>Komponen</th>
                        <th>Nama Mesin</th>
                        <th>SMV</th>
                        <th>Proses Terakhir</th>
                        <th>Critical Process</th>
                        <th>Sistem log</th>
                    </tr>
                </thead>
                <tbody id="draw"></tbody>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('result', '[]', array('id' => 'result')) !!}
@endsection

@section('page-modal')
    @include('workflow._import')
@endsection

@section('page-js')
    <script src="{{ mix('js/import_detail_workflow.js') }}"></script>
@endsection