<table class="table">
	<thead>
	  <tr>
		<th>Style</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($data as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-style="{{ $list->style }}" >Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>