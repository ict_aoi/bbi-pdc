@extends('layouts.app',['active' => 'work-flow'])

@section('page-header')


<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Buat Alur Kerja Sewing</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('alur_kerja.index') }}">Alur Kerja Sewing</a></li>
            <li class="active">Buat Alur Kerja Sewing</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row" id="">    
<div class="panel-body">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Detail<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>                
        </div>
        <div class="panel-body">
        <a href="{{route('alur_kerja.import',$id)}}" class="btn btn-default space"><i class="icon-drawer-out position-left"></i>Import</a>
            <br><br><br>
            {!!
                Form::open([
                    'role'    => 'form',
                    'url'     => route('alur_kerja.update_header',$id),
                    'method'  => 'post',
                    'class'   => 'form-horizontal',
                    'enctype' => 'multipart/form-data',
                    'id'      => 'form'
                ])
            !!}
            @include('form.text', [
                'field'     => 'style',
                'label'     => 'Style',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $data->style,
                'attributes' => [
                    'id'        => 'style',
                    'readonly'  => '',
                ]
            ])
            @include('form.text', [
                'field'     => 'gsd',
                'label'     => 'SMV',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'default'   => $data->smv,
                'attributes' => [
                    'id'        => 'gsd',
                    'required'  =>''
                ]
            ])
            {!! Form::hidden('id',$id, array('id' => 'id')) !!}
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
<div class="row" id="">
    
    <div class="col-lg-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                
            </div>
            
            <div class="panel-body">
                <button type="button" id="btn-tambah" class="btn btn-success btn-rounded legitRipple">Tambah <i class="fa fa-plus position-left"></i></button>
                <button type="button" id="btn-gabung" class="btn btn-primary btn-rounded legitRipple">Gabung <i class="fa fa-plus position-left"></i></button>
                <br><br>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-striped table-hover" id="detailWorkflowTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Proses</th>
                                    <th>Komponen</th>
                                    <th>Machine</th>
                                    <th>SMV</th>
                                    <th>Proses Terakhir</th>
                                    <th>Critical Process</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>
                
            
            </div>
        </div>
    </div>
    {{-- {!! Form::hidden('items',json_encode($detail), array('id' => 'items')) !!} --}}
    {!! Form::hidden('create_process',route('alur_kerja.create_process', $id), array('id' => 'create_process')) !!}
    {!! Form::hidden('merge_process',route('alur_kerja.merge_process', $id), array('id' => 'merge_process')) !!}
    

</div>

@endsection
@section('page-modal')
    @include('workflow._fields_item_workflow')
    @include('form.modal', [
        'name'          => 'edit',
        'title'         => 'Edit Proses',
    ])
    @include('form.modal', [
        'name'          => 'mergeProcess',
        'title'         => 'Merge Proses',
    ])
@endsection

@section('page-js')
<script src="{{ mix('js/create_workflow.js') }}"></script>
@endsection
