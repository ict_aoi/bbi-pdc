<div id="createWorkflowModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('alur_kerja.simpan_header'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Buat Alur Kerja</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@include('form.picklist', [
                                'field'       => 'style',
                                'label'       => 'Pilih Style',
                                'name'        => 'style',
                                'mandatory'   => '*Wajib diisi',
                                'placeholder' => 'Silahkan Pilih Style',
                                'readonly'    => 'readonly',
                                'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes'  => [
                                    'id'       => 'styleName',
                                    'readonly' =>'readonly',
                                ]
                            ])
                            
                            @include('form.text', [
                                'field'      => 'gsd',
                                'label'      => 'SMV',
                                'name'       => 'gsd',
                                'mandatory'  => '*Wajib diisi',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'gsd',
                                    'required' => ''
                                ]
                            ])
						</div>
						</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</div>