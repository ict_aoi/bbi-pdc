
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#" onclick="edit('{!! $edit !!}')"><i class="icon-pencil6"></i> Edit</a></li>
            <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Hapus</a></li>
        </ul>
    </li>
</ul>
