<script type="x-tmpl-mustache" id="items-table">  
    {% #item %}
      <tr>
        <td style="text-align:center;">{% no %}</td>
        <td>
          <div id="item_{% _id %}">{% proses_name %}</div>
          
          <input type="text" class="form-control input-md hidden" id="itemInputId_{% _id %}" 
            value="{% id %}" data-id="{% _id %}"
          <span class="text-danger hidden" id="errorInput_{% _id %}">
          </span>
        </td>
        <td>
          <div id="component_{% _id %}">{% component %}</div>
        </td>
        <td>
          <div id="smv_{% _id %}">{% smv %}</div>
        </td>
        <td width="80px">
          <input type="checkbox" class="checkbox_item" disabled id="check_{% _id %}" data-id="{% _id %}" {% #lastproses %} checked="checked" {% /lastproses %}>
        </td>      
        <td width="80px">
              <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="icon-trash"></i></button>
        </td>
      </tr>
    {%/item%}
  
     <tr>
      <td width="20px">
        #
      </td>
      <td colspan="4">
        <input type="hidden" id="prosesId">
        <br>      
      </td>
      
      
    </tr>
  </script>