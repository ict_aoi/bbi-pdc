<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('alur_kerja.update',$data->processSewing->id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'=> 'form-edit'
            ])
        !!}
        @include('form.text', [
            'field' => 'name_process',
            'label' => 'Nama Proses',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'default'   => strtoupper($data->processSewing->name),
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'name_process',
            ]
        ])
        @include('form.text', [
            'field' => 'component',
            'label' => 'Komponen',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'default'   => strtoupper($data->processSewing->component),
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'component',
            ]
        ])
        @include('form.text', [
            'field' => 'name_machine',
            'label' => 'Nama Mesin',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'default'   => isset($data->machine->name)?strtoupper($data->machine->name):'',
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'name_machine'
            ]
        ])
        @include('form.text', [
            'field' => 'smv',
            'label' => 'SMV',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'default'   => strtoupper($data->processSewing->smv),
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'smv',
            ]
        ])
        @include('form.select', [
            'field' => 'last_process',
            'default' => ($data->last_process)?"Y":"N",
            'label' => 'Proses Terakhir',
            'mandatory' => '*Wajib diisi',
            'options' => [
                '' => '-- Pilih Proses Terakhir --',
                'Y' => 'YA',
                'N' => 'TIDAK',
            ],
            'class' => 'select-search2',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'last_process'
            ]
        ])
        @include('form.select', [
            'field' => 'critical_process',
            'default' => ($data->critical_process)?"Y":"N",
            'label' => 'Critical Process',
            'mandatory' => '*Wajib diisi',
            'options' => [
                '' => '-- Pilih Critical PRocess --',
                'Y' => 'YA',
                'N' => 'TIDAK',
            ],
            'class' => 'select-search2',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'critical_process'
            ]
        ])
        {!! Form::hidden('workflow_id',$data->workflow_id, array('id' => 'workflow_id')) !!}
        {!! Form::hidden('process_id',$data->process_sewing_id, array('id' => 'process_id')) !!}
        <div class="text-right">
            <button type="submit" class="btn btn-primary col-xs-12 legitRipple">Update <i class="icon-floppy-disk position-right"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('.select-search2').select2();
    $('#smv').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $("#name_process").change(function (e) { 
        e.preventDefault();
        $('#component').val("");
    });
    
    $('#form-edit').submit(function (event){
        event.preventDefault();
        var name_process = $('#name_process').val();
        var component = $('#component').val();
        var name_machine = $('#name_machine').val();
        var smv = $('#smv').val();
        var last_process = $('#last_process').val();
        var critical_process = $('#critical_process').val();
        
        if(!name_process)
        {
            $("#alert_warning").trigger("click", 'Nama Process wajib diisi');
            return false;
        }

        if(!component)
        {
            $("#alert_warning").trigger("click", 'Nama Komponen Wajib diisi');
            return false
        }
        if(!name_machine)
        {
            $("#alert_warning").trigger("click", 'Nama Machine Wajib diisi');
            return false
        }
        if(!smv)
        {
            $("#alert_warning").trigger("click", 'SMV Wajib diisi');
            return false
        }
        if(!last_process)
        {
            $("#alert_warning").trigger("click", 'Jenis Proses Terakhir wajib dipilih');
            return false
        }
        if(!critical_process)
        {
            $("#alert_warning").trigger("click", 'Jenis Critical Process wajib dipilih');
            return false
        }
        bootbox.confirm("Apakah anda yakin akan update data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form-edit').attr('action'),
					data:new FormData($("#form-edit")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Data berhasil disimpan.')
                        $('#editModal').modal('hide');
                        $('#detailWorkflowTable').DataTable().ajax.reload();
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });
</script>