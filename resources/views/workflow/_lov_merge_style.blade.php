<div id="mergeStyleModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form-style',
						'url' 		=> route('alur_kerja.simpan_style'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form-style'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Gabung Style</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
                            @include('form.text', [
                                'field'      => 'style',
                                'label'      => 'Style',
                                'name'       => 'gsd',
                                'mandatory'  => '*Wajib diisi',
                                'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                'attributes' => [
                                    'id'       => 'txtStyleMerge',
                                    'required' => ''
                                ]
							])
							<div class="form-group  col-lg-12">
								<label for="style" class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">
								Gabung Style</label>
								<div class="control-input  col-md-9 col-lg-9 col-sm-12">
									<select multiple data-placeholder="Pilih style..." class="form-control select-search" name="styleMerge[]" id="select_style_merge">
									
									</select>
									<span id="style_danger" class="help-block text-danger">*Wajib diisi</span>
								</div>
							</div>
						</div>
						</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</div>