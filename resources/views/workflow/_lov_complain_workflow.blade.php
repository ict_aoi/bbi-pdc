<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>No. Complain</th>
		<th>Line</th>
		<th>QC</th>
		<th>Style</th>
		<th>Pesan</th>
		<th>Status</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->nomor) }}
				</td>
				<td>
					{{ strtoupper($list->line->name) }}
				</td>
				<td>
					{{ strtoupper($list->create_qc_name)."-".$list->create_qc_nik }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					{{ strtoupper($list->message) }}
				</td>
				<td>
					@if ($list->status=='open')
						<span class="label label-danger">Open</span>
					@elseif($list->status=='close')
						<span class="label label-flat border-success text-success-600 position-right">Close</span>
					@endif
				</td>
				<td>
					@if ($list->status=='open')
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="javascript:void(0)" onclick="send_message('{{ $list->id }}')"><i class="icon-pencil6"></i> Ubah Status</a></li>
									@if ($list->workflow_id!=null)
										<li><a href="{{ route('alur_kerja.create',$list->workflow_id) }}"><i class="icon-spell-check"></i> Tambah / Edit</a></li>
									@else
										<li><a href="javascript:void(0)" onclick="editWorkflow('{{ $list->style }}')"><i class="icon-spell-check"></i> Tambah / Edit</a></li>
									@endif
									
								</ul>
							</li>
						</ul>
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
