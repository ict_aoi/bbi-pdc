@extends('layouts.app',['active' => 'work-flow'])

@section('page-header')
<style>
    .daterangepicker{z-index:1151 !important;}
</style>
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Alur Kerja Sewing</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Alur Kerja Sewing</li>
        </ul>
        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-filter3 position-left"></i>
                    Filter
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-lock"></i> Factory</a></li>
                    <li class="divider"></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h6 class="panel-title">Style Statistics</h6>
    </div>
    
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-4 col-md-4">
                <div class="panel bg-teal-400">
                    <div class="panel-body">
                        <h2 class="no-margin total_defect"><span id="active">0</span></h2>
                        Style Active
                    </div>
        
                    <div class="container-fluid">
                        <div id="members-online"></div>
                    </div>
                </div>
        
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="panel bg-danger-400">
                    <a href="javascript:void(0)" onclick="showStyleOutstanding()">
                        <div class="panel-body">
                            <h2 class="no-margin total_defect"><span id="outstanding">0</span></h2>
                            Style Outstanding
                        </div>
            
                        <div class="container-fluid">
                            <div id="members-online"></div>
                        </div>
                    </a>
                </div>
        
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="panel bg-orange-700">
                    <a href="javascript:void(0)" onclick="showComplainWorkflow()">
                        <div class="panel-body">
                            <h2 class="no-margin total_defect"><span id="complainWorkflow">0</span></h2>
                            Complain Workflow
                        </div>
            
                        <div class="container-fluid">
                            <div id="members-online"></div>
                        </div>
                    </a>
                </div>
        
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-gear position-left"></i>
                    Action
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#createWorkflowModal"><i class="icon-plus2"></i>Create</a></li>
                    <li class="divider"></li>
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#mergeStyleModal"><i class="icon-merge"></i> Merge style</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="dataWorkflowTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Style</th>
                        <th>GSD</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection
{!! Form::hidden('url_style_outstanding',route('alur_kerja.style_outstanding'), array('id' => 'url_style_outstanding')) !!}
{!! Form::hidden('url_complain_workflow',route('alur_kerja.complain_workflow_data'), array('id' => 'url_complain_workflow')) !!}
{!! Form::hidden('url_send_message',route('alur_kerja.send_message'), array('id' => 'url_send_message')) !!}
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'style',
        'title'       => 'Daftar Style',
        'placeholder' => 'Cari berdasarkan Style',
    ])
    @include('form.modal', [
        'name'        => 'styleOutstanding',
        'title'       => 'List Style Yang belum ada Proses Sewing',
    ])
    @include('form.modal_picklist', [
		'name'          => 'complainWorkflow',
		'title'         => 'Daftar Complain Workflow',
		'placeholder'   => 'Cari berdasarkan Status/Style',
    ])
    @include('form.modal', [
        'name' => 'send_message',
        'title' => 'Send Message'
    ])
    @include('workflow._lov_create_header_workflow')
    @include('workflow._lov_merge_style')
    
@endsection
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/data_workflow.js') }}"></script>
@endsection