<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form-send',
                'url' => route('alur_kerja.store_message',$id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'=> 'form-send'
            ])
        !!}
        
        @include('form.select', [
            'field' => 'status',
            'label' => 'Pilih Status',
            'mandatory' => '*Wajib diisi',
            'options' => [
                '' => '-- Pilih Status --',
                'close' => 'Close',
            ],
            'class' => 'select-search',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'mandatory' => '*Wajib diisi',
            'attributes' => [
                'id' => 'status-message'
            ]
        ])
        @include('form.textarea', [
            'field' => 'message',
            'label' => 'Isi Pesan',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'message',
                'rows' => 5,
                'style' => 'resize: none;'
            ]
        ])
        <div class="text-right">
            <button type="submit" class="btn btn-primary col-xs-12 legitRipple">Kirim Pesan <i class="icon-mail-read"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('.select-search').select2();
    $('#smv').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $("#name_process").change(function (e) { 
        e.preventDefault();
        $('#component').val("");
    });
    
    $('#form-send').submit(function (event){
        event.preventDefault();
        var status = $('#status-message').val();
        var message = $('#message').val();

        if(!status)
        {
            $("#alert_warning").trigger("click", 'Status Wajib di pilih');
            return false;
        }

        if(!message)
        {
            $("#alert_warning").trigger("click", 'Message Wajib diisi');
            return false
        }
        bootbox.confirm("Apakah anda yakin kirim pesan ini ?.", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: $('#form-send').attr('action'),
					data:new FormData($("#form-send")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Message berhasil dikirim.')
                        $('#send_messageModal').modal('toggle');
                        getWorkflow();
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });
</script>