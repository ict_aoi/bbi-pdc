<div class="panel panel-flat">
    <div class="panel-body">
        
        @include('form.select', [
            'field' => 'process',
            'label' => 'Process',
            'options' => [
                '' => '-- Pilih Process --',
            ]+$lists,
            'class' => 'select-search',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col' => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'process'
            ]
        ])
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('alur_kerja.store_merge_process',$id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'=> 'form-merge'
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label for="style" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
            Gabung Proses</label>
            <div class="control-input  col-md-10 col-lg-10 col-sm-12">
                <select multiple data-placeholder="" class="form-control select-search" name="processMerge[]" id="select_process_merge">
                
                </select>
                <span id="process_danger" class="help-block text-danger">*Wajib diisi</span>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary col-xs-12 legitRipple">Update <i class="icon-floppy-disk position-right"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('.select-search').select2();
    $('#process').on('select2:select', function (e) {
        var data = e.params.data;
        $("#select_process_merge").select2({
            data: data
        })
        var newOption = new Option(data.text, data.id, true, true);
        var diff = checkProcess(data.id);
        if(diff!=0){
            $("#alert_warning").trigger("click", 'Process sudah dipilih');
            return false;
        }
        
        $("#select_process_merge").append(newOption).trigger('change');
    });
    $('#smv').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $("#name_process").change(function (e) { 
        e.preventDefault();
        $('#component').val("");
    });
    function checkProcess(id){

        var dataMerge = $("#select_process_merge").val();
        if(dataMerge){
            var count =0;
            var x =0;
            dataMerge.forEach(element => {
                if(element==id){
                    x = count+1
                    
                }
            });
            return x;
            
        }else{
            return 0;
        }
        
    }
    $('#form-merge').submit(function (event){
        event.preventDefault();
        var select_process_merge = $('#select_process_merge').val();
        
        
        if(!select_process_merge)
        {
            $("#alert_warning").trigger("click", 'Process yang di gabung wajib dipilih');
            return false
        }
        bootbox.confirm("Apakah anda yakin akan di gabung?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form-merge').attr('action'),
					data:new FormData($("#form-merge")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Data berhasil disimpan.')
                        $('#mergeProcessModal').modal('hide');
                        $('#detailWorkflowTable').DataTable().ajax.reload();
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });
</script>