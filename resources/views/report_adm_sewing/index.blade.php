@extends('layouts.app',['active' => 'adm-line'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Admin Loading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Admin Receive</li>
        </ul>
    </div>
    
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownload"><i class="icon-file-excel"></i> Download</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('report_admin_sewing.export_form_admin_sewing'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="row">
            <div class="form-group">
                <div class="col-md-6">
                    @include('form.date', [
                        'field' => 'start_date',
                        'label' => 'Tanggal Awal',
                        'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                        'placeholder' => 'dd/mm/yyyy',
                        'attributes' => [
                            'id'        => 'start_date',
                        ]
                    ])
                </div>
                <div class="col-md-6">
                    @include('form.date', [
                        'field' => 'end_date',
                        'label' => 'Tanggal Akhir',
                        'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                        'placeholder' => 'dd/mm/yyyy',
                        'attributes' => [
                            'id'        => 'end_date',
                        ]
                    ])
                </div>
            </div>
            @include('form.picklist', [
            'field'       => 'poreference',
            'label'       => 'Pilih Po Buyer',
            'name'        => 'poreference',
            'placeholder' => 'Silahkan Pilih Po Buyer',
            'readonly'    => 'readonly',
            'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
            'attributes'  => [
                'id'       => 'poreference',
                'readonly' =>'readonly',
            ]
        ])
        @include('form.select', [
            'field'     => 'size',
            'label'     => 'Size',
            'options'   => [
                '' => '-- Pilih Size --',
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'selectSize',
            ]
        ])
        </div>
        
        
        <button type="button" id="btnTampilkan" class="btn btn-primary col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="button" id="btnReset" class="btn btn-warning col-xs-12 legitRipple" style="margin-top: 15px">Reset <i class="glyphicon glyphicon-remove position-right"></i></button>
        <button type="submit" id="btnDownloadFilter" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportAdmLoadingTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>code</th>
                            <th>Line Name</th>
                            <th>Tanggal</th>
                            <th>Style</th>
                            <th>Po Buyer</th>
                            <th>Article</th>
                            <th>Size</th>
                            <th>Total Qty Loading</th>
                            <th>Qty Loading</th>
                            <th>Total Qc Output</th>
                            <th>Qc Output</th>
                            <th>WIP Sewing</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'poreference',
        'title'       => 'Daftar Po Buyer',
        'placeholder' => 'Cari berdasarkan Po Buyer',
    ])
@endsection
@section('page-js')
<script src="{{ mix('js/report_admin_loading.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
@endsection