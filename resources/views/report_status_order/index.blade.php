@extends('layouts.app',['active' => 'status-order'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-cube3 position-left"></i> <span class="text-semibold">Status Order</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">data</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownloadFilter"><i class="icon-file-excel"></i> Download Filter</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('report_status_order.export_form_filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Date Finish Sewing</label>
            <div class="input-group">
                <input type="text" class="form-control pickadate-weekday" name="date" id="txtDate" placeholder="Choose Date Finish Sewing" required> 
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
            </div>
        </div>
        
        <button type="submit" id="btnDownload" class="hidden">Download</button>
        {!! Form::close() !!}
        <button type="button" id="btnTampilkan" class="btn btn-primary col-xs-12 legitRipple" style="margin-top: 15px">Show Data <i class="glyphicon glyphicon-filter position-right"></i></button>
    </div>
    
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportStatusOrderTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Line Name</th>
                            <th>Date Order</th>
                            <th>Date Finish Sewing</th>
                            <th>Last Update Output</th>
                            <th>Buyer</th>
                            <th>Category</th>
                            <th>Po Buyer</th>
                            <th>Style</th>
                            <th>Article</th>
                            <th>Size</th>
                            <th>Qty Order</th>
                            <th>Qty Loading</th>
                            <th>Total Qc Output</th>
                            <th>WIP Sewing</th>
                            <th>Balanace Output</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
{!! Form::hidden('url_download_filter',route('report_status_order.export_form_filter'), array('id' => 'url_download_filter')) !!}
{!! Form::hidden('url_download_all',route('report_status_order.export_form_filter'), array('id' => 'url_download_all')) !!}

@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.pickadate-weekday').pickadate({
            firstDay: 1
        });
        $('#buttonDownloadFilter').on('click', function() {
            var url = $("#url_download_filter").val();
            $('#form').attr('action',url);
            $('#btnDownload').trigger('click');
        });
        $('#buttonDownloadAll').on('click', function() {
            console.log('asdas');
            var url = $("#url_download_all").val();
            $('#form').attr('action',url);
            $('#btnDownload').trigger('click');
        });
        var outputTable = $('#reportStatusOrderTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/status-order/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = outputTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'line',
                name: 'line',
                searchable: true,
                visible: true,
                orderable: true
            }, {
                data: 'date_order',
                name: 'date_order',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'date_finish',
                name: 'date_finish',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'last_update',
                name: 'last_update',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'buyer',
                name: 'buyer',
                searchable: false,
                orderable: false
            }, {
                data: 'category',
                name: 'category',
                searchable: true,
                orderable: true
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: false,
                orderable: false
            },{
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: true
            },{
                data: 'article',
                name: 'article',
                searchable: false,
                orderable: true
            },{
                data: 'size',
                name: 'size',
                searchable: false,
                orderable: false
            },{
                data: 'qty_ordered',
                name: 'qty_ordered',
                searchable: false,
                orderable: false
            },{
                data: 'qty_receive',
                name: 'qty_receive',
                searchable: false,
                orderable: false
            },{
                data: 'total_output',
                name: 'total_output',
                searchable: false,
                orderable: false
            },{
                data: 'wip_sewing',
                name: 'wip_sewing',
                searchable: false,
                orderable: false
            },{
                data: 'balance',
                name: 'balance',
                searchable: false,
                orderable: false
            }
        ],
    
        });
    
        var dtable = $('#reportStatusOrderTable').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    
</script>
@endsection