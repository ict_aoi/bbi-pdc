@extends('layouts.front')

@section('content-css')
    <link href="{{ mix('css/keyboard.css') }}" rel="stylesheet" type="text/css">
@endsection

@include('includes.front_navbar')
@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
            </div>
            <!--div class="heading-elements">
            </div-->
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('adminLoading.index',[$obj->factory,$obj->line]) }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Admin Loading</li>
            </ul>
            @if (isset($obj->nik))
                @if ($obj->is_scan_receive=='1')
                <div class="menu-setting">
                    <ul class="breadcrumb-elements">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                Ganti Scan
                                <span class="caret"></span>
                            </a>
        
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="menu_scan_terima active"><a href="javascript:void(0)" onclick="return menuScan('menu_scan_terima')"><i class="icon-barcode2"></i> Scan Terima</a></li>
                                <li class="divider"></li>
                                <li class="menu_scan_pindah "><a href="javascript:void(0)" onclick="return menuScan('menu_scan_pindah')"><i class="icon-barcode2"></i> Scan Pindah</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                @endif
            @endif
        </div>
    </div>
    
@endsection

@section('page-content')
    @if(isset($obj->nik))
        

        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
            <li>
                <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                    <i class="fab-icon-open icon-grid3"></i>
                    <i class="fab-icon-close icon-cross2"></i>
                </a>

                <ul class="fab-menu-inner">
                    <li>
                        <div data-fab-label="Logout">
                            <a onclick="document.getElementById('admin_loading_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-switch2"></i>
                                <form id="admin_loading_logout" action="{{ route('adminLoading.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::text('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::text('line',$obj->line, array('id' => 'line')) !!}
                                </form>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Laporan Adm. Loading">
                            <a data-toggle="modal" href="{{ route('adminLoading.report',[$obj->factory,$obj->line],$obj->line_id) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-cabinet"></i>
                            </a>
                        </div>
                    </li>
                    @if ($obj->is_scan_receive=='1')
                    <li>
                        <div data-fab-label="Laporan bundle">
                            <a  id="report_bundle" href="#" class="btn btn-default btn-rounded btn-icon btn-float btn-report-bundle">
                                <i class="icon-drawer3"></i>
                            </a>
                            
                        </div>
                    </li>
                    
                    @endif
                    
                    <li>
                        <div data-fab-label="Print Barcode">
                            <a data-toggle="modal" href="{{ route('adminLoading.printBarcode',[$obj->factory,$obj->line],$obj->line_id) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-barcode2"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Shutdown">
                            <a href="#" data-toggle="modal" data-target="#shutdownModal" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-power-cord"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        @if ($obj->is_scan_receive=='1')
            @include('admin_loading._scan_loading')
        @elseif($obj->is_scan_receive=='0')
            @include('admin_loading._input_loading')
        @endif
    @else 
        {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
    @endif
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('url_line_picklist',route('adminLoading.linePicklist'), array('id' => 'url_line_picklist')) !!}
    {!! Form::hidden('url_poreference_picklist',route('adminLoading.poreferencePickList'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('laporan_adm_loading_data','[]' , array('id' => 'laporan_adm_loading_data')) !!}
    {!! Form::hidden('url_breakdown_size_data',route('adminLoading.breakDownSizeData'), array('id' => 'url_breakdown_size_data')) !!} 
    {!! Form::hidden('url_get_output_qc',route('adminLoading.get_output_qc'), array('id' => 'url_get_output_qc')) !!} 
    {!! Form::hidden('url_delete_size',route('adminLoading.delete_size_loading'), array('id' => 'url_delete_size')) !!} 
    {!! Form::hidden('url_send_alert_andon',route('adminLoading.send_alert_andon'), array('id' => 'url_send_alert_andon')) !!} 
    {!! Form::hidden('page','home', array('id' => 'page')) !!} 
    {!! Form::hidden('buyer',$obj->buyer, array('id' => 'buyer')) !!}
    {!! Form::hidden('base_url',route('adminLoading.index',[$obj->buyer,$obj->line]), array('id' => 'base_url')) !!} 
@endsection

@section('page-modal')
    @include('admin_loading._login')
    @include('admin_loading._breakdown_size_data')
    @include('admin_loading._input_breakdown_size')
    @include('admin_loading._scan_loading_data')
    
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => 'Daftar Po Buyer',
		'placeholder'   => 'Cari berdasarkan nomor po',
    ])
    @include('form.modal_picklist', [
		'name'          => 'detailBundle',
		'title'         => 'Detail Bundle',
		'placeholder'   => 'Cari Berdasarkan nama komponen',
    ])
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar Line',
		'placeholder'   => 'Cari berdasarkan nomor line',
    ])
    @include('qc_endline._shutdown')
@endsection
{!! Form::hidden('url_ssh_reboot',route('endLine.ssh_reboot'), array('id' => 'url_ssh_reboot')) !!}
{!! Form::hidden('url_ssh_shutdown',route('endLine.ssh_shutdown'), array('id' => 'url_ssh_shutdown')) !!}
@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/admin_loading.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
@endsection
