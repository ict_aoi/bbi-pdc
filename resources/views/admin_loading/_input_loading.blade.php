<div class="panel panel-flat">
    <div class="panel-body">
        @if($obj->line == 'all')
            @include('form.picklist', [
                'label' 		=> 'Line',
                'field' 		=> 'line',
                'name' 			=> 'line',
                'placeholder' 	=> 'Silahkan Pilih Line',
                'title' 		=> 'Silahkan Pilih Line',
                'readonly'		=> true,
            ])
        @endif

        @include('form.picklist', [
            'label' 		=> 'Poreference',
            'field' 		=> 'poreference',
            'name' 			=> 'poreference',
            'placeholder' 	=> 'Silahkan Pilih Po buyer',
            'title' 		=> 'Silahkan Pilih Po buyer',
            'readonly'		=> true,
        ])
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <center>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">POREFERENCE <span id="poreference_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">ARTICLE <span id="article_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">STYLE <span id="style_label">-</span></h1></div>
            </center>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-basic table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Size</th>
                            <th>Qty Order</th>
                            <th>Total Qty Loading</th>
                            <th>Qty Loading</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="breadown_size_data_tbody">
                    </tbody>
                </table>
            </div>
        </div>

        <br/>
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('adminLoading.store',[$obj->factory,$obj->line]),
                'class'     => 'form-horizontal',
                'enctype'   => 'multipart/form-data',
                'id'        => 'form'
            ])
        !!}
            {!! Form::hidden('poreference','', array('id' => 'poreference')) !!}
            {!! Form::hidden('style','', array('id' => 'style')) !!}
            {!! Form::hidden('article','', array('id' => 'article')) !!}
            {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
            {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}
            {!! Form::hidden('line_name',($obj->line ? $obj->line : -1), array('id' => 'line_name')) !!}
            {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}

            <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
{!! Form::hidden('form_status', 'input_loading', array('id' => 'form_status')) !!}