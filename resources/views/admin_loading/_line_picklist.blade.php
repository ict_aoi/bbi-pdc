<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Line</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->name }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" 
						data-name="{{ $list->name }}" 
						>Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
