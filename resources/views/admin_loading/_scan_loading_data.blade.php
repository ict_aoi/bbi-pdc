<script type="x-tmpl-mustache" id="scan_loading_data_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>{% no %}</td>
			<td>{% style %}</td>
			<td>{% poreference %}</td>
			<td>{% size %}</td>
			<td>{% article %}</td>
			<td>{% cut_num %}</td>
			<td>{% no_sticker %}</td>
			<td>{% component_name %}</td>
			<td>{% qty %}</td>
			<td width="200px">
				<button type="button" id="deleteQty_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-circle btn-delete-qty"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
</script>