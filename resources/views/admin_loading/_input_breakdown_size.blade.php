<div id="modalInputSize" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content login-form width-400">
			<div class="modal-body">
				@include('form.text', [
					'field' 			=> 'size',
					'label' 			=> 'Size',
					'attributes' 		=> [
						'id' 			=> 'size',
						'readonly' 		=> true
					]
				])

				@include('form.text', [
					'field' 			=> 'qty_size',
					'label' 			=> 'Qty',
					'attributes' 		=> [
						'id' 			=> 'qty_size',
						'autofocus'		=> 'true',
						'autocomplete'	=> 'off'
					]
				])

				</br>
				<center id="size-keyboard">
					<div class="size-keyboard" data-target="input[name='qty_size']"></div>
				</center>

				{!! Form::hidden('size_id','', array('id' => 'size_id')) !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" id="btn-save-input-size" data-dismiss="modal">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>