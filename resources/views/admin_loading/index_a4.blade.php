<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }
    
    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }
    
    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }
    
    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }
    
    .print-friendly {
        //height: 30%;
        width: 100%;
        line-height: 0px;
        padding-top: 8px;
        //margin-left: -5px;
        //margin-right: -5px;
        font-size: 14px;
        border-right: thick solid #E0DCDC;
        border-width: 0.2px;
        margin-right: 0.5em;
    }
    
    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }
    
    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 6px;
    }
    
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -8em;
        //margin-left: 230px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }
    
    .img_barcode {
        display: block;
        padding: 0px;
    }
    
    .img_barcode > img {
        width: 166px;
        height: 40px;
    }
    
    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }
    
    .area_barcode {
        width: 25%;
    }
    
    p{
        line-height: 0.5;
    }

    .verticalLine {
        border-right: thick solid #ff0000;
    }
    
    </style>
    
    @if(isset($data))
        @foreach($data as $key => $production_size)
            <table class="print-friendly" style="width: 52%; float: left; padding-left: -10px;">
                <tr>
                    <td style="width: 5.2rem;">POREFERENCE</td>
                    <td colspan="2">: {{ $production_size->production->poreference }}</td>
                </tr>
                <tr>
                    <td>ARTICLE</td>
                    <td colspan="2">: {{ $production_size->production->article }}</td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $production_size->production->style }}</td>
                </tr>
                <tr>
                    <td>SIZE</td>
                    <td colspan="2">: {{ $production_size->size }}</td>
                </tr>
               
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
               
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($production_size->barcode, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ $production_size->barcode }} </span >
                        </div>
                    </td>
                </tr>
            </table>

            @if(($key + 1) % 2 == 0 && $key != (count($data) - 1))
            <hr style="clear: left; margin: -0.47em; border-width: 0.1px;" />
            @endif
        @endforeach
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
    </div>
    