<script type="x-tmpl-mustache" id="laporan_adm_loading_table">
	{% #list %}
		<tr>
			<td>{% no %}</td>
			<td>{% poreference %}</td>
			<td>{% article %}</td>
			<td>{% style %}</td>
			<td></td>
			<td>
				{% #is_selected %}
					<button type="button" id="uncheck_{% _id %}" data-id="{% _id %}" class="btn btn-success checkbox-unchecked-header"><i class="icon-checkbox-checked"></i></button>
				{% /is_selected %}
				{% ^is_selected %}
					<button type="button" id="check_{% _id %}" data-id="{% _id %}" class="btn btn-default checkbox-checked-header"><i class="icon-checkbox-unchecked"></i></button>
				{% /is_selected %}

				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-item"><i class="icon-trash"></i></button>
			</td>
		</tr>
		{% #details %}
			<tr>
				<td>-</td>
				<td></td>
				<td></td>
				<td></td>
				<td>{% size %}</td>
				<td>
					{% #is_selected %}
						<button type="button" id="uncheck_{% _id %}_{%_idy%}" data-id="{% _id %}" data-idy="{% _idy %}" class="btn btn-success checkbox-unchecked-line"><i class=" icon-checkbox-checked"></i></button>
					{% /is_selected %}
					{% ^is_selected %}
						<button type="button" id="check_{% _id %}_{%_idy%}" data-id="{% _id %}" data-idy="{% _idy %}" class="btn btn-default checkbox-checked-line"><i class="icon-checkbox-unchecked"></i></button>
					{% /is_selected %}
				</td>
			</tr>
		{%/details%}
	{%/list%}
</script>