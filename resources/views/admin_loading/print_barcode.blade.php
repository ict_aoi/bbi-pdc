@extends('layouts.front')

@section('content-css')
    <link href="{{ mix('css/keyboard.css') }}" rel="stylesheet" type="text/css">
@endsection

@include('includes.front_navbar')
@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
            </div>
            <!--div class="heading-elements">
            </div-->
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('adminLoading.index',[$obj->factory,$obj->line]) }}">Admin Loading</a></li>
                <li class="active">Print Barcode</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    @if(isset($obj->nik))
        <div class="panel panel-flat">
            <div class="panel-body">
                @if($obj->line == 'all')
                    @include('form.picklist', [
                        'label' 		=> 'Line',
                        'field' 		=> 'line',
                        'name' 			=> 'line',
                        'placeholder' 	=> 'Silahkan Pilih Line',
                        'title' 		=> 'Silahkan Pilih Line',
                        'readonly'		=> true,
                    ])
                @endif

                @include('form.picklist', [
                    'label' 		=> 'Poreference',
                    'field' 		=> 'poreference',
                    'name' 			=> 'poreference',
                    'placeholder' 	=> 'Silahkan Pilih Po buyer',
                    'title' 		=> 'Silahkan Pilih Po buyer',
                    'readonly'		=> true,
                ])
            </div>
        </div>

        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Poreference</th>
                                    <th>Article</th>
                                    <th>Style</th>
                                    <th>Size</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="laporan_adm_loading_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>

                <br/>
               
                    {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
                    {!! Form::hidden('laporan_adm_loading_data','[]' , array('id' => 'laporan_adm_loading_data')) !!}
                    {!! Form::hidden('url_printout',route('adminLoading.printout',[$obj->factory,$obj->line]), array('id' => 'url_printout')) !!}
                    <button type="button" class="btn bg-teal-400 col-xs-12 legitRipple" id="printout">Cetak Barcode<i class=" icon-printer position-right"></i></button>
            </div>
        </div>

        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
            <li>
                <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                    <i class="fab-icon-open icon-grid3"></i>
                    <i class="fab-icon-close icon-cross2"></i>
                </a>

                <ul class="fab-menu-inner">
                    <li>
                        <div data-fab-label="Logout">
                            <a onclick="document.getElementById('admin_loading_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-switch2"></i>
                                <form id="admin_loading_logout" action="{{ route('adminLoading.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::text('factory',$obj->factory, array('id' => 'factory')) !!}
								{!! Form::text('line',$obj->line, array('id' => 'line')) !!}
                                </form>
                            </a>
                        </div>
                    </li>
                   
                    <li>
                        <div data-fab-label="Laporan Adm. Loading">
                            <a data-toggle="modal" href="{{ route('adminLoading.report',[$obj->factory,$obj->line],$obj->line_id) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-cabinet"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Admin Loading">
                            <a data-toggle="modal" href="{{ route('adminLoading.index',[$obj->factory,$obj->line]) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-home"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    @else 
        {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
    @endif
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('url_line_picklist',route('adminLoading.linePicklist'), array('id' => 'url_line_picklist')) !!}
    {!! Form::hidden('url_poreference_picklist',route('adminLoading.poreferencePickListPerLine'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('url_laporan_adm_loading_data',route('adminLoading.reportData',['line_id' => $obj->line_id,'factory_id'=> $obj->factory_id]), array('id' => 'url_laporan_adm_loading_data')) !!}
    {!! Form::hidden('page','print_barcode', array('id' => 'page')) !!} 
    {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
    {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => 'Daftar Po Buyer',
		'placeholder'   => 'Cari berdasarkan nomor po',
    ])
@endsection

@section('page-js')
    @include('admin_loading._barcode_breakdown_size_data')
    <script src="{{ mix('js/keyboard.js') }}"></script>
    <script src="{{ mix('js/admin_loading.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
@endsection
