{!! Form::hidden('flag_menu', 'menu-scan', array('id' => 'flag_menu')) !!}
{!! Form::hidden('form_status', 'scan_loading', array('id' => 'form_status')) !!}


<div id="menu-scan">
    <div class="scan-terima">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group">
                    <div class="form-group col-lg-12">
                        <h1><label class="control-label text-semibold col-lg-2">Scan Terima</label></h1>
                        <input id="txtScanBarcode" class="form-control"  placeholder="Silahkan Scan Barcode Bundle di sini" autocomplete="off" name="scan_barcode" type="text" autofocus>
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="panel panel-flat">
            <div class="panel-body">
                <br/>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Style</th>
                                    <th>{{ ($obj->buyer=='nagai')?'No. Lot':'Po Buyer' }}</th>
                                    <th>Size</th>
                                    <th>Article</th>
                                    <th>Cutting Number</th>
                                    <th>No. Sticker</th>
                                    <th>Nama Komponen</th>
                                    <th>Qty</th>
                                    <th>Aksi</th>
                                </tr>
                                <tr>
                                </tr>
                            </thead>
                            <tbody class="scan_loading_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
                <br/>
                {!!
                    Form::open([
                        'role'      => 'form_scan',
                        'url'       => route('adminLoading.store_scan',[$obj->factory,$obj->line]),
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form_scan'
                    ])
                !!}
                    {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
                    {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}
                    {!! Form::hidden('line_name',($obj->line ? $obj->line : -1), array('id' => 'line_name')) !!}
                    {!! Form::hidden('scan_loading_data','[]' , array('class' => 'scan_loading_data')) !!}
                    <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                {!! Form::close() !!}
            </div>
        </div>
        {!! Form::hidden('url_data_cdms_scan', route('adminLoading.scan_distribusi_receive'), array('id' => 'url_data_cdms_scan')) !!}
    </div>
    <div class="scan-pindah hidden">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group">
                    <div class="form-group col-lg-12 ">
                        <h1><label class="control-label text-semibold col-lg-2">Scan Pindah</label></h1>
                        <input id="txtScanPindahBarcode" class="form-control"  placeholder="Silahkan Scan Barcode Bundle di sini" autocomplete="off" name="scan_barcode" type="text" autofocus>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-body">
                <br/>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Style</th>
                                    <th>{{ ($obj->buyer=='nagai')?'No. Lot':'Po Buyer' }}</th>
                                    <th>Size</th>
                                    <th>Article</th>
                                    <th>Cutting Number</th>
                                    <th>No. Sticker</th>
                                    <th>Nama Komponen</th>
                                    <th>Qty</th>
                                    <th>Aksi</th>
                                </tr>
                                <tr>
                                </tr>
                            </thead>
                            <tbody class="scan_loading_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
                <br/>
                {!!
                    Form::open([
                        'role'      => 'form_pindah',
                        'url'       => route('adminLoading.store_pindah',[$obj->factory,$obj->line]),
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form_pindah'
                    ])
                !!}
                    {!! Form::hidden('scan_loading_data','[]' , array('class' => 'scan_loading_data')) !!}
                    <button type="submit" class="btn bg-teal-400 col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                    {!! Form::close() !!}
                </div>
        </div>
    </div>
</div>

<div id="laporan" class="hidden">
    @include('admin_loading.report.report_bundle')
</div>