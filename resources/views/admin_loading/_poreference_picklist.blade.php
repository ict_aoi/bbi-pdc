<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Poreference</th>
		<th>Article</th>
		<th>Style</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					@if($buyer=='nagai')
						{{ strtoupper($list->poreference).'('.strtoupper($list->no_order_nagai).')'}}
					@else
						{{ strtoupper($list->poreference) }}
					@endif
				</td>
				<td>
					{{ strtoupper($list->article) }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					@if($list->article == '-')
						Artikel tidak ditemukan, silahkan hubungi tim erp untuk di update.
					@elseif($list->style == '-')
					Artikel tidak ditemukan, silahkan hubungi tim erp untuk di update.
					@else
						<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
							type="button" 
							data-buyer="{{ $list->poreference }}" 
							data-style="{{ $list->style }}" 
							data-article="{{ $list->article }}" 
							>Select</button>
					@endif
					
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
