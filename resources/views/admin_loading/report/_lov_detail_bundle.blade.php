<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Nama Komponen</th>
		<th>Tgl Terima</th>
		<th>Diterima</th>
		<th>Tgl Pindah</th>
		<th>Dipindah</th>
		<th>Status</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($data as $key => $d)
            @php
                $bundleDetail = \App\Models\DistributionDetail::active()
                                ->where([
                                    ['distribution_id',$id],
                                    ['barcode_id',$d->barcode_id],
                                    ['style_id_cdms',$d->style_detail_id],
                                ])->first();
                $dateReceive = $bundleDetail?$bundleDetail->created_at:'';
                $receiver    = $bundleDetail?$bundleDetail->create_user_name:'';
                $dateMove    = $bundleDetail?$bundleDetail->date_movement:'';
                $mover       = $bundleDetail?$bundleDetail->move_user_name:'';
                $status      = $bundleDetail?(($bundleDetail->date_movement!=null)?'Pindah':(($bundleDetail->is_movement)?'Proses Pindah':'Diterima')):'Blm Diterima';
                $color       = $bundleDetail?(($bundleDetail->date_movement!=null)?'border-warning text-warning-600':(($bundleDetail->is_movement)?'border-info text-info-600':'border-success text-success-600')):'border-grey text-grey-600';
            @endphp
			<tr>
				<td>
					{{ strtoupper($d->komponen_name) }}
				</td>
				<td>
					{{ strtoupper($dateReceive) }}
				</td>
				<td>
					{{ strtoupper($receiver) }}
				</td>
				<td>
					{{ strtoupper($dateMove) }}
				</td>
				<td>
					{{ strtoupper($mover) }}
				</td>
				<td>
					<span class="label label-flat {{ $color }} position-right">{{ $status }}</span>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $data->appends(Request::except('page'))->render() !!}
