@extends('layouts.front')

@section('content-css')
    <link href="{{ mix('css/keyboard.css') }}" rel="stylesheet" type="text/css">
@endsection

@include('includes.front_navbar')
@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <!--h1><i class="icon-grid5 position-left"></i> <span class="text-semibold">ADMIN LOADING (LINE {{ ucwords($obj->line) }})</span></h1-->
            </div>
            <!--div class="heading-elements">
                <h1 class="text-semibold no-margin">{{ ucwords($obj->name) }} <small class="display-block">{{ ucwords($obj->subdept_name) }}</small></h1>
            </div-->
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('adminLoading.index',[$obj->factory,$obj->line]) }}">Admin Loading</a></li>
                <li class="active">Laporan</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    @if(isset($obj->nik))
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group  col-lg-12">
                    <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                        <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
                    </div>
                </div>
                @include('form.picklist', [
                    'field'       => 'poreference',
                    'label'       => 'Pilih Po Buyer',
                    'name'        => 'poreference',
                    'placeholder' => 'Silahkan Pilih Po Buyer',
                    'readonly'    => 'readonly',
                    'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'  => [
                        'id'       => 'poreference',
                        'readonly' =>'readonly',
                    ]
                ])
                @include('form.select', [
                    'field'     => 'size',
                    'label'     => 'Size',
                    'options'   => [
                        '' => '-- Pilih Size --',
                    ],
                    'class'      => 'select-search',
                    'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id' => 'selectSize',
                    ]
                ])
                <button type="button" id="btnTampilkan" class="btn btn-primary col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
                <button type="button" id="btnReset" class="btn btn-warning col-xs-12 legitRipple" style="margin-top: 15px">Reset<i class="glyphicon glyphicon-filter position-right"></i></button>
            </div>
        </div> 

        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="reportAdmLoadingTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>code</th>
                                    <th>Tanggal</th>
                                    <th>Style</th>
                                    <th>Po Buyer</th>
                                    <th>Article</th>
                                    <th>Size</th>
                                    <th>Total Qty Loading</th>
                                    <th>Qty Loading</th>
                                    <th>Total Qc Output</th>
                                    <th>Qc Output</th>
                                    <th>Balance</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
            <li>
                <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                    <i class="fab-icon-open icon-grid3"></i>
                    <i class="fab-icon-close icon-cross2"></i>
                </a>

                <ul class="fab-menu-inner">
                    <li>
                        <div data-fab-label="Logout">
                            <a onclick="document.getElementById('admin_loading_logout').submit();" class="btn btn-danger btn-rounded btn-icon btn-float">
                                <i class="icon-switch2"></i>
                                <form id="admin_loading_logout" action="{{ route('adminLoading.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {!! Form::text('factory',$obj->factory, array('id' => 'factory')) !!}
								    {!! Form::text('line',$obj->line, array('id' => 'line')) !!}
                                </form>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Admin Loading">
                            <a data-toggle="modal" href="{{ route('adminLoading.index',[$obj->factory,$obj->line]) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-home"></i>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div data-fab-label="Print Barcode">
                            <a data-toggle="modal" href="{{ route('adminLoading.printBarcode',[$obj->factory,$obj->line],$obj->line_id) }}" class="btn btn-default btn-rounded btn-icon btn-float">
                                <i class="icon-barcode2"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    @else 
        {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
    @endif
    {!! Form::hidden('user_session',($obj->nik ? 1 : -1), array('id' => 'user_session')) !!}
    {!! Form::hidden('url_line_picklist',route('adminLoading.linePicklist'), array('id' => 'url_line_picklist')) !!}
    {!! Form::hidden('url_poreference_picklist',route('adminLoading.poreferencePickListPerLine'), array('id' => 'url_poreference_picklist')) !!}
    {!! Form::hidden('url_laporan_adm_loading_data',route('adminLoading.reportDataWip',['line_id' => $obj->line_id,'factory_id'=> $obj->factory_id]), array('id' => 'url_laporan_adm_loading_data')) !!}
    {!! Form::text('url_get_size',route('adminLoading.get_size',[$obj->buyer,$obj->line]), array('id' => 'url_get_size')) !!}
    {!! Form::hidden('laporan_adm_loading_data','[]' , array('id' => 'laporan_adm_loading_data')) !!}
	{!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
    {!! Form::hidden('page','laporan', array('id' => 'page')) !!} 
    {!! Form::hidden('form_status', 'laporan', array('id' => 'form_status')) !!}
    {!! Form::hidden('factory_id',($obj->factory_id ? $obj->factory_id : -1), array('id' => 'factory_id')) !!}
    {!! Form::hidden('line_id',($obj->line_id ? $obj->line_id : -1), array('id' => 'line_id')) !!}

@endsection


@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'poreference',
		'title'         => 'Daftar Po Buyer',
		'placeholder'   => 'Cari berdasarkan nomor po',
    ])
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/admin_loading.js') }}"></script>
    <script src="{{ mix('js/floating_button.js') }}"></script>
@endsection
