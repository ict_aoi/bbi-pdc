<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
            </div>
        </div>
        @include('form.select', [
            'field'     => 'poreference',
            'label'     => ($obj->buyer=='nagai')?'No Lot':'Poreference',
            'mandatory' => '*Wajib diisi',
            'options'   => [
                '' => '-- Pilih Style/Po --',
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'poreference',
            ]
        ])
        <div id="div-po" class="form-group  col-lg-12 hidden">
            <label class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">{{ ($obj->buyer=='nagai')?'No Lot':'Poreference' }}</label>
            <div class="input-group">
                <input type="text" class="form-control poreference" onclick="return callBack()" name="poreference"readonly> 
                <a href="javascript:void(0)" onclick="return clearData('poreference')" class="input-group-addon"><i class="icon-close2"></i></a>
            </div>
        </div>
        <br>
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">Style</label>
            <div class="input-group">
                <input type="text" class="form-control" name="style" id="style" readonly> 
                <a href="javascript:void(0)" class="input-group-addon" onclick="return clearData('style')"><i class="icon-close2"></i></a>
            </div>
        </div>
        <br>
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">Article</label>
            <div class="input-group">
                <input type="text" class="form-control" name="article" id="article" readonly> 
                <a href="javascript:void(0)" onclick="return clearData('article')" class="input-group-addon"><i class="icon-close2"></i></a>
            </div>
        </div>
        {!! Form::hidden('production_id','' , array('id' => 'production_id')) !!}
        @include('form.select', [
            'field'     => 'size',
            'label'     => 'Size',
            'options'   => [
                '' => '-- Pilih Size --',
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
            'attributes' => [
                'id' => 'selectSize',
            ]
        ])
        <button type="button" id="btnTampilkanBundle" class="btn btn-primary col-xs-12 legitRipple" article="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="button" id="btnResetBundle" class="btn btn-warning col-xs-12 legitRipple" style="margin-top: 15px">Reset<i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="button" id="btnBundleClear" class="btn btn-warning col-xs-12 legitRipple hidden" style="margin-top: 15px">Reset<i class="glyphicon glyphicon-filter position-right"></i></button>
        
    </div>
</div> 

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportScanBundleTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th></th>
                            <th>Tanggal</th>
                            <th>Style</th>
                            <th>{{ ($obj->buyer=='nagai')?'No. Lot':'Po Buyer' }}</th>
                            <th>Article</th>
                            <th>Size</th>
                            <th>Cutting Number</th>
                            <th>No. Sticker</th>
                            <th>Qty</th>
                            <th>Jenis</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
