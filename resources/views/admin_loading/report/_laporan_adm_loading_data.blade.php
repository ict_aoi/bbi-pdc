<script type="x-tmpl-mustache" id="laporan_adm_loading_table">
	{% #list %}
		<tr>
			<td rowspan="{% total_line %}">{% no %}</td>
			<td rowspan="{% total_line %}">{% poreference %}</td>
			<td rowspan="{% total_line %}">{% article %}</td>
			<td rowspan="{% total_line %}">{% style %}</td>
			<td rowspan="{% total_line %}">{% size %}</td>
			<td rowspan="{% total_line %}">{% qty_order %}</td>
			{% #details %}
				{% #first_index %}
					<td>{% line_name %}</td>
					<td>{% qty_per_line %}</td>
				{% /first_index %}
				{% ^first_index %}
					<tr>
					<td>{% line_name %}</td>
					<td>{% qty_per_line %}</td>
				{% /first_index %}
			{%/details%}
		</tr>
	{%/list%}
</script>