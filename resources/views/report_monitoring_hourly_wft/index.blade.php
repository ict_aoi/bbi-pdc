@extends('layouts.app',['active' => 'monitoring_hourly_wft'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Monitoring Perjam WFT</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Monitoring Perjam WFT</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" id="DownloadFilterSum"><i class="icon-file-excel"></i> Output Perjam</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('monitoring_hourly_wft.export_form_output_hourly'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
            </div>
        </div>
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownloadFilter" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="monitoringHourlyDefectTable">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">No</th>
                        <th rowspan="2" class="text-center">Code</th>
                        <th rowspan="2" class="text-center">Date</th>
                        <th rowspan="2" class="text-center">Line</th>
                        <th rowspan="2" class="text-center">Buyer</th>
                        <th rowspan="2" class="text-center">Style</th>
                        <th rowspan="2" class="text-center">Cat</th>
                        <th colspan="13" class="text-center">Jam ke</th>
                        <th rowspan="2" class="text-center">TOTAL WFT</th>
                    </tr>
                    <tr>
                        @foreach ($workingHours as $w)
                            <th>{{$w->hours}}</th>
                        @endforeach
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default'
        });
    $('#DownloadFilterSum').on('click', function() {
        var url_download_filter_summary = $("#url_download_filter_summary").val();   
        $('#form').attr('action',url_download_filter_summary);
        $('#btnDownloadFilter').trigger('click');
        
    });
    $('#DownloadFilterStyle').on('click', function() {
        var download_filter_style = $("#url_download_filter_style").val();   
        $('#form').attr('action',download_filter_style);
        $('#btnDownloadFilter').trigger('click');
        
    });
        var monitoringHourlyDefectTable = $('#monitoringHourlyDefectTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 50,
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/monitoring-perjam-wft/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = monitoringHourlyDefectTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'code',
                name: 'code',
                searchable: true,
                visible: false,
                orderable: true
            }, {
                data: 'date',
                name: 'date',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'line_name',
                name: 'line_name',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'buyer',
                name: 'buyer',
                searchable: true,
                orderable: false
            }, {
                data: 'style',
                name: 'style',
                searchable: true,
                orderable: true
            }, {
                data: 'category',
                name: 'category',
                searchable: false,
                orderable: false
            },{
                data: 'I',
                name: 'I',
                searchable: false,
                orderable: false
            },{
                data: 'II',
                name: 'II',
                searchable: false,
                orderable: false
            },{
                data: 'III',
                name: 'III',
                searchable: false,
                orderable: false
            },{
                data: 'IV',
                name: 'IV',
                searchable: false,
                orderable: false
            },{
                data: 'V',
                name: 'V',
                searchable: false,
                orderable: false
            },{
                data: 'VI',
                name: 'VI',
                searchable: false,
                orderable: false
            },{
                data: 'VII',
                name: 'VII',
                searchable: false,
                orderable: false
            },{
                data: 'VIII',
                name: 'VIII',
                searchable: false,
                orderable: false
            },{
                data: 'IX',
                name: 'IX',
                searchable: false,
                orderable: false
            },{
                data: 'X',
                name: 'X',
                searchable: false,
                orderable: false
            },{
                data: 'XI',
                name: 'XI',
                searchable: false,
                orderable: false
            },{
                data: 'XII',
                name: 'XII',
                searchable: false,
                orderable: false
            },{
                data: 'XIII',
                name: 'XIII',
                searchable: false,
                orderable: false
            },{
                data: 'total_wft',
                name: 'total_wft',
                searchable: false,
                orderable: false
            },
        ],

        });

        var dtable = $('#monitoringHourlyDefectTable').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    
</script>
@endsection