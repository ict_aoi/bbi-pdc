@extends('layouts.app',['active' => 'output_folding'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Folding Output</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Folding Output</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownloadFilter"><i class="icon-file-excel"></i> Download Filter</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('folding_output.export_form_Filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
                <span class="input-group-addon" id="clearDate"><i class="icon-close2"></i></span>
            </div>
        </div>
        
        @include('form.picklist', [
            'field'       => 'poreference',
            'label'       => 'Pilih Po Buyer',
            'name'        => 'poreference',
            'placeholder' => 'Silahkan Pilih Po Buyer',
            'readonly'    => 'readonly',
            'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
            'attributes'  => [
                'id'       => 'poreference',
                'readonly' =>'readonly',
            ]
        ])
        @include('form.select', [
            'field'     => 'size',
            'label'     => 'Size',
            'options'   => [
                '' => '-- Pilih Size --',
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'selectSize',
            ]
        ])
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownload" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="outputTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Tanggal</th>
                        <th>Nama Line</th>
                        <th>Style</th>
                        <th>Po Buyer</th>
                        <th>Article</th>
                        <th>Size</th>
                        <th>Qty Loading</th>
                        <th>Total Scan</th>
                        <th>Total Scan Hari</th>
                        <th>Balance</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'poreference',
        'title'       => 'Daftar Po Buyer',
        'placeholder' => 'Cari berdasarkan Po Buyer',
    ])
@endsection
{!! Form::hidden('url_download_filter',route('folding_output.export_form_Filter'), array('id' => 'url_download_filter')) !!}
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
    
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default'
        });
        $("#clearDate").click(function (e) { 
            e.preventDefault();
            $(".daterange-basic").val('');
        });
        $('#buttonDownloadFilter').on('click', function() {
            var url = $("#url_download_filter").val();
            $('#form').attr('action',url);
            $('#btnDownload').trigger('click');
        });
        var outputTable = $('#outputTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 50,
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/folding-output/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                        "poreference": $('#poreferenceName').val(),
                        "size"       : $('#selectSize').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = outputTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'code',
                name: 'code',
                searchable: true,
                visible: false,
                orderable: true
            }, {
                data: 'date',
                name: 'date',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'line_name',
                name: 'line_name',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: false
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: true,
                orderable: true
            }, {
                data: 'article',
                name: 'article',
                searchable: false,
                orderable: false
            },{
                data: 'size',
                name: 'size',
                searchable: false,
                orderable: true
            },{
                data: 'qty_loading',
                name: 'qty_loading',
                searchable: false,
                orderable: false
            },{
                data: 'total_scan',
                name: 'total_scan',
                searchable: false,
                orderable: false
            },{
                data: 'total_scan_day',
                name: 'total_scan_day',
                searchable: false,
                orderable: false
            },{
                data: 'balance',
                name: 'balance',
                searchable: false,
                orderable: false
            }
        ],

        });

        var dtable = $('#outputTable').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    $('#poreferenceButtonLookup ').on('click', function () {
        $('#poreferenceModal').modal();
        $('#poreferenceModal.body').removeClass('modal-open');
        $('#poreferenceModal.modal-backdrop').remove();
        lov('poreference', '/report/qc-endline-output/po-buyer-picklist?');
    });
    $('#poreferenceName').click(function () {
        $('#styleModal').modal();
        $('#poreferenceModal.body').removeClass('modal-open');
        $('#poreferenceModal.modal-backdrop').remove();
        lov('poreference', '/report/folding-output/po-buyer-picklist?');
    });
    function lov(name, url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        var buttonDel = '#' + name + 'ButtonDel';

        function itemAjax() {
            var q = $(search).val();
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');

            $.ajax({
                    url: url + '&q=' + q
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(search).val('');
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');

                    $(table).find('.btn-choose').on('click', chooseItem);
                });
        }

        function chooseItem() {
            var buyer = $(this).data('buyer');
            $('#poreferenceName').val(buyer);
            $('#poreferenceModal').modal();
            if (buyer) {
                $.ajax({
                        type: 'get',
                        url: '/report/qc-endline-output/get-size',
                        data:{
                            poreference:buyer,
                        }
                    })
                    .done(function(response) {
                        var size = response.size;

                        $("#selectSize").empty();
                        $("#selectSize").append('<option value="">-- Pilih Size --</option>');

                        $.each(size, function(size, size) {

                            $("#selectSize").append('<option value="' + size + '">' + size + '</option>');
                        });
                    })
            } else {

                $("#selectSize").empty();
                $("#selectSize").append('<option value="">-- Pilih Subcont --</option>');
            }
        }

        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                //console.log(params);
                url = $(this).attr('href') + (params == undefined ? '?' : '');

                e.preventDefault();
                itemAjax();
            });
        }

        $(search).val("");
        $(buttonSrc).unbind();
        $(search).unbind();

        $(buttonSrc).on('click', itemAjax);

        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });

        $(buttonDel).on('click', function () {
            $(item_id).val('');
            $(item_name).val('');

        });

        itemAjax();
    }
</script>
@endsection