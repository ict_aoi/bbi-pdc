@extends('layouts.app',['active' => 'output_qc_endline'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Qc Endline Jurnal Output</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Jurnal Output</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownloadFilter"><i class="icon-file-excel"></i> Download Filter</a></li>
                    <li><a href="javascript:void(0)" id="buttonDownloadAll"><i class="icon-file-excel"></i> Download All</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('qc_endline_output.export_form_Filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
                <span class="input-group-addon" id="clearDate"><i class="icon-close2"></i></span>
            </div>
        </div>
        
        @include('form.picklist', [
            'field'       => 'poreference',
            'label'       => 'Pilih Po Buyer',
            'name'        => 'poreference',
            'placeholder' => 'Silahkan Pilih Po Buyer',
            'readonly'    => 'readonly',
            'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
            'attributes'  => [
                'id'       => 'poreference',
                'readonly' =>'readonly',
            ]
        ])
        @include('form.select', [
            'field'     => 'size',
            'label'     => 'Size',
            'options'   => [
                '' => '-- Pilih Size --',
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'selectSize',
            ]
        ])
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownload" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="outputTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Tanggal</th>
                        <th>Date Finish Sewing</th>
                        <th>Nama Line</th>
                        <th>Style</th>
                        <th>Po Buyer</th>
                        <th>Article</th>
                        <th>Size</th>
                        <th>Job Order</th>
                        <th>Qty Order</th>
                        <th>Total Cek</th>
                        <th>Total Cek Hari</th>
                        <th>Balance</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'poreference',
        'title'       => 'Daftar Po Buyer',
        'placeholder' => 'Cari berdasarkan Po Buyer',
    ])
@endsection
{!! Form::hidden('url_download_all',route('qc_endline_output.export_all'), array('id' => 'url_download_all')) !!}
{!! Form::hidden('url_download_filter',route('qc_endline_output.export_form_Filter'), array('id' => 'url_download_filter')) !!}
@section('page-js')
<script src="{{ mix('js/report_qc_endline_output.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
@endsection