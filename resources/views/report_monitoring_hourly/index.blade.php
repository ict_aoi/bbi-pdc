@extends('layouts.app',['active' => 'monitoring_hourly'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Monitoring Perjam Output</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Monitoring Perjam Output</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" id="DownloadFilterSum"><i class="icon-file-excel"></i> Output Perjam</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('monitoring_hourly.export_form_output_hourly'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
            </div>
        </div>
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownloadFilter" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="monitoringHourlyTable">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">No</th>
                        <th rowspan="2" class="text-center">Code</th>
                        <th rowspan="2" class="text-center">Date</th>
                        <th rowspan="2" class="text-center">Line</th>
                        <th rowspan="2" width="50px" class="text-center">Style</th>
                        <th rowspan="2" class="text-center">Cat</th>
                        <th rowspan="2" class="text-center">SMV</th>
                        <th rowspan="2" class="text-center">MP</th>
                        <th rowspan="2" class="text-center">Plan Jam</th>
                        <th rowspan="2" class="text-center">WIP Sewing</th>
                        <th rowspan="2" class="text-center">Input Loading</th>
                        <th rowspan="2" class="text-center">Target Perhari</th>
                        <th rowspan="2" class="text-center">Target Output Perjam</th>
                        <th colspan="13" class="text-center">Jam ke</th>
                        <th rowspan="2" class="text-center">Total</th>
                        <th rowspan="2" class="text-center">Total Output Kemarin</th>
                        <th rowspan="2" class="text-center">Output mnt prd</th>
                        <th rowspan="2" class="text-center">Eff Style</th>
                        <th rowspan="2" class="text-center">Jam Ke</th>
                        <th rowspan="2" class="text-center">Eff Jam</th>
                        <th rowspan="2" class="text-center">TOTAL WFT</th>
                        <th rowspan="2" class="text-center">EFF Kemarin</th>
                    </tr>
                    <tr>
                        @foreach ($workingHours as $w)
                            <th>{{$w->hours}}</th>
                        @endforeach
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('url_download_filter_summary',route('operator_performance.export_form_filter_summary'), array('id' => 'url_download_filter_line')) !!}

@endsection
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default'
        });
    $('#DownloadFilterSum').on('click', function() {
        var url_download_filter_summary = $("#url_download_filter_summary").val();   
        $('#form').attr('action',url_download_filter_summary);
        $('#btnDownloadFilter').trigger('click');
        
    });
    $('#DownloadFilterStyle').on('click', function() {
        var download_filter_style = $("#url_download_filter_style").val();   
        $('#form').attr('action',download_filter_style);
        $('#btnDownloadFilter').trigger('click');
        
    });
        var monitoringHourlyTable = $('#monitoringHourlyTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            // pageLength: 10,
            searching: true, 
            paging: false, 
           // info: false,
            scrollY:        "500px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 6
            },
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/monitoring-perjam/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                        "grade"       : $('#selectgrade').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = monitoringHourlyTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'code',
                name: 'code',
                searchable: true,
                visible: false,
                orderable: true
            }, {
                data: 'date',
                name: 'date',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'line_name',
                name: 'line_name',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'style',
                name: 'style',
                searchable: true,
                orderable: true
            }, {
                data: 'category',
                name: 'category',
                searchable: false,
                orderable: false
            },{
                data: 'smv',
                name: 'smv',
                searchable: false,
                orderable: true
            },{
                data: 'present_sewer',
                name: 'present_sewer',
                searchable: false,
                orderable: true
            },  {
                data: 'working_hours',
                name: 'working_hours',
                searchable: false,
                orderable: false
            }, {
                data: 'wip_sewing',
                name: 'wip_sewing',
                searchable: false,
                orderable: false
            },{
                data: 'input_loading',
                name: 'input_loading',
                searchable: false,
                orderable: false
            },{
                data: 'target_daily',
                name: 'target_daily',
                searchable: false,
                orderable: false
            },{
                data: 'target_hourly',
                name: 'target_hourly',
                searchable: false,
                orderable: false
            },{
                data: 'I',
                name: 'I',
                searchable: false,
                orderable: false
            },{
                data: 'II',
                name: 'II',
                searchable: false,
                orderable: false
            },{
                data: 'III',
                name: 'III',
                searchable: false,
                orderable: false
            },{
                data: 'IV',
                name: 'IV',
                searchable: false,
                orderable: false
            },{
                data: 'V',
                name: 'V',
                searchable: false,
                orderable: false
            },{
                data: 'VI',
                name: 'VI',
                searchable: false,
                orderable: false
            },{
                data: 'VII',
                name: 'VII',
                searchable: false,
                orderable: false
            },{
                data: 'VIII',
                name: 'VIII',
                searchable: false,
                orderable: false
            },{
                data: 'IX',
                name: 'IX',
                searchable: false,
                orderable: false
            },{
                data: 'X',
                name: 'X',
                searchable: false,
                orderable: false
            },{
                data: 'XI',
                name: 'XI',
                searchable: false,
                orderable: false
            },{
                data: 'XII',
                name: 'XII',
                searchable: false,
                orderable: false
            },{
                data: 'XIII',
                name: 'XIII',
                searchable: false,
                orderable: false
            },{
                data: 'total',
                name: 'total',
                searchable: false,
                orderable: false
            },{
                data: 'total_output',
                name: 'total_output',
                searchable: false,
                orderable: false
            },{
                data: 'output_mnt',
                name: 'output_mnt',
                searchable: false,
                orderable: false
            },{
                data: 'eff_style',
                name: 'eff_style',
                searchable: false,
                orderable: false
            },{
                data: 'jam_ke',
                name: 'jam_ke',
                searchable: false,
                orderable: false
            },{
                data: 'actual_eff',
                name: 'actual_eff',
                searchable: false,
                orderable: false
            },{
                data: 'total_wft',
                name: 'total_wft',
                searchable: false,
                orderable: false
            },{
                data: 'eff_kemarin',
                name: 'eff_kemarin',
                searchable: false,
                orderable: false
            },
        ],

        });

        var dtable = $('#monitoringHourlyTable').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    
</script>
@endsection