<div id="filterFactoryModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-indigo">
                <h5 class="modal-title"><p>Filter Factory</h5>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    @include('form.select', [
                        'field' => 'factory',
                        'label' => 'Factory',
                        'mandatory' => '*Wajib diisi',
                        'default' => Auth::user()->factory_id,
                        'options' => [
                            '' => '-- Pilih Factory --',
                        ]+$factory,
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'factory',
                        ]
                    ])
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary legitRipple" onclick="return btnSaveFactory()">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
		</div>
	</div>
</div>