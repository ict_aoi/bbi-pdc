{!!
    Form::open([
        'role' => 'form',
        'url' => route('process_sewing.update',$data->id),
        'method' => 'post',
        'class' => 'form-horizontal',
        'id'=> 'form'
    ])
!!}
    
    @include('form.text', [
        'field' => 'name',
        'label' => 'Nama',
        'default' => ucwords($data->name),
        'mandatory' => '*Wajib diisi',
        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
        'attributes' => [
            'id' => 'txtNama'
        ]
    ])
    @include('form.text', [
        'field' => 'component',
        'label' => 'Komponen',
        'default' => ucwords($data->component),
        'mandatory' => '*Wajib diisi',
        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
        'attributes' => [
            'id' => 'txtComponent'
        ]
    ])
    @include('form.text', [
        'field' => 'smv',
        'label' => 'SMV',
        'default' => ucwords($data->smv),
        'mandatory' => '*Wajib diisi',
        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
        'attributes' => [
            'id' => 'txtSmv'
        ]
    ])

    <div class="text-right">
    <button type="submit" class="btn btn-primary legitRipple">update <i class="icon-arrow-right14 position-right"></i></button>
    </div>
{!! Form::close() !!}
<script>
     $('#form').submit(function(event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        $("#alert_success").trigger("click", 'Data berhasil disimpan.')
                        $('#edit_prosesModal').modal('hide');
                        $('#processTable').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                    }
                });
            }
        });
    });
</script>