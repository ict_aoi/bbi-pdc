@extends('layouts.app',['active' => 'report_output_hourly'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Output Perjam</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Output Perjam</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('report_hourly_output.export_form_filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <input type="text" class="form-control pickadate-weekday" name="date" id="txtDate" placeholder="Pilih Tanggal disini" required> 
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
            </div>
        </div>
        
        <button type="submit" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
        
    </div>
</div>
@endsection
{!! Form::hidden('url_download_filter_mont',route('report_nagai.export_form_filter_mont'), array('id' => 'url_download_filter_mont')) !!}
{!! Form::hidden('url_download_filter_day',route('report_nagai.export_form_filter'), array('id' => 'url_download_filter_mont')) !!}
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.pickadate-weekday').pickadate({
        firstDay: 1
    });
        $('#btnDownloadFilterDay').on('click', function() {
             var txtDate                  = $("#txtDate").val();
             var url_download_filter_day = $("#url_download_filter_day").val();
             if(!txtDate){
                 $("#alert_error").trigger("click", 'silahkan Pilih Tanggal Terlebih dahulu');
                return false;
                 
             }
             $('#form').attr('action',url_download_filter_day);
            $('#btnDownload').trigger('click');
            
            
        });
        $('#btnDownloadFilterMonth').on('click', function() {
            var txtDate = $("#txtDate").val();
            var url_download_filter_mont = $("#url_download_filter_mont").val(); 
             if(!txtDate){
                $("#alert_error").trigger("click", 'silahkan Pilih Tanggal Terlebih dahulu');
                return false;
             }
            $('#form').attr('action',url_download_filter_mont);
            $('#btnDownload').trigger('click');
            
        });
    });
    
</script>
@endsection