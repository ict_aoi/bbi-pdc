{!!
    Form::open([
        'role'   => 'form',
        'url'    => route('adjustment_output_qc_subtraction.store_revision'),
        'method' => 'post',
        'class'  => 'form-horizontal',
        'id'     => 'form_update'
    ])
!!}
<div class="col-md-12">
    <div class="col-md-12">
        
        @include('form.text', [  
            'field'     => 'line_name',
            'label'     => 'Nama Line',
            'default'   => ($data)?$data->line_name:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'line_name',
                'readonly' => ''
            ]
        ])
        
        @include('form.text', [  
            'field'     => 'style',
            'label'     => 'Style',
            'default'   => ($data)?$data->style:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'style',
                'readonly' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'poreference',
            'label'     => 'Po Buyer',
            'default'   => ($data)?$data->poreference:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'poreference',
                'readonly' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'article',
            'label'     => 'Article/Color',
            'default'   => ($data)?$data->article:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'article',
                'readonly' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'size',
            'label'     => 'Size',
            'default'   => ($data)?$data->size:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'size',
                'readonly' => ''
            ]
        ])
        
        @include('form.text', [  
            'field'     => 'qty_order',
            'label'     => 'QTY Order',
            'default'   => ($data)?$data->qty_order:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'qty_order',
                'readonly' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'total_output',
            'label'     => 'Total Output',
            'default'   => ($data)?$data->total_output:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'total_output',
                'readonly' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'balance',
            'label'     => 'Balance',
            'default'   => ($data)?$data->balance:'',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'balance',
                'readonly' => ''
            ]
        ])
        @include('form.date', [
            'field' => 'date',
            'label' => 'Tanggal',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'placeholder' => 'dd/mm/yyyy',
            'attributes' => [
                'id'        => 'date',
            ]
        ])
        @include('form.select', [
            'field' => 'hours',
            'label' => 'Jam Ke-',
            'options' => [
                '' => '-- Pilih Jam Ke --',
            ]+$workingHours,
            'class' => 'select-search',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'hours'
            ]
        ])
        @include('form.text', [  
            'field'     => 'revisi_output',
            'label'     => 'Revisi Output',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'revisi_output',
                'required' => ''
            ]
        ])
        @include('form.text', [  
            'field'     => 'nik',
            'label'     => 'NIK',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id'       => 'nik_label',
                'required' => ''
            ]
        ])
        
        @include('form.text', [
            'field'     => 'name_label',
            'label'     => 'Nama',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'default'   => '',
            'attributes' => [
                'id'        => 'name_label',
                'readonly'  => '',
            ]
        ])
        @include('form.textarea', [
            'field'     => 'reason',
            'label'     => 'Alasan',
            'label_col' => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'reason',
                'rows' => 5,
                'style' => 'resize: none;'
            ]
        ])
    </div>
</div>
{!! Form::hidden('production_id',($data)?$data->production_id:null, array('id' => 'production_id')) !!}
{!! Form::hidden('production_size_id',($data)?$data->production_size_id:null, array('id' => 'production_size_id')) !!}
{!! Form::hidden('qc_endline_id',($data)?$data->qc_endline_id:null, array('id' => 'qc_endline_id')) !!}
<div class="modal-footer">
    <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
</div>
{!! Form::close() !!}
<script>
    $(document).ready(function () {
        $('input.input-date').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: true
        });
    });
    $("#revisi_output").change(function (e) { 
        e.preventDefault();
        var revisi_output = parseInt($("#revisi_output").val());
        var output_day    = parseInt($("#output_day").val());
        if (revisi_output>output_day) {
            $("#alert_error").trigger("click", 'Qty Revisi Tidak boleh lebih besar dari QTY output Sekarang');
            $("#revisi_output").val(" ");
            return false;
        }
    });
    $("#nik_label").change(function (e) { 
        e.preventDefault();
        var value = $(this).val();
        $("#name_label").val(" ");
        $.ajax({
            type: "GET",
            url: '/adjustment/penambahan-output-qc/get-sewer',
            data: {
                nik: value,
            },
            success: function (response) {
                $("#name_label").val(response.name);
                $("#nik_label").val(response.nik);
            },
            error: function (response) {
                $.unblockUI();
                if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function () {
            
        });
    });
    
    
    $('#form_update').submit(function(event) {
        event.preventDefault();
        var revisi_output = parseInt($("#revisi_output").val());
        var output_day    = parseInt($("#output_day").val());
        if (revisi_output>output_day) {
            $("#alert_error").trigger("click", 'Qty Revisi Tidak boleh lebih besar dari QTY output Sekarang');
            $("#revisi_output").val(" ");
            return false;
        }
        $('#editQtyModal').modal('hide');
        bootbox.confirm("Apakah Anda Yakin Data Akan di update ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form_update').attr('action'),
                    data: $('#form_update').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function(response) {
                        $("#alert_success").trigger("click", response.responseJSON);
                        $('#listOutputQcTable').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);
                        if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    }
                });
            }
        });
    });
</script>