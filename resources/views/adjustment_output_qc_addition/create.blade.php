@extends('layouts.app',['active' => 'adjustment-output-qc-additional'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Penambahan Output Qc</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('adjustment_output_qc_addition.index') }}" >Data</a></li>
            <li class="active">Tambah Data Qc</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                @include('form.picklist', [
                    'label' 		=> 'Line',
                    'field' 		=> 'line',
                    'name' 			=> 'line',
                    'placeholder' 	=> 'Silahkan Pilih Line',
                    'title' 		=> 'Silahkan Pilih Line',
                    'readonly'		=> true,
                ])
            </div>
            <div class="col-lg-12">
                @include('form.picklist', [
                    'label' 		=> (Auth::user()->production=='nagai')?'No Lot':'Po Buyer',
                    'field' 		=> 'pobuyer',
                    'name' 			=> 'pobuyer',
                    'placeholder' 	=> (Auth::user()->production=='nagai')?'Silahkan Pilih No Lot':'Silahkan Pilih PO Buyer',
                    'title' 		=> (Auth::user()->production=='nagai')?'Silahkan Pilih No Lot':'Silahkan Pilih PO Buyer',
                    'readonly'		=> true,
                ])
            </div>
            <button type="button" id="btn_cari" class="btn bg-teal-400 col-xs-12">Cari <i class="icon-search4 position-left"></i></button>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="listOutputQcTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Line</th>
                        <th>Style</th>
                        @if (\Auth::user()->buyer=='other')
                            <th>Po Buyer</th>
                        @else
                            <th>No. Lot</th>
                        @endif
                        <th>Article</th>
                        <th>Size</th>
                        <th>Total Output</th>
                        <th>Balance</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar List Line',
		'placeholder'   => 'Cari berdasarkan Nama Line',
    ])
    @include('form.modal_picklist', [
		'name'          => 'pobuyer',
		'title'         => 'Daftar List PO Buyer',
		'placeholder'   => 'Cari berdasarkan PO Buyer',
    ])
    @include('form.modal', [
		'name'          => 'good',
		'title'         => 'Tambah Qc Endline',
    ])
    @include('form.modal', [
		'name'          => 'defect',
		'title'         => 'Defect Qc Endline',
    ])
@endsection
@section('page-js')
<script src="{{ mix('js/adjustment_output_addition.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
@endsection