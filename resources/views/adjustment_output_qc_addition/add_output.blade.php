@extends('layouts.app',['active' => 'adjustment-output-qc-additional'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Penambahan Output Qc</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li><a href="{{ route('adjustment_output_qc_addition.index') }}" >Data</a></li>
            <li><a href="{{ route('adjustment_output_qc_addition.create') }}" >Tambah Data Qc</a></li>
            <li class="active">Revisi Output</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'   => 'form',
                'url'    => route('adjustment_output_qc_addition.store_revision'),
                'method' => 'post',
                'class'  => 'form-horizontal',
                'id'     => 'form_update'
            ])
        !!}
        <div class="col-md-12">
            <div class="col-md-12">
                
                @include('form.text', [  
                    'field'     => 'line_name',
                    'label'     => 'Nama Line',
                    'default'   => ($data)?$data->line_name:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'line_name',
                        'readonly' => ''
                    ]
                ])
                
                @include('form.text', [  
                    'field'     => 'style',
                    'label'     => 'Style',
                    'default'   => ($data)?$data->style:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'style',
                        'readonly' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'poreference',
                    'label'     => 'Po Buyer',
                    'default'   => ($data)?$data->poreference:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'poreference',
                        'readonly' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'article',
                    'label'     => 'Article/Color',
                    'default'   => ($data)?$data->article:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'article',
                        'readonly' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'size',
                    'label'     => 'Size',
                    'default'   => ($data)?$data->size:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'size',
                        'readonly' => ''
                    ]
                ])
                
                @include('form.text', [  
                    'field'     => 'qty_order',
                    'label'     => 'QTY Order',
                    'default'   => ($data)?$data->qty_order:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'qty_order',
                        'readonly' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'total_output',
                    'label'     => 'Total Output',
                    'default'   => ($data)?$data->total_output:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'total_output',
                        'readonly' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'balance',
                    'label'     => 'Balance',
                    'default'   => ($data)?$data->balance:'',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'balance',
                        'readonly' => ''
                    ]
                ])
                <div class="form-group  col-xs-12">
                    <label for="date" class="control-label col-md-3 col-lg-3 col-sm-12 text-semibold">
                        Tanggal
                    </label>
                    <div class="col-md-9 col-lg-9 col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                            <input type="text" class="form-control pickadate-limits" name="date" id="txtDate" placeholder="Pilih tanggal&hellip;">
                        </div>
                    </div>
                </div>
                @include('form.select', [
                    'field' => 'hours',
                    'label' => 'Jam Ke-',
                    'options' => [
                        '' => '-- Pilih Jam Ke --',
                    ]+$workingHours,
                    'class' => 'select-search',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id' => 'hours'
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'revisi_output',
                    'label'     => 'Revisi Output',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'revisi_output',
                        'required' => ''
                    ]
                ])
                @include('form.text', [  
                    'field'     => 'nik',
                    'label'     => 'NIK',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'nik_label',
                        'required' => ''
                    ]
                ])
                
                @include('form.text', [
                    'field'     => 'name_label',
                    'label'     => 'Nama',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'default'   => '',
                    'attributes' => [
                        'id'        => 'name_label',
                        'readonly'  => '',
                    ]
                ])
                @include('form.textarea', [
                    'field'     => 'reason',
                    'label'     => 'Alasan',
                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id' => 'reason',
                        'rows' => 5,
                        'style' => 'resize: none;'
                    ]
                ])
            </div>
        </div>
        {!! Form::hidden('production_id',($data)?$data->production_id:null, array('id' => 'production_id')) !!}
        {!! Form::hidden('production_size_id',($data)?$data->production_size_id:null, array('id' => 'production_size_id')) !!}
        {!! Form::hidden('qc_endline_id',($data)?$data->qc_endline_id:null, array('id' => 'qc_endline_id')) !!}
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
		'name'          => 'line',
		'title'         => 'Daftar List Line',
		'placeholder'   => 'Cari berdasarkan Nama Line',
    ])
@endsection
@section('page-js')
<script src="{{ mix('js/adjustment_output_addition.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
@endsection