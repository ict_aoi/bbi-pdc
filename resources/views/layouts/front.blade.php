<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>PDC - Production Data Control</title>
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('content-css')
    </head>

    <body class="frontend-cover">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    @yield('page-header')
                    @include('includes.top_notif')
                    <div class="content">
                        @yield('page-content')
                        @yield('page-modal')
                        <script src="{{ mix('js/backend.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        <script src="{{ mix('js/highchart.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        @yield('page-js')
                        {{-- <div class="footer text-muted">
                        &copy; 2019. <a href="route('home')">PDC - Production Data Control</a>
                        </div> --}}
                    </div>
                 </div>
            </div>
        </div>
    </body>
</html>
