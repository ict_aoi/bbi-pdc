
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if ($model->end_job==null)
                <li><a href="#" onclick="dataEntry('{!! $data_entry !!}')" ><i class="icon-grid7"></i> Data Entry</a></li>
            @else
                <li><a href="{{$report}}" ><i class="icon-file-excel"></i> Report Time Study</a></li>
                <li><a href="{{$report_data_entry}}" ><i class="icon-file-excel"></i> data Entry</a></li>
            @endif
        </ul>
    </li>
</ul>
