@extends('layouts.app',['active' => 'actual-production-time'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-cube3 position-left"></i> <span class="text-semibold">Actual Production Time(APT)</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Actual Production Time</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
{{--  <div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownloadFilter"><i class="icon-file-excel"></i> Download Filter</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('report_status_order.export_form_filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Date Finish Sewing</label>
            <div class="input-group">
                <input type="text" class="form-control pickadate-weekday" name="date" id="txtDate" placeholder="Choose Date Finish Sewing" required> 
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
            </div>
        </div>
        
        <button type="submit" id="btnDownload" class="hidden">Download</button>
        {!! Form::close() !!}
        <button type="button" id="btnTampilkan" class="btn btn-primary col-xs-12 legitRipple" style="margin-top: 15px">Show Data <i class="glyphicon glyphicon-filter position-right"></i></button>
    </div>
    
</div>  --}}
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportApt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Line</th>
                            <th>Style</th>
                            <th>Condition Day</th>
                            <th>NIK</th>
                            <th>Name</th>
                            <th>Date Start</th>
                            <th>Finish Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-modal')
    @include('form.modal_picklist', [
        'name'          => 'dataEntry',
        'title'         => 'Laporan Data Entry',
        'placeholder'   => 'Cari berdasarkan Nama Sewer/NIK',
    ])
@endsection
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        
        var outputTable = $('#reportApt').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/actual-production-time/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = outputTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'line',
                name: 'line',
                searchable: true,
                visible: true,
                orderable: true
            }, {
                data: 'style',
                name: 'style',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'condition_day',
                name: 'condition_day',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'nik',
                name: 'nik',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'name',
                name: 'name',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'start_job',
                name: 'start_job',
                searchable: false,
                orderable: false
            }, {
                data: 'end_job',
                name: 'end_job',
                searchable: true,
                orderable: true
            }, {
                data: 'status',
                name: 'status',
                searchable: false,
                orderable: false
            },{
                data: 'action',
                name: 'action',
                searchable: false,
                orderable: true
            }
        ],
    
        });
    
        var dtable = $('#reportApt').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    
    function dataEntry(url) {
        $('#dataEntryModal').modal('show');
        datEntryPicklist('dataEntry',url);
    }

    function datEntryPicklist(name,url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
    
            var q                   = $(search).val();
            var cycle_time_batch_id = $("#cycle_time_batch_id").val();
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                url: url,
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

            });
        }
        
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
</script>
@endsection