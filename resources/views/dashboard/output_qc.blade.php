@extends('layouts.app',['active' => 'dashboard-output-qc'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard Output Qc</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Dashboard</li>
            <li class="active">Output Qc</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="panel-heading">
            <div class="heading-elements">
                <div class="form-group  col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                        <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
        
    </div>
    <div class="panel-body">
        @if (Auth::user()->production=='all'||Auth::user()->production=='nagai')
            <div class="chart-container">
                <h5 class="panel-title">Output Qc Line Nagai</h5>
                <div class="chart has-fixed-height" id="line_bar_nagai"></div>
            </div>
        @endif
        
        @if (Auth::user()->production=='all'||Auth::user()->production=='other')
            <div class="chart-container">
                <h5 class="panel-title">Output Qc Line Other</h5>
                <div class="chart has-fixed-height" id="line_bar_other"></div>
            </div>
        @endif
        <br>
    </div>
</div>
{!! Form::hidden('user_session',Auth::user()->production, array('id' => 'user_session')) !!}    
{!! Form::hidden('line_nagai','[]', array('id' => 'line_nagai')) !!}    
{!! Form::hidden('line_other','[]', array('id' => 'line_other')) !!}    
{!! Form::hidden('target_output_nagai','[]', array('id' => 'target_output_nagai')) !!}    
{!! Form::hidden('actual_output_nagai','[]', array('id' => 'actual_output_nagai')) !!}    
{!! Form::hidden('target_output_other','[]', array('id' => 'target_output_other')) !!}    
{!! Form::hidden('actual_output_other','[]', array('id' => 'actual_output_other')) !!}    
@endsection
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/echarts.js') }}"></script>
<script>
    
    $(function () {
        $('.pickadate-weekday').pickadate({
            firstDay: 1
        });
        getSummaryDataOutput();
    });
    $("#txtDate").change(function (e) {
        getSummaryDataOutput();
    });
    function getSummaryDataOutput()
    {
        var pilih_tanggal = $("#txtDate").val();
        $.ajax({
            type: 'get',
            url: '/dashboard/get-output-qc',
            data:{
                'date': pilih_tanggal
            }
        })
        .done(function(response)
        {
            $('#line_nagai').val(JSON.stringify(response.line_nagai));
            $('#target_output_nagai').val(JSON.stringify(response.target_output_nagai));
            $('#actual_output_nagai').val(JSON.stringify(response.actual_output_nagai));
            $('#target_output_other').val(JSON.stringify(response.target_output_other));
            $('#actual_output_other').val(JSON.stringify(response.actual_output_other));
            $('#line_other').val(JSON.stringify(response.line_other));
            renderBarChart();
        });  
    }

    function renderBarChart(){
        var APP_URL = {!! json_encode(url('/')) !!}
        require.config({
            paths: {
                echarts: APP_URL+'/js/visualization/echarts'
            }
        });

        require(

            // Add necessary charts
            [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/line',
            'echarts/chart/bar'
            ],


            // Charts setup
            function (ec, limitless) {
                var user_session = $('#user_session').val();
                var line_nagai   = JSON.parse($('#line_nagai').val());
                var line_other   = JSON.parse($('#line_other').val());
                var actual_output_nagai   = JSON.parse($('#actual_output_nagai').val());
                var target_output_nagai   = JSON.parse($('#target_output_nagai').val());
                var actual_output_other   = JSON.parse($('#actual_output_other').val());
                var target_output_other   = JSON.parse($('#target_output_other').val());
                if(user_session=='all'||user_session=='other'){
                    var line_bar_other = ec.init(document.getElementById('line_bar_other'), limitless);
                }
                if(user_session=='all'||user_session=='nagai'){
                    var line_bar_nagai = ec.init(document.getElementById('line_bar_nagai'), limitless);
                }
                if(user_session=='all'||user_session=='other'){
                    line_bar_other_options = {

                    // Setup grid
                        grid: {
                            x: 55,
                            x2: 45,
                            y: 35,
                            y2: 25
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis'
                        },

                        // Add legend
                        legend: {
                            data: ['Actual','Target']
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: line_other
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: '',
                                axisLabel: {
                                    formatter: '{value} Pcs'
                                }
                            },
                        ],

                        // Add series
                        series: [
                            {
                            name: 'Actual',
                            type: 'bar',
                            data: actual_output_other
                        },
                        {
                            name: 'Target',
                            type: 'line',
                            data: target_output_other
                        }
                        ]
                    };
                }
                if(user_session=='all'||user_session=='nagai'){
                    line_bar_nagai_options = {

                    // Setup grid
                        grid: {
                            x: 55,
                            x2: 45,
                            y: 35,
                            y2: 25
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis'
                        },

                        // Add legend
                        legend: {
                            data: ['Actual','Target']
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: line_nagai
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: '',
                                axisLabel: {
                                    formatter: '{value} Pcs'
                                }
                            },
                        ],

                        // Add series
                        series: [
                            {
                                name: 'Actual',
                                type: 'bar',
                                data: actual_output_nagai
                            },
                            {
                                name: 'Target',
                                type: 'line',
                                yAxisIndex: 0,
                                data: target_output_nagai
                            }
                        ]
                    };
                }
                if(user_session=='all'||user_session=='other'){
                    line_bar_other.setOption(line_bar_other_options);
                }
                if(user_session=='all'||user_session=='nagai'){
                    line_bar_nagai.setOption(line_bar_nagai_options);
                }
                
                

                // Resize charts
                // ------------------------------

                window.onresize = function () {
                    setTimeout(function (){
                        if(user_session=='all'||user_session=='other'){
                            line_bar_other.resize();
                        }
                        if(user_session=='all'||user_session=='nagai'){
                            line_bar_nagai.resize();
                        }
                        
                        
                    }, 200);
                }
            }
            
        );
    }
</script>
@endsection