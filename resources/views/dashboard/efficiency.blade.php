@extends('layouts.app',['active' => 'dashboard-efficiency'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard Efficiency</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Dashboard</li>
            <li class="active">Efficiency</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="panel-heading">
            <div class="heading-elements">
                <div class="form-group  col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                        <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
        
    </div>
    <div class="panel-body">
        @if (Auth::user()->production=='all'||Auth::user()->production=='nagai')
            <div class="chart-container">
                <h5 class="panel-title">Efficiency Line Nagai</h5>
                <div class="chart has-fixed-height" id="line_bar_nagai"></div>
            </div>
        @endif
        
        @if (Auth::user()->production=='all'||Auth::user()->production=='other')
            <div class="chart-container">
                <h5 class="panel-title">Efficiency Line Other</h5>
                <div class="chart has-fixed-height" id="line_bar_other"></div>
            </div>
        @endif
        <br>
    </div>
</div>

{!! Form::hidden('user_session',Auth::user()->production, array('id' => 'user_session')) !!}    
{!! Form::hidden('line_nagai','[]', array('id' => 'line_nagai')) !!}    
{!! Form::hidden('line_other','[]', array('id' => 'line_other')) !!}    
{!! Form::hidden('target_eff_nagai','[]', array('id' => 'target_eff_nagai')) !!}    
{!! Form::hidden('actual_eff_nagai','[]', array('id' => 'actual_eff_nagai')) !!}    
{!! Form::hidden('target_eff_other','[]', array('id' => 'target_eff_other')) !!}    
{!! Form::hidden('actual_eff_other','[]', array('id' => 'actual_eff_other')) !!}    
@endsection
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/echarts.js') }}"></script>
<script src="{{ mix('js/efficiency.js') }}"></script>
@endsection