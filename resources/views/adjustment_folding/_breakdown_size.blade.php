<script type="x-tmpl-mustache" id="breakdown_size_data_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>
				{% no %}. 
			</td>
			<td>{% line_name %}</td>
			<td>{% style %}</td>
			<td>{% poreference %}</td>
			<td>{% article %}</td>
			<td>{% size %}</td>
			<td>
				<div class="input-group col-xs-8">
					<div id="qty_order_{% _id %}">{% qty_order %}</div>
				</div>
			</td>
			<td>{% wip %}</td>
			<td>{% balance %}</td>
			<td>{% inner_pack %}</td>
			<td>{% qty_input %}</td>
			<td width="200px">
				<button type="button" id="fullQty{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-icon-anim btn-circle btn-full-qty"><i class="icon-box-add"></i></button>
			</td>
		</tr>
	{%/list%}
</script>