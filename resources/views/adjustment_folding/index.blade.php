@extends('layouts.app',['active' => 'adjustment-folding'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Adjustment Folding</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}" ><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Data</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="form-group col-lg-12 " id="">
                <label class="control-label text-semibold col-lg-2">Scan</label>
                <input id="txtScanBarcode" class="form-control"  placeholder="Silahkan Scan Barcode Karton di sini" autocomplete="off" name="scan_barcode" type="text" autofocus>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <center>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">Isi Karton <span id="inner_pack_label">-</span></h1></div>
                <div class="col-md-4"> <h1 class="no-margin text-semibold">Status <span class="label bg-danger hidden" id="status_on_label">On Proses</span><span class="label bg-blue hidden" id="status_com_label">Complete</span><span id="status_label">-</span></h1></div>
            </center>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="table-responsive">
                <table class="table datatable-basic table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Line Name</th>
                            <th>Style</th>
                            <th>Poreference</th>
                            <th>Article</th>
                            <th>Size</th>
                            <th>Qty Order</th>
                            <th>WIP</th>
                            <th>Balance</th>
                            <th>Inner Pack</th>
                            <th>Qty Input</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="breadown_size_data_tbody">
                    </tbody>
                </table>
            </div>
        </div>

        <br/>
        {!! Form::hidden('breakdown_size_data','[]' , array('id' => 'breakdown_size_data')) !!}
        {!! Form::hidden('url_breakdown_size_scan',route('adjustment_folding.breakdown_size_scan'), array('id' => 'url_breakdown_size_scan')) !!}
    </div>
</div>
@endsection
@section('page-modal')
    @include('form.modal', [
        'name' => 'edit_proses',
        'title' => 'Edit Proses'
    ])
    @include('adjustment_folding._breakdown_size')
@endsection
@section('page-js')
<script>
    list_breakdown_size_data = JSON.parse($('#breakdown_size_data').val());
    $(document).ready( function ()
    {
        document.getElementById("txtScanBarcode").focus();
        $("#txtScanBarcode").click(function (e) { 
            e.preventDefault();
            $(this).select();
        });
    });
    $("#txtScanBarcode").change(function (e) {
        e.preventDefault();
        var barcode = $(this).val();
        var url     = $('#url_breakdown_size_scan').val();
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: url,
            data: {
                barcode : barcode,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                $("#txtScanBarcode").val(' ');
                list_breakdown_size_data = [];
                render();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) 
            {
                var status = response.status;
                $("#inner_pack_label").text(response.inner_pack);
                for (i in response.data) 
                {
                    let data  = response.data[i]
                    let input = {
                        'id'                            : data.id,
                        'line_name'                     : data.line_name,
                        'production_id'                 : data.production_id,
                        'production_size_id'            : data.production_size_id,
                        'folding_package_id'            : data.folding_package_id,
                        'folding_package_detail_size_id': data.folding_package_detail_size_id,
                        'poreference'                   : data.poreference,
                        'style'                         : data.style,
                        'article'                       : data.article,
                        'size'                          : data.size,
                        'qty_order'                     : data.qty_order,
                        'balance'                       : data.balance,
                        'wip'                           : data.wip,
                        'qty_available'                 : data.qty_available,
                        'inner_pack'                    : data.inner_pack,
                        'qty_input'                     : data.qty_input,
                        'qty'                           : 0,
                        'status'                           : 0,
                        'qty_polibag'                   : data.qty_polibag,
                    }
                    list_breakdown_size_data.push(input);
                    
                }
                if (status==1) {
                    $('#status_com_label').removeClass("hidden");
                    $('#status_on_label').addClass('hidden');
                    $('#status_label').addClass('hidden');
                } else if(status==0){
                    $('#status_on_label').removeClass("hidden");
                    $('#status_label').addClass('hidden');
                    $('#status_com_label').addClass('hidden');
                }
                
            },
            error: function (response) 
            {
                $.unblockUI();
                
                if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
    
                if (response['status'] == 422)  {
                    $("#alert_warning").trigger('click', response.responseJSON.message);
                    document.getElementById("txtScanBarcode").focus();
                };
            }
        })
        .done(function ()
        {
            render();
        });
    });
    function render() 
    {
        getIndex();
        $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
        var tmpl 		= $('#breakdown_size_data_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: list_breakdown_size_data };
        var html 		= Mustache.render(tmpl, data);
        
        $('#breadown_size_data_tbody').html(html);
        bind();
    }
    function getIndex() 
    {
        $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
        for (idx in list_breakdown_size_data) 
        {
            list_breakdown_size_data[idx]['_id'] 	= idx;
            list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
        }
    }
    function bind() 
    {
        $('.btn-full-qty').on('click', fullQty);
    }
    function fullQty() {
        let data   = list_breakdown_size_data[i];
        let status = data.status;
        console.log(data);
        let wip    = parseInt(data.wip)-parseInt(data.qty_available);
        if(status==1){
            $("#alert_warning").trigger("click", "Karton sudah Komplit silahkan scan karton lain");
            return false;
        }
        if (wip<0) {
            $("#alert_warning").trigger("click", "WIP tidak boleh 0, Info Qc Untuk output");
            return false;
        }
        if (parseInt(data.qty_available)==0) {
            $("#alert_warning").trigger("click", "Qty Input tidak boleh lebih dari Qty Inner Pack");
            return false;
        }
        if (parseInt(data.status)==1) {
            $("#alert_warning").trigger("click", "karton sudah komplit silahkan karton lain");
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: '/adjustment/folding/store',
            data: {
                production_id                 : data.production_id,
                production_size_id            : data.production_size_id,
                folding_package_id            : data.folding_package_id,
                folding_package_detail_size_id: data.folding_package_detail_size_id,
                qty_input                     : data.qty_available,
            },
            cache: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                let qty    = data.qty_available;
                let status = response.status;

                data.wip           = parseInt(data.wip)-parseInt(qty);
                data.qty_available = parseInt(data.qty_available)-parseInt(qty);
                data.balance       = parseInt(data.balance)-parseInt(qty);
                data.qty_input     = parseInt(data.qty_input)+parseInt(qty);
                data.status        = status;
                if (status==1) {
                    $('#status_com_label').removeClass("hidden");
                    $('#status_on_label').addClass('hidden');
                    $('#status_label').addClass('hidden');
                    document.getElementById("txtScanBarcode").focus();
                } else if(status==0){
                    $('#status_on_label').removeClass("hidden");
                    $('#status_label').addClass('hidden');
                    $('#status_com_label').addClass('hidden');
                }
                    
                render();
            },
            error: function (response) {
                $.unblockUI();
                
                if (response.status != 200) {
                    $("#alert_warning").trigger("click",response.responseJSON.message);
                };
            }
        });
    }
    function saveQty() {
        
    }
    
</script>
@endsection