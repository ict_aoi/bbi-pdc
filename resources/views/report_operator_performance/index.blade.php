@extends('layouts.app',['active' => 'operator-performance'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Operator Performance</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Operator Performance</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" id="DownloadFilterSum"><i class="icon-file-excel"></i> Filter Opt Performance</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('operator_performance.export_form_filter_summary'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
            </div>
        </div>
        @include('form.select', [
            'field'     => 'grade',
            'label'     => 'Grade',
            'options'   => [
                '' => '-- Pilih Grade --',
                'A' => 'A',
                'B' => 'B',
                'C' => 'C'
            ],
            'class'      => 'select-search',
            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
            'attributes' => [
                'id' => 'selectgrade',
            ]
        ])
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownloadFilter" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="optPerformanceTable">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">No</th>
                        <th rowspan="2" class="text-center">Code</th>
                        <th rowspan="2" class="text-center">Tanggal</th>
                        <th rowspan="2" class="text-center">Nama Line</th>
                        <th rowspan="2" class="text-center">Nama Operator</th>
                        <th rowspan="2" class="text-center">NIK Operator</th>
                        <th rowspan="2" class="text-center">Nama Komponen</th>
                        <th rowspan="2" class="text-center">Nama Proses</th>
                        <th rowspan="2" class="text-center">SMV</th>
                        <th rowspan="2" class="text-center">Nama Mesin</th>
                        <th rowspan="2" class="text-center">Style</th>
                        <th rowspan="2" class="text-center">Total Round</th>
                        <th rowspan="2" class="text-center">Total Score</th>
                        <th rowspan="2" class="text-center">Grade</th>
                        <th colspan="8" class="text-center">Round</th>
                    </tr>
                    <tr>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('url_download_filter_summary',route('operator_performance.export_form_filter_summary'), array('id' => 'url_download_filter_line')) !!}

@endsection
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default'
        });
    $('#DownloadFilterSum').on('click', function() {
        var url_download_filter_summary = $("#url_download_filter_summary").val();   
        $('#form').attr('action',url_download_filter_summary);
        $('#btnDownloadFilter').trigger('click');
        
    });
    $('#DownloadFilterStyle').on('click', function() {
        var download_filter_style = $("#url_download_filter_style").val();   
        $('#form').attr('action',download_filter_style);
        $('#btnDownloadFilter').trigger('click');
        
    });
        var optPerformanceTable = $('#optPerformanceTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 50,
            // deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/operator-performance/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "date"       : $('#txtDate').val(),
                        "grade"       : $('#selectgrade').val(),
                    });
                }
            },
            fnCreatedRow: function(row, data, index) {
                var info = optPerformanceTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [{
                data: null,
                sortable: false,
                orderable: false,
                searchable: false
            }, {
                data: 'code',
                name: 'code',
                searchable: true,
                visible: false,
                orderable: true
            }, {
                data: 'date',
                name: 'date',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'line_name',
                name: 'line_name',
                searchable: false,
                visible: true,
                orderable: true
            }, {
                data: 'sewer_name',
                name: 'sewer_name',
                searchable: true,
                orderable: false
            }, {
                data: 'sewer_nik',
                name: 'sewer_nik',
                searchable: true,
                orderable: true
            }, {
                data: 'component',
                name: 'component',
                searchable: false,
                orderable: false
            },{
                data: 'process_name',
                name: 'process_name',
                searchable: false,
                orderable: false
            },{
                data: 'smv',
                name: 'smv',
                searchable: false,
                orderable: false
            },{
                data: 'machine_name',
                name: 'machine_name',
                searchable: false,
                orderable: true
            },{
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: true
            },{
                data: 'total_round',
                name: 'total_round',
                searchable: false,
                orderable: false
            },{
                data: 'total_score',
                name: 'total_score',
                searchable: false,
                orderable: false
            },{
                data: 'grade',
                name: 'grade',
                searchable: false,
                orderable: false
            },{
                data: 'round1',
                name: 'round1',
                searchable: false,
                orderable: false
            }, {
                data: 'round2',
                name: 'round2',
                searchable: false,
                orderable: false
            },{
                data: 'round3',
                name: 'round3',
                searchable: false,
                orderable: false
            },{
                data: 'round4',
                name: 'round4',
                searchable: false,
                orderable: false
            },{
                data: 'round5',
                name: 'round5',
                searchable: false,
                orderable: false
            },{
                name: 'round6',
                data: 'round6',
                searchable: false,
                orderable: false
            },{
                data: 'round7',
                name: 'round7',
                searchable: false,
                orderable: false
            },{
                data: 'round8',
                name: 'round8',
                searchable: false,
                orderable: false
            }
        ],

        });

        var dtable = $('#optPerformanceTable').dataTable().api();
        $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
    });
    
</script>
@endsection