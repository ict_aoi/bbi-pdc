<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Po Buyer</th>
		<th>Style</th>
		<th>Article</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($data as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->poreference) }}
				</td>
				<td>
					{{ strtoupper($list->style) }}
				</td>
				<td>
					{{ strtoupper($list->article) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
					type="button" 
					data-buyer="{{ $list->poreference."|".$list->style."|".$list->article }}" 
					>Pilih</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $data->appends(Request::except('page'))->render() !!}