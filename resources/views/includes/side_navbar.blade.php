@php
    $roles             = Session::get('roles');
    $permissions       = Session::get('permissions');
    $data_master       = ['menu-line','menu-factory','menu-process','menu-workflow','menu-daily-line','menu-qty-karton'];
    $adjustment_output = ['menu-adjustment-output-subtraction','menu-adjustment-output-addition','menu-adjustment-folding-addition'];
    $skill_matriks     = ['menu-cycle-time'];
    $report            = ['menu-qc-endline-output','menu-wft-inline-endline','menu-operator-performance','menu-tls-inline','menu-monitoring-hourly','menu-monitoring-hourly-defect','menu-actual-production-time',"menu-folding-output",'menu-hourly-output','menu-andon-supply'];
    $dashboard         = ['menu-dashboard-efficiency','menu-dashboard-output-qc','menu-andon-supply'];
    $acount_setting    = ['menu-user','menu-role','menu-permission'];
@endphp
<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>Akun saya</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Pengaturan Akun</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Keluar</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon-home4"></i> <span>Home</span></a></li>
                    @if (!empty(array_intersect($permissions, $data_master)))
                        <li class="navigation-header"><span>Data Master</span> <i class="icon-menu" title="Main pages"></i></li>
                        @if (in_array('menu-factory',$permissions))
                            <li class="{{ $active == 'factory' ? 'active' : '' }}"><a href="{{ route('master_factory.index') }}"><i class="icon-office"></i> <span>Factory</span></a></li>
                        @endif
                        @if (in_array('menu-line',$permissions))
                            <li class="{{ $active == 'line' ? 'active' : '' }}"><a href="{{ route('master_line.index') }}"><i class="icon-cabinet"></i> <span>Line</span></a></li>
                        @endif
                        @if (in_array('menu-daily-line',$permissions))
                            <li class="{{ $active == 'daily-line' ? 'active' : '' }}"><a href="{{ route('daily_line.index') }}"><i class=" icon-calendar2"></i> <span>Harian Line</span></a></li>
                        @endif
                        @if (!empty(array_intersect($permissions, ['menu-mesin','menu-process','menu-workflow'])))
                            <li class="{{ (in_array($active,['process','workflow'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="glyphicon glyphicon-list"></i> <span>Alur Proses Sewing</span></a>
                                <ul>
                                    @if (in_array('menu-mesin',$permissions))
                                        <li class="{{ $active == 'mesin' ? 'active' : '' }}"><a href="{{ route('mesin.index') }}" class="legitRipple">Mesin</a></li>
                                    @endif
                                    @if (in_array('menu-process',$permissions))
                                        <li class="{{ $active == 'process-sewing' ? 'active' : '' }}"><a href="{{ route('process_sewing.index') }}" class="legitRipple">Proses</a></li>
                                    @endif
                                    @if (in_array('menu-workflow',$permissions))
                                        <li class="{{ $active == 'work-flow' ? 'active' : '' }}"><a href="{{ route('alur_kerja.index') }}" class="legitRipple">Alur Kerja Sewing</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (in_array('menu-qty-karton',$permissions))
                            <li class="{{ $active == 'qty_karton' ? 'active' : '' }}"><a href="{{ route('qty_carton.index') }}"><i class="icon-box"></i> <span>Qty Polibag</span></a></li>
                        @endif
                    @endif
                    @if (!empty(array_intersect($permissions, $adjustment_output)))
                            <li class="navigation-header"><span>Adjustment Output</span> <i class="icon-menu" title="Main pages"></i></li>
                            @if (!empty(array_intersect($permissions, ['menu-adjustment-output-addition','menu-adjustment-folding-addition'])))
                            <li class="{{ (in_array($active,['adjustment-output-qc-additional','menu-adjustment-folding-addition'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-database-add"></i> <span>Tambah Output</span></a>
                                <ul>
                                    @if (in_array('menu-adjustment-output-addition',$permissions))
                                        <li class="{{ $active == 'adjustment-output-qc-additional' ? 'active' : '' }}"><a href="{{ route('adjustment_output_qc_addition.index') }}"><span>Qc</span></a>
                                    @endif
                                </ul>
                                <ul>
                                    @if (in_array('menu-adjustment-folding-addition',$permissions))
                                        <li class="{{ $active == 'adjustment-folding' ? 'active' : '' }}"><a href="{{ route('adjustment_folding.index') }}"><span>Folding</span></a>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (['menu-adjustment-output-subtraction'])
                            @if (!empty(array_intersect($permissions, ['menu-adjustment-output-subtraction'])))
                                <li class="{{ (in_array($active,['adjustment-outputs-qc-subtraction'])) ? 'active' : '' }}">
                                    <a href="#" class="has-ul legitRipple"><i class="icon-database-remove"></i> <span>Pengurangan Output</span></a>
                                    <ul>
                                        @if (in_array('menu-adjustment-output-subtraction',$permissions))
                                            <li class="{{ $active == 'adjustment-outputs-qc-subtraction' ? 'active' : '' }}"><a href="{{ route('adjustment_output_qc_subtraction.index') }}"><span>Qc</span></a>
                                        @endif
                                    </ul>
                                </li>
                            @endif
                        @endif
                    @endif
                    @if (!empty(array_intersect($permissions, $skill_matriks)))
                        <li class="navigation-header"><span>Skill Matriks</span> <i class="icon-menu" title="Main pages"></i></li>
                        @if (in_array('menu-cycle-time',$permissions))
                            <li class="{{ $active == 'cycle-time' ? 'active' : '' }}"><a href="{{ route('cycle_time.index') }}"><i class="icon-alarm"></i> <span>Cycle Time</span></a></li>
                        @endif
                    @endif
                    @if (!empty(array_intersect($permissions, $report)))
                        <li class="navigation-header"><span>LAPORAN</span> <i class="icon-menu" title="Main pages"></i></li>
                        @if (!empty(array_intersect($permissions, ['menu-qc-endline-output','menu-hourly-output'])))
                            <li class="{{ (in_array($active,['output_qc_endline','menu-hourly-output'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-graph"></i> <span>Qc Endline</span></a>
                                <ul>
                                    @if (in_array('menu-qc-endline-output',$permissions))
                                        <li class="{{ $active == 'report_output_hourly' ? 'active' : '' }}"><a href="{{ route('qc_endline_output.index') }}" class="legitRipple">Jurnal Output</a></li>
                                    @endif
                                </ul>
                                <ul>
                                    @if (in_array('menu-hourly-output',$permissions))
                                        <li class="{{ $active == 'output_qc_endline' ? 'active' : '' }}"><a href="{{ route('report_hourly_output.index') }}" class="legitRipple">Perjam</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{--  folding  --}}
                        @if (!empty(array_intersect($permissions, ['menu-folding-output'])))
                            <li class="{{ (in_array($active,['output_qc_endline'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-file-spreadsheet2"></i> <span>Folding</span></a>
                                <ul>
                                    @if (in_array('menu-folding-output',$permissions))
                                        <li class="{{ $active == 'output_folding' ? 'active' : '' }}"><a href="{{ route('folding_output.index') }}" class="legitRipple">Output</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if (in_array('menu-status-order',$permissions))
                            <li class="{{ $active == 'status-order' ? 'active' : '' }}"><a href="{{ route('report_status_order.index') }}"><i class="icon-cube3"></i> <span>Status Order</span></a></li>
                        @endif
                        @if (in_array('menu-andon-supply',$permissions))
                            <li class="{{ $active == 'status-order' ? 'active' : '' }}"><a href="{{ route('report_andon_supply.index') }}"><i class="icon-cube3"></i> <span>Andon Supplies</span></a></li>
                        @endif

                        @if (!empty(array_intersect($permissions, ['menu-tls-inline'])))
                            <li class="{{ (in_array($active,['tls_inline'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-insert-template"></i> <span>Qc Inline</span></a>
                                <ul>
                                    @if (in_array('menu-tls-inline',$permissions))
                                        <li class="{{ $active == 'tls_inline' ? 'active' : '' }}"><a href="{{ route('tls_inline.index') }}" class="legitRipple">TLS</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (in_array('menu-operator-performance',$permissions))
                            <li class="{{ $active == 'operator-performance' ? 'active' : '' }}"><a href="{{ route('operator_performancec.index') }}"><i class="icon-people"></i> <span>Operator Performance</span></a></li>
                        @endif
                        @if (in_array('menu-adm-line',$permissions))
                            <li class="{{ $active == 'adm-line' ? 'active' : '' }}"><a href="{{ route('report_admin_sewing.index') }}"><i class="icon-file-spreadsheet"></i> <span>ADM Line</span></a></li>
                        @endif

                        @if (!empty(array_intersect($permissions, ['menu-monitoring-hourly','menu-monitoring-hourly-sewing','menu-monitoring-hourly-wft','menu-monitoring-hourly-defect-outstanding','menu-monitoring-hourly-defect'])))
                            <li class="{{ (in_array($active,['monitoring_hourly'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-insert-template"></i><span>Monitoring PerJam</span></a>
                                <ul>
                                    @if (in_array('menu-monitoring-hourly',$permissions))
                                        <li class="{{ $active == 'monitoring_hourly' ? 'active' : '' }}"><a href="{{ route('monitoring_hourly.index') }}" class="legitRipple">Output</a></li>
                                    @endif
                                </ul>
                                <ul>
                                    @if (in_array('menu-monitoring-hourly-sewing',$permissions))
                                        <li class="{{ $active == 'monitoring_hourly_sewing' ? 'active' : '' }}"><a href="{{ route('monitoring_hourly_sewing.index') }}" class="legitRipple">Sewing</a></li>
                                    @endif
                                </ul>
                                @if (!empty(array_intersect($permissions, ['menu-monitoring-hourly-wft','menu-monitoring-hourly-defect-outstanding','menu-monitoring-hourly-defect'])))
                                <ul>
                                    <li>
                                        <a href="#">Defect</a>
                                        <ul>
                                            @if (in_array('menu-monitoring-hourly-wft',$permissions))
                                                <li class="{{ $active == 'monitoring_hourly_wft' ? 'active' : '' }}"><a href="{{ route('monitoring_hourly_wft.index') }}" class="legitRipple">WFT %</a></li>
                                            @endif
                                            @if (in_array('menu-monitoring-hourly-defect',$permissions))
                                                <li class="{{ $active == 'monitoring_hourly_defect' ? 'active' : '' }}"><a href="{{ route('monitoring_hourly_defect.index') }}" class="legitRipple">Total Defect</a></li>
                                            @endif
                                            @if (in_array('menu-monitoring-hourly-defect-outstanding',$permissions))
                                                <li class="{{ $active == 'monitoring_hourly_defect_outstanding' ? 'active' : '' }}"><a href="{{ route('monitoring_hourly_defect_outstanding.index') }}" class="legitRipple">Total Defect Outstanding</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </li>
                        @endif

                        @if (!empty(array_intersect($permissions, ['menu-wft-inline-endline','menu-nagai-endline'])))
                            <li class="{{ (in_array($active,['wft_inline_endline','menu-nagai-endline'])) ? 'active' : '' }}">
                                <a href="#" class="has-ul legitRipple"><i class="icon-stats-dots"></i> <span>WFT</span></a>
                                <ul>
                                    @if (in_array('menu-wft-inline-endline',$permissions))
                                        <li class="{{ $active == 'wft_inline_endline' ? 'active' : '' }}"><a href="{{ route('wft.index') }}" class="legitRipple">Inline & Endline</a></li>
                                    @endif
                                </ul>
                                <ul>
                                    @if (in_array('menu-nagai-endline',$permissions))
                                        <li class="{{ $active == 'wft_nagai_endline' ? 'active' : '' }}"><a href="{{ route('report_nagai.index') }}" class="legitRipple">Nagai</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (in_array('menu-actual-production-time',$permissions))
                            <li class="{{ $active == 'actual-production-time' ? 'active' : '' }}"><a href="{{ route('report_actual_production_time.index') }}"><i class="icon-table2"></i> <span>Actual Production Time(APT)</span></a></li>
                        @endif
                    @endif
                    @if (!empty(array_intersect($permissions, $dashboard)))
                            <li class="navigation-header"><span>Dashboard</span> <i class="icon-menu" title="Main pages"></i></li>
                            @if (!empty(array_intersect($permissions, ['menu-dashboard-efficiency','menu-dashboard-output-qc','menu-andon-supply'])))
                                <li class="{{ (in_array($active,['menu-dashboard-efficiency','menu-dashboard-output-qc'])) ? 'active' : '' }}">
                                    <a href="#" class="has-ul legitRipple"><i class="icon-stats-bars"></i> <span>Dashboard</span></a>
                                    <ul>
                                        @if (in_array('menu-dashboard-efficiency',$permissions))
                                            <li class="{{ $active == 'dashboard-efficiency' ? 'active' : '' }}"><a href="{{ route('dashboard_efficiency.efficiency') }}" class="legitRipple">Efficiency</a></li>
                                        @endif
                                    </ul>
                                    <ul>
                                        @if (in_array('menu-dashboard-output-qc',$permissions))
                                            <li class="{{ $active == 'dashboard-output-qc' ? 'active' : '' }}"><a href="{{ route('dashboard_efficiency.output_qc') }}" class="legitRipple">Output Qc</a></li>
                                        @endif
                                    </ul>
                                    <ul>
                                        @if (in_array('menu-andon-supply',$permissions))
                                            <li class="{{ $active == 'dashboard-andon-supply' ? 'active' : '' }}"><a href="{{ route('report_andon_supply.andon_supply') }}" class="legitRipple">Andon Supply</a></li>
                                        @endif
                                    </ul>
                                </li>
                            @endif
                    @endif
                    @if (!empty(array_intersect($permissions, $acount_setting)))
                        <li class="navigation-header"><span>Akun Setting</span> <i class="icon-menu" title="Main pages"></i></li>
                            @if (!empty(array_intersect($permissions, ['menu-user','menu-role','menu-permission'])))
                                <li class="{{ (in_array($active,['permission','role','user'])) ? 'active' : '' }}">
                                    <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>Pengelolaan Pengguna</span></a>
                                    <ul>
                                        @if (in_array('menu-permission',$permissions))
                                            <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Izin Akses</a></li>
                                        @endif
                                    </ul>
                                    <ul>
                                        @if (in_array('menu-role',$permissions))
                                            <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Pengelompakan Data</a></li>
                                        @endif
                                    </ul>
                                    <ul>
                                        @if (in_array('menu-user',$permissions))
                                            <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">Pengguna</a></li>
                                        @endif
                                    </ul>
                                </li>
                            @endif
                    @endif

                    <!-- END OF MASTER DATA -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
