<div class="navbar navbar-inverse navbar-fixed-top bg-indigo">
    <div class="navbar-header">
        <a class="navbar-brand" href="#" style="color:#fff"><i class="icon-grid5 position-left"></i>{{ ucwords($obj->header_name) }}</a>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <div class="navbar-right">
            <p class="navbar-text">{{ ucwords($obj->name)." - ".ucwords($obj->subdept_name) }}</p>
            <p class="navbar-text"><span class="label bg-success-400">Online</span></p>
        </div>
    </div>
</div>