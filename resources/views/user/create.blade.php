@extends('layouts.app',['active' => 'user'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Pengguna</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Pengelolaan Pengguna</li>
            <li><a href="{{ route('user.index') }}">Pengguna</a></li>
            <li class="active">Baru</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-body">
                {!!
                    Form::open([
                        'role' => 'form',
                        'url' => route('user.store'),
                        'method' => 'post',
                        'class' => 'form-horizontal',
                        'enctype' => 'multipart/form-data',
                        'id'=> 'form'
                    ])
                !!}
               
                @if (Auth::user()->is_super_admin)
                    @include('form.select', [
                        'field' => 'factory',
                        'label' => 'Factory',
                        'default' => $factory_id,
                        'mandatory' => '*Wajib diisi',
                        'options' => [
                            '' => '-- Pilih Factory --',
                        ]+$factory,
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'factory',
                            'required' =>''
                        ]
                    ])
                @endif
                
                    <div class="form-group  col-lg-12">
                            <label for="nik" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                            Nik
                        </label>
                        <div class="control-input col-md-10 col-lg-10 col-sm-12">
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" id="nik" name="nik">
                                <span  class="help-block text-danger">*Wajib diisi</span>
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-base"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('form.text', [
                        'field' => 'name',
                        'label' => 'Nama',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'name',
                            'readonly' => 'readonly'
                        ]
                    ])

                    @include('form.text', [
                        'field' => 'department',
                        'label' => 'Department',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'department',
                            'readonly' => 'readonly'
                        ]
                    ])

                    @include('form.select', [
                        'field' => 'sex',
                        'label' => 'Jenis Kelamin',
                        'mandatory' => '*Wajib diisi',
                        'options' => [
                            '' => '-- Pilih Jenis Kelamin --',
                            'laki' => 'Laki',
                            'perempuan' => 'Perempuan',
                        ],
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'select_kelamin'
                        ]
                    ])
                    @include('form.select', [
                        'field' => 'production',
                        'label' => 'Production',
                        'mandatory' => '*Wajib diisi',
                        'options' => [
                            ''      => '-- Pilih Production --',
                            'nagai' => 'Nagai',
                            'other' => 'Other',
                            'all'   => 'All',
                        ],
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'select_production'
                        ]
                    ])
                    
                    @include('form.select', [
                        'field' => 'role',
                        'label' => 'Kelompok Akses',
                        'options' => [
                            '' => '-- Pilih Kelompok Akses --',
                        ]+$roles,
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'select_role'
                        ]
                    ])

                    @include('form.file', [
                        'field' => 'photo',
                        'label' => 'Upload Photo',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'help' => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
                        'attributes' => [
                            'id' => 'photo',
                            'accept' => 'image/*'
                            ]
                    ])
                    <label for="is_super_admin" class="control-label font-weight-bold col-md-2 col-lg-2 col-sm-12">
                        Super Admin
                    </label>
                    <div class="checkbox checkbox-switchery col-md-10 col-lg-10 col-sm-12 ">
                        <label>
                            <input class="switchery" name="is_super_admin" type="checkbox" value="0">
                        </label>
                    </div>
                
                    {!! Form::hidden('factory_id', $factory_id, array('id' => 'factory_id')) !!}
                    {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
                    {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary col-xs-12 legitRipple">Simpan <i class="icon-floppy-disk position-right"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold">Pemetaan Pengguna Dengan Kelompok Akses &nbsp;</span></h6>
            </div>
            <div class="panel-body">
                <table class="table datatable-basic table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kelompok Akses</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="user_role"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
{!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
{!! Form::hidden('is_super_admin', (Auth::user()->is_super_admin)?'1':'0', array('id' => 'is_super_admin')) !!}
@section('page-modal')
	@include('user._table')
@endsection

@section('page-js')
<script src="{{ mix('js/switch.js') }}"></script>
<script src="{{ mix('js/user.js') }}"></script>
@endsection
