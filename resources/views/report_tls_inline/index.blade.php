@extends('layouts.app',['active' => 'tls_inline'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">TLS INLINE</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Qc Inline</li>
            <li class="active">TLS Inline</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>

        </div>
        
    </div>
    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="javascript:void(0)" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="DownloadFilterDataEntry"><i class="icon-file-excel"></i> Download Filter</a></li>
                    <li><a href="#"><i class="icon-file-excel"></i> Download All</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('tls_inline.export_form_rft'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
            </div>
        </div>
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownloadFilter" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="tlsInlineTable">
                <thead>
                    <tr>
                        <th class="text-center" rowspan="2">DATE</th>
                        <th class="text-center" rowspan="2">LINE</th>            
                         <th class="text-center" rowspan="2">TOTAL GOOD</th>
                         <th class="text-center" rowspan="2">TOTAL DEFECT</th>
                         <th class="text-center" rowspan="2">RFT(%)</th>
                        <th class="text-center" colspan="30">DEFECT CODE</th>
                    </tr>
                    <tr>
                     @foreach ($data as $d)
                        <th>{{$d->id}}</th>
                     @endforeach
                    </tr>  
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('url_download_filter_data_entry',route('tls_inline.export_form_rft'), array('id' => 'url_download_filter_data_entry')) !!}
@endsection
@section('page-modal')
    @include('form.modal_picklist', [
        'name'        => 'poreference',
        'title'       => 'Daftar Po Buyer',
        'placeholder' => 'Cari berdasarkan Po Buyer',
    ])
@endsection
@section('page-js')
{{-- <script src="{{ mix('js/report_qc_endline_output.js') }}"></script> --}}
<script src="{{ mix('js/datepicker.js') }}"></script>
<script>
    $(document).ready(function () {
    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });
    $('#DownloadFilterDataEntry').on('click', function() {
        var url_download_filter_data_entry = $("#url_download_filter_data_entry").val();   
        $('#form').attr('action',url_download_filter_data_entry);
        $('#btnDownloadFilter').trigger('click');
        
    });
    var tlsInlineTable = $('#tlsInlineTable').DataTable({
        dom: 'Bfrtip',
        "bFilter": false,
        processing: true,
        serverSide: true,
        pageLength: 50,
        // deferRender: true,
        ajax: {
            type: 'GET',
            url: '/report/qc-inline/tls-inline/data',
            data: function(d) {
                return $.extend({}, d, {
                    "date"       : $('#txtDate').val(),
                });
            }
        },
        
        columns: [
            { data: 'date',name: 'date',searchable: false,visible: true,orderable: true }, 
            { data: 'line_name',name: 'line_name',searchable: false,visible: true,orderable: true }, 
            { data: 'total_good',name: 'total_good',searchable: false,visible: true,orderable: true }, 
            { data: 'total_defect',name: 'total_defect',searchable: false,visible: true,orderable: true },
            { data: 'rft',name: 'rft',searchable: false,visible: true},
            { data: 'defect1',name: 'defect1',visible: true },
            { data: 'defect2',name: 'defect2',visible: true },
            { data: 'defect3',name: 'defect3',visible: true },
            { data: 'defect4',name: 'defect4',visible: true },
            { data: 'defect5',name: 'defect5',visible: true },
            { data: 'defect6',name: 'defect6',visible: true },
            { data: 'defect7',name: 'defect7',visible: true },
            { data: 'defect8',name: 'defect8',visible: true },
            { data: 'defect9',name: 'defect9',visible: true },
            { data: 'defect10',name: 'defect10',visible: true },
            { data: 'defect11',name: 'defect11',visible: true },
            { data: 'defect12',name: 'defect12',visible: true },
            { data: 'defect13',name: 'defect13',visible: true },
            { data: 'defect14',name: 'defect14',visible: true },
            { data: 'defect15',name: 'defect15',visible: true },
            { data: 'defect16',name: 'defect16',visible: true },
            { data: 'defect17',name: 'defect17',visible: true },
            { data: 'defect18',name: 'defect18',visible: true },
            { data: 'defect19',name: 'defect19',visible: true },
            { data: 'defect20',name: 'defect20',visible: true },
            { data: 'defect21',name: 'defect21',visible: true },
            { data: 'defect22',name: 'defect22',visible: true },
            { data: 'defect23',name: 'defect23',visible: true },
            { data: 'defect24',name: 'defect24',visible: true },
            { data: 'defect25',name: 'defect25',visible: true },
            { data: 'defect26',name: 'defect26',visible: true },
            { data: 'defect27',name: 'defect27',visible: true },
            { data: 'defect28',name: 'defect28',visible: true },
            { data: 'defect29',name: 'defect29',visible: true },
            { data: 'defect30',name: 'defect30',visible: true } 
        ],

    });

    var dtable = $('#tlsInlineTable').dataTable().api();
    $(".dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("keyup", function(e) { // Bind our desired behavior
        // If the user pressed ENTER, search
        if (e.keyCode == 13) {
            // Call the API search function
            dtable.search(this.value).draw();
        }
        // Ensure we clear the search if they backspace far enough
        if (this.value == "") {
            dtable.search("").draw();
        }
        return;
    });
    $("#btnTampilkan").click(function (e) { 
        e.preventDefault();
        dtable.draw();
        
    });
});
</script>
@endsection