@extends('layouts.app',['active' => 'andon-supply'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Andon Supply</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">Data</li>
        </ul>
    </div>
    
</div>
@endsection
@section('page-content')
<div class="panel panel-flat panel-collapsed">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
        
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb-elements">
            <li class="dropdown open">
                <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true">
                    <i class="glyphicon glyphicon-download-alt position-left"></i>
                    Download
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="javascript:void(0)" id="buttonDownloadFilter"><i class="icon-file-excel"></i> Download</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'id' => 'form',
                'url' => route('qc_endline_output.export_form_Filter'),
                'method' => 'get',
                'class' => 'form-horizontal',
            ])
        !!}
        @if (Auth::user()->is_super_admin)
            <div class="col-lg-12">
                @include('form.select', [
                    'field' => 'factory',
                    'label' => 'Factory',
                    'mandatory' => '*Wajib diisi',
                    'default' => $factory_id,
                    'options' => [
                        '' => '-- Pilih Factory --',
                    ]+$factory,
                    'class' => 'select-search',
                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes' => [
                        'id' => 'factory',
                    ]
                ])
            </div>
        @endif
        <div class="form-group  col-lg-12">
            <label class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">Tanggal</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
                <span class="input-group-addon" id="clearDate"><i class="icon-close2"></i></span>
            </div>
        </div>
        <button type="button" id="btnTampilkan" class="btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Tampilkan <i class="glyphicon glyphicon-filter position-right"></i></button>
        <button type="submit" id="btnDownload" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportAndonTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>code</th>
                            <th>Tanggal</th>
                            <th>Line Name</th>
                            <th>Working Hours</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Duration</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
{!! Form::hidden('url_download',route('report_andon_supply.download_andon_report'), array('id' => 'url_download')) !!}
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/report_andon_supplies.js') }}"></script>
@endsection