@extends('layouts.app',['active' => 'dashboard-andon-supply'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard Andon Supply</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li>Dashboard</li>
            <li class="active">Andon Supply</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="panel-heading">
            <div class="heading-elements">
                <div class="form-group  col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar5"></i></span>
                        <input type="text" class="form-control pickadate-weekday" id="txtDate" placeholder="Pilih tanggal&hellip;">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
        
    </div>
    <div class="panel-body">
        @if (Auth::user()->production=='all'||Auth::user()->production=='nagai')
            <div class="chart-container">
                <h5 class="panel-title">Andon Supply Line Nagai</h5>
                <div class="chart has-fixed-height" id="line_bar_nagai"></div>
            </div>
        @endif
        
        @if (Auth::user()->production=='all'||Auth::user()->production=='other')
            <div class="chart-container">
                <h5 class="panel-title">Andon Supply Line Other</h5>
                <div class="chart has-fixed-height" id="line_bar_other"></div>
            </div>
        @endif
        <br>
    </div>
</div>
{{--  {!! Form::hidden('user_session',Auth::user()->production, array('id' => 'user_session')) !!}      --}}
{!! Form::text('line_nagai','[]', array('id' => 'line_nagai')) !!}    
{!! Form::text('line_other','[]', array('id' => 'line_other')) !!}    
{!! Form::hidden('target_output_nagai','[]', array('id' => 'target_output_nagai')) !!}    
{!! Form::hidden('actual_output_nagai','[]', array('id' => 'actual_output_nagai')) !!}    
{!! Form::hidden('target_output_other','[]', array('id' => 'target_output_other')) !!}    
{!! Form::hidden('actual_output_other','[]', array('id' => 'actual_output_other')) !!}
{!! Form::text('base_url',route('report_andon_supply.index'), array('id' => 'base_url')) !!}
@endsection
@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/highchart.js') }}"></script>
<script>
    
    $(function () {
        $('.pickadate-weekday').pickadate({
            firstDay: 1
        });
        getDataAndon();
    });
    $("#txtDate").change(function (e) {
        getSummaryDataOutput();
    });
    function getDataAndon()
	{
		var tanggal = $("#txtDate").val();
		var base_url = $("#base_url").val();
		$.ajax({
			type: 'get',
			url: base_url+'/data-andon-supply',
			data:{
				'date': tanggal,
			}
		})
		.done(function(response)
		{
			$('#line_nagai').val(JSON.stringify(response.line_nagai));
			$('#line_other').val(JSON.stringify(response.line_other));
			//renderChart();
		});  
	}
    function renderChart(){
        Highcharts.chart('line_bar_nagai', {
            chart: {
                // width: 1320,
                height: 600,
                backgroundColor: 'rgba(0,0,0,0)' 
                // '#FCFFC5'
            },
            title: {
                // enabled: false
                text: ' '
            },
            xAxis: {
                //categories: ['a','b','c','d','e','f'],
                categories: data_label,
                labels: {
                    style: {
                        color: '#ffffff',
                        textOutline: false 
                    }
                },
                title: {
                    style: {
                        color: '#ffffff',
                        fontWeight: 'bold',
                        fontSize: '12px',
                        textOutline: false 
                    }            
                }
            },
            yAxis: {
                title: {
                    text: 'Carton',
                    style: {
                        color: '#ffffff',
                        fontWeight: 'bold',
                        fontSize: '12px',
                        textOutline: false 
                    } 
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: '#ffffff',
                        textOutline: false 
                    }
                },
                // max: 2000,
                min: 0
            },
            legend: {
                enabled: true,
                itemStyle: {
                    color: '#ffffff'
                }   
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                // series: {
        
                    spline:{
                        dataLabels:{
                            enabled: true,
                            stacking: 'normal',
                            borderWidth: 0,
                            dataLabels: {
                            
                                allowOverlap:true,
                                enabled: true,
                                formatter: function() {return this.y},
                                formatter: function(){
                                    return (this.y!=0)?this.y:"";
                                }
                            }
                        },
                        label: {
                            enabled: false,
                        }
                    },
                    column:{
                        
                        stacking: 'normal',
                        borderWidth: 0,
                        dataLabels: {
                        
                            allowOverlap:true,
                            enabled: true,
                            formatter: function() {return this.y},
                            inside: true,
                            formatter: function(){
                                return (this.y!=0)?this.y:"";
                            }
                        }
                    }
    
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:f}</b> of total<br/>'
            },
            series: [{
                name: 'Shipping',
                type: 'column',
                data: data_shipping,
                //data: [1,2,3,4,5,6],
                dataLabels: {
                    style:{
                        textOutline: false,
                        fontSize: '17px',
                        color: 'white'
                    }
                },
                stack: 'sewing'
            },{
                name: 'Inventory',
                type: 'column',
                data: data_inventory,
                //data: [1,2,3,4,5,6],
                dataLabels: {
                    style:{
                        textOutline: false,
                        fontSize: '17px',
                        color: 'white'
                    }
                },
                stack: 'sewing'
            },{
                name: 'Sewing',
                type: 'column',
                data: data_sewing,
                //data: [1,2,3,4,5,6],
                dataLabels: {
                    style:{
                        textOutline: false,
                        fontSize: '17px',
                        color: 'white'
                    }
                },
                stack: 'sewing'
            },{
                name: 'Preparation',
                type: 'column',
                data: data_preparation,
                //data: [1,2,3,4,5,6],
                dataLabels: {
                    style:{
                        textOutline: false,
                        fontSize: '17px',
                        color: 'white'
                    }
                },
                stack: 'sewing'
            },{
                //target
                name: 'Total Package',
                type: 'spline',
                data: data_target,
                dataLabels: {
                    style:{
                        textOutline: false,
                        fontSize: '17px',
                        color: '#00ff99'
                    }
                },
                lineWidth: 5
            }],
            colors: ['#33FF57','#ffcc00','#3695F3','#FF5733','#ff00ff']
        });
    }
</script>
@endsection