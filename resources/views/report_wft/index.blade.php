@extends('layouts.app',['active' => 'wft_inline_endline'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">WFT</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class="active">WFT Inlline&Endline</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    
    
    <div class="panel-body">
        {{-- <div class="panel panel-flat"> --}}
            <div class="panel-heading">
                <h6 class="panel-title">Grand Total</h6>
                <div class="heading-elements">
                    <span class="label bg-success heading-text">Update Terakhir</span>
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i> <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="javascript:void(0)" id="DownloadFilterStyle"><i class="icon-file-excel"></i> Download Day</a></li>
                                <li><a href="javascript:void(0)" id="DownloadWeek"><i class="icon-file-excel"></i> Download Week</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-xlg text-nowrap">
                    <tbody>
                        <tr>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-blue-800 text-blue-800 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-flag4"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="ttl_defect">0</span><small class="display-block no-margin">TTL Defect Inline</small>
                                    </h5>
                                </div>
                            </td>

                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-green text-green btn-flat btn-rounded btn-xs btn-icon"><i class="icon-thumbs-down3"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="qty_defect_endline">0</span> <small class="display-block no-margin">Qty Defect Endline</small>
                                    </h5>
                                </div>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-blue text-blue btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spell-check2"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="total_inspection">0</span> <small class="display-block no-margin">Total Inspection</small>
                                    </h5>
                                </div>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-orange text-orange btn-flat btn-rounded btn-xs btn-icon"><i class="icon-clipboard"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="wft_inline">0</span> <small class="display-block no-margin">WFT Inline</small>
                                    </h5>
                                </div>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-orange text-orange btn-flat btn-rounded btn-xs btn-icon"><i class="icon-clipboard2"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="wft_endline">0</span> <small class="display-block no-margin">WFT Endline</small>
                                    </h5>
                                </div>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-orange text-orange btn-flat btn-rounded btn-xs btn-icon"><i class="icon-check"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="total_wft">0</span> <small class="display-block no-margin">Total WFT</small>
                                    </h5>
                                </div>
                            </td>
                            <td class="col-md-3">
                                <div class="media-left media-middle">
                                    <a href="#" class="btn border-orange text-orange btn-flat btn-rounded btn-xs btn-icon"><i class=" icon-newspaper2"></i></a>
                                </div>

                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">
                                        <span id="rft">0</span> <small class="display-block no-margin">RFT</small>
                                    </h5>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>	
            </div>

        {{-- </div> --}}
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            {!!
                Form::open([
                    'role' => 'form',
                    #'url' => route('wft.export_form_filter_line'),
                    'method' => 'get',
                    'id' => 'form',
                    'class' => 'form-horizontal',
                ])
            !!}
            <div class="form-group  col-lg-6">
                <label class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">Tanggal</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date" id="txtDate"> 
                    <span class="input-group-btn">
                        <button class="btn bg-teal legitRipple" type="button" id="btnShow"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </span>
                </div>
            </div>
            <button type="submit" id="btnDownloadFilter" class="hidden btn btn-default col-xs-12 legitRipple" style="margin-top: 15px">Download <i class="glyphicon glyphicon-filter position-right"></i></button>
            {!! Form::close() !!}
        </div>
        <div class="table-responsive">
            <table class="table table-xlg text-nowrap" id="wftInlineEndlineTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Date</th>
                        <th>Line Name</th>
                        <th>Buyer</th>
                        <th>Mayor Defect</th>
                        <th>TTL Defect Inline</th>
                        <th>Qty Defect Endline</th>
                        <th>Total Inspection</th>
                        <th>WFT Inline</th>
                        <th>WFT Endline</th>
                        <th>Total WFT</th>
                        <th>RFT</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('url_download_week',route('wft.export_form_week'), array('id' => 'url_download_week')) !!}
{!! Form::hidden('url_download_filter_style',route('wft.export_form_filter_style'), array('id' => 'url_download_filter_style')) !!}
@endsection
@section('page-js')
<script src="{{ mix('js/report_wft_endline_inline.js') }}"></script>
<script src="{{ mix('js/datepicker.js') }}"></script>
@endsection