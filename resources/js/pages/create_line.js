$(document).ready(function() {
    items = JSON.parse($('#items').val());
    render();
    $("#txtCode").focus();

    $('#formtambahline').submit(function(event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#formtambahline').attr('action'),
                    data: $('#formtambahline').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        document.location.href = '/master-line';
                    },
                    error: function(response) {
                        $.unblockUI();

                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    }
                });
            }
        });
    });

});
function addItem() {
    var code  = $("#txtCode").val();
    var order = $("#txtOrder").val();
    var buyer = $("#selectBuyer").val();
    var name  = $("#txtNama").val();
    var diff = checkItem(code,name);

    if (code == '') {
        $("#alert_error").trigger("click", 'Code tidak boleh kosong');
        return false;
    }
    if (buyer == '') {
        $("#alert_error").trigger("click", 'Buyer tidak boleh kosong');
        return false;
    }
    if (order == '') {
        $("#alert_error").trigger("click", 'Order tidak boleh kosong');
        return false;
    }
    if (name == '') {
        $("#alert_error").trigger("click", 'Nama Line tidak boleh kosong');
        return false;
    }
    if (!diff) {
        $("#alert_error").trigger("click", 'Data Sudah ada');
        return false;
    }
    var input = {
        'id'   : -1,
        'code' : code,
        'order': order,
        'buyer': buyer,
        'name' : name
    };
    items.push(input);
    render();
}
function checkItem(code,name) {
    for (var i in items) {
        var data = items[i];

        if (data.nama == nama)
            return false;
    }
    return true;
}
function render() {
    getIndex();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();
    Mustache.parse(tmpl);
    var data = {
        item: items
    };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
}
function bind() {
    $('.btn-edit-item').on('click', editItem);
    $('.btn-save-item').on('click', saveItem);
    $('.btn-cancel-item').on('click', cancelEdit);
    $('.btn-delete-item').on('click', deleteItem);
    document.getElementById("txtCode").focus();
    $('#btnAdd').click(function () {
        addItem();
    })
    $("#txtNama").keypress(function (e) { 
        if (e.keyCode==13) {
            e.preventDefault();
            addItem();
        }
    });
}
function getIndex() {
    for (idx in items) {
        items[idx]['_id'] = idx;
        items[idx]['no'] = parseInt(idx) + 1;
    }
}
function editItem() {
    var i = $(this).data('id');

    //add hidden div
    $('#code_' + i).addClass('hidden');
    $('#order_' + i).addClass('hidden');
    $('#buyer_' + i).addClass('hidden');
    $('#name_' + i).addClass('hidden');

    $('#editItem_' + i).addClass('hidden');
    $('#deleteItem_' + i).addClass('hidden');

    //remove hidden textbox
    $('#codeInput_' + i).removeClass('hidden');
    $('#orderInput_' + i).removeClass('hidden');
    $('#buyerInput_' + i).removeClass('hidden');
    $('#nameInput_' + i).removeClass('hidden');

    $('#simpanItem_' + i).removeClass('hidden');
    $('#cancelItem_' + i).removeClass('hidden');
}

function cancelEdit() {
    var i = $(this).data('id');

    //remove hidden div
    $('#code_' + i).removeClass('hidden');
    $('#order_' + i).removeClass('hidden');
    $('#buyer_' + i).removeClass('hidden');
    $('#name_' + i).removeClass('hidden');

    $('#editItem_' + i).removeClass('hidden');
    $('#deleteItem_' + i).removeClass('hidden');

    //add hidden textbox
    $('#codeInput_' + i).addClass('hidden');
    $('#orderInput_' + i).addClass('hidden');
    $('#buyerInput_' + i).addClass('hidden');
    $('#nameInput_' + i).addClass('hidden');

    $('#simpanItem_' + i).addClass('hidden');
    $('#cancelItem_' + i).addClass('hidden');
}
function saveItem() {
    var i = $(this).data('id');

    var code = $('#codeInput_' + i).val();
    var buyer = $('#buyerInput_' + i).val();
    var order = $('#orderInput_' + i).val();
    var name = $('#nameInput_' + i).val();
    
    if (code == '') {
        $("#alert_error").trigger("click", 'Code tidak boleh kosong');
        return false;
    }
    if (buyer == '') {
        $("#alert_error").trigger("click", 'Buyer tidak boleh kosong');
        return false;
    }
    if (order == '') {
        $("#alert_error").trigger("click", 'Order tidak boleh kosong');
        return false;
    }
    if (name == '') {
        $("#alert_error").trigger("click", 'Nama Line tidak boleh kosong');
        return false;
    }

    items[i].code = code;
    items[i].buyer = buyer;
    items[i].order = order;
    items[i].name = name;
    render();
}
function deleteItem() {
    var i = $(this).data('id');
    var getItems = items[i];
    bootbox.confirm("Apakah Item Detail ini akan di hapus ?.", function(result) {

        if (result) {
            if (getItems["id"] != -1) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                        type: "put",
                        url: '/master-line/delete-detail/'+getItems['id'],
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        success: function() {
                            $.unblockUI();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                        }
                    })
                    .done(function() {
                        $("#alert_success").trigger("click", 'Data successfully deleted');
                    });
            }
            
            items.splice(i, 1);
            render();
        }

    });

}