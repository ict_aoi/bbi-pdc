$(document).ready(function() {

    var processTable = $('#processTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/process-sewing/data',
            data: function(d) {
                return $.extend({}, d, {
                    "factory"         : $("#factory").val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = processTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'name',
            name: 'name',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'component',
            name: 'component',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'smv',
            name: 'smv',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'action',
            name: 'action',
            searchable: false,
            orderable: false
        }, ],
    
    });
    
    var dtable = $('#processTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();
    });
    function edit_proses(url) {
        $('#edit_prosesModal').modal();
        lov('edit_proses', url);
    }
    function lov(name, url) {
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        let factory = $("#factory").val();
        function itemAjax() {
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $.ajax({
                url: url,
                data:{
                    factory_id:factory
                }
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
        }
        itemAjax();
    
    }
    function hapus(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $.ajax({
            type: "put",
            url: url,
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
            }
        }).done(function($result) {
            $('#processTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil hapus');
        });
    }
    function edit_prosesClose(){
        $('#edit_prosesModal').modal('toggle');
    }
    function btnSaveFactory() {  
        $('#processTable').DataTable().ajax.reload();
        $('#filterFactoryModal').modal('hide');
    }
    function create() {  
        $('#create_prosesModal').modal();
        let baseUrl = $("#base_url").val();
        
        lov('create_proses', baseUrl+'/baru');
    }
    function create_prosesClose() { 
        $('#create_prosesModal').modal('hide');
    }