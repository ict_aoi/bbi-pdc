$(document).ready(function () {
    
    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });
    $("#clearDate").click(function (e) { 
        e.preventDefault();
        $(".daterange-basic").val('');
    });
    $('#buttonDownloadFilter').on('click', function() {
        var url = $("#url_download_filter").val();
        $('#form').attr('action',url);
        $('#btnDownload').trigger('click');
    });
    $('#buttonDownloadAll').on('click', function() {
        var url = $("#url_download_all").val();
        $('#form').attr('action',url);
        $('#btnDownload').trigger('click');
    });
    var outputTable = $('#outputTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 50,
        // deferRender: true,
        ajax: {
            type: 'GET',
            url: '/report/qc-endline-output/data',
            data: function(d) {
                return $.extend({}, d, {
                    "date"       : $('#txtDate').val(),
                    "poreference": $('#poreferenceName').val(),
                    "size"       : $('#selectSize').val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = outputTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'code',
            name: 'code',
            searchable: true,
            visible: false,
            orderable: true
        }, {
            data: 'date',
            name: 'date',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'date_finish',
            name: 'date_finish',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'line_name',
            name: 'line_name',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'style',
            name: 'style',
            searchable: false,
            orderable: false
        }, {
            data: 'poreference',
            name: 'poreference',
            searchable: true,
            orderable: true
        }, {
            data: 'article',
            name: 'article',
            searchable: false,
            orderable: false
        },{
            data: 'size',
            name: 'size',
            searchable: false,
            orderable: true
        },{
            data: 'job_order',
            name: 'job_order',
            searchable: false,
            orderable: true
        },{
            data: 'qty_order',
            name: 'qty_order',
            searchable: false,
            orderable: false
        },{
            data: 'total_output',
            name: 'total_output',
            searchable: false,
            orderable: false
        },{
            data: 'output_day',
            name: 'output_day',
            searchable: false,
            orderable: false
        },{
            data: 'balance',
            name: 'balance',
            searchable: false,
            orderable: false
        }
    ],

    });

    var dtable = $('#outputTable').dataTable().api();
    $(".dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("keyup", function(e) { // Bind our desired behavior
        // If the user pressed ENTER, search
        if (e.keyCode == 13) {
            // Call the API search function
            dtable.search(this.value).draw();
        }
        // Ensure we clear the search if they backspace far enough
        if (this.value == "") {
            dtable.search("").draw();
        }
        return;
    });
    $("#btnTampilkan").click(function (e) { 
        e.preventDefault();
        dtable.draw();
        
    });
});
$('#poreferenceButtonLookup ').on('click', function () {
    $('#poreferenceModal').modal();
    $('#poreferenceModal.body').removeClass('modal-open');
    $('#poreferenceModal.modal-backdrop').remove();
    lov('poreference', '/report/qc-endline-output/po-buyer-picklist?');
});
$('#poreferenceName').click(function () {
    $('#styleModal').modal();
    $('#poreferenceModal.body').removeClass('modal-open');
    $('#poreferenceModal.modal-backdrop').remove();
    lov('poreference', '/report/qc-endline-output/po-buyer-picklist?');
});
function lov(name, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax() {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(search).val('');
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var buyer = $(this).data('buyer');
        $('#poreferenceName').val(buyer);
        $('#poreferenceModal').modal();
        if (buyer) {
            $.ajax({
                    type: 'get',
                    url: '/report/qc-endline-output/get-size',
                    data:{
                        poreference:buyer,
                    }
                })
                .done(function(response) {
                    var size = response.size;

                    $("#selectSize").empty();
                    $("#selectSize").append('<option value="">-- Pilih Size --</option>');

                    $.each(size, function(size, size) {

                        $("#selectSize").append('<option value="' + size + '">' + size + '</option>');
                    });
                })
        } else {

            $("#selectSize").empty();
            $("#selectSize").append('<option value="">-- Pilih Subcont --</option>');
        }
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            //console.log(params);
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}