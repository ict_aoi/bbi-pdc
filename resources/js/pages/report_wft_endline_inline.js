$(document).ready(function () {
    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });
    $('#DownloadWeek').on('click', function() {
        var download_week = $("#url_download_week").val();   
        $('#form').attr('action',download_week);
        $('#btnDownloadFilter').trigger('click');
        
    });
    $('#DownloadFilterStyle').on('click', function() {
        var download_filter_style = $("#url_download_filter_style").val();   
        $('#form').attr('action',download_filter_style);
        $('#btnDownloadFilter').trigger('click');
        
    });
    var wftInlineEndlineTable = $('#wftInlineEndlineTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 50,
        // deferRender: true,
        ajax: {
            type: 'GET',
            url: '/report/wft/data',
            data: function(d) {
                return $.extend({}, d, {
                    "date"       : $('#txtDate').val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = wftInlineEndlineTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'code',
            name: 'code',
            searchable: true,
            visible: false,
            orderable: true
        }, {
            data: 'date',
            name: 'date',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'line_name',
            name: 'line_name',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'buyer',
            name: 'buyer',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'mayor_defect',
            name: 'mayor_defect',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'total_defect_inline',
            name: 'total_defect_inline',
            searchable: false,
            orderable: false
        }, {
            data: 'total_defect_endline',
            name: 'total_defect_endline',
            searchable: true,
            orderable: true
        }, {
            data: 'total_inspection',
            name: 'total_inspection',
            searchable: false,
            orderable: false
        },{
            data: 'wft_inline',
            name: 'wft_inline',
            searchable: false,
            orderable: true
        },{
            data: 'wft_endline',
            name: 'wft_endline',
            searchable: false,
            orderable: false
        },{
            data: 'total_wft',
            name: 'total_wft',
            searchable: false,
            orderable: false
        },{
            data: 'rft',
            name: 'rft',
            searchable: false,
            orderable: false
        }
    ],

    });

    var dtable = $('#wftInlineEndlineTable').dataTable().api();
    $(".dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("keyup", function(e) { // Bind our desired behavior
        // If the user pressed ENTER, search
        if (e.keyCode == 13) {
            // Call the API search function
            dtable.search(this.value).draw();
        }
        // Ensure we clear the search if they backspace far enough
        if (this.value == "") {
            dtable.search("").draw();
        }
        return;
    });
    $("#btnShow").click(function (e) { 
        e.preventDefault();
        dtable.draw();
        getGrandTotal();
        
    });
    getGrandTotal();
});
function getGrandTotal() {
    var date = $('#txtDate').val();
    $.ajax({
        url: '/report/wft/get-grand-total',
        data: {
            date: date,
        },
        
        complete: function() {
        },
        success: function (response) {
            $("#ttl_defect").text(response.output_defect_inline);
            $("#qty_defect_endline").text(response.output_defect_endline);
            $("#total_inspection").text(response.output_qc_endline);
            $("#wft_inline").text(response.wft_inline);
            $("#wft_endline").text(response.wft_endline);
            $("#total_wft").text(response.total_wft);
            $("#rft").text(response.rft);
        }
    });
}