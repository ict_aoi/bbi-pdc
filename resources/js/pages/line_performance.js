$(document).ready(function () {
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        h = checkTime(h);
        $('.time').text(h + ":" + m + ":" + s);
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
    function getRand() {
        var min = 60,max = 80;
        var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
        // alert('Wait for ' + rand + ' seconds');
        getDataDisplay(0);
        setTimeout(getRand, rand * 1000);
    }
    
    startTime();
    getRand();
});
function getDataDisplay(params) {
    var url_get_data = $("#url_get_data").val();
    var line_id      = $("#line_id").val();
    var line         = $("#line").val();
    var factory      = $("#factory").val();
    var factory_id   = $("#factory_id").val();
    $.ajax({
        url: url_get_data,
        data: {
            line_id   : line_id,
            factory_id: factory_id,
            line      : line,
            factory   : factory,
        },
        beforeSend: function() {
            if (params==1) {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            }
        },
        complete: function() {
            $.unblockUI();
        },
        success: function (response) {
            if (response.active==1) {
                $("#style").text(response.style);
                $("#smv").text(response.smv);
                $("#commitment_line").text(response.commitment_line);
                $("#cumday").text(response.cumulative_day);
                $("#man_power").text(response.man_power);
                $("#accumulation").text(response.accumulation);
                $("#current").text(response.current);
                $("#outputsewingcurrent").text(response.outputCurrentSewing);
                $("#outputqccurrent").text(response.outputCurrentQc);
                $("#outputqaccumulation").text(response.outputAccumulationQc);
                $("#outputsewingaccumulation").text(response.outputAccumulationSewing);
                $("#target_hourly").text(response.targetHourly);
                $("#target_accumulation").text(response.targetAccumulation);
                $("#targeteff").text(response.targetEff);
                $("#actualeff").html(response.actualEff);
                $("#wft").html(response.wft);
                $("#targetWft").text(response.targetWft);
                $("#runningText1").html(response.runningText);
                $("#runningText2").html(response.runningText);
            } else {
                window.open(response.active, '_self');
            }
        }
    });
}