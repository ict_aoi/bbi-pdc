list_breakdown_size_data = JSON.parse($('#breakdown_size_data').val());
    $(document).ready(function () {
        render();
    });
    $('#styleButtonLookup').click(function () 
    {
        var url        = $('#url_style_picklist').val();
        $("#styleModal").modal("show");
        stylePicklist('style',url+'?');
        
    });
    $('#styleName').click(function () {
        var url = $('#url_style_picklist').val();
        
        stylePicklist('style',url+'?');
    });
    function stylePicklist(name, url) 
    {
        var search 		= '#' + name + 'Search';
        var modal 		= '#' + name + 'Modal';
        var table 		= '#' + name + 'Table';
        var buttonSrc 	= '#' + name + 'ButtonSrc';
        $(search).val('');
        
        function itemAjax()
        {
            var q = $(search).val();
            
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');

            $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
            $(table).find('.btn-choose').on('click', chooseItem);
            });
        }

        function chooseItem()
        {
            list_breakdown_size_data=[];
            var sizes     = $(this).data('size');
            var style     = $(this).data('style');
            $('#styleName').val(style);
            $('#style').val(style);
            $('#style_label').text(style);
            for (i in sizes) 
            {
                var data  = sizes[i];
                console.log(data.f1);
                
                let check = checkItem(data.f1);
                if(check){
                    var input = {
                        'id'  : '-1',
                        'size': data.f1,
                        'qty' : 0,
                    }
                    list_breakdown_size_data.push(input);
                }
            }
            render();
        }

        function pagination()
        {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');

                e.preventDefault();
                itemAjax();
            });
        }

        $(buttonSrc).unbind();
        $(search).unbind();

        $(buttonSrc).on('click', itemAjax);

        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });

        itemAjax();
    }
    function checkItem(size) {
    
        for (var i in list_breakdown_size_data) {
            var data = list_breakdown_size_data[i];
    
            if (data.size == size)
                return false;
        }
    
        return true;
    }
    function render() 
    {
        getIndex();
        $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
        var tmpl 		= $('#breakdown_size_data_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: list_breakdown_size_data };
        var html 		= Mustache.render(tmpl, data);
        
        $('#breadown_size_data_tbody').html(html);
        bind();
    }

    function getIndex() 
    {
        for (idx in list_breakdown_size_data) 
        {
            list_breakdown_size_data[idx]['_id'] 	= idx;
            list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
        }
    }

    function bind() 
    {
        $('.btn-edit-qty').on('click', editQty);
        $('.btn-save-qty').on('click', saveQty);
        $('.btn-cancel-qty').on('click', cancelEdit);
        $('.btn-delete-qty').on('click', deleteQty);
    }

    function editQty() {
        var i = $(this).data('id');
        $('#qtyLabel_' + i).addClass('hidden');
    
        //remove hidden textbox
        $('#qtyInput_' + i).removeClass('hidden');
        $('#qtyInput_' + i).select();
    
        $('#editQty_'+i).addClass("hidden");
        $('#simpanQty_' + i).removeClass('hidden');
        $('#cancelQty_' + i).removeClass('hidden');
    }
    function saveQty() {
        var i    = $(this).data('id');
        var qty  = $('#qtyInput_' + i).val();
        var data = list_breakdown_size_data[i];
        data.qty = qty;
        $('#qtyInput_' + i).addClass('hidden');
        $('#qtyLabel_' + i).removeClass('hidden');
    
        $('#editQty_'+i).removeClass("hidden");
        $('#simpanQty_' + i).addClass('hidden');
        $('#cancelQty_' + i).addClass('hidden');
        render();
    }
    function cancelEdit() {
        var i = $(this).data('id');
        var qty  = $('#qtyLabel_' + i).val();
        var data = list_breakdown_size_data[i];
        $('#qtyInput_' + i).removeClass('hidden');
    
        //add hidden textbox
        $('#qtyLabel_' + i).addClass('hidden');
    
        $('#simpanQty_' + i).addClass('hidden');
        $('#cancelQty_' + i).addClass('hidden');
        $('#editQty_' + i).removeClass('hidden');
        qty = data.qty;
        render();
    }
    function deleteQty() {
        var i = $(this).data('id');
        var data = list_breakdown_size_data[i];
        let url = window.location.origin;
        bootbox.confirm("Apakah anda yakin data akan di hapus ?.", function(result) {
            if (result) {
                if (data.id!='-1') {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                            type: "put",
                            url: url+"/qty-karton/delete/"+data.id,
                            beforeSend: function() {
                                $.blockUI({
                                    message: '<i class="icon-spinner4 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            success: function() {
                                $.unblockUI();
                                list_breakdown_size_data.splice(i, 1);
                                render();
                            },
                            error: function(response) {
                                $.unblockUI();
                                if (response.status != 200) $("#alert_warning").trigger("click", response.responseJSON.message);
    
                            }
                        })
                        .done(function() {
                            $("#alert_success").trigger("click", 'Data successfully deleted');
                        });
                }else{
                    list_breakdown_size_data.splice(i, 1);
                    render();
                }
                
            }
        });
        
    }
    function inputAll(){
        if(list_breakdown_size_data.length>0){
            swal("Masukan Qty Polibag", {
                content: "input",
                buttons: true,
            })
            .then((value) => {
                if (parseInt(value)>0) {
                    if (value) {
                        for (var i in list_breakdown_size_data) {
                            var data = list_breakdown_size_data[i];
                            data.qty=value;
                        }
                        render();
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
                
            });
        }
    }

    $('#form-store-carton').submit(function (event){
        event.preventDefault();
        
        bootbox.confirm("Apakah anda yakin simpan data ini ?.", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: $('#form-store-carton').attr('action'),
					data:new FormData($("#form-store-carton")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Message berhasil disimpan.');
                        // document.location.href = '/qty-karton';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status != 200) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });