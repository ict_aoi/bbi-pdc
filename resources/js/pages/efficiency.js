$(function () {
    $('.pickadate-weekday').pickadate({
        firstDay: 1
    });
    getSummaryDataEfficiency();
});
$("#txtDate").change(function (e) {
    getSummaryDataEfficiency();
});
function getSummaryDataEfficiency()
{
    var pilih_tanggal = $("#txtDate").val();
    $.ajax({
        type: 'get',
        url: '/dashboard/get-efficiency',
        data:{
            'date': pilih_tanggal
        }
    })
    .done(function(response)
    {
        $('#line_nagai').val(JSON.stringify(response.line_nagai));
        $('#target_eff_nagai').val(JSON.stringify(response.target_eff_nagai));
        $('#actual_eff_nagai').val(JSON.stringify(response.actual_eff_nagai));
        $('#target_eff_other').val(JSON.stringify(response.target_eff_other));
        $('#actual_eff_other').val(JSON.stringify(response.actual_eff_other));
        $('#line_other').val(JSON.stringify(response.line_other));
        renderBarChart();
    });  
}

function renderBarChart(){
    require.config({
        paths: {
            echarts: 'js/visualization/echarts'
        }
    });

    require(

        // Add necessary charts
        [
          'echarts',
          'echarts/theme/limitless',
          'echarts/chart/line',
          'echarts/chart/bar'
        ],


        // Charts setup
        function (ec, limitless) {
            var user_session = $('#user_session').val();
            var line_nagai   = JSON.parse($('#line_nagai').val());
            var line_other   = JSON.parse($('#line_other').val());
            var actual_eff_nagai   = JSON.parse($('#actual_eff_nagai').val());
            var target_eff_nagai   = JSON.parse($('#target_eff_nagai').val());
            var actual_eff_other   = JSON.parse($('#actual_eff_other').val());
            var target_eff_other   = JSON.parse($('#target_eff_other').val());
            if(user_session=='all'||user_session=='other'){
                var line_bar_other = ec.init(document.getElementById('line_bar_other'), limitless);
            }
            if(user_session=='all'||user_session=='nagai'){
                var line_bar_nagai = ec.init(document.getElementById('line_bar_nagai'), limitless);
            }
            if(user_session=='all'||user_session=='other'){
                line_bar_other_options = {

                // Setup grid
                    grid: {
                        x: 55,
                        x2: 45,
                        y: 35,
                        y2: 25
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis'
                    },

                    // Add legend
                    legend: {
                        data: ['Actual','Target']
                    },

                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        data: line_other
                    }],

                    // Vertical axis
                    yAxis: [
                        {
                            type: 'value',
                            name: '',
                            axisLabel: {
                                formatter: '{value} %'
                            }
                        },
                    ],

                    // Add series
                    series: [
                        {
                        name: 'Actual',
                        type: 'bar',
                        data: actual_eff_other
                    },
                    {
                        name: 'Target',
                        type: 'line',
                        data: target_eff_other
                    }
                    ]
                };
            }
            if(user_session=='all'||user_session=='nagai'){
                line_bar_nagai_options = {

                // Setup grid
                    grid: {
                        x: 55,
                        x2: 45,
                        y: 35,
                        y2: 25
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis'
                    },

                    // Add legend
                    legend: {
                        data: ['Actual','Target']
                    },

                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        data: line_nagai
                    }],

                    // Vertical axis
                    yAxis: [
                        {
                            type: 'value',
                            name: '',
                            axisLabel: {
                                formatter: '{value} %'
                            }
                        },
                    ],

                    // Add series
                    series: [
                        {
                            name: 'Actual',
                            type: 'bar',
                            data: actual_eff_nagai
                        },
                        {
                            name: 'Target',
                            type: 'line',
                            yAxisIndex: 0,
                            data: target_eff_nagai
                        }
                    ]
                };
            }
            if(user_session=='all'||user_session=='other'){
                line_bar_other.setOption(line_bar_other_options);
            }
            if(user_session=='all'||user_session=='nagai'){
                line_bar_nagai.setOption(line_bar_nagai_options);
            }
            
            

            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    if(user_session=='all'||user_session=='other'){
                        line_bar_other.resize();
                    }
                    if(user_session=='all'||user_session=='nagai'){
                        line_bar_nagai.resize();
                    }
                    
                    
                }, 200);
            }
        }
        
    );
}