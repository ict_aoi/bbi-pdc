$(document).ready(function () {
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    $('#buttonDownload').on('click', function() {
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        if (start_date!='') {
            if (end_date=='') {
                $("#alert_warning").trigger("click", 'Tanggal Akhir Tidak boleh kosong');
                return false;
            }
        }
        if (end_date!='') {
            if (start_date=='') {
                $("#alert_warning").trigger("click", 'Tanggal Akhir Tidak boleh kosong');
                return false;
            }
            
        }
        $('#btnDownloadFilter').trigger('click');
    });
    var reportAdmLoadingTable = $('#reportAdmLoadingTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:20,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/report/admin-sewing/data',
            data: function(d) {
                    return $.extend({}, d, {
                        "start_date" : $('#start_date').val(),
                        "end_date"   : $('#end_date').val(),
                        "poreference": $('#poreferenceName').val(),
                        "size"       : $('#selectSize').val(),
                    });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = reportAdmLoadingTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null,sortable: false,orderable: false,searchable: false},
            {
                data: 'code',
                name: 'code',
                searchable: true,
                visible: false,
                orderable: true
            }, {
                data: 'line_name',
                name: 'line_name',
                searchable: true,
                visible: true,
            }, {
                data: 'date',
                name: 'date',
                searchable: true,
                visible: true,
            }, {
                data: 'style',
                name: 'style',
                searchable: false,
                orderable: false
            }, {
                data: 'poreference',
                name: 'poreference',
                searchable: false,
                orderable: false
            }, {
                data: 'article',
                name: 'article',
                searchable: false,
                orderable: false
            }, {
                data: 'size',
                name: 'size',
                searchable: false,
                orderable: false
            }, {
                data: 'total_qty_loading',
                name: 'total_qty_loading',
                searchable: false,
                orderable: false
            }, {
                data: 'qty_loading',
                name: 'qty_loading',
                searchable: false,
                orderable: false
            }, {
                data: 'total_qc_output',
                name: 'total_qc_output',
                searchable: false,
                orderable: false
            }, {
                data: 'qc_output',
                name: 'qc_output',
                searchable: false,
                orderable: false
            }, {
                data: 'balance',
                name: 'balance',
                searchable: false,
                orderable: false
            },
        ],
    
    });
    
    var dtable = $('#reportAdmLoadingTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    $("#btnTampilkan").click(function (e) { 
        e.preventDefault();
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        if (start_date!='') {
            if (end_date=='') {
                $("#alert_warning").trigger("click", 'Tanggal Akhir Tidak boleh kosong');
                return false;
            }
        }
        if (end_date!='') {
            if (start_date=='') {
                $("#alert_warning").trigger("click", 'Tanggal Akhir Tidak boleh kosong');
                return false;
            }
            
        }
        dtable.draw();
        
    });
    $("#btnReset").click(function (e) { 
        e.preventDefault();
        $("#start_date").val('');
        $("#end_date").val('');
        $("#poreferenceName").val('');
        $("#poreferenceName").attr("placeholder", "Silahkan Pilih Po Buyer");
        $("#selectSize").empty();
        $("#selectSize").append('<option value="">-- Pilih Size --</option>');
        dtable.draw();
    });
});
$('#poreferenceButtonLookup ').on('click', function () {
    $('#poreferenceModal').modal();
    $('#poreferenceModal.body').removeClass('modal-open');
    $('#poreferenceModal.modal-backdrop').remove();
    lov('poreference', '/report/qc-endline-output/po-buyer-picklist?');
});
$('#poreferenceName').click(function () {
    $('#styleModal').modal();
    $('#poreferenceModal.body').removeClass('modal-open');
    $('#poreferenceModal.modal-backdrop').remove();
    lov('poreference', '/report/admin-sewing/po-buyer-picklist?');
});
function lov(name, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax() {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(search).val('');
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var buyer = $(this).data('buyer');
        $('#poreferenceName').val(buyer);
        $('#poreferenceModal').modal();
        if (buyer) {
            $.ajax({
                    type: 'get',
                    url: '/report/admin-sewing/get-size',
                    data:{
                        poreference:buyer,
                    }
                })
                .done(function(response) {
                    var size = response.size;

                    $("#selectSize").empty();
                    $("#selectSize").append('<option value="">-- Pilih Size --</option>');

                    $.each(size, function(size, size) {

                        $("#selectSize").append('<option value="' + size + '">' + size + '</option>');
                    });
                })
        } else {

            $("#selectSize").empty();
            $("#selectSize").append('<option value="">-- Pilih Subcont --</option>');
        }
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            //console.log(params);
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}