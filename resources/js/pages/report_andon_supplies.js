
    $(document).ready(function () {
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default'
        });
        $("#clearDate").click(function (e) { 
            e.preventDefault();
            $(".daterange-basic").val('');
        });
        $('#buttonDownloadFilter').on('click', function() {
            var url = $("#url_download").val();
            var start_date = $("#txtDate").val();
            console.log(start_date);
            if (!start_date) {
                $("#alert_warning").trigger("click", 'Selected Date First');
                return false;
            }
            $('#form').attr('action',url);
            $('#btnDownload').trigger('click');
        });
        var reportAndonTable = $('#reportAndonTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/report/andon-supply/data',
                data: function(d) {
                        return $.extend({}, d, {
                            "date"       : $('#txtDate').val(),
                            "factory"       : $('#factory').val(),
                        });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = reportAndonTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null,sortable: false,orderable: false,searchable: false},
                {
                    data: 'code',
                    name: 'code',
                    searchable: true,
                    visible: false,
                    orderable: true
                }, {
                    data: 'date',
                    name: 'date',
                    searchable: true,
                    visible: true,
                }, {
                    data: 'name',
                    name: 'name',
                    searchable: true,
                    visible: true,
                }, {
                    data: 'working_hours',
                    name: 'working_hours',
                    searchable: false,
                    visible: true,
                }, {
                    data: 'start_time',
                    name: 'start_time',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'close_time',
                    name: 'close_time',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'duration',
                    name: 'duration',
                    searchable: false,
                    orderable: false
                },
            ],
        
        });
    
        var dtable = $('#reportAndonTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
});