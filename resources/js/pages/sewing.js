$(document).ready(function () {
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        h = checkTime(h);
        $('#time').text(h + ":" + m + ":" + s);
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }; // add zero in front of numbers < 10
        return i;
    }
    $("#plus").click(function (e) { 
        e.preventDefault();
        var counter_label = parseInt($('#counter_label').text())+1;
        var balance = parseInt($('#balance').val())-1;
        var counterIdle = parseInt($('#qtyIdle').val())+1;
        var sewing_id = $("#sewing_id").val();
        if (!sewing_id) {
            $("#alert_error").trigger("click", 'Silahkan pilih Style.');
            return false;
        }
        
        if (balance<0) {
            $("#alert_error").trigger("click", 'Style Sudah Balance.');
            return false;
        }
        $("#counter_label").text(counter_label);
        $("#qtyIdle").val(counterIdle);
        $("#balance").val(balance);
        $("#process").removeClass('hidden');
        $("#selesai").addClass("hidden");
    });
    $(document).idle({
        onIdle: function(){
            var qtyidle = parseInt($('#qtyIdle').val());
            if (qtyidle>0) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                        type: "post",
                        url: $('#form').attr('action'),
                        data: new FormData(document.getElementById("form")),
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $("#qtyIdle").val('0');
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function(response) {
                            $.unblockUI();
                            $("#selesai").removeClass('hidden');
                            $("#process").addClass("hidden");
            
                        },
                        error: function(response) {
                            $.unblockUI();
                            $('#upload_file_allocation').trigger('reset');
                            if (response['status'] == 500)
                                $("#alert_error").trigger("click", 'Please Contact ICT.');
                            
                            if (response['status'] == 422||response['status']==419)
                                $("#alert_error").trigger("click", response.responseJSON.message);
                            $("#qtyIdle").val('0');
                            get_counter();
                        }
                    })
                    .done(function() {
                        $("#qtyIdle").val('0');
                });
            }      
        },
        idle: 3000
    });
    function getRand() {
        var min = 50,max = 60;
        var qtyIdle = $("#qtyIdle").val();
        var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
        // alert('Wait for ' + rand + ' seconds');
        if (qtyIdle==0) {
            get_counter();
        }
        setTimeout(getRand, rand * 1000);
    }
    startTime();
    getRand();
});
function get_counter() {
    var line_id         = $("#line_id").val();
    var style           = $("#style").val();
    var style_default   = $("#style_default").val();
    var article         = $("#article").val();
    var url_get_counter = $("#url_get_counter").val();
    $.ajax({
        url: url_get_counter,
        data:{
            line_id      : line_id,
            style        : style,
            style_default: style_default,
            article      : article,
        }
    })
    .done(function (response) {
        $("#counter_label").text(response.counter_day);
        $("#balance").val(response.balance);
    });
}
function selectStyle() {
    var line_id    = $("#line_id").val();
    var factory_id = $("#factory_id").val();
    var url_get_style = $("#url_get_style").val();

    if (!(line_id&&factory_id)) {
        $("#alert_warning").trigger("click", 'silahkan refresh halaman');
        return false;
    }
    $("#selectStyleModal").modal();
    lov('selectStyle',url_get_style);
}
function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';
    var line_id          = $("#line_id").val();
    var factory_id       = $("#factory_id").val();
    var url_select_style = $("#url_select_style").val();

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
                data:{
                    line_id   : line_id,
                    factory_id: factory_id
                }
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }
    function chooseItem() {
        var style      = $(this).data('style');
        var article    = $(this).data('article');
        var balance    = $(this).data('balance');
        var line_id    = $("#line_id").val();
        var factory_id = $("#factory_id").val();
        console.log(balance);
        $.ajax({
                type: "GET",
                url: url_select_style,
                data: {
                    line_id: line_id,
                    factory_id: factory_id,
                    article: article,
                    style: style
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('#style').val(style);
                    $('#article').val(article);
                    $('#balance').val(balance);
                    $("#select_style").text(style+"-"+article);
                    $("#sewing_id").val(response.sewing_id);
                    $("#style_default").val(response.style_default);
                    get_counter();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                    if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
                    $('#style').val('');
                    $("#select_style").text('Pilih Style');
                }
            })
            .done(function () {
                
            });
    }
    
    itemAjax();

}
function selectStylePicklist(name, url_get_style) {
    var search           = '#' + name + 'Search';
    var item_id          = '#' + name + 'Id';
    var item_name        = '#' + name + 'Name';
    var modal            = '#' + name + 'Modal';
    var table            = '#' + name + 'Table';
    var buttonSrc        = '#' + name + 'ButtonSrc';
    var line_id          = $("#line_id").val();
    var factory_id       = $("#factory_id").val();
    var url_select_style = $("#url_select_style").val();
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url_get_style,
                data:{
                    line_id   : line_id,
                    q         : q,
                    factory_id: factory_id
                }
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');
            $(table).find('.btn-choose').on('click', chooseItem);

        });
    }

    function chooseItem() {
        var style      = $(this).data('style');
        var article    = $(this).data('article');
        var line_id    = $("#line_id").val();
        var factory_id = $("#factory_id").val();
        
        $.ajax({
                type: "GET",
                url: url_select_style,
                data: {
                    line_id: line_id,
                    factory_id: factory_id,
                    article: article,
                    style: style
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('#style').val(style);
                    $("#select_style").text(style+"-"+article);
                    $("#sewing_id").val(response.sewing_id);
                    get_counter();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                    if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
                    $('#style').val('');
                    $("#select_style").text('Pilih Style');
                }
            })
            .done(function () {
                
            });
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function selectStyleClose() {
    $('#selectStyleModal').modal('toggle');
}