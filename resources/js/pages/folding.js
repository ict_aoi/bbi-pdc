method       = $("#method").val();
scan_garment = $("#scan_garment").val();
if (method == 0&&user_session==-1) {
    list_breakdown_size_data = JSON.parse($('#breakdown_size_data').val());
}else if(method == 1&&user_session==-1){
    list_breakdown_size_data= JSON.parse($('#breakdown_size_scan').val());
    document.getElementById("txtScanBarcode").focus();
}
$(document).ready( function ()
{ 
   $("#txtScanBarcode").click(function (e) { 
       e.preventDefault();
       $(this).select();
   });
   var user_session= $('#user_session').val();
    $('#user_session').trigger('change');
    $("#btn_cari").click(function (e) { 
        var production_id = $("#prod_id").val();
        var url           = $('#url_breakdown_size_data').val();
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: url,
            data: {
                production_id : production_id,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                list_breakdown_size_data = [];
                render();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) 
            {
                for (i in response) 
                {
                    var data  = response[i]
                    var input = {
                        'id'                  : data.id,
                        'production_id'       : data.production_id,
                        'production_size_id'  : data.production_size_id,
                        'qc_endline_id'       : data.qc_endline_id,
                        'size'                : data.size,
                        'qty_order'           : data.qty_order,
                        'total_output_qc'     : data.total_output_qc,
                        'total_output_hangtag': data.total_output_hangtag,
                        'wip'                 : data.wip,
                        'qty_input'           : 0,
                    }
                    list_breakdown_size_data.push(input);
                }
                
            },
            error: function (response) 
            {
                $.unblockUI();
                
                if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
    
                if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function ()
        {
            render();
        });
    });
    $("#txtScanBarcode").change(function (e) { 
        e.preventDefault();
        var barcode = $(this).val();
        var url     = $('#url_breakdown_size_scan').val();
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: url,
            data: {
                barcode : barcode,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                $("#txtScanBarcode").val(' ');
                list_breakdown_size_data = [];
                render();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) 
            {
                var status = response.status;
                $("#inner_pack_label").text(response.inner_pack);
                for (i in response.data) 
                {
                    var data  = response.data[i]
                    var input = {
                        'id'                            : data.id,
                        'barcode_id'                    : data.barcode_garment,
                        'production_id'                 : data.production_id,
                        'production_size_id'            : data.production_size_id,
                        'folding_package_id'            : data.folding_package_id,
                        'folding_package_detail_size_id': data.folding_package_detail_size_id,
                        'poreference'                   : data.poreference,
                        'style'                         : data.style,
                        'article'                       : data.article,
                        'size'                          : data.size,
                        'qty_order'                     : data.qty_order,
                        'balance'                       : data.balance,
                        'wip'                           : data.wip,
                        'qty_available'                 : data.qty_available,
                        'inner_pack'                    : data.inner_pack,
                        'qty_input'                     : data.qty_input,
                        'qty'                           : 0,
                        'scan_garment'                  : data.scan_garment
                    }
                    list_breakdown_size_data.push(input);
                    
                }
                if (status==1) {
                    $('#status_com_label').removeClass("hidden");
                    $('#status_on_label').addClass('hidden');
                    $('#status_label').addClass('hidden');
                } else if(status==0){
                    $('#status_on_label').removeClass("hidden");
                    $('#status_label').addClass('hidden');
                    $('#status_com_label').addClass('hidden');
                }
                
            },
            error: function (response) 
            {
                $.unblockUI();
                
                if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
    
                if (response['status'] == 422)  {
                    $("#alert_warning").trigger('click', response.responseJSON.message);
                    document.getElementById("txtScanBarcode").focus();
                };
            }
        })
        .done(function ()
        {
            render();
            checkFocus();
        });
    });
});

$('#user_session').on('change',function(){
    var value = $(this).val();
    if(value == -1)
    {
        $('#modal_login_folding').modal();
    }
});

$('#login_folding').submit(function (event) 
{
    event.preventDefault();
    var nik 	= $('#nik_folding').val();
    
    if (!nik) 
    {
        $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
        return false;
    }

    $.ajax({
        type: "POST",
        url: $('#login_folding').attr('action'),
        data: $('#login_folding').serialize(),
        beforeSend: function () 
        {
            $('#modal_login_folding').find('.folding-shade-screen').removeClass('hidden');
            $('#modal_login_folding').find('#folding-body-login').addClass('hidden');
        },
        complete: function () 
        {
            $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
        },
        success: function () 
        {
            $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
            $('#login_folding').trigger("reset");
        },
        error: function (response) 
        {
            $('#modal_login_folding').find('.folding-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#folding-body-login').removeClass('hidden');
            
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
        }
    })
    .done(function (request)
    {
        document.location.href = request;
    });
});

$('#login_adm_loading').submit(function (event) 
{
    event.preventDefault();
    var nik 	= $('#nik_adm_loading').val();
    
    if (!nik) 
    {
        $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
        return false;
    }

    $.ajax({
        type: "POST",
        url: $('#login_adm_loading').attr('action'),
        data: $('#login_adm_loading').serialize(),
        beforeSend: function () 
        {
            $('#modal_login_folding').find('.admin-loading-shade-screen').removeClass('hidden');
            $('#modal_login_folding').find('#admin-loading-body-login').addClass('hidden');
        },
        complete: function () 
        {
            $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
        },
        success: function () 
        {
            $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
            $('#login_adm_loading').trigger("reset");
        },
        error: function (response) 
        {
            $('#modal_login_folding').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_folding').find('#admin-loading-body-login').removeClass('hidden');
            
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
        }
    })
    .done(function (request)
    {
        document.location.href = request;
    });
});
$('#poreferenceButtonLookup').click(function () {
    var url = $('#url_poreference_picklist').val();
    $("#poreferenceModal").modal('show');
    poreferencePicklist('poreference',  url + '?');
});

$('#poreferenceName').click(function () {
    var url = $('#url_poreference_picklist').val();
    
    poreferencePicklist('poreference', url + '?');
});
function poreferencePicklist(name, url) 
{
	var search 		= '#' + name + 'Search';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
    var buttonSrc 	= '#' + name + 'ButtonSrc';
    var buyer = $("#buyer").val();
    $(search).val('');
    
    function itemAjax()
    {
        var q = $(search).val();
        
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url+ '&q=' + q+'&buyer='+buyer
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');
           $(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
        var production_id            = $(this).data('id');
        var poreference              = $(this).data('buyer');
        var style                    = $(this).data('style');
        var article                  = $(this).data('article');
        var url                      = $('#url_breakdown_size_data').val();
        $('#prod_id').val(production_id);
        $('#poreference_label').text(poreference);
        $('#style_label').text(style);
        $('#article_label').text(article);
        
        $.ajax({
            type: "GET",
            url: url,
            data: {
                production_id : production_id,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                list_breakdown_size_data = [];
                render();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) 
            {
                for (i in response) 
                {
                    var data  = response[i]
                    var input = {
                        'id'                  : data.id,
                        'production_id'       : data.production_id,
                        'production_size_id'  : data.production_size_id,
                        'qc_endline_id'       : data.qc_endline_id,
                        'size'                : data.size,
                        'qty_order'           : data.qty_order,
                        'total_output_qc'     : data.total_output_qc,
                        'total_output_hangtag': data.total_output_hangtag,
                        'wip'                 : data.wip,
                        'qty_input'           : 0,
                    }
                    list_breakdown_size_data.push(input);
                }
                
            },
            error: function (response) 
            {
                $.unblockUI();
                
                if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
    
                if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function ()
        {
            render();
        });

        
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

    itemAjax();
}
function render() 
{
    if (method == 0) {
        getIndex();
        $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
        var tmpl 		= $('#breakdown_size_data_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: list_breakdown_size_data };
        var html 		= Mustache.render(tmpl, data);
        
        $('#breadown_size_data_tbody').html(html);
        bind();
    } else if(method == 1) {
        getIndex();
        $('#breakdown_size_scan').val(JSON.stringify(list_breakdown_size_data));
        var tmpl 		= $('#breakdown_size_scan_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: list_breakdown_size_data };
        var html 		= Mustache.render(tmpl, data);
        
        $('#breadown_size_scan_tbody').html(html);
        bind();
    }
	
}

function getIndex() 
{
    if (method == 0) {
        $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
        for (idx in list_breakdown_size_data) 
        {
            list_breakdown_size_data[idx]['_id'] 	= idx;
            list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
        }
    } else if (method == 1) {
        $('#breakdown_size_scan').val(JSON.stringify(list_breakdown_size_data));
        for (idx in list_breakdown_size_data) 
        {
            list_breakdown_size_data[idx]['_id'] 	= idx;
            list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
        }
    }
    
}

function bind() 
{
    $('.btn-edit-qty').on('click', editQty);
    $('.btn-save-qty').on('click', saveQty);
    $('.btn-cancel-qty').on('click', cancelEdit);
    $('.btn-cancel-qty').on('click', cancelEdit);
    if (method=='1') {
        if (scan_garment=='1') {
            $('.txt-scan-garment').on('change', scanGarment);
        }
        $('.btn-full-qty').on('click', fullQty);
    }
}
function checkFocus() {

    for (var i in list_breakdown_size_data) {
        var data = list_breakdown_size_data[i];
        if (data.inner_pack>data.qty_input) {
            document.getElementById("scanGarment_"+i).focus();
            return false;
        }
        
    }

    return true;
}
function editQty() {
    var i = $(this).data('id');
    $('#qtyFolding_' + i).addClass('hidden');

    //remove hidden textbox
    $('#qtyFoldingInput_' + i).removeClass('hidden');
    $('#qtyFoldingInput_' + i).select();

    $('#editQty_'+i).addClass("hidden");
    if (method=='packing') {
        $('.btn-full-qty').addClass("hidden");
    }
    $('#simpanQty_' + i).removeClass('hidden');
    $('#cancelQty_' + i).removeClass('hidden');
}
function saveQty() {
    if (method==0) {
        var i                  = $(this).data('id');
        var url                = $("#url_breakdown_store_hangtag").val();
        var qty                = $('#qtyFoldingInput_' + i).val();
        var data               = list_breakdown_size_data[i];
        var qty_new            = data.wip-qty;
        var production_id      = data.production_id;
        var production_size_id = data.production_size_id;
        var qc_endline_id      = data.qc_endline_id;
        
        if (!$.isNumeric(qty)) {
            $("#alert_error").trigger("click", 'Qty harus angka');
            $('#qtyFoldingInput_' + i).focus();
            return false;
        }
        if (qty == 0) {
            $("#alert_error").trigger("click", 'Qty Loading Tidak boleh 0');
            $('#qtyFoldingInput_' + i).focus();
            return false;
        }
        if (qty_new < 0) {
            $("#alert_error").trigger("click", 'Qty yang dimasukan lebih dari qty wip');
            $('#qtyFoldingInput_' + i).focus();
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            data: {
                production_id     : production_id,
                production_size_id: production_size_id,
                qc_endline_id     : qc_endline_id,
                qty               : qty,
            },
            cache: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function () {
                data.qty_input            = 0;
                data.wip                  = qty_new;
                data.total_output_hangtag = parseInt(data.total_output_hangtag)+parseInt(qty);
                $('#qtyFoldingInput_' + i).addClass('hidden');
                $('#qtyFolding_' + i).removeClass('hidden');

                $('#editQty_'+i).removeClass("hidden");
                $('#simpanQty_' + i).addClass('hidden');
                $('#cancelQty_' + i).addClass('hidden');
                render();
            },
            error: function (response) {
                $.unblockUI();
                
                if (response.status == 422) {
                    $("#alert_warning").trigger("click",response.responseJSON);
                    $("#btn_cari").trigger("click");
                };
            }
        });
    } else if(method==1) {
        var i                              = $(this).data('id');
        var dataScan                       = list_breakdown_size_data[i];
        var url                            = $("#url_store_scan_packing").val();
        var qty                            = (scan_garment==0)?$('#qtyFoldingInput_' + i).val():dataScan.qty;
        var production_id                  = dataScan.production_id;
        var production_size_id             = dataScan.production_size_id;
        var folding_package_id             = dataScan.folding_package_id;
        var folding_package_detail_size_id = dataScan.folding_package_detail_size_id;
        var barcode_id                     = dataScan.barcode_id;
        var wip                            = parseInt(dataScan.wip)-parseInt(qty);
        var qty_available                  = parseInt(dataScan.qty_available)-parseInt(qty);
        var inner_pack                     = parseInt(dataScan.inner_pack);
        var qty_input                      = parseInt(dataScan.qty);
        
        
        if (scan_garment==0) {
            if (wip<0) {
                $("#alert_warning").trigger("click", "WIP tidak boleh 0, Info Qc Untuk output");
                return;
            }
            if (qty_available<0) {
                $("#alert_warning").trigger("click", "Qty Input tidak boleh lebih dari Qty Inner Pack");
                return;
            }
           
        }else if(scan_garment==1){
            if (dataScan.qty==0) {
                $("#alert_warning").trigger("click", "Data Tidak bisa di simpan,Silahkan Scan Garment");
                checkFocus();
                return;
            }
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            data: {
                production_id                 : production_id,
                production_size_id            : production_size_id,
                folding_package_id            : folding_package_id,
                folding_package_detail_size_id: folding_package_detail_size_id,
                qty_input                     : qty,
                barcode_id                    : barcode_id,
                scan_garment                  : scan_garment,
                full_inner                    : 0,
            },
            cache: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                dataScan.qty = 0;
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var qty                = response.qty;
                var status                = response.status;

                    if (scan_garment==0) {
                        dataScan.wip           = parseInt(dataScan.wip)-parseInt(qty);
                        dataScan.qty_available = parseInt(dataScan.qty_available)-parseInt(qty);
                        dataScan.balance       = parseInt(dataScan.balance)-parseInt(qty);
                        dataScan.qty_input     = parseInt(dataScan.qty_input)+parseInt(qty);
                    }
                    if (status==1) {
                        $('#status_com_label').removeClass("hidden");
                        $('#status_on_label').addClass('hidden');
                        $('#status_label').addClass('hidden');
                        document.getElementById("txtScanBarcode").focus();
                    } else if(status==0){
                        $('#status_on_label').removeClass("hidden");
                        $('#status_label').addClass('hidden');
                        $('#status_com_label').addClass('hidden');
                    }
                    
                render();
                if (scan_garment==1) {
                    var dataRender = list_breakdown_size_data[i];
                    let countRow   = dataRender.length;
                    if (parseInt(countRow)>1) {
                        var index = parseInt(i)+1;
                        if (parseInt(countRow<index)) {
                            document.getElementById("txtScanBarcode").focus();
                        }else{
                            checkFocus();
                        }
                        
                    }else{
                        if (dataRender.inner_pack>dataRender.qty_input) {
                            document.getElementById("scanGarment_"+i).focus();
                        }else{
                            document.getElementById("txtScanBarcode").focus();
                        }
                    }
                }
            },
            error: function (response) {
                $.unblockUI();
                
                if (response.status == 422) {
                    $("#alert_warning").trigger("click",response.responseJSON.message);
                };
            }
        });
    }
    
}
function scanGarment(){
    var i             = $(this).data('id');
    var barcode_id    = $(this).val();
    var data          = list_breakdown_size_data[i];
    var qty_input     = data.qty_input;
    var qty_available = data.qty_available;
    var wip           = data.wip;
    var balance       = data.balance;
    var qty           = data.qty;
    var cekitem       = checkBarcodeGarment(barcode_id,i);

    if ((parseInt(wip)-1)<0) {
        $("#alert_warning").trigger("click", "Wip Tidak boleh 0");
        $(this).val('');
        return false;
    }
    if ((parseInt(qty_available)-1)<0) {
        $("#alert_warning").trigger("click", "Qty Input tidak boleh lebih dari Qty Inner Pack");
        $(this).val('');
        var countRow = list_breakdown_size_data.length;
        if (parseInt(countRow)>1) {
            var index = parseInt(i)+1;
            if (parseInt(countRow<index)) {
                document.getElementById("txtScanBarcode").focus();
            }else{
                checkFocus();
            }
            
        }else{
            document.getElementById("txtScanBarcode").focus();
        }
        return;
    }
    if (data.barcode_id!=-1) {
        if (!cekitem) {
            $("#alert_warning").trigger("click", "Barcode Salah");
            
            $(this).val('');
            checkFocus();
            return false;
        }
    }
    var cek_complete       = qty_available-1;

    data.qty_available = cek_complete;
    data.barcode_id    = barcode_id;
    data.qty_input     = parseInt(qty_input)+1;
    data.wip           = parseInt(wip)-1;
    data.balance       = parseInt(balance)-1;
    data.qty           = parseInt(qty)+1;
    render();
    $(this).val('');
    checkFocus();
    if (cek_complete==0) {
        $("#simpanQty_"+i).trigger("click");
    }

    
}
function checkBarcodeGarment(barcode_id,index) {

    for (var i in list_breakdown_size_data) {
        var data = list_breakdown_size_data[index];
        if (data.barcode_id == barcode_id){
            return true;
        }
            
            // console.log(data.barcode_id+"-"+barcode_id);
    }

    return false;
}
function cancelEdit() {
    var i = $(this).data('id');
    var qty  = $('#qtyFolding_' + i).val();
    var data = list_breakdown_size_data[i];
    $('#qtyLoadingInput_' + i).removeClass('hidden');

    //add hidden textbox
    $('#qtyFolding_' + i).addClass('hidden');

    $('#simpanQty_' + i).addClass('hidden');
    $('#cancelQty_' + i).addClass('hidden');
    $('#editQty_' + i).removeClass('hidden');
    qty = data.qty_input;
    render();
}
function fullQty() {
    var i                              = $(this).data('id');
    var data                           = list_breakdown_size_data[i];
    var url                            = $("#url_store_scan_packing").val();
    var production_id                  = data.production_id;
    var production_size_id             = data.production_size_id;
    var folding_package_id             = data.folding_package_id;
    var folding_package_detail_size_id = data.folding_package_detail_size_id;
    var wip                            = parseInt(data.wip);
    var qty_available                  = parseInt(data.qty_available);
    var inner_pack                     = parseInt(data.inner_pack);
    var qty_input                      = parseInt(data.qty_input);
    if ((wip-qty_available)<0) {
        $("#alert_warning").trigger("click", "WIP tidak boleh 0, Info Qc Untuk output");
        return;
    }
    if (qty_available<=0) {
        $("#alert_warning").trigger("click", "Qty Inner Pack sudah penuh");
        return;
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            production_id                 : production_id,
            production_size_id            : production_size_id,
            folding_package_id            : folding_package_id,
            folding_package_detail_size_id: folding_package_detail_size_id,
            qty_input                 : qty_available,
            full_inner                 : 1,
        },
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var qty                = response.qty;
            var status                = response.status;

                data.wip           = parseInt(data.wip)-parseInt(qty);
                data.qty_available = parseInt(data.qty_available)-parseInt(qty);
                data.balance       = parseInt(data.balance)-parseInt(qty);
                data.qty_input     = parseInt(data.qty_input)+parseInt(qty);
                if (status==1) {
                    $('#status_com_label').removeClass("hidden");
                    $('#status_on_label').addClass('hidden');
                    $('#status_label').addClass('hidden');
                    document.getElementById("txtScanBarcode").focus();
                } else if(status==0){
                    $('#status_on_label').removeClass("hidden");
                    $('#status_label').addClass('hidden');
                    $('#status_com_label').addClass('hidden');
                }
                
            render();
        },
        error: function (response) {
            $.unblockUI();
            
            if (response.status == 422) {
                $("#alert_warning").trigger("click",response.responseJSON.message);
            };
        }
    });
}
function reportScanFolding() {
    var url = $("#url_report_folding_scan").val();
    $("#reportScanFoldingModal").modal();
    lov('reportScanFolding', url);
}
function reportScanFoldingClose() {
    $('#reportScanFoldingModal').modal('toggle');
}
function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
    }
    
    itemAjax();

}
function reboot() {
    var url_ssh_reboot = $("#url_ssh_reboot").val();
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_reboot,
        beforeSend: function() {
            $('#shutdownModal').modal('toggle');
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
            
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
    });
}
function shutdown() {
    var url_ssh_shutdown = $("#url_ssh_shutdown").val();
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_shutdown,
        beforeSend: function() {
            $('#shutdownModal').modal('toggle');
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
    });
}


