items     = JSON.parse($('#defect_confirmation_data').val());
buyer     = $("#buyer").val();
connected_flag=0;
let mqtt;
let reconnectTimeout = 2000;
$(document).ready(function () {
    
    $('#user_session').trigger('change');
    $('#factory_id').trigger('change');

    $("#btn_cari").click(function (e) { 
        e.preventDefault();
        getData(1);
    });
    function startTime() {
        let today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        h = checkTime(h);
        $('#time').text(h + ":" + m + ":" + s);
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }; // add zero in front of numbers < 10
        return i;
    }
    $("#btnGood").click(function (e) { 
        e.preventDefault();
        let production_id      = $("#production_id").val();
        let production_size_id = $("#production_size_id").val();
        
        if (!production_id) {
            if(buyer=='nagai'){
                $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
                return false;
            }else{
                $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
                return false;
            }
        }
        if (!production_size_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Size Telebih Dahulu');
            return false;
        }
        
        swal("Masukan Qty Output", {
            content: "input",
            buttons: true,
        })
        .then((value) => {
            if (parseInt(value)>0) {
                if (value) {
                    let is_style_input = $("#is_style_input").val();
                    let qty            = parseInt(value);
                    let wip            = parseInt($('.wip').text())-qty;
                    let counterDay     = parseInt($('.check_day').text())+qty;
                    let counterAll     = parseInt($('.all').text())+qty;
                    let balance        = parseInt($('.balance').text())+qty;
                    let counterIdle    = parseInt($('#qtyIdle').val())+qty;
                    let repairing      = parseInt($('.repairing').text());
                    let commitment_line = parseInt($('#commitment_line').val());
                    if (wip<0) {
                        $("#alert_error").trigger("click", 'Tidak Ada Tumpukan Garment');
                        return false;
                    }
                    
                    if ((balance+repairing)>0) {
                        $("#alert_error").trigger("click", 'Qty Sudah Balance');
                        return false;
                    }
                    if (qty>20) {
                        $("#alert_error").trigger("click", 'Maksimal output 20 Tumpukan');
                        return false;
                    }
                    $("#qtyIdle").val(qty);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                            type: "post",
                            url: $('#form').attr('action'),
                            data: new FormData(document.getElementById("form")),
                            processData: false,
                            contentType: false,
                            beforeSend: function() {
                                $("#qtyIdle").val('0');
                                $.blockUI({
                                    message: '<i class="icon-spinner4 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                            },
                            complete: function() {
                                $.unblockUI();
                            },
                            success: function(response) {
                                $.unblockUI();
                
                            },
                            error: function(response) {
                                $.unblockUI();
                                if (response['status'] == 500){
                                    $("#alert_error").trigger("click", 'Please Contact ICT.');
                                    $("#qtyIdle").val('0');
                                    getData(1);
                                }
                                    
                                
                                if (response['status'] == 422||response['status']==419){
                                    $("#alert_error").trigger("click", response.responseJSON.message);
                                    $("#qtyIdle").val('0');
                                    getData(1);
                                }
                                    
                            }
                        })
                        .done(function() {
                            
                            $(".wip").text(wip);
                            $(".check_day").text(counterDay);
                            $(".all").text(counterAll);
                            $(".balance").text(balance);
                            $("#qtyIdle").val('0');
                            if (is_style_input=='1') {
                                let balance_line = parseInt($('#balance_line').val())-qty;
                                let style_merge  = $("#style_merge").val();
                                let line_id      = $("#line_id").val();
                                $('#balance_line').val(balance_line);
                                if (commitment_line>balance_line) {
                                    checkAndon();
                                }

                                let topic       = JSON.parse($("#topic").val());
                                var topic_andon = '';
                                topic.forEach(element => {
                                    // console.log(element.andon);
                                    topic_andon=element.andon;
                                });
                                console.log(topic_andon);
                                let msg = '{"type":"andon_update","payload": {"line_id":"'+line_id+'","qty":"'+qty+'","style":"'+style_merge+'"}}';
                                send_message(topic_andon,msg);
                            }
                            
                            

                    });
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        });
        /* $("#selesai").addClass('hidden');
        $("#process").removeClass("hidden");
         */
    });
    $("#btnDefect").click(function (e) { 
        e.preventDefault();
        var production_id      = $("#production_id").val();
        var production_size_id = $("#production_size_id").val();
        var wip                = parseInt($(".wip").text());
        var balance            = parseInt($(".balance").text());
        var url                = $("#url_get_defect").val();
        var repairing          = parseInt($('.repairing').text());

        if (!production_id) {
            if(buyer=='nagai'){
                $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
                return false;
            }else{
                $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
                return false;
            }

        }
        if (!production_size_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Size Telebih Dahulu');
            return false;
        }
        
        if (wip<=0) {
            $("#alert_error").trigger("click", 'Tidak Ada Tumpukan Garment');
            return false;
        }
        if ((balance+repairing)>=0) {
            $("#alert_error").trigger("click", 'Qty Sudah Balance');
            return false;
        }
        $("#defectModal").modal();
        defectPicklist('defect', url + '?');
    });

    var page = $('#page').val();
    if (page == 'laporan') {
        var url_laporan_qc_endline_data = $('#url_laporan_qc_endline_data').val();
        laporanAdmLoading(url_laporan_qc_inline_data);
    }
    function getDataQc() {
        var   min   = 120, max = 180;
        var   rand  = Math.floor(Math.random() * (max - min + 1) + min);  //Generate Random number between 60 - 80
        // alert('Wait for ' + rand + ' seconds');
        getData(0);
        setTimeout(getDataQc, rand * 1000);
    }
    function getAlertFolding() {
        const buyer = $("#buyer").val();
        if (buyer=='nagai') {
            var min = 40,max = 60;
            var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
            // alert('Wait for ' + rand + ' seconds');
            alertFolding();
            setTimeout(getAlertFolding, rand * 1000);
        }
        
    }
    function getOutputStyle() {
        const buyer = $("#buyer").val();
        if (buyer=='nagai') {
            var min = 80,max = 120;
            var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
            // alert('Wait for ' + rand + ' seconds');
            OuputStyle();
            setTimeout(OuputStyle, rand * 1000);
        }
        
    }
    setInterval(function(){ 
        var weekday             = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        var today               = new Date();
        var d                   = weekday[today.getDay()];
        var h                   = today.getHours();
        var m                   = today.getMinutes();
        var s                   = today.getSeconds();
        var s                   = today.getSeconds();
        var start               = $("#start").val();
        var end                 = $("#end").val();
        var split_start         = start.split(':')[0];
        var split_end           = end.split(':')[0];
        var commitment_line_jam = $("#commitment_line_jam").val();
        
        if (m=='45'&&s=='00') {
            if (split_start<=h&&split_end>h) {
                getOutputHourly();
            }
        }
    },1000);
    getDataQc();
    startTime();
    getAlertFolding();
    getOutputStyle();
    MQTTconnect();
});
$('#user_session').on('change', function () {
    var value = $(this).val();
    if (value == -1) {
        $('#modal_login_qc_endline').modal();
    }
});

$('#login_qc_endline').submit(function (event) {
    event.preventDefault();
    var nik = $('#nik_qc_endline').val();

    if (!nik) {
        $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
        return false;
    }

    $.ajax({
            type: "POST",
            url: $('#login_qc_endline').attr('action'),
            data: $('#login_qc_endline').serialize(),
            beforeSend: function () {
                $('#modal_login_qc_endline').find('.admin-loading-shade-screen').removeClass('hidden');
                $('#modal_login_qc_endline').find('#admin-loading-body-login').addClass('hidden');
            },
            complete: function () {
                $('#modal_login_qc_endline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_endline').find('#admin-loading-body-login').removeClass('hidden');
            },
            success: function () {
                $('#modal_login_qc_endline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_endline').find('#admin-loading-body-login').removeClass('hidden');
                $('#login_qc_endline').trigger("reset");
            },
            error: function (response) {
                $('#modal_login_qc_endline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_endline').find('#admin-loading-body-login').removeClass('hidden');

                if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                if (response['status'] == 419) $("#alert_warning").trigger("click", 'Silahkan Refresh Halaman Anda');
            }
        })
        .done(function (request) {
            document.location.href = request;
        });
});

$('#factory_id').change(function () {
    var url = $('#url_line_picklist').val();
    var factory_id = $(this).val();
    // linePicklist('line', factory_id, url + '?');
});

$('#poreferenceButtonLookup').click(function () {
    var url = $('#url_poreference_picklist').val();
    $("#poreferenceModal").modal('show');
    poreferencePicklist('poreference',  url + '?');
});

$('#poreferenceName').click(function () {
    var url = $('#url_poreference_picklist').val();
    
    poreferencePicklist('poreference', url + '?');
});
function poreferencePicklist(name, url) {
    var search = '#' + name + 'Search';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q     = $(search).val();
        var buyer = $("#buyer").val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+"&buyer="+buyer
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');
            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem() {
        var production_id = $(this).data('id');
        var buyer = $(this).data('buyer');
        var style = $(this).data('style');
        var article = $(this).data('article');
        var url = $('#url_get_status').val();

        $('#style_label').text(style);
        $('#style').text(style);
        $('#article_label').text(article);
        $("#production_id").val(production_id);
        $("#poreferenceName").val(buyer);
        $("#poreference_label").text(buyer);
        $("#sizeModal").modal();
        $('#sizeButtonLookup').trigger('click');
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
$('#sizeButtonLookup').click(function () {
    var url = $('#url_size_picklist').val();
    $("#sizeModal").modal('show');
    sizePicklist('size', url + '?');

});

$('#sizeName').click(function () {
    var url = $('#url_size_picklist').val();
    
    sizePicklist('size', url + '?');
});
function sizePicklist(name, url) {
    var search        = '#' + name + 'Search';
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';
    var buttonSrc     = '#' + name + 'ButtonSrc';
    var production_id = $("#production_id").val();
    $(search).val('');

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q+'&production_id='+production_id
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var production_size_id = $(this).data('id');
        var size = $(this).data('size');

        $("#sizeName").val(size);
        $("#size_label").text(size);
        $("#production_size_id").val(production_size_id);
        getData(1);
        
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function getData(params) {
    var flagRefresh        = $("#flag_refresh").val();
    var url_get_data       = $("#url_get_data").val();
    var production_id      = $("#production_id").val();
    var production_size_id = $("#production_size_id").val();
    var qtyidle            = parseInt($('#qtyIdle').val());
    
    if (flagRefresh==0) {
        if (qtyidle==0&&production_id&&production_size_id) {
            $.ajax({
                url: url_get_data,
                data: {
                    production_id: production_id,
                    production_size_id: production_size_id
                },
                beforeSend: function() {
                    if (params==1) {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    }
                },
            })
            .done(function (data) {
                $.unblockUI();
                $(".wip").text(data.wip);
                $(".check_day").text(data.counterDay);
                $(".balance").text(data.balance);
                $(".total_repairing").text(data.totRepairing);
                $(".repairing").text(data.repairing);
                $(".all").text(data.counter);
                $("#qc_endline_id").val(data.qcEndlineId);
                $("#style_merge").val(data.style_merge);
                OuputStyle(data.style_merge);
                checkDaily();
            });
        }
    }
}
function repairing() {
    var url = $("#url_getRepairing").val();
    var repairing = parseInt($(".repairing").text());
    if (repairing==0) {
        $("#alert_error").trigger("click", 'Tidak Ada List Repairing');
        return false;
    }
    $("#repairingModal").modal();
    lov('repairing', url);
}
$("#btnReportDaily").click(function (e) { 
    e.preventDefault();
    reportQcCheck();
});
function reportQcCheck() {
    var url = $("#url_reportQcCheck").val();
    $("#reportDailyModal").modal();
    lov('reportDaily', url);
}
function status_pobuyer() {
    var url = $("#url_get_status_po").val();
    
    $("#statusPOModal").modal();
    lov('statusPO', url);
}
function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';
    var qc_endline_id = $("#qc_endline_id").val();

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
    }
    
    itemAjax();

}
function defectPicklist(name, urldefect) {
    var search = '#' + name + 'Search';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');
    var buyer = $("#buyer").val();
    function itemAjax() {
        var q     = $(search).val();
        
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: urldefect + '&q=' + q
        })
        .done(function (data) {
            $(table).html(data['view']);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');
            $(table).find('.btn-choose').on('click', chooseItem);
            if(buyer=='nagai'){
                $(table).find('.btn-unchoose').on('click', unchooseItem);
            }
            draw(data['data']);
        });
    }
    
    if (buyer=='other') {
        
        function chooseItem() {
            var id                 = $(this).data('id');
            var url_storeDefect    = $("#url_storeDefect").val();
            var production_id      = $("#production_id").val();
            var production_size_id = $("#production_size_id").val();
            var qc_endline_id      = $("#qc_endline_id").val();
            $.ajax({
        
                url: url_storeDefect,
                data: {
                    production_id     : production_id,
                    production_size_id: production_size_id,
                    defect_id         : id,
                    qc_endline_id     : qc_endline_id,
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    var repair = parseInt($(".repairing").text())+1;
                    var wip    = parseInt($('.wip').text())-1;
                    $(".repairing").text(repair);
                    $(".wip").text(wip);
                    $("#qc_endline_id").val(response.qc_endline_id);
        
                },
                error: function(response) {
                    $.unblockUI();
                    if (response['status'] == 500)
                        $("#alert_error").trigger("click", 'Please Contact ICT.');
                    
                    if (response['status'] == 422||response['status']==419)
                        $("#alert_error").trigger("click", response.responseJSON.message);
                        $("#qtyIdle").val('0');
                    getData(1);
                }
            })
        }
    } else if(buyer=='nagai'){
        function chooseItem() {
            var id           = $(this).data('id');
            var name         = $(this).data('name');
            var is_available = isAvailable(id);
            var wip          = parseInt($(".wip").text());
            var repairing    = parseInt($(".repairing").text());
            var balance      = parseInt($(".balance").text());
            
            if (wip<=0) {
                $("#alert_error").trigger("click", 'Wip Habis');
                return false;
            }
            if ((balance+repairing)>=0) {
                $("#alert_error").trigger("click", 'Qty Sudah Balance');
                return false;
            }

            if(is_available > 0)
            {
                $("#alert_info").trigger("click", 'Defect Sudah pernah di pilih.');
                return false;
            }
            var input = {
                'id': -1,
                'defect_id': id,
                'defect_name': name,
                'qty_defect': 1,
            };
            items.push(input);
            $('#'+id+'_UnSelect').removeClass('hidden');
            $('#'+id+'_Select').addClass('hidden');
            $('#defect_confirmation_data').val(JSON.stringify(items));
            $('#'+id).css('background-color', '#ffcc66');
            $(".wip").text(wip-1);
            $(".repairing").text(repairing+1);
            $("#flag_refresh").val(1);
            $(search).focus();
            render();
        }
        function unchooseItem() {
            var id           = $(this).data('id');
            var wip          = parseInt($(".wip").text());
            var repairing    = parseInt($(".repairing").text());
            var balance      = parseInt($(".balance").text());
            for (var idx in items) 
            {
                var data = items[idx];
                if (id == data.defect_id) 
                {
                    items.splice(idx, 1);
                    $('#'+data.defect_id).css('background-color', '#ffffff');
                    
                }
            }

            $(".wip").text(wip+1);
            $(".repairing").text(repairing-1);
            $('#'+id+'_UnSelect').addClass('hidden');
            $('#'+id+'_Select').removeClass('hidden');
            render();
        }
        
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            urldefect = $(this).attr('href') + (params == undefined ? '?' : '');
            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function render() {
    getIndex();
    $('#defect_confirmation_data').val(JSON.stringify(items));
    var tmpl 		= $('#defect_data_table').html();
	Mustache.parse(tmpl);
    var data 		= { list: items };
    var html 		= Mustache.render(tmpl, data);
	$('#defect_data_tbody').html(html);
    bind();
}
function bind() {
    $('.btn-delete-defect').on('click', deleteItem);
    $('.btn-cancel-defect').on('click', cancelEdit);
    $('.btn-save-defect').on('click', saveQty);
    $('.btn-edit-defect').on('click', editQty);
}

function getIndex() {
    for (idx in items) {
        items[idx]['_id'] = idx;
        items[idx]['no'] = parseInt(idx) + 1;
    }
}
function deleteItem() {
    var i         = $(this).data('id');
    var data      = items[i];
    var wip       = parseInt($(".wip").text())+parseInt(data.qty_defect);
    var repairing = parseInt($(".repairing").text())-parseInt(data.qty_defect);
    $(".wip").text(wip);
    $(".repairing").text(repairing);
    items.splice(i, 1);
    render();
}
function editQty() {
    var i = $(this).data('id');
    $('#qty_' + i).addClass('hidden');

    //remove hidden textbox
    $('#qtyDefectInput_' + i).removeClass('hidden');
    $('#qtyDefectInput_' + i).select();

    $('#editDefect_'+i).addClass("hidden");
    $('#simpanDefect_' + i).removeClass('hidden');
    $('#cancelDefect_' + i).removeClass('hidden');
}
function saveQty() {
    var i         = $(this).data('id');
    var wip       = parseInt($(".wip").text());
    var repairing = parseInt($(".repairing").text());
    var balance   = parseInt($(".balance").text());
    var qty       = $('#qtyDefectInput_' + i).val();
    var data      = items[i];
    var newQty    = qty-parseInt(data.qty_defect);
    var newWip    = wip-parseInt(newQty);
    var newrepairing    = repairing+parseInt(newQty);
    if (qty == 0) {
        $("#alert_error").trigger("click", 'Qty Defect Tidak boleh 0');
        $('#qtyDefectInput_' + i).focus();
        return false;
    }
    
    
    if (parseInt(newWip)<0) {
        $("#alert_error").trigger("click", 'Wip Habis');
        return false;
    }
    if ((balance+newrepairing)>=0) {
        $("#alert_error").trigger("click", 'Qty Sudah Balance');
        return false;
    }
    data.qty_defect = qty;
    $('#qtyDefectInput_' + i).addClass('hidden');
    $('#qty_' + i).removeClass('hidden');
    $(".wip").text(newWip);
    $(".repairing").text(newrepairing);
    $('#editQty_'+i).removeClass("hidden");
    $('#simpanQty_' + i).addClass('hidden');
    $('#cancelQty_' + i).addClass('hidden');
    render();
}
function cancelEdit() {
    var i = $(this).data('id');
    var qty  = $('#qtyDefectInput_' + i).val();
    var data = items[i];
    $('#qtyDefectInput_' + i).removeClass('hidden');
    
    //add hidden textbox
    $('#qty_' + i).addClass('hidden');
    qty = data.qty_input;
    $('#simpanQty_' + i).addClass('hidden');
    $('#cancelQty_' + i).addClass('hidden');
    $('#editQty_' + i).removeClass('hidden');
    render();
}
function draw(data)
{
	for (var id in data.data) 
	{
        var _data = data.data[id];
        
		for (var idx in items)
		{
			var list_data = items[idx];
			if (_data.id_defect == list_data.defect_id)
			{
				$('#'+_data.id_defect).css('background-color', '#ffcc66');
				$('#'+_data.id_defect+'_UnSelect').removeClass('hidden');
				$('#'+_data.id_defect+'_Select').addClass('hidden');
			}
		}
	}
	
}
function drawEachLine(id_defect) 
{
	for (var idx in items) {
		var data = items[idx];
		if (id_defect == data.defect_id) {
			$('#'+data.id_defect).css('background-color', '#ffcc66');
		}
	}

}
function isAvailable(id)
{
	var flag = 0;
	for (var idx in items) 
	{
		var data = items[idx];
		if (id == data.defect_id) 
		{
			flag++;
		}
	}
	return flag;
}

function reportDailyClose() {
    $('#reportDailyModal').modal('toggle');
}
function repairingClose() {
    $('#repairingModal').modal('toggle');
    $("#flag_refresh").val(0);
    getData(0);
    
}
function refresh() {
    location.reload()
}
function reboot() {
    var url_ssh_reboot = $("#url_ssh_reboot").val();
    $('#shutdownModal').modal('toggle');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_reboot,
        beforeSend: function() {
            
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
                $('#shutdownModal').modal('show');
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
                $('#shutdownModal').modal('show');
        }
    })
    .done(function() {
    });
}
function shutdown() {
    var url_ssh_shutdown = $("#url_ssh_shutdown").val();
    $('#shutdownModal').modal('toggle');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_shutdown,
        beforeSend: function() {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
            
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
    });
}
function defectClose() {
    if (items.length>0) {
        var production_id = $("#production_id").val();
        var production_size_id = $("#production_size_id").val();
        var qc_endline_id = $("#qc_endline_id").val();
        $('#defectModal').modal('toggle');
        $("#confrimationDefectModal").modal('show');
        $("#production_id_defect").val(production_id);
        $("#production_size_id_defect").val(production_size_id);
        $("#qc_endline_id_defect").val(qc_endline_id);
        render();
    } else {
        $('#defectModal').modal('toggle');
        $("#flag_refresh").val(0);
        getData(0);
    }
}
$('#btnBackDefect').click(function (e) { 
    e.preventDefault();
    $("#btnDefect").trigger("click");
});
$('#btnCloseConfirmationDefect').click(function (e) { 
    e.preventDefault();
    $("#production_id_defect").val('');
    $("#production_size_id_defect").val('');
    $("#qc_endline_id_defect").val('');
    $("#flag_refresh").val(0);
    getData(0);
    items = [];
    render();
    $('#confrimationDefectModal').modal('hide');
});
$('#form_collection_defect').submit(function (event){
    event.preventDefault();
    var production_id = $("#production_id_defect").val();
    var production_size_id = $("#production_size_id_defect").val();
    var qc_endline_id = $("#qc_endline_id_defect").val();
    
    if(items.length>=0&&!production_id&&!production_size_id)
    {
        $("#alert_warning").trigger("click", 'Kesalahan data silahkan refresh halaman anda');
        return false;
    }
    $.ajax({
        type: "POST",
        url: $('#form_collection_defect').attr('action'),
        data: $('#form_collection_defect').serialize(),
        beforeSend: function () {
            $('#confrimationDefectModal').modal('hide');
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function () {
            $("#production_id_defect").val('');
            $("#production_size_id_defect").val('');
            $("#qc_endline_id_defect").val('');
            $("#flag_refresh").val(0);
            getData(0);
            items = [];
            render();

            getData(0);
        },
        error: function (response) {
            $.unblockUI();
            
            if (response.status == 422) {
                $("#confrimationDefectModal").modal('show');
                $("#alert_warning").trigger("click",response.responseJSON.message)
            };
        }
    });
});
function getOutputHourly() {

    var src     = $('#img_div').find('img').attr('src');
    var line_id = $("#line_id").val();
    var start   = $("#start").val();
    var end     = $("#end").val();
    var url     = $("#url_get_commitment_line").val();

    $.ajax({
        url: url,
        data: {
            line_id: line_id,
            start: start,
        },
    })
    .done(function (data) 
    {
        var actual_output = parseInt(data.output);
        var commitment_line_jam = parseInt($("#commitment_line_jam").val());
        
        if (commitment_line_jam>actual_output) {
            swal({
                title: "Perhatian",
                icon: src,
                text: data.message,
                content:true,
            });
        }
    });
}
function alertFolding() {

    var src     = $('#img_div').find('img').attr('src');
    var line_id = $("#line_id").val();
    var start   = $("#start").val();
    var end     = $("#end").val();
    var url     = $("#url_get_alert_folding").val();

    $.ajax({
        url: url,
        data: {
            line_id: line_id,
            start: start,
        },
    })
    .done(function (data) 
    {
        let total_data = data.total_data;
        if (total_data!=0) {
            const myhtml = document.createElement("div");
            myhtml.innerHTML = data.message;
            swal({
                title: "Perhatian",
                content: myhtml,
                icon: "warning",
            })
        }
    });
}
function checkAndon() {
    let line_id     = $("#line_id").val();
    let url         = $("#url_check_andon").val();
    $.ajax({
        url: url,
        data: {
            line_id: line_id
        },
    })
    .done(function (data) {
    });
}
function OuputStyle(style) {
    
    let production_id = $("#production_id").val();
    let line_id       = $("#line_id").val();
    let style_merge   = (style)?style:$("#style_merge").val();
    let url           = $("#url_output_style").val();
    if (production_id) {
        $.ajax({
            url: url,
            data: {
                line_id: line_id,
                style: style_merge
            },
        })
        .done(function (data) {
            $("#balance_line").val(data.balance);
            $("#commitment_line_style").val(data.commitment_line_style);
        });
    }
    
}
function MQTTconnect() {
    var s = $("#server").val();
    var p = $("#port").val();;
    if (p!="")
    {
        console.log("ports");
        port=parseInt(p);
        console.log("port" +port);
    }
    if (s!="")
    {
        host=s;
        console.log("host");
    }
    console.log("connecting to "+ host +" "+ port);
    var x=Math.floor(Math.random() * 10000); 
    var cname="orderform-"+x;
    mqtt = new Paho.MQTT.Client(host,port,cname);
    //document.write("connecting to "+ host);
    var options = {
        timeout: 3,
        onSuccess: onConnect,
        onFailure: onFailure,
    };
    
    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;
        //mqtt.onConnected = onConnected;

    mqtt.connect(options);
    return false;
}
function onConnectionLost(){
    console.log("connection lost");
    // document.getElementById("status").innerHTML = "Connection Lost";
    // document.getElementById("messages").innerHTML ="Connection Lost";
    connected_flag=0;
}
function onFailure(message) {
    console.log("Failed");
    // document.getElementById("messages").innerHTML = "Connection Failed- Retrying";
    setTimeout(MQTTconnect, reconnectTimeout);
}
function onMessageArrived(r_message){
    
    let value = JSON.parse(r_message.payloadString);
    if (value.type=='commitment') {
        $("#commitment_line").val(value.payload.commitment_line);
        $("#balance_line").val(value.payload.commitment_line);
        $("#style_daily").val(JSON.stringify(value.payload.style));
        setTimeout(function () {
            checkDaily();
        }, 2000);
    }else if(value.type=='refresh'){
        refresh();
    }else if(value.type=='balance'){
        let production_id      = $("#production_id").val();
        let production_size_id = $("#production_size_id").val();
        if ((production_id==value.payload.production_id)&&(production_size_id==value.payload.production_size_id)) {
            let balance            = parseInt($('.balance').text())+parseInt(value.payload.balance);
            setTimeout(function () {
                $('.balance').text(balance);
            }, 100);
        }
    }
}
function onConnected(recon,url){
    console.log(" in onConnected " +recon);
}
function onConnect() {
    connected_flag=1
    console.log("on Connect "+connected_flag);
    let topic = JSON.parse($("#topic").val());
    topic.forEach(element => {
        sub_topics(element.balance);
        sub_topics(element.target);
    });
}
function send_message(topic,msg){
    // document.getElementById("messages").innerHTML ="";
    if (connected_flag==0){
    out_msg="<b>Not Connected so can't send</b>"
    console.log(out_msg);
    // document.getElementById("messages").innerHTML = out_msg;
    return false;
    }
    // let msg = msg;
    console.log(msg);

    // let topic = 'cmd/andonbbis/group1/123';
    // let topic = topic;
    message = new Paho.MQTT.Message(msg);
    if (topic=="")
        message.destinationName = "test-topic"
    else
        message.destinationName = topic;
    mqtt.send(message);
    return false;
}
function sub_topics(stopic){
    if (connected_flag==0){
        out_msg="<b>Not Connected so can't subscribe</b>";
        return false;
    }
    console.log("Subscribing to topic ="+stopic);
    mqtt.subscribe(stopic);
    return false;
}
function checkDaily() {  
    let style_merge = $("#style_merge").val();
    let style_daily = $("#style_daily").val();
    let BreakException = {};

    
    
    try {
        JSON.parse(style_daily).forEach(element => {
            console.log(style_merge==element);
            if (style_merge==element) {
                $("#is_style_input").val(1);
                throw BreakException;
            }else{
                $("#is_style_input").val(0);
            }
        });
        $("#is_style_input").val(0);
    } catch (e) {
        if (e !== BreakException) $("#is_style_input").val(0);
        
    }
    
}