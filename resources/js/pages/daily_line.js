$(document).ready(function() 
{
    connected_flag=0;
	let mqtt;
    let reconnectTimeout = 2000;
    $('.clockpicker').clockpicker();
    var dailyLineTable = $('#dailyLineTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 10,
        // deferRender: true,
        ajax: {
            type: 'GET',
            url: '/daily-line/data',
            data: function(d) {
                return $.extend({}, d, {
                    "line_id": $('#lineId').val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = dailyLineTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'style',
            name: 'style',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'smv',
            name: 'smv',
            searchable: false,
            orderable: false
        }, {
            data: 'commitment_line',
            name: 'commitment_line',
            searchable: false,
            orderable: false
        }, {
            data: 'change_over',
            name: 'change_over',
            searchable: false,
            orderable: false
        },{
            data: 'cumulative_day',
            name: 'cumulative_day',
            searchable: false,
            orderable: false
        },{
            data: 'present_sewer',
            name: 'present_sewer',
            searchable: false,
            orderable: false
        },{
            data: 'action',
            name: 'action',
            searchable: false,
            orderable: false
        }
    ],

    });

    var dtable = $('#dailyLineTable').dataTable().api();
    $(".dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("keyup", function(e) { // Bind our desired behavior
        // If the user pressed ENTER, search
        if (e.keyCode == 13) {
            // Call the API search function
            dtable.search(this.value).draw();
        }
        // Ensure we clear the search if they backspace far enough
        if (this.value == "") {
            dtable.search("").draw();
        }
        return;
    });
    $('#lineButtonLookup').click(function () {
        let factory_id = $("#factory_id").val();
        let url        = '/daily-line/line-picklist/'+factory_id;
        linePicklist('line',url);
    
    });
    
    $('#lineName').click(function () {
        let factory_id = $("#factory_id").val();
        let url        = '/daily-line/line-picklist/'+factory_id;
        linePicklist('line',url);
    });
    $("#factory").change(function (e) { 
        e.preventDefault();
        let value = $(this).val();
        $("#factory_id").val(value);
        $('#lineId').val('');
        $('#line_id').val('');
        $('#lineName').val('');
        $('#daily_working_id').val('');
    });
    function linePicklist(name,url) {
        let search     = '#' + name + 'Search';
        let item_id    = '#' + name + 'Id';
        let item_name  = '#' + name + 'Name';
        let modal      = '#' + name + 'Modal';
        let table      = '#' + name + 'Table';
        let buttonSrc  = '#' + name + 'ButtonSrc';
        
        $(search).val('');
    
        function itemAjax() {
    
            var q = $(search).val();
            
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url,
                    data:{
                        q:q,
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var id         = $(this).data('id');
            var name       = $(this).data('name');
            var working_id = $(this).data('working_id');
            getDailyWorking(id);
            $('#lineId').val(id);
            $('#line_id').val(id);
            $('#lineName').val(name);
            $('#daily_working_id').val(working_id);
            MQTTconnect();
            dtable.draw();
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    $('#styleButtonLookup').click(function () {
        var line_id = $("#line_id").val();
        if (!line_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
            return false;
        }
        $('#createDailyModal').modal('hide');
        $('#styleModal').modal();
        stylePicklist('style');
    });
    
    $('#styleName').click(function () {
        var line_id = $("#line_id").val();
        if (!line_id) {
            $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
            return false;
        }
        $('#createDailyModal').modal('hide');
        $('#styleModal').modal();
        stylePicklist('style');
    });
    
    function stylePicklist(name) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
    
            var q       = $(search).val();
            var line_id = $("#line_id").val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: '/daily-line/style-picklist',
                    data:{
                        q      : q,
                        line_id: line_id
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var style   = $(this).data('style');
            var smv   = $(this).data('smv');
            if (!smv) {
                $("#alert_error").trigger("click", 'SMV belum ada Info, IE development');
                return false;
            }
            $('#styleName').val(style);
            $('#smv').val(smv);
            $('#createDailyModal').modal();
            document.getElementById("commitment_line").focus();
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    $("#btn_cari").click(function (e) { 
        e.preventDefault();
        var line_id = $().val();
        dtable.draw();
    });
    dtable.draw();
});
function addDailyWorking() {
    var line_id               = $("#line_id").val();
    var url_add_daily_working = $("#url_add_daily_working").val();
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
        return false;
    }
    $("#working_hoursModal").modal();
    lov('working_hours', url_add_daily_working+'?line_id='+line_id);
}
function getDailyWorking(line_id) {
    $.ajax({
        url: '/daily-line/get-daily-working',
        data: {
            line_id: line_id,
        },
        
        complete: function() {
        },
        success: function (response) {
            if(response.id){
                $("#add_hours").addClass('hidden');
                $("#edit").removeClass('hidden');
                $("#add_style").removeClass('hidden');
                $("#start_hours").text(response.start);
                $("#end_hours").text(response.end);
                $("#working_hours").text(response.working_hours);
                $("#recess").text(response.recess);
                // $("#daily_working_id").val(response.id);
            }else{
                $("#add_hours").removeClass('hidden');
                $("#edit").addClass('hidden');
                $("#add_style").addClass('hidden');
                $("#start_hours").text("-");
                $("#end_hours").text("-");
                $("#working_hours").text("-");
                $("#working_hours").text("-");
                $("#recess").text("-");
                // $("#daily_working_id").val();
            }
        }
    });
}
$('#form').submit(function(event) {
    event.preventDefault();
    var line_id = $("#line_id").val();
    var daily_working_id = $("#daily_working_id").val();
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
        return false;
    }
    if (!daily_working_id) {
        $("#alert_error").trigger("click", 'Ada kesalahan,Silahkan refresh halaman anda');
        return false;
    }
    $('#createDailyModal').modal('hide');
    bootbox.confirm("Apakah Anda Yakin Data Akan di Simpan ?.", function(result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    sendAlertAndon(response.line_id,response.style);
                    $("#alert_success").trigger("click", "Data Berhasil di Simpan");
                    $('#dailyLineTable').DataTable().ajax.reload();
                    $("#styleName").val('');
                    $("#styleId").val('');
                    $("#smv").val('');
                    $("#commitment_line").val('');
                    $("#present_sewer").val('');
                    $("#select_change_over").empty();
                    $("#select_change_over").append('<option value="">-- Pilih Change Over --</option>');
                    $("#form").trigger('reset');

                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    $('#confirmationModal').modal();
                }
            });
        }
    });
});

function edit(url) {
    var line_id = $("#line_id").val();
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan Pilih Line Terlebih dahulu');
        return false;
    }
    $("#editModal").modal();
    lov('edit', url);
}
function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';
    var qc_endline_id = $("#qc_endline_id").val();

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');

                $('.pickatime-limits').pickatime({
                    min: [7,00],
                    max: [20,0]
                });
            });
    }
    itemAjax();
}
function editClose() {
    $("#editModal").modal('toggle');
}
function working_hoursClose() {
    $("#working_hoursModal").modal('toggle');
}
function hapus(url) {
    bootbox.confirm("Apakah Anda Yakin Data Akan di Hapus ?.", function(result) {
        if (result) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    $("#alert_success").trigger("click", "Data Berhasil di Hapus");
                    sendAlertAndon(response.line_id,response.style);
                    $('#dailyLineTable').DataTable().ajax.reload();

                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    $('#confirmationModal').modal();
                }
            });
        }
    });
}
function sendAlertAndon(line_id,style) {  
    $.ajax({
        type: 'get',
        url: '/daily-line/send-alert-andon',
        data:{
            'line_id': line_id,
            'style'  : style
        }
    })
    .done(function(data)
    {
        let message = '{"type":"commitment","payload":'+JSON.stringify(data.msg)+'}';
        let message_andon = '{"type":"andon_add","payload":"update message"}';
        send_message(data.topic,message);
        send_message(data.topic_andon,message_andon);
    });
}

function MQTTconnect() {
    var s = $("#server").val();
    var p = $("#port").val();;
    if (p!="")
    {
        port=parseInt(p);
    }
    if (s!="")
    {
        host=s;
    }
    console.log("connecting to "+ host +" "+ port);
    var x=Math.floor(Math.random() * 10000); 
    var cname="orderform-"+x;
    mqtt = new Paho.MQTT.Client(host,port,cname);
    var options = {
        timeout: 3,
        onSuccess: onConnect,
        onFailure: onFailure,
    };
    
    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;

    mqtt.connect(options);
    return false;
}
function onConnectionLost(){
    console.log("connection lost");
    connected_flag=0;
}
function onFailure(message) {
    console.log("Failed");
    setTimeout(MQTTconnect, reconnectTimeout);
}
function onMessageArrived(r_message){
    out_msg="Message received "+r_message.payloadString+"<br>";
    out_msg=out_msg+"Message received Topic "+r_message.destinationName;
    
}
function onConnected(recon,url){
    console.log(" in onConnected " +reconn);
}
function onConnect() {
    connected_flag=1;
    console.log("on Connect "+connected_flag);
}
function send_message(topic,msg){
    console.log(topic);
    if (connected_flag==0){
    out_msg="<b>Not Connected so can't send</b>"
    console.log(out_msg);
    return false;
    }
    message = new Paho.MQTT.Message(msg);
    console.log(message);
    if (topic=="")
        message.destinationName = "test-topic"
    else
        message.destinationName = topic;
        console.log(message);
    mqtt.send(message);
    return false;
}
