var items                  = JSON.parse($('#items').val());

list_data_process_from = JSON.parse($('#data_process_from').val());
list_data_process_to   = JSON.parse($('#data_process_to').val());
machines               = JSON.parse($('#machines').val());
buyer                  = $("#buyer").val();
$(document).ready(function () {
    
    $('#user_session').trigger('change');
    $('#factory_id').trigger('change');

    $('#form').submit(function (event) {
        event.preventDefault();
        var production_id = $('#production_id').val();
        var line_id       = $('#line_id').val();
        var sewer_nik     = $('#sewer_nik').val();
        var workflow_detail_id    = $('#workflow_detail_id').val();
        var last_process    = $('#last_process').val();
        var cekVal        = validasiInspect(production_id, line_id, machine_id, sewer_nik, workflow_detail_id,'submit');
        
        if (items.length<5) {
            $("#alert_error").trigger("click", 'Inspection tidak lengkap!!');
            return false
        }
        if (cekVal) {
            saveSample();
            /* if(last_process==1){
                swal({
                    title: "PERHATIAN!!!",
                    text: "Proses ini adalah Proses Terakhir,Klik OK Untuk Pindah Round",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((wilsave) => {
                    if (wilsave) {
                        saveSample()
                    }
                });
            }else{
                saveSample();
            } */
            /* bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                if (result) {
                    
                }
            }); */ 
        }
        
    });

    var page = $('#page').val();
    if (page == 'laporan') {
        var url_laporan_qc_inline_data = $('#url_laporan_qc_inline_data').val();
        laporanAdmLoading(url_laporan_qc_inline_data);
    }
    function getNotif() {
        var min = 40,max = 50;
        var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 60 - 80
        // alert('Wait for ' + rand + ' seconds');
        getNotifData();
        setTimeout(getNotif, rand * 1000);
    }
    render();
    getNotif();
    getSewer();
    getDataMachine();
    
});
function saveSample(){
    $.ajax({
        type: "POST",
        url: $('#form').attr('action'),
        data: $('#form').serialize(),
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            resetNik();
            resetHidden();
            $("#select_machine").val("").trigger("change");
            $("#round_label").text(response.round);
            $("#round_icon").text(response.round);
            $("#round").val(response.round);
            $("#alert_success").trigger("click", 'Data berhasil disimpan');
        },
        error: function (response) {
            $.unblockUI();
            resetNik();
            resetHidden();
            $("#select_machine").val("").trigger("change");
            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

            if (response['status'] == 500){
                $("#alert_error").trigger("click", 'Please Contact ICT.');
            }
        }
    });
}
$('#user_session').on('change', function () {
    var value = $(this).val();
    if (value == -1) {
        $('#modal_login_qc_inline').modal();
    }
});

$('#login_qc_inline').submit(function (event) {
    event.preventDefault();
    var nik = $('#nik_qc_inline').val();

    if (!nik) {
        $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
        return false;
    }

    $.ajax({
            type: "POST",
            url: $('#login_qc_inline').attr('action'),
            data: $('#login_qc_inline').serialize(),
            beforeSend: function () {
                $('#modal_login_qc_inline').find('.admin-loading-shade-screen').removeClass('hidden');
                $('#modal_login_qc_inline').find('#admin-loading-body-login').addClass('hidden');
            },
            complete: function () {
                $('#modal_login_qc_inline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_inline').find('#admin-loading-body-login').removeClass('hidden');
            },
            success: function () {
                $('#modal_login_qc_inline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_inline').find('#admin-loading-body-login').removeClass('hidden');
                $('#login_qc_inline').trigger("reset");
            },
            error: function (response) {
                $('#modal_login_qc_inline').find('.admin-loading-shade-screen').addClass('hidden');
                $('#modal_login_qc_inline').find('#admin-loading-body-login').removeClass('hidden');

                if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
            }
        })
        .done(function (request) {
            document.location.href = request;
        });
});

$('#factory_id').change(function () {
    var url = $('#url_line_picklist').val();
    var factory_id = $(this).val();
    linePicklist('line', factory_id, url + '?');
});

$('#poreferenceButtonLookup').click(function () {
    var url = $('#url_poreference_picklist').val();
    var line_id = $('#line_id').val();
    var line_name = $('#line_name').val();
    if (line_name == 'all') {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }

    poreferencePicklist('poreference', line_id, url + '?');

});
$('#sewerButtonLookup').click(function () {
    var url = $('#url_show_history_operator_sewer').val();
    var line_id = $('#line_id').val();
    var line_name = $('#line_name').val();
    var production_id   = $('#production_id').val();
    if (line_name == 'all') {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    if (!production_id) {
        if(buyer=='nagai'){
            $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
            return false;
        }else{
            $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
            return false;
        }
    }
    $('#sewerModal').modal('show');
    sewerPicklist('sewer', line_id, url + '?');

});

$('#poreferenceName').click(function () {
    var url = $('#url_poreference_picklist').val();
    var line_id = $('#line_id').val();
    var line_name = $('#line_name').val();

    if (line_name == 'all') {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    poreferencePicklist('poreference', line_id, url + '?');
});
$('#componentButtonLookup').click(function () {
    var url             = $('#url_get_process').val();
    var production_id   = $('#production_id').val();
    var line_name       = $('#line_name').val();
    var sewer_nik       = $('#sewer_nik').val();
    var round           = $('#round').val();
    var count_component = $('#count_component').val();

    if (line_name == 'all') {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    if (!production_id) {
        $("#alert_warning").trigger("click", 'Silahkan pilih Po Buyer terlebih dahulu');
        return false;
    }
    if (!sewer_nik) {
        $("#alert_warning").trigger("click", 'Silahkan input Nama Sewer');
        return false;
    }
    if (parseInt(count_component)==0) {
        $('#componentModal').modal('toggle');
        $('#processModal').modal('show');
        processPicklist('process', production_id,round,url,'');
    } else {
        var url_get_component_process = $("#url_get_component_process").val();
        var workflow_id               = $("#workflow_id").val();

        // $('#componentModal').modal('show');
        componentPicklist('component', workflow_id,url_get_component_process);
    }
    

});

$('#componentName').click(function () {
    var url             = $('#url_get_process').val();
    var production_id   = $('#production_id').val();
    var line_name       = $('#line_name').val();
    var round           = $('#round').val();
    var sewer_nik       = $('#sewer_nik').val();
    var count_component = $('#count_component').val();

    if (line_name == 'all') {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    if (!production_id) {
        if(buyer=='nagai'){
            $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
            return false;
        }else{
            $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
            return false;
        }
    }
    if (!sewer_nik) {
        $("#alert_warning").trigger("click", 'Silahkan input Nama Sewer');
        return false;
    }
    if (parseInt(count_component)==0) {
        $('#componentModal').modal('toggle');
        $('#processModal').modal('show');
        processPicklist('process', production_id,round,url,'');
    } else {
        var url_get_component_process = $("#url_get_component_process").val();
        var workflow_id               = $("#workflow_id").val();

        // $('#componentModal').modal('show');
        componentPicklist('component', workflow_id,url_get_component_process);
    }
});


function render() {
    getIndex();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();
    Mustache.parse(tmpl);
    var data = {
        item: items
    };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
}

function bind() {
    $('#addGood').on('click', addGood);
    $('#AddDefect').on('click', AddDefect);
    $('.btn-delete-item').on('click', deleteItem);
}

function getIndex() {
    for (idx in items) {
        items[idx]['_id'] = idx;
        items[idx]['no'] = parseInt(idx) + 1;
    
    }
   
   
}

function addGood() {
    var production_id = $('#production_id').val();
    var line_id = $('#line_id').val();
    var machine_id = $('#machine_id').val();
    var sewer_nik = $('#sewer_nik').val();
    var workflow_detail_id = $('#workflow_detail_id').val();
    var cek = 'cek';
    var cekVal = validasiInspect(production_id, line_id, machine_id, sewer_nik, workflow_detail_id,cek);
    if (cekVal) {
        var jumlah = 5-parseInt(items.length);
        for (i = 0; i < jumlah; i++) {
            var input = {
                'defect_id': '0',
                'defect_code': 'No Defect',
                'grade_id': '1'
            };
            items.push(input);
            $('#grade').val(1);
            grade_calculation(1);
            render();
        }
    }
}

function AddDefect() {
    var production_id      = $('#production_id').val();
    var line_id            = $('#line_id').val();
    var machine_id         = $('#machine_id').val();
    var sewer_nik          = $('#sewer_nik').val();
    var workflow_detail_id = $('#workflow_detail_id').val();
    var urldefect          = $('#url_get_defect').val();
    var cek                = 'cek';
    var cekVal             = validasiInspect(production_id, line_id, machine_id, sewer_nik, workflow_detail_id,'cek');
    if (cekVal) {
        $("#defectModal").modal();
        defectPicklist('defect', urldefect + '?');
        
    }
}

function deleteItem() {
    var i = $(this).data('id');

    var arr_new = $("#items_grade").val().split(',');

    items.splice(i, 1);
    arr_new.splice(i, 1);

    $("#items_grade").val(arr_new.join(',')).trigger('change');
    render();
}
$("#select_machine").change(function (e) {
    e.preventDefault();
    var value = $(this).val();
    if (value!='') {
        var id = $("#select_machine option:selected").val();
        $("#machine_id").val(id);
        historyGrade();
    }
});

function linePicklist(name, factory_id, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q + '&factory_id=' + factory_id
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var id    = $(this).data('id');
        var name  = $(this).data('name');

        $(item_id).val(id);
        $(item_name).val(name);
        $('#line_id').val(id);
        $('#line_name').val(name);
        
        $('#poreferenceName').trigger('click');
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function poreferencePicklist(name, line_id, url) {
    var search = '#' + name + 'Search';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&line_id=' + line_id + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var production_id = $(this).data('id');
        var workflow_id   = $(this).data('workflow_id');
        var buyer         = $(this).data('buyer');
        var style         = $(this).data('style');
        var style_merge   = $(this).data('style_merge');
        var article       = $(this).data('article');
        var url           = $('#url_get_status').val();

        $('#style_label').text(style);
        $('#style').text(style);
        $('#style_merge').val(style_merge);
        $('#article_label').text(article);
        $("#poreferenceName").val(buyer);
        $("#production_id").val(production_id);
        $("#workflow_id").val(workflow_id);
        $.ajax({
            type: "GET",
            url: url,
            data: {
                line_id    : line_id,
                workflow_id: workflow_id,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#round_label").text(response.round);
                $("#round_icon").text(response.round);
                $("#round").val(response.round);
                $("#count_component").val(response.countComponent);
                $(".lap_qc_inline").removeClass('hidden');
                $(".join_process").removeClass('hidden');
                $(".next_round").removeClass('hidden');
                $(".send_message").removeClass('hidden');
                resetNik();
                resetHidden();
                $("#select_machine").val("").trigger("change");
            },
            error: function (response) {
                $.unblockUI();
                if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function () {
            render();
        });
        
        // getProses();
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function sewerPicklist(name, line_id, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q           = $(search).val();
        var style       = $("#style_merge").val();
        var line_id     = $("#line_id").val();
        var round       = $("#round").val();
        var workflow_id = $("#workflow_id").val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url,
            data:{
                style      : style,
                round      : round,
                line_id    : line_id,
                workflow_id: workflow_id,
                q          : q
            }
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var nik                = $(this).data('nik');
        var sewer_name         = $(this).data('name');
        var workflow_detail_id = $(this).data('workflow_detail_id');
        var proses_name        = $(this).data('proses_name');
        var machine            = $(this).data('machine');
        var machine_name       = $(this).data('machine_name');
        var last_proses        = $(this).data('last_proses');

        $('#sewer_nik').val(nik);
        $('#nik_label').val(nik);
        $('#name_label').val(sewer_name);
        $('#sewer_name').val(sewer_name);
        $('#componentName').val(proses_name);
        $('#workflow_detail_id').val(workflow_detail_id);
        $('#machine_id').val(machine);
        $('#last_process').val(last_proses);
        $("#select_machine").val(machine).trigger("change");
        if (machine!='') {
            $("#select_machine").empty();
            $("#select_machine").append('<option value="' + machine + '">' + machine_name + '</option>');
            $("#select_machine").val(machine).trigger("change");
        }else{
            addSelectMachine();
        }
        historyGrade();
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function processPicklist(name, production_id,round, url,component) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q           = $(search).val();
        var workflow_id = $('#workflow_id').val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url,
                data:{
                    production_id: production_id,
                    workflow_id  : workflow_id,
                    q            : q,
                    round        : round,
                    component    : component
                }
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var id                 = $(this).data('id');
        var workflow_detail_id = $(this).data('workflow_detail_id');
        var name               = $(this).data('proses_name');
        var last_process       = $(this).data('last');
        var last_process       = $(this).data('last');
        var machine_id         = $(this).data('machine_id');
        var machine_name       = $(this).data('machine_name');
        
        resetHidden();
        $('#select_machine').val('').trigger("change");
        $("#workflow_detail_id").val(workflow_detail_id);
        $("#componentName").val(name);
        $('#process_id').val(id);
        $('#last_process').val(last_process);
        if (machine_id!='') {
            $("#select_machine").empty();
            $("#select_machine").append('<option value="' + machine_id + '">' + machine_name + '</option>');
            $("#select_machine").val(machine_id).trigger("change");
        }else{
            addSelectMachine();
        }
        
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function processClose() {
    var url_get_component_process = $("#url_get_component_process").val();
    var workflow_id               = $("#workflow_id").val();

    $('#processModal').modal('toggle');

    $('#componentModal').modal('show');
    componentPicklist('component', workflow_id,url_get_component_process);
}
function componentPicklist(name, workflow_id, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q           = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url,
                data:{
                    workflow_id: workflow_id,
                    q            : q,
                }
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var component     = $(this).data('name');
        var production_id = $("#production_id").val();
        var round         = $("#round").val();
        var get_proses    = $('#url_get_process').val();
        $('#processModal').modal('show');
        processPicklist('process', production_id,round,get_proses,component);
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function historyProcessPicklist(name,urlprocess,urlmesin) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: urlprocess
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }


    function chooseItem() {
        var workflow_detail_id = $(this).data('workflow_detail_id');
        var proses_name               = $(this).data('proses_name');
        var machine_id         = $(this).data('machine_id');
        var machine_name       = $(this).data('machine_name');
        var last_process       = $(this).data('last');
        
        resetHidden();
        $("#select_machine").empty();
        $("#select_machine").append('<option value="' + machine_id + '">' + machine_name + '</option>');
        $("#select_machine").val(machine_id).trigger("change");
        
        $('#workflow_detail_id').val(workflow_detail_id);
        $('#componentName').val(proses_name);
        $('#last_process').val(last_process);
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function historyMachinePicklist(name, urlmesin) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: urlmesin
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }


    function chooseItem() {
        var id = $(this).data('id');
        var name = $(this).data('name');

        $(item_id).val(id);
        $(item_name).val(name);
        $('#machine_id').val(id);
        $("#select_machine").empty();
        $("#select_machine").append('<option value="' + id + '" selected>' + name + '</option>');
        historyGrade();
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function history_sewerPicklist(name, urlsewer) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: urlsewer
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var nik = $(this).data('nik');
        var name = $(this).data('name');

        $(item_id).val(nik);
        $(item_name).val(name);
        $('#sewer').val(nik);
        $('#sewer_nik').val(nik);
        $('#nik_label').val(nik);
        $('#name_label').val(name);
        $('#sewer_name').val(name);
        historyGrade();
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function defectPicklist(name, urldefect) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q = $(search).val();
        var line_id = $("#line_id").val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: urldefect + '&q=' + q+'&line_id='+line_id
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var grade = $(this).data('grade');

        $(item_id).val(id);
        $(item_name).val(grade);
        var input = {
            'defect_id': id,
            'defect_code': name,
            'grade_id': grade
        };
        items.push(input);
        $("#color").removeClass(" ");

        if(buyer == 'other') {grade_calculation(grade);}
        
        render();

        var isZero =0;
    
        for (idx in items) {
            if (items[idx]['defect_id']>0) {
                isZero++;
            }
        }
       if (buyer=='nagai') 
        {
            if (isZero==1) {
                isZero = 2;
            } else if(isZero>1){
                isZero = 3;
            }else{
                isZero = 1;
            }
            $('#grade').val(isZero);
            grade_calculation(isZero); 
        }
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}

function getDataMachine() {
    $.ajax({
            type: 'get',
            url: '/qc/in-line/get-machine'
        })
        .done(function (response) {
            var listMachine = response.list;
            listMachine.forEach(element => {
                var input = {
                    'id': element.id,
                    'name': element.name
                };
               
                machines.push(input);
            });
            $('#machines').val(JSON.stringify(machines));
        })
}
$('#auto_completes').on('change',function()
{
    available_nik = JSON.parse($('#auto_completes').val());
   
    $("#sewer").autocomplete({
        source: available_nik
    });
});

$("#nik_label").change(function (e) {
    var nik           = $("#nik_label").val();
    var production_id = $('#production_id').val();
    var line_id       = $('#line_id').val();
    var factory_id    = $('#factory_id').val();
    var style_merge   = $('#style_merge').val();
    var workflow_id   = $('#workflow_id').val();

    var url           = $('#url_get_process').val();
    var round         = $('#round').val();

    if (!line_id) {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        resetNik();
        return false;
    }
    if (!production_id) {
        if(buyer=='nagai'){
            $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
            resetNik();
            return false;
        }else{
            $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
            resetNik();
            return false;
        }
    }
   
    $("#grade_before").val(0);
    $("#grade").val(0);
    items = [];
    render();
    $(".listround").empty();
    $(".beforeround").empty();
    $("#color").removeClass();
    if(nik)
    {
        var value     = nik.split(':');
        $('#nik_label').val(value[0]);
        $('#sewer_nik').val(value[0]);
        $('#name_label').val(value[1]);
        $('#sewer_name').val(value[1]);
        
    }else
    {
        $('#nik_label').val('');
        $('#sewer_nik').val('');
        $('#name_label').val('');
        $('#sewer_name').val('');
    }
    if ($("#name_label").val()!='') {
        
        $.ajax({
            type: "GET",
            url: '/qc/in-line/show-history',
            data: {
                line_id    : line_id,
                factory_id : factory_id,
                nik        : nik,
                style_merge: style_merge,
                workflow_id: workflow_id
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.list > 1) {
                    $("#historyProcessModal").modal();
                    historyProcessPicklist('historyProcess', response.urlprocess, response.urlmesin);
                }else if(response.list==1){
                    $("#select_machine").empty();
                    $("#select_machine").append('<option value="' + response.machine_id + '">' + response.machine_name + '</option>');
                    $("#select_machine").val(response.machine_id).trigger("change");
                    $("#process_id").val(response.process_id);
                    $("#workflow_detail_id").val(response.workflow_detail_id);
                    $("#componentName").val(response.process_name);
                    $("#last_process").val(response.last_process);
                    historyGrade();
                } else {
                    var count_component = $("#count_component").val();
                    if (parseInt(count_component)==0) {
                        $('#processModal').modal('show');
                        processPicklist('process', production_id,round,url,'');
                    } else {
                        var url_get_component_process = $("#url_get_component_process").val();
                        var workflow_id               = $("#workflow_id").val();

                        $('#componentModal').modal('show');
                        componentPicklist('component', workflow_id,url_get_component_process);
                    }
                    
                }

            },
            error: function (response) {
                $.unblockUI();
                if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function () {
            render();
        });
       
    }else{
        $("#alert_warning").trigger("click", 'Nik Tidak di temukan');
        $('#nik_label').val('');
        $('#sewer_nik').val('');
        $('#name_label').val('');
        $('#sewer_name').val('');
    }
    /* if (process_id) {
        historyGrade();
    } */
    
});

function resetNik() {
    $("#nik_label").val('');
    $("#sewer_nik").val('');
    $("#name_label").val('');
    $("#sewer_name").val('');
}
function resetHidden() {
    $("#componentName").val('');
    // $("#select_process").val('').trigger('change');
    $("#workflow_detail_id").val('');
    $("#machine_id").val('');
    $("#last_process").val('');
    $("#items_grade").val('');
    $("#grade_before").val(0);
    $("#grade").val(0);
    items = [];
    $(".listround").empty();
    $(".beforeround").empty();
    $("#color").removeClass();
    
    render();
}
function validasiInspect(production_id, line_id, machine_id, sewer_nik, workflow_detail_id,params) {
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    if (!production_id) {
        if(buyer=='nagai'){
            $("#alert_error").trigger("click", 'Silahkan Pilih No Lot Telebih Dahulu');
            return false;
        }else{
            $("#alert_error").trigger("click", 'Silahkan Pilih Po Buyer Telebih Dahulu');
            return false;
        }
    }
    if (!workflow_detail_id) {
        $("#alert_error").trigger("click", 'Silahkan pilih Proses terlebih dahulu');
        return false;
    }
    if (!machine_id) {
        $("#alert_error").trigger("click", 'Silahkan pilih Mesin terlebih dahulu');
        return false;
    }
    if (!sewer_nik) {
        $("#alert_error").trigger("click", 'Silahkan Isi NIK terlebih dahulu');
        return false;
    }
    if (params=='cek') {
        if (items.length > 4) {
            $("#alert_error").trigger("click", 'Maks. Inspection');
            return false;
        }
    }
    return true;

}

function historyGrade() {
    var url_get_history_grade = $('#url_get_history_grade').val();
    var url_get_grade_before = $('#url_get_grade_before').val();
    var workflow_detail_id = $("#workflow_detail_id").val();
    var sewer_nik = $("#sewer_nik").val();
    $.ajax({
            type: 'get',
            url: url_get_grade_before,
            data: {
                workflow_detail_id: workflow_detail_id,
                sewer_nik: sewer_nik
            }
    })
    .done(function (response) {
        $("#grade_before").val(response.lastGrade);
    })
    $.ajax({
            type: 'get',
            url: url_get_history_grade,
            data: {
                workflow_detail_id: workflow_detail_id,
                sewer_nik         : sewer_nik
            }
    })
    .done(function (response) {
        var listRound = response.detailGrade;
        var beforeGrade = response.beforeGrade;

        $(".listround").empty();
        $(".beforeround").empty();
        $("#color").removeClass();
        $.each(listRound, function (key,val) {
            $(".listround").append('<a href="#" class="btn bg-' + val.warna + '-400 btn-rounded btn-icon btn-xs legitRipple"><span class="letter-icon">' + val.round + '</span></a>');
        });
        
        $.each(beforeGrade, function (key,val) {
            $(".beforeround").append('<a href="#" class="btn bg-' + val.warna + '-400 btn-rounded btn-icon btn-xs legitRipple"><span class="letter-icon">' + val.round + '</span></a> ');
        });
    })
}

function grade_calculation(grade) {
    var oldValue = $("#items_grade").val();
        var grade_before = $('#grade_before').val();
        var arr = oldValue === "" ? [] : oldValue.split(',');
        arr.push(grade);

        var newValue = arr.join(',');

        $("#items_grade").val(newValue);

        var arr_new = $("#items_grade").val().split(',');

        var count = 0
        if (arr_new.length == 5) {
            $.each(arr_new, function (index, value) {
                
                if (value == 3) {
                    color_grade(3, grade_before);
                    return false;
                }
                if (value == 2) {
                    count++;
                }

                if (count > 1) {
                    color_grade(3, grade_before);
                } else if (count == 1) {
                    color_grade(2, grade_before);
                } else {
                    color_grade(1, grade_before);
                }
            });
        }
}

function color_grade(grade_now, grade_before) {

    var grade_now = parseInt(grade_now);
    var grade_before = parseInt(grade_before);

    var grade = 0;
    var color = 'bg-brown-400';

    if (grade_before >= 1) {

        if (grade_before == 3) {
            if (grade_now == (grade_before - 1)) {
                grade = grade_before;
            } else if (grade_now < (grade_before - 1)) {
                grade = grade_before - 1;
            } else {
                grade = grade_now;
            }
        } else if (grade_before == 2) {
            if (grade_now >= grade_before) {
                grade = grade_before + 1;
            }
            // else if (grade_now > grade_before) {
            //     grade = grade_before + 1;
            else {
                grade = grade_before - 1;
            }
        } else {
            grade = grade_now;
        }

    } else {
        grade = grade_now;
    }
    
    if (grade == 1) {
        color = 'btn btn-rounded btn-icon btn-xs legitRipple bg-success-400';
    } else if (grade == 2) {
        color = 'btn btn-rounded btn-icon btn-xs legitRipple bg-warning-400';
    } else if (grade == 3) {
        color = 'btn btn-rounded btn-icon btn-xs legitRipple bg-danger-400';
    }
    $("#color").removeClass();
    $("#grade").val(grade);
    $("#color").addClass(color);
}
function report_sample() {
    var line_id = $("#line_id").val();
    var url = $("#url_report_sample_round").val();
    $('#report_sample_roundModal').modal();
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan pilih Line  terlebih dahulu');
        return false;
    }
    reportSampleRoundPicklist('report_sample_round',line_id, url);
}
function reportSampleRoundPicklist(name,line_id, url) {
    var search = '#' + name + 'Search';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '?line_id=' + line_id + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                //$(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    

    itemAjax();
}
function report_sample_roundClose() {
    $('#report_sample_roundModal').modal('toggle');
}
function selectprocess() {
    $('.btn-choose').trigger('click');
    // $("#processTable").find('.btn-choose').on('click', chooseItem);
}
function getSewer() {
    var url = $("#url_get_sewer").val(); 
    var factory_id = $("#factory_id").val(); 
    $.ajax({
        type: 'get',
        url: url+'?factory_id='+factory_id
    })
    .done(function(response){
        $('#auto_completes').val(JSON.stringify(response)).trigger('change');
    })
}
$("#nik_awal").change(function (e) { 
    e.preventDefault();
    var nik = $(this).val();
    if(nik)
    {
        var value     = nik.split(':');
        $('#nik_awal').val(value[0]);
        $('#name_awal').val(value[1]);
        ajaxGetProcessHistory(value[0],'from');
    }else
    {
        $('#nik_awal').val("");
        $('#name_awal').val("");
    }
});
$("#nik_akhir").change(function (e) { 
    e.preventDefault();

    var nik = $(this).val();
    if(nik)
    {
        var value     = nik.split(':');
        var nik_awal = $("#nik_awal").val();
        if (value[0]==nik_awal) {
            $("#alert_error").trigger("click", 'Nik Asal dan Tujuan tidak boleh sama');
            return false;
        }
        $('#nik_akhir').val(value[0]);
        $('#name_akhir').val(value[1]);
        ajaxGetProcessHistory(value[0],'to');
    }else
    {
        $('#nik_akhir').val("");
        $('#name_akhir').val("");
    }
});
function ajaxGetProcessHistory(nik,origin) {
    var style      = $("#style_merge").val();
    var line_id    = $('#line_id').val();
    var factory_id = $('#factory_id').val();
    var url        = $("#url_get_history_process").val();

    $.ajax({
        type: "GET",
        url: url,
        data: {
            nik    : nik,
            style  : style,
            line_id: line_id,
            factory_id: factory_id
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            if (origin=='from') {
                list_data_process_from = [];
                list_data_process_to = [];
                renderDataFrom();
                renderDataTo();
            }
            if (origin=='to') {
                list_data_process_to = [];
                renderDataTo();
            }
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) 
        {
            for (i in response.data) 
            {
                var data  = response.data[i]
                var input = {
                    'id'               : data.history_id,
                    'history_id'       : data.history_id,
                    'machine_id'       : data.machine_id,
                    'style'            : data.style,
                    'process_sewing_id': data.process_sewing_id,
                    'line_id'          : data.line_id,
                    'factory_id'       : data.factory_id,
                    'machine_name'     : data.machine_name,
                    'proses_name'      : data.proses_name,
                }

                if (origin=='from') {
                    list_data_process_from.push(input);
                    renderDataFrom();
                }
                if (origin=='to') {
                    list_data_process_to.push(input);
                    renderDataTo();
                }
            }
            
        },
        error: function (response) 
        {
            $.unblockUI();
            
            if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');

            if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
        }
    })
    .done(function ()
    {
        
    });
}
function renderDataFrom() {
    getIndexDataFrom();
    $('#data_process_from').val(JSON.stringify(list_data_process_from));
    var tmpl 		= $('#process_from_data_table').html();
    Mustache.parse(tmpl);
    var data 		= { list: list_data_process_from };
    var html 		= Mustache.render(tmpl, data);
    $('#data_process_from_tbody').html(html);
    bindDataCopy();
}
function getIndexDataFrom() {
    $('#data_process_from').val(JSON.stringify(list_data_process_from));
    for (idx in list_data_process_from) 
    {
        list_data_process_from[idx]['_id'] 	= idx;
        list_data_process_from[idx]['no'] 	= parseInt(idx) + 1;
    }
}
function renderDataTo() {
    getIndexDataTo();
    $('#data_process_to').val(JSON.stringify(list_data_process_to));
    var tmpl 		= $('#process_to_data_table').html();
    Mustache.parse(tmpl);
    var data 		= { list: list_data_process_to };
    var html 		= Mustache.render(tmpl, data);
    $('#data_process_to_tbody').html(html);
    bindDataCopy();
}
function getIndexDataTo() {
    $('#data_process_to').val(JSON.stringify(list_data_process_to));
    for (idx in list_data_process_to) 
    {
        list_data_process_to[idx]['_id'] 	= idx;
        list_data_process_to[idx]['no'] 	= parseInt(idx) + 1;
    }
}
function bindDataCopy() 
{
    $('.btn-delete-process').on('click', deleteProcessFrom);
    $('.btn-delete-process-to').on('click', deleteProcessTo);
}
function deleteProcessFrom() {
    var i = $(this).data('id');
    var getData = list_data_process_from[i];
    if (getData.id!=-1) {
        ajaxActionDelete(getData.id)
    }

    list_data_process_from.splice(getData.id, 1);

    renderDataFrom();
}
function deleteProcessTo() {
    var i = $(this).data('id');
    var getData = list_data_process_to[i];
    if (getData.id!=-1) {
        console.log("to");
    }
    list_data_process_to.splice(getData.id, 1);

    renderDataTo();
}
function ajaxActionDelete(history_id) {
    var url = $("#url_destroy_process_sewing").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            'id': history_id,
        },
        success: function() {
            
        },
        error: function(response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
        $("#alert_success").trigger("click", 'Data Berhasil di hapus');
    });
}
function nextRound() {
    var url        = $("#url_store_switch_round").val();
    var line_id    = $("#line_id").val();
    var factory_id = $("#factory_id").val();
    swal({
        title: "PERHATIAN!!!",
        text: "Anda Akan pindah Round,Klik OK Untuk Pindah Round",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((wilsave) => {
        if (wilsave) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            $.ajax({
                type: "post",
                url: url,
                data: {
                    'line_id'   : line_id,
                    'factory_id': factory_id
                },
                success: function() {
                    
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                }
            })
            .done(function(response) {
                resetNik();
                resetHidden();
                $("#select_machine").val("").trigger("change");
                $("#round_label").text(response.round);
                $("#round_icon").text(response.round);
                $("#round").val(response.round);
                $("#alert_success").trigger("click", 'Pindah Round Berhasil');
            });
        }
    });
}
$('#auto_completes').on('change',function()
{
    available_nik = JSON.parse($('#auto_completes').val());
   
    $("#nik_label").autocomplete({
        source: available_nik
    });
    $("#nik_awal").autocomplete({
        source: available_nik,
        appendTo : "#JoinProcessModal"
    });
    $("#nik_akhir").autocomplete({
        source: available_nik,
        appendTo : "#JoinProcessModal"
    });
});
$("#btnCopy").click(function (e) { 
    e.preventDefault();
    if (parseInt(list_data_process_from.length)<1) {
        $("#alert_error").trigger("click", 'Nik Asal tidak boleh Kosong');
        return false;
    }
    for (i in list_data_process_from) 
    {
        var data  = list_data_process_from[i];
        var check = checkItem(data.history_id);
        if (check) {
            var input = {
                'id'               : -1,
                'history_id'       : data.history_id,
                'machine_id'       : data.machine_id,
                'process_sewing_id': data.process_sewing_id,
                'style'            : data.style,
                'line_id'          : data.line_id,
                'factory_id'       : data.factory_id,
                'machine_name'     : data.machine_name,
                'proses_name'      : data.proses_name,
            }
            list_data_process_to.push(input);
            renderDataTo();
        }
    }
    
});
function checkItem(history_id) {
    
    for (var i in list_data_process_to) {
        var data = list_data_process_to[i];

        if (data.history_id == history_id)
            return false;
    }

    return true;
}
$("#btnSaveCopyProcess").click(function (e) { 
    e.preventDefault();
    var url        = $("#url_store_history_process").val();
    var data      = $("#data_process_to").val();
    var nik_akhir  = $("#nik_akhir").val();
    var name_akhir = $("#name_akhir").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            'items'     : data,
            'sewer_nik' : nik_akhir,
            'sewer_name': name_akhir,
        },
        success: function() {
            
        },
        error: function(response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
        // $("#btnCopyProsesClose").trigger("click");
        $("#alert_success").trigger("click", 'Data Berhasil di Copy');
    });
});
$("#btnCopyProsesClose").click(function (e) { 
    e.preventDefault();
    $('#nik_awal').val("");
    $('#name_awal').val("");
    $('#nik_akhir').val("");
    $('#name_akhir').val("");
    list_data_process_from = [];
    list_data_process_to = [];
    renderDataFrom();
    renderDataTo();
});
function addSelectMachine() {  
    $("#select_machine").empty();
    $("#select_machine").append('<option value="">-- Pilih Jenis Mesin --</option>');
    machines.forEach(element => {
        $("#select_machine").append('<option value="' + element.id + '">' + element.name + '</option>');
    });
}
function sendMessage() {
    var line_id = $("#line_id").val();
    var url     = $("#url_send_message").val();
    var name    = 'send_message';

    
    $('#send_messageModal').modal();
    if (!line_id) {
        $("#alert_error").trigger("click", 'Silahkan pilih Line  terlebih dahulu');
        return false;
    }
    var search = '#' + name + 'Search';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '?line_id=' + line_id + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                //$(table).find('.btn-choose').on('click', chooseItem);
            });
    }
    itemAjax();
}
function send_messageClose() {  
    $('#send_messageModal').modal('toggle');
}
function getNotifData() {
    var line_id = $("#line_id").val();
    var url     = $("#url_get_notification").val();
    if (line_id) {
        $.ajax({
            url: url,
            data: {
                line_id: line_id,
            },
        })
        .done(function (response) {

            if (parseInt(response)>0) {
                new PNotify({
                    title: 'Notifikasi IE Development',
                    text: "Ada pesan baru, Silahkan cek menu kirim pesan utk lebih detail nya",
                    addclass: 'bg-primary'
                });
            }
        });
    }
}
function showComplainWorkflow() {  
    var url     = $("#url_complain_workflow").val();
    var line_id = $("#line_id").val();
    var name    = 'complainWorkflow';
    
    $("#send_messageModal").modal('hide');
    $("#complainWorkflowModal").modal();

    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q           = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url,
                data:{
                    q      : q,
                    line_id: line_id,
                }
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var component     = $(this).data('name');
        var production_id = $("#production_id").val();
        var round         = $("#round").val();
        var get_proses    = $('#url_get_process').val();
        $('#processModal').modal('show');
        // processPicklist('process', production_id,round,get_proses,component);
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}