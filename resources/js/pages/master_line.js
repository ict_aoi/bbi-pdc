$(document).ready(function() {
    var lineTable = $('#lineTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 10,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/master-line/data',
        },
        fnCreatedRow: function(row, data, index) {
            var info = lineTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'code',
            name: 'code',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'factory',
            name: 'factory',
            searchable: false,
            visible: true,
        }, {
            data: 'order',
            name: 'order',
            searchable: true,
            visible: true,
        },{
            data: 'buyer',
            name: 'buyer',
            searchable: true,
            orderable: true
        }, {
            data: 'name',
            name: 'name',
            searchable: false,
            orderable: true
        }, {
            data: 'wft',
            name: 'wft',
            searchable: false,
            orderable: true
        }, {
            data: 'alias',
            name: 'alias',
            searchable: true,
            orderable: true
        }, {
            data: 'action',
            name: 'action',
            searchable: false,
            orderable: true
        } ],

    });

    var dtable = $('#lineTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    // dtable.draw();
});
$('#form').submit(function (event){
    event.preventDefault();
    let factory = $('#factory').val();
    let jenis   = $('#jenis').val();
    let name    = $('#name').val();
    let code    = $('#code').val();
    let order   = $('#order').val();
    let alias   = $('#alias').val();
    let wft     = $('#wft').val();
    
    if(!factory)
    {
        $("#alert_warning").trigger("click", 'Factory wajib diisi');
        return false;
    }

    if(!jenis)
    {
        $("#alert_warning").trigger("click", 'Jenis wajib dipilih');
        return false
    }
    if(!name)
    {
        $("#alert_warning").trigger("click", 'Name wajib dipilih');
        return false
    }
    if(!code)
    {
        $("#alert_warning").trigger("click", 'Code wajib dipilih');
        return false
    }
    if(!order)
    {
        $("#alert_warning").trigger("click", 'Order wajib dipilih');
        return false
    }
    if(!alias)
    {
        $("#alert_warning").trigger("click", 'Alias wajib dipilih');
        return false
    }
    if(!wft)
    {
        $("#alert_warning").trigger("click", 'WFT wajib dipilih');
        return false
    }

    bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
        if(result){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data:new FormData($("#form")[0]),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function () {
                    document.location.href = '/master-line';
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status != 200) $("#alert_warning").trigger("click",response.responseText);
                }
            });
        }
    });
});
function hapus(url) {
    bootbox.confirm("Apakah Anda Yakin Data Akan di Hapus ?.", function(result) {
        if (result) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "PUT",
                url: url,
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    $("#alert_success").trigger("click", "Data Berhasil di Hapus");
                    $('#lineTable').DataTable().ajax.reload();

                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status != 200) $("#alert_warning").trigger("click",response.responseText);
                }
            });
        }
    });
}