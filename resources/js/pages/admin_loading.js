
form_status = $("#form_status").val();
page        = $('#page').val();
base_url    = $('#base_url').val();
if(form_status=='input_loading'){
    var method_scan            = 'scan-terima';
    list_breakdown_size_data = JSON.parse($('#breakdown_size_data').val());
}else if(form_status=='scan_loading'){
    list_scan_loading_data = JSON.parse($('.scan_loading_data').val());
    var method_scan            = 'scan-terima';
    flag_menu              = $("#flag_menu").val();
}
if (page=='laporan') {
    list_laporan_adm_loading_data = JSON.parse($('#laporan_adm_loading_data').val());
}
$('.pickadate-weekday').pickadate({
    firstDay: 1
});
_qcOutput = 0;

$(document).ready( function ()
{
    $('#user_session').trigger('change');
    $('#factory_id').trigger('change');

    if (form_status=='input_loading') {
        $('#form').submit(function (event){
            event.preventDefault();
            var line_name = $('#line_name').val();
            
            if(line_name == 'all')
            {
                $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
                return false;
            }
    
            if(list_breakdown_size_data.length == 0)
            {
                $("#alert_warning").trigger("click", 'Silahkan pilih po buyer dan size terlebih dahulu');
                return false
            }
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data: $('#form').serialize(),
                        cache: false,
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function () {
                            sendAlertAndon();
                            $('#poreference').val('');
                            $('#style').val('');
                            $('#article').val('');
                            $('#poreference_label').text('-');
                            $('#style_label').text('-');
                            $('#article_label').text('-');
    
                            
                            $("#alert_success").trigger("click",'Data berhasil disimpan');
                            list_breakdown_size_data = [];
                            render();
                            //document.location.href = '/pengelolaan-pengguna/pengguna';
                        },
                        error: function (response) {
                            $.unblockUI();
                            
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                            if (response.status == 500) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });
        });
    }
    if (form_status='scan_loading') {
        
        $('#flag_menu').on('change', function() {
            if (flag_menu=='menu-scan') {
                if (method_scan=='scan-terima') {
                    $("#txtScanBarcode").change(function (e) { 
                        e.preventDefault();
                        var barcode = $("#txtScanBarcode").val();
                        if (barcode.length>1) {
                            if (barcode.length < 5) 
                            {
                                $("#alert_warning").trigger("click", 'Barcode tidak boleh kosong');
                                $("#txtScanBarcode").val('')
                                return false;
                            }
                            var url     = $('#url_data_cdms_scan').val();
                            e.preventDefault();
                            $.ajax({
                                type: "GET",
                                url: url,
                                data: {
                                    barcode : barcode,
                                },
                                beforeSend: function () {
                                    $.blockUI({
                                        message: '<i class="icon-spinner4 spinner"></i>',
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.8,
                                            cursor: 'wait'
                                        },
                                        css: {
                                            border: 0,
                                            padding: 0,
                                            backgroundColor: 'transparent'
                                        }
                                    });
                                    $("#txtScanBarcode").val('');
                                },
                                complete: function () {
                                    $.unblockUI();
                                },
                                success: function (response) 
                                {   
                                    for (i in response.data) 
                                    {
                                        let data  = response.data[i];
                                        let diff = checkScanLoading(data.barcode_id,data.style_detail_id);
                                        if (diff) {
                                            let input = {
                                                'barcode_id'            : data.barcode_id,
                                                'poreference'           : data.poreference,
                                                'style'                 : data.style,
                                                'article'               : data.article,
                                                'size'                  : data.size,
                                                'season'                : data.season,
                                                'cut_num'               : data.cut_num,
                                                'start_no'              : data.start_no,
                                                'end_no'                : data.end_no,
                                                'no_sticker'            : data.no_sticker,
                                                'component_name'        : data.component_name,
                                                'qty'                   : data.qty,
                                                'style_detail_id'       : data.style_detail_id,
                                                'source'               : data.source,
                                                // 'distribution_id_before': data.distribution_id_before,
                                            }
                                            list_scan_loading_data.push(input);
                                        }else{
                                            $("#alert_error").trigger('click', 'Barcode Sudah di Scan');
                                        }
                                    }
                                    
                                },
                                error: function (response) 
                                {
                                    $.unblockUI();
                                    
                                    if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
                                    if (response['status'] == 422) $("#alert_error").trigger('click',response.responseJSON.message);
                                    document.getElementById("txtScanBarcode").focus();
                                /*  if (response['status'] != 200)  {
                                        $("#alert_warning").trigger('click', response.responseJSON.message.barcode);
                                    }; */
                                }
                            })
                            .done(function ()
                            {
                                renderScanDistribution();
                                document.getElementById("txtScanBarcode").focus();
                            });
                        }
                    });
                    $('#form_scan').submit(function (event){
                        event.preventDefault();
                        var countLenght = list_scan_loading_data.length;
                        
                        if(countLenght<1)
                        {
                            $("#alert_error").trigger("click", 'Simpan Data Gagal, Silahkan Scan Bundle Garment');
                            return false;
                        }
                
                        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                            if(result){
                                $.ajax({
                                    type: "POST",
                                    url: $('#form_scan').attr('action'),
                                    data: $('#form_scan').serialize(),
                                    cache: false,
                                    beforeSend: function () {
                                        $.blockUI({
                                            message: '<i class="icon-spinner4 spinner"></i>',
                                            overlayCSS: {
                                                backgroundColor: '#fff',
                                                opacity: 0.8,
                                                cursor: 'wait'
                                            },
                                            css: {
                                                border: 0,
                                                padding: 0,
                                                backgroundColor: 'transparent'
                                            }
                                        });
                                    },
                                    complete: function () {
                                        $.unblockUI();
                                    },
                                    success: function () {
                                        list_scan_loading_data=[];
                                        renderScanDistribution();
                                        $("#alert_success").trigger("click",'Simpan Berhasil');
                                        document.getElementById("txtScanBarcode").focus();
                                    },
                                    error: function (response) {
                                        $.unblockUI();
                                        
                                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                                        if (response.status == 500) $("#alert_warning").trigger("click",response.responseJSON);
                                        if (response.status != 200) $("#alert_warning").trigger("click",response.responseJSON);
                                    }
                                });
                            }
                        });
                    });
                } else if(method_scan=='scan-pindah'){
                    $("#txtScanPindahBarcode").change(function (e) { 
                        e.preventDefault();
                        var barcode_id = $("#txtScanPindahBarcode").val();
                        if (barcode_id.length>1) {
                            if (barcode_id.length < 5) 
                            {
                                $("#alert_warning").trigger("click", 'Barcode tidak boleh kosong');
                                $("#txtScanPindahBarcode").val('')
                                return false;
                            }
                            let url     = base_url+'/scan-distribusi-movement';
                            e.preventDefault();
                            getDataScan(url,barcode_id);
                        }
                    });

                    $('#form_pindah').submit(function (event){
                        event.preventDefault();
                        var countLenght = list_scan_loading_data.length;
                        
                        if(countLenght<1)
                        {
                            $("#alert_error").trigger("click", 'Simpan Data Gagal, Silahkan Scan Bundle Garment');
                            return false;
                        }
                
                        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
                            if(result){
                                $.ajax({
                                    type: "POST",
                                    url: $('#form_pindah').attr('action'),
                                    data: $('#form_pindah').serialize(),
                                    cache: false,
                                    beforeSend: function () {
                                        $.blockUI({
                                            message: '<i class="icon-spinner4 spinner"></i>',
                                            overlayCSS: {
                                                backgroundColor: '#fff',
                                                opacity: 0.8,
                                                cursor: 'wait'
                                            },
                                            css: {
                                                border: 0,
                                                padding: 0,
                                                backgroundColor: 'transparent'
                                            }
                                        });
                                    },
                                    complete: function () {
                                        $.unblockUI();
                                    },
                                    success: function () {
                                        list_scan_loading_data=[];
                                        renderScanDistribution();
                                        $("#alert_success").trigger("click",'Simpan Berhasil');
                                        document.getElementById("txtScanPindahBarcode").focus();
                                    },
                                    error: function (response) {
                                        $.unblockUI();
                                        
                                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                                        if (response.status == 500) $("#alert_warning").trigger("click",response.responseJSON);
                                        if (response.status != 200) $("#alert_warning").trigger("click",response.responseJSON);
                                    }
                                });
                            }
                        });
                    });
                }
            }
            if (flag_menu=='menu-laporan') {
                $('#poreference').select2({
                    placeholder: 'Cari berdasarkan Po/Style..',
                    ajax: {
                        url: base_url+'/poreference-search',
                        delay:1000,
                        dataType: 'json',
                        processResults: function (data) {
                        return {
                            results:  $.map(data.production, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                            })
                        };
                        },
                        cache: true
                    },
                    minimumInputLength: 5
                });
                $('#poreference').on('change', function() {
                    let data        = $("#poreference option:selected").text();
                    let style       = data.split('|')[0];
                    let poreference = data.split('|')[1];
                    let article     = data.split('|')[2];
                    let production_id = $("#poreference").val(data);
        
                    $("#style").val(style);
                    $("#article").val(article);
                    $(".poreference").val(poreference);
                    $('#poreferenceDiv').addClass("hidden");
                    $("#div-po").removeClass("hidden");
                    let poConcat     = poreference+"|"+style+"|"+article;
                    let url = base_url+'/distribution-get-size';
                    $("#poreferenceName").val(data);
                    getSize(poConcat,url);
                });
                
                let reportScanBundleTable = $('#reportScanBundleTable').DataTable({
                    dom        : 'Bfrtip',
                    processing : true,
                    serverSide : true,
                    pageLength : 20,
                    deferRender: true,
                    ajax       : {
                        type: 'GET',
                        url: base_url+'/laporan-data-bundle',
                        data: function(d) {
                                return $.extend({}, d, {
                                    "date"       : $('#txtDate').val(),
                                    "poreference": $('.poreference').val(),
                                    "style"      : $('#style').val(),
                                    "article"    : $('#article').val(),
                                    "size"       : $('#selectSize').val(),
                                });
                        }
                    },
                    fnCreatedRow: function (row, data, index) {
                        var info = reportScanBundleTable.page.info();
                        var value = index+1+info.start;
                        $('td', row).eq(0).html(value);
                    },
                    columns: [
                        {data: null,sortable: false,orderable: false,searchable: false,defaultContent:''},
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            orderable: false
                        },{
                            data: 'date',
                            name: 'date',
                            searchable: true,
                            visible: true,
                        }, {
                            data: 'style',
                            name: 'style',
                            searchable: true,
                            orderable: true
                        }, {
                            data: 'poreference',
                            name: 'poreference',
                            searchable: true,
                            orderable: true
                        }, {
                            data: 'article',
                            name: 'article',
                            searchable: true,
                            orderable: true
                        }, {
                            data: 'size',
                            name: 'size',
                            searchable: true,
                            orderable: true
                        }, {
                            data: 'cut_num',
                            name: 'cut_num',
                            searchable: false,
                            orderable: false
                        }, {
                            data: 'no_sticker',
                            name: 'no_sticker',
                            searchable: false,
                            orderable: false
                        }, {
                            data: 'qty',
                            name: 'qty',
                            searchable: false,
                            orderable: false
                        }, {
                            data: 'jenis',
                            name: 'jenis',
                            searchable: false,
                            orderable: false
                        }, {
                            data: 'status',
                            name: 'status',
                            searchable: false,
                            orderable: false
                        },
                    ],
                
                });
                
                
                let dtableBundle = $('#reportScanBundleTable').dataTable().api();
                $(".dataTables_filter input")
                    .unbind() // Unbind previous default bindings
                    .bind("keyup", function(e) { // Bind our desired behavior
                        // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtableBundle.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtableBundle.search("").draw();
                    }
                    return;
                });
                $("#btnTampilkanBundle").click(function (e) { 
                    e.preventDefault();
                    dtableBundle.draw();
                    
                });
                $("#btnBundleClear").click(function (e) { 
                    e.preventDefault();
                    dtableBundle.destroy();       
                    console.log('asdas');             
                });
            }
        });
        setFocusMethod();
        
    }
    if(page == 'laporan')
    {
        var url_laporan_adm_loading_data = $("#url_laporan_adm_loading_data").val();
        var url_get_poreference_report = $("#url_get_poreference_report").val();
        var reportAdmLoadingTable = $('#reportAdmLoadingTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:20,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url_laporan_adm_loading_data,
                data: function(d) {
                        return $.extend({}, d, {
                            "date"       : $('#txtDate').val(),
                            "line_id"    : $('#line_id').val(),
                            "poreference": $('#poreferenceName').val(),
                            "size"       : $('#selectSize').val(),
                        });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = reportAdmLoadingTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null,sortable: false,orderable: false,searchable: false},
                {
                    data: 'code',
                    name: 'code',
                    searchable: true,
                    visible: false,
                    orderable: true
                }, {
                    data: 'date',
                    name: 'date',
                    searchable: true,
                    visible: true,
                }, {
                    data: 'style',
                    name: 'style',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'poreference',
                    name: 'poreference',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'article',
                    name: 'article',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'size',
                    name: 'size',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'total_qty_loading',
                    name: 'total_qty_loading',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'qty_loading',
                    name: 'qty_loading',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'total_qc_output',
                    name: 'total_qc_output',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'qc_output',
                    name: 'qc_output',
                    searchable: false,
                    orderable: false
                }, {
                    data: 'balance',
                    name: 'balance',
                    searchable: false,
                    orderable: false
                },
            ],
        
        });
        
        var dtable = $('#reportAdmLoadingTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function(e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        $("#btnTampilkan").click(function (e) { 
            e.preventDefault();
            dtable.draw();
            
        });
        $("#btnReset").click(function (e) { 
            e.preventDefault();
            resetBtnReport();
        });
    }
    $("#flag_menu").trigger("change");
    $("#btnBundleClear").trigger("click");
});

$('#printout').on('click',function()
{
    //var url = $('#url_printout').va();
    var laporan_adm_loading_data    = $('#laporan_adm_loading_data').val();
    var url                         = $('#url_printout').val();
    

    
    if(laporan_adm_loading_data.length == 0)
    { 
        $("#alert_warning").trigger("click", 'Silahkan pilih size yg mau dicetak');
        return false;
    }

    window.open(url+'?laporan_adm_loading_data='+laporan_adm_loading_data, 'barcode','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
});

$('#user_session').on('change',function(){
    var value = $(this).val();
    if(value == -1)
    {
        $('#modal_login_adm_loading').modal();
    }
});

$('#login_adm_loading').submit(function (event) 
{
    event.preventDefault();
    var nik 	= $('#nik_adm_loading').val();
    
    if (!nik) 
    {
        $("#alert_warning").trigger("click", 'Silahkan masukan nik anda terlebih dahulu');
        return false;
    }

    $.ajax({
        type: "POST",
        url: $('#login_adm_loading').attr('action'),
        data: $('#login_adm_loading').serialize(),
        beforeSend: function () 
        {
            $('#modal_login_adm_loading').find('.admin-loading-shade-screen').removeClass('hidden');
            $('#modal_login_adm_loading').find('#admin-loading-body-login').addClass('hidden');
        },
        complete: function () 
        {
            $('#modal_login_adm_loading').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_adm_loading').find('#admin-loading-body-login').removeClass('hidden');
        },
        success: function () 
        {
            $('#modal_login_adm_loading').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_adm_loading').find('#admin-loading-body-login').removeClass('hidden');
            $('#login_adm_loading').trigger("reset");
        },
        error: function (response) 
        {
            $('#modal_login_adm_loading').find('.admin-loading-shade-screen').addClass('hidden');
            $('#modal_login_adm_loading').find('#admin-loading-body-login').removeClass('hidden');
            
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
        }
    })
    .done(function (request)
    {
        document.location.href = request;
    });
});

$('#factory_id').change(function () 
{
    var url        = $('#url_line_picklist').val();
	var factory_id = $(this).val();
    linePicklist('line',factory_id, url+'?');
});

$('#poreferenceButtonLookup').click(function () 
{
    var url        = $('#url_poreference_picklist').val();
    var line_id    = $('#line_id').val();
    var factory_id = $('#factory_id').val();
    var line_name  = $('#line_name').val();

    if(line_name == 'all')
    {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    $("#poreferenceModal").modal("show");
    poreferencePicklist('poreference',line_id,factory_id, url+'?');
    
});

$('#poreferenceName').click(function () 
{
    var url        = $('#url_poreference_picklist').val();
    var line_id    = $('#line_id').val();
    var factory_id = $('#factory_id').val();
    var line_name  = $('#line_name').val();

    if(line_name == 'all')
    {
        $("#alert_warning").trigger("click", 'Silahkan pilih line terlebih dahulu');
        return false;
    }
    poreferencePicklist('poreference',line_id,factory_id, url+'?');
});

$('#loginButtonKeyboard').click(function()
{
    if($('#login-keyboard').is(":hidden")) $('#login-keyboard').removeClass('hidden');
    else $('#login-keyboard').addClass('hidden');
});

$('#poreferenceButtonKeyboard').click(function()
{
    if($('#poreferenceKeyboard').is(":hidden")) $('#poreferenceKeyboard').removeClass('hidden');
    else $('#poreferenceKeyboard').addClass('hidden');
});


$('#lineButtonKeyboard').click(function()
{
    if($('#lineKeyboard').is(":hidden")) $('#lineKeyboard').removeClass('hidden');
    else $('#lineKeyboard').addClass('hidden');
});

function poreferencePicklist(name,line_id,factory_id, url) 
{
	var search 		= '#' + name + 'Search';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
    var buttonSrc 	= '#' + name + 'ButtonSrc';
    $(search).val('');
    
    function itemAjax()
    {
        var q = $(search).val();
        
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&line_id=' + line_id + '&factory_id=' + factory_id + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');
           $(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		let poreference     = $(this).data('buyer');
		let style           = $(this).data('style');
        let article         = $(this).data('article');
        let url             = $('#url_breakdown_size_data').val();

        var page = $('#page').val();
        if(page == 'laporan')
        {
            let poConcat     = poreference+"|"+style+"|"+article;
            let url_get_size = $("#url_get_size").val();
            $("#poreferenceName").val(poConcat);
            getSize(poConcat,url_get_size);
        }else if(page == 'home')
        {
            $('#poreference').val(poreference);
            $('#style').val(style);
            $('#article').val(article);
            $('#poreference_label').text(poreference);
            $('#style_label').text(style);
            $('#article_label').text(article);
            
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    poreference : poreference,
                    style       : style,
                    article     : article,
                    line_id     : line_id,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                    list_breakdown_size_data = [];
                    render();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) 
                {
                    for (i in response) 
                    {
                        var data  = response[i]
                        var input = {
                            'checked'              : false,
                            'id'                   : data.id,
                            'production_id'        : data.production_id,
                            'size'                 : data.size,
                            'qty_order'            : data.qty_order,
                            'qty_reserved'         : data.qty_reserved,
                            'qty_outstanding'      : data.qty_input,
                            'qty_input': 0,
                        }
                        
                        list_breakdown_size_data.push(input);
                    }
                    
                },
                error: function (response) 
                {
                    $.unblockUI();
                   
                    if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
        
                    if (response['status'] == 422)  $("#alert_warning").trigger('click', response.responseJSON);
                }
            })
            .done(function ()
            {
                render();
            });
        }

        
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

    itemAjax();
}

function linePicklist(name,factory_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
    $(search).val('');
    function itemAjax(){
        
        var q = $(search).val();
        
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q +'&factory_id=' + factory_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
        
        $(item_id).val(id);
		$(item_name).val(name);
		$('#line_id').val(id);
		$('#line_name').val(name);
		
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}

function laporanAdmLoading(url,poreference,style,article,page) 
{
    function itemAjax()
    {
        $.ajax({
            url: url,
            data: {
                poreference : poreference,
                style       : style,
                article     : article,
                page        : page,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
        })
        .done(function(result)
        {
            
            if(page == 'print_barcode')
            {
                for (idx in result) 
                {
                    var input = result[idx];
                    list_laporan_adm_loading_data.push(input);
                }
                renderLaporanAdmLoading();
            }
            else
            {
                list_laporan_adm_loading_data = result;
                renderLaporanAdmLoading();
            }
            
            
        });
	}

	itemAjax();
}
function poreferencePickList(url,poreference,style,article,page) {
    
}

function render() 
{
	getIndex();
    $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
    var tmpl 		= $('#breakdown_size_data_table').html();
	Mustache.parse(tmpl);
    var data 		= { list: list_breakdown_size_data };
    var html 		= Mustache.render(tmpl, data);
    
	$('#breadown_size_data_tbody').html(html);
    bind();
}
function getIndex() 
{
	for (idx in list_breakdown_size_data) 
	{
		list_breakdown_size_data[idx]['_id'] 	= idx;
		list_breakdown_size_data[idx]['no'] 	= parseInt(idx) + 1;
	}
}
function bind() 
{
    /* $('.checkbox_item').on('click', checkedUnChecked);
    $('.btn-keyboard-size').on('click', inputQtyPerSize);
    $('#btn-save-input-size').on('click', saveQtySize); */
    $('.btn-edit-qty').on('click', editQty);
    $('.btn-save-qty').on('click', saveQty);
    $('.btn-cancel-qty').on('click', cancelEdit);
    $('.btn-delete-qty').on('click', deleteQty);
}
function renderScanDistribution() 
{
	getIndexScanDistribution();
    $('.scan_loading_data').val(JSON.stringify(list_scan_loading_data));
    var tmpl 		= $('#scan_loading_data_table').html();
	Mustache.parse(tmpl);
    var data 		= { list: list_scan_loading_data };
    var html 		= Mustache.render(tmpl, data);
    
	$('.scan_loading_tbody').html(html);
    bindScanDistribution();
}


function getIndexScanDistribution() 
{
	for (idx in list_scan_loading_data) 
	{
		list_scan_loading_data[idx]['_id'] 	= idx;
		list_scan_loading_data[idx]['no'] 	= parseInt(idx) + 1;
	}
}
function bindScanDistribution() 
{
    $('.btn-delete-qty').on('click', deleteDistributionQty);
}
function checkScanLoading(barcodeId,styleDetailId) {
    for (var i in list_scan_loading_data) {
        var data = list_scan_loading_data[i];

        if (data.barcode_id == barcodeId&&data.style_detail_id==styleDetailId)
            return false;
    }
    return true;
}
function editQty() {
    var i = $(this).data('id');
    $('#qtyLoading_' + i).addClass('hidden');

    //remove hidden textbox
    $('#qtyLoadingInput_' + i).removeClass('hidden');
    $('#qtyLoadingInput_' + i).select();

    $('#editQty_'+i).addClass("hidden");
    $('#simpanQty_' + i).removeClass('hidden');
    $('#cancelQty_' + i).removeClass('hidden');
}
function saveQty() {
    var i    = $(this).data('id');
    var qty  = $('#qtyLoadingInput_' + i).val();
    var data = list_breakdown_size_data[i];
    if (qty == 0) {
        $("#alert_error").trigger("click", 'Qty Loading Tidak boleh 0');
        $('#qtyLoadingInput_' + i).focus();
        return false;
    }
    if (parseInt(data.qty_outstanding)<parseInt(qty)) {
        $("#alert_error").trigger("click", 'Qty Input tidak boleh lebih dari qty Order');
        $('#qtyLoadingInput_' + i).focus();
        return false;
    }
    if (data.id!='-1') {
        if (parseInt(qty)<0) {
            getOutputQc(data.id,data,qty,i);
        }
    } else {
        if (data.qty_reserved!=0) {
            var cekQty = parseInt(data.qty_reserved)+parseInt(qty);
            if (cekQty < 0) {
                $("#alert_error").trigger("click", 'Qty Loading Tidak boleh 0');
                $('#qtyLoadingInput_' + i).focus();
                return false;
            }
        }
    }
    
    data.qty_input = qty;
    data.qty_outstanding = qty;
    $('#qtyLoadingInput_' + i).addClass('hidden');
    $('#qtyLoading_' + i).removeClass('hidden');

    $('#editQty_'+i).removeClass("hidden");
    $('#simpanQty_' + i).addClass('hidden');
    $('#cancelQty_' + i).addClass('hidden');
    render();
}
function cancelEdit() {
    var i = $(this).data('id');
    var qty  = $('#qtyLoading_' + i).val();
    var data = list_breakdown_size_data[i];
    $('#qtyLoadingInput_' + i).removeClass('hidden');

    //add hidden textbox
    $('#qtyLoading_' + i).addClass('hidden');

    $('#simpanQty_' + i).addClass('hidden');
    $('#cancelQty_' + i).addClass('hidden');
    $('#editQty_' + i).removeClass('hidden');
    qty = data.qty_input;
    render();
}
function deleteQty() {
    var i = $(this).data('id');
    var data = list_breakdown_size_data[i];
    var url_delete_size = $('#url_delete_size').val();

    bootbox.confirm("Apakah Size ini akan di hapus ?.", function(result) {

        if (result) {
            if (data.id!='-1') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                        type: "put",
                        url: url_delete_size,
                        data: {
                            'size_id': data.id
                        },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        success: function() {
                            $.unblockUI();
                            list_breakdown_size_data.splice(i, 1);
                            render();
                            sendAlertAndon();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status != 200) $("#alert_warning").trigger("click", response.responseJSON.message);
                            document.getElementById("txtScanBarcode").focus();
                        }
                    })
                    .done(function() {
                        $("#alert_success").trigger("click", 'Data successfully deleted');
                        document.getElementById("txtScanBarcode").focus();
                    });
            }else{
                list_breakdown_size_data.splice(i, 1);
                render();
            }
            
        }

    });
}
function inputQtyPerSize()
{
    var id      = $(this).data('id');
    var data    = list_breakdown_size_data[id];

    if(!data.checked)
    {
        $("#alert_warning").trigger("click", 'Qty tidak dapat dirubah karna status size ini tidak di ceklis');
        return false;
    }
    
    $('#size_id').val(id);
    $('#size').val(data.size);
    $('#qty_size').val(data.qty_outstanding);
    $('#modalInputSize').modal();
}

function saveQtySize(data)
{
    var size_id             = $('#size_id').val();
    var qty_size            = $('#qty_size').val();
    var data                = list_breakdown_size_data[size_id];
    $('#'+size_id+'Size').val(qty_size);
    data.qty_outstanding    = qty_size;
    if (data.id!='-1') {
        var outputQc = getOutputQc(data.id);
        
        var balance = parseInt(outputQc)-parseInt(qty_size);
        if (balance<0) {
            $("#alert_warning").trigger("click", 'Qty tidak dapat dirubah karna sudah di input QC Endlline');
            return false;
        }
        
    }
    $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
}
function getOutputQc(size_id,data,qty,i) {
    var url_get_output_qc = $("#url_get_output_qc").val();
    $.ajax({
        type: "GET",
        url: url_get_output_qc,
        data: {
            size_id : size_id,
            data    :data,
            qty     :qty,
            i       :i
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) 
        {
            $.unblockUI();
            
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

            if (response['status'] == 422)
            $("#alert_warning").trigger('click', response.responseJSON.message);
        }
    })
    .done(function(response)
    {
        var qc_output   = response.total_output;
        var old         = response.data;
        var qty         = response.qty;
        var i           = response.i;
        var data        = list_breakdown_size_data[i];
       
        var qty_reserved = parseInt(old.qty_reserved);
        var balance = parseInt(qty_reserved)+parseInt(qty);
        if (parseInt(balance)<parseInt(qc_output)) {
            $("#alert_error").trigger("click", 'Qty Sudah Balance tidak bisa dikurangi');
            data.qty_input          =  old.qty_input;
            data.qty_outstanding    = old.qty_outstanding;

            render();
            //return false;
        }//else return true;
    });
}
function checkedUnChecked()
{
    var id      = $(this).data('id');
    var data    = list_breakdown_size_data[id];

    if (document.getElementById('check_'+id).checked) 
	{
        data.checked = true;
        $("#check_"+id).attr("checked","checked");
    }else
    {
        data.checked = false;
        $("#check_"+id).removeAttr("checked");
    }

    $('#breakdown_size_data').val(JSON.stringify(list_breakdown_size_data));
}

function renderLaporanAdmLoading() 
{
	getIndexLaporan();
    $('#laporan_adm_loading_data').val(JSON.stringify(list_laporan_adm_loading_data));
   
    var tmpl 		= $('#laporan_adm_loading_table').html();
	Mustache.parse(tmpl);
    var data 		= { list: list_laporan_adm_loading_data };
    var html 		= Mustache.render(tmpl, data);
    
    $('#laporan_adm_loading_tbody').html(html);
    bindLaporan();
}

function getIndexLaporan() 
{
    for (idx in list_laporan_adm_loading_data) 
	{
		list_laporan_adm_loading_data[idx]['_id'] 	                = idx;
        list_laporan_adm_loading_data[idx]['no'] 	                = parseInt(idx) + 1;
        
        for (idy in list_laporan_adm_loading_data[idx].details) 
        {
            list_laporan_adm_loading_data[idx].details[idy]['_idy'] 	= idy;
            list_laporan_adm_loading_data[idx].details[idy]['nox'] 	    = parseInt(idy) + 1;
        }
	}
}

function bindLaporan()
{
    $('.checkbox-checked-header').on('click', checkedHeader);
    $('.checkbox-unchecked-header').on('click', uncheckedHeader);
    $('.checkbox-checked-line').on('click', checkedLine);
    $('.checkbox-unchecked-line').on('click', uncheckedLine);
    $('.btn-delete-item').on('click', deleteHeader);
}

function checkedHeader()
{
    var i       = $(this).data('id');
    var data    = list_laporan_adm_loading_data[i];

    data.is_selected = true;

    var _selected = parseInt(data.total_line_selected);
    for (idy in data.details) 
    {
        var detail = data.details[idy];
        detail.is_selected = true;
        data.total_line_selected  = _selected+parseInt(1);
    }
    
    renderLaporanAdmLoading();
}


function uncheckedHeader()
{
    var i       = $(this).data('id');
    var data    = list_laporan_adm_loading_data[i];

    data.is_selected = false;

    var _selected = parseInt(data.total_line_selected);
    for (idy in data.details) 
    {
        var detail = data.details[idy];
        detail.is_selected = false;
        data.total_line_selected  = _selected-parseInt(1);
    }

    renderLaporanAdmLoading();
}

function checkedLine()
{
    var i = $(this).data('id');
    var y = $(this).data('idy');

    var data                    = list_laporan_adm_loading_data[i];
    var _selected               = parseInt(data.total_line_selected);
    var detail                  = list_laporan_adm_loading_data[i].details[y];
    detail.is_selected          = true;

    _selected += parseInt(1);
    data.total_line_selected    = _selected;
    
    if(_selected > 0) data.is_selected = true;
    renderLaporanAdmLoading();
}

function uncheckedLine()
{
    var i = $(this).data('id');
    var y = $(this).data('idy');

    var data                    = list_laporan_adm_loading_data[i];
    var _selected               = parseInt(data.total_line_selected);
    var detail                  = list_laporan_adm_loading_data[i].details[y];
    detail.is_selected          = false;

    _selected -= parseInt(1);

    data.total_line_selected    = _selected;

    if(_selected == 0) data.is_selected = false;
    renderLaporanAdmLoading();
}

function deleteHeader()
{
    var i = $(this).data('id');
    list_laporan_adm_loading_data.splice(i, 1);
    renderLaporanAdmLoading();
}
function reboot() {
    var url_ssh_reboot = $("#url_ssh_reboot").val();
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_reboot,
        beforeSend: function() {
            $('#shutdownModal').modal('toggle');
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {
    });
}
function shutdown() {
    var url_ssh_shutdown = $("#url_ssh_shutdown").val();
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
        type: "post",
        url: url_ssh_shutdown,
        beforeSend: function() {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

        },
        error: function(response) {
            $.unblockUI();
            if (response['status'] == 500)
                $("#alert_error").trigger("click", response.responseJSON.message);
            if (response['status'] == 422||response['status']==419)
                $("#alert_error").trigger("click", response.responseJSON.message);
        }
    })
    .done(function() {

    });
}
function sendAlertAndon() {  
    let url     = $("#url_send_alert_andon").val();
    let line_id = $("#line_id").val();
    $.ajax({
        type: 'get',
        url: url,
        data:{
            'line_id': line_id,
        }
    })
    .done(function(data)
    {
    });
    
}
function resetBtnReport() {  
    $("#txtDate").val('');
    $("#txtDate").attr("placeholder", "Pilih Tanggal");
    $("#poreferenceName").val('');
    $("#poreferenceName").attr("placeholder", "Silahkan Pilih Po Buyer");
    $("#selectSize").empty();
    $("#selectSize").append('<option value="">-- Pilih Size --</option>');
}
$('#report_bundle').click(function (e) { 
    e.preventDefault();
    switch (flag_menu) {
        case 'menu-scan':
            flag_menu='menu-laporan';
            $("#flag_menu").trigger("change");
            $("#menu-scan").fadeOut();
            $("#laporan").removeClass('hidden');
            $("#menu-scan").addClass('hide');
            $(".menu-setting").addClass('hide');
            $("#laporan").fadeIn();
            $("i", this).toggleClass("icon-drawer3 icon-home");            
            $('#report_bundle').not(this).find("i").removeClass("icon-home").addClass("icon-drawer3");
            // $("#btnBundleClear").trigger("click");
            // $("#btnTampilkanBundle").trigger("click");
            console.log('menu-scan');
            setFocusMethod();
            break;
            case 'menu-laporan':
            flag_menu='menu-scan';
            $("#laporan").fadeOut();
            $("#menu-scan").removeClass('hide');
            $("#laporan").addClass('hidden');
            $("#menu-scan").fadeIn();
            $("#flag_menu").trigger("change");
            $("#btnBundleClear").trigger("click");
            $(".menu-setting").removeClass('hide');
            $("i", this).toggleClass("icon-home icon-drawer3");
            $('#report_bundle').not(this).find("i").removeClass("icon-drawer3").addClass("icon-home");
            $('#poreferenceDiv').addClass("hidden");
            $("#div-po").removeClass("hidden");
            console.log('menu-laporan');
            setFocusMethod();
            break
        default:
            $("#alert_error").trigger("click", 'Ada Kesalahan Silahkan Refresh Halaman Anda');
            location.reload();
            break;
    }
});
function callBack() {  
    $('#poreferenceDiv').removeClass("hidden");
    $("#div-po").addClass("hidden");
    resetDataReport();
}
function clearData(params) {  
    switch (params) {
        case 'style':
            $("#style").val('');
            break;
        case 'poreference':
            $(".poreference").val('');
            break;
        case 'article':
            $("#article").val('');
            break;
        default:
            $("#alert_error").trigger("click", 'Ada Kesalahan Silahkan Refresh Halaman Anda');
            location.reload();
            break;
    }
    $("#production_id").val('');
    let style       = $("#style").val();
    let article     = $("#article").val();
    let poreference = $(".poreference").val();
    let poConcat    = poreference+"|"+style+"|"+article;
    let url         = base_url+'/distribution-get-size';
    getSize(poConcat,url);
}
function resetDataReport() {  
    $("#style").val('');
    $(".poreference").val('');
    $("#article").val('');
    $("#production_id").val('');
    $("#poreference").empty();
}
function getSize(poreference,url) {  
    
    if (poreference) {
        
        $.ajax({
                type: 'get',
                url: url,
                data:{
                    poreference:poreference
                }
            })
            .done(function(response) {
                var size = response.size;

                $("#selectSize").empty();
                $("#selectSize").append('<option value="">-- Pilih Size --</option>');

                $.each(size, function(size, size) {

                    $("#selectSize").append('<option value="' + size + '">' + size + '</option>');
                });
            })
    } else {

        $("#selectSize").empty();
        $("#selectSize").append('<option value="">-- Pilih Size --</option>');
    }
}

function lovBundle(url) {
    $("#detailBundleModal").modal("show");
    lovDetailBundle('detailBundle', url);
}
function lovDetailBundle(name,url){
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {

        var q                   = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url,
            data: {
                q: q
            },
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

        });
    }
    

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');
            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function detailBundleClose() {
    $('#detailBundleModal').modal('toggle');
}
function menuScan(menu) {

    switch (menu) {
        case 'menu_scan_terima':
            list_scan_loading_data=[];
            renderScanDistribution();
            method_scan='scan-terima';
            $(".menu_scan_pindah").removeClass('active');
            $(".menu_scan_terima").addClass('active');
            $(".scan-terima").removeClass('hidden');
            $(".scan-pindah").addClass('hidden');
            $("#flag_menu").trigger("change");
            document.getElementById("txtScanBarcode").focus();
            
            break;
        case 'menu_scan_pindah':
            method_scan='scan-pindah';
            list_scan_loading_data=[];
            renderScanDistribution();
            let url = base_url+'/data-temp-distribution-movement'
            $(".menu_scan_terima").removeClass('active');
            $(".menu_scan_pindah").addClass('active');
            $(".scan-pindah").removeClass('hidden');
            $(".scan-terima").addClass('hidden');
            $("#flag_menu").trigger("change");
            document.getElementById("txtScanPindahBarcode").focus();
            getDataScan(url);
            break;
        default:
            $("#alert_error").trigger("click", 'Ada Kesalahan Silahkan Refresh Halaman Anda');
            location.reload();
            break;
    }
}
function setFocusMethod() {  
    if (method_scan=='scan-terima') {
        $("#txtScanBarcode").val(' ');
        document.getElementById("txtScanBarcode").focus();
    }else if(method_scan=='scan-pindah'){
        $("#txtScanPindahBarcode").val(' ');
        document.getElementById("txtScanPindahBarcode").focus();  
    }
}
function getDataScan(url,barcode_id=null) {  
    $.ajax({
        type: "GET",
        url: url,
        data: {
            barcode : barcode_id,
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            setFocusMethod();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) 
        {   
            let count = response.data.length;
            
            if (parseInt(count)>0) {
                for (i in response.data) 
                {
                    let data = response.data[i];
                    let diff = checkScanLoading(data.barcode_id,data.style_detail_id);
                    if (diff) {
                        let input = {
                            'barcode_id'            : data.barcode_id,
                            'distribution_id'       : data.distribution_id,
                            'distribution_detail_id': data.distribution_detail_id,
                            'poreference'           : data.poreference,
                            'style'                 : data.style,
                            'article'               : data.article,
                            'size'                  : data.size,
                            'season'                : data.season,
                            'cut_num'               : data.cut_num,
                            'start_no'              : data.start_no,
                            'end_no'                : data.end_no,
                            'no_sticker'            : data.no_sticker,
                            'component_name'        : data.component_name,
                            'qty'                   : data.qty,
                            'style_detail_id'       : data.style_detail_id,
                            'source'               : data.sources,
                            // 'distribution_id_before': data.distribution_id_before,
                        }
                        list_scan_loading_data.push(input);
                    }else{
                        $("#alert_error").trigger('click', 'Barcode Sudah di Scan');
                    }
                }
            } else {
                list_scan_loading_data=[];
            }
     
        },
        error: function (response) 
        {
            $.unblockUI();
            
            if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
            if (response['status'] == 422) $("#alert_error").trigger('click',response.responseJSON.message);
            setFocusMethod();
        }
    })
    .done(function ()
    {
        renderScanDistribution();
        setFocusMethod();
    });
}
function deleteDistributionQty() {  
    if (method_scan=='scan-pindah') {
        var i = $(this).data('id');
        var data = list_scan_loading_data[i];
        var url =base_url+'/delete-temp-distribution';

        bootbox.confirm("Apakah Bundle ini akan di hapus ?.", function(result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "put",
                    url: url,
                    data: {
                        'barcode_id': data.barcode_id
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function() {
                        $.unblockUI();
                        let urlgetdata     = base_url+'/data-temp-distribution-movement';
                        getDataScan(urlgetdata);
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status != 200) $("#alert_warning").trigger("click", response.responseJSON.message);
                        setFocusMethod();
                    }
                })
                .done(function() {
                    $("#alert_success").trigger("click", 'Data successfully deleted');
                    setFocusMethod();
                });
            }

        });
    }else{
        list_scan_loading_data.splice(i, 1);
        renderScanDistribution();
    }
    
}
function send() {  
    
}