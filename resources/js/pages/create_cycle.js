    items       = JSON.parse($('#pick_time_data').val());
    processList = JSON.parse($('#process_pick_list').val());
    
var base,        baseLap, playPause = 0;
    /* default Value */
    var milisecond = 0,second=0, minute=0,milisecond2=0,second2=0,minute2=0;

    /* return value */
    var milisecondVal=0,secondVal=0,minuteVal=0,milisecondLap=0,secondLap=0,minuteLap=0;
    $(document).ready(function () {
        $('.sidebar-control').trigger("click");
        $('#lineButtonLookup').click(function () {
            $('#lineName').trigger('click');
        
        });
        $('#lineName').click(function () {
            linePicklist('line');
        
        });
        $('#styleName').click(function () {
            stylePicklist('style');
        
        });
        $("#prosesName").click(function (e) { 
            e.preventDefault();
            var nik = $("#nik_label").val();
            var name = $("#name_label").val();
            if (!nik) {
                $("#alert_error").trigger("click", 'NIK belum di input');
                return false
            }
        
            if (!name) {
                $("#alert_error").trigger("click", 'Nama belum di input');
                return false
            }
            $('#processModal').modal('show');
            processPicklist('process','/skill-matriks/cycle-time/get-process');
        });
        $("#prosesButtonLookup").click(function (e) { 
            e.preventDefault();
            let nik        = $("#nik_label").val();
            let name       = $("#name_label").val();
            let workflowId = $("#workflowId").val();
            if (!nik) {
                $("#alert_error").trigger("click", 'NIK belum di input');
                return false
            }
        
            if (!name) {
                $("#alert_error").trigger("click", 'Nama belum di input');
                return false
            }
            /* $('#processModal').modal('show');
            processPicklist('process','/skill-matriks/cycle-time/get-process'); */
            $('#componentModal').modal('show');
            componentPicklist('component',workflowId,'/skill-matriks/cycle-time/get-component-process');
        });

        $('#btn-close').click(function (e) { 
            e.preventDefault();
            resetFunc();
            stopFunc();
            playPause =0;
            items = [];
            $("#cycle_time_id").val("0");
            $("#btn-draft").removeClass("hidden");
            render();
        });
        
        $('#auto_completes').on('change',function()
        {
            available_nik = JSON.parse($('#auto_completes').val());
        
            $("#nik_label").autocomplete({
                source: available_nik
            });
        });
        $('#form').submit(function (event) {
            event.preventDefault();
            var flag = $("#flag").val();
            if(parseInt(items.length)==0){
                $("#alert_error").trigger("click", 'tidak ada data yang di simpan');
                return false
            }
            if (flag=='1') {
                $("#alert_error").trigger("click", 'Timer Sedang jalan, Silahkan Pause Terlebih dahulu');
                return false
            }

            if(parseInt(items.length)<=2){
                $("#alert_error").trigger("click", 'Cycle Time Kurang Dari 3');
                return false
            }
            $('#pickTimeModal').modal('toggle');
            saveCycle();
            
        });
        getSewer();
    });
    function getSewer() {
        var cycle_time_batch_id = $("#cycle_time_batch_id").val(); 
        $.ajax({
            type: 'get',
            url: '/skill-matriks/cycle-time/get-sewer',
            data:{
                'cycle_time_batch_id':cycle_time_batch_id
            }
        })
        .done(function(response){
            $('#auto_completes').val(JSON.stringify(response)).trigger('change');
        })
    }
    function saveCycle(){
        $.ajax({
            type: "POST",
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            cache: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#btn-close").trigger("click");
                resetData();
                $("#alert_success").trigger("click", 'Data berhasil disimpan');
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422 || response.status==500){
                    $("#alert_warning").trigger("click", response.responseJSON.message);
                    $('#pickTimeModal').modal('show');
                }
                if (response.status == 433){
                    swal({
                        title: "PERHATIAN!!!",
                        text: response.responseJSON.message,
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((wilsave) => {
                        if (wilsave) {
                            $("#cycle_time_id").val(response.responseJSON.id)
                            saveCycle();
                        }
                        resetData();
                    });
                }
            }
        });
    }
    function backComponent() {
        let workflowId = $("#workflowId").val();
        $('#processModal').modal('hide');
        $('#componentModal').modal('show');
        componentPicklist('component',workflowId,'/skill-matriks/cycle-time/get-component-process');
    }
    $("#nik_label").change(function (e) {
        var nik           = $("#nik_label").val();
        
        if(nik)
        {
            var value     = nik.split(':');
            $('#nik_label').val(value[0]);
            $('#sewer_nik').val(value[0]);
            $('#name_label').val(value[1]);
            $('#sewer_name').val(value[1]);
            
        }else
        {
            $('#nik_label').val('');
            $('#sewer_nik').val('');
            $('#name_label').val('');
            $('#sewer_name').val('');
        }
        if($("#name_label").val()!=''){
            let cycle_time_batch_id = $("#cycle_time_batch_id").val();
            let workflowId          = $("#workflowId").val();
            $.ajax({
                type: "GET",
                url: '/skill-matriks/cycle-time/show-history',
                data: {
                    cycle_time_batch_id: cycle_time_batch_id,
                    nik                : nik,
                    workflowId         : workflowId,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    // console.log(parseInt(response.list)==1);
                    if (response.list > 1) {
                        $('#processModal').modal('show');
                        processPicklist('process', response.urlprocess);
                    }else if(parseInt(response.list)==1){
                        $('#select_process_merge').val(null).trigger('change');
                        $('#select_mesin_merge').val(null).trigger('change');
                        processList = [];
                        $("#workflow_detail_id").val(response.workflow_detail_id);
                        $("#prosesName").val(response.process_name);
                        $("#machine").val(response.machine_name);
                        let workflow_detail_id = response.workflow_detail_id;
                        let proses_name        = response.process_name;
                        let machine_name       = response.machine_name;
                        let machine_id         = response.machine_id;

                        
                        var is_available = isAvailable(workflow_detail_id);

                        if(is_available > 0)
                        {
                            $("#alert_info").trigger("click", 'Proses Sudah pernah di pilih.');
                            return false;
                        }
                        var input = {
                            'id': -1,
                            'workflow_detail_id': workflow_detail_id,
                            'proses_name': proses_name,
                            'machine_name': machine_name,
                            'machine_id': machine_id,
                        };
                        $("#select_process_merge").prop("disabled", true);
                        $("#select_mesin_merge").prop("disabled", true);
                        $("#select_process_merge").select2({
                            data: input
                        })
                        $("#select_mesin_merge").select2({
                            data: input
                        })
                        var diff = checkMachine(machine_id);
                        
                        var newOption = new Option(input.proses_name, input.workflow_detail_id, true, true);
                        $("#select_process_merge").append(newOption).trigger('change');

                        if(diff==0){
                            var optionMachine = new Option(input.machine_name, input.machine_id, true, true);
                            $("#select_mesin_merge").append(optionMachine).trigger('change');
                        }
                        processList.push(input);
                        $('#'+workflow_detail_id+'_UnSelect').removeClass('hidden');
                        $('#'+workflow_detail_id+'_Select').addClass('hidden');
                        $('#process_pick_list').val(JSON.stringify(processList));
                        $('#'+workflow_detail_id).css('background-color', '#ffcc66');
                        renderProcess();
                    } else {
                        $('#componentModal').modal('show');
                        // processPicklist('process','/skill-matriks/cycle-time/get-process');
                        componentPicklist('component',workflowId,'/skill-matriks/cycle-time/get-component-process');
                        
                    }
    
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');
    
                    if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON.message);
                }
            })
            .done(function () {
                render();
            });
        }else{
            $("#alert_warning").trigger("click", 'Nik Tidak di temukan');
            $('#nik_label').val('');
            $('#sewer_nik').val('');
            $('#name_label').val('');
            $('#sewer_name').val('');
            resetData();
        }
    });
    function processPicklist(name,url,component) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
    
            var q       = $(search).val();
            var idcycle = $("#cycle_time_batch_id").val();
            var style   = $("#style").val();
            var workflowId = $("#workflowId").val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                url: url,
                data: {
                    id         : idcycle,
                    workflow_id: workflowId,
                    component  : component,
                    q          : q,
                },
            })
            .done(function (data) {
                $(table).html(data['view']);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
                $(table).find('.btn-unchoose').on('click', unchooseItem);
                draw(data['data']);

            });
        }
        
        function chooseItem() {
            var proses_name        = $(this).data('proses_name');
            var workflow_detail_id = $(this).data('workflow_detail_id');
            var machine_name       = $(this).data('machine_name');
            var machine_id       = $(this).data('machine_id');

            var is_available = isAvailable(workflow_detail_id);

            if(is_available > 0)
            {
                $("#alert_info").trigger("click", 'Proses Sudah pernah di pilih.');
                return false;
            }
            var input = {
                'id': -1,
                'workflow_detail_id': workflow_detail_id,
                'proses_name': proses_name,
                'machine_name': machine_name,
                'machine_id': machine_id,
            };
            $("#select_process_merge").prop("disabled", true);
            $("#select_mesin_merge").prop("disabled", true);
            $("#select_process_merge").select2({
                data: input
            })
            $("#select_mesin_merge").select2({
                data: input
            })
            var diff = checkMachine(machine_id);
            
            var newOption = new Option(input.proses_name, input.workflow_detail_id, true, true);
            $("#select_process_merge").append(newOption).trigger('change');

            if(diff==0){
                var optionMachine = new Option(input.machine_name, input.machine_id, true, true);
                $("#select_mesin_merge").append(optionMachine).trigger('change');
            }
            processList.push(input);
            $('#'+workflow_detail_id+'_UnSelect').removeClass('hidden');
            $('#'+workflow_detail_id+'_Select').addClass('hidden');
            $('#process_pick_list').val(JSON.stringify(processList));
            $('#'+workflow_detail_id).css('background-color', '#ffcc66');
            renderProcess();
        }
        function unchooseItem() {
            var id           = $(this).data('workflow_detail_id');
            for (var idx in processList) 
            {
                var data = processList[idx];
                if (id == data.workflow_detail_id) 
                {
                    processList.splice(idx, 1);
                    $('#'+data.workflow_detail_id).css('background-color', '#ffffff');
                    $('#select_process_merge').val(null).trigger('change');
                    $('#select_mesin_merge').val(null).trigger('change');
                    processList.forEach(element => {
                        $("#select_process_merge").select2({
                            data: processList
                        });

                        $("#select_mesin_merge").select2({
                            data: processList
                        });
                        var newOption = new Option(element.proses_name, element.workflow_detail_id, true, true);
                        var newOptionMachine = new Option(element.machine_name, element.machine_id, true, true);
                        
                        $("#select_process_merge").append(newOption).trigger('change');
                        $("#select_mesin_merge").append(newOptionMachine).trigger('change');
                    });
                    
                }
            }

            $('#'+id+'_UnSelect').addClass('hidden');
            $('#'+id+'_Select').removeClass('hidden');
            renderProcess();
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function componentPicklist(name, workflowId, url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
            var q           = $(search).val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url,
                    data:{
                        workflowId: workflowId,
                        q            : q,
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            let component     = $(this).data('name');
            
            $('#processModal').modal('show');
            processPicklist('process', '/skill-matriks/cycle-time/get-process',component);
            // processPicklist('process','/skill-matriks/cycle-time/get-process');
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function checkMachine(id){

        var machine = $("#select_mesin_merge").val();
        if(machine){
            var count =0;
            var x =0;
            machine.forEach(element => {
                if(element==id){
                    x = count+1
                    
                }
            });
            return x;
            
        }else{
            return 0;
        }
        
    }
    function draw(data)
    {
        for (var id in data.data) 
        {
            var _data = data.data[id];
            
            for (var idx in processList)
            {
                var list_data = processList[idx];
                if (_data.workflow_detail_id == list_data.workflow_detail_id)
                {
                    $('#'+_data.workflow_detail_id).css('background-color', '#ffcc66');
                    $('#'+_data.workflow_detail_id+'_UnSelect').removeClass('hidden');
                    $('#'+_data.workflow_detail_id+'_Select').addClass('hidden');
                }
            }
        }
        
    }
    function drawEachLine(workflow_detail_id) 
    {
        for (var idx in processList) {
            var data = processList[idx];
            if (workflow_detail_id == data.workflow_detail_id) {
                $('#'+data.workflow_detail_id).css('background-color', '#ffcc66');
            }
        }

    }
    function isAvailable(id)
    {
        var flag = 0;
        for (var idx in processList) 
        {
            var data = processList[idx];
            if (id == data.workflow_detail_id) 
            {
                flag++;
            }
        }
        return flag;
    }
    function renderProcess() {
        getIndexProcess();
        $('#process_pick_list').val(JSON.stringify(processList));
        var tmpl 		= $('#defect_data_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: processList };
        var html 		= Mustache.render(tmpl, data);
    }
    function getIndexProcess() {
        for (idx in processList) {
            processList[idx]['_id'] = idx;
            processList[idx]['no'] = parseInt(idx) + 1;
        }
    }
    function processClose() {
        $('#processModal').modal('hide');
        items = [];
    }
    function componentClose() {
        $('#componentModal').modal('hide');
    }
    
    $("#pick-time").click(function (e) {
        var nik = $("#nik_label").val();
        var name = $("#name_label").val();
        let is_copied = $("#is_copied").val();
        var workflow_detail_id = $("#workflow_detail_id").val();
        if (!nik) {
            $("#alert_error").trigger("click", 'NIK belum di input');
            return false
        }
    
        if (!name) {
            $("#alert_error").trigger("click", 'Nama belum di input');
            return false
        }
        if (processList.length<1) {
            $("#alert_error").trigger("click", 'Proses blm di pilih');
            return false
        }
        
        if (is_copied==1) {
            var url = $("#url_store_cycle_history").val();
        } else {
            var url = $("#url_save_detail_cycle").val();   
            
        }
        $('#form').attr('action',url);
        $('#pickTimeModal').modal('show');
    });
    $("#btn-save-pick").click(function (e) {
        $("#btn-save").trigger("click");
    });
    $("#btn-draft").click(function (e) {
        var url = $("#url_draft_detail_cycle").val();   
        $('#form').attr('action',url);
        $("#btn-save").trigger("click");
    });
    function resetData(){
        $("#nik_label").val('');
        $("#name_label").val('');
        $("#prosesName").val('');
        $("#machine").val('');
        $("#wip").val('');
        $("#is_copied").val(0);
        $("#sewer_nik").val('');
        $("#sewer_name").val('');
        $('#select_process_merge').val(null).trigger('change');
        $('#select_mesin_merge').val(null).trigger('change');
        reset();
        processList = [];
        items = [];
        render();
        renderProcess();
    }
    function stylePicklist(name) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        var line_id = $('#line_id').val();
        $(search).val('');
    
        function itemAjax() {
    
            var q = $(search).val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: '/skill-matriks/cycle-time/style-picklist'+ '?q=' + q+'&line_id='+line_id
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var id    = $(this).data('id');
            var name  = $(this).data('name');
    
            $(item_id).val(id);
            $(item_name).val(name);
            $('#line_id').val(id);
            $('#line_name').val(name);
            
            $('#styleName').trigger('click');
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
        
    function playFunc() {
        playPause = playPause +1;
        
        if (playPause===1) {
            // play();
            // playLap();
            start();
            startLaps();
            document.getElementById("play").classList.add("pause");
            document.getElementById("reset").classList.add("hidden");
            document.getElementById("lap").classList.remove("hidden");
            document.getElementById("animateCircle").classList.add("addAnimation");
            $("#animateCircle.addAnimation").css("animation-play-state","running");
            $("#flag").val(1);
        }else if(playPause===2){
            document.getElementById("play").classList.remove("pause");
            document.getElementById("lap").classList.add("hidden");
            document.getElementById("reset").classList.remove("hidden");
            $("#animateCircle").css("animation-play-state","paused");
            $("#flag").val(0);
            playPause =0;
            pause();
            // stop();
        }
    }
    function LapFunc(){
        let today    = new Date();
        let timeLaps = $("#time_lap").text();
        let time     = $("#time").text();
        var input = {
            'id': -1,
            'interval': timeLaps,
            'total': time,
            'time_recorded': today.toTimeString().split(' ')[0]
        };
        items.push(input);
        render();

        clearInterval(timerLaps);
        printLaps("00:00:00");
        elapsedTimeLaps = 0;

        startLaps();
    }
    function play() {
        base = setInterval(timer,10)
    }
    function playLap() {
        baseLap = setInterval(timerLap,10)
    }
    function stop() {
        clearInterval(base);
        clearInterval(baseLap);
    }

    function timer() { 
        milisecondVal = updateTime(milisecond);
        secondVal     = updateTime(second);
        minuteVal     = updateTime(minute);

        milisecond = ++milisecond;
        if (milisecond==100) {
            milisecond=0;
            second=++second;
        }
        if (second==60) {
            minute = ++minute;
            second = 0;
        }
        if (minute == 60) {
            minute = 0;
        }
        $("#milisecond").text(milisecondVal);
        $("#second").text(secondVal);
        $("#minute").text(minuteVal);
    }
    function timerLap() { 
        /* milisecondLap = updateTime(milisecond2);
        secondLap     = updateTime(second2);
        minuteLap     = updateTime(minute2);

        milisecond2 = ++milisecond2;
        if (milisecond2==100) {
            milisecond2=0;
            second2=++second2;
        }
        if (second2==60) {
            minute2 = ++minute2;
            second2 = 0;
        }
        if (minute2 == 60) {
            minute2 = 0;
        }
        $("#milisecondLap").text(milisecondLap);
        $("#secondLap").text(secondLap);
        $("#minuteLap").text(minuteLap); */
        
    }
    /* update time every second milisecond */
    function updateTime (i) { 
        if (i<10) {
            i="0" +i;
        }
        return i;
    }
    function resetFunc() {  
        /* milisecond = 0;
        second     = 0;
        minute     = 0;

        milisecond2 = 0;
        second2     = 0;
        minute2     = 0;
        
        $("#milisecond").text("00");
        $("#second").text("00");
        $("#minute").text("00");

        $("#milisecondLap").text("00");
        $("#secondLap").text("00");
        $("#minuteLap").text("00"); */
        reset();
        if (!$("#play").hasClass("pause")) {
            document.getElementById("animateCircle").classList.remove("addAnimation");
        } else {
            document.getElementById("animateCircle").classList.remove("addAnimation");
            setTimeout(() => {
                document.getElementById("animateCircle").classList.add("addAnimation");
            }, 10);
        }
    }
    function stopFunc() {  
        clearInterval(base);
        clearInterval(baseLap);
        resetFunc();

        if ($("#play").hasClass("pause")) {
            document.getElementById("animateCircle").classList.remove("addAnimation");
            playFunc();
        }
    }
    function render() {
        getIndex();
        $('#pick_time_data').val(JSON.stringify(items));
        var tmpl 		= $('#pick_time_table').html();
        Mustache.parse(tmpl);
        var data 		= { list: items };
        var html 		= Mustache.render(tmpl, data);
        $('#pick_time_tbody').html(html);
        bind();
    }
    function bind() {
        $('.btn-delete-defect').on('click', deleteItem);
    }
    
    function getIndex() {
        for (idx in items) {
            items[idx]['_id'] = idx;
            items[idx]['no'] = parseInt(idx) + 1;
        }
    }
    function deleteItem() {
        var i         = $(this).data('id');
        var data      = items[i];
        if (data.id!='-1') {
            $('#pickTimeModal').modal('hide');
            bootbox.confirm("Apakah anda yakin data akan di hapus ?.", function(result) {
                if (result) {
                    $.ajax({
                        type: "GET",
                        url: '/skill-matriks/cycle-time/destroy-draft-cycle-time-detail/'+data.id,
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        },
                        success: function() {
                            // document.location.href = '/master-line';
                            $('#pickTimeModal').modal('show');
                            
                        },
                        error: function(response) {
                            $.unblockUI();
                            
                            if (response.status == 422) {
                                $("#alert_warning").trigger("click", response.responseJSON.message);
                                $('#pickTimeModal').modal('show');
                            }
                        }
                    })
                    .done(function () {
                        items.splice(i, 1);
                        render();
                    });
                }else{
                    $('#pickTimeModal').modal('show');
                }
            });
        }else{
            items.splice(i, 1);
            render();
        }
        
    }
    function sortList(array, key) 
    {
        return array.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }
    function lapDataEntry() {
        $('#dataEntryModal').modal('show');
        datEntryPicklist('dataEntry','/skill-matriks/cycle-time/report-data-entry');
    }

    function datEntryPicklist(name,url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
    
            var q                   = $(search).val();
            var cycle_time_batch_id = $("#cycle_time_batch_id").val();
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                url: url,
                data: {
                    q                  : q,
                    cycle_time_batch_id: cycle_time_batch_id,
                },
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

            });
        }
        
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    $('#processButtonLookup').click(function (e) { 
        e.preventDefault();
        let nik        = $("#nik_label").val();
        let name       = $("#name_label").val();
        let workflowId = $("#workflowId").val();
        if (!nik) {
            $("#alert_error").trigger("click", 'NIK belum di input');
            return false
        }
    
        if (!name) {
            $("#alert_error").trigger("click", 'Nama belum di input');
            return false
        }
        /* $('#processModal').modal('show');
        processPicklist('process','/skill-matriks/cycle-time/get-process'); */
        $('#componentModal').modal('show');
        componentPicklist('component',workflowId,'/skill-matriks/cycle-time/get-component-process');
    });
    $("#historyButtonLookup").click(function (e) { 
        e.preventDefault();
        $('#historyProcessModal').modal('show');
        historyProcessPicklist('historyProcess','/skill-matriks/cycle-time/history-day-process');
    });
    function historyProcessClose() {
        $('#historyProcessModal').modal('hide');
    }
    function historyProcessPicklist(name, url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
            let batch_id = $("#cycle_time_batch_id").val();
            let style    = $("#style").val();
            var q        = $(search).val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url,
                    data:{
                        q       : q,
                        batch_id: batch_id,
                        style   : style,
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var sewer_name   = $(this).data('sewer_name');
            var sewer_nik    = $(this).data('sewer_nik');
            var process      = $(this).data('process');
            var workflow     = $(this).data('workflow');
            var cycle_id     = $(this).data('cylce_id');
            $("#sewer_nik").val(sewer_nik);
            $("#nik_label").val(sewer_nik);
            $("#name_label").val(sewer_name);
            $("#sewer_name").val(sewer_name);
            $("#cycle_time_id").val(cycle_id);
            $('#select_process_merge').val(null).trigger('change');
            $('#select_mesin_merge').val(null).trigger('change');
            processList = [];
            $.map(workflow, function (element, index) {
                var is_available = isAvailable(element.f1);

                if(is_available > 0)
                {
                    return false;
                }
                var input = {
                    'id': -1,
                    'workflow_detail_id': element.f1,
                    'proses_name': element.f2,
                    'machine_name': element.f3,
                    'machine_id': element.f4,
                };
                $("#select_process_merge").prop("disabled", true);
                $("#select_mesin_merge").prop("disabled", true);
                $("#select_process_merge").select2({
                    data: input
                })
                $("#select_mesin_merge").select2({
                    data: input
                })
                var diff = checkMachine(element.f4);
                var newOption = new Option(input.proses_name, input.workflow_detail_id, true, true);
                $("#select_process_merge").append(newOption).trigger('change');

                if(diff==0){
                    var optionMachine = new Option(input.machine_name, input.machine_id, true, true);
                    $("#select_mesin_merge").append(optionMachine).trigger('change');
                }
                processList.push(input);
                $('#'+element.f1+'_UnSelect').removeClass('hidden');
                $('#'+element.f1+'_Select').addClass('hidden');
                $('#process_pick_list').val(JSON.stringify(processList));
                $('#'+element.f1).css('background-color', '#ffcc66');
                $('#is_copied').val(1);
                renderProcess();
                
                
            });
            /* var is_available = isAvailable(workflow_detail_id);

            if(is_available > 0)
            {
                $("#alert_info").trigger("click", 'Proses Sudah pernah di pilih.');
                return false;
            }
            var input = {
                'id': -1,
                'workflow_detail_id': workflow_detail_id,
                'proses_name': proses_name,
                'machine_name': machine_name,
                'machine_id': machine_id,
            };
            $("#select_process_merge").prop("disabled", true);
            $("#select_mesin_merge").prop("disabled", true);
            $("#select_process_merge").select2({
                data: input
            })
            $("#select_mesin_merge").select2({
                data: input
            })
            var diff = checkMachine(machine_id);
            
            var newOption = new Option(input.proses_name, input.workflow_detail_id, true, true);
            $("#select_process_merge").append(newOption).trigger('change');

            if(diff==0){
                var optionMachine = new Option(input.machine_name, input.machine_id, true, true);
                $("#select_mesin_merge").append(optionMachine).trigger('change');
            }
            processList.push(input);
            $('#'+workflow_detail_id+'_UnSelect').removeClass('hidden');
            $('#'+workflow_detail_id+'_Select').addClass('hidden');
            $('#process_pick_list').val(JSON.stringify(processList));
            $('#'+workflow_detail_id).css('background-color', '#ffcc66');
            renderProcess(); */
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function deleteCyle(id) {  
        $('#dataEntryModal').modal('hide');
        bootbox.confirm("Apakah anda yakin data akan di hapus ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "GET",
                    url: '/skill-matriks/cycle-time/destroy-cycle-time/'+id,
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        // document.location.href = '/master-line';
                    },
                    error: function(response) {
                        $.unblockUI();

                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    }
                });
            }
        });
    }
    function selectDraft() {  }
    function sendMessage() {
        var name    = 'send_message';
        var url = '/skill-matriks/cycle-time/send-message'
        $('#send_messageModal').modal();
        sendMessagePicklist(name,url)
    }
    function sendMessagePicklist(name,url) {  
        var search = '#' + name + 'Search';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
            var q              = $(search).val();
            var workflowId     = $("#workflowId").val();
            var cycle_batch_id = $("#cycle_batch_id").val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url + '?workflowId=' + workflowId + '&cycle_batch_id=' + cycle_batch_id
                })
                .done(function (data) {
                    $(table).html(data);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    //$(table).find('.btn-choose').on('click', chooseItem);
                });
        }
        itemAjax();
    }
    function send_messageClose() {
        $('#send_messageModal').modal('hide');
    }
    
    function listDraft() {
        var name    = 'listDraft';
        var url = '/skill-matriks/cycle-time/list-draft'
        $('#listDraftModal').modal();
        listDraftPicklist(name,url)
    }
    function listDraftPicklist(name,url) {  
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        $(search).val('');
    
        function itemAjax() {
    
            var q                   = $(search).val();
            var cycle_time_batch_id = $("#cycle_time_batch_id").val();
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                url: url,
                data: {
                    q                  : q,
                    cycle_time_batch_id: cycle_time_batch_id,
                },
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
        }
        
        function chooseItem() {
            var cycle_id = $(this).data('id');
            var name = $(this).data('sewer');
            var nik = $(this).data('nik');
            var batch_id = $(this).data('batch_id');
            
            $('#cycle_time_id').val(cycle_id);
            $('#listDraftModal').modal('toggle');
            $.ajax({
                type: "GET",
                url: '/skill-matriks/cycle-time/detail-draft',
                data: {
                    cycle_id    : cycle_id,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    for (i in response) 
                    {
                        var data  = response[i]
                        var input = {
                            
                            'id'           : data.id,
                            'interval'     : data.time,
                            'total'        : data.total_time,
                            'time_recorded': data.time_recorded
                        }
                        items.push(input);
                    }
                    var urls = $("#url_store_draft_cycle").val();   
                    $('#form').attr('action',urls);
                    $("#btn-draft").addClass("hidden");
                    $('#pickTimeModal').modal('show');
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');
    
                    if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
                }
            })
            .done(function () {
                render();
                // $("#pick-time").trigger('click');
            });
        }
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function listDraftClose() {
        $('#listDraftModal').modal('hide');
    }
    function timeToString(time) {
        let diffInHrs = time / 3600000;
        let hh = Math.floor(diffInHrs);

        let diffInMin = (diffInHrs - hh) * 60;
        let mm = Math.floor(diffInMin);

        let diffInSec = (diffInMin - mm) * 60;
        let ss = Math.floor(diffInSec);

        let diffInMs = (diffInSec - ss) * 100;
        let ms = Math.floor(diffInMs);

        let formattedMM = mm.toString().padStart(2, "0");
        let formattedSS = ss.toString().padStart(2, "0");
        let formattedMS = ms.toString().padStart(2, "0");

        return `${formattedMM}:${formattedSS}:${formattedMS}`;
    }
    let startTime;
    let elapsedTime = 0;
    let timerInterval;

    let startTimeLaps;
    let elapsedTimeLaps = 0;
    let timerLaps;

    // Create function to modify innerHTML

    function print(txt) {
        document.getElementById("time").innerHTML = txt;
    }
    function printLaps(txt) {
        document.getElementById("time_lap").innerHTML = txt;
    }

    // Create "start", "pause" and "reset" functions

    function start() {
        startTime = Date.now() - elapsedTime;
        timerInterval = setInterval(function printTime() {
        elapsedTime = Date.now() - startTime;
        print(timeToString(elapsedTime));
        }, 10);
        // showButton("PAUSE");
    }
    function startLaps() {
        startTimeLaps = Date.now() - elapsedTimeLaps;
        timerLaps     = setInterval(function printTime() {
            elapsedTimeLaps = Date.now() - startTimeLaps;
            printLaps(timeToString(elapsedTimeLaps));
        }, 10);
        // showButton("PAUSE");
    }

    function pause() {
        clearInterval(timerInterval);
        clearInterval(timerLaps);
        // showButton("PLAY");
    }

    function reset() {
        clearInterval(timerInterval);
        print("00:00:00");
        elapsedTime = 0;
        
        clearInterval(timerLaps);
        printLaps("00:00:00");
        elapsedTimeLaps = 0;
        // showButton("PLAY");
    }

    // Create function to display buttons

    function showButton(buttonKey) {
        const buttonToShow = buttonKey === "PLAY" ? playButton : pauseButton;
        const buttonToHide = buttonKey === "PLAY" ? pauseButton : playButton;
        buttonToShow.style.display = "block";
        buttonToHide.style.display = "none";
    }
    // Create event listeners

    let playButton = document.getElementById("playButton");
    let pauseButton = document.getElementById("pauseButton");
    let resetButton = document.getElementById("resetButton");

    playButton.addEventListener("click", start);
    pauseButton.addEventListener("click", pause);
    resetButton.addEventListener("click", reset);