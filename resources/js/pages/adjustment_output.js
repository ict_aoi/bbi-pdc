$(document).ready(function() {
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    var listOutputQcTable = $('#listOutputQcTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/adjustment/pengurangan-output-qc/data-qc-output',
            data: function(d) {
                return $.extend({}, d, {
                    "date"   : $('#date').val(),
                    "line_id": $('#lineId').val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = listOutputQcTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'tanggal',
            name: 'tanggal',
            searchable: false,
            visible: true,
            orderable: true
        }, {
            data: 'line_name',
            name: 'line_name',
            searchable: true,
            visible: true
        }, {
            data: 'style',
            name: 'style',
            searchable: true,
            visible: true
        }, {
            data: 'poreference',
            name: 'poreference',
            searchable: true,
            visible: true
        }, {
            data: 'article',
            name: 'article',
            searchable: true,
            visible: true
        }, {
            data: 'size',
            name: 'size',
            searchable: true,
            visible: true
        }, {
            data: 'qty_order',
            name: 'qty_order',
            searchable: true,
            visible: true
        }, {
            data: 'total_counter',
            name: 'total_counter',
            searchable: true,
            visible: true
        }, {
            data: 'counter_day',
            name: 'counter_day',
            searchable: false,
            orderable: false
        }, {
            data: 'balance_per_day',
            name: 'balance_per_day',
            searchable: false,
            orderable: false
        },{data: 'action', name: 'action',searchable:false,orderable:false} ],
    
    });
    
    var dtableOutputQc = $('#listOutputQcTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtableOutputQc.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtableOutputQc.search("").draw();
            }
            return;
        });
    $("#btn_cari").click(function (e) { 
        e.preventDefault();
        dtableOutputQc.draw();
    });
    dtableOutputQc.draw();
});
    $('#lineButtonLookup').click(function () {
        linePicklist('line');
    
    });
    
    $('#lineName').click(function () {
        linePicklist('line');
    });
    function linePicklist(name) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc'; 
        var url = '/adjustment/pengurangan-output-qc/line-picklist';
        $(search).val('');
    
        function itemAjax() {
    
            var q = $(search).val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url,
                    data:{
                        q:q
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var id   = $(this).data('id');
            var name = $(this).data('name');
            $('#lineId').val(id);
            $('#lineName').val(name);
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function edit_proses(url) {
        $('#edit_prosesModal').modal();
        lov('edit_proses', url);
    }
    function lov(name, url) {
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        function itemAjax() {
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $.ajax({
                url: url
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
        }
        itemAjax();
    
    }
    function hapus(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $.ajax({
            type: "put",
            url: url,
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
            }
        }).done(function($result) {
            $('#RevisiTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil hapus');
        });
    }
    function edit_output(url) {
        $('#editQtyModal').modal();
        lov('editQty', url);
    }
    function editQtyClose() {
        $('#editQtyModal').modal('toggle');
    }