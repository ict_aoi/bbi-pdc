
$(document).ready(function() {
    items = JSON.parse($('#items').val());

    $('#formtambahprocess').submit(function(event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#formtambahprocess').attr('action'),
                    data: $('#formtambahprocess').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        document.location.href = '/process-sewing';
                    },
                    error: function(response) {
                        $.unblockUI();

                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    }
                });
            }
        });
    });
    
    render();
});
function render() {
    getIndex();
    $('#items').val(JSON.stringify(items));
    var tmpl = $('#items-table').html();
    Mustache.parse(tmpl);
    var data = {
        item: items
    };
    var html = Mustache.render(tmpl, data);
    $('#tbody-items').html(html);
    bind();
}
function bind() {
    $('.btn-edit-item').on('click', editItem);
    $('.btn-save-item').on('click', saveItem);
    $('.btn-cancel-item').on('click', cancelEdit);
    $('.btn-delete-item').on('click', deleteItem);
    $("#txtNama").keypress(function (e) { 
        if (e.keyCode==13) {
            e.preventDefault();
            document.getElementById("txtComponent").focus();
        }
    });
    $("#txtComponent").keypress(function (e) { 
        if (e.keyCode==13) {
            e.preventDefault();
            document.getElementById("txtSmv").focus();
        }
    });
    $('#txtSmv').keypress(function(e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        if (e.keyCode==13) {
            e.preventDefault();
            addItem();
        }
        return true;
    });
    document.getElementById("txtNama").focus();
}
function getIndex() {
    for (idx in items) {
        items[idx]['_id'] = idx;
        items[idx]['no'] = parseInt(idx) + 1;
    }
}
function checkItem(nama,component,smv) {
    for (var i in items) {
        var data = items[i];

        if (data.nama == nama&&data.component==component&&data.smv==smv)
            return false;
    }
    return true;
}
function addItem() {
    var nama  = $("#txtNama").val();
    var component  = $("#txtComponent").val();
    var smv  = $("#txtSmv").val();
    var diff = checkItem(nama,component,smv);

    if (nama == '') {
        $("#alert_error").trigger("click", 'Nama Proses tidak boleh kosong');
        return false;
    }
    if (component == '') {
        $("#alert_error").trigger("click", 'Komponen tidak boleh kosong');
        return false;
    }
    if (smv == '') {
        $("#alert_error").trigger("click", 'SMV tidak boleh kosong');
        return false;
    }
    
    if (!diff) {
        $("#alert_error").trigger("click", 'Nama Proses & Komponen Sudah Ada');
        return false;
        $("#txtNama").focus();
    }
    var input = {
        'id'       : -1,
        'nama'     : nama,
        'component': component,
        'smv'      : smv
    };
    items.push(input);
    render();
}
function editItem() {
    var i = $(this).data('id');

    //add hidden div
    $('#nama_' + i).addClass('hidden');
    $('#component_' + i).addClass('hidden');

    $('#editItem_' + i).addClass('hidden');
    $('#deleteItem_' + i).addClass('hidden');

    //remove hidden textbox
    $('#namaInput_' + i).removeClass('hidden');
    $('#componentInput_' + i).removeClass('hidden');

    $('#simpanItem_' + i).removeClass('hidden');
    $('#cancelItem_' + i).removeClass('hidden');
}
function cancelEdit() {
    var i = $(this).data('id');

    //remove hidden div
    $('#nama_' + i).removeClass('hidden');
    $('#component_' + i).removeClass('hidden');

    $('#editItem_' + i).removeClass('hidden');
    $('#deleteItem_' + i).removeClass('hidden');

    //add hidden textbox
    $('#namaInput_' + i).addClass('hidden');
    $('#componentInput_' + i).addClass('hidden');

    $('#simpanItem_' + i).addClass('hidden');
    $('#cancelItem_' + i).addClass('hidden');
}
function saveItem() {
    var i = $(this).data('id');

    var nama = $('#namaInput_' + i).val();
    var component = $('#componentInput_' + i).val();
    
    if (nama == '') {
        $("#alert_error").trigger("click", 'Nama Proses tidak boleh kosong');
        return false;
    }
    if (component == '') {
        $("#alert_error").trigger("click", 'Komponen tidak boleh kosong');
        return false;
    }

    items[i].nama = nama;
    items[i].component = component;
    render();
}
function deleteItem() {
    var i = $(this).data('id');
    var getItems = items[i];
    bootbox.confirm("Apakah Item Detail ini akan di hapus ?.", function(result) {

        if (result) {
            if (getItems["id"] != -1) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                        type: "put",
                        url: '/process-sewing/delete/'+getItems['id'],
                        beforeSend: function() {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        success: function() {
                            $.unblockUI();
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                        }
                    })
                    .done(function() {
                        $("#alert_success").trigger("click", 'Data successfully deleted');
                    });
            }
            
            items.splice(i, 1);
            render();
        }

    });

}