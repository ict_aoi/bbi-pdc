$(document).ready(function () {
    
    $('#gsd').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    var detailWorkflowTable = $('#detailWorkflowTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/alur-kerja/data-detail-workflow',
            data: function(d) {
                return $.extend({}, d, {
                    "id" : $('#id').val(),
                });
        }
        },
        fnCreatedRow: function (row, data, index) {
            var info = detailWorkflowTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'proses_name',
            name: 'proses_name',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'component',
            name: 'component',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'machine_name',
            name: 'machine_name',
            searchable: true,
            visible: true,
            orderable: true
        },{
            data: 'smv',
            name: 'smv',
            searchable: true,
            visible: true,
            orderable: true
        },{
            data: 'lastproses',
            name: 'lastproses',
            searchable: true,
            visible: true,
            orderable: true
        },{
            data: 'critical_process',
            name: 'critical_process',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'action',
            name: 'action',
            searchable: false,
            orderable: false
        }, ],

    });

    var dtable = $('#detailWorkflowTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;                                           
        });
    dtable.draw();
});
$('#form').submit(function (event){
    event.preventDefault();
    
    var smv = $('#gsd').val();
    
    
    if(!smv)
    {
        $("#alert_warning").trigger("click", 'SMV Wajib diisi');
        return false
    }
    bootbox.confirm("Apakah anda yakin SMV akan update ?.", function (result) {
        if(result){
            $.ajax({
                type: "post",
                url: $('#form').attr('action'),
                data:new FormData($("#form")[0]),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function () {
                    $("#alert_success").trigger("click", 'Data berhasil disimpan.')
                },
                error: function (response) {
                    $.unblockUI();
                    
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                }
            });
        }
    });
});
function edit(url) {  
    $("#editModal").modal('show');
    lov('edit',url)
}
function hapus(url) {  
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
        }
    }).done(function ($result) {
        $('#detailWorkflowTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data Berhasil hapus');
    });
}

$("#btn-tambah").click(function (e) { 
    e.preventDefault();
    var url = $("#create_process").val();
    $("#editModal").modal('show');
    lov('edit',url)
});
$("#btn-gabung").click(function (e) { 
    e.preventDefault();
    var url = $("#merge_process").val();
    $("#mergeProcessModal").modal('show');
    lov('mergeProcess',url)
});

function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
    }
    
    itemAjax();

}
function editClose() {
    $('#editModal').modal('toggle');
}
function mergeProcessClose() {
    $('#mergeProcessModal').modal('toggle');
}