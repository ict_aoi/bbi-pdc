$(document).ready(function () {
    $('#gsd').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });
    var dataWorkflowTable = $('#dataWorkflowTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/alur-kerja/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = dataWorkflowTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'style',
            name: 'style',
            searchable: true,
            visible: true,
            orderable: true
        },{
            data: 'smv',
            name: 'smv',
            searchable: true,
            visible: true,
            orderable: true
        }, {
            data: 'action',
            name: 'action',
            searchable: false,
            orderable: false
        }, ],

    });

    var dtable = $('#dataWorkflowTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;                                           
        });
    dtable.draw();
    getWorkflow();
    $("#txtStyleMerge").change(function (e) { 
        e.preventDefault();
        var value = $(this).val();  
        if (value) {
            $.ajax({
                    type: 'get',
                    url: '/alur-kerja/get-style',
                    data:{
                        style:value,
                    }
                })
                .done(function(response) {
                    var style = response.style;

                    $("#select_style_merge").empty();
                    $("#select_style_merge").append('<option value="">-- Pilih Style --</option>');

                    $.each(style, function(erp, def) {

                        $("#select_style_merge").append('<option value="' + erp + '">' + def + '</option>');
                    });
                })
        } else {

            $("#select_style_merge").empty();
            $("#select_style_merge").append('<option value="">-- Pilih Style --</option>');
        }
    });
});

function edit_proses(url) {
    $('#edit_prosesModal').modal();
    lov('edit_proses', url);
}

function lov(name, url) {
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $.ajax({
                url: url
            })
            .done(function (data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
    }
    itemAjax();

}
$('#form').submit(function(event) {
    event.preventDefault();
    var styleName = $('#styleName').val();
    var gsd = $('#gsd').val();

    if (!styleName) {
        $("#alert_warning").trigger("click", 'Style Silahkan di isi');
        return false
    }

    if (!gsd) {
        $("#alert_warning").trigger("click", 'SMV Silahkan di isi');
        return false
    }

    $('#createWorkflowModal').modal('hide');
    bootbox.confirm("Are you sure want to save this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form').attr('action'),
                data: $('#form').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {

                    window.open(response, '_self');

                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    $('#confirmationModal').modal();
                }
            });
        }
    });
});
$('#form-style').submit(function(event) {
    event.preventDefault();
    var style              = $('#txtStyleMerge').val();
    var select_style_merge = $('#select_style_merge').val();

    if (!style) {
        $("#alert_warning").trigger("click", 'Style Silahkan di isi');
        return false
    }

    if (!select_style_merge) {
        $("#alert_warning").trigger("click", 'Pilih Style Yang akan di Gabung');
        return false
    }

    $('#mergeStyleModal').modal('hide');
    bootbox.confirm("Are you sure want to save this data ?.", function(result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#form-style').attr('action'),
                data: $('#form-style').serialize(),
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
                    $("#alert_success").trigger("click", 'Style Berhasil di Update');
                    $('#txtStyleMerge').val("");
                    $("#select_style_merge").empty();
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    $('#confirmationModal').modal();
                }
            });
        }
    });
});
function hapus(url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
        }
    }).done(function ($result) {
        $('#dataWorkflowTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data Berhasil hapus');
    });
}

$('#styleButtonLookup ').on('click', function () {
    // $('#styleModal').modal('show');
    $('#createWorkflowModal').modal('hide');
    stylePicklist('style', '/alur-kerja/list-of-style?')
});
$('#styleName').click(function () {
    // $('#styleModal').modal('show');
    $('#createWorkflowModal').modal('hide');
    stylePicklist('style', '/alur-kerja/list-of-style?')
});
function stylePicklist(name, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax() {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(search).val('');
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var style = $(this).data('style');
        var obj = $(this).data('obj');
        /*var e = jQuery.Event("keypress");
        e.keyCode = $.ui.keyCode.ENTER*/
        $('#styleName').val(style);
        $('#createWorkflowModal').modal();
        document.getElementById("gsd").focus();
        if (!obj || obj.length == 0)
            obj = [{}];
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            //console.log(params);
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}
function styleClose(params) {
    $('#styleModal').modal('toggle');
}
function styleOutstandingClose(params) {
    $('#styleOutstandingModal').modal('toggle');
}
function getWorkflow() {
    $.ajax({
        url: '/alur-kerja/get-workflow',
        
        complete: function() {
        },
        success: function (response) {
            $("#active").text(response.active);
            $("#outstanding").text(response.outstanding);
            $("#complainWorkflow").text(response.dataComplainWorkflow);
        }
    });
}
function showStyleOutstanding() {
    var url = $("#url_style_outstanding").val();
    
    $("#styleOutstandingModal").modal();
    lov('styleOutstanding', url);
}
function lov(name, url) {
    var modal         = '#' + name + 'Modal';
    var table         = '#' + name + 'Table';

    function itemAjax() {
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');

        $.ajax({
                url: url,
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }
    function chooseItem() {
        var style = $(this).data('style');
        $("#styleOutstandingModal").modal();
        $('#styleName').val(style);
        $('#createWorkflowModal').modal();
        document.getElementById("gsd").focus();
    }
    itemAjax();

}
function showComplainWorkflow() {  
    var url  = $("#url_complain_workflow").val();
    var name = 'complainWorkflow';
    
    $("#complainWorkflowModal").modal();

    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q           = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url,
                data:{
                    q            : q,
                }
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);

            });
    }

    function chooseItem() {
        var component     = $(this).data('name');
        var production_id = $("#production_id").val();
        var round         = $("#round").val();
        var get_proses    = $('#url_get_process').val();
        $('#processModal').modal('show');
        // processPicklist('process', production_id,round,get_proses,component);
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}
function send_message(id) {
    console.log(id);
    $("#complainWorkflowModal").modal('hide');
    $("#send_messageModal").modal();
    var url       = $("#url_send_message").val();
    var name      = 'send_message';
    var search    = '#' + name + 'Search';
    var modal     = '#' + name + 'Modal';
    var table     = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    $(search).val('');

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '?id=' + id
            })
            .done(function (data) {
                $(table).html(data);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                //$(table).find('.btn-choose').on('click', chooseItem);
            });
    }
    itemAjax();
}
function send_messageClose() {  
    $('#send_messageModal').modal('toggle');
    $("#complainWorkflowModal").modal();
}
function editWorkflow(style) {
    $("#complainWorkflowModal").modal('hide');
    $('#styleName').val(style);
    $('#createWorkflowModal').modal();
    document.getElementById("gsd").focus();
}