$(document).ready(function() {
    $('.pickadate-limits').pickadate({
        max: new Date()
    });
    var listOutputQcTable = $('#listOutputQcTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 20,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/adjustment/penambahan-output-qc/data-qc-output',
            data: function(d) {
                return $.extend({}, d, {
                    "production_id": $('#pobuyerId').val(),
                    "line_id"      : $('#lineId').val(),
                });
            }
        },
        fnCreatedRow: function(row, data, index) {
            var info = listOutputQcTable.page.info();
            var value = index + 1 + info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [{
            data: null,
            sortable: false,
            orderable: false,
            searchable: false
        }, {
            data: 'line_name',
            name: 'line_name',
            searchable: true,
            visible: true
        }, {
            data: 'style',
            name: 'style',
            searchable: true,
            visible: true
        }, {
            data: 'poreference',
            name: 'poreference',
            searchable: true,
            visible: true
        }, {
            data: 'article',
            name: 'article',
            searchable: true,
            visible: true
        }, {
            data: 'size',
            name: 'size',
            searchable: true,
            visible: true
        }, {
            data: 'total_output',
            name: 'total_output',
            searchable: true,
            visible: true
        }, {
            data: 'balance',
            name: 'balance',
            searchable: false,
            orderable: false
        },{data: 'action', name: 'action',searchable:false,orderable:false} ],
    
    });
    
    var dtableOutputQc = $('#listOutputQcTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtableOutputQc.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtableOutputQc.search("").draw();
            }
            return;
        });
    $("#btn_cari").click(function (e) { 
        e.preventDefault();
        dtableOutputQc.draw();
    });
    dtableOutputQc.draw();
});
    $('#revisi_output').keypress(function(e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
    $('#lineButtonLookup').click(function () {
        linePicklist('line');
    });
    
    $('#lineName').click(function () {
        linePicklist('line');
    });
    $('#pobuyerButtonLookup').click(function () {
        poreferencePicklist('pobuyer');
    });
    
    $('#pobuyerName').click(function () {
        poreferencePicklist('pobuyer');
    });
    $("#revisi_output").change(function (e) { 
        e.preventDefault();
        var balance = parseInt($("#balance").val());
        var revisi_output = parseInt($("#revisi_output").val());
        var new_qty = revisi_output+balance;
        if(new_qty>0){
            $("#alert_error").trigger("click", 'Qty Tidak boleh lebih dari Balance');
            return false;
        }

    });
    $("#nik_label").change(function (e) { 
        e.preventDefault();
        var value = $(this).val();
        $("#name_label").val(" ");
        $.ajax({
            type: "GET",
            url: '/adjustment/penambahan-output-qc/get-sewer',
            data: {
                nik: value,
            },
            success: function (response) {
                $("#name_label").val(response.name);
                $("#nik_label").val(response.nik);
            },
            error: function (response) {
                $.unblockUI();
                if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');

                if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
            }
        })
        .done(function () {
            
        });
    });
    $('#form_update').submit(function(event) {
        event.preventDefault();
        var revisi_output = $("#revisi_output").val();
        var nik_label     = $("#nik_label").val();
        var balance       = parseInt($("#balance").val());
        var revisi_output = parseInt($("#revisi_output").val());
        var new_qty       = revisi_output+balance;
        
        if (!revisi_output) {
            $("#alert_error").trigger("click", 'Qty Revisi Silahkan di Isi');
            return false;
        }
        if(new_qty>0){
            $("#alert_error").trigger("click", 'Qty Tidak boleh lebih dari Balance');
            return false;
        }
        if (!nik_label) {
            $("#alert_error").trigger("click", 'Nik Silahkan di Isi');
            return false;
        }
        bootbox.confirm("Apakah Anda Yakin Data Akan di update ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form_update').attr('action'),
                    data: $('#form_update').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function(response) {
                        $("#alert_success").trigger("click", response.responseJSON);
                        $('#listOutputQcTable').DataTable().ajax.reload();
                        document.location.href = '/adjustment/penambahan-output-qc/baru';
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON);
                        if (response.status == 500) $("#alert_warning").trigger("click", "simpan data gagal info ict");
                    }
                });
            }
        });
    });
    function linePicklist(name) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc'; 
        var url = '/adjustment/pengurangan-output-qc/line-picklist';
        $(search).val('');
    
        function itemAjax() {
    
            var q = $(search).val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                    url: url,
                    data:{
                        q:q
                    }
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');
                    $(table).find('.btn-choose').on('click', chooseItem);
    
                });
        }
    
        function chooseItem() {
            var id   = $(this).data('id');
            var name = $(this).data('name');
            $('#lineId').val(id);
            $('#lineName').val(name);
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function poreferencePicklist(name, url) {
        var search    = '#' + name + 'Search';
        var modal     = '#' + name + 'Modal';
        var table     = '#' + name + 'Table';
        var buttonSrc = '#' + name + 'ButtonSrc';
        var url       = '/adjustment/penambahan-output-qc/poreference-picklist';
        $(search).val('');
    
        function itemAjax() {
            var q     = $(search).val();
            var lineId = $("#lineId").val();
    
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');
    
            $.ajax({
                url: url + '?q=' + q+"&line_id="+lineId
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');
                $(table).find('.btn-choose').on('click', chooseItem);
            });
        }
    
        function chooseItem() {
            var id   = $(this).data('id');
            var buyer = $(this).data('buyer');
            $('#pobuyerId').val(id);
            $('#pobuyerName').val(buyer);
            $("#btn_cari").trigger("click");
        }
    
        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');
    
                e.preventDefault();
                itemAjax();
            });
        }
    
        $(buttonSrc).unbind();
        $(search).unbind();
    
        $(buttonSrc).on('click', itemAjax);
    
        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });
    
        itemAjax();
    }
    function good(url) {
        $('#goodModal').modal();
        lov('good', url);
    }
    
    function lov(name, url) {
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        function itemAjax() {
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $.ajax({
                url: url
            })
            .done(function(data) {
                $(table).html(data);
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
            });
        }
        itemAjax();
    
    }
    function goodClose() {
        $('#goodModal').modal('toggle');
    }