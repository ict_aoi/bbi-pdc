<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can regist==er web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

Route::prefix('sewing')->group(function () 
{

});

Route::prefix('qc')->group(function () 
{
    Route::prefix('in-line')->group(function () 
    {
        Route::get('/{factory}/{line}', 'QcInlineController@index')->name('qc_inline.index');
        Route::post('login', 'QcInlineController@login')->name('qc_inline.login');
        Route::post('logout', 'QcInlineController@logout')->name('qc_inline.logout');
        Route::get('line-picklist', 'QcInlineController@linePicklist')->name('qc_inline.linePicklist');
        Route::get('poreference-picklist', 'QcInlineController@poreferencePickList')->name('qc_inline.poreferencePickList');
        Route::get('get-status', 'QcInlineController@getStatus')->name('qc_inline.get_status');
        Route::get('get-process', 'QcInlineController@getProcess')->name('qc_inline.get_process');
        Route::get('get-machine', 'QcInlineController@getMachine')->name('qc_inline.get_machine');
        Route::get('get-sewer', 'QcInlineController@getSewer')->name('qc_inline.get_sewer');
        Route::get('get-process-all', 'QcInlineController@getProcessAll')->name('qc_inline.get_process_all');
        Route::get('get-defect', 'QcInlineController@getDefect')->name('qc_inline.get_defect');
        Route::get('get-history-grade', 'QcInlineController@getHistoryGrade')->name('qc_inline.get_history_grade');
        Route::get('get-grade-before', 'QcInlineController@getGradeBefore')->name('qc_inline.get_grade_before');
        Route::get('show-history', 'QcInlineController@showHistory')->name('qc_inline.show_history');
        Route::get('history-process/{factory}/{line}/{workflow_id}/{nik}/{style}', 'QcInlineController@HistoryProcess')->name('qc_inline.history_process');
        Route::get('history-machine/{factory}/{line}/{nik}/{style}', 'QcInlineController@HistoryMachine')->name('qc_inline.history_machine');
        Route::get('history-sewer/{factory}/{line}/{process}/{style}', 'QcInlineController@HistorySewer')->name('qc_inline.history_sewer');
        Route::post('store', 'QcInlineController@store')->name('qc_inline.store');
        Route::get('report-sample-round/', 'QcInlineController@ReportSampleRound')->name('qc_inline.report_sample_round');
        Route::get('report-data-sample-round', 'QcInlineController@dataReportSampleRound')->name('qc_inline.report_data_sample_round');
        Route::get('insert-history', 'QcInlineController@insertHistorySewing')->name('qc_inline.insert_history');
        Route::get('get-history-process', 'QcInlineController@getHistoryProcess')->name('qc_inline.get_history_process');
        Route::post('destroy-process-sewing', 'QcInlineController@destroyProcessSewing')->name('qc_inline.destroy_process_sewing');
        Route::post('store-history-procesess', 'QcInlineController@storeHistoryProcess')->name('qc_inline.store_history_process');
        Route::get('get-component-process', 'QcInlineController@getComponentProcess')->name('qc_inline.get_component_process');
        Route::post('store-switch-round', 'QcInlineController@storeSwitchRound')->name('qc_inline.store_switch_round');
        Route::get('show-history-operator-sewer', 'QcInlineController@showHistoryOperatorSewer')->name('qc_inline.show_history_operator_sewer');
        Route::get('get-dashboard-report', 'QcInlineController@getDashboard')->name('qc_inline.get_dashboard_report');
        Route::get('send-message', 'QcInlineController@sendMessage')->name('qc_inline.send_message');
        Route::post('store-message', 'QcInlineController@storeMessage')->name('qc_inline.store_message');
        Route::get('get-notification', 'QcInlineController@getNotification')->name('qc_inline.get_notification');
        Route::get('complain-workflow-data', 'QcInlineController@getComplainWorkflowData')->name('qc_inline.complain_workflow_data');
    });

    Route::prefix('end-line')->group(function () 
    {
        Route::get('/{factory}/{line}', 'QcEndlineController@index')->name('endLine.index');
        Route::post('login', 'QcEndlineController@login')->name('endLine.login');
        Route::post('logout', 'QcEndlineController@logout')->name('endLine.logout');
        Route::get('poreference-picklist', 'QcEndlineController@poreferencePickList')->name('endLine.poreferencePickList');
        Route::get('size-picklist', 'QcEndlineController@sizePickList')->name('endLine.sizePickList');
        Route::get('defect-picklist', 'QcEndlineController@defectPickList')->name('endLine.defectPickList');
        Route::get('get-data', 'QcEndlineController@getDataQc')->name('endLine.get_data');
        Route::get('get-repairing', 'QcEndlineController@getRepairing')->name('endLine.getRepairing');
        Route::get('get-data-repairing', 'QcEndlineController@getDataRepairing')->name('endLine.getDataRepairing');
        Route::get('get-repairing-all', 'QcEndlineController@getRepairingAll')->name('endLine.getRepairingAll');
        Route::get('get-data-repairing-all', 'QcEndlineController@getDataRepairingAll')->name('endLine.getDataRepairingAll');
        Route::post('store', 'QcEndlineController@store')->name('endLine.store');
        Route::get('store-defect', 'QcEndlineController@storeDefect')->name('endLine.storeDefect');
        Route::post('store-repair', 'QcEndlineController@storeRepair')->name('endLine.storeRepair');
        Route::get('report-qc-check', 'QcEndlineController@reportQcCheck')->name('endLine.reportQcCheck');
        Route::get('get-data-output-qc', 'QcEndlineController@getDataOutputQc')->name('endLine.getDataOutputQc');
        Route::get('get-data-sum-qc', 'QcEndlineController@getDataSumQc')->name('endLine.getDataSumQc');
        Route::get('get-detail-defect', 'QcEndlineController@getDetailDefect')->name('endLine.getDetailDefect');
        Route::get('get-detail-output', 'QcEndlineController@getDetailOutput')->name('endLine.getDetailOutput');
        Route::get('get-status-po', 'QcEndlineController@getStatusPo')->name('endLine.getStatusPo');
        Route::get('get-status-data-po', 'QcEndlineController@getDataStatusPo')->name('endLine.getDataStatusPo');
        Route::get('get-history-data-po', 'QcEndlineController@getHistoryDataPo')->name('endLine.getHistoryDataPo');
        Route::post('ssh-reboot', 'QcEndlineController@sshReboot')->name('endLine.ssh_reboot');
        Route::post('ssh-shutdown', 'QcEndlineController@sshShutdown')->name('endLine.ssh_shutdown');
        Route::get('get-defect', 'QcEndlineController@getDefect')->name('endLine.get_defect');
        Route::get('get_commitment_line', 'QcEndlineController@getCommitmentLine')->name('endLine.get_commitment_line');
        Route::get('get-alert-folding', 'QcEndlineController@getAlertFolding')->name('endLine.get_alert_folding');
        Route::post('store-collection-defect', 'QcEndlineController@storeColectionDefect')->name('endLine.store_collection_defect');
        Route::get('check-andon', 'QcEndlineController@checkAndon')->name('endLine.check_andon');
        Route::get('get_output_style', 'QcEndlineController@getOutputStyle')->name('endLine.get_output_style');
    });
});

Route::prefix('folding')->group(function () 
{
    Route::get('/{factory}/{line}', 'FoldingController@index')->name('folding.index');
    Route::post('login', 'FoldingController@login')->name('folding.login');
    Route::post('logout', 'FoldingController@logout')->name('folding.logout');
    Route::post('change_switch', 'FoldingController@changeSwitch')->name('folding.change_switch');
    Route::get('poreference-picklist', 'FoldingController@poreferencePickList')->name('folding.poreference_picklist');
    Route::post('/{factory}/{line}/set-type-folding/{type_folding}', 'FoldingController@setTypeFolding')->name('folding.set_type_folding');
    Route::get('breakdown-size-data', 'FoldingController@breakDownSizeData')->name('folding.breakdown_size_data');
    Route::get('breakdown-size-scan', 'FoldingController@breakDownSizeScan')->name('folding.breakdown_size_scan');
    Route::post('/{factory}/{line}/store-hangtag', 'FoldingController@storeHangtag')->name('folding.store_hangtag');
    Route::post('/{factory}/{line}/store-packing', 'FoldingController@storePacking')->name('folding.store_packing');
    Route::get('report-folding-scan', 'FoldingController@reportFoldingScan')->name('folding.report_folding_scan');
    Route::get('/{factory}/{line}/send-alert-qc', 'FoldingController@sendAlertQc')->name('folding.send_alert_qc');
    Route::get('/get-size', 'FoldingController@getSize')->name('folding.get_size');
    Route::post('/{factory}/{line}/store-idle-scan-packing', 'FoldingController@storeIdleScanPacking')->name('folding.store_idle_scan_packing');
    Route::post('/{factory}/{line}/store-alert-folding', 'FoldingController@storeAlertFolding')->name('folding.store_alert_folding');
    Route::get('detail_size', 'FoldingController@detailSize')->name('folding.detail_size');
});
Route::prefix('/scan-packing/karton')->middleware(['permission:menu-scan-packing'])->group(function(){
    Route::get('', 'ScanPackingController@index')->name('scan_packing.index');
    Route::get('scan-barcode', 'ScanPackingController@scanBarcode')->name('scan_packing.scan_barcode');
});
Route::prefix('display')->group(function () 
{
    Route::get('/{factory}/{line}', 'DisplayController@index')->name('display.index');
    Route::get('/{factory}/{line}/line-performance', 'DisplayController@linePerformance')->name('display.line_performance');
    Route::get('/{factory}/{line}/graph-performance', 'DisplayController@graphPerformance')->name('display.graph_performance');
    Route::get('/get-data-performanace', 'DisplayController@getDataPerformance')->name('display.get_data_performance');
});
Route::prefix('sewing')->group(function () 
{
    Route::get('/{factory}/{line}', 'SewingController@index')->name('sewing.index');
    Route::get('get-style', 'SewingController@getStyle')->name('sewing.get_style');
    Route::get('get-counter', 'SewingController@getCounter')->name('sewing.get_counter');
    Route::get('select-style', 'SewingController@SelectStyle')->name('sewing.select_style');
    Route::post('store', 'SewingController@store')->name('sewing.store');   
});
Route::prefix('washing')->group(function () 
{
    Route::get('/{factory}/{line}', 'WashingController@index')->name('washing.index');

});
Route::prefix('admin-loading')->group(function () 
{
    Route::get('/{factory}/{line}', 'AdminLoadingController@index')->name('adminLoading.index');
    Route::get('poreference-picklist', 'AdminLoadingController@poreferencePickList')->name('adminLoading.poreferencePickList');
    Route::get('/{factory}/{line}/poreference-search', 'AdminLoadingController@SearchPoreference')->name('adminLoading.poreferenceSearch');
    Route::get('poreference-picklist-per-line', 'AdminLoadingController@poreferencePickListPerLine')->name('adminLoading.poreferencePickListPerLine');
    Route::get('line-picklist', 'AdminLoadingController@linePicklist')->name('adminLoading.linePicklist');
    Route::get('/{factory}/{line}/laporan', 'AdminLoadingController@report')->name('adminLoading.report');
    Route::get('/{factory}/{line}/print-barcode', 'AdminLoadingController@printBarcode')->name('adminLoading.printBarcode');
    Route::get('/{factory}/{line}/printout', 'AdminLoadingController@printout')->name('adminLoading.printout');
    Route::get('laporan-data', 'AdminLoadingController@reportData')->name('adminLoading.reportData');
    Route::get('laporan-data-wip', 'AdminLoadingController@reportDataWip')->name('adminLoading.reportDataWip');
    Route::get('breakdown-size-data', 'AdminLoadingController@breakDownSizeData')->name('adminLoading.breakDownSizeData');
    Route::get('get-output-qc', 'AdminLoadingController@getOutputQc')->name('adminLoading.get_output_qc');
    Route::put('delete-size-loading', 'AdminLoadingController@destroySize')->name('adminLoading.delete_size_loading');
    Route::post('login', 'AdminLoadingController@login')->name('adminLoading.login');
    Route::post('logout', 'AdminLoadingController@logout')->name('adminLoading.logout');
    Route::post('/{factory}/{line}/store', 'AdminLoadingController@store')->name('adminLoading.store');
    Route::post('/{factory}/{line}/store-scan', 'AdminLoadingController@storeScan')->name('adminLoading.store_scan');
    Route::post('/{factory}/{line}/store-pindah', 'AdminLoadingController@storeScanMovement')->name('adminLoading.store_pindah');
    Route::get('/{factory}/{line}/laporan-get-size', 'AdminLoadingController@getSize')->name('adminLoading.get_size');
    Route::get('/{factory}/{line}/distribution-get-size', 'AdminLoadingController@getSizeDistribution')->name('adminLoading.get_size_distribution');
    Route::get('/{factory}/{line}/laporan-data-bundle', 'AdminLoadingController@reportDataBundle')->name('adminLoading.report_data_bundle');
    Route::get('send-alert-andon', 'AdminLoadingController@SendAlertAndon')->name('adminLoading.send_alert_andon');
    Route::get('scan-distribusi-receive', 'AdminLoadingController@ScanDistribusiReceive')->name('adminLoading.scan_distribusi_receive');
    Route::get('/{factory}/{line}/scan-distribusi-movement', 'AdminLoadingController@scanDistributionMove')->name('adminLoading.scan_distribusi_movement');
    Route::get('/{factory}/{line}/laporan-detail-bundle/{id}', 'AdminLoadingController@getDetailBundle')->name('adminLoading.get_detail_bundle');
    Route::get('/{factory}/{line}/data-temp-distribution-movement', 'AdminLoadingController@getTempDistributionMovement')->name('adminLoading.data_distribution_movement');
    Route::put('/{factory}/{line}/delete-temp-distribution', 'AdminLoadingController@destroyTempDistributionMovement')->name('adminLoading.delete_temp_distribution');
    Route::get('/{factory}/{line}/coba', 'AdminLoadingController@coba')->name('adminLoading.coba');
});

Route::prefix('account-management')->middleware(['permission:menu-permission'])->group(function () 
{
    Route::get('', 'PermissionController@index')->name('permission.index');
    Route::get('baru', 'PermissionController@create')->name('permission.create');
    Route::get('data', 'PermissionController@data')->name('permission.data');
    Route::post('store', 'PermissionController@store')->name('permission.store');
    Route::get('ubah/{id}', 'PermissionController@edit')->name('permission.edit');
    Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
    Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
});

Route::prefix('/pengelolaan-pengguna/pengelompokan-izin')->middleware(['permission:menu-role'])->group(function()
{
    Route::get('', 'RoleController@index')->name('role.index');
    Route::get('baru', 'RoleController@create')->name('role.create');
    Route::get('data', 'RoleController@data')->name('role.data');
    Route::get('ubah/{id}', 'RoleController@edit')->name('role.edit');
    Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
    Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
    Route::post('update/{id}', 'RoleController@update')->name('role.update');
    Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
});

Route::prefix('/pengelolaan-pengguna/pengguna')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'UserController@index')->name('user.index');
    Route::get('baru', 'UserController@create')->name('user.create');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::get('get-absence', 'UserController@getAbsence')->name('user.getAbsence');
    Route::get('get-information-user', 'UserController@getInformationUser')->name('user.getInformationUser');
    Route::get('ubah/{id}', 'UserController@edit')->name('user.edit');
    Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
    Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
    Route::put('delete/{id}', 'UserController@destroy')->name('user.destroy');
});
Route::prefix('/master-line')->middleware(['permission:menu-line'])->group(function(){
    Route::get('', 'LineController@index')->name('master_line.index');
    Route::get('baru', 'LineController@create')->name('master_line.create');
    Route::get('ubah/{id}', 'LineController@edit')->name('master_line.edit');
    Route::get('data', 'LineController@data')->name('master_line.data');
    Route::post('store', 'LineController@store')->name('master_line.store');
    Route::post('update/{id}', 'LineController@update')->name('master_line.update');
    Route::put('delete/{id}', 'LineController@destroy')->name('master_line.destroy');
});
Route::prefix('/daily-line')->middleware(['permission:menu-daily-line'])->group(function(){
    Route::get('', 'DailyLineController@index')->name('daily_line.index');
    Route::get('baru', 'DailyLineController@create')->name('daily_line.create');
    Route::get('ubah/{id}', 'DailyLineController@edit')->name('daily_line.edit');
    Route::get('data', 'DailyLineController@data')->name('daily_line.data');
    Route::post('store', 'DailyLineController@store')->name('daily_line.store');
    Route::post('update/{id}', 'DailyLineController@update')->name('daily_line.update');
    Route::post('delete/{id}', 'DailyLineController@destroy')->name('daily_line.destroy');
    Route::get('line-picklist/{id}', 'DailyLineController@linePicklist')->name('daily_line.linePicklist');
    Route::get('style-picklist', 'DailyLineController@stylePicklist')->name('daily_line.stylePicklist');
    Route::get('get-daily-working', 'DailyLineController@getDailyWorking')->name('daily_line.get_daily_working');
    Route::get('add-daily-working', 'DailyLineController@addDailyWorking')->name('daily_line.add_daily_working');
    Route::post('store-daily-working', 'DailyLineController@storeDailyWorking')->name('daily_line.store_daily_working');
    Route::post('import/upload-form-import', 'DailyLineController@uploadFormImport')->name('daily_line.uploadFormImport');
    Route::get('import/export-form-import', 'DailyLineController@exportFormImport')->name('daily_line.exportFormImport');
    Route::get('import', 'DailyLineController@import')->name('daily_line.import');
    Route::get('send-alert-andon', 'DailyLineController@SendAlertAndon')->name('daily_line.send_alert_andon');
    
});
Route::prefix('/process-sewing')->middleware(['permission:menu-process'])->group(function(){
    Route::get('', 'ProcessSewingController@index')->name('process_sewing.index');
    Route::get('baru', 'ProcessSewingController@create')->name('process_sewing.create');
    Route::get('ubah/{id}', 'ProcessSewingController@edit')->name('process_sewing.edit');
    Route::get('data', 'ProcessSewingController@data')->name('process_sewing.data');
    Route::post('store', 'ProcessSewingController@store')->name('process_sewing.store');
    Route::post('update/{id}', 'ProcessSewingController@update')->name('process_sewing.update');
    Route::put('delete/{id}', 'ProcessSewingController@destroy')->name('process_sewing.destroy');
});
Route::prefix('/alur-kerja')->middleware(['permission:menu-workflow'])->group(function(){
    Route::get('', 'WorkFlowController@index')->name('alur_kerja.index');
    Route::get('baru/{id}', 'WorkFlowController@create')->name('alur_kerja.create');
    Route::post('update-header/{id}', 'WorkFlowController@updateHeader')->name('alur_kerja.update_header');
    Route::get('list-of-style', 'WorkFlowController@listOfStyle')->name('alur_kerja.listOfStyle');
    Route::get('ubah/{id}', 'WorkFlowController@edit')->name('alur_kerja.edit');
    Route::get('data', 'WorkFlowController@data')->name('alur_kerja.data');
    Route::get('data-detail-workflow', 'WorkFlowController@dataDetailWorkflow')->name('alur_kerja.data_detail_workflow');
    Route::post('simpan-header', 'WorkFlowController@storeHeader')->name('alur_kerja.simpan_header');
    Route::post('simpan-style', 'WorkFlowController@storeStyle')->name('alur_kerja.simpan_style');
    Route::post('simpan-detail', 'WorkFlowController@storeDetail')->name('alur_kerja.simpan_detail');
    Route::get('baru/create-process/{id}', 'WorkFlowController@createProcess')->name('alur_kerja.create_process');
    Route::get('baru/merge-process/{id}', 'WorkFlowController@mergeProcess')->name('alur_kerja.merge_process');
    Route::post('store-process/{id}', 'WorkFlowController@storeProcess')->name('alur_kerja.store_process');
    Route::post('store-merger-process/{id}', 'WorkFlowController@storeMergeProcess')->name('alur_kerja.store_merge_process');
    Route::post('update/{id}', 'WorkFlowController@update')->name('alur_kerja.update');
    Route::put('delete/{id}', 'WorkFlowController@destroyHeader')->name('alur_kerja.destroy');
    Route::put('delete-detail/{id}', 'WorkFlowController@destroyDetail')->name('alur_kerja.destroy_detail');
    Route::get('import/{id}', 'WorkFlowController@import')->name('alur_kerja.import');
    Route::post('import/upload-form-import/{id}', 'WorkFlowController@uploadFormImport')->name('alur_kerja.uploadFormImport');
    Route::get('import/export-form-import/{id}', 'WorkFlowController@exportFormImport')->name('alur_kerja.exportFormImport');
    Route::get('get-workflow', 'WorkFlowController@getWorkflow')->name('alur_kerja.get_workflow');
    Route::get('style-outstanding', 'WorkFlowController@styleOutstanding')->name('alur_kerja.style_outstanding');
    Route::get('get-style', 'WorkFlowController@getStyle')->name('alur_kerja.get_style');
    Route::get('get-process', 'WorkFlowController@getProcess')->name('alur_kerja.get_process');
    Route::get('complain-workflow-data', 'WorkFlowController@getComplainWorkflowData')->name('alur_kerja.complain_workflow_data');
    Route::get('send-message', 'WorkFlowController@sendMessage')->name('alur_kerja.send_message');
    Route::post('store-message/{id}', 'WorkFlowController@storeMessage')->name('alur_kerja.store_message');
    Route::get('download/report-complain', 'WorkFlowController@downloadReportComplain')->name('alur_kerja.download_report_complain');
});
Route::prefix('/mesin')->middleware(['permission:menu-mesin'])->group(function(){
    Route::get('', 'MachineController@index')->name('mesin.index');
    Route::get('baru', 'MachineController@create')->name('mesin.create');
    Route::get('ubah/{id}', 'MachineController@edit')->name('mesin.edit');
    Route::get('data', 'MachineController@data')->name('mesin.data');
    Route::post('store', 'MachineController@store')->name('mesin.store');
    Route::post('update/{id}', 'MachineController@update')->name('mesin.update');
    Route::put('delete/{id}', 'MachineController@destroy')->name('mesin.destroy');
});
Route::prefix('/report/qc-endline-output')->middleware(['permission:menu-qc-endline-output'])->group(function(){
    Route::get('', 'ReportQcEndlineOutput@index')->name('qc_endline_output.index');
    Route::get('data', 'ReportQcEndlineOutput@data')->name('qc_endline_output.data');
    Route::get('po-buyer-picklist', 'ReportQcEndlineOutput@poBuyerPicklist')->name('qc_endline_output.po_buyer_picklist');
    Route::get('get-size', 'ReportQcEndlineOutput@getSize')->name('qc_endline_output.get_size');
    Route::get('export-form-filter', 'ReportQcEndlineOutput@exportFormFilter')->name('qc_endline_output.export_form_Filter');
    Route::get('export-all', 'ReportQcEndlineOutput@downloadAll')->name('qc_endline_output.export_all');
});
Route::prefix('/report/wft')->middleware(['permission:menu-wft-inline-endline'])->group(function(){
    Route::get('', 'ReportWftController@index')->name('wft.index');
    Route::get('data', 'ReportWftController@data')->name('wft.data');
    Route::get('get-grand-total', 'ReportWftController@getGrandTotal')->name('wft.get_grand_total');
    Route::get('po-buyer-picklist', 'ReportWftController@poBuyerPicklist')->name('wft.po_buyer_picklist');
    Route::get('get-size', 'ReportWftController@getSize')->name('wft.get_size');
    Route::get('export-form-filter-line', 'ReportWftController@exportFormFilterLine')->name('wft.export_form_filter_line');
    Route::get('export-form-filter-style', 'ReportWftController@exportFormFilterStyle')->name('wft.export_form_filter_style');
    Route::get('export-form-week', 'ReportWftController@downloadWeek')->name('wft.export_form_week');
});
Route::prefix('/report/operator-performance')->middleware(['permission:menu-operator-performance'])->group(function(){
    Route::get('', 'ReportOperatorPerformanceController@index')->name('operator_performancec.index');
    Route::get('data', 'ReportOperatorPerformanceController@data')->name('operator_performancec.data');
    Route::get('po-buyer-picklist', 'ReportOperatorPerformanceController@poBuyerPicklist')->name('operator_performancec.po_buyer_picklist');
    Route::get('get-size', 'ReportOperatorPerformanceController@getSize')->name('operator_performancec.get_size');
    Route::get('export-form-filter-summary', 'ReportOperatorPerformanceController@exportFormFilterSummary')->name('operator_performance.export_form_filter_summary');
});
Route::prefix('/report/qc-inline/tls-inline')->middleware(['permission:menu-tls-inline'])->group(function(){
    Route::get('', 'ReportTlsInlineController@index')->name('tls_inline.index');
    Route::get('data', 'ReportTlsInlineController@data')->name('tls_inline.data');
    Route::get('po-buyer-picklist', 'ReportTlsInlineController@poBuyerPicklist')->name('tls_inline.po_buyer_picklist');
    Route::get('get-size', 'ReportTlsInlineController@getSize')->name('tls_inline.get_size');
    Route::get('export-form-rft', 'ReportTlsInlineController@exportFromRft')->name('tls_inline.export_form_rft');
});
Route::prefix('/report/monitoring-perjam')->middleware(['permission:menu-monitoring-hourly'])->group(function(){
    Route::get('', 'ReportHourlyMonitoringController@index')->name('monitoring_hourly.index');
    Route::get('data', 'ReportHourlyMonitoringController@data')->name('monitoring_hourly.data');
    Route::get('export-form-output-hourly', 'ReportHourlyMonitoringController@exportFormOutputHourly')->name('monitoring_hourly.export_form_output_hourly');
});
Route::prefix('/report/monitoring-perjam-wft')->middleware(['permission:menu-monitoring-hourly-wft'])->group(function(){
    Route::get('', 'ReportHourlyWftController@index')->name('monitoring_hourly_wft.index');
    Route::get('data', 'ReportHourlyWftController@data')->name('monitoring_hourly_wft.data');
    Route::get('export-form-output-hourly', 'ReportHourlyWftController@exportFormOutputHourly')->name('monitoring_hourly_wft.export_form_output_hourly');
});
Route::prefix('/report/menu-monitoring-hourly-defect')->middleware(['permission:menu-monitoring-hourly-defect'])->group(function(){
    Route::get('', 'ReportHourlyDefectController@index')->name('monitoring_hourly_defect.index');
    Route::get('data', 'ReportHourlyDefectController@data')->name('monitoring_hourly_defect.data');
    Route::get('export-form-output-hourly', 'ReportHourlyDefectController@exportFormOutputHourly')->name('monitoring_hourly_defect.export_form_output_hourly');
});
Route::prefix('/report/menu-monitoring-hourly-defect-outstanding')->middleware(['permission:menu-monitoring-hourly-defect-outstanding'])->group(function(){
    Route::get('', 'ReportHourlyDefectOutStandingController@index')->name('monitoring_hourly_defect_outstanding.index');
    Route::get('data', 'ReportHourlyDefectOutStandingController@data')->name('monitoring_hourly_defect_outstanding.data');
    Route::get('export-form-output-hourly', 'ReportHourlyDefectOutStandingController@exportFormOutputHourly')->name('monitoring_hourly_defect_outstanding.export_form_output_hourly');
});
Route::prefix('/report/monitoring-perjam-sewing')->middleware(['permission:menu-monitoring-hourly-sewing'])->group(function(){
    Route::get('', 'ReportHourlySewingController@index')->name('monitoring_hourly_sewing.index');
    Route::get('data', 'ReportHourlySewingController@data')->name('monitoring_hourly_sewing.data');
    Route::get('export-form-output-hourly', 'ReportHourlySewingController@exportFormOutputHourly')->name('monitoring_hourly_sewing.export_form_output_hourly');
});
Route::prefix('/report/admin-sewing')->middleware(['permission:menu-adm-line'])->group(function(){
    Route::get('', 'ReportAdmSewingController@index')->name('report_admin_sewing.index');
    Route::get('data', 'ReportAdmSewingController@data')->name('report_admin_sewing.data');
    Route::get('export-form-admin-sewing', 'ReportAdmSewingController@exportFormAdminSewing')->name('report_admin_sewing.export_form_admin_sewing');
    Route::get('po-buyer-picklist', 'ReportAdmSewingController@poBuyerPicklist')->name('report_admin_sewing.po_buyer_picklist');
    Route::get('get-size', 'ReportAdmSewingController@getSize')->name('report_admin_sewing.get_size');
});
Route::prefix('/adjustment/pengurangan-output-qc')->middleware(['permission:menu-adjustment-output-subtraction'])->group(function(){
    Route::get('', 'AdjustmentOutputQcSubtractionController@index')->name('adjustment_output_qc_subtraction.index');
    Route::get('baru', 'AdjustmentOutputQcSubtractionController@create')->name('adjustment_output_qc_subtraction.create');
    Route::get('ubah/{id}/{date}', 'AdjustmentOutputQcSubtractionController@edit')->name('adjustment_output_qc_subtraction.edit');
    Route::get('data', 'AdjustmentOutputQcSubtractionController@data')->name('adjustment_output_qc_subtraction.data');
    Route::get('data-qc-output', 'AdjustmentOutputQcSubtractionController@QcOutputData')->name('adjustment_output_qc_subtraction.data');
    Route::post('store-revision', 'AdjustmentOutputQcSubtractionController@storeRevision')->name('adjustment_output_qc_subtraction.store_revision');
    Route::post('update/{id}', 'AdjustmentOutputQcSubtractionController@update')->name('adjustment_output_qc_subtraction.update');
    Route::put('delete/{id}', 'AdjustmentOutputQcSubtractionController@destroy')->name('adjustment_output_qc_subtraction.destroy');
    Route::get('line-picklist', 'AdjustmentOutputQcSubtractionController@linePicklist')->name('adjustment_output_qc_subtraction.linePicklist');
    Route::get('get-sewer', 'AdjustmentOutputQcSubtractionController@getSewer')->name('adjustment_output_qc_subtraction.get_sewer');
});
Route::prefix('/adjustment/penambahan-output-qc')->middleware(['permission:menu-adjustment-output-addition'])->group(function(){
    Route::get('', 'AdjustmentOutputQcAdditionController@index')->name('adjustment_output_qc_addition.index');
    Route::get('baru', 'AdjustmentOutputQcAdditionController@create')->name('adjustment_output_qc_addition.create');
    Route::get('tambah-output/{production_id}/{production_size_id}', 'AdjustmentOutputQcAdditionController@addOutput')->name('adjustment_output_qc_addition.add_output');
    Route::get('data', 'AdjustmentOutputQcAdditionController@data')->name('adjustment_output_qc_addition.data');
    Route::get('data-qc-output', 'AdjustmentOutputQcAdditionController@QcOutputData')->name('adjustment_output_qc_addition.data');
    Route::get('poreference-picklist', 'AdjustmentOutputQcAdditionController@poreferencePickList')->name('adjustment_output_qc_addition.poreferencePickList');
    Route::post('store-revision', 'AdjustmentOutputQcAdditionController@storeRevision')->name('adjustment_output_qc_addition.store_revision');
    Route::post('update/{id}', 'AdjustmentOutputQcAdditionController@update')->name('adjustment_output_qc_addition.update');
    Route::put('delete/{id}', 'AdjustmentOutputQcAdditionController@destroy')->name('adjustment_output_qc_addition.destroy');
    Route::get('line-picklist', 'AdjustmentOutputQcAdditionController@linePicklist')->name('adjustment_output_qc_addition.linePicklist');
    Route::get('get-sewer', 'AdjustmentOutputQcAdditionController@getSewer')->name('adjustment_output_qc_addition.get_sewer');
});
Route::prefix('/report/wft-nagai')->middleware(['permission:menu-nagai-endline'])->group(function(){
    Route::get('', 'ReportNagaiController@index')->name('report_nagai.index');
    Route::get('data', 'ReportNagaiController@data')->name('report_nagai.data');
    Route::get('export-form-output-hourly', 'ReportNagaiController@exportFormFilter')->name('report_nagai.export_form_filter');
    Route::get('export-form-filter-month', 'ReportNagaiController@downloadFilterMont')->name('report_nagai.export_form_filter_mont');
});
Route::prefix('/dashboard')->middleware(['permission:menu-dashboard-efficiency','permission:menu-dashboard-output-qc'])->group(function(){
    Route::get('', 'DashboardController@efficiency')->name('dashboard_efficiency.efficiency');
    Route::get('output-qc', 'DashboardController@outputQc')->name('dashboard_efficiency.output_qc');
    Route::get('get-efficiency', 'DashboardController@getEfficiency')->name('dashboard_efficiency.get_efficiency');
    Route::get('get-output-qc', 'DashboardController@getOutputQc')->name('dashboard_efficiency.get_output_qc');
    
});
Route::prefix('/report/status-order')->middleware(['permission:menu-status-order'])->group(function(){
    Route::get('', 'StatusOrderController@index')->name('report_status_order.index');
    Route::get('data', 'StatusOrderController@data')->name('report_status_order.data');
    Route::get('export-form-filter', 'StatusOrderController@exportFormFilter')->name('report_status_order.export_form_filter');
});
Route::prefix('/skill-matriks/cycle-time')->middleware(['permission:menu-cycle-time'])->group(function(){
    Route::get('', 'CycleTimeController@index')->name('cycle_time.index');
    Route::get('data', 'CycleTimeController@data')->name('cycle_time.data');
    Route::get('get-data-line', 'CycleTimeController@getDataLine')->name('cycle_time.get_data_line');
    Route::get('get-sewer', 'CycleTimeController@getSewer')->name('cycle_time.get_sewer');
    Route::get('line-picklist', 'CycleTimeController@linePicklist')->name('cycle_time.linePicklist');
    Route::get('style-picklist', 'CycleTimeController@stylePicklist')->name('cycle_time.stylePicklist');
    Route::get('get-day-header', 'CycleTimeController@getDayHeader')->name('daily_line.get_day_header');
    Route::post('store-header', 'CycleTimeController@storeHeader')->name('cycle_time.store_header');
    Route::get('create-cycle/{id}', 'CycleTimeController@createCycle')->name('cycle_time.create_cycle');
    Route::get('get-process', 'CycleTimeController@getProcess')->name('cycle_time.get_process');
    Route::post('store-detail-cylcle/{params}', 'CycleTimeController@storeDetailCycle')->name('cycle_time.store_detail_cycle');
    Route::post('store-draft-cylcle', 'CycleTimeController@storeDraftCycle')->name('cycle_time.store_draft_cycle');
    Route::post('store-cycle-history', 'CycleTimeController@storeCycleHistory')->name('cycle_time.store_cycle_history');
    Route::get('show-history', 'CycleTimeController@showHistory')->name('cycle_time.show_history');
    Route::get('history-process/{line}/{nik}/{style}', 'CycleTimeController@HistoryProcess')->name('cycle_time.history_process');
    Route::get('report-data-entry', 'CycleTimeController@ReportDataEntry')->name('cycle_time.report_data_entry');
    Route::post('lock-cycle', 'CycleTimeController@lockCycle')->name('cycle_time.lock_cycle');
    Route::get('report-time-study/{id}', 'CycleTimeController@reportTimeStudyChart')->name('cycle_time.report_time_study');
    Route::get('destroy-cycle-time/{id}', 'CycleTimeController@destroyCycleTime')->name('cycle_time.destroy-cycle-time');
    Route::get('destroy-draft-cycle-time-detail/{id}', 'CycleTimeController@destroyDraftCycleTimeDetail')->name('cycle_time.destroy_draft_cycle_time_detail');
    Route::get('send-message', 'CycleTimeController@sendMessage')->name('cycle_time.send_message');
    Route::get('list-draft', 'CycleTimeController@listDraft')->name('cycle_time.list_draft');
    Route::get('detail-draft', 'CycleTimeController@detailDraft')->name('cycle_time.detail_draft');
    Route::post('store-message', 'CycleTimeController@storeMessage')->name('cycle_time.store_message');
    Route::get('history-day-process', 'CycleTimeController@HistoryDayProcess')->name('cycle_time.history_day_process');
    Route::get('get-component-process', 'CycleTimeController@getComponent')->name('cycle_time.get_component_process');
});
Route::prefix('/report/actual-production-time')->middleware(['permission:menu-actual-production-time'])->group(function(){
    Route::get('', 'ReportActualTimeProduction@index')->name('report_actual_production_time.index');
    Route::get('data', 'ReportActualTimeProduction@data')->name('report_actual_production_time.data');
    Route::get('data-entry/{id}', 'ReportActualTimeProduction@dataEntry')->name('report_actual_production_time.data_entry');
    Route::get('report-time-study/{id}', 'ReportActualTimeProduction@reportTimeStudyChart')->name('report_actual_production_time.report_time_study');
    Route::get('report-data-entry/{id}', 'ReportActualTimeProduction@reportDataEntry')->name('report_actual_production_time.report_data_entry');
});
Route::prefix('/qty-karton')->middleware(['permission:menu-qty-karton'])->group(function(){
    Route::get('', 'QtyCartonFoldingController@index')->name('qty_carton.index');
    Route::get('baru', 'QtyCartonFoldingController@create')->name('qty_carton.create');
    Route::get('style-picklist', 'QtyCartonFoldingController@stylePicklist')->name('qty_carton.style_picklist');
    Route::get('ubah/{id}', 'QtyCartonFoldingController@edit')->name('qty_carton.edit');
    Route::get('data', 'QtyCartonFoldingController@data')->name('qty_carton.data');
    Route::post('store-qty-carton', 'QtyCartonFoldingController@storeQtyCarton')->name('qty_carton.store_qty_carton');
    Route::post('update/{id}', 'QtyCartonFoldingController@update')->name('qty_carton.update');
    Route::put('delete/{id}', 'QtyCartonFoldingController@destroy')->name('qty_carton.destroy');
});
Route::prefix('/report/folding-output')->middleware(['permission:menu-folding-output'])->group(function(){
    Route::get('', 'ReportFoldingOutputController@index')->name('folding_output.index');
    Route::get('data', 'ReportFoldingOutputController@data')->name('folding_output.data');
    Route::get('po-buyer-picklist', 'ReportFoldingOutputController@poBuyerPicklist')->name('folding_output.po_buyer_picklist');
    Route::get('get-size', 'ReportFoldingOutputController@getSize')->name('folding_output.get_size');
    Route::get('export-form-filter', 'ReportFoldingOutputController@exportFormFilter')->name('folding_output.export_form_Filter');
    Route::get('export-all', 'ReportFoldingOutputController@downloadAll')->name('folding_output.export_all');
});

Route::prefix('/adjustment/folding')->middleware(['permission:menu-adjustment-folding-addition'])->group(function(){
    Route::get('', 'AdjustmentFoldingController@index')->name('adjustment_folding.index');
    Route::get('breakdown-size-scan', 'AdjustmentFoldingController@breakDownSizeScan')->name('adjustment_folding.breakdown_size_scan');
    Route::post('store', 'AdjustmentFoldingController@store')->name('adjustment_folding.store');
});
Route::prefix('/report/hourly-output')->middleware(['permission:menu-hourly-output'])->group(function(){
    Route::get('', 'ReportOutputHourlyController@index')->name('report_hourly_output.index');
    Route::get('data', 'ReportOutputHourlyController@data')->name('report_hourly_output.data');
    Route::get('export-form-output-hourly', 'ReportOutputHourlyController@exportFormFilter')->name('report_hourly_output.export_form_filter');
    Route::get('export-form-filter-month', 'ReportOutputHourlyController@downloadFilterMont')->name('report_hourly_output.export_form_filter_mont');
});
Route::prefix('andon')->group(function () 
{
    Route::get('supply-center/{factory}/{id}', 'AndonSupplyController@index')->name('andon.index');
    Route::get('supply-data', 'AndonSupplyController@getDataDashboard')->name('andon.supply_data');
});
Route::prefix('/master-factory')->middleware(['permission:menu-factory'])->group(function(){
    Route::get('', 'FactoryController@index')->name('master_factory.index');
    Route::get('baru', 'FactoryController@create')->name('master_factory.create');
    Route::get('ubah/{id}', 'FactoryController@edit')->name('master_factory.edit');
    Route::get('data', 'FactoryController@data')->name('master_factory.data');
    Route::post('store', 'FactoryController@store')->name('master_factory.store');
    Route::post('update/{id}', 'FactoryController@update')->name('master_factory.update');
    Route::put('delete/{id}', 'FactoryController@destroy')->name('master_factory.destroy');
});
Route::prefix('/report/andon-supply')->middleware(['permission:menu-andon-supply'])->group(function(){
    Route::get('', 'ReportAndonSupplyController@index')->name('report_andon_supply.index');
    Route::get('data', 'ReportAndonSupplyController@data')->name('report_andon_supply.data');
    Route::get('export-form-filter', 'ReportAndonSupplyController@exportFormFilter')->name('report_andon_supply.export_form_filter');
    Route::get('download-andon-report', 'ReportAndonSupplyController@downloadAndonExcel')->name('report_andon_supply.download_andon_report');
    Route::get('dashboard-andon-supply', 'ReportAndonSupplyController@dashboardAndonSupply')->name('report_andon_supply.andon_supply');
    Route::get('data-andon-supply', 'ReportAndonSupplyController@dataDashboardAndonSupply')->name('report_andon_supply.data_andon_supply');
});