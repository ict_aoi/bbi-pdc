<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class QcInline extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','production_id','process_sewing_id','machine_id','grade_defect_id','round','sewer_name','sewer_nik','qc_name','workflow_detail_id','qc_nik','update_at','delete_at','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];

	public function gradeDefect()
	{
		return $this->belongsTo('App\Models\GradeDefect');
	}
	public function workflowDetail()
	{
		return $this->belongsTo('App\Models\WorkflowDetail');
	}
	static function getRoundBefore($line_id,$qc_nik)
	{
		$roundBefore = StatusRound::whereNotNull('delete_at')
		->where(function($query) use($line_id,$qc_nik)
		{
			if ($qc_nik) {
				$query = $query->Where('qc_nik',$qc_nik);
			}
			$query = $query->Where('line_id',$line_id);
		})
		->orderBy('delete_at','desc')
		->first();
		return $roundBefore;
	}
	static function getStyleUnion($factory,$startdate,$enddate){
		$query = DB::select(db::Raw("
		select * from (
			(SELECT DISTINCT
				lines.factory_id,
				summary_productions.line_id,
				summary_productions.DATE AS created_at,
				summary_productions.style,
				lines.NAME AS line_name,
				lines.order as urut
			FROM
				summary_productions
				LEFT JOIN lines ON lines.id = summary_productions.line_id 
			WHERE
				summary_productions.date BETWEEN '$startdate'
				AND '$enddate' and lines.factory_id='$factory'
			ORDER BY lines.order)
			UNION
			(SELECT DISTINCT
				qc_inlines.factory_id,
				productions.line_id,
				DATE ( qc_inlines.created_at ) AS created_at,
				productions.style,
				lines.NAME AS line_name,
				lines.order as urut
			FROM
				qc_inlines
				LEFT JOIN productions ON productions.id = qc_inlines.production_id
				LEFT JOIN lines ON lines.id = productions.line_id 
			WHERE
				qc_inlines.factory_id ='$factory'
				AND DATE ( qc_inlines.created_at ) BETWEEN '$startdate'
				AND '$enddate'
			ORDER BY lines.order)
			)tbl_style
			ORDER BY created_at,urut"));
		return $query;
	}
	static function getOptSewing($line_id,$date)
	{
		$query = DB::select(db::Raw("
		select date,factory_id,line_id,count(*) as total_operator,max(round) as round from (
			select
				date,
				factory_id,
				line_id,
				process_name,
				sewer_nik,
				round
			from get_qc_inline_v2 where date='$date'
			and line_id='$line_id'
			GROUP BY
			date,
				factory_id,
				line_id,
				process_name,
				sewer_nik,round
			)tbl_count
			GROUP BY
			date,factory_id,line_id"));
		return $query;
	}
	static function getTotalDefect($line_id,$date)
	{
		$query = DB::select(db::Raw("
			SELECT count(*) as total
			FROM qc_inlines qi
			LEFT JOIN qc_inline_details qid ON qid.qc_inline_id = qi.id
			LEFT JOIN productions pr ON pr.id = qi.production_id
			LEFT JOIN lines li ON li.id = pr.line_id
		where date(qi.created_at)='$date'
		and qid.defect_id<>0
		and pr.line_id='$line_id'"));
		return $query;
	}
	static function getTotalProcess($line_id,$date)
	{
		$query = DB::select(db::Raw("
		SELECT distinct workflow_detail_id
		FROM qc_inlines qi
			LEFT JOIN qc_inline_details qid ON qid.qc_inline_id = qi.id
			LEFT JOIN productions pr ON pr.id = qi.production_id
			LEFT JOIN lines li ON li.id = pr.line_id
		where date(qi.created_at)='$date'
		and pr.line_id='$line_id'"));
		return $query;
	}
	
}