<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class SewingDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	public $timestamps = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['sewing_id','counter','ip_address','created_at','updated_at'];
	protected $dates     = ['created_at'];


	static function getCounterSewing($style,$line_id,$date,$article)
	{
		$data            = DB::select(db::raw("
							select count(*) as total_counter from sewing_details where sewing_id in(
								SELECT 
										sw.id
								FROM sewings sw
								LEFT JOIN styles st ON erp = style
								where st.default='$style' and sw.line_id='$line_id' and sw.article='$article'
							) and date(created_at)<='$date'
						"));
		return $data;
	}
}