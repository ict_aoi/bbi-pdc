<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class QcInlineDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['qc_inline_id','defect_id','update_at','delete_at','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];
}
