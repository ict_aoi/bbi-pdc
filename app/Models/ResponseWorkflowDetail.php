<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ResponseWorkflowDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['response_workflow_id','message','status','is_read_qc','update_at','delete_at','update_user_id'];
	protected $dates     = ['created_at'];
}