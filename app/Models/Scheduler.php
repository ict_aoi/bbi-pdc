<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['job','status','start_job','end_job'];
	protected $dates     = ['created_at'];
}
