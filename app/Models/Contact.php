<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'contact_number', 'name'
    ];
    protected $dates     = ['created_at'];
}