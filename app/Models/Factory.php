<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
	use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
    protected $fillable  = ['code','name','alamat','description','wft','delete_at'];
	protected $dates     = ['created_at']; 

	public function lines()
    {
        return $this->hasMany('App\Models\lines');
    }
	public function scopeActive($query)
	{
		return $query->whereNull('delete_at');
	}

}