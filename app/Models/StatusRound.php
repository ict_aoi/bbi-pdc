<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class StatusRound extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','line_id','round','qc_nik','update_at','delete_at','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];
	
	public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }
}
