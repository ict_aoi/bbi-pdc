<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class QcOutputView extends Model
{
    public $incrementing    = false;
    protected $guarded      = ['qc_endline_id'];
    protected $table        = 'qc_output_v';
    
    public function lines()
    {
        return $this->hasMany('App\Models\lines');
    }

    static function getBalance($date,$style,$line_id)
    {
        $styleMerge = '';
        foreach ($style as $s) {
            $styleMerge.=$s."','";
        }
        $styleMerge = rtrim($styleMerge,",'");
        $data = QcOutputView::select(db::raw('sum(qc_output_v.qty_order)-(sum(qc_output_v.repairing)+sum(qc_output_v.total_output)) as balance'),db::raw('COALESCE(daily.commitment_line,0) as commitment_line'),'qc_output_v.line_id',db::raw('sum(qc_output_v.qty_order) as qty_loading'),'daily.safety_stock')
        ->leftjoin(
            DB::raw('(select line_id,sum(commitment_line) as commitment_line,round(1.5*(sum(commitment_line)/working_hours),0) as safety_stock from daily_line_v where date(created_at)=' . "'$date'" . ' and style in(' . "'$styleMerge'" . ') and line_id=' . "'$line_id'" . ' group by line_id,working_hours)
            daily'),
            function ($leftjoin) {
                $leftjoin->on('qc_output_v.line_id', '=', 'daily.line_id');
            }
        )
        // ->where('start_date','>=','2021-01-01')
        ->whereIn('qc_output_v.style_merge',$style)
        ->where('qc_output_v.line_id',$line_id)
        ->groupBy('daily.commitment_line','qc_output_v.line_id','daily.safety_stock')
        ->first();
        return $data;
    }
    static function getBalanceStyle($date,$style,$line_id)
    {
        $data = QcOutputView::select(db::raw('sum(qc_output_v.qty_order)-(sum(qc_output_v.repairing)+sum(qc_output_v.total_output)) as balance'),db::raw('COALESCE(daily.commitment_line,0) as commitment_line'),'qc_output_v.line_id',db::raw('sum(qc_output_v.qty_order) as qty_loading'))
        ->leftjoin(
            DB::raw('(select line_id,sum(commitment_line) as commitment_line from daily_line_v where date(created_at)=' . "'$date'" . ' and style=' . "'$style'" . ' and line_id=' . "'$line_id'" . ' group by line_id)
            daily'),
            function ($leftjoin) {
                $leftjoin->on('qc_output_v.line_id', '=', 'daily.line_id');
            }
        )
        // ->where('start_date','>=','2021-01-01')
        ->where('qc_output_v.style_merge',$style)
        ->where('qc_output_v.line_id',$line_id)
        ->groupBy('daily.commitment_line','qc_output_v.line_id')
        ->first();
        return $data;
    }
}