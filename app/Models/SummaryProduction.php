<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class SummaryProduction extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['line_id','name','style','poreference','size','article','output','start_hours','end_hours','date'];
	protected $dates     = ['created_at'];
}
