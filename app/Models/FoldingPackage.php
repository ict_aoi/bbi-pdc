<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class FoldingPackage extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['scan_id','barcode_id','pkg_count','item_qty','counter','current_department','current_status','delete_at'];
	protected $dates     = ['created_at'];
}
