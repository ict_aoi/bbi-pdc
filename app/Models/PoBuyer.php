<?php namespace App\Models;

use app\Uuids;
use Illuminate\Database\Eloquent\Model;

class PoBuyer extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['poreference','lc_date','statistical_date','job_order','season','brand','requirement_date','promise_date'];
	protected $dates     = ['created_at'];
}
