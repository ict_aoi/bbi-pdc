<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ProductionSizeHistory extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['production_size_id'
        ,'note'
        ,'created_at'
        ,'updated_at'
        ,'qty_old'
        ,'qty_new'
        ,'qty_operator'
    ];

    public function productionSize()
    {
        return $this->belongsTo('App\Models\ProductionSize');
    }
    static function getHistoryDate($date,$lineId)
    {
        $data = DB::select(db::raw("
                select 
                    pr.line_id,
                    min(psh.created_at) as date,
                    sum(psh.qty_new) as total
                from production_size_histories psh
                LEFT JOIN production_sizes ps on ps.id=psh.production_size_id
                LEFT JOIN productions pr on pr.id=ps.production_id
                where date(psh.created_at)>'$date'
                and pr.line_id='$lineId' 
                GROUP BY
                line_id;
            "));
        return $data; 
    }
}