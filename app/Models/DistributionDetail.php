<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DistributionDetail extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['distribution_id'
        ,'barcode_id'
        ,'component_name'
        ,'style_id_cdms'
        ,'create_user_nik'
        ,'create_user_name'
        ,'move_user_nik'
        ,'move_user_name'
        ,'is_movement'
        ,'deleted_at'
        ,'date_movement'
        ,'ip_address'
    ];
    public function scopeActive($query)
	{
		return $query->whereNull('deleted_at');
	}
    public function scopeMovement($query)
	{
		return $query->whereNull('date_movement');
	}
    public function distribution()
    {
        return $this->belongsTo('App\Models\Distribution');
    }
    static function countDistributionMove($distribution_id,$type=null)
    {
        return DistributionDetail::active()
                        ->where('distribution_id',$distribution_id)
                        ->when($type,function($q)use($type){
                            $q->whereNull('date_movement');
                        })->get();
    }
}