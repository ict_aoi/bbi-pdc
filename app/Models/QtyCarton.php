<?php namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class QtyCarton extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['style','size','qty','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];
}