<?php namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class CycleTime extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['cycle_time_batch_id','workflow_detail_id','wip','burst','is_assembly','smv','sewer_name','is_draft','is_copied','cycle_time_id_before','sewer_nik','average','update_at','delete_at'];
	protected $dates     = ['created_at'];

    public function workflowDetail()
	{
		return $this->belongsTo('App\Models\WorkflowDetail');
	}
    public function cycleTimeBatch()
	{
		return $this->belongsTo('App\Models\CycleTimeBatch');
	}
	static function fetchDataEntry($isDraft,$cycle_time_batch_id,$search,$user=true)
	{
		$data = DB::table('report_data_entry_cycle')
        ->where(function($query) use ($search,$user){
            $query->where(db::raw('lower(sewer_name)'), 'LIKE', "%$search%")
            ->orWhere(db::raw('sewer_nik'), 'LIKE', "%$search%");
        })
        ->where([
            ['cycle_time_batch_id', $cycle_time_batch_id],
            ['is_draft', $isDraft]
		]);
		if ($user==true) {
			$data= $data->where('create_user_id', Auth::user()->id);
		}
        $data=$data->paginate(10);
		return $data;
	}
}