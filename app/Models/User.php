<?php namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'factory_id', 'department_name','nik','name','photo','sex','email','email_verified_at','production', 'password','is_super_admin','delete_at','create_user_id','delete_user_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    protected $dates     = ['created_at'];
    public function roles(){
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }
    public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }
}