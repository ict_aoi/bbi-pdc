<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ProductionSize extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at'];
    protected $fillable     = ['production_id'
        ,'size'
        ,'qty'
        ,'barcode'
        ,'created_at'
        ,'updated_at'
        ,'delete_at'
        ,'created_user_by_nik'
        ,'created_user_by_name'
        ,'updated_user_by_nik'
        ,'updated_user_by_name'
        ,'deleted_user_by_nik'
        ,'deleted_user_by_name'
        ,'delete_user_id'];

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }

    public function production()
    {
        return $this->belongsTo('App\Models\Production');
    }

    public function productionSizeHistory()
    {
        return $this->hasMany('App\Models\ProductionSizeHistory');
    }

    public function distribution()
    {
        return $this->hasMany('App\Models\Distribution');
    }
}
