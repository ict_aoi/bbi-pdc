<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class DailyLineView extends Model
{
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $table        = 'daily_line_v';

    public function lines()
    {
        return $this->hasMany('App\Models\lines');
    }
    static function getDataDaily($date,$buyer)    
    {
        $dataDaily = DailyLineView::whereNull('deleted_at')
        ->where([
            [db::raw('date(created_at)'),$date],
            ['buyer',$buyer]
        ])
        ->orderBy('urut','asc');
        return $dataDaily;
    }
    static function getStyle($date,$line_id)
    {
        $dailyLine = DailyLineView::select('start','end','working_hours',db::raw("string_agg(style, ',') as style"),db::raw("sum(commitment_line) as commitment_line"),'line_id')
        ->where([
            [db::raw('date(created_at)'),$date],
            ['line_id',$line_id]
        ])
        ->whereNull('delete_at')
        ->groupBy('line_id','start','end','working_hours')
        ->first();
        return $dailyLine;
    }
    static function getDashboardAndon($date,$buyer,$factory_id,$line_id=null)
    {
        $close_by = 'ie';
        $data = DailyLineView::select(
            'daily_line_v.factory_id',
            'daily_line_v.line_id',
            'daily_line_v.name',
            'daily_line_v.code',
            db::raw('sum(daily_line_v.commitment_line) as commitment_line'),
            db::raw('round(1.5*(sum(daily_line_v.commitment_line)/daily_line_v.working_hours)) as safety_stock'),
            'data_balance.balance',
            'daily_line_v.working_hours',
            db::raw('json_agg(daily_line_v.style) as style'),
            db::raw('andon.start_time as andon_active'),
            db::raw('count(*) as total'),
            db::raw('COALESCE(andon_history.total_history,0)as total_history'),
            'andon.close_time'
            )
        ->leftjoin(
            DB::raw('(
                select sum(qv.qty_order)-(sum(qv.repairing)+sum(qv.total_output)) as balance,qv.line_id from qc_output_v qv 
                left join daily_line_v dlv on dlv.line_id=qv.line_id and dlv.style=qv.style_merge
                where  DATE (dlv.created_at ) =' . "'$date'" . ' and dlv.factory_id=' . "'$factory_id'" . ' and qv.buyer=' . "'$buyer'" . '
                GROUP BY qv.line_id)
            data_balance'),
            function ($leftjoin) {
                $leftjoin->on('data_balance.line_id', '=', 'daily_line_v.line_id');
            }
        )
        ->leftjoin(
            DB::raw('(
                select line_id,start_time,close_time from andon_supplies where date(created_at)='. "'$date'".' and close_time is null)
                andon'),
            function ($leftjoin) {
                $leftjoin->on('andon.line_id', '=', 'daily_line_v.line_id');
            }
        )
        ->leftjoin(
            DB::raw('(
                select 
                    line_id,count(*) as total_history from andon_supplies  where date(created_at)='. "'$date'".' and close_time is not null and close_by is null GROUP BY line_id)andon_history'),
            function ($leftjoin) {
                $leftjoin->on('andon_history.line_id', '=', 'daily_line_v.line_id');
            }
        )
        ->whereNull('daily_line_v.delete_at')
        ->where([
            [db::raw('date(daily_line_v.created_at)'),$date],
            ['daily_line_v.buyer',$buyer],
            ['daily_line_v.factory_id',$factory_id],
        ])
        ->when($line_id,function($q)use($line_id){
            $q->where('daily_line_v.line_id',$line_id);
        })
        ->groupBy('daily_line_v.line_id','daily_line_v.factory_id','daily_line_v.code','daily_line_v.name','data_balance.balance','daily_line_v.urut','andon.start_time','andon.close_time','andon_history.total_history','daily_line_v.working_hours')
        ->orderBy('daily_line_v.urut')->get();
        return $data;
    }
    static function getMaxDaily($date,$buyer,$factory)
    {
        $data = DB::select('select max(total) from (
            select line_id,count(*) as total from daily_line_v where deleted_at is null and
            date(created_at)='."'$date'".' and buyer='."'$buyer'" .' and factory_id='."'$factory'" .'
            GROUP BY line_id
            )selectmax');
        return $data;
    }
    
}