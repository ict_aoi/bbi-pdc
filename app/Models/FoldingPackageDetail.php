<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class FoldingPackageDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['folding_output_id','folding_package_id','folding_package_detail_size_id','barcode_garment_id','counter','ip_address','created_by_name','created_by_nik','delete_at'];
	protected $dates     = ['created_at'];
}
