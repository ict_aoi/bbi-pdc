<?php namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

use App\Models\QcOutputView;
use App\Models\DailyLineView;
use App\Models\ProductionSizeHistory;

class AndonSupply extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['line_id','target_line','safety_stock','balance','start_time','close_time','create_user_name','create_user_nik','close_user_name','close_user_nik','close_by'];
	protected $dates     = ['created_at','deleted_at'];

	public function scopeActive($query)
	{
		return $query->whereNull('close_time');
	}
	public function scopeNonActive($query)
	{
		return $query->whereNotNull('close_time');
	}
	public function lines()
    {
        return $this->hasMany('App\Models\lines');
    }
	static function getDataSummary($date,$buyer)
	{

		$data = DailyLineView::select(
			'daily_line_v.line_id',
			'daily_line_v.code_machine',
			'daily_line_v.name',
			db::raw('sum(daily_line_v.commitment_line) as commitment_line'),
			'daily_line_v.urut',
			db::raw("string_agg(distinct daily_line_v.style, ',') as style"),
			db::raw("string_agg(distinct active.style, ',') as style_active"),
			db::raw("string_agg(closed.leadtime,',') as leadtime")
		)
        ->leftjoin(
            DB::raw("(select line_id,style || ' start : ' ||start_time as style from andon_supplies	where close_time is null)
            active"),
            function ($leftjoin) {
                $leftjoin->on('active.line_id', '=', 'daily_line_v.line_id');
            }
        )
        ->leftjoin(
            DB::raw("(select line_id,style || ' '||COALESCE(date_part('hours', start_time::timestamp - close_time::timestamp),0) as leadtime
			from andon_supplies where close_time is not null and date(close_time)="."'$date'".")
            closed"),
            function ($leftjoin) {
                $leftjoin->on('closed.line_id', '=', 'daily_line_v.line_id');
            }
        )
		->where([
			[db::raw('date(daily_line_v.created_at)'),$date],
			['daily_line_v.buyer',$buyer],
		])
		->whereNull('daily_line_v.deleted_at')
        ->groupBy('daily_line_v.name','daily_line_v.line_id','daily_line_v.code_machine','daily_line_v.urut')
        ->get();
        return $data;
	}
	static function getBalance($style,$line_id)
	{
		$styleMerge = explode(",",$style);
		// dd($styleMerge);
		$data = QcOutputView::select(db::raw('sum(qty_order)-(sum(total_output)+sum(repairing)) as balance'))
				->whereIn('style_merge',$styleMerge)
				->where('line_id',$line_id)
				->first();
		return $data;
	}
	static function getBalanceStyle($date,$style,$line_id)
    {
		$styleMerge = '';
		// $style=explode(',',$style);
        foreach ($style as $s) {
            $styleMerge.=$s."','";
        }
        $styleMerge = rtrim($styleMerge,",'");
        $data = QcOutputView::select(db::raw('qc_output_v.style_merge as style'),db::raw('sum(qc_output_v.qty_order)-(sum(qc_output_v.repairing)+sum(qc_output_v.total_output)) as balance'),db::raw('COALESCE(daily.commitment_line,0) as commitment_line'),'qc_output_v.line_id',db::raw('sum(qc_output_v.qty_order) as qty_loading'))
        ->leftjoin(
            DB::raw('(select style,line_id,sum(commitment_line) as commitment_line from daily_line_v where date(created_at)=' . "'$date'" . ' and style in(' . "'$styleMerge'" . ') and line_id=' . "'$line_id'" . ' group by line_id,style)
            daily'),
            function ($leftjoin) {
                $leftjoin->on('qc_output_v.line_id', '=', 'daily.line_id');
                $leftjoin->on('qc_output_v.style_merge', '=', 'daily.style');
            }
        )
        // ->where('start_date','>=','2021-01-01')
        ->whereIn('qc_output_v.style_merge',$style)
        ->where('qc_output_v.line_id',$line_id)
        ->groupBy('daily.commitment_line','qc_output_v.line_id','qc_output_v.style_merge')
        ->get();
        return $data;
    }
    static function getHistoryLoading($date,$startDate,$endDate,$line_id,$style)
    {
        $data = ProductionSizeHistory::select(
            db::raw("string_agg(distinct productions.style,',') as style"),
            'productions.line_id',
            db::raw('sum(production_size_histories.qty_operator) as qty')
        )
        ->leftJoin('production_sizes','production_sizes.id','production_size_histories.production_size_id')
        ->leftJoin('productions','productions.id','production_sizes.production_id')
        ->where([
            [db::raw('date(production_size_histories.updated_at)'),$date],
            ['production_size_histories.updated_at','>=',$startDate],
            ['production_size_histories.updated_at','<=',$endDate]
        ])
        ->whereIn('productions.style',$style)
        ->groupBy('productions.line_id')
        ->get();
        return $data;
    }
    static function getMaxHistory($date,$buyer,$factory)
    {
        $close_by = 'adm';
        $data = DB::select('select max(total_history)
        from (select line_id,COALESCE(count(*),0) as total_history from andon_supplies ads LEFT JOIN lines li on li.id=ads.line_id
        where date(ads.created_at)='."'$date'".' and close_time is not null and li.factory_id='."'$factory'".'
        and li.buyer='."'$buyer'".' and close_by is null
        GROUP BY
        line_id
            )selectmax');
        return $data;
    }
    static function getAndonSupplyView($factory,$buyer,$startDate,$endDate,$checkCloseTime)
    {
        $data = DB::table('andon_supplies_v')
        ->where('factory_id',$factory)
        ->whereIn('buyer',$buyer)
        ->whereBetween('transdate',[$startDate,$endDate])
        ->where(function($query) use ($checkCloseTime){
            if ($checkCloseTime) {
                $query->whereNull('close_time');
            } else {
                $query->whereNotNull('close_time');
            }
        });
        return $data;
    }
}