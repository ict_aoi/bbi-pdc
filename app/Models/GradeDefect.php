<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class GradeDefect extends Model
{
    // use Uuids;
	public $incrementing = true;
	protected $guarded   = ['id'];
	protected $fillable  = ['name','color'];
	protected $dates     = ['created_at'];
	
}
