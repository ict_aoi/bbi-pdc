<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AdjustmentQcSubtraction extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at'];
    protected $fillable     = ['factory_id'
        ,'line_id'
        ,'name'
        ,'poreference'
        ,'article'
        ,'style'
        ,'size'
        ,'qty_revision'
        ,'reason'
        ,'request_by_name'
        ,'request_by_nik'
        ,'created_at'
        ,'updated_at'
        ,'date_output'
        ,'delete_user_id'];
}
