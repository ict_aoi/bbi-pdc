<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['barcode','referral_code','sequence','is_fixing'];
}
