<?php namespace App\Models;

use Request;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class LogLogin extends Model
{
    use Uuids;
	public $timestamps   = false;
	public $incrementing = false;
	protected $guarded      = ['id'];
	protected $fillable     = ['line_id','name','created_by_nik','created_by_name','factory_id','subdept_name','delete_at','expired_date','ip_address'];
	protected $dates        = ['created_at'];
	
	public function lines()
    {
        return $this->belongsTo('App\Models\Line');
    }
}