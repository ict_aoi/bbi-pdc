<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','code','alias','buyer','order','name','fast_react_id','wft','is_scan','is_scan_garment','is_infinity_round','update_at','delete_at','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];
	
	public function scopeActive($query)
	{
		return $query->whereNull('delete_at');
	}
	public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }
}