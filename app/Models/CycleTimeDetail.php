<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class CycleTimeDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['cycle_time_id','time','time_recorded','duration','update_at','delete_at'];
	protected $dates     = ['created_at'];
}