<?php namespace App\Models;

use App\Uuids;
use App\Models\Machine;
use App\Models\ProcessSewing;
use Illuminate\Database\Eloquent\Model;

class HistoryProcessSewing extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','process_sewing_id','machine_id','line_id','style','sewer_name','sewer_nik','workflow_detail_id','style','delete_at'];
	protected $dates     = ['created_at'];

	public function machine()
	{
		return $this->belongsTo('App\Models\Machine');
	}
	public function processSewing()
	{
		return $this->belongsTo('App\Models\ProcessSewing');
	}
}