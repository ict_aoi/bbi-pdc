<?php namespace App\Models\Absensi;

use Illuminate\Database\Eloquent\Model;

class AbsensiBbi extends Model
{
    protected $connection   = 'bbi_apparel';
    protected $guarded      = ['id'];
    protected $table        = 'get_employee';
}