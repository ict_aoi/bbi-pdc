<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DistributionMovementTemp extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['distribution_id'
        ,'distribution_detail_id'
        ,'line_id'
        ,'barcode_id'
        ,'ip_address'
    ];
    
    public function distributionDetail()
    {
        return $this->belongsTo('App\Models\DistributionDetail');
    }
}