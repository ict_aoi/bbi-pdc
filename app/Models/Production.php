<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at'];
    protected $fillable     = ['factory_id'
        ,'line_id'
        ,'poreference'
        ,'article'
        ,'style'
        ,'total_qty'
        ,'created_at'
        ,'updated_at'
        ,'delete_at'
        ,'created_user_by_nik'
        ,'created_user_by_name'
        ,'updated_user_by_nik'
        ,'updated_user_by_name'
        ,'deleted_user_by_nik'
        ,'deleted_user_by_name'
        ,'delete_user_id'
        ,'is_show_qc'
        ,'is_show_folding'
        ,'kode_product'
        ,'history_line_id'
];

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }

    public function line()
    {
        return $this->belongsTo('App\Models\Line');
    }

    public function productionSize()
    {
        return $this->hasMany('App\Models\ProductionSize');
    }
    static public function getProductionActive($factory_id,$line_id,$poreference,$style,$article)
    {
        $data = Production::whereNull('delete_at')
        ->where([
            ['factory_id',$factory_id],
            ['line_id',$line_id],
            ['poreference',$poreference],
            ['style',$style],
            ['article',$article],
        ])
        ->first();
        return $data;
    }
	
}