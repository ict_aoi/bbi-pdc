<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HangtagOutput extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['production_id','production_size_id','counter','counter_day','counter_day_at','update_at','delete_at'];
	protected $dates     = ['created_at'];
}
