<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class QcEndlineDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	public $timestamps = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['qc_endline_id','counter','ip_address','update_at','delete_at','created_by_name','created_by_nik','created_at','updated_at'];
	protected $dates     = ['created_at'];

	static function getCounterQcDetail($style,$line_id,$date,$article)
	{
		$data            = DB::select(db::raw("
							select count(*) as total_counter from qc_endline_details where qc_endline_id in(select qev.id from qc_endlines qev
							LEFT JOIN productions pr on pr.id=qev.production_id
							LEFT JOIN styles st ON st.erp = pr.style
							where st.default='$style' and pr.article='$article'
							and pr.line_id='$line_id')
							and date(created_at)<='$date'
						"));
		return $data;
	}
}