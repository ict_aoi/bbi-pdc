<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Models\FoldingPackageDetailSize;

class FoldingPackageDetailSize extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['folding_package_id','style','poreference','article','size','inner_pack','delete_at'];
	protected $dates     = ['created_at'];

	static function dataPackageDetailSize($id)
	{
		$data = FoldingPackageDetailSize::select(
			'folding_packages.item_qty',
			'folding_package_detail_sizes.inner_pack',
			db::raw('folding_packages.counter as qty_package'),
			db::raw('COALESCE(folding_package_details.qty_detail,0) as qty_inner')
		)
		->leftJoin('folding_packages','folding_packages.id','folding_package_detail_sizes.folding_package_id')
		->leftjoin(
			DB::raw('(select folding_package_id,folding_package_detail_size_id,count(*) as qty_detail from folding_package_details
			where folding_package_detail_size_id=' . "'$id'" . '
			GROUP BY folding_package_id,folding_package_detail_size_id)
			folding_package_details'),
			function ($leftjoin) {
				$leftjoin->on('folding_packages.id', '=', 'folding_package_details.folding_package_id');
				$leftjoin->on('folding_package_detail_sizes.id', '=', 'folding_package_details.folding_package_detail_size_id');
			}
		)
		->where('folding_package_detail_sizes.id',$id)
		->first();
		return $data;
	}
}