<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class WorkflowDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['workflow_id','process_sewing_id','last_process','update_at','delete_at','create_user_id','update_user_id','delete_user_id','critical_process','smv','machine_id'];
	protected $dates     = ['created_at'];

	public function processSewing()
	{
		return $this->belongsTo('App\Models\ProcessSewing');
	}
	public function machine()
	{
		return $this->belongsTo('App\Models\Machine');
	}
	public function workflow()
	{
		return $this->belongsTo('App\Models\Workflow');
	}
}