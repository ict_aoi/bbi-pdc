<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BarcodeGarment extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['style','poreference','article','size','barcode_id','delete_at'];
	protected $dates     = ['created_at'];
}
