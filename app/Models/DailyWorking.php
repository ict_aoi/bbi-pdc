<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DailyWorking extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['line_id','start','end','working_hours','update_at','delete_at','create_user_id','update_user_id','delete_user_id','recess'];
	protected $dates     = ['created_at'];
}