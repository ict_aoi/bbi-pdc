<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class QcEndlineDefect extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['qc_endline_detail_id','qc_endline_id','defect_id','update_defect_date','update_at','delete_at'];
	protected $dates     = ['created_at'];

	public function defect()
    {
        return $this->belongsTo('App\Models\Defect');
    }
}
