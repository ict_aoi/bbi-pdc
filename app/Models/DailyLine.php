<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DailyLine extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['line_id','style','change_over','smv','cumulative_day','job_status','update_at','delete_at'];
	protected $dates     = ['created_at'];
}
