<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{
    // use Uuids;
	public $incrementing = true;
	protected $guarded   = ['id'];
	protected $fillable  = ['name','grade_defect_id'];
	protected $dates     = ['created_at'];
}
