<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    use Uuids;
	public $timestamps 		= false;
	public $incrementing 	= false;
	protected $guarded   	= ['id'];
	protected $fillable  	= ['erp','edit','default','created_at','updated_at'];
	protected $dates     	= ['created_at'];
}
