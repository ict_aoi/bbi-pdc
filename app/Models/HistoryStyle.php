<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryStyle extends Model
{
    use Uuids;
	public $timestamps 		= false;
	public $incrementing 	= false;
	protected $guarded   	= ['id'];
	protected $fillable  	= ['style_id','change','created_by_name','edit_user_id'];
	protected $dates     	= ['created_at'];
}
