<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BreakDownSize extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded          = ['id'];
    protected $table            = 'breakdown_size';
    protected $fillable         = ['id',
        'documentno',
        'poreference',
        'm_product_id',
        'kode_product',
        'nama_product',
        'description',
        'style',
        'size',
        'article',
        'color',
        'kst_season',
        'category',
        'dateorder',
        'datepromise',
        'buyer',
        'c_order_id',
        'c_orderline_id',
        'qtyordered',
        'no_order_nagai',
        'job_order',
        'article_so',
        'date_finish_schedule',
        'style_so'
    ];
    static function getBuyer($style,$poreference,$article,$size)
    {
        $data = BreakDownSize::where([
            ['style',$style],
            ['poreference',$poreference],
            ['article',$article],
            ['size',$size],
        ])->first();
        return ($data)?$data->buyer:null;
    }
}