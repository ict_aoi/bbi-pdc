<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class CycleWorkflowDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['cycle_time_id','workflow_detail_id','delete_at'];
	protected $dates     = ['created_at'];
}