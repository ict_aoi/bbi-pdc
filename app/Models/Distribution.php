<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    use Uuids;
	public $timestamp       = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['factory_id'
        ,'line_id'
        ,'production_id'
        ,'production_size_id'
        ,'poreference'
        ,'style'
        ,'article'
        ,'size'
        ,'season'
        ,'qty'
        ,'cut_num'
        ,'start_no'
        ,'end_no'
        ,'is_completed'
        ,'is_movement'
        ,'complete_date'
        ,'date_completed'
        ,'deleted_at'
        ,'date_completed'
        ,'date_movement'
        ,'sources'
    ];
    public function scopeActive($query)
	{
		return $query->whereNull('deleted_at');
	}
    public function scopeMovement($query)
	{
		return $query->whereNull('date_movement');
	}
    public function line()
    {
        return $this->belongsTo('App\Models\Line');
    }
}