<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','style','article','smv','update_at','delete_at','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];
}
