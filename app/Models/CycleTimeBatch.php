<?php namespace App\Models;

use App\Uuids;
use App\Models\Workflow;
use Illuminate\Database\Eloquent\Model;

class CycleTimeBatch extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','line_id','style','start_job','end_job','day','update_at','delete_at','create_user_id'];
	protected $dates     = ['created_at'];

    public function line()
	{
		return $this->belongsTo('App\Models\Line');
	}
    public function factory()
	{
		return $this->belongsTo('App\Models\Factory');
	}
    public function user()
	{
		return $this->belongsTo('App\Models\User','create_user_id');
	}
	static function getWorkflow($style)
	{
		$data = Workflow::whereNull('delete_at')
		->where('style',$style)
		->first();
		return $data;
	}
}