<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DailyLineDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['daily_line_id','day','present_sewer','present_ipf','present_folding','commitment_line','start','end','working_hours','daily_working_id','absent_sewer','create_user_id','update_user_id','delete_user_id'];
	protected $dates     = ['created_at'];

	public function dailyLine()
    {
        return $this->belongsTo('App\Models\DailyLine');
    }
}