<?php namespace App\Models;

use DB;
use App\Uuids;
use App\Models\WorkingHours;
use Illuminate\Database\Eloquent\Model;

class WorkingHours extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['name','start','end'];
	protected $dates     = ['created_at'];


	static function getWorkingHours()
	{
		$workingHours = WorkingHours::select(db::raw("date_part('hour', start) AS hours"))
        ->where('start','<','20:00:00')
        ->orderBy('start')
		->get();
		return $workingHours;
	}
}
