<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ResponseWorkflow extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['line_id','workflow_id','nomor','style','message','create_qc_nik','create_qc_name','status','update_at','delete_at'];
	protected $dates     = ['created_at'];

	public function line()
	{
		return $this->belongsTo('App\Models\Line');
	}
	
}