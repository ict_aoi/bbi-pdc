<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Sewing extends Model
{
    use Uuids;
	public $incrementing = false;
	public $timestamps = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id','line_id','style','article','counter','counter_day','status','created_at','updated_at'];
	protected $dates     = ['created_at'];
}
