<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HangtagOutputDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	public $timestamps = true;
	protected $guarded   = ['id'];
	protected $fillable  = ['hangtag_output_id','counter','ip_address','created_by_name','created_by_nik','created_at'];
	protected $dates     = ['created_at'];
}
