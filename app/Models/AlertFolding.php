<?php namespace App\Models;

use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AlertFolding extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['production_id','production_size_id','qc_output','create_user_name','create_user_nik'];
	protected $dates     = ['created_at'];
}