<?php namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Scheduler;
use Illuminate\Console\Command;

use App\Http\Controllers\ReportWftController;
use App\Http\Controllers\ReportNagaiController;
use App\Http\Controllers\ReportQcEndlineOutput;

class SyncReportNagai extends Command
{
    protected $signature = 'sync:syncReportNagai';
    protected $description = 'Sinkronisasi Report Nagai';
    
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $schedulerReportNagai = Scheduler::where('job','SYNC_REPORT_NAGAI')
        ->where('status','ongoing')
        ->first();

        // dd($schedulerProductionHourly."-".$schedulerProductionDaily);
        if(!empty($schedulerReportNagai))
        {
            $this->info('SYNC REPORT NAGAI JOB AT '.carbon::now());
            $this->setStatus($schedulerReportNagai,'ongoing');
            $this->setStartJob($schedulerReportNagai);
            ReportNagaiController::exportFormMont();
            // ReportQcEndlineOutput::downloadAll();
            ReportQcEndlineOutput::exportNagai();
            ReportQcEndlineOutput::exportOther();
            ReportWftController::exportWeek();
            $this->setStatus($schedulerReportNagai,'done');
            $this->setEndJob($schedulerReportNagai);
            $this->info('SYNC REPORT NAGAI JOB AT '.carbon::now());
        }else{
            $schedulerReportNagai = Scheduler::where('job','SYNC_REPORT_NAGAI')
            ->where('status','ongoing')
            ->exists();
            
            if(!$schedulerReportNagai){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_REPORT_NAGAI',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC REPORT NAGAI JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ReportNagaiController::exportFormMont();
                ReportQcEndlineOutput::exportAll();
                ReportQcEndlineOutput::exportNagai();
                ReportQcEndlineOutput::exportOther();
                ReportWftController::exportWeek();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC REPORT NAGAI JOB AT '.carbon::now());
            }else{
                $this->info('SYNC SYNC REPORT NAGAI SEDANG BERJALAN');
            }
        }
    }
    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
