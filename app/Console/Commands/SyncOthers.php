<?php namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Scheduler;
use Illuminate\Console\Command;

use App\Http\Controllers\WorkFlowController;
use App\Http\Controllers\BreakdownSizeController;

class SyncOthers extends Command
{
    protected $signature = 'sync:syncOther';
    protected $description = 'Sinkronisasi Other';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $schedulerOther = Scheduler::where('job','SYNC_OTHER')
        ->where('status','queue')
        ->first();

        $schedulerOther = Scheduler::where('job','SYNC_OTHER')
        ->where('status','queue')
        ->first();

        // dd($schedulerOther."-".$schedulerOther);
        if(!empty($schedulerOther))
        {
            $this->info('SYNC OTHER JOB AT '.carbon::now());
            $this->setStatus($schedulerOther,'ongoing');
            $this->setStartJob($schedulerOther);
            WorkFlowController::updateQcInline();
            // BreakdownSizeController::InsertBreakdownSize();
            // BreakdownSizeController::InsertBreakdownSize_jz();
            
            $this->setStatus($schedulerOther,'done');
            $this->setEndJob($schedulerOther);
            $this->info('SYNC OTHER JOB AT '.carbon::now());
        }else if(!empty($schedulerOther))
        {
            $this->info('SYNC OTHER JOB AT '.carbon::now());
            $this->setStatus($schedulerOther,'ongoing');
            $this->setStartJob($schedulerOther);
            // BreakdownSizeController::InsertBreakdownSize();
            // BreakdownSizeController::InsertBreakdownSize_jz();
            WorkFlowController::updateQcInline();
            $this->setStatus($schedulerOther,'done');
            $this->setEndJob($schedulerOther);
            $this->info('SYNC OTHER JOB AT '.carbon::now());
        }else{
            $schedulerOther = Scheduler::where('job','SYNC_OTHER')
            ->where('status','ongoing')
            ->exists();
            $schedulerOther = Scheduler::where('job','SYNC_OTHER')
            ->where('status','ongoing')
            ->exists();
            if(!$schedulerOther&&!$schedulerOther){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_OTHER',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC OTHER JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                // BreakdownSizeController::InsertBreakdownSize();
                // BreakdownSizeController::InsertBreakdownSize_jz();
                WorkFlowController::updateQcInline();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC OTHER JOB AT '.carbon::now());
            }else{
                $this->info('SYNC SYNC OTHER SEDANG BERJALAN');
            }
        }
    }
    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}