<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\SummaryProductionController;

use App\Models\Scheduler;

class SyncProductionDaily extends Command
{
    protected $signature = 'sync:syncProductionDaily';
    protected $description = 'Sinkronisasi Production Hari';
    
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $schedulerProductionHourly = Scheduler::where('job','SYNC_PRODUCTION_HOURLY')
        ->where('status','ongoing')
        ->first();
        // dd(!$schedulerProductionHourly);
        $schedulerProductionDaily = Scheduler::where('job','SYNC_PRODUCTION_DAILY')
        ->where('status','queue')
        ->first();

        // dd($schedulerProductionHourly."-".$schedulerProductionDaily);
        if(!empty($schedulerProductionDaily)&&!empty($schedulerProductionHourly))
        {
            $this->info('SYNC PRODUCTION DAILY JOB AT '.carbon::now());
            $this->setStatus($schedulerProductionDaily,'ongoing');
            $this->setStartJob($schedulerProductionDaily);
            SummaryProductionController::CronInserProductionDaily();
            $this->setStatus($schedulerProductionDaily,'done');
            $this->setEndJob($schedulerProductionDaily);
            $this->info('SYNC PRODUCTION DAILY JOB AT '.carbon::now());
        }else{
            $schedulerProductionDaily = Scheduler::where('job','SYNC_PRODUCTION_DAILY')
            ->where('status','ongoing')
            ->exists();
            $schedulerProductionHourly = Scheduler::where('job','SYNC_PRODUCTION_HOURLY')
            ->where('status','ongoing')
            ->exists();
            if(!$schedulerProductionDaily&&!$schedulerProductionHourly){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_PRODUCTION_DAILY',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC PRODUCTION DAILY JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                SummaryProductionController::CronInserProductionDAILY();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC PRODUCTION DAILY JOB AT '.carbon::now());
            }else{
                $this->info('SYNC SYNC PRODUCTION DAILY SEDANG BERJALAN');
            }
        }
    }
    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
