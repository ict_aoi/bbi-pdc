<?php namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Scheduler;
use Illuminate\Console\Command;


use App\Http\Controllers\SummaryProductionController;

class SyncProduction extends Command
{
    protected $signature = 'sync:syncProduction';
    protected $description = 'Sinkronisasi Production';
    
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $schedulerProductionHourly = Scheduler::where('job','SYNC_PRODUCTION_HOURLY')
        ->where('status','queue')
        ->first();

        $schedulerProductionDaily = Scheduler::where('job','SYNC_PRODUCTION_DAILY')
        ->where('status','queue')
        ->first();

        // dd($schedulerProductionHourly."-".$schedulerProductionDaily);
        if(!empty($schedulerProductionHourly))
        {
            $this->info('SYNC PRODUCTION HOURLY JOB AT '.carbon::now());
            $this->setStatus($schedulerProductionHourly,'ongoing');
            $this->setStartJob($schedulerProductionHourly);
            SummaryProductionController::CronInserProductionHourly();
            
            $this->setStatus($schedulerProductionHourly,'done');
            $this->setEndJob($schedulerProductionHourly);
            $this->info('SYNC PRODUCTION HOURLY JOB AT '.carbon::now());
        }else if(!empty($schedulerProductionDaily))
        {
            $this->info('SYNC PRODUCTION DAILY JOB AT '.carbon::now());
            $this->setStatus($schedulerProductionDaily,'ongoing');
            $this->setStartJob($schedulerProductionDaily);
            SummaryProductionController::CronInserProductionDaily();
            $this->setStatus($schedulerProductionDaily,'done');
            $this->setEndJob($schedulerProductionDaily);
            $this->info('SYNC PRODUCTION DAILY JOB AT '.carbon::now());
        }else{
            $schedulerProductionHourly = Scheduler::where('job','SYNC_PRODUCTION_HOURLY')
            ->where('status','ongoing')
            ->exists();
            $schedulerProductionDaily = Scheduler::where('job','SYNC_PRODUCTION_DAILY')
            ->where('status','ongoing')
            ->exists();
            if(!$schedulerProductionHourly&&!$schedulerProductionDaily){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_PRODUCTION_HOURLY',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC PRODUCTION HOURLY JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                SummaryProductionController::CronInserProductionHourly();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC PRODUCTION HOURLY JOB AT '.carbon::now());
            }else{
                $this->info('SYNC SYNC PRODUCTION HOURLY SEDANG BERJALAN');
            }
        }
    }
    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}