<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\SummaryProductionController;

use App\Models\Scheduler;
use App\Models\DailyWorking;



class SyncAllertProduction extends Command
{
    protected $signature = 'sync:syncAllertProduction';
    protected $description = 'Sinkronisasi Allert Production';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $now          = Carbon::now();
        $time         = Carbon::createFromFormat('Y-m-d H:i:s',  $now)->format('H:i:s');
        $date         = Carbon::createFromFormat('Y-m-d H:i:s',  $now)->format('Y-m-d');
        $startDate    = "07:00:00";
        $dailyWorking = DailyWorking::whereNull('delete_at')
        ->where(db::raw('date(created_at)'),$date)
        ->orderBy('end','desc')
        ->first();
        $endDate = ($dailyWorking)?$dailyWorking->end:'16:00:00';
        
        if ($time > $startDate && $time < $endDate) {
            $schedulerAllert = Scheduler::where('job','SYNC_ALLERT_PRODUCTION')
            ->where('status','ongoing')
            ->first();

            if(!$schedulerAllert)
            {
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_ALLERT_PRODUCTION',
                    'status' => 'ongoing'
                ]);
                
                $this->info('SYNC ALLERT JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                SummaryProductionController::sendAllertProduction();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC ALLERT PRODUCTION JOB AT '.carbon::now());
            }else{
                $this->info('SYNC ALLERT PRODUCTION SEDANG BERJALAN');
            }
        }
        
    }
    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}