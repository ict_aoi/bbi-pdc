<?php

namespace App\Console\Commands;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Style;
use App\Models\BreakDownSize;

class IncaseEvent extends Command
{
    protected $signature = 'IncaseEvent:update';
    protected $description = 'Update row based on case';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $running_at = carbon::now();
        $this->info('Start incase event at '.$running_at);
        $this->insertMasterStyle($running_at);
        $this->info('End incase event at '.$running_at);
    }

    static function insertMasterStyle($inserted_at)
    {
        $data = BreakDownSize::select('style')
        ->whereNotNull('style')
        ->where('style','!=','')
        ->groupby('style')
        ->get();

        foreach ($data as $key => $value) 
        {
            $is_style_exists = Style::where('erp',$value->style)->exists();
            if(!$is_style_exists)
            {
                Style::create([
                    'erp'           => $value->style,
                    'edit'          => $value->style,
                    'default'       => $value->style,
                    'created_at'    => $inserted_at,
                    'updated_at'    => $inserted_at,
                ]);
            }
        }
    }
}
