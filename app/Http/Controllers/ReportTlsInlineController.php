<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Defect;
use App\Models\QcInline;
use App\Models\QcEndlineDefect;
use App\Models\SummaryProduction;


class ReportTlsInlineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $data = Defect::where('id','<>','0')
        ->whereBetween('id',['1','30'])
        ->orderBy('id','asc')
        ->get();
        return view('report_tls_inline.index',compact('data'));
    }
    public function data(Request $request)
    {
        list($start,$end) = explode("-",$request->date);
        $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $factory_id  = Auth::user()->factory_id;
        $data = DB::select(db::Raw("select code,date,line_name,sum(total_checked) as total_checked,sum(total_good) as total_good,sum(total_defect) as total_defect,(round(sum(total_good)/sum(total_checked),2))*100 as rft,sum(defect1) as defect1,
        sum(defect2) as defect2,sum(defect3) as defect3,sum(defect4) as defect4,sum(defect5) as defect5,sum(defect6) as defect6,
        sum(defect7) as defect7,sum(defect8) as defect8,sum(defect9) as defect9,sum(defect10) as defect10,sum(defect11) as defect11,sum(defect12) as defect12,sum(defect13) as defect13,
        sum(defect14) as defect14,sum(defect15) as defect15,sum(defect16) as defect16,sum(defect17) as defect17,sum(defect18) as defect18,
        sum(defect19) as defect19,sum(defect20) as defect20,sum(defect21) as defect21,sum(defect22) as defect22,
        sum(defect23) as defect23,sum(defect24) as defect24,sum(defect25) as defect25,sum(defect26) as defect26,sum(defect27) as defect27,sum(defect28) as defect28,
        sum(defect29) as defect29,sum(defect30) as defect30 from f_tls_inline('$factory_id','$startDate','$endDate') GROUP BY
        date,line_name,code"));
        return datatables()->of($data)
        ->make(true);
    }
    public function exportFromRft(Request $request)
    {
        list($start,$end) = explode("-",$request->date);
        $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $factory_id = Auth::user()->factory_id;
        return Excel::create('TLS  -'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($startDate,$endDate,$factory_id){
                $excel->sheet('RFT INLINE', function($sheet) use($startDate,$endDate,$factory_id){
                    $array = array();
                    for ($i=1; $i <= 5; $i++) 
                    { 
                        array_push($array, 'R'.$i.' date', 'R'.$i.' line','R'.$i.' week','R'.$i.' random');
                        for ($d=1; $d <=30 ; $d++) 
                        { 
                            array_push($array, 'R'.$i.' defect '.$d);
                        }
                        
                    }
                    dd($array);
                    foreach ($array as $key1 => $a) {
                        
                        $alphabets = $this->Alphabets();
                        
                        $sheet->setCellValue($alphabets[$key1].'1',$a);
                    }
                    $sheet->setColumnFormat(array(
                        'D' => '@',
                        'F' => '@',
                        'H' => '0',
                        'I' => '0',
                        'F' => '0',
                    ));             
                });
                $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    private function week_of_today($date)
    {
        $today = $date;
        $month = Carbon::createFromFormat('Y-m-d',$date)->format('m');
        $month = str_pad($month,2,'0',STR_PAD_LEFT);

        $minggu = 0;
        $week_end = 0;

        $last_date =  $this->last_date_ofthe_month();

        for($i = 1; $i<=$last_date; $i++)
        {
            $i = str_pad($i,2,'0',STR_PAD_LEFT);
            $date =  date("Y-{$month}-{$i}");
            $day  =  date('D', strtotime($date));

            if($day == 'Sat')
            {
            $minggu = $minggu + 1;
            }
            if($date == $today)
            {
            $minggu = $minggu + 1;
            break;

            }
        }
        return $minggu;
    }
    private function last_date_ofthe_month($month='', $year='')
    {
        if(!$year)   $year   = date('Y');
        if(!$month)  $month  = date('m');
        $date = $year.'-'.$month.'-01';

        $next_month = strtotime('+ 1 month', strtotime($date));

        $last_date  = date('d', strtotime('-1 minutes',  $next_month));
        return $last_date;

    }
    private function mayorDefectSumStyle($date,$line_id,$style)
    {
        $data = QcEndlineDefect::select('productions.line_id','productions.style','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'),db::raw('count(*)as total_defect'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('defects','defects.id','qc_endline_defects.defect_id')
        ->where(db::raw('date(qc_endline_defects.created_at)'),$date)
        ->where('line_id',$line_id)
        ->where('productions.style',$style)
        ->groupBy('productions.line_id','productions.style','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'))
        ->orderBy(db::raw('count(*)'),'DESC')
        ->orderBy('qc_endline_defects.defect_id','ASC')
        ->first();
        return ($data?$data->name:"-");
    }
    private function getDataInline($factory_id,$startDate,$endDate,$round){
        $data = DB::select(db::Raw("select code,date,line_name,sum(total_checked) as total_checked,sum(total_good) as total_good,sum(total_defect) as total_defect,(round(sum(total_good)/sum(total_checked),2))*100 as rft,sum(defect1) as defect1,
        sum(defect2) as defect2,sum(defect3) as defect3,sum(defect4) as defect4,sum(defect5) as defect5,sum(defect6) as defect6,
        sum(defect7) as defect7,sum(defect8) as defect8,sum(defect9) as defect9,sum(defect10) as defect10,sum(defect11) as defect11,sum(defect12) as defect12,sum(defect13) as defect13,
        sum(defect14) as defect14,sum(defect15) as defect15,sum(defect16) as defect16,sum(defect17) as defect17,sum(defect18) as defect18,
        sum(defect19) as defect19,sum(defect20) as defect20,sum(defect21) as defect21,sum(defect22) as defect22,
        sum(defect23) as defect23,sum(defect24) as defect24,sum(defect25) as defect25,sum(defect26) as defect26,sum(defect27) as defect27,sum(defect28) as defect28,
        sum(defect29) as defect29,sum(defect30) as defect30 from f_tls_inline('$factory_id','$startDate','$endDate') GROUP BY
        date,line_name,code"));
        return $data;
    }
    private function Alphabets(Type $var = null)
    {
        $alphabets = array(
            0 => 'A',
            1 => 'B',
            2 => 'C',
            3 => 'D',
            4 => 'E',
            5 => 'F',
            6 => 'G',
            7 => 'H',
            8 => 'I',
            9 => 'J',
            10 => 'K',
            11 => 'L',
            12 => 'M',
            13 => 'N',
            14 => 'O',
            15 => 'P',
            16 => 'Q',
            17 => 'R',
            18 => 'S',
            19 => 'T',
            20 => 'U',
            21 => 'V',
            22 => 'W',
            23 => 'X',
            24 => 'Y',
            25 => 'Z',
            26 => 'AA',
            27 => 'AB',
            28 => 'AC',
            29 => 'AD',
            30 => 'AE',
            31 => 'AF',
            32 => 'AG',
            33 => 'AH',
            34 => 'AI',
            35 => 'AJ',
            36 => 'AK',
            37 => 'AL',
            38 => 'AM',
            39 => 'AN',
            40 => 'AO',
            41 => 'AP',
            42 => 'AQ',
            43 => 'AR',
            44 => 'AS',
            45 => 'AT',
            46 => 'AU',
            47 => 'AV',
            48 => 'AW',
            49 => 'AX',
            50 => 'AY',
            51 => 'AZ',
            52 => 'BA',
            53 => 'BB',
            54 => 'BC',
            55 => 'BD',
            56 => 'BE',
            57 => 'BF',
            58 => 'BG',
            59 => 'BH',
            60 => 'BI',
            61 => 'BJ',
            62 => 'BK',
            63 => 'BL',
            64 => 'BM',
            65 => 'BN',
            66 => 'BO',
            67 => 'BP',
            68 => 'BQ',
            69 => 'BR',
            70 => 'BS',
            71 => 'BT',
            72 => 'BU',
            73 => 'BV',
            74 => 'BW',
            75 => 'BX',
            76 => 'BY',
            77 => 'BZ',
        );
        return $alphabets;
    }
}
