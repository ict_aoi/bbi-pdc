<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\DailyWorking;
use App\Models\DailyLineView;
use App\Models\SummaryProduction;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function efficiency(Request $request)
    {
        return view('dashboard.efficiency');
    }
    public function outputQc(Request $request)
    {
        return view('dashboard.output_qc');
    }
    public function getEfficiency(Request $request)
    {
        if ($request->ajax()) {
            $now            = Carbon::now();
            $hours          = $now->format('H:i:s');
            $dateNow        = $now->format('Y-m-d');
            $date           = ($request->date?Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d'):$now->format('Y-m-d'));
            $line_nagai     = array();
            $targetEffNagai = array();
            $actualEffNagai = array();
            $line_other     = array();
            $targetEffOther = array();
            $actualEffOther = array();
            $data_other     = array();
            $data_nagai     = array();

            
            $dailyLine = DailyLineView::select('line_id','name','buyer','urut')
            ->distinct()
            ->whereNull('delete_at')
            ->where(db::raw('date(created_at)'),$date)
            ->orderBy('urut','asc');
            
            foreach ($dailyLine->get() as $key => $daily) {
                $workingHours = DailyWorking::where('line_id',$daily->line_id)
                ->where(db::raw('date(created_at)'),$date)->first();
                $commitment_line = 0;
                $targetOutput    = 0;
                $actualOutput    = 0;
                $line_name       = '';
                if ($workingHours->start<=$hours) {
                    $dailyFilters = DailyLineView::whereNull('delete_at')
                    ->where(db::raw('date(created_at)'),$date)
                    ->where('line_id',$daily->line_id)
                    ->orderBy('urut','asc');
                    
                    foreach ($dailyFilters->get() as $key => $filter) {
                        if($filter->buyer=='nagai'){
                            $finish           = Carbon::parse($hours);
                            $jam = (($finish->diffInHours($workingHours->start)+1)>5?($finish->diffInHours($workingHours->start)+1)-1:($finish->diffInHours($workingHours->start)+1));
                            $hours_to         = ($date==$dateNow)?(($jam<$workingHours->working_hours)?$jam:$workingHours->working_hours):$workingHours->working_hours;
                            $line_name        = $daily->name;
                            $present_sewer    = $filter->present_sewer;
                            $working_hours    = $filter->working_hours;
                            $outputQc         = $this->getSumProductionall($filter->style,$filter->created_at,'output_qc_endline',$workingHours->start,$filter->line_id);
                            $actualOutput +=$outputQc*$filter->smv;
                            $commitment_line += $filter->commitment_line;
                            $targetOutput    += $filter->commitment_line*$filter->smv;
                        }
                        if($daily->buyer=='other'){
                            $finish           = Carbon::parse($hours);
                            $jam              = (($finish->diffInHours($workingHours->start)+1)>5?($finish->diffInHours($workingHours->start)+1)-1:($finish->diffInHours($workingHours->start)+1));
                            $hours_to         = ($date==$dateNow)?(($jam<$workingHours->working_hours)?$jam:$workingHours->working_hours):$workingHours->working_hours;
                            $line_name        = $daily->name;
                            $present_sewer    = $filter->present_sewer;
                            $working_hours    = $filter->working_hours;
                            $outputQc         = $this->getSumProductionall($filter->style,$filter->created_at,'output_qc_endline',$workingHours->start,$filter->line_id);
                            $actualOutput    += $outputQc*$filter->smv;
                            $commitment_line += $filter->commitment_line;
                            $targetOutput    += $filter->commitment_line*$filter->smv;
                        }
                    }
                }
                if($daily->buyer=='nagai'){
                    $nagai                  = new StdClass();
                    $nagai->line_name       = ucwords($line_name);
                    $nagai->present_sewer   = $present_sewer;
                    $nagai->working_hours   = $working_hours;
                    $nagai->commitment_line = $commitment_line;
                    $nagai->targetOutput    = $targetOutput;
                    $nagai->hours_to    = $hours_to;
                    $nagai->outputQc    = $outputQc;
                    $nagai->actualOutput    = $actualOutput;
                    $data_nagai[]  = $nagai;
                }
               
               
                if($daily->buyer=='other'){
                    $other                  = new StdClass();
                    $other->line_name       = ucwords($line_name);
                    $other->present_sewer   = $present_sewer;
                    $other->working_hours   = $working_hours;
                    $other->commitment_line = $commitment_line;
                    $other->targetOutput    = $targetOutput;
                    $other->hours_to        = $hours_to;
                    $other->outputQc        = $outputQc;
                    $other->actualOutput    = $actualOutput;
                    $data_other[]                       = $other;
                }
            }
            foreach ($data_nagai as $data) {
                $targetEff                     = (($data->targetOutput)/($data->present_sewer*$data->working_hours*60))*100;
                $actualEff                     = ($data->actualOutput>0?(($data->actualOutput)/($data->present_sewer*$data->hours_to*60))*100:0);
                array_push($line_nagai,$data->line_name);
                array_push($targetEffNagai,round($targetEff));
                array_push($actualEffNagai,round($actualEff));
            }
            foreach ($data_other as $data) {
                $targetEff                     = (($data->targetOutput)/($data->present_sewer*$data->working_hours*60))*100;
                $actualEff                     = ($data->actualOutput>0?(($data->actualOutput)/($data->present_sewer*$data->hours_to*60))*100:0);
                array_push($line_other,$data->line_name);
                array_push($targetEffOther,round($targetEff));
                array_push($actualEffOther,round($actualEff));
            }
            return response()->json([
                'line_nagai' => $line_nagai,
                'line_other' => $line_other,
                'target_eff_nagai' => $targetEffNagai,
                'actual_eff_nagai' => $actualEffNagai,
                'target_eff_other' => $targetEffOther,
                'actual_eff_other' => $actualEffOther,
            ], 200);
        }
    }
    public function getOutputQc(Request $request)
    {
        if ($request->ajax()) {
            $now            = Carbon::now();
            $hours          = $now->format('H:i:s');
            $dateNow        = $now->format('Y-m-d');
            $date           = ($request->date?Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d'):$now->format('Y-m-d'));
            $line_nagai     = array();
            $targetOutputNagai = array();
            $actualOutputNagai = array();
            $line_other     = array();
            $targetOutputOther = array();
            $actualOutputOther = array();
            $data_other     = array();
            $data_nagai     = array();

            
            $dailyLine = DailyLineView::select('line_id','name','buyer','urut')
            ->distinct()
            ->whereNull('delete_at')
            ->where(db::raw('date(created_at)'),$date)
            ->orderBy('urut','asc');
            
            foreach ($dailyLine->get() as $key => $daily) {
                $workingHours = DailyWorking::where('line_id',$daily->line_id)
                ->where(db::raw('date(created_at)'),$date)->first();
                $commitment_line = 0;
                $targetOutput    = 0;
                $actualOutput    = 0;
                $line_name       = '';
                if ($workingHours->start<=$hours) {
                    $dailyFilters = DailyLineView::whereNull('delete_at')
                    ->where(db::raw('date(created_at)'),$date)
                    ->where('line_id',$daily->line_id)
                    ->orderBy('urut','asc');
                    
                    foreach ($dailyFilters->get() as $key => $filter) {
                        if($filter->buyer=='nagai'){
                            $working_hours    = $workingHours->working_hours;
                            $line_name        = $daily->name;
                            $outputQc         = $this->getSumProductionall($filter->style,$filter->created_at,'output_qc_endline',$workingHours->start,$filter->line_id);
                            $actualOutput    += $outputQc;
                            $commitment_line += $filter->commitment_line;
                        }
                        if($daily->buyer=='other'){
                            $working_hours    = $workingHours->working_hours;
                            $line_name        = $daily->name;
                            $outputQc         = $this->getSumProductionall($filter->style,$filter->created_at,'output_qc_endline',$workingHours->start,$filter->line_id);
                            $actualOutput    += $outputQc;
                            $commitment_line += $filter->commitment_line;
                        }
                    }
                }
                if($daily->buyer=='nagai'){
                    $nagai                  = new StdClass();
                    $nagai->line_name       = ucwords($line_name);
                    $nagai->actual_output   = $actualOutput;
                    $nagai->working_hours   = $working_hours;
                    $nagai->commitment_line = $commitment_line;
                    $data_nagai[]                       = $nagai;
                }
               
                
                if($daily->buyer=='other'){
                    $other                  = new StdClass();
                    $other->line_name       = ucwords($line_name);
                    $other->actual_output   = $actualOutput;
                    $other->working_hours   = $working_hours;
                    $other->commitment_line = $commitment_line;
                    $data_other[]                       = $other;
                }
            }

            foreach ($data_nagai as $data) {
                $targetOutput                     = $data->commitment_line;
                $actualOutput                     = $data->actual_output;
                array_push($line_nagai,$data->line_name);
                array_push($targetOutputNagai,round($targetOutput));
                array_push($actualOutputNagai,round($actualOutput));
            }
            foreach ($data_other as $data) {
                $targetOutput                     = $data->commitment_line;
                $actualOutput                     = $data->actual_output;
                array_push($line_other,$data->line_name);
                array_push($targetOutputOther,round($targetOutput));
                array_push($actualOutputOther,round($actualOutput));
            }
            return response()->json([
                'line_nagai' => $line_nagai,
                'line_other' => $line_other,
                'target_output_nagai' => $targetOutputNagai,
                'actual_output_nagai' => $actualOutputNagai,
                'target_output_other' => $targetOutputOther,
                'actual_output_other' => $actualOutputOther,
            ], 200);
        }
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    private function getSumProductionall($style,$date,$name,$awal,$line_id)
    {
        $dataSum = SummaryProduction::where('summary_productions.name',$name)
        ->leftJoin('styles','styles.erp','summary_productions.style')
        ->where('styles.default',$style)
        ->where('summary_productions.date',Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('Y-m-d'))
        ->where('summary_productions.start_hours','>=',$awal)
        ->where('summary_productions.line_id',$line_id)
        ->sum('summary_productions.output');
        return $dataSum;
    }
}
