<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\Production;

class ReportQcEndlineOutput extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return view('report_qc_endline_output.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $poreference = $request->poreference;
            $size        = $request->size;
            $style       = null;
            $article     = null;
            $buyer       = $this->Buyer();
            if ($poreference) {
                list($poreference,$style,$article)= explode("|",$request->poreference);
            }
            if($request->date==null){
                if ($request->poreference==null) {
                    $startDate = Carbon::now()->format('Y-m-d');
                    $endDate   = Carbon::now()->format('Y-m-d');
                } else {
                    $startDate= null;
                    $endDate= null;
                }
            }else{
                list($start,$end) = explode("-",$request->date);
                $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
                $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            }
            $data        = DB::table('jurnal_output_qc_v')
            ->whereIn('buyer',$buyer)
            ->where(function($query) use($poreference,$style,$article,$size)
            {
                if ($poreference!=NULL) {
                    $query = $query->where('poreference',$poreference)
                    ->where('style',$style)
                    ->where('article',$article);
                }
                if ($size!=null) {
                    $query = $query->where('size',$size);
                }
                
            });
            if ($startDate!=null&&$endDate!=null) {
                $data->whereBetween('date',[$startDate,$endDate]);
            }
            $data->orderBy('date');
            $data->orderBy('order');
            $data->orderBy('style');
            $data->orderBy('poreference');
            $data->orderBy('article');
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('date_finish',function($data){
                return $data->date_finish_schedule;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->name);
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('job_order',function($data){
                return $data->job_order;
            })
            ->editColumn('qty_order',function($data){
                return $data->qty_order;
            })
            ->editColumn('total_output',function($data){
                return $data->total_counter;
            })
            ->editColumn('output_day',function($data){
                return $data->counter_day;
            })
            ->editColumn('balance',function($data){
                return $data->balance_per_day;
            })
            ->setRowAttr([
                'style' => function($data) 
                {
                    if ($data->qty_order==$data->total_counter) {
                        return  'background-color: #76B645';
                    } else if($data->qty_order>$data->total_counter){
                        return  'background-color: #ffffff';
                    }else if($data->qty_order<$data->total_counter){
                        return  'background-color: #FA4C4C';
                    }
                },
            ])
            ->rawColumns(['style'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    public function poBuyerPicklist(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtoupper($request->q));
            $buyer = $this->Buyer();
            $data  = Production::select('productions.factory_id','lines.buyer','productions.style','productions.poreference','productions.article')
            ->leftJoin('lines','lines.id','productions.line_id')
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->where(function($query) use ($q){
                $query->where(db::raw('upper(productions.poreference)'),'LIKE',"%$q%")
                ->orWhere(db::raw("upper(productions.article)"),'LIKE',"%$q%")
                ->orWhere(db::raw('upper(productions.style)'),'LIKE',"%$q%");
            })
            ->whereIn('lines.buyer',$buyer)
            ->groupBy('lines.buyer','productions.factory_id','productions.style','productions.poreference','productions.article')
            ->paginate(10);
            return view('report_qc_endline_output._poreference_picklist', compact('data'));
        }
    }
    static function getSize(Request $request)
    {
        if ($request->ajax()) {
            list($poreference,$style,$article)= explode("|",$request->poreference);

            $size = Production::select('production_sizes.size')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->where('productions.poreference',$poreference)
            ->where('productions.style',$style)
            ->where('productions.article',$article)
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->pluck('production_sizes.size','production_sizes.size');
            return response()->json(['size' => $size]);
        }
    }
    public function exportFormFilter(Request $request)
    {
        $poreference = $request->poreference;
        $poreference = $request->poreference;
        $size        = $request->size;
        $buyer       = $this->Buyer();
        $style       = null;
        $article     = null;
        if ($poreference) {
            list($poreference,$style,$article)= explode("|",$request->poreference);
        }
        if($request->date==null){
            if ($request->poreference==null) {
                $startDate = Carbon::now()->format('Y-m-d');
                $endDate   = Carbon::now()->format('Y-m-d');
            } else {
                $startDate= null;
                $endDate= null;
            }
        }else{
            list($start,$end) = explode("-",$request->date);
            $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        }
        
        $data        = DB::table('jurnal_output_qc_v')
        ->whereIn('buyer',$buyer)
        ->where(function($query) use($poreference,$style,$article,$size)
        {
            if ($poreference!=NULL) {
                $query = $query->where('poreference',$poreference)
                ->where('style',$style)
                ->where('article',$article);
            }
            if ($size!=null) {
                $query = $query->where('size',$size);
            }
            
        });
        if ($startDate!=null&&$endDate!=null) {
            $data->whereBetween('date',[$startDate,$endDate]);
        }
        $data->orderBy('date');
        $data->orderBy('order');
        $data->orderBy('style');
        $data->orderBy('poreference');
        $data->orderBy('article');
        return Excel::create('Jurnal Output QC Endline -'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','TANGGAL');
                $sheet->setCellValue('B1','DATE FINISH SCHEDULE');
                $sheet->setCellValue('C1','NAMA LINE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','ARTICLE');
                $sheet->setCellValue('G1','SIZE');
                $sheet->setCellValue('H1','JOB ORDER');
                $sheet->setCellValue('I1','QTY ORDER');
                $sheet->setCellValue('J1','TOTAL OUTPUT');
                $sheet->setCellValue('K1','TOTAL OUTPUT DAY');
                $sheet->setCellValue('L1','BALANCE');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, $a->date_finish_schedule);
                    $sheet->setCellValue('C'.$row, strtoupper($a->name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->poreference);
                    $sheet->setCellValue('F'.$row, $a->article); 
                    $sheet->setCellValue('G'.$row, $a->size); 
                    $sheet->setCellValue('H'.$row, $a->job_order); 
                    $sheet->setCellValue('I'.$row, $a->qty_order); 
                    $sheet->setCellValue('J'.$row, $a->total_counter);
                    $sheet->setCellValue('K'.$row, $a->counter_day);
                    $sheet->setCellValue('L'.$row, $a->balance_per_day);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    static function exportAll()
    {
        $location = Config::get('storage.jurnal');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);

        $data        = DB::table('jurnal_output_qc_v');
        return Excel::create('Jurnal Output QC Endline All',function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','TANGGAL');
                $sheet->setCellValue('B1','DATE FINISH SCHEDULE');
                $sheet->setCellValue('C1','NAMA LINE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','ARTICLE');
                $sheet->setCellValue('G1','SIZE');
                $sheet->setCellValue('H1','JOB ORDER');
                $sheet->setCellValue('I1','QTY ORDER');
                $sheet->setCellValue('J1','TOTAL OUTPUT');
                $sheet->setCellValue('K1','TOTAL OUTPUT DAY');
                $sheet->setCellValue('L1','BALANCE');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, $a->date_finish_schedule);
                    $sheet->setCellValue('C'.$row, strtoupper($a->name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->poreference);
                    $sheet->setCellValue('F'.$row, $a->article); 
                    $sheet->setCellValue('G'.$row, $a->size); 
                    $sheet->setCellValue('H'.$row, $a->job_order); 
                    $sheet->setCellValue('I'.$row, $a->qty_order); 
                    $sheet->setCellValue('J'.$row, $a->total_counter);
                    $sheet->setCellValue('K'.$row, $a->counter_day);
                    $sheet->setCellValue('L'.$row, $a->balance_per_day);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->save('xlsx', $location);
    }
    static function exportNagai()
    {
        $location = Config::get('storage.jurnal');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);

        $data        = DB::table('jurnal_output_qc_v')->where('buyer','nagai');
        return Excel::create('Jurnal Output QC Endline Nagai',function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','TANGGAL');
                $sheet->setCellValue('B1','DATE FINISH SCHEDULE');
                $sheet->setCellValue('C1','NAMA LINE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','ARTICLE');
                $sheet->setCellValue('G1','SIZE');
                $sheet->setCellValue('H1','JOB ORDER');
                $sheet->setCellValue('I1','QTY ORDER');
                $sheet->setCellValue('J1','TOTAL OUTPUT');
                $sheet->setCellValue('K1','TOTAL OUTPUT DAY');
                $sheet->setCellValue('L1','BALANCE');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, $a->date_finish_schedule);
                    $sheet->setCellValue('C'.$row, strtoupper($a->name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->poreference);
                    $sheet->setCellValue('F'.$row, $a->article); 
                    $sheet->setCellValue('G'.$row, $a->size); 
                    $sheet->setCellValue('H'.$row, $a->job_order); 
                    $sheet->setCellValue('I'.$row, $a->qty_order); 
                    $sheet->setCellValue('J'.$row, $a->total_counter);
                    $sheet->setCellValue('K'.$row, $a->counter_day);
                    $sheet->setCellValue('L'.$row, $a->balance_per_day);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->save('xlsx', $location);
    }
    static function exportOther()
    {
        $location = Config::get('storage.jurnal');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);

        $data        = DB::table('jurnal_output_qc_v')->where('buyer','other');
        return Excel::create('Jurnal Output QC Endline Other',function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','TANGGAL');
                $sheet->setCellValue('B1','DATE FINISH SCHEDULE');
                $sheet->setCellValue('C1','NAMA LINE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','ARTICLE');
                $sheet->setCellValue('G1','SIZE');
                $sheet->setCellValue('H1','JOB ORDER');
                $sheet->setCellValue('I1','QTY ORDER');
                $sheet->setCellValue('J1','TOTAL OUTPUT');
                $sheet->setCellValue('K1','TOTAL OUTPUT DAY');
                $sheet->setCellValue('L1','BALANCE');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, $a->date_finish_schedule);
                    $sheet->setCellValue('C'.$row, strtoupper($a->name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->poreference);
                    $sheet->setCellValue('F'.$row, $a->article); 
                    $sheet->setCellValue('G'.$row, $a->size); 
                    $sheet->setCellValue('H'.$row, $a->job_order); 
                    $sheet->setCellValue('I'.$row, $a->qty_order); 
                    $sheet->setCellValue('J'.$row, $a->total_counter);
                    $sheet->setCellValue('K'.$row, $a->counter_day);
                    $sheet->setCellValue('L'.$row, $a->balance_per_day);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->save('xlsx', $location);
    }
    public function downloadAll(Request $request){
        $buyer  = Auth::user()->production;
        if ($buyer =='all') {
            $filename   = "Jurnal Output QC Endline All.xlsx";
        }else if($buyer=='nagai'){
            $filename   = "Jurnal Output QC Endline Nagai.xlsx";
        }else if($buyer=='other'){
            $filename   = "Jurnal Output QC Endline Other.xlsx";
        }
        
        $file       = Config::get('storage.jurnal') . '/' . $filename;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
