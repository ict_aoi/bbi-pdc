<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\SummaryProduction;
use App\Models\ProductionSizeHistory;

class ReportHourlyWftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $workingHours = WorkingHours::getWorkingHours();
        return view('report_monitoring_hourly_wft.index',compact('workingHours'));
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);
            $factory_id = Auth::user()->factory_id;
            $grade      = $request->grade;
            $buyer      = $this->Buyer();
            $marks      = array_fill(0, sizeof($buyer), '?');
            $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            // $data      = DB::select(db::Raw("select * from f_hourly_output('$startDate','$endDate') where factory_id='$factory_id'"));
            $data = DB::select(db::raw("
            select 
                    output.date,
                    output.factory_id,
                    output.line_id,
                    output.code,
                    defect.type_buyer,
                    output.line_name,
                    output.style,
                    output.buyer,
                    output.category,
                    defect.i as defect_i,
                    output.i as output_i,
                    defect.ii as defect_ii,
                    output.ii as output_ii,
                    defect.iii as defect_iii,
                    output.iii as output_iii,
                    defect.iv as defect_iv,
                    output.iv as output_iv,
                    defect.v as defect_v,
                    output.v as output_v,
                    defect.vi as defect_vi,
                    output.vi as output_vi,
                    defect.vi as defect_vii,
                    output.vi as output_vii,
                    defect.viii as defect_viii,
                    output.viii as output_viii,
                    defect.ix as defect_ix,
                    output.ix as output_ix,
                    defect.x as defect_x,
                    output.x as output_x,
                    defect.xi as defect_xi,
                    output.xi as output_xi,
                    defect.xii as defect_xii,
                    output.xii as output_xii,
                    defect.xiii as defect_xiii,
                    output.xiii as output_xiii,
                    defect.total_defect,
	                output.total_output
                from f_hourly_output('$startDate','$endDate') output
                left join f_hourly_defect('$startDate','$endDate') defect
                on defect.style = output.style
                and defect.line_id = output.line_id
                and defect.date = output.date
                where output.production in(" . implode(',', $marks) . ")
                order by output.urut asc
            "),$buyer);
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('buyer',function($data){
                return ucwords($data->buyer);
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('category',function($data){
                return $data->category;
            })
            ->editColumn('I',function($data){
                $wft = ($data->output_i>0)?($data->defect_i/$data->output_i)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('II',function($data){
                $wft = ($data->output_ii>0)?($data->defect_ii/$data->output_ii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('III',function($data){
                $wft = ($data->output_iii>0)?($data->defect_iii/$data->output_iii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('IV',function($data){
                $wft = ($data->output_iv>0)?($data->defect_iv/$data->output_iv)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('V',function($data){
                $wft = ($data->output_v>0)?($data->defect_v/$data->output_v)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('VI',function($data){
                $wft = ($data->output_vi>0)?($data->defect_vi/$data->output_vi)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('VII',function($data){
                $wft = ($data->output_vii>0)?($data->defect_vii/$data->output_vii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('VIII',function($data){
                $wft = ($data->output_viii>0)?($data->defect_viii/$data->output_viii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('IX',function($data){
                $wft = ($data->output_ix>0)?($data->defect_ix/$data->output_ix)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('X',function($data){
                $wft = ($data->output_x>0)?($data->defect_x/$data->output_x)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('XI',function($data){
                $wft = ($data->output_xi>0)?($data->defect_xi/$data->output_xi)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('XII',function($data){
                $wft = ($data->output_xii>0)?($data->defect_xii/$data->output_xii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('XIII',function($data){
                $wft = ($data->output_xiii>0)?($data->defect_xiii/$data->output_xiii)*100:0;
                return number_format($wft,1,',',',');
            })
            ->editColumn('total_wft',function($data){
                $wft = ($data->total_output>0)?($data->total_defect/$data->total_output)*100:0;
                return number_format($wft,1,',',',');
            })
            ->make(true);
        }
    }
    public function exportFormOutputHourly(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $factory_id = Auth::user()->factory_id;
        $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $data = DB::select(db::raw("
            select 
                    output.date,
                    output.factory_id,
                    output.line_id,
                    output.code,
                    defect.type_buyer,
                    output.line_name,
                    output.style,
                    output.buyer,
                    output.category,
                    defect.i as defect_i,
                    output.i as output_i,
                    defect.ii as defect_ii,
                    output.ii as output_ii,
                    defect.iii as defect_iii,
                    output.iii as output_iii,
                    defect.iv as defect_iv,
                    output.iv as output_iv,
                    defect.v as defect_v,
                    output.v as output_v,
                    defect.vi as defect_vi,
                    output.vi as output_vi,
                    defect.vi as defect_vii,
                    output.vi as output_vii,
                    defect.viii as defect_viii,
                    output.viii as output_viii,
                    defect.ix as defect_ix,
                    output.ix as output_ix,
                    defect.x as defect_x,
                    output.x as output_x,
                    defect.xi as defect_xi,
                    output.xi as output_xi,
                    defect.xii as defect_xii,
                    output.xii as output_xii,
                    defect.xiii as defect_xiii,
                    output.xiii as output_xiii,
                    defect.total_defect,
	                output.total_output
                from f_hourly_output('$startDate','$endDate') output
                left join f_hourly_defect('$startDate','$endDate') defect
                on defect.style = output.style
                and defect.line_id = output.line_id
                and defect.date = output.date
                order by output.urut asc
            "));
        return Excel::create('Monitoring WFT Perjam-'.Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('Perjam', function($sheet) use($data){
                $sheet->setCellValue('A2','Date');
                $sheet->setCellValue('B2','Line');
                $sheet->setCellValue('C2','Style');
                $sheet->setCellValue('D2','Buyer');
                $sheet->setCellValue('E2','Cat');
                $sheet->setCellValue('F1','Jam Ke-');
                $sheet->mergeCells('F1:Q1');
                $sheet->cell('F1:Q1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('F2','7');
                $sheet->setCellValue('G2','8');
                $sheet->setCellValue('H2','9');
                $sheet->setCellValue('I2','10');
                $sheet->setCellValue('J2','11');
                $sheet->setCellValue('K2','12');
                $sheet->setCellValue('L2','13');
                $sheet->setCellValue('M2','14');
                $sheet->setCellValue('N2','15');
                $sheet->setCellValue('O2','16');
                $sheet->setCellValue('P2','17');
                $sheet->setCellValue('Q2','18');
                $sheet->setCellValue('R2','19');
                $sheet->setCellValue('S2','TOTAL WFT');
                $index    = 0;
                $array    = array();
                $row      = 3;
                $totalAll = 0;
                foreach ($data as $key => $d) {
                    $obj                  = new stdClass();
                    $obj->date            = $d->date;
                    $obj->line_name       = $d->line_name;
                    $obj->buyer           = $d->buyer;
                    $obj->style           = $d->style;
                    $obj->category        = $d->category;
                    $obj->i               = ($d->output_i>0)?($d->defect_i/$d->output_i)*100:0;
                    $obj->ii              = ($d->output_ii>0)?($d->defect_ii/$d->output_ii)*100:0;
                    $obj->iii             = ($d->output_iii>0)?($d->defect_iii/$d->output_iii)*100:0;
                    $obj->iv              = ($d->output_iv>0)?($d->defect_iv/$d->output_iv)*100:0;
                    $obj->v               = ($d->output_v>0)?($d->defect_v/$d->output_v)*100:0;
                    $obj->vi              = ($d->output_vi>0)?($d->defect_vi/$d->output_vi)*100:0;
                    $obj->vii             = ($d->output_ii>0)?($d->defect_ii/$d->output_ii)*100:0;
                    $obj->viii            = ($d->output_viii>0)?($d->defect_viii/$d->output_viii)*100:0;
                    $obj->ix              = ($d->output_ix>0)?($d->defect_ix/$d->output_ix)*100:0;
                    $obj->x               = ($d->output_x>0)?($d->defect_x/$d->output_x)*100:0;
                    $obj->xi              = ($d->output_xi>0)?($d->defect_xi/$d->output_xi)*100:0;
                    $obj->xii             = ($d->output_xii>0)?($d->defect_xii/$d->output_xii)*100:0;
                    $obj->xiii            = ($d->output_xiii>0)?($d->defect_xiii/$d->output_xiii)*100:0;
                    $obj->total           = ($d->total_output>0)?($d->total_defect/$d->total_output)*100:0;
                    $array [] = $obj;
                }
                foreach ($array as $key => $a) {
                    
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $a->style);
                    $sheet->setCellValue('D'.$row, $a->buyer);
                    $sheet->setCellValue('E'.$row, $a->category);
                    $sheet->setCellValue('F'.$row, number_format($a->i,1,',',','));
                    $sheet->cell('F'.$row, function($row)use($a) { 
                        $color = ($a->i>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('G'.$row, number_format($a->ii,1,',',','));
                    $sheet->cell('G'.$row, function($row)use($a) { 
                        $color = ($a->ii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color);
                    });
                    $sheet->setCellValue('H'.$row, number_format($a->iii,1,',',','));
                    $sheet->cell('H'.$row, function($row)use($a) { 
                        $color = ($a->iii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('I'.$row, number_format($a->iv,1,',',','));
                    $sheet->cell('I'.$row, function($row)use($a) { 
                        $color = ($a->iv>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color);
                    });
                    $sheet->setCellValue('J'.$row, number_format($a->v,1,',',','));
                    $sheet->cell('J'.$row, function($row)use($a) { 
                        $color = ($a->v>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('K'.$row, number_format($a->vi,1,',',','));
                    $sheet->cell('K'.$row, function($row)use($a) { 
                        $color = ($a->vi>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('L'.$row, number_format($a->vii,1,',',','));
                    $sheet->cell('L'.$row, function($row)use($a) { 
                        $color = ($a->vii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('M'.$row, number_format($a->viii,1,',',','));
                    $sheet->cell('M'.$row, function($row)use($a) { 
                        $color = ($a->viii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('N'.$row, number_format($a->ix,1,',',','));
                    $sheet->cell('N'.$row, function($row)use($a) { 
                        $color = ($a->ix>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('O'.$row, number_format($a->x,1,',',','));
                    $sheet->cell('O'.$row, function($row)use($a) { 
                        $color = ($a->x>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('P'.$row, number_format($a->xi,1,',',','));
                    $sheet->cell('P'.$row, function($row)use($a) { 
                        $color = ($a->xi>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('Q'.$row, number_format($a->xii,1,',',','));
                    $sheet->cell('Q'.$row, function($row)use($a) { 
                        $color = ($a->xii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('R'.$row, number_format($a->xiii,1,',',','));
                    $sheet->cell('R'.$row, function($row)use($a) { 
                        $color = ($a->xiii>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $sheet->setCellValue('S'.$row, number_format($a->total,1,',',','));
                    $sheet->cell('S'.$row, function($row)use($a) { 
                        $color = ($a->total>3)?'#e53f3f':'#69F173';
                        $row->setBackground($color); 
                    });
                    $row++;
                }
                
                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
}
