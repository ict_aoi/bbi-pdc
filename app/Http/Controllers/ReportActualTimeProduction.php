<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use View;
use Excel;
use Config;
use DateTime;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Workflow;
use App\Models\CycleTime;
use App\Models\CycleTimeBatch;

Config::set(['excel.export.calculate' => true]);
class ReportActualTimeProduction extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('report_time_study.index');
    }
    public function data()
    {
    	if(request()->ajax())
        {
            $data = CycleTimeBatch::whereNull('delete_at')
            ->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('line',function($data){
            	return ucwords($data->line->name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('condition_day',function($data){
            	return $data->day;
            })
            ->editColumn('nik',function($data){
            	return $data->user->nik;
            })
            ->editColumn('name',function($data){
            	return ucwords($data->user->name);
            })
            ->editColumn('start_job',function($data){
            	return $data->start_job;
            })
            ->editColumn('end_job',function($data){
            	return $data->end_job;
            })
            ->editColumn('end_job',function($data){
            	return $data->end_job;
            })
            ->editColumn('status',function($data){
                if ($data->end_job==null) {
                    return '<span class="label label-danger">In progress</span>';
                }else{
                    return '<span class="label label-flat border-success text-success-600 position-right">Done</span>';
                }
            })
            
            ->addColumn('action', function($data) {
                return view('report_time_study._action', [
                    'model'             => $data,
                    'data_entry'        => '/report/actual-production-time/data-entry/'.$data->id,
                    'report'            => route('report_actual_production_time.report_time_study',$data->id),
                    'report_data_entry' => route('report_actual_production_time.report_data_entry',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action','status'])
            ->make(true);
        }
    }
    public function dataEntry(Request $request,$id)
    {
        $search              = trim(strtolower($request->q));
        $cycle_time_batch_id = $request->cycle_time_batch_id;
        $workflow_id         = $request->workflow_id;
        $listProcess         = CycleTime::fetchDataEntry(false,$id,$search,false);
        // dd($listProcess);
        return view('report_time_study.lov._lov_lap_data_entry', compact('listProcess'));
    }
    public function reportTimeStudyChart(Request $request,$id){
        $data = CycleTimeBatch::whereNull('delete_at')
                            ->where('id',$id)
                            ->first();
        $line = $data->line->alias;
        if ($data) {
            $excel = new \PHPExcel();

            $dataBatch     = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY no_op desc limit 1"));
            $dataBreakdown = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY count_duration desc limit 1"));
            if ($dataBatch) {
                $dataWorkflow = Workflow::whereNull('delete_at')
                    ->where('style',$data->style)
                    ->first();
                    
                $ts = ($dataWorkflow)?$dataWorkflow->smv:0;

                $excel->getProperties()->setCreator('ICT Developer')
                ->setLastModifiedBy('ICT Developer')
                ->setTitle($data->style."-L.".$line."-".Carbon::now()->format('u'))
                ->setSubject($data->style."-L.".$line)
                ->setDescription($data->style."-L.".$line)
                ->setKeywords($data->style."-L.".$line);
                
                $sheet = $excel->setActiveSheetIndex(0);
                $excel->getActiveSheet(0)->setTitle("timestudy");

                $style_col = array(
                    'font' => array('bold' => true),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'top'    => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'right'  => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'bottom' => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'left'   => array('style'  => \PHPExcel_Style_Border::BORDER_THIN)
                    )
                );
        
                $style_row = array(
                    'alignment' => array(
                        'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'top'    => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'right'  => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'bottom' => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'left'   => array('style'  => \PHPExcel_Style_Border::BORDER_THIN)
                    )
                );

                $maxOp                    = $dataBatch[0]->no_op;
                $countBreakdown           = $dataBreakdown[0]->count_duration;
                $current_colum_p          = 'L';
                $current_colum_p_end      = chr(ord($current_colum_p)+$maxOp);
                $current_colum_op         = $current_colum_p_end;
                $current_colum_op_end     = chr(ord($current_colum_op)+$maxOp);
                $current_column_breakdown = self::getNameFromNumber(self::alpha2num($current_colum_op_end));
                

                $sheet->setCellValue('A1',"ACTUAL PRODUCTION TIME REPORT");
                $sheet->mergeCells('A1:'.$current_colum_op_end.'1');
                $sheet->mergeCells('A1:Q1');
                $sheet->getStyle('A1')->getFont()->setBold(TRUE);
                $sheet->getStyle('A1')->getFont()->setSize(18);
                $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                for ($i=3; $i <= 9; $i++) { 
                    $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('H'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('L'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $sheet->getStyle('I'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $sheet->getStyle('M'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                }

                $no_op      = 1;
                $no_op2      = 1;
                $no_p       = 1;
                $currentRow = 13;
                $no         = 1;
                for ($i=$current_colum_op; $i <$current_colum_op_end; $i++) {
                    $sheet->setCellValue($i.'11','OP name'.$no_op++);
                }

                for ($i=$current_colum_p; $i <$current_colum_p_end; $i++) {                        
                    $sheet->setCellValue($i.'11','Process time'.$no_p++);
                }
                foreach(self::excelColumnRange('A',$current_colum_op_end) as $v){
                    $sheet->mergeCells($v.'11:'.$v.'12');
                    $sheet->getColumnDimension($v)->setAutoSize(true);
                    $sheet->getStyle($v.'11')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle($v.'12')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                        )
                    );
                
                }
                $sheet->setCellValue('C3',"Style :");
                $sheet->setCellValue('C4',"Line :");
                $sheet->setCellValue('C5',"Date :");
                $sheet->setCellValue('C6',"Second :");
                $sheet->setCellValue('C7',"PPH :");
                $sheet->setCellValue('C8',"Minute :");
                $sheet->setCellValue('H3',"Condition Day :");
                $sheet->setCellValue('H4',"Actual Output :");
                $sheet->setCellValue('H5',"PIC :");
                $sheet->setCellValue('H6',"Deferention SMV :");
                $sheet->setCellValue('H7',"PPH Estimasi :");
                $sheet->setCellValue('H8',"Target :");
                $sheet->setCellValue('L3',"TS:");
                $sheet->setCellValue('L4',"Max TS Process:");
                $sheet->setCellValue('L5',"Takt Time:");
                $sheet->setCellValue('L6',"No Of Process:");
                $sheet->setCellValue('L7',"TS Actual:");
                $sheet->setCellValue('L8',"Line Balancing Rate:");
                $sheet->setCellValue('D3',$data->style);
                $sheet->setCellValue('D4',$line);
                $sheet->setCellValue('D5',Carbon::createFromFormat('Y-m-d H:i:s',  $data->end_job)->format('j F, Y'));
                $sheet->setCellValue('I3',$data->day);
                $sheet->setCellValue('I5',ucwords($data->user->name));
                $sheet->setCellValue('M3',$ts);

                $sheet->setCellValue('A11','No');
                $sheet->setCellValue('B11','Machine Name');
                $sheet->setCellValue('C11','Process Name');
                $sheet->setCellValue('D11','Target 80%');
                $sheet->setCellValue('E11','Target 100%');
                $sheet->setCellValue('F11','Alocated time');
                $sheet->setCellValue('G11','Harmonic ave');
                $sheet->setCellValue('H11','TT');
                $sheet->setCellValue('I11','UCL');
                $sheet->setCellValue('J11','LCL');
                $sheet->setCellValue('K11','SMV OB');
                $sheet->setCellValue($current_colum_op_end.'11','No. Of OP');
                $column    = self::alpha2num($current_colum_op_end);
                $column2   = self::alpha2num($current_colum_op_end);
                
                $no        = 1;
                for ($i=1; $i <= $maxOp ; $i++) {
                    $sheet->setCellValue(self::getNameFromNumber($column).'11','Pick Time OP '.$no_op2++);
                    $sheet->mergeCells(self::getNameFromNumber($column).'11:'.self::getNameFromNumber(($column+$countBreakdown)-1).'11');
                    
                    $sheet->getStyle(self::getNameFromNumber($column).'11:'.self::getNameFromNumber(($column+$countBreakdown)-1).'11')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle(self::getNameFromNumber($column).'11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $column+=$countBreakdown;
                }
                $maxCycle  = $countBreakdown*$maxOp;
                $columnMax = self::getNameFromNumber(self::alpha2num($current_colum_op_end)+$maxCycle-1);
                $no        = 1;
                for ($k=1; $k <=$maxCycle ; $k++) { 
                    $sheet->setCellValue(self::getNameFromNumber($column2).'12',$no++);
                    if ($no>$countBreakdown) {
                        $no=1;
                    }
                    $sheet->getStyle(self::getNameFromNumber($column2).'12')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle($column2.'12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $column2++;
                }

                $dataCycle = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY created_at"));
                $noUrut =1;
                foreach ($dataCycle as $key => $d) {
                    $sheet->setCellValue('A'.$currentRow, $noUrut++);
                    $sheet->setCellValue('B'.$currentRow, strtoupper($d->machine_name));
                    $sheet->setCellValue('C'.$currentRow, strtoupper($d->process_name));
                    $sheet->setCellValue('K'.$currentRow, $d->smv);
                    $operators       = explode(",",$d->operator);
                    $averages        = explode(",",$d->average);
                    $kolom           = "L";
                    $kolom_p_end     = chr(ord($kolom)+$maxOp);
                    $kolom_op_end    = chr(ord($kolom_p_end)+$maxOp);
                    $column          = self::alpha2num($kolom_op_end);
                    $kolom_breakdown = self::getNameFromNumber(self::alpha2num($kolom_op_end));
                    
                    foreach ($averages as $av) {
                        $sheet->setCellValue($kolom.$currentRow, round($av,2));
                        $kolom++;
                    }
                    foreach ($operators as $op) {
                        $sheet->setCellValue($kolom_p_end.$currentRow, strtoupper($op));
                        $kolom_p_end++;
                    }
                    $durations = json_decode($d->duration);
                    $maxCycle  = $countBreakdown;
                    $no        = 1;
                    $next      = 1;
                    foreach ($durations as $duration) {
                        foreach (json_decode($duration) as $value) {
                            $drt           = explode(",",$value);
                            $countDuration = count($drt);
                            foreach ($drt as $drt) {
                                $sheet->setCellValue(self::getNameFromNumber($column).$currentRow, $drt);
                                if ($no==$countDuration) {
                                    $next=$maxCycle-$countDuration;
                                    $column = self::alpha2num(self::getNameFromNumber(($column+$next)-1));
                                    $no     = 1;
                                    $next   = 1;
                                }else{

                                    $no++;
                                }
                                
                                $column++;
                            }
                        }
                    }
                    // die();
                    $sheet->setCellValue($kolom_op_end.$currentRow, $d->no_op);
                    $sheet->setCellValue('G'.$currentRow, '=ROUND(HARMEAN(L'.$currentRow.':'.(chr(ord('L')+$d->no_op-1)).$currentRow.'),2)');
                    $sheet->setCellValue('H'.$currentRow, '=M5');
                    $sheet->setCellValue('F'.$currentRow, '=ROUND(G'.$currentRow.'/'.$kolom_op_end.$currentRow.',2)');
                    $sheet->setCellValue('E'.$currentRow, '=ROUNDUP((3600/F'.$currentRow.'),0)');
                    $sheet->setCellValue('D'.$currentRow, '=E'.$currentRow.'*0.8');
                    
                    // $lRow = 13;
                    $lRow=13;
                    foreach(self::excelColumnRange('A', $columnMax) as $v){
                        if ($d->is_copied==false&&$d->total_update!=0) {
                            $sheet->getStyle($v.$currentRow)->applyFromArray(
                                array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    ),
                                    'fill' => array(
                                        'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '14FF24')
                                    )
                                )
                            );
                        }else{
                            $sheet->getStyle($v.$currentRow)->applyFromArray(
                                array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                )
                            );
                        }
                        $sheet->getColumnDimension($v)->setAutoSize(true);
                        // $sheet->getStyle($v.$currentRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                    $currentRow++;
                }
                $sheet->setCellValue('I4', '=MIN(D13:D'.($currentRow-1).')');
                $sheet->setCellValue('D6', '=SUM(G13:G'.($currentRow-1).')');
                $sheet->setCellValue('D7', '=ROUND(3600/D6,2)');
                $sheet->setCellValue('D8', '=ROUND(60/D7,2)');
                $sheet->setCellValue('M4','=MAX(F13:F'.($currentRow-1).')');
                $sheet->setCellValue('M6','=COUNTA(C13:C'.($currentRow-1).')');
                $sheet->setCellValue('M5', '=ROUND((M3/M6)*60,2)');
                $sheet->setCellValue('M7', '=ROUND(SUM(F13:F'.($currentRow-1).'),2)');
                $sheet->setCellValue('M8', '=ROUND(M7/(M6*M4),2)');

                $dataseriesLabels1  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$F$11', NULL, 1),
                    // 'Allocated'
                    // new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$H$11', NULL, 1),
                );
                $dataseriesLabels2  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$H$11', NULL, 1),
                );
        
                $xAxisTickValues  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$C$13:$C$' . ($currentRow-1), NULL, ($currentRow-1)),
                    // new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$C$13:$C$' . ($currentRow-1), NULL, ($currentRow-1)),
                );

                $dataSeriesValues1  = array(
                    new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$F$13:$F$' . ($currentRow-1), NULL, ($currentRow-1)),
                    // new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$H$13:$H$' . ($currentRow-1), NULL, ($currentRow-1))
                );
                $dataSeriesValues2  = array(
                    // new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$F$13:$F$' . ($currentRow-1), NULL, ($currentRow-1)),
                    new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$H$13:$H$' . ($currentRow-1), NULL, ($currentRow-1))
                );
                
                $series1 = new \PHPExcel_Chart_DataSeries(
                    \PHPExcel_Chart_DataSeries::TYPE_BARCHART,
                    \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,
                    range(0, count($dataSeriesValues1) - 1),
                    $dataseriesLabels1 ,
                    $xAxisTickValues ,
                    $dataSeriesValues1 
                );
                $series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);
                
                $series2 = new \PHPExcel_Chart_DataSeries(
                    \PHPExcel_Chart_DataSeries::TYPE_LINECHART, // plotType
                    \PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
                    range(0, count($dataSeriesValues2) - 1), // plotOrder
                    $dataseriesLabels2, // plotLabel
                    $xAxisTickValues, // plotCategory
                    $dataSeriesValues2                              // plotValues
                );
                // dd($series2);
                $title  = new \PHPExcel_Chart_Title('VISUAL KEMAMPUAN LINE (AFTER)');
                $plotarea = new \PHPExcel_Chart_PlotArea(NULL, array($series1, $series2));
                $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);
                $chart  = new \PHPExcel_Chart(
                    'timestudy',
                    $title,
                    $legend,
                    $plotarea,
                    true,
                    0,
                    NULL, 
                    NULL
                );
                $chart->setTopLeftPosition("A".($currentRow+5));
                $chart->setBottomRightPosition($columnMax.($currentRow+17));

                $excel->getActiveSheet()->addChart($chart);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="'.$data->style."-L.".$line."-".Carbon::now()->format('u').'.xlsx"'); // Set nama file excel nya
                header('Cache-Control: max-age=0');
                $write = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $write->setIncludeCharts(TRUE);
                $write->save('php://output');
            }
        }
        
    }
    public function reportDataEntry(Request $request,$id)
    {
        $cycleTimeBatch = CycleTimeBatch::find($id);
        if (!$cycleTimeBatch) {
            return response()->json(['message'=>'Data Tidak di Temukan, silahkan refresh halaman anda'],422);
        }
        $data = DB::table('report_data_entry_apt')
                        ->where([
                            ['cycle_time_batch_id', $id],
                        ])->get();
        
        return Excel::create('Report Data Entry APT -'.$cycleTimeBatch->style,function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','DATA ENTRY APT');
                $sheet->mergeCells('A1:P1');
                $sheet->cells('A1', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                });
                $sheet->setCellValue('A3','NO');
                $sheet->setCellValue('B3','DATE');
                $sheet->setCellValue('C3','LINE');
                $sheet->setCellValue('D3','STYLE');
                $sheet->setCellValue('E3','PROCESS NAME');
                $sheet->setCellValue('F3','MACHINE NAME');
                $sheet->setCellValue('G3','NIK OPERATOR');
                $sheet->setCellValue('H3','OPERATOR NAME');
                $sheet->setCellValue('I3','IE NIK');
                $sheet->setCellValue('J3','IE NAME');
                $sheet->setCellValue('K3','WIP');
                $sheet->setCellValue('L3','TYPE WIP');
                $sheet->setCellValue('M3','STICHING BURST');
                $sheet->setCellValue('N3','ROUND');
                $sheet->setCellValue('O3','SMV');
                $sheet->setCellValue('P3','DURATION');
                foreach(range('A','P') as $v){
                    $sheet->cells($v.'3', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#b0b0b0');
                    });
                }

                $array         = array();
                $row           = 4;
                $no            = 0;
                $round         = 1;
                $cycle_time_id = '';
                foreach ($data as $key => $a) {
                    if ($cycle_time_id!=$a->cycle_time_id) {
                        $no++;
                        $round=1;
                    }else{
                        $round++;
                    }
                    
                    $sheet->setCellValue('A'.$row, $no);
                    $sheet->setCellValue('B'.$row, $a->date_cycle);
                    $sheet->setCellValue('C'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, strtoupper($a->process_name));
                    $sheet->setCellValue('F'.$row, strtoupper($a->machine_name)); 
                    $sheet->setCellValue('G'.$row, $a->sewer_nik); 
                    $sheet->setCellValue('H'.$row, strtoupper($a->sewer_name)); 
                    $sheet->setCellValue('I'.$row, $a->ie_nik);
                    $sheet->setCellValue('J'.$row, strtoupper($a->ie_name));
                    $sheet->setCellValue('K'.$row, $a->wip);
                    $sheet->setCellValue('L'.$row, $a->wip);
                    $sheet->setCellValue('M'.$row, $a->burst);
                    $sheet->setCellValue('N'.$row, $round);
                    $sheet->setCellValue('O'.$row, $a->smv);
                    $sheet->setCellValue('P'.$row, $a->duration);
                    
                    foreach(range('A','P') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                    }
                    $cycle_time_id = $a->cycle_time_id;
                    $row++;
                }
                // die();

                /* $sheet->setColumnFormat(array(
                    'B' => 'yyyy-mm-dd',
                    'C' => '@',
                    'D' => '@',
                    'F' => '@',
                    'G' => '0',
                    'H' => '0',
                    'I' => '0',
                    'J' => '0',
                    'K' => '0',
                ));  */            
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    static function getNameFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return self::getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
    static function excelColumnRange($lower, $upper) {
        ++$upper;
        for ($i = $lower; $i !== $upper; ++$i) {
            yield $i;
        }
    }
    static function alpha2num($column) {
        $number = 0;
        foreach(str_split($column) as $letter){
            $number = ($number * 26) + (ord(strtolower($letter)) - 96);
        }
        return $number;
    }
}