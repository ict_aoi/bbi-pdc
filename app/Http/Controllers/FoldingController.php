<?php namespace App\Http\Controllers;

use DB;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\LogLogin;
use App\Models\QtyCarton;
use App\Models\QcEndline;
use App\Models\Production;
use App\Models\AlertFolding;
use App\Models\HangtagOutput;
use App\Models\FoldingOutput;
use App\Models\FoldingPackage;
use App\Models\BarcodeGarment;
use App\Models\Absensi\AbsensiBbi;
use App\Models\HangtagOutputDetail;
use App\Models\FoldingPackageDetail;
use App\Models\FoldingPackageMovement;
use App\Models\FoldingPackageDetailSize;


class FoldingController extends Controller
{
    public function index(Request $request,$factory,$line)
    {
        $nik            = null;
        $name           = null;
        $subdept_name   = null;
        $code           = null;
        $method_packing = null;
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();
        
        if($master_line){
            $line_id        = $master_line->id;
            $ip_address     = \Request::ip();
            $factory_id     = $master_line->factory_id;
            $buyer          = $master_line->buyer;
            $method_packing = ($master_line->is_scan=='t')?1:0;
            $scan_garment   = ($master_line->is_scan_garment=='t')?1:0;
            $dataLog        = LogLogin::whereNull('delete_at')
            ->where('ip_address',$ip_address)
            ->where(db::raw('date(created_at)'),Carbon::now())
            ->whereIn('name',['admin','folding'])->orderBy('created_at','desc')->first();
            
            $lines = Line::whereNull('delete_at')
                    ->where('buyer',$master_line->buyer)
                    ->orderBy('order','asc')
                    ->pluck('name','id')->all();
            if ($dataLog) {
                $valid = $master_line->id==$dataLog->line_id;
                if ($dataLog->expired_date>Carbon::now()&&$valid) {
                    $request->session()->put('nik', $dataLog->created_by_nik);
                    $request->session()->put('name', $dataLog->created_by_name);
                    $request->session()->put('factory',$factory);
                    $request->session()->put('subdept_name',$dataLog->subdept_name);
                    $request->session()->put('line_name',$master_line->name);
                    $request->session()->put('buyer', $master_line->buyer);
                    $request->session()->put('alias', $master_line->alias);
                    $request->session()->put('line_id', $dataLog->line_id);
                    $request->session()->put('menu_name',$dataLog->name);
                    $request->session()->put('method_packing',$method_packing);
                    $request->session()->put('scan_garment',$scan_garment);
                   
                    $obj                 = new stdClass();
                    $obj->nik            = $dataLog->created_by_nik;
                    $obj->name           = $dataLog->created_by_name;
                    $obj->factory_id     = $dataLog->factory_id;
                    $obj->line_id        = $dataLog->line_id;
                    $obj->buyer          = $request->session()->get('buyer');
                    $obj->factory        = $factory;
                    $obj->line           = $master_line->code;
                    $obj->line_name      = $master_line->name;
                    $obj->subdept_name   = $dataLog->subdept_name;
                    $obj->header_name    = $dataLog->name." ".$line;
                    $obj->method_packing = $method_packing;
                    $obj->scan_garment   = $scan_garment;

                    if ($dataLog->name=='folding') {
                        return view('folding.index',compact('obj','lines'));
                    } else if($dataLog->name=='admin-loading') {
                        return view('admin_loading.index',compact('obj','lines'));
                    }
                    
                } else {
                    $request->session()->forget(['nik', 'nama', 'factory', 'line', 'subdept_name','line_id','buyer','menu_name']);
                    $obj                 = new stdClass();
                    $obj->nik            = $nik;
                    $obj->name           = $name;
                    $obj->factory_id     = ($master_line)?$master_line->factory_id:'';
                    $obj->line_id        = ($master_line)?$master_line->id:'';
                    $obj->buyer          = ($master_line)?$master_line->buyer:'';
                    $obj->factory        = $factory;
                    $obj->line           = $master_line->code;
                    $obj->method_packing = $method_packing;
                    $obj->scan_garment   = $scan_garment;
                    $obj->subdept_name   = $subdept_name;
                    $obj->header_name    = (!$nik && $master_line->buyer == 'other'?'FOLDING ('.strtoupper($master_line->name).')':'ADMIN LOADING ('.strtoupper($master_line->name).')');

                    return view('folding.index',compact('obj','lines'));
                }

            } else {
                $obj                 = new stdClass();
                $obj->nik            = $nik;
                $obj->name           = $name;
                $obj->factory_id     = ($master_line)?$master_line->factory_id:'';
                $obj->line_id        = ($master_line)?$master_line->id:'';
                $obj->buyer          = ($master_line)?$master_line->buyer:'';
                $obj->factory        = $factory;
                $obj->line           = $master_line->code;
                $obj->method_packing = $method_packing;
                $obj->scan_garment   = $scan_garment;
                $obj->subdept_name   = $subdept_name;
                $obj->line_name      = $master_line->name;
                $obj->header_name    = (!$nik && $master_line->buyer == 'other'?'FOLDING (LINE '.strtoupper($master_line->name).')':'ADMIN LOADING ('.strtoupper($master_line->name).')');

                return view('folding.index',compact('obj','lines'));
            }
        };
    }

    public function login(Request $request)
    {
        $active_tab  = $request->active_tab;
        $nik         = trim($request->nik);
        $change_line = $request->change_line;
        $line        = $request->line;
        $factory     = $request->factory;
        $user        = $this->getAbsence($nik);
        $ip_address  = \Request::ip();

        if($user){
            $master_line    = Line::whereNull('delete_at')
            ->when($line,function($q)use($line,$change_line){
                if ($change_line!=null) {
                    $q->where('id',$change_line);
                } else {
                    $q->where('code', $line);
                }
            })
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->first();
            if (!$master_line) {
                return response()->json(['message'=>'Code Line Tidak di Temukan'],422);
            }
            $line = $master_line->code;
            $request->session()->put('nik',$user->nik);
            $request->session()->put('name',$user->name);
            $request->session()->put('menu_name',$active_tab);
            $request->session()->put('factory_name',$factory);
            $request->session()->put('factory_id',$master_line->factory_id);
            $request->session()->put('subdept_name',$user->subdept_name);
            $request->session()->put('line_id',$master_line->id);
            $request->session()->put('line_name',$master_line->name);
            $request->session()->put('factory',$factory);
            $request->session()->put('alias',$master_line->alias);
            $dataLogin = LogLogin::whereNull('delete_at')
            ->where('ip_address',$ip_address)
            ->where('name','folding')
            ->orderBy('created_at','desc')
            ->first();
            if($dataLogin){
                LogLogin::create([
                    'line_id'         => $master_line->id,
                    'name'            => $active_tab,
                    'created_by_nik'  => $user->nik,
                    'created_by_name' => $user->name,
                    'ip_address'      => \Request::ip(),
                    'subdept_name'    => $user->subdept_name,
                    'factory_id'      => $master_line->factory_id,
                    'expired_date'    => Carbon::parse(now())->addHours(9),
                ]);
                $dataLogin  ->delete_at = Carbon::now();
                $dataLogin->save();
            }else{
                $data = LogLogin::create([
                    'line_id'         => $master_line->id,
                    'name'            => $active_tab,
                    'created_by_nik'  => $user->nik,
                    'ip_address'      => \Request::ip(),
                    'created_by_name' => $user->name,
                    'subdept_name'    => $user->subdept_name,
                    'factory_id'      => $master_line->factory_id,
                    'expired_date'    => Carbon::parse(now())->addHours(9),
                ]);
            }
            if($active_tab == 'folding') $url_redirect = route('folding.index',[$factory,$line]);
            else $url_redirect = route('adminLoading.index',[$factory,$line]);
            
            return response()->json($url_redirect,200);
        }else{
            return response()->json('Nik tidak ditemukan di absensi',422);
        };
    }

    public function logout(Request $request)
    {
        $factory    = $request->factory;
        $line       = $request->line;
        $ip_address = \Request::ip();
        $menu_name  = $request->session()->get('menu_name');
        
        $master_line = Line::where('code', $line)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();
        $dataLog    = LogLogin::whereNull('delete_at')
        ->where('ip_address',$ip_address)
        ->where('name',$menu_name)->orderBy('created_at','desc')->first();
        $request->session()->forget(['nik', 'nama', 'factory', 'line', 'subdept_name','line_id','buyer','menu_name','method_packing']);
        $dataLog->delete_at = Carbon::now();
        $dataLog->save();
        return redirect()->route('folding.index',[$factory,$line]); 
    }

    static function getAbsence($nik)
    {
        $master = AbsensiBbi::where('nik',$nik)->first();
        if($master)
        {
            $obj                = new stdClass();
            $obj->nik           = $master->nik;
            $obj->name          = strtolower($master->name);
            $obj->subdept_name  = strtolower($master->subdept_name);

            return $obj;
        }else return null;
    }
    public function setTypeFolding(Request $request,$factory,$line,$typeFolding)
    {
        $request->session()->put('method_packing',$typeFolding);
        return redirect()->route('folding.index',[$factory,$line]); 
    }
    public function changeSwitch(Request $request)
    {
        $factory = $request->factory;
        $line = $request->line;
        $request->session()->forget(['method_packing']);
        return redirect()->route('folding.index',[$factory,$line]);
    }
    static function poreferencePickList(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $buyer = $request->buyer;
            $lists = DB::table('ns_qcoutput')
            ->distinct()
            ->select('ns_qcoutput.production_id','style','poreference','article','start_date',db::raw('COALESCE(folding_output.total_folding,0) as folding_output'))
            ->leftjoin(
                DB::raw('(select production_id,production_size_id,sum(counter)as total_folding from folding_outputs where scan_from=' . "'folding'" . '
                GROUP BY production_id,production_size_id)
                folding_output'),
                function ($leftjoin) {
                    $leftjoin->on('ns_qcoutput.production_id', '=', 'folding_output.production_id');
                    $leftjoin->on('ns_qcoutput.production_size_id', '=', 'folding_output.production_size_id');
                }
            )
            ->where('line_id', $request->session()->get('line_id'))
            ->where(db::raw('lower(poreference)'), 'LIKE', "%$q%")
            // ->where(db::raw('COALESCE(balance_hangtag,balance_folding)'),'<',0)
            // ->orderBy('start_date','total_output_folding','desc')
            ->where(db::raw('(ns_qcoutput.qty_order)-(COALESCE(folding_output.total_folding,0))'),'<',0)
            ->orderBy('start_date',db::raw('COALESCE(folding_output.total_folding,0)'),'desc')
            ->paginate(10);

            return view('folding._poreference_picklist', compact(['buyer','lists']));
        }
    }
    public function breakDownSizeData(Request $request)
    {
        if ($request->ajax()) {

            $data = DB::table('hangtag_output_v')
            ->where('line_id', $request->session()->get('line_id'))
            ->where('production_id',$request->production_id);
            $array=[];
            foreach ($data->get() as $key => $value) {
                $obj                       = new stdClass();
                $obj->id                   = ($value->hangtag_output_id ? $value->hangtag_output_id : -1);
                $obj->production_id        = $value->production_id;
                $obj->production_size_id   = $value->production_size_id;
                $obj->qc_endline_id        = $value->qc_endline_id;
                $obj->size                 = $value->size;
                $obj->qty_order            = $value->qty_order;
                $obj->total_output_qc      = $value->total_output_qc;
                $obj->total_output_hangtag = ($value->total_output_hangtag?$value->total_output_hangtag:0);
                $obj->wip                  = $value->total_output_qc-$value->total_output_hangtag;

                $array []              = $obj;
            }
            return response()->json($array,200);
        }
    }
    public function storeHangtag(Request $request,$factory,$line)
    {
        if ($request->ajax()) {

            $production_id      = $request->production_id;
            $production_size_id = $request->production_size_id;
            $qc_endline_id = $request->qc_endline_id;
            $qty                = (int)$request->qty;

            $hangtag = HangtagOutput::whereNull('delete_at')
            ->where('production_id',$production_id)
            ->where('production_size_id',$production_size_id)
            ->first();
            $qcEndline         = QcEndline::find($qc_endline_id);

            $hangtag_output_id = ($hangtag)?$hangtag->id:null;
            $hangtagCounter    = ($hangtag)?$hangtag->counter:0;
            $qcEndlineCounter  = ($qcEndline)?$qcEndline->counter:0;
            $qtyNew            = $qcEndlineCounter-$hangtagCounter;
            if ($qtyNew<0) {
                return response()->json(['message' => 'Simpan Data Gagal, Qty yang dimasukan lebih dari qty wip'], 422);
            }
            try {
                DB::beginTransaction();
                if (!$hangtag) {
                    $hangtag = HangtagOutput::FirstOrCreate([
                        'production_id' => $production_id,
                        'production_size_id' => $production_size_id
                    ]);
                    $hangtag_output_id = $hangtag->id;
                }
                for ($i=0; $i < $qty; $i++) { 
                    HangtagOutputDetail::create([
                        'hangtag_output_id'   => $hangtag_output_id,
                        'counter'         => 1,
                        'ip_address'      => \Request::ip(),
                        'created_by_name' => $request->session()->get('name'),
                        'created_by_nik'  => $request->session()->get('nik')
                    ]);
                }
                $date = Carbon::now()->format('Y-m-d');
                DB::select(db::Raw("select * from update_counter_hangtag_detail('$hangtag_output_id')"));
                DB::select(db::Raw("select * from update_counter_hangtag('$hangtag_output_id','$date')"));
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function breakDownSizeScan(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_fgms = 'fgms_live';
            else $connection_fgms = 'fgms_dev';
            $barcode_id    = $request->barcode;
            $packageDetail = DB::connection($connection_fgms)
            ->table('v_package_detail_split_detail')
            ->where('barcode_id',$barcode_id)
            ->get();
            if (count($packageDetail)==0) {
                return response()->json(['message' => 'Barcode tidak di temukan, Info Packing'], 422);
            }

            $current_department = $packageDetail[0]->current_department;
            $current_status = $packageDetail[0]->current_status;

            if (!($current_department=='preparation'&&$current_status=='completed')&&!($current_department=='sewing'&&$current_status=='onprogress')) {
                $checkScan = DB::connection($connection_fgms)
                ->table('v_check_scan')
                ->where('barcode_id',$barcode_id)
                ->get();
                $message = (count($checkScan))?$checkScan[0]->description:null;
                return response()->json(['message' => "Info Packing, ".$message], 422);
            }
            $style         = array();
            $poreference   = array();
            $article       = array();
            $customer_size = array();
            foreach ($packageDetail as $key => $p) {
                array_push($style,$p->style);
                array_push($poreference,$p->po_number);
                array_push($article,$p->article);
                array_push($customer_size,$p->customer_size);
                array_push($customer_size,$p->manufacturing_size);
            }
            
            $inner_pack_all = $packageDetail[0]->inner_pack_all;
            $buyer          = $request->session()->get('buyer');
            $lineName       = $request->session()->get('line_name');
            $poName         = ($buyer=='nagai')?"NO LOT :":"Po Buyer :";
            $production     = Production::whereNull('productions.delete_at')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->where([
                ['productions.line_id', $request->session()->get('line_id')],
            ])
            ->whereIn('productions.poreference',$poreference)
            ->whereIn('productions.style',$style)
            ->whereIn('productions.article',$article)
            ->whereIn('production_sizes.size',$customer_size)->first();
            if (!$production) {
                return response()->json(['message' => $poName.implode(",",$poreference).", Style :".implode(",",$style)." ,Article :".implode(",",$article).", Size :".implode(",",$customer_size)." Tidak jalan di line ini(".strtoupper($lineName).")"], 422);
            }
            $line = Line::find($production->line_id);
            $line_fgms = DB::connection($connection_fgms)
            ->table('lines')
            ->where('name',$line->code)
            ->get();
            $line_id_fgms = (count($line_fgms))>0?$line_fgms[0]->id:null;
            $scan_garment = $line->is_scan_garment?1:0;
            try {
                DB::beginTransaction();
                $array = [];
                foreach ($packageDetail as $key => $value) {
                    $custsize = $value->customer_size;
                    $manfsize = $value->manufacturing_size;

                    $qc_outputs = DB::table('qc_output_v')
                    ->where('line_id', $request->session()->get('line_id'))
                    ->where('poreference',$value->po_number)
                    ->where('style',$value->style)
                    ->where('article',$value->article)
                    ->where(function($query) use ($custsize,$manfsize){
                        $query->where('size',$custsize)->orWhere('size',$manfsize);
                    });
                    // ->where('size',$value->customer_size);
                    
                    
                    $obj= new stdClass();
                    foreach ($qc_outputs->get() as $key => $qc) {
                        $foldingPackage = FoldingPackage::whereNull('delete_at')
                        ->where('barcode_id',$value->barcode_id);
                        $folding_package_id = ($foldingPackage->first())?$foldingPackage->first()->id:null;
                        $barcode_garment = -1;
                        if ($scan_garment==1) {
                            $getBarcode = BarcodeGarment::whereNull('delete_at')
                            ->where([
                                ['poreference',$qc->poreference],
                                ['style',$qc->style],
                                ['size',$qc->size],
                                ['article',$qc->article]
                            ])->first();
                            $barcode_garment = $getBarcode?$getBarcode->barcode_id:-1;
                        }

                        if (!$foldingPackage->exists()) {
                            $foldingPackage = FoldingPackage::FirstOrCreate([
                                'scan_id'            => $value->scan_id,
                                'barcode_id'         => $value->barcode_id,
                                'pkg_count'          => $value->pkg_count?$value->pkg_count:0,
                                'item_qty'           => $value->inner_pack,
                                'current_department' => "sewing",
                                'current_status'     => "onprogress"
                            ]);
                            $packageDetail = DB::connection($connection_fgms)
                            ->table('packages')
                            ->where('barcode_id',$barcode_id)
                            ->update([
                                'current_department' => 'sewing',
                                'current_status'     => 'onprogress',
                                'status_sewing'      => 'onprogress',
                                'updated_at'         => Carbon::now()
                            ]);
                            $folding_package_id = $foldingPackage->id;
                        }
                        $foldingPackageDetailSize = FoldingPackageDetailSize::whereNull('delete_at')
                        ->where('folding_package_id',$folding_package_id)
                        ->where([
                            ['poreference',$value->po_number],
                            ['style',$value->style],
                            ['size',$value->customer_size],
                            ['article',$value->article],
                            [ 'folding_package_id',$folding_package_id],
                        ]);

                        $folding_package_detail_size_id = ($foldingPackageDetailSize->first())?$foldingPackageDetailSize->first()->id:null;
                        $folding_inner_pack = ($foldingPackageDetailSize->first())?$foldingPackageDetailSize->first()->inner_pack:null;
                        if (!$foldingPackageDetailSize->exists()) {
                            $foldingPackageDetailSize = FoldingPackageDetailSize::Create([
                                'folding_package_id' => $folding_package_id,
                                'style'              => $value->style,
                                'poreference'        => $value->po_number,
                                'size'               => $value->customer_size,
                                'article'            => $value->article,
                                'inner_pack'         => $value->inner_pack
                            ]);
                            $folding_package_detail_size_id = $foldingPackageDetailSize->id;
                            $folding_inner_pack             = $foldingPackageDetailSize->inner_pack;
                        }
                        $packageDetail = DB::connection($connection_fgms)
                        ->table('package_movements')
                        ->where('barcode_package',$barcode_id)
                        ->whereNull('deleted_at')
                        ->update([
                            'deleted_at'         => Carbon::now()
                        ]);
                        
                        $packageDetail = DB::connection($connection_fgms)
                        ->table('package_movements')
                        ->insert([
                            'barcode_package' => $barcode_id,
                            'department_from' => 'preparation',
                            'department_to'   => 'sewing',
                            'status_from'     => 'completed',
                            'status_to'       => 'onprogress',
                            'user_id'         => ($buyer=='other')?41:42,
                            'created_at'      => Carbon::now(),
                            'description'     => 'accepted on checkin sewing',
                            'line_id'         => $line_id_fgms,
                            'ip_address'      => \Request::ip()
                        ]);
                        $foldingOutput = FoldingOutput::whereNull('delete_at')
                        ->where('production_id',$qc->production_id)
                        ->where('production_size_id',$qc->production_size_id)
                        ->first();

                        $foldingPackageDetail = FoldingPackageDetail::whereNull('delete_at')
                        ->where([
                            ['folding_package_detail_size_id',$folding_package_detail_size_id],
                            ['folding_package_id',$folding_package_id],
                        ]);
                        $dataQtyCarton = QtyCarton::where([
                            ['style',$qc->style],
                            ['size',$qc->size],
                        ])->first();
                        $qtyPolibag = $dataQtyCarton?$dataQtyCarton->qty:1;
                        
                        $qty_available                       = (int)$folding_inner_pack -  (int)$foldingPackageDetail->count();
                        $foldingCounter                      = ($foldingOutput)?$foldingOutput->counter:0;
                        $balance                             = (int)$qc->qty_order - (int)$foldingCounter;

                        $obj->folding_package_id             = $folding_package_id;
                        $obj->production_id                  = $qc->production_id;
                        $obj->production_size_id             = $qc->production_size_id;
                        $obj->folding_package_detail_size_id = $folding_package_detail_size_id;
                        $obj->barcode_garment                = $barcode_garment;
                        $obj->poreference                    = $value->po_number;
                        $obj->style                          = $value->style;
                        $obj->article                        = $value->buyer_item;
                        $obj->size                           = $value->customer_size;
                        $obj->inner_pack                     = $value->inner_pack;
                        $obj->qty_order                      = $qc->qty_order;
                        $obj->qty_input                      = $foldingPackageDetail->count();
                        $obj->wip                            = (int)$qc->total_output- (int)$foldingCounter;
                        $obj->balance                        = $balance;
                        $obj->qty_available                  = $qty_available;
                        $obj->scan_garment                   = $scan_garment;
                        $obj->qty_polibag                    = $qtyPolibag;
                        
                    }
                    $array []              = $obj;
                }
                DB::commit();
                return response()->json(['data'=>$array,'inner_pack'=>$inner_pack_all,'status'=>0],200);

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function storePacking(Request $request,$factory,$line)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_fgms = 'fgms_live';
            else $connection_fgms = 'fgms_dev';

            $production_id                  = $request->production_id;
            $production_size_id             = $request->production_size_id;
            $folding_package_id             = $request->folding_package_id;
            $folding_package_detail_size_id = $request->folding_package_detail_size_id;
            $qty_input                      = $request->qty_input;
            $full_inner                     = $request->full_inner;
            $scan_garment                   = $request->scan_garment;
            $barcode_garment                = $request->barcode_id;
            $buyer                          = $request->session()->get('buyer');
            
            $outputHangtag = HangtagOutput::whereNull('delete_at')
            ->where('production_id',$production_id)
            ->where('production_size_id',$production_size_id)
            ->exists();
            if ($outputHangtag) {
                return response()->json(['message' => 'Simpan Data Gagal, Qty Sudah di input di hangtag'], 422);
            }

            $dataPackage = FoldingPackageDetailSize::select(
                'folding_packages.item_qty',
                'folding_package_detail_sizes.inner_pack',
                db::raw('folding_packages.counter as qty_package'),
                db::raw('COALESCE(folding_package_details.qty_detail,0) as qty_inner')
            )
            ->leftJoin('folding_packages','folding_packages.id','folding_package_detail_sizes.folding_package_id')
            ->leftjoin(
                DB::raw('(select folding_package_id,folding_package_detail_size_id,count(*) as qty_detail from folding_package_details
                GROUP BY folding_package_id,folding_package_detail_size_id)
                folding_package_details'),
                function ($leftjoin) {
                    $leftjoin->on('folding_packages.id', '=', 'folding_package_details.folding_package_id');
                    $leftjoin->on('folding_package_detail_sizes.id', '=', 'folding_package_details.folding_package_detail_size_id');
                }
            )
            ->where('folding_package_detail_sizes.id',$folding_package_detail_size_id)
            ->first();
            $qtyInner    = (int)$dataPackage->qty_inner + (int)$qty_input;
            $qtyPackage  = (int)$dataPackage->qty_package + (int)$qty_input;
            $cekComplete = ($dataPackage->item_qty==$qtyPackage);
            
            if ($qtyInner>$dataPackage->inner_pack) {
                return response()->json(['message' => 'Simpan Data Gagal, Qty Inner Pack Sudah terpenuhi'], 422);
            }
            if ($qtyPackage>$dataPackage->item_qty) {
                return response()->json(['message' => 'Simpan Data Gagal, Qty Karton Sudah Penuh'], 422);
            }

            if ($full_inner==1) {
                $qty_input = (int)$dataPackage->inner_pack-(int)$dataPackage->qty_inner;
            }
            $dataFoldingOutput = FoldingOutput::whereNull('delete_at')
            ->where('production_id',$production_id)
            ->where('production_size_id',$production_size_id)
            ->first();
            $folding_output_id = ($dataFoldingOutput)?$dataFoldingOutput->id:null;
            $foldingOutput     = ($dataFoldingOutput)?$dataFoldingOutput->counter:0;
            $barcode_garment_id = null;
            if ($scan_garment==1) {
                $production = Production::whereNull('productions.delete_at')
                ->select('productions.style','productions.poreference','productions.article','production_sizes.size')
                ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
                ->where('productions.id',$production_id)
                ->where('production_sizes.id',$production_size_id)
                ->first();
                if (!$production) {
                    return response()->json(['message' => 'Simpan Data Gagal, Silahkan Refresh Halaman anda'], 422);
                }
                $barcodeGarment = BarcodeGarment::whereNull('delete_at')
                ->where([
                    ['poreference',$production->poreference],
                    ['style',$production->style],
                    ['size',$production->size],
                    ['article',$production->article]
                ])->first();
               
                $barcode_garment_id = $barcodeGarment?$barcodeGarment->id:null;
                if (!$barcodeGarment) {
                    $insert = BarcodeGarment::create([
                        'poreference'=> $production->poreference,
                        'style'      => $production->style,
                        'size'       => $production->size,
                        'article'    => $production->article,
                        'barcode_id' => $barcode_garment
                    ]);
                    $barcode_garment_id = $insert->id;
                }

            }
            $line_fgms = DB::connection($connection_fgms)
            ->table('lines')
            ->where('name',$line)
            ->get();
            $line_id_fgms = (count($line_fgms))>0?$line_fgms[0]->id:null;
            try {
                DB::beginTransaction();
                if(!$dataFoldingOutput){
                    $dataFoldingOutput = FoldingOutput::create([
                        'production_id'      => $production_id,
                        'production_size_id' => $production_size_id,
                        'counter'            => 0,
                        'scan_from'          => 'folding'
                    ]);
                    $folding_output_id = $dataFoldingOutput->id;
                    $foldingOutput     = $dataFoldingOutput->counter;
                }
                $dataFoldingOutput->counter    = $foldingOutput+$qty_input;
                $dataFoldingOutput->updated_at = Carbon::now();
                $dataFoldingOutput->save();

                for ($i=0; $i < $qty_input; $i++) { 
                    FoldingPackageDetail::create([
                        'folding_output_id'              => $folding_output_id,
                        'folding_package_id'             => $folding_package_id,
                        'folding_package_detail_size_id' => $folding_package_detail_size_id,
                        'barcode_garment_id'             => $barcode_garment_id,
                        'ip_address'                     => \Request::ip(),
                        'created_by_name'                => $request->session()->get('name'),
                        'created_by_nik'                 => $request->session()->get('nik')
                    ]);
                }
                

                $dataFoldingPackage = FoldingPackage::find($folding_package_id);
                $dataFoldingPackage->counter = $dataFoldingPackage->counter+$qty_input;
                if ($cekComplete) {
                    $dataFoldingPackage->current_department = 'sewing';
                    $dataFoldingPackage->current_status = 'completed';
                    $dataFoldingPackage->updated_at = Carbon::now();
                }
                $dataFoldingPackage->save();
                $status = '0';
                if ($cekComplete) {
                    DB::connection($connection_fgms)
                    ->table('packages')
                    ->where('barcode_id',$dataFoldingPackage->barcode_id)
                    ->update([
                        'current_department' => 'sewing',
                        'current_status'     => 'completed',
                        'status_sewing'      => 'completed',
                        'updated_at'         => Carbon::now()
                    ]);
                    $packageDetail = DB::connection($connection_fgms)
                    ->table('package_movements')
                    ->where('barcode_package',$dataFoldingPackage->barcode_id)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at'         => Carbon::now()
                    ]);
                    $packageDetail = DB::connection($connection_fgms)
                    ->table('package_movements')
                    ->insert([
                        'barcode_package' => $dataFoldingPackage->barcode_id,
                        'department_from' => 'sewing',
                        'department_to'   => 'sewing',
                        'status_from'     => 'onprogress',
                        'status_to'       => 'completed',
                        'user_id'         => ($buyer=='other')?41:42,
                        'created_at'      => Carbon::now(),
                        'description'     => 'set as completed on sewing',
                        'line_id'         => $line_id_fgms,
                        'ip_address'      => \Request::ip()
                    ]);
                    $status = '1';
                }
                DB::commit();
                return response()->json(['qty'=>$qty_input,'status' => $status],200);

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }
    }
    public function storeIdleScanPacking(Request $request,$factory,$line)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_fgms = 'fgms_live';
            else $connection_fgms = 'fgms_dev';
            $data = json_decode($request->data);
            try {
                DB::beginTransaction();
                $folding_package_id = '';
                foreach ($data as $key => $d) {
                    if ($d->qty>0) {
                        $dataPackage = FoldingPackageDetailSize::dataPackageDetailSize($d->folding_package_detail_size_id);
                        $qtyInner    = (int)$dataPackage->qty_inner + (int)$d->qty;
                        $qtyPackage  = (int)$dataPackage->qty_package + (int)$d->qty;
                        if ($qtyInner<$dataPackage->inner_pack) {
                            if ($qtyPackage<$dataPackage->item_qty) {
                                $dataFoldingOutput = FoldingOutput::whereNull('delete_at')
                                                    ->where('production_id',$d->production_id)
                                                    ->where('production_size_id',$d->production_size_id)
                                                    ->first();
                                $folding_output_id = ($dataFoldingOutput)?$dataFoldingOutput->id:null;
                                $foldingOutput     = ($dataFoldingOutput)?$dataFoldingOutput->counter:0;
                                $barcode_garment_id = null;
                                if ($d->scan_garment==1) {
                                    $production = Production::whereNull('productions.delete_at')
                                                ->select('productions.style','productions.poreference','productions.article','production_sizes.size')
                                                ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
                                                ->where('productions.id',$d->production_id)
                                                ->where('production_sizes.id',$d->production_size_id)
                                                ->first();
                                    $barcodeGarment = BarcodeGarment::whereNull('delete_at')
                                                        ->where([
                                                            ['poreference',$production->poreference],
                                                            ['style',$production->style],
                                                            ['size',$production->size],
                                                            ['article',$production->article]
                                                        ])->first();
                                    $barcode_garment_id = $barcodeGarment?$barcodeGarment->id:null;
                                    if (!$barcodeGarment) {
                                        $insert = BarcodeGarment::create([
                                            'poreference'=> $production->poreference,
                                            'style'      => $production->style,
                                            'size'       => $production->size,
                                            'article'    => $production->article,
                                            'barcode_id' => $d->barcode_id
                                        ]);
                                        $barcode_garment_id = $insert->id;
                                    }                    
                                }
                                
                                if(!$dataFoldingOutput){
                                    $dataFoldingOutput = FoldingOutput::create([
                                        'production_id'      => $d->production_id,
                                        'production_size_id' => $d->production_size_id,
                                        'counter'            => 0,
                                        'scan_from'          => 'folding'
                                    ]);
                                    $folding_output_id = $dataFoldingOutput->id;
                                    $foldingOutput     = $dataFoldingOutput->counter;
                                }
                                $dataFoldingOutput->counter    = $foldingOutput+$d->qty;
                                $dataFoldingOutput->updated_at = Carbon::now();
                                $dataFoldingOutput->save();
                                for ($i=0; $i < $d->qty; $i++) { 
                                    FoldingPackageDetail::create([
                                        'folding_output_id'              => $folding_output_id,
                                        'folding_package_id'             => $d->folding_package_id,
                                        'folding_package_detail_size_id' => $d->folding_package_detail_size_id,
                                        'barcode_garment_id'             => $barcode_garment_id,
                                        'ip_address'                     => \Request::ip(),
                                        'created_by_name'                => $request->session()->get('name'),
                                        'created_by_nik'                 => $request->session()->get('nik')
                                    ]);
                                }
                                $folding_package_id          = $d->folding_package_id;
                                $dataFoldingPackage          = FoldingPackage::find($d->folding_package_id);
                                $dataFoldingPackage->counter = $dataFoldingPackage->counter+$d->qty;
                                $dataFoldingPackage->save();

                                
                                }
                            }
                        }
                    }
                    $status = '0';
                    if ($folding_package_id!='') {
                        $line_fgms = DB::connection($connection_fgms)
                                            ->table('lines')
                                            ->where('name',$line)
                                            ->get();
                        $line_id_fgms   = (count($line_fgms))>0?$line_fgms[0]->id:null;
                        $foldingPackage = FoldingPackage::find($folding_package_id);
                        $cekComplete    = ($foldingPackage->item_qty==$foldingPackage->counter);
                        if ($cekComplete) {

                            $foldingPackage->current_department = 'sewing';
                            $foldingPackage->current_status     = 'completed';
                            $foldingPackage->updated_at         = Carbon::now();
                            $foldingPackage->save();

                            DB::connection($connection_fgms)
                            ->table('packages')
                            ->where('barcode_id',$foldingPackage->barcode_id)
                            ->update([
                                'current_department' => 'sewing',
                                'current_status'     => 'completed',
                                'status_sewing'      => 'completed',
                                'updated_at'         => Carbon::now()
                            ]);
                            $packageDetail = DB::connection($connection_fgms)
                            ->table('package_movements')
                            ->where('barcode_package',$foldingPackage->barcode_id)
                            ->whereNull('deleted_at')
                            ->update([
                                'deleted_at'         => Carbon::now()
                            ]);
                            $packageDetail = DB::connection($connection_fgms)
                            ->table('package_movements')
                            ->insert([
                                'barcode_package' => $foldingPackage->barcode_id,
                                'department_from' => 'sewing',
                                'department_to'   => 'sewing',
                                'status_from'     => 'onprogress',
                                'status_to'       => 'completed',
                                'user_id'         => ($buyer=='other')?41:42,
                                'created_at'      => Carbon::now(),
                                'description'     => 'set as completed on sewing',
                                'line_id'         => $line_id_fgms,
                                'ip_address'      => \Request::ip()
                            ]);
                            $status = '1';
                        }
                    }
                    

                DB::commit();
                return response()->json(['status' => $status],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function reportFoldingScan(Request $request)
    {
        if ($request->ajax()) {
            $q    = trim(strtolower($request->q));
            /* $data = Production::select(
                'productions.factory_id',
                'productions.line_id',
                'productions.id as production_id',
                'folding_outputs.id as folding_output_id',
                'productions.style',
                'productions.poreference',
                'productions.article',
                'production_sizes.size',
                'production_sizes.qty as qty_loading',
                db::raw('COALESCE(folding_outputs.counter,0) as total_folding'),
                db::raw('COALESCE(package_detail.pkg_count,0) as pkg_count'),
                'package_detail.scan_id'
            )
            ->leftJoin('production_sizes','productions.id','production_sizes.production_id')
            ->leftjoin("folding_outputs",function($join){
                $join->on("folding_outputs.production_id","=","productions.id")
                    ->on("folding_outputs.production_size_id","=","production_sizes.id");
            })
            ->where(function ($query) use ($q) {
                $query->where(db::raw('lower(productions.poreference)'), 'LIKE', "%$q%")
                    ->orWhere(db::raw('lower(productions.style)'), 'LIKE', "%$q%");
            })
            ->leftjoin(
                DB::raw('(select folding_output_id,scan_id,pkg_count,count(*) as total_scan from folding_package_details fpd LEFT JOIN folding_packages fp on fp.id=fpd.folding_package_id GROUP BY scan_id,pkg_count,folding_output_id)package_detail'),
                function ($leftjoin) {
                    $leftjoin->on('package_detail.folding_output_id', '=', 'folding_outputs.id');
                }
            )
            ->whereNull('productions.delete_at')
            ->where([
                ['productions.line_id',$request->session()->get('line_id')],
                ['productions.is_show_folding','t']
            ])
            ->orderBy('productions.created_at','desc')
            ->paginate(20);; */
            $data = Production::select('id as production_id','style','poreference','article')
            ->leftjoin(
                DB::raw('(select production_id,max(updated_at) as updated_at from folding_outputs
                GROUP BY production_id)fo'),
                function ($leftjoin) {
                    $leftjoin->on('fo.production_id', '=', 'productions.id');
                }
            )
            ->whereNull('productions.delete_at')
            ->where(function ($query) use ($q) {
                $query->where(db::raw('lower(poreference)'), 'LIKE', "%$q%")
                    ->orWhere(db::raw('lower(style)'), 'LIKE', "%$q%");
            })
            ->where([
                ['productions.line_id',$request->session()->get('line_id')],
                ['productions.is_show_folding','t']
            ])
            ->orderByRaw('fo.updated_at DESC NULLS LAST')
            ->orderBy('productions.created_at','desc')
            ->paginate(20);
            return view('folding.report_folding_scan',compact('data'));
        }
    }
    public function sendAlertQc(Request $request,$factory,$line)
    {
        if ($request->ajax()) {
            $listStyle = DB::table('qc_output_v')
                    ->where('line_id', $request->session()->get('line_id'))
                    ->whereRaw('balance<0')
                    ->groupBy('production_id','style','poreference','article','start_date')
                    ->orderBy('start_date','desc')
                    ->pluck(db::raw("CONCAT(style,'|',poreference,'|',article) as name"),'production_id')->all();
            return view('folding.lov._lov_info_qc',compact('listStyle','factory','line'));
        }
    }
    public function getSize(Request $request)
    {
        if ($request->ajax()) {
            $productionId = $request->id;
            $size = Production::select('production_sizes.size','production_sizes.id')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->where([
                ['productions.line_id',$request->session()->get('line_id')],
                ['productions.id',$productionId]
            ])
            ->pluck('production_sizes.id','production_sizes.size');
            return response()->json(['size' => $size]);
        }
    }
    public function storeAlertFolding(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';
            
            $productionId = $request->no_order;
            $productionSizeId = $request->size;
            $dataOutput = DB::table('qc_output_v')
            ->where([
                ['production_id',$productionId],
                ['production_size_id',$productionSizeId],
            ])->first();
            $qcOuput = ($dataOutput)?$dataOutput->total_output:0;
            try {
                DB::beginTransaction();
                AlertFolding::create([
                    'production_id'      => $productionId,
                    'production_size_id' => $productionSizeId,
                    'qc_output'          => $qcOuput,
                    'create_user_name'   => $request->session()->get('name'),
                    'create_user_nik'    => $request->session()->get('nik'),
                ]);

                DB::connection($connection_mid)
                ->table('whatsapp')
                ->insert([
                    'contact_number' => 'Nagai Digitalisasi',
                    'is_group' => 't',
                    'message' => '*Dear GL/SPV QC ENDLINE*|Tolong input Nomor Order Berikut, Karena barang akan di kirim ke  Packing|Line :'.$dataOutput->line_name.'|No. Lot : '.$dataOutput->poreference.'|Style : '.$dataOutput->style.'|Color  :'.$dataOutput->article.'|Size : '.$dataOutput->size.'|Terimakasih.'
                ]);
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function detailSize(Request $request)
    {
        $rules = [
            'production_id'        => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $production_id= $request->production_id;
        $data = DB::table('folding_details')
        ->where('production_id',$production_id)
        ->get();

        return response()->json(['data' => $data]);
    }
}