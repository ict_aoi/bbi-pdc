<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;

class ReportOperatorPerformanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('report_operator_performance.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);
            $factory_id = Auth::user()->factory_id;
            $grade     = $request->grade;
            $startDate = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate   = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            if($grade==null){
                $data      = DB::select(db::Raw("select * from f_operator_performance2('$factory_id','$startDate','$endDate') order by urut"));
            }else{
                $data      = DB::select(db::Raw("select * from f_operator_performance2('$factory_id','$startDate','$endDate')where grade='$grade' order by urut"));
            }
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('sewer_name',function($data){
                return ucwords($data->sewer_name);
            })
            ->editColumn('sewer_nik',function($data){
                return $data->sewer_nik;
            })
            ->editColumn('component',function($data){
                return ucwords($data->component);
            })
            ->editColumn('process_name',function($data){
                return ucwords($data->process_name);
            })
            ->editColumn('smv',function($data){
                return ucwords($data->smv);
            })
            ->editColumn('machine_name',function($data){
                
                return ucwords($data->machine_name);
            })
            ->editColumn('total_round',function($data){
                return $data->total_round;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('total_score',function($data){
                return $data->total_score;
            })
            ->editColumn('grade',function($data){
                return $data->grade;
            })
            ->editColumn('round1',function($data){
                // return '<button class="button" style="background-color: '.$data->grade_round1.';border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin:4px 2px;cursor: pointer;">'.$data->round1.'</button>';
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round1.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round1.'</span>';
            })
            ->editColumn('round2',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round2.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round2.'</span>';
            })
            ->editColumn('round3',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round3.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round3.'</span>';
            })
            ->editColumn('round4',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round4.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round4.'</span>';
            })
            ->editColumn('round5',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round5.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round5.'</span>';
            })
            ->editColumn('round6',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round6.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round6.'</span>';
            })
            ->editColumn('round7',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round7.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round7.'</span>';
            })
            ->editColumn('round8',function($data){
                return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round8.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round8.'</span>';
            })
            ->rawColumns(['round1','round2','round3','round4','round5','round6','round7','round8'])
            ->make(true);
        }
    }
    public function exportFormFilterSummary(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $grade     = $request->grade;
        $factory_id = Auth::user()->factory_id;
        $startDate = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate   = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        if($grade==null){
            $data      = DB::select(db::Raw("
                    select * from f_operator_performance2('$factory_id','$startDate','$endDate') opt  
                    LEFT JOIN(
                    SELECT
                        qi.process_sewing_id,
                        pr.line_id,sewer_nik as nik,round,date(qi.created_at) as created_at,
                        MAX ( qi.created_at ) AS max_created 
                    FROM
                        qc_inlines qi
                        LEFT JOIN productions pr ON pr.ID = qi.production_id 
                    GROUP BY
                        qi.process_sewing_id,sewer_nik,round,date(qi.created_at),
                        pr.line_id
                    )urut on urut.process_sewing_id=opt.process_sewing_id and urut.line_id=opt.line_id and urut.nik=opt.sewer_nik and urut.round=opt.total_round and urut.created_at=opt.date
                    ORDER BY urut,urut.max_created
            "));
        }else{
            $data      = DB::select(db::Raw("select * from f_operator_performance2('$factory_id','$startDate','$endDate')where grade='$grade'"));
        }
        return Excel::create('Sewing Operator Performance-'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A2','Tanggal');
                $sheet->setCellValue('B2','Nama Line');
                $sheet->setCellValue('C2','Nama Operator');
                $sheet->setCellValue('D2','NIK Operator');
                $sheet->setCellValue('E2','Nama Komponen');
                $sheet->setCellValue('F2','Nama Proses');
                $sheet->setCellValue('G2','SMV');
                $sheet->setCellValue('H2','Nama Mesin');
                $sheet->setCellValue('I2','Style');
                $sheet->setCellValue('J1','Round');
                $sheet->mergeCells('J1:O1');
                $sheet->cell('J1:O1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('J2','R 1');
                $sheet->setCellValue('K2','R 2');
                $sheet->setCellValue('L2','R 3');
                $sheet->setCellValue('M2','R 4');
                $sheet->setCellValue('N2','R 5');
                $sheet->setCellValue('O2','R 6');
                $sheet->setCellValue('P2','R 7');
                $sheet->setCellValue('Q2','R 8');
                $sheet->setCellValue('R2','Total Round');
                $sheet->setCellValue('S2','Total Score');
                $sheet->setCellValue('T2','Grade');
                $row   = 3;
                foreach ($data as $key => $a) {
                    $colorBackground1 = (($a->grade_round1 == 'red') ? "#ff0000" :(($a->grade_round1 == 'LawnGreen') ? "#40ff00" :(($a->grade_round1 == 'Gold') ? "#ffea00" : "")));
                    $fontColor1 = ($a->grade_round1=='red')?'#ffffff':(($a->grade_round1!='red')?'#000000':'');
                    $colorBackground2 = (($a->grade_round2 == 'red') ? "#ff0000" :(($a->grade_round2 == 'LawnGreen') ? "#40ff00" :(($a->grade_round2 == 'Gold') ? "#ffea00" : "")));
                    $fontColor2 = ($a->grade_round2=='red')?'#ffffff':(($a->grade_round2!='red')?'#000000':'');
                    $colorBackground3 = (($a->grade_round3 == 'red') ? "#ff0000" :(($a->grade_round3 == 'LawnGreen') ? "#40ff00" :(($a->grade_round3 == 'Gold') ? "#ffea00" : "")));
                    $fontColor3 = ($a->grade_round3=='red')?'#ffffff':(($a->grade_round3!='red')?'#000000':'');
                    $colorBackground4 = (($a->grade_round4 == 'red') ? "#ff0000" :(($a->grade_round4 == 'LawnGreen') ? "#40ff00" :(($a->grade_round4 == 'Gold') ? "#ffea00" : "")));
                    $fontColor4 = ($a->grade_round4=='red')?'#ffffff':(($a->grade_round4!='red')?'#000000':'');
                    $colorBackground5 = (($a->grade_round5 == 'red') ? "#ff0000" :(($a->grade_round5 == 'LawnGreen') ? "#50ff00" :(($a->grade_round5 == 'Gold') ? "#ffea00" : "")));
                    $fontColor5 = ($a->grade_round5=='red')?'#ffffff':(($a->grade_round5!='red')?'#000000':'');
                    $colorBackground6 = (($a->grade_round6 == 'red') ? "#ff0000" :(($a->grade_round6 == 'LawnGreen') ? "#50ff00" :(($a->grade_round6 == 'Gold') ? "#ffea00" : "")));
                    $fontColor6 = ($a->grade_round6=='red')?'#ffffff':(($a->grade_round6!='red')?'#000000':'');
                    $colorBackground7 = (($a->grade_round7 == 'red') ? "#ff0000" :(($a->grade_round7 == 'LawnGreen') ? "#50ff00" :(($a->grade_round7 == 'Gold') ? "#ffea00" : "")));
                    $fontColor7 = ($a->grade_round7=='red')?'#ffffff':(($a->grade_round7!='red')?'#000000':'');
                    $colorBackground8 = (($a->grade_round8 == 'red') ? "#ff0000" :(($a->grade_round8 == 'LawnGreen') ? "#50ff00" :(($a->grade_round8 == 'Gold') ? "#ffea00" : "")));
                    $fontColor8 = ($a->grade_round8=='red')?'#ffffff':(($a->grade_round8!='red')?'#000000':'');
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, ucwords($a->sewer_name));
                    $sheet->setCellValue('D'.$row, $a->sewer_nik);
                    $sheet->cell('D'.$row,function($cell)
                    {
                        $cell->setAlignment('left');
                    });
                    $sheet->setCellValue('E'.$row, strtoupper($a->component)); 
                    $sheet->setCellValue('F'.$row, strtoupper($a->process_name));
                    $sheet->setCellValue('G'.$row, strtoupper($a->smv));
                    $sheet->setCellValue('H'.$row, strtoupper($a->machine_name)); 
                    $sheet->setCellValue('I'.$row, $a->style); 
                    $sheet->setCellValue('J'.$row, $a->round1);
                    $sheet->cell('J'.$row, function($cell) use($colorBackground1,$fontColor1)
                    {
                        $cell->setBackground($colorBackground1);
                        $cell->setFontColor($fontColor1);
                    }); 
                    $sheet->setCellValue('K'.$row, $a->round2);
                    $sheet->cell('K'.$row, function($cell) use($colorBackground2,$fontColor2)
                    {
                        $cell->setBackground($colorBackground2);
                        $cell->setFontColor($fontColor2);
                    });
                    $sheet->setCellValue('L'.$row, $a->round3);
                    $sheet->cell('L'.$row, function($cell) use($colorBackground3,$fontColor3)
                    {
                        $cell->setBackground($colorBackground3);
                        $cell->setFontColor($fontColor3);
                    });
                    $sheet->setCellValue('M'.$row, $a->round4);
                    $sheet->cell('M'.$row, function($cell) use($colorBackground4,$fontColor4)
                    {
                        $cell->setBackground($colorBackground4);
                        $cell->setFontColor($fontColor4);
                    });
                    $sheet->setCellValue('N'.$row, $a->round5);
                    $sheet->cell('N'.$row, function($cell) use($colorBackground5,$fontColor5)
                    {
                        $cell->setBackground($colorBackground5);
                        $cell->setFontColor($fontColor5);
                    });
                    $sheet->setCellValue('O'.$row, $a->round6);
                    $sheet->cell('O'.$row, function($cell) use($colorBackground6,$fontColor6)
                    {
                        $cell->setBackground($colorBackground6);
                        $cell->setFontColor($fontColor6);
                    });
                    $sheet->setCellValue('P'.$row, $a->round7);
                    $sheet->cell('P'.$row, function($cell) use($colorBackground7,$fontColor7)
                    {
                        $cell->setBackground($colorBackground7);
                        $cell->setFontColor($fontColor7);
                    });
                    $sheet->setCellValue('Q'.$row, $a->round8);
                    $sheet->cell('Q'.$row, function($cell) use($colorBackground8,$fontColor8)
                    {
                        $cell->setBackground($colorBackground8);
                        $cell->setFontColor($fontColor8);
                    });
                    $sheet->setCellValue('R'.$row, $a->total_round);
                    $sheet->setCellValue('S'.$row, $a->total_score);
                    $sheet->setCellValue('T'.$row, $a->grade);
                    $row++;
                }
                
                $sheet->setColumnFormat(array(
                    'A' => 'yyyy-mm-dd',
                    'B' => '@',
                    'D' => '@',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
}