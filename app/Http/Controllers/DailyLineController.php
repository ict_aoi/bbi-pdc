<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Mqtt;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\Setting;
use App\Models\DailyLine;
use App\Models\AndonSupply;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\QcOutputView;
use App\Models\DailyLineView;
use App\Models\DailyLineDetail;


class DailyLineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $setting        = Setting::whereNull('delete_at')->where('name','websocket')->first();
        $ip_websocket   = $setting?$setting->value1:0;
        $port_websocket = $setting?$setting->value2:0;
        $factory        = Factory::active()->pluck('name','id')->all();
        $factory_id     = Auth::user()->factory_id;
        return view('daily_line.index',compact('ip_websocket','port_websocket','factory','factory_id'));
    }
    public function data(Request $request)
    {
       
    	if(request()->ajax())
        {
            $data = DailyLineView::where('line_id',$request->line_id)
            ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'))
            ->orderBy('delete_at','desc');
            
            return datatables()->of($data)
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('smv',function($data){
                return $data->smv;
            })
            ->editColumn('commitment_line',function($data){
                return $data->commitment_line;
            })
            ->editColumn('change_over',function($data){
                return strtoupper($data->change_over);
            })
            ->editColumn('cumulative_day',function($data){
                return $data->day;
            })
            ->editColumn('present_sewer',function($data){
                return $data->present_sewer;
            })
            ->addColumn('action', function($data) {
                return view('daily_line._action', [
                    'model' => $data,
                    'edit' => route('daily_line.edit',$data->id),
                    'delete' => route('daily_line.destroy',$data->id),
                ]);
            })
            ->setRowAttr([
                'style' => function($data) 
                {
                    if(!$data->delete_at) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['action','style'])
            ->make(true);
        }
    }
    function linePicklist(Request $request,$factory_id)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $buyer = $this->Buyer();
            $date = Carbon::now();
            $lists = Line::select('lines.id','lines.name','lines.buyer',db::raw('daily_workings.id as daily_working_id'))
            ->leftjoin(
                DB::raw('(select * from daily_workings  where date(created_at)=' . "'$date'" . ')
                daily_workings'),
                function ($leftjoin) {
                    $leftjoin->on('daily_workings.line_id', '=', 'lines.id');
                }
            )
            ->whereNull('lines.delete_at')
            ->whereIn('lines.buyer',$buyer)
            ->where([
                ['lines.factory_id',$factory_id],
                ['lines.code','<>','all'],
                [db::raw('lower(lines.alias)'), 'LIKE', "%$q%"],
            ])
            ->orderBy('lines.order','asc')
            ->paginate(10);

            return view('daily_line._line_picklist', compact('lists'));
        }
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    static function getDailyWorking(Request $request)
    {
        if ($request->ajax()) {
            $line_id = $request->line_id;
            $dailyWorking = DailyWorking::whereNull('delete_at')
            ->where('line_id',$line_id)
            ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'))
            ->first();
            $id            = ($dailyWorking?$dailyWorking->id:null);
            $start         = ($dailyWorking?$dailyWorking->start:null);
            $end           = ($dailyWorking?$dailyWorking->end:null);
            $working_hours = ($dailyWorking?$dailyWorking->working_hours:null);
            $recess        = ($dailyWorking?$dailyWorking->recess:null);
            
            return response()->json(['id'=>$id,'start' => $start, 'end'=>$end,'working_hours'=>$working_hours,'recess'=>$recess], 200);

        }
    }
    static function addDailyWorking(Request $request)
    {
        if ($request->ajax()) {
            $line_id = $request->line_id;
            $data = DailyWorking::whereNull('delete_at')
            ->where('line_id',$line_id)
            ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'))
            ->first();
            $obj                = new StdClass();
            $obj->start         = ($data?date("g:i a", strtotime($data->start)):'07:00');
            $obj->end           = ($data?$data->end:'16:00');
            $obj->id            = ($data?$data->id:null);
            $obj->working_hours = ($data?$data->working_hours:null);
            $obj->recess        = ($data?$data->recess:'12:00');
            $obj->line_id       = $line_id;
            return view('daily_line._add_daily_working', compact('obj'));
        }
    }
    public function storeDailyWorking(Request $request)
    {
        if ($request->ajax()) {
            $startTime    = Carbon::parse($request->start);
            $finishTime   = Carbon::parse($request->end);
            $workingHours = ($finishTime->diffInHours($startTime)>5?$finishTime->diffInHours($startTime)-1:$finishTime->diffInHours($startTime));
            if ($workingHours<1) {
                return response()->json(['message' => 'Jam Kerja salah, Silahkan cek kembali'], 422);
            }
            try {
                DB::beginTransaction();
                if (!$request->id) {
                    $dailyWorking = DailyWorking::FirstOrCreate([
                        'line_id'        => $request->line_id,
                        'start'          => $request->start,
                        'end'            => $request->end,
                        'recess'         => $request->recess,
                        'working_hours'  => $workingHours,
                        'created_at'     => Carbon::now(),
                        'delete_at'      => null,
                        'create_user_id' => Auth::user()->id
                    ]);
                    $id = $dailyWorking->id;
                }else{
                    $updateDaily                 = DailyWorking::findOrFail($request->id);
                    $updateDaily->start          = $request->start;
                    $updateDaily->end            = $request->end;
                    $updateDaily->recess         = $request->recess;
                    $updateDaily->working_hours  = $workingHours;
                    $updateDaily->updated_at     = Carbon::now();
                    $updateDaily->update_user_id = Auth::user()->id;
                    $updateDaily->save();
                    $id = $request->id;
                }
                $obj                = new StdClass();
                $obj->start         = date("g:i A", strtotime($request->start));
                $obj->end           = date("g:i A", strtotime($request->end));
                $obj->recess        = date("g:i A", strtotime($request->recess));
                $obj->working_hours = $workingHours;
                $obj->id            = $id;
                DB::commit();
                return response()->json(['success'=> 'success','obj'=>$obj],200);
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    static function stylePicklist(Request $request)
    {
        if ($request->ajax()) {
            $line_id = $request->line_id;
            $q = strtoupper($request->q);
            $lists = DB::table('qc_output_v')
            ->select(db::raw('qc_output_v.style_merge as style'),'workflows.smv')
            ->leftJoin('workflows','workflows.style','qc_output_v.style_merge')
            ->where('line_id',$line_id)
            ->where(function($query) use($q)
            {
                if ($q==NULL) {
                    $query->whereRaw('balance<0');
                }else{
                    $query->where('qc_output_v.style_merge','like','%'.$q.'%');
                }
                
            })
            ->groupBy('qc_output_v.style_merge','workflows.smv')
            ->paginate(10);
            return view('daily_line._style_picklist', compact('lists'));
        }
    }
    
    public function create(Request $request)
    {
        $line_id        = $request->line_id;
        return view('daily_line._create',compact('line_id'));
    }
    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                $dailyWorking = DailyWorking::find($request->daily_working_id);

                $cekDaily = DailyLine::whereNull('delete_at')
                ->where('line_id',$dailyWorking->line_id)
                ->where('style',$request->style)
                ->where('change_over',$request->change_over)
                ->first();
                // dd(!$cekDaily);
                DB::beginTransaction();
                    if (!$cekDaily) {
                        $dailyLine = DailyLine::FirstOrCreate([
                            'line_id'     => $dailyWorking->line_id,
                            'style'       => $request->style,
                            'change_over' => $request->change_over,
                            'smv'         => $request->smv,
                            'delete_at'   => null
                        ]);
                        $cekDaily = $dailyLine;
                        $count    = 0;
                        DB::table('daily_lines')
                        ->where('id','<>',$cekDaily->id)
                        ->whereNull('delete_at')
                        ->where('line_id',$dailyWorking->line_id)
                        ->where('style',$request->style)
                        ->update(['delete_at'=>Carbon::now()->format('Y-m-d')]);
                    }else{
                        $count = DailyLineDetail::whereNull('deleted_at')->where('daily_line_id',$cekDaily->id)->count();
                        $cekDaily->cumulative_day = $count;
                        $cekDaily->save();
                    }
                    $cekDailyDetail = DailyLineDetail::whereNull('deleted_at')
                    ->where('daily_line_id',$cekDaily->id)
                    ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'));
                    if($cekDailyDetail->exists()) return response()->json(['message' => 'Data Sudah Ada'], 422);
                    $dailyLineDetail = DailyLineDetail::Create([
                        'daily_line_id'    => $cekDaily->id,
                        'day'              => $count,
                        'present_sewer'    => $request->present_sewer,
                        'absent_sewer'     => $request->absent_sewer,
                        'present_ipf'      => $request->present_ipf,
                        'commitment_line'  => $request->commitment_line,
                        'daily_working_id' => $request->daily_working_id,
                        'create_user_id'   => Auth::user()->id
                        // 'present_folding'  => $request->present_folding,
                    ]);    
                DB::commit();
                $date      = Carbon::now()->format('Y-m-d');
                $dailyLine = DailyLineView::getStyle($date,$dailyWorking->line_id);
                $style = explode(",",$dailyLine->style);
                return response()->json(['style'=>$style,'line_id'=>$dailyWorking->line_id],200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }
    public function update(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            try {
                
                DB::beginTransaction();
                    $dailyDetail                  = DailyLineDetail::findOrFail($request->id);
                    $dailyDetail->present_sewer   = $request->present_sewer;
                    $dailyDetail->present_ipf     = $request->present_ipf;
                    $dailyDetail->absent_sewer    = $request->absent_sewer;
                    $dailyDetail->commitment_line = $request->commitment_line;
                    $dailyDetail->save();

                    DailyLine::where('id',$dailyDetail->daily_line_id)
                    ->update([
                        'smv'=>$request->smv
                    ]);
                    
                DB::commit();

                $date      = Carbon::now()->format('Y-m-d');
                $dailyLine = DailyLineView::getStyle($date,$request->line_id);
                    
                $style   = explode(",",$dailyLine->style);
                $line_id = $request->line_id;
                return response()->json(['line_id'=>$line_id,'style'=>$style],200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }
    static function edit(Request $request,$id)
    {
        if ($request->ajax()) {
            $data = DailyLineDetail::find($id);
            return view('daily_line._edit', compact('data'));
        }
    }
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $update = DailyLIneDetail::find($id);
            if ($update) {
                $line_id = $update->dailyLine->line_id;
                $date      = Carbon::now()->format('Y-m-d');
                // dd($dailyLine);
                $dailyLine = DailyLineView::getStyle($date,$line_id);

                $update->deleted_at      = Carbon::now();
                $update->delete_user_id = Auth::user()->id;
                $update->save();

                $style     = explode(",",$dailyLine?$dailyLine->style:'');
                return response()->json(['line_id'=>$line_id,'style'=>$style],200);
            }
        }
    }
    public function import(Request $request)
    {
        return view('daily_line.import');
    }
    public function exportFormImport(Request $request)
    {
        $buyer = $this->Buyer();
        $data  = DB::table('qc_output_v')
        ->select(db::raw('qc_output_v.style_merge as style'),'workflows.smv','qc_output_v.line_id','qc_output_v.line_name')
        ->leftJoin('workflows','workflows.style','qc_output_v.style_merge')
        ->whereRaw('balance<0')
        ->whereIn('buyer',$buyer)
        ->groupBy('qc_output_v.style_merge','workflows.smv','qc_output_v.line_id','qc_output_v.line_name')
        ->orderBy('qc_output_v.line_name','asc')
        ->get();
        return Excel::create('Template Harian Line -'.Carbon::now()."_".Carbon::now()->format('u'),function ($excel)use($data){

            $excel->sheet('active', function($sheet) use($data) {
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','NAMA_LINE');
                $sheet->setCellValue('C1','STYLE');
                $sheet->setCellValue('D1','SMV');
                $sheet->setCellValue('E1','COMMITMENT_LINE');
                $sheet->setCellValue('F1','PRESENT_SEWER');
                $sheet->setCellValue('G1','JAM_KERJA_MULAI');
                $sheet->setCellValue('H1','JAM_KERJA_AKHIR');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data as $key => $a) {
                    $row++;
                    $sheet->setCellValue('A'.$row, $a->line_id);
                    $sheet->setCellValue('B'.$row, $a->line_name);
                    $sheet->setCellValue('C'.$row, $a->style);
                    $sheet->setCellValue('D'.$row, $a->smv);
                    $sheet->setCellValue('E'.$row, '');
                    $sheet->setCellValue('F'.$row, '');
                    $sheet->setCellValue('G'.$row, ''); 
                    $sheet->setCellValue('H'.$row, ''); 
                    $objValidation = $sheet->getCell('G'.$row)->getDataValidation();
                    $objValidation2 = $sheet->getCell('H'.$row)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value tidak ada dalam list.');
                    $objValidation->setPromptTitle('Pilih Jam Kerja');
                    $objValidation->setPrompt('Silahkan Pilih Sesuai Dengan List.');
                    $objValidation->setFormula1('working_hours');
                    $objValidation2->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation2->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation2->setAllowBlank(false);
                    $objValidation2->setShowInputMessage(true);
                    $objValidation2->setShowErrorMessage(true);
                    $objValidation2->setShowDropDown(true);
                    $objValidation2->setErrorTitle('Input error');
                    $objValidation2->setError('Value tidak ada dalam list.');
                    $objValidation2->setPromptTitle('Pilih Jam Kerja');
                    $objValidation2->setPrompt('Silahkan Pilih Sesuai Dengan List.');
                    $objValidation2->setFormula1('working_hours');
                    $index++;
                }
                
            
            });
            $excel->sheet('list',function($sheet)
            {
                $sheet->SetCellValue("A1", "JAM KERJA");
                $index     = 0;
                $total_row = 1;
                $workingHours = WorkingHours::get();
                foreach ($workingHours as $key => $value) {
                    $row          = $index + 2;
                    
                    $sheet->setCellValue('A'.$row, strtoupper($value->start));
                    $index++;
                    $total_row++;
                }
              $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet

              $sheet->_parent->addNamedRange(
                 new \PHPExcel_NamedRange(
                    'working_hours', $variantsSheet, 'A2:A'.$total_row // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                 )
              );
              
            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
        
    }
    public function uploadFormImport(Request $request)
    {  
        $array              = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            // dd($data);
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        if ($value->id&&$value->nama_line&&$value->style&&$value->smv&&$value->commitment_line&&$value->present_sewer&&$value->jam_kerja_mulai&&$value->jam_kerja_akhir) {
                            
                            $line            = Line::find($value->id);
                            $nama_line       = strtoupper($value->nama_line);
                            $style           = $value->style;
                            $smv             = $value->smv;
                            $commitment_line = $value->commitment_line;
                            $present_sewer   = $value->present_sewer;
                            $start           = Carbon::parse($value->jam_kerja_mulai);
                            $end             = Carbon::parse($value->jam_kerja_akhir);
                            $buyer           = $this->Buyer();
                            if($line)
                            {
                                $data = DB::table('qc_output_v')
                                ->select(db::raw('qc_output_v.style_merge as style'),'workflows.smv','qc_output_v.line_id','qc_output_v.line_name')
                                ->leftJoin('workflows','workflows.style','qc_output_v.style_merge')
                                ->where('line_id',$line->id)
                                ->whereIn('buyer',$buyer)
                                ->where('qc_output_v.style_merge',$style)
                                ->groupBy('qc_output_v.style_merge','workflows.smv','qc_output_v.line_id','qc_output_v.line_name')
                                ->orderBy('qc_output_v.line_name','asc');
                                
                                if (!$data->exists()) {
                                    $obj                  = new stdClass();
                                    $obj->nama_line       = $nama_line;
                                    $obj->style           = $style;
                                    $obj->smv             = $smv;
                                    $obj->commitment_line = $commitment_line;
                                    $obj->present_sewer   = $present_sewer;
                                    $obj->start           = date("g:i a", strtotime($start));
                                    $obj->end             = date("g:i a", strtotime($end));
                                    $obj->is_error        = true;
                                    $obj->system_log      = 'Style Tidak di Temukan';
                                    $array []             = $obj;
                                }else{
                                    if ($start>$end) {
                                        $obj                  = new stdClass();
                                        $obj->nama_line       = $nama_line;
                                        $obj->style           = $style;
                                        $obj->smv             = $smv;
                                        $obj->commitment_line = $commitment_line;
                                        $obj->present_sewer   = $present_sewer;
                                        $obj->start           = date("g:i a", strtotime($start));
                                        $obj->end             = date("g:i a", strtotime($end));
                                        $obj->is_error        = true;
                                        $obj->system_log      = 'Jam Awal Tidak Boleh Lebih besar';
                                        $array []             = $obj;
                                    }else{
                                        $daily = DailyLineView::whereNull('delete_at')
                                        ->where('line_id',$line->id)
                                        ->where('style',$style)
                                        ->where(db::raw('date(created_at)'),Carbon::now());
                                        if ($daily->exists()) {
                                            $obj                  = new stdClass();
                                            $obj->nama_line       = $nama_line;
                                            $obj->style           = $style;
                                            $obj->smv             = $smv;
                                            $obj->commitment_line = $commitment_line;
                                            $obj->present_sewer   = $present_sewer;
                                            $obj->start           = date("g:i a", strtotime($start));
                                            $obj->end             = date("g:i a", strtotime($end));
                                            $obj->is_error        = true;
                                            $obj->system_log      = 'Style Sudah di input';
                                            $array []             = $obj;
                                        }else{
                                            $cekDailyWorking = DailyWorking::where(db::raw('date(created_at)'),Carbon::now())->where('line_id',$line->id)->first();
                                            $workingHours = ($end->diffInHours($start)>5?$end->diffInHours($start)-1:$end->diffInHours($start));
                                            if (!$cekDailyWorking) {
                                                $dailyWorking = DailyWorking::FirstOrCreate([
                                                    'line_id'        => $line->id,
                                                    'start'          => $start,
                                                    'end'            => $end,
                                                    'working_hours'  => $workingHours,
                                                    'created_at'     => Carbon::now(),
                                                    'delete_at'      => null,
                                                    'create_user_id' => Auth::user()->id
                                                ]);
                                                $cekDailyWorking = $dailyWorking;
                                            }
                                            $cekDaily = DailyLine::whereNull('delete_at')
                                            ->where('line_id',$line->id)
                                            ->where('style',$style)
                                            ->first();
                                            if (!$cekDaily) {
                                                $dailyLine = DailyLine::FirstOrCreate([
                                                    'line_id'     => $line->id,
                                                    'style'       => $style,
                                                    'smv'         => $smv,
                                                    'delete_at'   => null
                                                ]);
                                                $cekDaily = $dailyLine;
                                                $count    = 0;
                                                DB::table('daily_lines')
                                                ->where('id','<>',$cekDaily->id)
                                                ->whereNull('delete_at')
                                                ->where('line_id',$request->line_id)
                                                ->where('style',$request->style)
                                                ->update(['delete_at'=>Carbon::now()->format('Y-m-d')]);
                                            }else{
                                                $count = DailyLineDetail::whereNull('deleted_at')->where('daily_line_id',$cekDaily->id)->count();
                                                $cekDaily->cumulative_day = $count;
                                                $cekDaily->save();
                                            }
                                            $cekDailyDetail = DailyLineDetail::whereNull('deleted_at')
                                            ->where('daily_line_id',$cekDaily->id)
                                            ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'));
                                            if($cekDailyDetail->exists()) return response()->json(['message' => 'Data Sudah Ada'], 422);
                                            $dailyLineDetail = DailyLineDetail::Create([
                                                'daily_line_id'    => $cekDaily->id,
                                                'day'              => $count,
                                                'present_sewer'    => $present_sewer,
                                                'commitment_line'  => $commitment_line,
                                                'daily_working_id' => $cekDailyWorking->id,
                                                'create_user_id'   => Auth::user()->id
                                            ]);

                                            $obj                  = new stdClass();
                                            $obj->nama_line       = $nama_line;
                                            $obj->style           = $style;
                                            $obj->smv             = $smv;
                                            $obj->commitment_line = $commitment_line;
                                            $obj->present_sewer   = $present_sewer;
                                            $obj->start           = date("g:i a", strtotime($start));
                                            $obj->end             = date("g:i a", strtotime($end));
                                            $obj->is_error        = false;
                                            $obj->system_log      = 'Berhasil';
                                            $array []             = $obj;
                                        }
                                    }
                                }
                            }else{
                                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
                            }
                        }

                    }  


                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }
        }
    }
    static function sendAllertDaily()
    {
        $date = Carbon::now();
        $date = Carbon::createFromFormat('Y-m-d H:i:s',  $date)->format('Y-m-d');
        $data = DB::select(db::Raw("
            select  
                tbl_style.date,
                tbl_style.line_id,
                l.name as line_name,
                l.buyer,
                tbl_style.style,
                dv.smv
            from (
            select distinct date(dv.created_at),dv.line_id,s.default as style from daily_line_v dv 
            left join styles s on s.default=dv.style where date(dv.created_at)='$date' and dv.delete_at is null
            union
            select distinct date,line_id,s.default as style from summary_productions sp
            left join styles s on s.erp=sp.style
            where date='$date' and name='output_qc_endline'
            )tbl_style
            LEFT JOIN (
            SELECT DATE
                (created_at) AS DATE,
                line_id,
                style,
                change_over,
                smv,
                working_hours,
                present_sewer,
                commitment_line
            FROM
                daily_line_v 
            WHERE
                DATE ( created_at )='$date' 
                AND delete_at IS NULL
            )dv on dv.date=tbl_style.date and dv.line_id=tbl_style.line_id and dv.style=tbl_style.style
            LEFT JOIN lines l on l.id=tbl_style.line_id
            where dv.smv is null
            ORDER BY l.order
        "));
       
        if (count($data)>0) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';
            $no =1;
            $pesan ='';
            foreach ($data as $key => $d) {
                $pesan .= $no++.'. '.ucwords($d->line_name).'|'.$d->style.'|';
            }
            DB::connection($connection_mid)
            ->table('whatsapp')
            ->insert([
                'contact_number' => 'Digitalisasi PDC AOI 3',
                'is_group' => 't',
                'message' => '*Mohon Bantuannya*|Dear Team IE APT,|Berikut ini line yang Style nya belum di input Komitment / Jam Kerja :|'.$pesan.'|Atas kerjasamanya.|Terimakasih.|_*info lebih lengkap silahkan kunjungi_|http://pdc.bbigarment.co.id/report/monitoring-perjam'
            ]);
        }
    }
    static function getAllertAndon()
    {
        $date      = Carbon::now()->format('Y-m-d');
        $buyer     = 'nagai';
        $factory   = Factory::where('code','bbis')->first();
        $data      = DailyLineView::getDashboardAndon($date,$buyer,$factory->id);
        foreach ($data as $key => $d) {
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $d->created_at)->format('Y-m-d');
            $qcOutput   = QcOutputView::getBalance($created_at,$d->style,$d->line_id);
            if ($qcOutput) {
                $check    = $qcOutput->commitment_line > $qcOutput->balance;
                if ($check) {
                    $this->andonSend($d->code_machine,"1");
                    $checkExist = AndonSupply::whereNull('close_time')
                    ->where([
                        ['line_id',$d->line_id],
                        ['style',$d->style],
                    ])
                    ->exists();
                    if (!$checkExist) {
                        AndonSupply::create([
                            'line_id'          => $d->line_id ,
                            'style'            => $d->style ,
                            'last_loading'     => $qcOutput->qty_loading ,
                            'start_time'       => Carbon::now() ,
                            'create_user_name' => 'admin ict',
                            'create_user_nik'  => '11111111'
                        ]);
                    }
                }
            }
        }
        $dataAndonActive = AndonSupply::whereNull('close_time');
        foreach ($dataAndonActive as $key => $dt) {
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $dt->start_time)->format('Y-m-d');
            $qcOutput   = QcOutputView::getBalance($created_at,$dt->style,$dt->line_id);
            $check      = $qcOutput->commitment_line < $qcOutput->balance;
            if ($check) {
                AndonSupply::where('id',$dt->id)
                ->update([
                    ['close_time',Carbon::now()],
                    ['close_user_name','admin ict'],
                    ['close_user_nik','11111111'],
                ]);
                $line    = Line::find($dt->line_id);
                $this->andonSend($line->code,"0");
            }
        }

    }
    public function SendAlertAndon(Request $request)
    {
        $rules = [
            'line_id' => 'required|string',
            'style' => 'required|array',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $line_id = $request->line_id;
        $style   = $request->style;
        $line    = Line::find($line_id);
        $factory = $line->factory->code;
        $buyer   = $line->buyer;
        $date    = Carbon::now()->format('Y-m-d');
        // echo count($style);
        // if ($line_id&&$style<>'') {
            $qcOutput   = QcOutputView::getBalance($date,$style,$line_id);
            if ($qcOutput) {
                $check    = $qcOutput->safety_stock > $qcOutput->balance;
                
                $dataAndon = AndonSupply::active()
                ->where([
                    ['line_id',$line_id],
                    [db::raw('date(created_at)'),$date]
                ])
                ->first();
                if ($check) {
                    $this->andonSend($line->code,"1");
                    if (!$dataAndon) {
                        AndonSupply::create([
                            'line_id'          => $line_id,
                            'balance'          => $qcOutput->balance,
                            'target_line'      => $qcOutput->commitment_line,
                            'safety_stock'     => $qcOutput->safety_stock,
                            'start_time'       => Carbon::now(),
                            'create_user_name' => Auth::user()->name,
                            'create_user_nik'  => Auth::user()->nik
                        ]);
                    }else{
                        $dataAndon->balance      = $qcOutput->balance;
                        $dataAndon->target_line  = $qcOutput->commitment_line;
                        $dataAndon->safety_stock = $qcOutput->safety_stock;
                        // $dataAndon->close_time = Carbon::now();

                        $dataAndon->save();
                    }
                }else{
                    
                    $this->andonSend($line->code,"0");
                    
                    if ($dataAndon) {
                        $dataAndon->balance         = $qcOutput->balance;
                        $dataAndon->target_line     = $qcOutput->commitment_line;
                        $dataAndon->safety_stock    = $qcOutput->safety_stock;
                        $dataAndon->close_by        = 'ie';
                        $dataAndon->close_user_name = Auth::user()->name;
                        $dataAndon->close_user_nik  = Auth::user()->nik;
                        $dataAndon->close_time      = Carbon::now();

                        $dataAndon->save();
                    }
                }
                $topicTarget = "cmd/$factory/qc_endline/commitment/$line_id";
                $topicAndon  = "cmd/$factory/$buyer/dashboard/andon-supply";

                $obj                  = new StdClass();
                $obj->balance         = $qcOutput->balance;
                $obj->commitment_line = $qcOutput->commitment_line;
                $obj->safety_stock    = $qcOutput->safety_stock;
                $obj->style           = $style;
                
                return response()->json(['msg'=>$obj,'topic' => $topicTarget,'topic_andon' => $topicAndon], 200);
                
            }
        // }
        
    }
    private function andonSend($codeMachine,$value)
    {
        $topic            = 'cmd/andonbbis/group1/123';
        $message          = '{"type":"set_var","payload":{"'.$codeMachine.'":"'.$value.'"}}';
        Mqtt::ConnectAndPublish($topic, $message);
    }
}