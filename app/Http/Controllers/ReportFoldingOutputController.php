<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\Production;

class ReportFoldingOutputController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        return view('report_folding_output.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $poreference = $request->poreference;
            $size        = $request->size;
            $style       = null;
            $article     = null;
            $buyer       = $this->Buyer();
            if ($poreference) {
                list($poreference,$style,$article)= explode("|",$request->poreference);
            }
            if($request->date==null){
                if ($request->poreference==null) {
                    $startDate = Carbon::now()->format('Y-m-d');
                    $endDate   = Carbon::now()->format('Y-m-d');
                } else {
                    $startDate= null;
                    $endDate= null;
                }
            }else{
                list($start,$end) = explode("-",$request->date);
                $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
                $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            }
            $data        = DB::table('folding_history_output_v')
            ->whereIn('buyer',$buyer)
            ->where(function($query) use($poreference,$style,$article,$size)
            {
                if ($poreference!=NULL) {
                    $query = $query->where('poreference',$poreference)
                    ->where('style',$style)
                    ->where('article',$article);
                }
                if ($size!=null) {
                    $query = $query->where('size',$size);
                }
                
            });
            if ($startDate!=null&&$endDate!=null) {
                $data->whereBetween('date',[$startDate,$endDate]);
            }
            $data->orderBy('style');
            $data->orderBy('poreference');
            $data->orderBy('article');
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('qty_loading',function($data){
                return $data->qty_loading;
            })
            ->editColumn('total_scan',function($data){
                return $data->total_scan;
            })
            ->editColumn('total_scan_day',function($data){
                return $data->total_scan_day;
            })
            ->editColumn('balance',function($data){
                $balance = $data->qty_loading-$data->total_scan;
                return $balance;
            })
            // ->rawColumns(['style'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    public function poBuyerPicklist(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtoupper($request->q));
            $buyer = $this->Buyer();
            $data  = Production::select('productions.factory_id','lines.buyer','productions.style','productions.poreference','productions.article')
            ->leftJoin('lines','lines.id','productions.line_id')
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->where(function($query) use ($q){
                $query->where(db::raw('upper(productions.poreference)'),'LIKE',"%$q%")
                ->orWhere(db::raw("upper(productions.article)"),'LIKE',"%$q%")
                ->orWhere(db::raw('upper(productions.style)'),'LIKE',"%$q%");
            })
            ->whereIn('lines.buyer',$buyer)
            ->groupBy('lines.buyer','productions.factory_id','productions.style','productions.poreference','productions.article')
            ->paginate(10);
            return view('report_folding_output._poreference_picklist', compact('data'));
        }
    }
    static function getSize(Request $request)
    {
        if ($request->ajax()) {
            list($poreference,$style,$article)= explode("|",$request->poreference);

            $size = Production::select('production_sizes.size')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->where('productions.poreference',$poreference)
            ->where('productions.style',$style)
            ->where('productions.article',$article)
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->pluck('production_sizes.size','production_sizes.size');
            return response()->json(['size' => $size]);
        }
    }
    public function exportFormFilter(Request $request)
    {
        $poreference = $request->poreference;
        $poreference = $request->poreference;
        $size        = $request->size;
        $buyer       = $this->Buyer();
        $style       = null;
        $article     = null;
        if ($poreference) {
            list($poreference,$style,$article)= explode("|",$request->poreference);
        }
        if($request->date==null){
            if ($request->poreference==null) {
                $startDate = Carbon::now()->format('Y-m-d');
                $endDate   = Carbon::now()->format('Y-m-d');
            } else {
                $startDate= null;
                $endDate= null;
            }
        }else{
            list($start,$end) = explode("-",$request->date);
            $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        }
        
        $data        = DB::table('folding_history_output_v')
        ->whereIn('buyer',$buyer)
        ->where(function($query) use($poreference,$style,$article,$size)
        {
            if ($poreference!=NULL) {
                $query = $query->where('poreference',$poreference)
                ->where('style',$style)
                ->where('article',$article);
            }
            if ($size!=null) {
                $query = $query->where('size',$size);
            }
            
        });
        if ($startDate!=null&&$endDate!=null) {
            $data->whereBetween('date',[$startDate,$endDate]);
        }
        $data->orderBy('style');
        $data->orderBy('poreference');
        $data->orderBy('article');
        return Excel::create('MONITORING FOLDING OUTPUT -'.Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','TANGGAL');
                $sheet->setCellValue('C1','NAMA LINE');
                $sheet->setCellValue('D1','STYLE');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','ARTICLE');
                $sheet->setCellValue('G1','SIZE');
                $sheet->setCellValue('I1','QTY LOADING');
                $sheet->setCellValue('J1','TOTAL SCAN');
                $sheet->setCellValue('K1','TOTAL SCAN DAY');
                $sheet->setCellValue('L1','BALANCE');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $balance = $a->qty_loading-$a->total_scan;
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('C'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->poreference);
                    $sheet->setCellValue('F'.$row, $a->article); 
                    $sheet->setCellValue('G'.$row, $a->size); 
                    $sheet->setCellValue('I'.$row, $a->qty_loading); 
                    $sheet->setCellValue('J'.$row, $a->total_scan);
                    $sheet->setCellValue('K'.$row, $a->total_scan_day);
                    $sheet->setCellValue('L'.$row, $balance);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
}