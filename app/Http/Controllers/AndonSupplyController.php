<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Factory;
use App\Models\Setting;
use App\Models\AndonSupply;
use App\Models\DailyLineView;

class AndonSupplyController extends Controller
{
    public function index(Request $request,$factory,$buyer)
    {
        $setting        = Setting::whereNull('delete_at')->where('name','websocket')->first();
        $ip_websocket   = $setting?$setting->value1:0;
        $port_websocket = $setting?$setting->value2:0;
        $topicTarget    = "cmd/$factory/$buyer/dashboard/andon-supply";
        return view('dashboard_andon.index',compact('buyer','factory','ip_websocket','port_websocket','topicTarget'));
    }
    public function getDataDashboard(Request $request)
    {
        $rules = [
            'buyer'   => 'required|string',
            'factory' => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $buyer          = $request->buyer;
        $codeFactory    = $request->factory;
        $factory        = Factory::whereNull('delete_at')->where('code',$codeFactory)->first();
        $date           = Carbon::now()->format('Y-m-d');
        $datamaxDaily   = DailyLineView::getMaxDaily($date,$buyer,$factory->id);
        // $datamaxHistory = AndonSupply::getMaxHistory($date,$buyer,$factory->id);
        $maxDaily       = $datamaxDaily[0]->max;
        // $maxHistory     = $datamaxHistory[0]->max;
        $data           = DailyLineView::getDashboardAndon($date,$buyer,$factory->id);
        $arrayData      = array();
        foreach ($data as $d) {
            $dataBalance = AndonSupply::getBalanceStyle($date,json_decode($d->style),$d->line_id);
            // dd($dataBalance);
            
            $arrayStyle                  = array();
            foreach ($dataBalance as $b) {
                $objBalance                  = new StdClass();
                $objBalance->style           = $b->style;
                $objBalance->balance_style   = $b->balance;
                $objBalance->commitment_line = $b->commitment_line;
                $objBalance->isNeedSupply    = $b->commitment_line>$b->balance;
                
                $arrayStyle[] = $objBalance;
            }
           /*  $dataAndonNonActive = AndonSupply::NonActive()
                                ->where([
                                    [db::raw('date(created_at)'),$date],
                                    ['line_id',$d->line_id]
                                ])
                                ->whereNull('close_by')
                                ->get();
            $arrayHistory = array();
            foreach ($dataAndonNonActive as $da) {
                $dataHistory = AndonSupply::getHistoryLoading($date,$da->start_time,$da->close_time,$da->line_id,json_decode($d->style));
                // $arrayHistory = array();
                foreach ($dataHistory as $dh) {
                    $objHistory             = new StdClass();
                    $objHistory->style      = $dh->style;
                    $objHistory->start_time = $da->start_time;
                    $objHistory->close_time = $da->close_time;

                    $arrayHistory[] = $objHistory;
                }
                
            } */
            $obj                  = new StdClass();
            $obj->factory_id      = $d->factory_id;
            $obj->line_id         = $d->line_id;
            $obj->alias           = strtoupper($d->code);
            $obj->name            = strtoupper($d->name);
            $obj->commitment_line = $d->commitment_line;
            $obj->safety_stock    = $d->safety_stock;
            $obj->balance         = $d->balance;
            $obj->andon_active    = $d->andon_active;
            $obj->close_time      = $d->close_time;
            $obj->style           = $arrayStyle;
            $obj->count_daily     = $maxDaily?$maxDaily-$d->total:0;
            
            $arrayData []        = $obj;
        }
        // dd($arrayData);
        return response()->json([
            'data' => $arrayData,
        ], 200);
    }
}