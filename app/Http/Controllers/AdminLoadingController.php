<?php namespace App\Http\Controllers;

use DB;
use Mqtt;
use Config;
use StdClass;
use App\Uuid;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Barcode;
use App\Models\Production;
use App\Models\AndonSupply;
use App\Models\QcOutputView;
use App\Models\Distribution;
use App\Models\DailyLineView;
use App\Models\BreakDownSize;
use App\Models\ProductionSize;
use App\Models\DistributionDetail;
use App\Models\ProductionSizeHistory;
use App\Models\DistributionMovementTemp;

use App\Models\Absensi\AbsensiBbi;

class AdminLoadingController extends Controller
{
    public function index(Request $request,$factory,$line)
    {
        $nik            = null;
        $name           = null;
        $subdept_name   = null;
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();

        if($request->session()->has('nik'))
        { 
            $nik            = $request->session()->get('nik');
            $name           = $request->session()->get('name');
            $subdept_name   = $request->session()->get('subdept_name');
        }

        $obj                  = new stdClass();
        $obj->nik             = $nik;
        $obj->name            = $name;
        $obj->factory_id      = ($master_line)?$master_line->factory_id:'';
        $obj->line_id         = ($master_line)?$master_line->id:'';
        $obj->buyer           = ($master_line)?$master_line->buyer:'';
        $obj->alias           = ($master_line)?$master_line->alias:'';
        $obj->is_scan_receive = ($master_line)?$master_line->is_scan_receive:'';
        $obj->factory         = $factory;
        $obj->line            = $line;
        $obj->subdept_name    = $subdept_name;
        $obj->header_name     = (!$nik && $master_line->buyer == 'other'?'FOLDING ('.$master_line->name.')':'ADMIN LOADING ('.strtoupper($master_line->name).')');

        if(!$nik && $master_line->buyer == 'other') return redirect()->route('folding.index',[$factory,$line]); 
        else return view('admin_loading.index',compact('obj'));
    }

    public function store(Request $request,$factory,$line)
    {
        $poreference    = $request->poreference;
        $style          = $request->style;
        $article        = $request->article;
        $factory_id     = $request->factory_id;
        $line_id        = $request->line_id;
        $total_qty      = 0;

        $items          = json_decode($request->breakdown_size_data);
        $inserted_date  = Carbon::now()->todatetimestring();
        
        try 
        {
            DB::beginTransaction();
            
            $is_exists  = Production::getProductionActive($factory_id,$line_id,$poreference,$style,$article);
            $production = $this->inserProduction($is_exists,$factory_id,$line_id,$poreference,$style,$article,$inserted_date,$request);
            
            foreach ($items as $key => $item) 
            {
                if($item->qty_input!= 0 && $item->qty_outstanding != 0)
                {
                    $size               = $item->size;
                    $qty_input          = $item->qty_input;
                    $production_size_id = $item->id;

                    $is_size_exists = ProductionSize::find($production_size_id);

                    $this->insertProductionSize($is_size_exists,$production,$size,$qty_input,$request);
                }
            }

            $production->total_qty = ProductionSize::where('production_id',$production->id)->sum('qty');
            $production->save();

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json(200);
    }

    public function login(Request $request)
    {
        $nik        = trim($request->nik);
        $line       = $request->line;
        $factory    = $request->factory;
        $user       = $this->getAbsence($nik);
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();
        if($user&&$master_line)
        {
            $request->session()->put('nik',$user->nik);
            $request->session()->put('name',$user->name);
            $request->session()->put('factory',$factory);
            $request->session()->put('subdept_name',$user->subdept_name);
            $request->session()->put('line',$line);
            $request->session()->put('line_id',$master_line->id);
            $request->session()->put('factory_id',$master_line->factory_id);
            $request->session()->put('alias',$master_line->alias);

            $url_admin_loading = route('adminLoading.index',[$factory,$line]);
            return response()->json($url_admin_loading,200);

        }else return response()->json('Nik tidak ditemukan di absensi',422);
        
    }

    public function logout(Request $request)
    {
        $factory        = $request->factory;
        $line           = $request->line;
        
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();

        $request->session()->forget(['nik', 'nama','factory','line','job_desc']);

        if($master_line->buyer == 'other') return redirect()->route('folding.index',[$factory,$line]); 
        else return redirect()->route('adminLoading.index',[$factory,$line]);
    }

    public function report(Request $request,$factory,$line)
    { 
        $nik            = null;
        $name           = null;
        $subdept_name   = null;
        
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();

        if($request->session()->has('nik'))
        {
            $nik            = $request->session()->get('nik');
            $name           = $request->session()->get('name');
            $subdept_name   = $request->session()->get('subdept_name');
        }

        $obj                = new stdClass();
        $obj->nik           = $nik;
        $obj->name          = $name;
        $obj->factory_id    = $master_line->factory_id;
        $obj->line_id       = $master_line->id;
        $obj->buyer         = $master_line->buyer;
        $obj->factory       = $factory;
        $obj->line          = $line;
        $obj->subdept_name  = $subdept_name;
        $obj->header_name  = (!$nik && $master_line->buyer == 'other'?'FOLDING (LINE '.$line.')':'ADMIN LOADING ('.strtoupper($master_line->name).')');

        if(!$nik && $master_line->buyer == 'other') return redirect()->route('adminLoading.index',[$factory,$line]); 
        else return view('admin_loading.report.index',compact('obj'));
    }

    public function reportData(Request $request)
    {
        $poreference    = $request->poreference;
        $style          = $request->style;
        $article        = $request->article;
        $page           = $request->page;
        $factory_id     = $request->factory_id;
        $line_id        = $request->line_id;

        if($page == 'print_barcode')
        {
            $data = DB::table('report_adm_loading_v')
            ->select('poreference','article','style')
            ->where([
                ['factory_id',$factory_id],
                ['line_id',$line_id],
            ]);
    
            if($poreference) $data = $data->where('poreference',$poreference);
            if($style) $data = $data->where('style',$style);
            if($article) $data = $data->where('article',$article);
    
            $data = $data->groupby('poreference','article','style')
            ->get();
    
            $array = array();
    
            foreach ($data as $key => $value) 
            {
                $_poreference               = $value->poreference;
                $_article                   = $value->article;
                $_style                     = $value->style;

                $obj                        = new stdClass();
                $obj->poreference           = $_poreference;
                $obj->article               = $_article;
                $obj->style                 = $_style;
                $obj->is_selected           = false;
                $obj->total_line_selected   = 0;
    
                $lines = ProductionSize::whereHas('production',function($query) use ($line_id,$factory_id,$_poreference,$_article,$_style)
                {
                    $query->where([
                        ['factory_id',$factory_id],
                        ['line_id',$line_id],
                        ['poreference',$_poreference],
                        ['article',$_article],
                        ['style',$_style],
                    ]);
                })
                ->get();
    
                $details = array();
                foreach ($lines as $key => $line) 
                {
                    if($line->barcode == null || !$line->barcode || $line->barcode =='')
                    {
                        $barcode    = $this->randomCode();
                        $line->barcode = $barcode;
                        $line->save();
                    }

                    $detail                 = new stdClass();
                    $detail->id             = $line->id;
                    $detail->size           = $line->size;
                    $detail->is_selected    = false;
                    $details []             = $detail;
                }
    
                $obj->total_line    = count($details);
                $obj->details       = $details;
                $array []           = $obj;
            }
        }
        else
        {
            $data = DB::table('report_adm_loading_v')
            ->select('poreference','article','style','qty_order','size')
            ->where([
                ['factory_id',$factory_id],
                ['line_id',$line_id],
            ]);
    
            if($poreference) $data = $data->where('poreference',$poreference);
            if($style) $data = $data->where('style',$style);
            if($article) $data = $data->where('article',$article);
    
            $data = $data->groupby('poreference','article','style','qty_order','size')
            ->get();
    
            $array = array();
    
            foreach ($data as $key => $value) 
            {
                $obj                = new stdClass();
                $obj->poreference   = $value->poreference;
                $obj->article       = $value->article;
                $obj->style         = $value->style;
                $obj->qty_order     = $value->qty_order;
                $obj->size          = $value->size;
    
                $lines = DB::table('report_adm_loading_v')
                ->where([
                    ['poreference',$value->poreference],
                    ['article',$value->article],
                    ['style',$value->style],
                    ['qty_order',$value->qty_order],
                    ['size',$value->size],
                ])
                ->orderby('order','asc')
                ->get();
    
                $details = array();
                foreach ($lines as $key => $line) 
                {
                    $detail                = new stdClass();
                    $detail->first_index   = ($key == 0 ? true : false);
                    $detail->line_name     = $line->line_name;
                    $detail->qty_per_line  = $line->qty_per_line;
                    $details []            = $detail;
                }
    
                $obj->total_line    = count($details);
                $obj->details       = $details;
                $array []           = $obj;
            }
          
        }
        
        return response()->json($array,200);
    }
    public function reportDataWip(Request $request){
        if ($request->ajax()) {
            $line_id     = $request->line_id;
            $poreference = $request->poreference;
            $size        = $request->size;
            $style       = null;
            $article     = null;
            if ($poreference) {
                list($poreference,$style,$article)= explode("|",$request->poreference);
            }
            // dd($request->date==null);
            if ($request->date==null&&$request->poreference==null) {
                $date = Carbon::now()->format('Y-m-d');
                $data    = DB::select(db::Raw("select * from f_wip_line('$line_id','$date')"));
            } else if($request->poreference!=''&&$request->date==null){
                $date ='';
                $data    = DB::select(db::Raw("select * from f_wip_line_style('$line_id','$style','$poreference','$article')where size like '$request->size%'"));
            }else if($request->date!=null&&$request->poreference!=''){
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $data    = DB::select(db::Raw("select * from f_wip_line('$line_id','$date')where style='$style' and poreference='$poreference' and article='$article' and size like '$request->size%'"));
            }else if($request->date!=null||$request->poreference==null){
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $data    = DB::select(db::Raw("select * from f_wip_line('$line_id','$date')"));
            }
            
            return datatables()->of($data)

            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('total_qty_loading',function($data){
                $totalLoading = $this->getTotalLoading($data->production_size_id,$data->date);
                return $totalLoading;
            })
            ->editColumn('qty_loading',function($data){
                return $data->qty_input_loading;
            })
            ->editColumn('total_qc_output',function($data){
                $output_qc = $this->getOutPutQcDaily($data->production_id,$data->production_size_id,$data->date);
                return $output_qc;
            })
            ->editColumn('qc_output',function($data){
                return $data->output_qc;
            })
            ->editColumn('balance',function($data){
                $totalLoading = $this->getTotalLoading($data->production_size_id,$data->date);
                
                return $totalLoading-$data->total_counter;
            })
            ->make(true);
        }
    }
    private function getTotalLoading($production_size_id,$date)
    {
        $data = ProductionSizeHistory::where('production_size_id',$production_size_id)
        ->where(db::raw('date(created_at)'),'<=',$date)
        ->orderBy('created_at','desc')
        ->orderBy('id','desc')->first();
        
        return ($data)?$data->qty_new:0;
    }
    private function getOutPutQcDaily($production_id,$production_size_id,$date)
    {
        $data = DB::select(db::Raw("select * from qc_history_output_v where production_id='$production_id' and production_size_id='$production_size_id' and date<='$date' order by date desc limit 1"));
        
        return ($data==array())?0:$data[0]->total_counter;
    }
    static function getSize(Request $request)
    {
        if ($request->ajax()) {
            list($poreference,$style,$article)= explode("|",$request->poreference);
            $production_id = $request->production_id;

            $size = Production::select('production_sizes.size')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->when($production_id,function($q)use($production_id){
                $q->where('productions.id',$production_id);
            })
            ->when($poreference,function($q)use($poreference){
                $q->where('productions.poreference',$poreference);
            })
            ->when($style,function($q)use($style){
                $q->where('productions.style',$style);
            })
            ->when($article,function($q)use($article){
                $q->where('productions.article',$article);
            })
            ->where('productions.factory_id',$request->session()->get('factory_id'))
            ->where('productions.line_id',$request->session()->get('line_id'))
            ->pluck('production_sizes.size','production_sizes.size');
            return response()->json(['size' => $size]);
        }
    }
    public function printBarcode(Request $request,$factory,$line)
    {
        $nik            = null;
        $name           = null;
        $subdept_name   = null;
        
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();

        if($request->session()->has('nik'))
        {
            $nik            = $request->session()->get('nik');
            $name           = $request->session()->get('name');
            $subdept_name   = $request->session()->get('subdept_name');
        }

        $obj                = new stdClass();
        $obj->nik           = $nik;
        $obj->name          = $name;
        $obj->factory_id    = $master_line->factory_id;
        $obj->line_id       = $master_line->id;
        $obj->buyer         = $master_line->buyer;
        $obj->factory       = $factory;
        $obj->line          = $line;
        $obj->subdept_name  = $subdept_name;
        $obj->header_name  = (!$nik && $master_line->buyer == 'other'?'FOLDING (LINE '.$line.')':'ADMIN LOADING (LINE '.$line.')');

        if(!$nik && $master_line->buyer == 'other') return redirect()->route('adminLoading.index',[$factory,$line]); 
        else return view('admin_loading.print_barcode',compact('obj'));
    }

    public function printout(Request $request,$factory,$line)
    {
        $items  = json_decode($request->laporan_adm_loading_data);
        $array  = array();

        if(isset($items))
        {
            foreach ($items as $key => $item)
            {
               $details = $item->details;
               foreach ($details as $key => $detail) 
               {
                   if($detail->is_selected)
                   {
                       $array [] = $detail->id;
                   }
               }
            }
    
            
            $production_sizes = ProductionSize::whereIn('id',$array)->get();
    
            $pdf = \PDF::loadView('admin_loading.index_a4',
            [
                'data'              => $production_sizes,
                'showbarcode'       => true,
            ])
            ->setPaper('a4', 'portrait');
    
            return $pdf->stream('barcode_print_production_size');
        }
        

        //return view('admin_loading.index_a4',compact('production_sizes'));

    }

    static function randomCode()
    {
        $referral_code = Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        return $referral_code.$sequence;
    }

    static function poreferencePickList(Request $request)
    {
        $q     = trim(strtolower($request->q));
        $line  = Line::whereNull('delete_at')->where('code',$request->session()->get('line'))->first();
        $buyer = $line?$line->buyer:'';
        $lists = BreakDownSize::select('poreference','no_order_nagai',db::raw("COALESCE(style,'-') as style"),db::raw("COALESCE(COALESCE(article,color),'-') as article",'dateorder'))
        ->where(function($query) use ($q){
            $query->where(db::raw('lower(poreference)'),'LIKE',"%$q%")
            ->orWhere(db::raw("COALESCE(COALESCE(article,color),'-')"),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(style)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(no_order_nagai)'),'LIKE',"%$q%");
        })
        // ->when($buyer,function($query)use($buyer){
        //     $query->where(db::raw('buyer'),'LIKE',"%NAGAI%")
        //     ->orWhere(db::raw('lower(style)'),'LIKE',"%LABEN%");
        // })
        ->groupby('poreference','no_order_nagai',db::raw("COALESCE(style,'-')"),db::raw("COALESCE(COALESCE(article,color),'-')"),"datepromise")
        ->orderBy('datepromise','desc')
        ->paginate(10);
        // dd
        return view('admin_loading._poreference_picklist',compact('lists','buyer'));
    }

    static function poreferencePickListPerLine(Request $request)
    {
        $line  = Line::whereNull('delete_at')->where('code',$request->session()->get('line'))->first();
        $buyer = $line?$line->buyer:'';

        $q          = trim(strtolower($request->q));
       
        $lists      = DB::table('report_adm_loading_v')
        ->select('poreference','article','style','no_order_nagai')
        ->where([
            ['factory_id',$request->factory_id],
            ['line_id',$request->line_id],
        ])
        ->where(function($query) use ($q)
        {   
            $query->where(db::raw('lower(poreference)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(style)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(article)'),'LIKE',"%$q%");
        })
        ->groupby('poreference','article','style','no_order_nagai')
        ->paginate(10);
        
        return view('admin_loading._poreference_picklist',compact('lists','buyer'));
    }

    static function linePicklist(Request $request)
    {
        $q          = trim(strtolower($request->q));
        $factory_id = $request->factory_id;

        $lists      = Line::where('factory_id',$factory_id)
        ->where('code','LIKE',"%$q%")
        ->paginate(10);
        
        return view('admin_loading._line_picklist',compact('lists'));
    }

    static function breakDownSizeData(Request $request)
    {
        $poreference    = $request->poreference;
        $style          = $request->style;
        $article        = $request->article;
        $line_id        = $request->line_id;

        $data = DB::select(db::raw("SELECT * FROM get_break_down_size_saldo(
            '".$poreference."',
            '".$style."',
            '".$article."'
            );"
        ));
        
        $array = array();
        foreach ($data as $key => $value) 
        {
            $obj                = new stdClass();
            $production_size    = ProductionSize::whereIn('production_id',function($query) use ($poreference,$style,$article,$line_id)
            {
                $query->select('id')
                ->from('productions')
                ->whereNull('delete_at')
                ->where([
                    ['poreference',$poreference],
                    ['style',$style],
                    ['article',$article],
                    ['line_id',$line_id],
                ]);
            })
            ->where(db::raw('lower(size)'),strtolower($value->size))
            ->first();
            

            $obj->id            = ($production_size ? $production_size->id : -1);
            $obj->production_id = ($production_size ? $production_size->production_id : -1);
            $obj->size          = ($production_size ? $production_size->size : $value->size);
            $obj->qty_order     = $value->qty_order;
            $obj->qty_reserved  = ($production_size ? $production_size->qty : 0);
            $obj->qty_input     = $value->total_not_mapping;
            $array [] = $obj;
        }
        return response()->json($array,200);
        
    }

    static function getAbsence($nik)
    {
        $master = AbsensiBbi::where('nik',$nik)->first();
        if($master)
        {
            $obj                = new stdClass();
            $obj->nik           = $master->nik;
            $obj->name          = strtolower($master->name);
            $obj->subdept_name  = strtolower($master->subdept_name);

            return $obj;
        }else return null;
    }
    public function getOutputQc(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('qc_output_v')->where('production_size_id',$request->size_id)->first();
            $total_output = $data->total_output;

            return response()->json([
                'total_output'  => $total_output,
                'data'          => $request->data,
                'qty'           => $request->qty,
                'i'           => $request->i,

            ], 200);
        }
    }
    public function destroySize(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('qc_output_v')->where('production_size_id',$request->size_id)->first();
            if ($data->total_output==0) {
                try 
                {
                    DB::beginTransaction();
                    ProductionSize::where('id',$request->size_id)->delete();
                    $dataProduction = ProductionSize::where('production_id',$data->production_id)->get();
                    if (count($dataProduction)==0) {
                        Production::where('id',$data->production_id)
                        ->update([
                            'deleted_user_by_nik'  => $request->session()->get('nik'),
                            'deleted_user_by_name' => $request->session()->get('name'),
                            'delete_at'            => Carbon::now(),
                        ]);
                    }
                    DB::commit();
                    return response()->json('success',200);
        
                } catch (Exception $e)
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }else{
                return response()->json(['message'=>'Size Tidak bisa di hapus Karna sudah di input Qc'],422);
            }
        }
    }
    
    public function SendAlertAndon(Request $request)
    {
        $rules = [
            'line_id' => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $line_id   = $request->line_id;
        $date      = Carbon::now()->format('Y-m-d');
        $dataAndon = AndonSupply::active()
        ->where([
            ['line_id',$line_id],
            [db::raw('date(created_at)'),$date]
        ])
        ->first();
        $line      = Line::find($line_id);
        $factory   = $line->factory->code;
        $buyer     = $line->buyer;
        $dailyLine = DailyLineView::getStyle($date,$line_id);
        $style     = explode(",",$dailyLine->style);
        $date      = Carbon::now()->format('Y-m-d');
        $qcOutput  = QcOutputView::getBalance($date,$style,$line_id);
        if ($qcOutput) {
            $check    = $qcOutput->safety_stock > $qcOutput->balance;
            
            $dataAndon = AndonSupply::active()
            ->where([
                ['line_id',$line_id],
                [db::raw('date(created_at)'),$date]
            ])
            ->first();
            if ($check) {
                $this->andonSend($line->code,"1");
                if (!$dataAndon) {
                    AndonSupply::create([
                        'line_id'          => $line_id,
                        'balance'          => $qcOutput->balance,
                        'target_line'      => $qcOutput->commitment_line,
                        'safety_stock'     => $qcOutput->safety_stock,
                        'start_time'       => Carbon::now(),
                        'create_user_name' => $request->session()->get('name'),
                        'create_user_nik'  => $request->session()->get('nik')
                    ]);
                }else{
                    $dataAndon->balance      = $qcOutput->balance;
                    $dataAndon->target_line  = $qcOutput->commitment_line;
                    $dataAndon->safety_stock = $qcOutput->safety_stock;
                    // $dataAndon->close_time = Carbon::now();

                    $dataAndon->save();
                }
            }else{
                
                $this->andonSend($line->code,"0");
                
                if ($dataAndon) {
                    $dataAndon->balance         = $qcOutput->balance;
                    $dataAndon->target_line     = $qcOutput->commitment_line;
                    $dataAndon->safety_stock    = $qcOutput->safety_stock;
                    $dataAndon->close_user_name = $request->session()->get('name');
                    $dataAndon->close_user_nik  = $request->session()->get('nik');
                    $dataAndon->close_time      = Carbon::now();

                    $dataAndon->save();
                }
            }
        }

        /* if ($qcOutput) {
            if ($dataAndon) {
                if ($qcOutput) {
                    $check = $qcOutput->safety_stock>$qcOutput->balance;
                    if ($check) {
                        $this->andonSend($line->code,"0");
                        
                        if ($dataAndon) {
                            $dataAndon->balance         = $qcOutput->balance;
                            $dataAndon->target_line     = $qcOutput->commitment_line;
                            $dataAndon->safety_stock    = $qcOutput->safety_stock;
                            $dataAndon->close_user_name = $request->session()->get('name');
                            $dataAndon->close_user_nik  = $request->session()->get('nik');
                            $dataAndon->close_time      = Carbon::now();
    
                            $dataAndon->save();
                        }
                    }
                    
                }
            }
        } */
        
        $topicTarget       = "cmd/$factory/qc_endline/commitment/$line_id";
        $topicAndon        = "cmd/$factory/$buyer/dashboard/andon-supply";
        $message_andon     = '{"type":"andon_add","payload":"update message"}';
        $messageCommitment = '{"type":"commitment","payload":{"balance":'.$qcOutput->balance.',"commitment_line":'.$qcOutput->commitment_line.',"style":'.json_encode($style).'}}';
        // Mqtt::ConnectAndPublish($topicTarget, $messageCommitment);
        // Mqtt::ConnectAndPublish($topicAndon, $message_andon);
    }
    private function andonSend($codeMachine,$value)
    {
        $topic   = 'cmd/andonbbis/group1/123';
        $message = '{"type":"set_var","payload":{"'.$codeMachine.'":"'.$value.'"}}';
        // $output  = Mqtt::ConnectAndPublish($topic, $message);
    }

    public function ScanDistribusiReceive(Request $request)
    {
        $rules = [
            'barcode' => 'required|string',
        ];
        $data = $request->all();
        $array = [];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $barcode = trim($request->barcode);
        $app_env    = Config::get('app.env');
        // if($app_env == 'live') $connection_cdms = 'cdms_live';
        // else $connection_cdms = 'cdms_dev';

        if ($app_env=='live') {
            $connection_cdms='cdms_live';
            $connection_cdmsnew='cdms_new';
        }else{
            $connection_cdms='cdms_dev';
            $connection_cdmsnew='cdms_newdev';
        }

        

        

        $source = DB::connection($connection_cdmsnew)->select("SELECT * FROM f_barcode_sources('".$barcode."')");
        
        if (empty($source)) {
            return response()->json(['message' => 'Barcode Not Found ! ! !'], 422);
        }else if ($source[0]->sources=="cdms_aoi_3") {
            $lineAlias = $request->session()->get('alias');
            $factoryAlias = $request->session()->get('factory');

            if ($lineAlias==null || $factoryAlias==null) {
                return response()->json(['message' => 'Ada Kesalahan, Silahkan Log out dan Login Kembali'], 422);
            }

            $supplyLine = DB::connection($connection_cdms)
                                        ->table('v_supply_line')
                                        ->where('barcode_id',$barcode)
                                        ->get();

            if(count($supplyLine)==0){
                return response()->json(['message' => 'Barcode tidak di temukan, Info Distribusi'], 422);
            }

            $current_locator_id = $supplyLine[0]->current_locator_id;
            $current_status = $supplyLine[0]->current_status;

            $line_name        = $supplyLine[0]->line_name;
            $factoryAliasCdms = $supplyLine[0]->factory_alias;

            if ($factoryAlias!=strtolower($factoryAliasCdms)) {
                return response()->json(['message' => 'Barcode Belum Scan Supply untuk '.$factoryAliasCdms.' Kembalikan ke distribusi'], 422);
            }

            if ($current_locator_id!=16 && $current_status!='out') {
                return response()->json(['message' => 'Barcode Belum Scan Supply, Info Distribusi'], 422);
            }

            $cekActive = DB::table('distribution_details')
                                ->join('distributions','distribution_details.distribution_id','distributions.id')
                                ->join('lines','distributions.line_id','lines.id')
                                ->where('distribution_details.barcode_id',$barcode)
                                ->whereNull('distribution_details.deleted_at')
                                ->whereNull('distributions.deleted_at')
                                ->whereNull('distributions.date_movement')
                                ->groupBy([
                                    'distribution_details.barcode_id',
                                    'lines.name'
                                ])
                                ->select(
                                    'distribution_details.barcode_id',
                                    'lines.name'
                                )
                                ->first();
            
            if ($cekActive) {
                return response()->json(['message' => 'Barcode sudah discan di '.$cekActive->name], 422);
            }
            
            foreach ($supplyLine as $s) {

                if ($s->set_type=="Non" || $s->set_type==null || $s->set_type=='0') {
                    $wipStyle = $s->style;
                }else{
                    $wipStyle = $s->style."-".substr($s->set_type,0,3);
                }

                $obj     = new StdClass();
                $obj->barcode_id             = $s->barcode_id;
                $obj->style                  = $wipStyle;
                $obj->poreference            = $s->poreference;
                $obj->article                = $s->article;
                $obj->season                 = $s->season;
                $obj->size                   = $s->size;
                $obj->cut_num                = $s->cut_num;
                $obj->start_no               = $s->start_no;
                $obj->end_no                 = $s->end_no;
                $obj->no_sticker             = $s->start_no.'-'.$s->end_no;
                $obj->component_name         = $s->komponen_name;
                $obj->qty                    = $s->qty;
                $obj->style_detail_id        = $s->style_detail_id;
                $obj->source                 = 'cdms_aoi_3';

                $array[]=$obj;
            }

            
        }else if($source[0]->sources=="cdms_new"){

            $lineAlias = $request->session()->get('alias');
            $factoryAlias = ($request->session()->get('factory')=='bbis') ? 'aoi3' : 'bbis';

            if ($lineAlias==null || $factoryAlias==null) {
                return response()->json(['message' => 'Ada Kesalahan, Silahkan Log out dan Login Kembali'], 422);
            }

            $supplyLine = DB::connection($connection_cdmsnew)
                                    ->table('v_supply_line')
                                    ->where('barcode_id',$barcode)
                                    ->get();
            
            if (count($supplyLine)==0) {
                return response()->json(['message' => 'Barcode tidak di temukan, Info Distribusi'], 422);
            }


            $current_locator_id = $supplyLine[0]->current_locator_id;
            $current_status = $supplyLine[0]->current_status;

            $factoryAliasCdms = $supplyLine[0]->factory_alias;

            if ($factoryAlias!=strtolower($factoryAliasCdms)) {
                return response()->json(['message' => 'Barcode Bukan Scan Supply untuk '.$factoryAliasCdms.' Kembalikan ke distribusi'], 422);
            }

            if ($current_locator_id!=3 && $current_status!='out') {
                return response()->json(['message' => 'Barcode Belum Scan Supply, Info Distribusi'], 422);
            }

            $cekActive = DB::table('distribution_details')
                                ->join('distributions','distribution_details.distribution_id','distributions.id')
                                ->join('lines','distributions.line_id','lines.id')
                                ->where('distribution_details.barcode_id',$barcode)
                                ->whereNull('distribution_details.deleted_at')
                                ->whereNull('distributions.deleted_at')
                                ->whereNull('distributions.date_movement')
                                ->groupBy([
                                    'distribution_details.barcode_id',
                                    'lines.name'
                                ])
                                ->select(
                                    'distribution_details.barcode_id',
                                    'lines.name'
                                )
                                ->first();

            if ($cekActive) {
                return response()->json(['message' => 'Barcode sudah discan di '.$cekActive->name], 422);
            }

            foreach ($supplyLine as $s) {
                if ($s->set_type=="Non" || $s->set_type==null || $s->set_type=='0') {
                    $wipStyle = $s->style;
                }else{
                    $wipStyle = $s->style."-".substr($s->set_type,0,3);
                }

                $obj     = new StdClass();
                $obj->barcode_id             = $s->barcode_id;
                $obj->style                  = $wipStyle;
                $obj->poreference            = $s->poreference;
                $obj->article                = $s->article;
                $obj->season                 = $s->season;
                $obj->size                   = $s->size;
                $obj->cut_num                = $s->cut_num;
                $obj->start_no               = $s->start_no;
                $obj->end_no                 = $s->end_no;
                $obj->no_sticker             = $s->start_no.'-'.$s->end_no;
                $obj->component_name         = $s->komponen_name;
                $obj->qty                    = $s->qty;
                $obj->style_detail_id        = $s->style_detail_id;
                $obj->source                 = 'cdms_new';

                $array[]=$obj;
            }
                                

        }

      
        return response()->json(['data'=>$array],200);
    }
    public function storeScan(Request $request)
    {
        $rules = [
            'factory_id'        => 'required|string',
            'line_id'           => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $data_scan  = json_decode($request->scan_loading_data);
        $line_id    = $request->line_id;
        $factory_id = $request->factory_id;
        $factory    = $request->session()->get('factory');
        $app_env    = Config::get('app.env');
        $distList = [];
        $listBarcode = [];
        if ($app_env=='live') {
            $connection_cdms='cdms_live';
            $connection_cdmsnew='cdms_new';
        }else{
            $connection_cdms='cdms_dev';
            $connection_cdmsnew='cdms_newdev';
        }
        
        try {
            DB::beginTransaction();
                foreach ($data_scan as $key => $d) {
                  
                    $cekDist = Distribution::active()->movement()
                                            ->where([
                                                ['factory_id',$factory_id],
                                                ['line_id',$line_id],
                                                ['style',$d->style],
                                                ['poreference',$d->poreference],
                                                ['article',$d->article],
                                                ['season',$d->season],
                                                ['size',$d->size],
                                                ['cut_num',$d->cut_num],
                                                ['start_no',$d->start_no],
                                                ['end_no',$d->end_no],
                                                ['sources',$d->source] 
                                            ])->first();
                    if ($cekDist) {
                        
                        $obj = new StdClass();
                        $obj->id = $cekDist->id;
                        $obj->style= $cekDist->style;
                        $obj->poreference= $cekDist->poreference;
                        $obj->article= $cekDist->article;
                        $obj->season= $cekDist->season;
                        $obj->size= $cekDist->size;
                        $obj->cut_num= $cekDist->cut_num;
                        $obj->start_no= $cekDist->start_no;
                        $obj->end_no= $cekDist->end_no;
                        $obj->qty= $cekDist->qty;
                        $obj->source= $cekDist->sources;
                        $obj->line_id= $line_id;
                        $obj->factory_id= $factory_id;
                        $obj->factory= $factory;
                        $obj->nik= $request->session()->get('nik');
                        $obj->name= $request->session()->get('name');

                        
                        $distID = $cekDist->id;
                        $distList []=$obj;

                    }else{
                        $insert = Distribution::Create([
                                                'factory_id'=>$factory_id,
                                                'line_id'=>$line_id,
                                                'style'=>$d->style,
                                                'poreference'=>$d->poreference,
                                                'article'=>$d->article,
                                                'season'=>$d->season,
                                                'size'=>$d->size,
                                                'cut_num'=>$d->cut_num,
                                                'start_no'=>$d->start_no,
                                                'end_no'=>$d->end_no,
                                                'qty'=>$d->qty,
                                                'sources'=>$d->source
                                                ]);
                        
                        

                        $obj = new StdClass();
                        $obj->id = $insert->id;
                        $obj->style= $insert->style;
                        $obj->poreference= $insert->poreference;
                        $obj->article= $insert->article;
                        $obj->season= $insert->season;
                        $obj->size= $insert->size;
                        $obj->cut_num= $insert->cut_num;
                        $obj->start_no= $insert->start_no;
                        $obj->end_no= $insert->end_no;
                        $obj->qty= $insert->qty;
                        $obj->source= $insert->sources;
                        $obj->line_id= $line_id;
                        $obj->factory_id= $factory_id;
                        $obj->factory= $factory;
                        $obj->nik= $request->session()->get('nik');
                        $obj->name= $request->session()->get('name');

                        $distList []=$obj;
                        $distID = $insert->id;
                    }

                    $cekDetail = DistributionDetail::active()
                                                        ->movement()
                                                        ->where([
                                                            ['distribution_id',$distID],
                                                            ['barcode_id',$d->barcode_id],
                                                            ['component_name',$d->component_name],
                                                            ['style_id_cdms',$d->style_detail_id]
                                                        ])->first();
                    
                    if (!$cekDetail) {
                        DistributionDetail::create([
                                                    'distribution_id'=>$distID,
                                                    'barcode_id'=>$d->barcode_id,
                                                    'component_name'=>$d->component_name,
                                                    'style_id_cdms'=>$d->style_detail_id,
                                                    'create_user_nik'=>$request->session()->get('nik'),
                                                    'create_user_name'=>$request->session()->get('name'),
                                                    'ip_address'=>$request->ip()
                                                ]);
                    }

                    if (!in_array($d->barcode_id."|".$d->source,$listBarcode)) {
                        $listBarcode[] =$d->barcode_id."|".$d->source;
                    }
                    
                    

                }

                
                foreach ($listBarcode as $lb) {
                    $dbs = (explode("|",$lb)[1]=='cdms_aoi_3') ? $connection_cdms : $connection_cdmsnew;
                    self::cdmsFlagsIn(explode("|",$lb)[0],$d->source,$dbs,$request,'in');
                }

                foreach ($distList as $key => $dL) {
                    $dbs = ($dL->source=='cdms_aoi_3') ? $connection_cdms : $connection_cdmsnew;
                    self::CompletedBundle($dL,$dbs,'in');
                }

            DB::commit();
        } catch (Exeption $th) {
            DB::rollback();
            $message = $th->getMessage();
            ErrorHandler::db($message);
        }
    }

    private function cdmsFlagsIn($barcode,$source,$db,$request,$type){

        if ($type=='in') {
            try {
                DB::connection($db)->beginTransaction();
                    if ($source=='cdms_aoi_3') {
                        $locator_id = 16;
                        $state      = 'in';
                        $nik        = ($request->session()->get('factory')=='bbis')?'LINEBBI':($request->session()->get('factory')=='aoi1')?'LINEAOI1':($request->session()->get('factory')=='aoi2')?'LINEAOI2':'';
                        $linename = $request->session()->get('alias');
    
                        $userCdms = DB::connection($db)
                            ->table('users')
                            ->where('nik',$nik)
                            ->first();
    
                        $lastMove = DB::connection($db)
                                                    ->table('distribusi_movements')
                                                    ->where('barcode_id',$barcode)
                                                    ->whereNull('deleted_at')
                                                    ->orderBy('created_at','desc')->first();
                        
                        if ($lastMove->locator_to==$locator_id && $lastMove->status_to!=$state) {
                            $linecd = DB::connection($db)->table('lines')->where('name',$request->session()->get('alias'))->whereNull('deleted_at')->first();
                            DB::connection($db)
                                        ->table('distribusi_movements')
                                        ->insert([
                                            'id'           => \Webpatser\Uuid\Uuid::generate(1)->string,
                                            'barcode_id'   => $barcode,
                                            'locator_from' => ($lastMove)?$lastMove->locator_to:'',
                                            'status_from'  => ($lastMove)?$lastMove->status_to:'',
                                            'locator_to'   => $locator_id,
                                            'status_to'    => $state,
                                            'user_id'      => ($userCdms)?$userCdms->id:'',
                                            'description'  => $state.' SUPPLY LINE '.$request->session()->get('alias'),
                                            'ip_address'   => \Request::ip(),
                                            'created_at'   => Carbon::now(),
                                            'updated_at'   => Carbon::now(),
                                            'line_id'      => $linecd->id
                                        ]);
                            
                            DB::connection($db)
                                        ->table('bundle_detail')
                                        ->where('header_bundle_detail',$barcode)
                                        ->update(['current_locator_id'=>$locator_id,'current_status'=>$state,'updated_at'=>Carbon::now()]);
                            
                            DB::connection($db)
                                        ->table('header_bundle_detail')
                                        ->where('barcode_header',$barcode)
                                        ->update(['current_locator_id'=>$locator_id,'current_status'=>$state,'updated_at'=>Carbon::now()]);
                        }
                        
                    }else{
                        $locator_id = 3;
                        $state = 'in';
                        $status = 'SUPPLY';
                        $nik = '03333333';
                        $factory_id = ($request->session()->get('factory')=='bbis')?'3':($request->session()->get('factory')=='aoi1')?'1':($request->session()->get('factory')=='aoi2')?'2':'';
                        $linealias = $request->session()->get('alias');
                        $line_id = $request->session()->get('line_id');
                        $factoryAlias = $request->session()->get('factory');
    
                        $userCdms = DB::connection($db)->table('users')->where('nik',$nik)->first();
    
                        $lastMove = DB::connection($db)->table('distribusi_movements')
                                                                ->where('barcode_id',$barcode)
                                                                ->whereNull('deleted_at')
                                                                ->where('is_canceled',false)
                                                                ->orderBy('created_at','desc')->first();
                        
                        if ($lastMove->locator_to==$locator_id && $lastMove->status_to!=$state) {
                            DB::connection($db)->table('distribusi_movements')
                                                        ->insert([
                                                            'id'=>\Webpatser\Uuid\Uuid::generate(1)->string,
                                                            'barcode_id'=>$barcode,
                                                            'locator_from'=>$lastMove->locator_to,
                                                            'status_from'=>$lastMove->status_to,
                                                            'locator_to'=>$locator_id,
                                                            'status_to'=>$state,
                                                            'description'=>$status." In ".$linealias,
                                                            'user_id'=>$userCdms->id,
                                                            'ip_address'=>$request->ip(),
                                                            'factory_id'=>(int)$factory_id,
                                                            'created_at'=>Carbon::now(),
                                                            'updated_at'=>Carbon::now(),
                                                            'supply_line'=>$linealias,
                                                            'sewing_received_by'=>$request->session()->get('nik')."|".$request->session()->get('name')
                                                        ]);
                            
                            DB::connection($db)->table('bundle_detail')
                                                            ->where('barcode_group',$barcode)
                                                            ->update([
                                                                'current_locator_id'=>$locator_id,
                                                                'current_status_to'=>$state,
                                                                'current_description'=>$state." ".$status,
                                                                'sewing_received_at'=>Carbon::now(),
                                                                'sewing_received_by'=>$request->session()->get('nik')."|".$factoryAlias,
                                                                'sewing_received_line'=>'LINE '.$linealias
                                                            ]);
                        }
                    }
                DB::connection($db)->commit();
            } catch (Exeption $th) {
                DB::connection($db)->rollback();
                $message = $th->getMessage();
                ErrorHandler::db($message);
            }
        }else if($type=='move'){
            try {
                DB::connection($db)->beginTransaction();
                    if ($source=='cdms_aoi_3') {
                        $locator_id = 16;
                        $state      = 'out';
                        $nik        = ($request->session()->get('factory')=='bbis')?'LINEBBI':($request->session()->get('factory')=='aoi1')?'LINEAOI1':($request->session()->get('factory')=='aoi2')?'LINEAOI2':'';
                        $linename = $request->session()->get('alias');
    
                        $userCdms = DB::connection($db)
                            ->table('users')
                            ->where('nik',$nik)
                            ->first();
    
                        $lastMove = DB::connection($db)
                                                    ->table('distribusi_movements')
                                                    ->where('barcode_id',$barcode)
                                                    ->whereNull('deleted_at')
                                                    ->orderBy('created_at','desc')->first();
                        
                        if ($lastMove->locator_to==$locator_id && $lastMove->status_to!=$state) {
                            $linecd = DB::connection($db)->table('lines')->where('name',$request->session()->get('alias'))->whereNull('deleted_at')->first();
                            DB::connection($db)
                                            ->table('distribusi_movements')
                                            ->insert([
                                                'id'           => \Webpatser\Uuid\Uuid::generate(1)->string,
                                                'barcode_id'   => $barcode,
                                                'locator_from' => ($lastMove)?$lastMove->locator_to:'',
                                                'status_from'  => ($lastMove)?$lastMove->status_to:'',
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => ($userCdms)?$userCdms->id:'',
                                                'description'  => $state.' SUPPLY LINE '.$request->session()->get('alias'),
                                                'ip_address'   => \Request::ip(),
                                                'created_at'   => Carbon::now(),
                                                'updated_at'   => Carbon::now(),
                                                'line_id'      => $linecd->id
                                            ]);
                            
                                DB::connection($db)
                                            ->table('bundle_detail')
                                            ->where('header_bundle_detail',$barcode)
                                            ->update(['current_locator_id'=>$locator_id,'current_status'=>$state,'updated_at'=>Carbon::now()]);
                                
                                DB::connection($db)
                                            ->table('header_bundle_detail')
                                            ->where('barcode_header',$barcode)
                                            ->update(['current_locator_id'=>$locator_id,'current_status'=>$state,'updated_at'=>Carbon::now()]);
                        }
                    }else{
                        $locator_id = 3;
                        $state = 'out';
                        $status = 'SUPPLY';
                        $nik = '03333333';
                        $factory_id = ($request->session()->get('factory')=='bbis')?'3':($request->session()->get('factory')=='aoi1')?'1':($request->session()->get('factory')=='aoi2')?'2':'';
                        $linealias = $request->session()->get('alias');
                        $line_id = $request->session()->get('line_id');
                        $factoryAlias = ($request->session()->get('factory')=='aoi3')?'3':$request->session()->get('factory');
                        
                        $userCdms = DB::connection($db)->table('users')->where('nik',$nik)->first();
    
                        $lastMove = DB::connection($db)->table('distribusi_movements')
                                                                ->where('barcode_id',$barcode)
                                                                ->whereNull('deleted_at')
                                                                ->where('is_canceled',false)
                                                                ->orderBy('created_at','desc')->first();
                        // dd($lastMove->locator_to,$locator_id, $lastMove->status_to,$state,($lastMove->locator_to==$locator_id),( $lastMove->status_to!=$state));
                        if ($lastMove->locator_to==$locator_id && $lastMove->status_to!=$state) {

                           
                            DB::connection($db)->table('distribusi_movements')
                                                        ->insert([
                                                            'id'=>\Webpatser\Uuid\Uuid::generate(1)->string,
                                                            'barcode_id'=>$barcode,
                                                            'locator_from'=>$lastMove->locator_to,
                                                            'status_from'=>$lastMove->status_to,
                                                            'locator_to'=>$locator_id,
                                                            'status_to'=>$state,
                                                            'description'=>$status." out ".$linealias,
                                                            'user_id'=>$userCdms->id,
                                                            'ip_address'=>$request->ip(),
                                                            'factory_id'=>(int)$factory_id,
                                                            'created_at'=>Carbon::now(),
                                                            'updated_at'=>Carbon::now(),
                                                            'sewing_canceled_by'=>$request->session()->get('nik')."|".$request->session()->get('name')
                                                        ]);
                            
                            DB::connection($db)->table('bundle_detail')
                                                            ->where('barcode_group',$barcode)
                                                            ->update([
                                                                'current_locator_id'=>$locator_id,
                                                                'current_status_to'=>$state,
                                                                'current_description'=>$state." ".$status,
                                                                'sewing_received_at'=>null,
                                                                'sewing_received_by'=>null,
                                                                'sewing_received_line'=>null
                                                            ]);
                        }
                    }
                DB::connection($db)->commit();
            } catch (Exception $th) {
                DB::connection($db)->rollback();
            }
        }
        

        return true;
    }

    private function CompletedBundle($data,$db,$type){

 
        if ($type=='in') {
            try {
                DB::beginTransaction();
                    if ($data->source=='cdms_aoi_3') {
    
                   
                        // switch ((explode("-",$data->style)[1])) {
                        //     case 'TOP':
                        //         $setType = "TOP";
                        //         break;
                            
                        //     case 'BOT':
                        //             $setType = "BOTTOM";
                        //             break;
    
                        //     default:
                        //         $setType = "Non";
                        //         break;
                        // }
    
                        if (isset(explode("-",$data->style)[1]) && (explode("-",$data->style)[1])=='TOP') {
                            $setType = "TOP";
                        }else if (isset(explode("-",$data->style)[1]) && (explode("-",$data->style)[1])=='BOT') {
                            $setType = "BOTTOM";
                        }else{
                            $setType = "Non";
                        }
                  
                        $countcomp =  DB::connection($db)->table('v_total_komponen_per_sticker')
                                                                ->where([
                                                                    ['poreference',$data->poreference],
                                                                    ['style',explode("-",$data->style)[0]],
                                                                    ['article',$data->article],
                                                                    ['size',$data->size],
                                                                    ['set_type',$setType],
                                                                    ['cut_num',$data->cut_num],
                                                                    ['start_no',$data->start_no],
                                                                    ['end_no',$data->end_no]
                                                                ])->first()->qty_komponen;
                    }else if($data->source=='cdms_new'){
                        // switch ((explode("-",$data->style)[1])) {
                        //     case 'TOP':
                        //         $setType = "TOP";
                        //         break;
                            
                        //     case 'BOT':
                        //             $setType = "BOTTOM";
                        //             break;
    
                        //     default:
                        //         $setType = "Non";
                        //         break;
                        // }
    
                        if (isset(explode("-",$data->style)[1]) && (explode("-",$data->style)[1])=='TOP') {
                            $setType = "TOP";
                            $styleori = explode("-",$data->style)[0];
                        }else if (isset(explode("-",$data->style)[1]) && (explode("-",$data->style)[1])=='BOT') {
                            $setType = "BOTTOM";
                            $styleori = explode("-",$data->style)[0];
                        }else{
                            $setType = "Non";
                            $styleori = $data->style;
                        }
    
                        // $countcomp =  DB::connection($db)->table('v_supply_line')
                        //                                         ->where([
                        //                                             ['poreference',$data->poreference],
                        //                                             ['style',explode("-",$data->style)[0]],
                        //                                             ['article',$data->article],
                        //                                             ['size',$data->size],
                        //                                             ['set_type',$setType],
                        //                                             ['cut_num',$data->cut_num],
                        //                                             ['start_no',$data->start_no],
                        //                                             ['end_no',$data->end_no]
                        //                                         ])->first()->qty_komponen;

                        $countcomp = DB::connection($db)->select("SELECT * FROM f_qty_komponen('".$data->poreference."', '".$styleori."', '".$data->article."', '".$data->size."', '".$data->season."', '".$data->cut_num."', '".$setType."', ".$data->start_no.", ".$data->end_no.") where qty_komponen is not null");
                        
                        // dd($countcomp);
                    }
    
                    $datascancom = DB::table('distributions')
                                            ->join('distribution_details','distributions.id','distribution_details.distribution_id')
                                            ->where('distributions.id',$data->id)
                                            ->whereNull('distribution_details.deleted_at')
                                            ->whereNull('distributions.deleted_at')
                                            ->whereNull('distributions.date_completed')
                                            ->whereNull('distributions.date_movement')
                                            ->groupBy([
                                                'distributions.id',
                                                'distribution_details.component_name',
                                                'distribution_details.style_id_cdms'
                                            ])
                                            ->select('distributions.id','distribution_details.component_name','distribution_details.style_id_cdms')->get();
                    // dd(count($datascancom)==$countcomp,count($datascancom),$countcomp);
                    if (count($datascancom)==$countcomp[0]->qty_komponen) {
    
              
                        $production = Production::where([
                                                        ['poreference',$data->poreference],
                                                        ['style',$data->style],
                                                        ['article',$data->article],
                                                        ['line_id',$data->line_id],
                                                        ['factory_id',$data->factory_id]
                                                    ])->whereNull('delete_at')->first();
                        
                        if ($production) {
                            $productionID = $production->id;
                        }else{
                            $inProd = Production::FirstOrCreate([
                                                        'factory_id'=>$data->factory_id,
                                                        'line_id'=>$data->line_id,
                                                        'poreference'=>$data->poreference,
                                                        'article'=>$data->article,
                                                        'style'=>$data->style,
                                                        'total_qty'=>0,
                                                        'created_user_by_nik'=>$data->nik,
                                                        'created_user_by_name'=>$data->name,
                                                        'created_at'=>Carbon::now(),
                                                        'updated_at'=>Carbon::now()
                                                    ]);
                            $productionID = $inProd->id;
                        }
    
                        $productionSize = ProductionSize::where([
                                                            ['production_id',$productionID],
                                                            ['size',$data->size]
                                                            ])->whereNull('delete_at')->first();
                        
                        if ($productionSize) {
                            $productionSizeID = $productionSize->id;
                            $oldqty = $productionSize->qty;
                            $newqty = ($productionSize->qty)+($data->qty);
                            ProductionSize::where('id',$productionSizeID)
                                                            ->update([
                                                            'qty'=>$newqty,
                                                            'updated_user_by_nik'=>$data->nik,
                                                            'updated_user_by_name'=>$data->name,
                                                            'updated_at'=>Carbon::now()
                                                            ]);
                            $note = "Size ".$data->size." dirubah menjadi ".$newqty." (qty awal ".$oldqty.") oleh ".$data->name." (".$data->nik.") pada tanggal ".Carbon::now()->format('d/M/Y H:i:s');
                        
                        }else{
                            $oldqty = 0;
                            $newqty = $data->qty;
                            $inProdSz = ProductionSize::FirstOrCreate([
                                                    'production_id'=>$productionID,
                                                    'size'=>$data->size,
                                                    'qty'=>$newqty,
                                                    'created_user_by_nik'=>$data->nik,
                                                    'created_user_by_name'=>$data->name,
                                                    'updated_user_by_nik'=>$data->nik,
                                                    'updated_user_by_name'=>$data->name,
                                                    'created_at'=>Carbon::now(),
                                                    'updated_at'=>Carbon::now()
                                                    ]);
                            $productionSizeID = $inProdSz->id;
                            
                            $note = "Size ".$data->size." dimapping ".$newqty." oleh ".$data->name." (".$data->nik.") pada tanggal ".Carbon::now()->format('d/M/Y H:i:s');
    
                        }
                        // dd($productionID,$productionSizeID);
                        ProductionSizeHistory::FirstOrCreate([
                                                                'production_size_id'=>$productionSizeID,
                                                                'note'=>$note,
                                                                'created_at'=>Carbon::now(),
                                                                'updated_at'=>Carbon::now(),
                                                                'qty_old'=>$oldqty,
                                                                'qty_new'=>$newqty,
                                                                'qty_operator'=>$data->qty
                                                            ]);
                        $totqty = ProductionSize::where('production_id',$productionID)->whereNull('delete_at')->sum('qty');
    
                        Production::where('id',$productionID)->update([
                                                                'total_qty'=>$totqty,
                                                                'updated_user_by_nik'=>$data->nik,
                                                                'updated_user_by_name'=>$data->name
                                                            ]);
                        
                        Distribution::where('id',$data->id)
                                                        ->update([
                                                            'production_id'=>$productionID,
                                                            'production_size_id'=>$productionSizeID,
                                                            'is_completed'=>true,
                                                            'date_completed'=>Carbon::now(),
                                                            'updated_at'=>Carbon::now()
                                                        ]);
                        $messageBalance = '{"type":"balance","payload":{"production_id":"'.$productionID.'","production_size_id":"'.$productionSizeID.'","balance":"'.(0-(int)$data->qty).'"}}';
                        $topicBalance    = "cmd/$data->factory/qc_endline/balance/$data->line_id";
                        // Mqtt::ConnectAndPublish($topicBalance, $messageBalance);
                    }
                                            
                DB::commit();
            } catch (Exception $th) {
                DB::rollback();
                echo ' '.$th->getMessage().'
                ';
            }
        }else if ($type=='move') {
            try {
               DB::beginTransaction();
                    Distribution::where('id',$data->id)->update([
                        'date_movement'=>Carbon::now(),
                        'is_movement'=>true,
                        'updated_at'=>Carbon::now()
                    ]);
                    DistributionDetail::where('distribution_id',$data->id)->update([
                        'move_user_nik'=>$data->nik,
                        'move_user_name'=>$data->name,
                        'is_movement'=>true,
                        'date_movement'=>Carbon::now()
                    ]);

                    $getProdcutinSize = ProductionSize::where('id',$data->production_size_id)->first();

                    if ($getProdcutinSize) {
                        $oldqty = $getProdcutinSize->qty;
                        $newqty = ($getProdcutinSize->qty)-($data->qty);
                        $optqty = 0-($data->qty);
    
                        ProductionSize::where('id',$getProdcutinSize->id)->update([
                            'qty'=>$newqty,
                            'updated_user_by_nik'=>$data->nik,
                            'updated_user_by_name'=>$data->name,
                            'updated_at'=>Carbon::now()
                        ]);
    
                        $getTotal = ProductionSize::where('production_id',$getProdcutinSize->production_id)->sum('qty');
    
                        Production::where('id',$getProdcutinSize->production_id)->update([
                            'updated_user_by_nik'=>$data->nik,
                            'updated_user_by_name'=>$data->name,
                            'total_qty'=>$getTotal,
                            'updated_at'=>Carbon::now()
                        ]);
    
                        ProductionSizeHistory::firstOrCreate([
                            'production_size_id'=>$getProdcutinSize->id,
                            'note'=>"Size ".$data->size." diubah menjadi ".$newqty." (qty awal ".$oldqty.") oleh ".$data->name." (".$data->nik.") pada tanggal ".Carbon::now()->format('d/M/Y H:i:s'),
                            'created_at'=>Carbon::now(),
                            'updated_at'=>Carbon::now(),
                            'qty_old'=>$oldqty,
                            'qty_new'=>$newqty,
                            'qty_operator'=>$optqty
                        ]);
    
                        $messageBalance = '{"type":"balance","payload":{"production_id":"'.$getProdcutinSize->production_id.'","production_size_id":"'.$getProdcutinSize->id.'","balance":"'.(0-(int)$optqty).'"}}';
                        $topicBalance    = "cmd/$data->factory/qc_endline/balance/$data->line_id";
                        // Mqtt::ConnectAndPublish($topicBalance, $messageBalance);
                    }
                    
               DB::commit();
            } catch (Exception $th) {
                DB::rollback();
                echo ' '.$th->getMessage().'
                ';
            }
        }

        return true;
    }

    
    private function inserProduction($is_exists,$factory_id,$line_id,$poreference,$style,$article,$inserted_date,$request)
    {
        if($is_exists)
        {
            $is_exists->updated_at              = $inserted_date;
            $is_exists->updated_user_by_nik     = $request->session()->get('nik');
            $is_exists->updated_user_by_name    = $request->session()->get('name');
            $is_exists->save();
            $production = $is_exists;
        }else
        {
            $production = Production::FirstOrCreate([
                'factory_id'            => $factory_id,
                'line_id'               => $line_id,
                'poreference'           => $poreference,
                'style'                 => $style,
                'article'               => $article,
                'created_at'            => $inserted_date,
                'updated_at'            => $inserted_date,
                'created_user_by_nik'   => $request->session()->get('nik'),
                'created_user_by_name'  => $request->session()->get('name'),
                'updated_user_by_nik'   => $request->session()->get('nik'),
                'updated_user_by_name'  => $request->session()->get('name'),
            ]);
        }
        return $production;
    }
    private function insertProductionSize($is_size_exists,$production,$size,$qty_input,$request)
    {
        if(!$is_size_exists)
        {
            $is_size_exists = ProductionSize::FirstOrCreate([
                'production_id'         => $production->id,
                'size'                  => $size,
                'qty'                   => $qty_input,
                'created_at'            => $production->created_at,
                'updated_at'            => $production->updated_at,
                'created_user_by_nik'   => $request->session()->get('nik'),
                'created_user_by_name'  => $request->session()->get('name'),
                'updated_user_by_nik'   => $request->session()->get('nik'),
                'updated_user_by_name'  => $request->session()->get('name'),
            ]);

            ProductionSizeHistory::create([
                'production_size_id' => $is_size_exists->id,
                'note'               => 'Size '.$is_size_exists->size.' di mapping untuk line '.$is_size_exists->production->line->name.' dengan qty awal '.$is_size_exists->qty.'. dibuat oleh '.$is_size_exists->created_user_by_name.' ('.$is_size_exists->created_user_by_nik.') pada tanggal '.$is_size_exists->created_at->format('d/M/y H:i:s'),
                'qty_old'            => 0,
                'qty_new'            => $is_size_exists->qty,
                'qty_operator'       => $is_size_exists->qty,
                'created_at'         => $is_size_exists->created_at,
                'updated_at'         => $is_size_exists->updated_at,
            ]);
            // $total_qty += $production_size->qty;
        }else
        {
            $old_qty  = $is_size_exists->qty;
            $qtyNew   = $old_qty+$qty_input;
            if ($qty_input<0) {
                $data         = DB::table('qc_output_v')->where('production_size_id',$is_size_exists->id)->first();
                $total_output = $data->total_output;
                $qty_order    = $data->qty_order;
                $balance      = (int)$qty_order+(int)$qty_input;
                // dd($qty_order." ".$qty_input." ".$balance."_".$total_output);
                if ($balance>=$total_output) {
                    $is_size_exists->qty                  = $qtyNew;
                    $is_size_exists->updated_at           = $production->updated_at;
                    $is_size_exists->updated_user_by_nik  = $production->updated_user_by_nik;
                    $is_size_exists->updated_user_by_name = $production->updated_user_by_name;
                    $is_size_exists->save();
                    
                    ProductionSizeHistory::create([
                        'production_size_id' => $is_size_exists->id,
                        'note'               => 'Size '.$is_size_exists->size.' untuk line '.$is_size_exists->production->line->name.' dirubah qtnya menjadi '.$qtyNew.' (qty awal '.$old_qty.'). dirubah oleh '.$production->updated_user_by_name.' ('.$production->updated_user_by_nik.') pada tanggal '.$production->updated_at->format('d/M/y H:i:s'),
                        'qty_old'            => $old_qty,
                        'qty_new'            => $qtyNew,
                        'qty_operator'       => $qty_input,
                        'created_at'         => $production->updated_at,
                        'updated_at'         => $production->updated_at,
                    ]);
                }
            } else {
                $is_size_exists->qty                  = $qtyNew;
                $is_size_exists->updated_at           = $production->updated_at;
                $is_size_exists->updated_user_by_nik  = $production->updated_user_by_nik;
                $is_size_exists->updated_user_by_name = $production->updated_user_by_name;
                $is_size_exists->save();
                
                ProductionSizeHistory::create([
                    'production_size_id' => $is_size_exists->id,
                    'note'               => 'Size '.$is_size_exists->size.' untuk line '.$is_size_exists->production->line->name.' dirubah qtnya menjadi '.$qtyNew.' (qty awal '.$old_qty.'). dirubah oleh '.$production->updated_user_by_name.' ('.$production->updated_user_by_nik.') pada tanggal '.$production->updated_at->format('d/M/y H:i:s'),
                    'qty_old'            => $old_qty,
                    'qty_new'            => $qtyNew,
                    'qty_operator'       => $qty_input,
                    'created_at'         => $production->updated_at,
                    'updated_at'         => $production->updated_at,
                ]);
            }
        }
        return $is_size_exists;
    }
    public function SearchPoreference(Request $request)
    {
        
        if ($request->ajax()) {
            $q      = trim(strtolower($request->q));
            $search = str_replace(" ","%",$q);
            $production = Distribution::select(db::raw("CONCAT(style,'|',poreference,'|',article) as name"),db::raw("CONCAT(style,'|',poreference,'|',article) as id"))
                    ->whereNull('deleted_at')
                    ->where(function($query) use ($q){
                        $query->where(db::raw('lower(poreference)'),'LIKE',"%$q%")
                        ->orWhere(db::raw("article"),'LIKE',"%$q%")
                        ->orWhere(db::raw('lower(style)'),'LIKE',"%$q%");
                    })
                    ->where([
                        ['factory_id',$request->session()->get('factory_id')],
                        ['line_id',$request->session()->get('line_id')],
                    ])
                    ->groupBy('style','poreference','article')->get();
            return response()->json(['production' => $production]);
        }
    }
    public function reportDataBundle(Request $request)
    {
        // dd($request->all());
        $factory = $request->session()->get('factory');
        $line    = $request->session()->get('line');
        if ($request->ajax()) {
            if ($factory!=null & $line != null) {
                if ($request->ajax()) {
                    $date        = $request->date;
                    $poreference = $request->poreference;
                    $style       = $request->style;
                    $article     = $request->article;
                    $size        = $request->size;
                   
                    $data = Distribution::active()
                            ->where([
                                ['line_id',$request->session()->get('line_id')],
                                ['factory_id',$request->session()->get('factory_id')],
                            ])
                            ->when($date,function($q)use($date){
                                $q->where(db::raw('date(created_at)'),$date);
                            })
                            ->when($poreference,function($q)use($poreference){
                                $q->where('poreference',$poreference);
                            })
                            ->when($style,function($q)use($style){
                                $q->where('style',$style);
                            })
                            ->when($article,function($q)use($article){
                                $q->where('article',$article);
                            })
                            ->when($size,function($q)use($size){
                                $q->where('size',$size);
                            })
                            ->orderBy('created_at','desc');
                    
                    return datatables()->of($data)
                    ->editColumn('date',function($data){
                        return $data->created_at;
                    })
                    ->editColumn('style',function($data){
                        return $data->style;
                    })
                    ->editColumn('poreference',function($data){
                        return $data->poreference;
                    })
                    ->editColumn('article',function($data){
                        return $data->article;
                    })
                    ->editColumn('size',function($data){
                        return $data->size;
                    })
                    ->editColumn('cut_num',function($data){
                        return $data->cut_num;
                    })
                    ->editColumn('no_sticker',function($data){
                        return $data->start_no.'-'.$data->end_no;
                    })
                    ->editColumn('qty',function($data){
                        return $data->qty;
                    })
                    ->editColumn('jenis',function($data){
                        $label = ($data->is_movement)?'Pindah':'Terima';
                        $color = ($data->is_movement)?'border-warning text-warning-600':'border-primary text-primary-600';
                        return "<span class='label label-flat label-rounded $color'>".$label."</span>";
                    })
                    ->editColumn('status',function($data){
                        if ($data->is_movement) {
                            $label = ($data->date_movement)?'Komplit':'Dlm Proses';
                            $color = ($data->date_movement)?'border-success text-success-600':'border-grey text-grey-600';
                        } else {
                            $label = ($data->date_completed)?'Komplit':'Dlm Proses';
                            $color = ($data->date_completed)?'border-success text-success-600':'border-grey text-grey-600';
                        }
                        return "<span class='label label-flat label-rounded $color'>".$label."</span>";
                    })
                    ->addColumn('action', function($data)use($line,$factory) {
                        return view('admin_loading._action_bundle', [
                            'model' => $data,
                            'select' => route('adminLoading.get_detail_bundle',[$factory,$line,$data->id]),
                        ]);
                    })
                    ->rawColumns(['jenis','status','action'])
                    ->make(true);
                }
            }
        }
        
    }
    public function getDetailBundle(Request $request,$factory,$line,$id)
    {
        $checkDisitribution = Distribution::find($id);
        if (!$checkDisitribution) {
            return response()->json([
                'status' => 'error',
                'message' => 'id tidak ditemukan'
            ],400);
        }

        $app_env    = Config::get('app.env');
        if($app_env == 'live') $connection_cdms = 'cdms_live';
        else $connection_cdms = 'cdms_dev';

        $data = DB::connection($connection_cdms)
                ->table('v_supply_line')
                ->where([
                    ['style',$checkDisitribution->style],
                    ['poreference',$checkDisitribution->poreference],
                    ['article',$checkDisitribution->article],
                    ['size',$checkDisitribution->size],
                    ['cut_num',$checkDisitribution->cut_num],
                    ['start_no',$checkDisitribution->start_no],
                    ['end_no',$checkDisitribution->end_no],
                ])->paginate(10);
        return view('admin_loading.report._lov_detail_bundle', compact('data','id'));

    }
    static function getSizeDistribution(Request $request)
    {
        if ($request->ajax()) {
            list($poreference,$style,$article)= explode("|",$request->poreference);

            $size = Distribution::select('size')
           
            ->when($poreference,function($q)use($poreference){
                $q->where('poreference',$poreference);
            })
            ->when($style,function($q)use($style){
                $q->where('style',$style);
            })
            ->when($article,function($q)use($article){
                $q->where('article',$article);
            })
            ->where('factory_id',$request->session()->get('factory_id'))
            ->where('line_id',$request->session()->get('line_id'))
            ->groupBy('size')
            ->pluck('size','size');
            return response()->json(['size' => $size]);
        }
    }
    public function storeScanMovement(Request $request)
    {
        $lineAlias    = $request->session()->get('alias');
        $factoryAlias = $request->session()->get('factory');
        
        if ($lineAlias==NULL&&$factoryAlias==NULL) {
            return response()->json(['message' => 'Ada Kesalahan, Silahkan Log out dan Login Kembali'], 422);
        }
        
        $data_scan      = json_decode($request->scan_loading_data);
        $distributionId = [];
        $barcodeId = [];
        $app_env    = Config::get('app.env');

        if ($app_env=='live') {
            $connection_cdms='cdms_live';
            $connection_cdmsnew='cdms_new';
        }else{
            $connection_cdms='cdms_dev';
            $connection_cdmsnew='cdms_newdev';
        }

        try 
        {
            DB::beginTransaction();
                foreach ($data_scan as $ds) {
                    if (!in_array($ds->distribution_id.'|'.$ds->source,$distributionId)) {
                        $distributionId[]=$ds->distribution_id.'|'.$ds->source;
                    }

                }


                foreach ($distributionId as $key => $disid) {
                    $dbs = (explode('|',$disid)[1]=='cdms_aoi_3') ? $connection_cdms : $connection_cdmsnew;
                    $datadistribusi = Distribution::where('id',explode('|',$disid)[0])->first();
                    $datadistribusi->nik = $request->session()->get('nik');
                    $datadistribusi->name = $request->session()->get('name');
                    $datadistribusi->factory = $request->session()->get('factory');
                    $datadistribusi->line_id = $request->session()->get('line_id');
                    self::CompletedBundle($datadistribusi,$dbs,'move');
                    
                    Distribution::where('id',explode('|',$disid)[0])->update([
                        'is_movement'=>true,
                        'date_movement'=>Carbon::now()
                    ]);

                    $Lbarcode = DistributionDetail::where('distribution_id',(explode('|',$disid)[0]))->groupBy('barcode_id')->select('barcode_id')->get();
                        
                    foreach ($Lbarcode as $key => $Lb) {
                        self::cdmsFlagsIn($Lb->barcode_id,explode('|',$disid)[1],$dbs,$request,'move');
                   
                        
                    }
                    
                }

            DB::commit();
            return response()->json('success',200);
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getTempDistributionMovement(Request $request)
    {
        $lineAlias    = $request->session()->get('alias');
        $factoryAlias = $request->session()->get('factory');
        
        if ($lineAlias==NULL&&$factoryAlias==NULL) {
            return response()->json(['message' => 'Ada Kesalahan, Silahkan Log out dan Login Kembali'], 422);
        }
        $dataTemp = DistributionMovementTemp::where('line_id',$request->session()->get('line_id'))->get();
        $array = [];
        foreach ($dataTemp as $key => $temp) {
            $obj                         = new stdClass();
            $obj->distribution_id        = $temp->distribution_id;
            $obj->distribution_detail_id = $temp->distribution_detail_id;
            $obj->barcode_id             = $temp->barcode_id;
            $obj->style                  = $temp->distributionDetail->distribution->style;
            $obj->poreference            = $temp->distributionDetail->distribution->poreference;
            $obj->article                = $temp->distributionDetail->distribution->article;
            $obj->season                 = $temp->distributionDetail->distribution->season;
            $obj->size                   = $temp->distributionDetail->distribution->size;
            $obj->cut_num                = $temp->distributionDetail->distribution->cut_num;
            $obj->start_no               = $temp->distributionDetail->distribution->start_no;
            $obj->end_no                 = $temp->distributionDetail->distribution->end_no;
            $obj->no_sticker             = $temp->distributionDetail->distribution->start_no.'-'.$temp->distributionDetail->distribution->end_no;
            $obj->component_name         = $temp->distributionDetail->component_name;
            $obj->qty                    = $temp->distributionDetail->distribution->qty;
            $obj->style_detail_id        = $temp->distributionDetail->style_id_cdms;
                
                $array []= $obj;
        }
        return response()->json(['data'=>$array],200);
    }
    public function scanDistributionMove(Request $request)
    {
        $rules = [
            'barcode' => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $barcode = $request->barcode;
        
        $lineAlias    = $request->session()->get('alias');
        $factoryAlias = $request->session()->get('factory');
        $line_id    = $request->session()->get('line_id');
        $factory_id    = $request->session()->get('factory_id');
        if ($lineAlias==NULL&&$factoryAlias==NULL) {
            return response()->json(['message' => 'Ada Kesalahan, Silahkan Log out dan Login Kembali'], 422);
        }
        
        $dataDistribution = DB::table('distribution_details')
                                ->join('distributions','distribution_details.distribution_id','distributions.id')
                                ->where('distribution_details.barcode_id',$barcode)
                                ->where('distributions.line_id',$line_id)
                                ->whereNull('distributions.date_movement')
                                ->whereNull('distributions.deleted_at')
                                ->whereNull('distribution_details.deleted_at')
                                ->select(
                                    'distributions.id',
                                    'distributions.factory_id',
                                    'distributions.line_id',
                                    'distributions.production_id',
                                    'distributions.production_size_id',
                                    'distributions.style',
                                    'distributions.poreference',
                                    'distributions.article',
                                    'distributions.season',
                                    'distributions.size',
                                    'distributions.cut_num',
                                    'distributions.start_no',
                                    'distributions.end_no',
                                    'distributions.qty',
                                    'distributions.sources',
                                    'distribution_details.distribution_id',
                                    'distribution_details.barcode_id',
                                    'distribution_details.component_name',
                                    'distribution_details.style_id_cdms'
                                )->get();
        // dd($dataDistribution,$barcode,$line_id);
        try {
            DB::beginTransaction();
                $array = [];

                foreach ($dataDistribution as $key => $dd) {
                    $obj = new StdClass();
                    $obj->id=$dd->id;
                    $obj->factory_id=$dd->factory_id;
                    $obj->line_id=$dd->line_id;
                    $obj->production_id=$dd->production_id;
                    $obj->production_size_id=$dd->production_size_id;
                    $obj->style=$dd->style;
                    $obj->poreference=$dd->poreference;
                    $obj->article=$dd->article;
                    $obj->season=$dd->season;
                    $obj->size=$dd->size;
                    $obj->cut_num=$dd->cut_num;
                    $obj->start_no=$dd->start_no;
                    $obj->end_no=$dd->end_no;
                    $obj->qty=$dd->qty;
                    $obj->sources=$dd->sources;
                    $obj->distribution_id=$dd->distribution_id;
                    $obj->barcode_id=$dd->barcode_id;
                    $obj->component_name=$dd->component_name;
                    $obj->style_detail_id=$dd->style_id_cdms;
                    $obj->no_sticker=$dd->start_no.'-'.$dd->end_no;

                    $array[]=$obj;
                }

                
            DB::commit();
            return response()->json(['data'=>$array],200);
        } catch (Exception $th) {
            DB::rollback();

            echo ' '.$th->getMessage().'
            ';
        }

    }
    
    public function destroyTempDistributionMovement(Request $request)
    {
        $rules = [
            'barcode_id'        => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        
        $barcodeId = $request->barcode_id;
        $dataTemp = DistributionMovementTemp::where([
            ['line_id',$request->session()->get('line_id')],
            ['barcode_id',$barcodeId],
        ])->get();
        if (count($dataTemp)==0) {
            return response()->json(['message' => 'Barcode tidak di temukan'], 422);
        }
        try 
        {
            DB::beginTransaction();
            $distributionId = $dataTemp[0]->distribution_id;
            foreach ($dataTemp as $key => $temp) {
                DistributionDetail::where('id',$temp->distribution_detail_id)
                ->update(array(
                    'is_movement'  => false,
                ));
                DistributionMovementTemp::where('barcode_id',$temp->barcode_id)->delete();
            }
            $checkDistributionDetail = DistributionDetail::active()
                                    ->where([
                                        ['is_movement',true],
                                        ['distribution_id',$distributionId],
                                    ])->get();
            if (count($checkDistributionDetail)==0) {
                $dataDistribution = Distribution::find($distributionId);
                if ($dataDistribution) {
                    if ($dataDistribution->is_movement) {
                        $dataDistribution->is_movement   = false;
                        $dataDistribution->date_movement = null;
                        $dataDistribution->save();

                        $productionSize        = ProductionSize::find($dataDistribution->production_size_id);
                        $production            = Production::find($dataDistribution->production_id);
                        $data                  = $this->insertProductionSize($productionSize,$production,$dataDistribution->size,$dataDistribution->qty,$request);
                        $production->total_qty = ProductionSize::where('production_id',$production->id)->sum('qty');
                        $production->save();

                        $factory = $request->session()->get('factory');
                        $line_id = $request->session()->get('line_id');
                    
                        $messageBalance = '{"type":"balance","payload":{"production_id":"'.$production->id.'","production_size_id":"'.$dataDistribution->production_size_id.'","balance":"'.(0-(int)$dataDistribution->qty).'"}}';
                        $topicBalance   = "cmd/$factory/qc_endline/balance/$line_id";
                        // Mqtt::ConnectAndPublish($topicBalance, $messageBalance);
                    }
                }
            }
            DB::commit();
            // return response()->json(['data'=>$array],200);
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }                    

    }
    public function coba(Request $request)
    {
        $factory        = $request->session()->get('factory');
        $line_id        = $request->session()->get('line_id');
        $line        = $request->session()->get('line');
        $master_line    = Line::where('code',$line)
        ->whereHas('factory',function($query) use ($factory)
        {
            $query->where('code',$factory);
        })
        ->whereNull('delete_at')
        ->first();
        $messageBalance = '{"type":"refresh","payload":{"production_id":"41234a00-9454-11ec-b402-8965adae4713","production_size_id":"4129d380-9454-11ec-9bfb-bde934b68916","balance":"-10"}}';
        // $topicBalance   = "cmd/$factory/qc_endline/balance/$line_id";
        $topicTargetNagai    = "cmd/$factory/nagai/dashboard/andon-supply";
        $topicTargetOther    = "cmd/$factory/other/dashboard/andon-supply";
        /* $data = Line::whereNull('delete_at')->orderBy('order','asc')->get();
        $no=1;
        foreach ($data as $key => $d) {
            $topicBalance = "cmd/bbis/qc_endline/balance/$d->id";
            Mqtt::ConnectAndPublish($topicBalance, $messageBalance);
            $no++;
        }
        echo $no; */
        // Mqtt::ConnectAndPublish($topicTargetNagai, $messageBalance);
        // Mqtt::ConnectAndPublish($topicTargetOther, $messageBalance);
    }
}