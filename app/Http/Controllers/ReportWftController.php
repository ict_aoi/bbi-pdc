<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\QcInline;
use App\Models\Production;
use App\Models\QcEndlineDefect;
use App\Models\SummaryProduction;

Config::set(['excel.export.calculate' => true]);

class ReportWftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('report_wft.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);
            $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');

            $factory_id = Auth::user()->factory_id;
            $data       = DB::select(db::Raw("select 
                date,
                code,
                factory_id,
                line_id,
                line_name,buyer,
                sum(total_process) as total_process,
                sum(output_defect_inline) as output_defect_inline,
                sum(output_defect_endline) as output_defect_endline,
                sum(output_qc_endline) as output_qc_endline
            from f_wft('$startDate','$endDate') where factory_id='$factory_id' GROUP BY
            date,code,factory_id,line_id,line_name,buyer"));
            
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('buyer',function($data){
                return strtoupper($data->buyer);
            })
            ->editColumn('mayor_defect',function($data){
                $mayorDefect = $this->mayorDefectSumLine($data->date,$data->line_id);
                return $mayorDefect;
            })
            ->editColumn('total_defect_inline',function($data){
                return $data->output_defect_inline;
            })
            ->editColumn('total_defect_endline',function($data){
                
                return $data->output_defect_endline;
            })
            ->editColumn('total_inspection',function($data){
                $onrepair = DB::select("SELECT line_id,outdate,sum(qtyrep) as qty FROM f_endline_defect_onrepair('".$data->line_id."','".$data->date."') GROUP BY line_id,outdate");

                return $data->output_qc_endline+ (isset($onrepair[0]->qty) ? $onrepair[0]->qty : 0);
            })
            ->editColumn('wft_inline',function($data){
                $inline = ($data->output_defect_inline>0)?(($data->output_defect_inline)/($data->total_process*5))*100:0;
                return number_format($inline,1,',',',');
            })
            ->editColumn('wft_endline',function($data){
                $endline = ($data->output_defect_endline>0)?(($data->output_defect_endline)/($data->output_qc_endline + $data->output_defect_endline))*100:0;
                return number_format($endline,1,',',',');
            })
            ->editColumn('total_wft',function($data){
                $inline = ($data->output_defect_inline>0)?(($data->output_defect_inline)/($data->total_process*5))*100:0;
                $endline = ($data->output_defect_endline>0)?(($data->output_defect_endline)/($data->output_qc_endline + $data->output_defect_endline))*100:0;
                $total = $inline + $endline;
                return number_format($total,1,',',',');
            })
            ->editColumn('rft',function($data){
                $rft = ($data->output_qc_endline>0)?(($data->output_qc_endline-$data->output_defect_endline)/($data->output_qc_endline))*100:0;
                
                return number_format($rft,1,',',',');;
            })
            // ->rawColumns(['mayor_defect','total_defect_inline','total_defect_endline','total_inspection'])
            ->make(true);
        }
    }
    private function getSumSummaryLine($line_id,$date,$name)
    {
        $data = SummaryProduction::where('name',$name)
        ->where('date',$date)
        ->where('line_id',$line_id)
        ->sum('output');
        return $data;
    }
    private function mayorDefectSumLine($date,$line_id)
    {
        $data = QcEndlineDefect::select('productions.line_id','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'),db::raw('count(*)as total_defect'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('defects','defects.id','qc_endline_defects.defect_id')
        ->where(db::raw('date(qc_endline_defects.created_at)'),$date)
        ->where('line_id',$line_id)
        ->groupBy('productions.line_id','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'))
        ->orderBy(db::raw('count(*)'),'DESC')
        ->orderBy('qc_endline_defects.defect_id','ASC')
        ->first();
        return ($data?$data->name:"-");
    }
    static function mayorDefectSumStyle($date,$line_id,$style)
    {
        $data = QcEndlineDefect::select('productions.line_id','productions.style','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'),db::raw('count(*)as total_defect'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('defects','defects.id','qc_endline_defects.defect_id')
        ->where(db::raw('date(qc_endline_defects.created_at)'),$date)
        ->where('line_id',$line_id)
        ->where('productions.style',$style)
        ->groupBy('productions.line_id','productions.style','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'))
        ->orderBy(db::raw('count(*)'),'DESC')
        ->orderBy('qc_endline_defects.defect_id','ASC')
        ->first();
        return ($data?$data->name:"-");
    }
    public function getGrandTotal(Request $request)
    {
        if ($request->ajax()) {
            list($start,$end) = explode("-",$request->date);

            $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            $factory_id  = Auth::user()->factory_id;
            $data        = DB::select(db::Raw("select 
            sum(total_process) as total_process,
            sum(output_defect_inline) as output_defect_inline,
            sum(output_defect_endline) as output_defect_endline,
            sum(output_qc_endline) as output_qc_endline
            from f_wft('$startDate','$endDate') where factory_id='$factory_id'"));
            
            $result      = $data[0];
            $wft_inline  = ($result->output_defect_inline>0)?(($result->output_defect_inline)/($result->total_process*5))*100:0;
            $wft_endline = ($result->output_defect_endline>0)?(($result->output_defect_endline)/($result->output_qc_endline + $result->output_defect_endline))*100:0;
            $rft         = ($result->output_qc_endline>0)?(($result->output_qc_endline-$result->output_defect_endline)/($result->output_qc_endline))*100:0;
                

            $obj                        = new StdClass();
            $obj->output_defect_inline  = ($result?$result->output_defect_inline:0);
            $obj->output_defect_endline = ($result?$result->output_defect_endline:0);
            $obj->output_qc_endline     = ($result?$result->output_qc_endline:0);
            $obj->wft_inline            = ($result?number_format($wft_inline,1,',',','):0);
            $obj->wft_endline           = ($result?number_format($wft_endline,1,',',','):0);
            $obj->total_wft             = ($result?number_format($wft_inline+$wft_endline,1,',',','):0);
            $obj->rft                   = ($result?number_format($rft,1,',',','):0);
            return response()->json($obj, 200);
        }
    }
    public function exportFormFilterLine(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $data        = DB::table('get_wft_v')
        ->select('date','code','factory_id','line_id','line_name',db::raw('sum(output_defect_inline) as output_defect_inline'),db::raw('sum(output_defect_endline) as output_defect_endline'),db::raw('sum(output_qc_endline) as output_qc_endline'),db::raw('sum(wft_inline) as wft_inline,sum(wft_endline) as wft_endline'),db::raw('sum(total_wft) as total_wft'),db::raw('sum(rft) as rft'))
        ->where('factory_id',Auth::user()->factory_id)
        ->whereBetween('date',[$startDate,$endDate])
        ->groupBy('date','code','factory_id','line_id','line_name');

        
        return Excel::create('Report Line WFT Inline&Endline-'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line Name');
                $sheet->setCellValue('C1','Major Defect');
                $sheet->setCellValue('D1','Sum of TTL Defect Inline/Defect');
                $sheet->setCellValue('E1','Sum of QTY Defect Endline');
                $sheet->setCellValue('F1','Total Inspection');
                $sheet->setCellValue('G1','WFT Inline');
                $sheet->setCellValue('H1','WFT Endline');
                $sheet->setCellValue('I1','Total WFT');
                $sheet->setCellValue('J1','RFT');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data->get() as $key => $a) {
                    $row++;
                    $mayorDefect = $this->mayorDefectSumLine($a->date,$a->line_id);
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, ucwords($mayorDefect));
                    $sheet->setCellValue('D'.$row, $a->output_defect_inline);
                    $sheet->setCellValue('E'.$row, $a->output_defect_endline); 
                    $sheet->setCellValue('F'.$row, $a->output_qc_endline); 
                    $sheet->setCellValue('G'.$row, ($a?number_format($a->wft_inline,1,',',','):0)); 
                    $sheet->setCellValue('H'.$row, ($a?number_format($a->wft_endline,1,',',','):0));
                    $sheet->setCellValue('I'.$row, ($a?number_format($a->total_wft,1,',',','):0));
                    $sheet->setCellValue('J'.$row, ($a?number_format($a->rft,1,',',','):0));
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'A' => 'yyyy-mm-dd',
                    'B' => '@',
                    'D' => '0',
                    'E' => '0',
                    'F' => '0',
                    'G' => '0.00',
                    'H' => '0.00',
                    'I' => '0.00',
                    'J' => '0.00',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    public function exportFormFilterStyle(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $factory_id  = Auth::user()->factory_id;
        $data        = DB::select(db::Raw("select * from f_wft('$startDate','$endDate') where factory_id='$factory_id'"));
        $styleUnion  = QcInline::getStyleUnion($factory_id,$startDate,$endDate);
        $week        = ReportWftController::week_of_today($startDate);
        
        return Excel::create('Report QC Endline & Inline-'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data,$styleUnion,$week){
            $excel->sheet('Summary Per Line', function($sheet) use($data){
                
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line Name');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Major Defect');
                $sheet->setCellValue('E1','Sum of TTL Defect Inline/Defect');
                $sheet->setCellValue('F1','Sum of QTY Defect Endline');
                $sheet->setCellValue('G1','Total Inspection');
                $sheet->setCellValue('H1','WFT Inline');
                $sheet->setCellValue('I1','WFT Endline');
                $sheet->setCellValue('J1','Total WFT');
                $sheet->setCellValue('K1','RFT');

                $position = 1;
                foreach(range('A','K') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#2eceff');
                        $cells->setFontColor('#000000');
                    });
                    $position++;
                }

                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data as $key => $a) {
                    $row++;
                    $mayorDefect = ReportWftController::mayorDefectSumStyle($a->date,$a->line_id,$a->style);

                    $onrepair = DB::select("SELECT line_id,style,outdate,qtyrep FROM f_endline_defect_onrepair('".$a->line_id."','".$a->date."') where style='".$a->style."' ");

                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, strtoupper($a->style));
                    $sheet->setCellValue('D'.$row, ucwords($mayorDefect));
                    $sheet->setCellValue('E'.$row, $a->output_defect_inline);
                    $sheet->setCellValue('F'.$row, $a->output_defect_endline); 
                    $sheet->setCellValue('G'.$row, $a->output_qc_endline+(isset($onrepair[0]->qtyrep) ? $onrepair[0]->qtyrep : 0)); 
                    $sheet->setCellValue('H'.$row, $a->wft_inline); 
                    $sheet->setCellValue('I'.$row, $a->wft_endline);
                    $sheet->setCellValue('J'.$row, '=(H'.$row.'+I'.$row.')');
                    $sheet->setCellValue('K'.$row, $a->rft);
                    $index++;
                    $sheet->setColumnFormat(
                        array(
                            'E'.$row=>'0',
                            'F'.$row=>'0',
                            'G'.$row=>'0',
                            'H'.$row=>'0.00%',
                            'I'.$row=>'0.00%',
                            'J'.$row=>'0.00%',
                            'K'.$row=>'0.00%',
                        )
                    );
                    foreach(range('A','K') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setBackground('#fdf0e9');
                            $cells->setFontColor('#000000');
                        });
                    }
                }       
            });
            
            $excel->sheet('Data Entry', function($sheet) use($styleUnion,$week){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Nama Line');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Code Defect');
                $sheet->setCellValue('E1','Defect');
                $sheet->setCellValue('F1','Mayor Defect');
                $sheet->setCellValue('G1','Round 1');
                $sheet->setCellValue('H1','Round 2');
                $sheet->setCellValue('I1','Round 3');
                $sheet->setCellValue('J1','Round 4');
                $sheet->setCellValue('K1','Round 5');
                $sheet->setCellValue('L1','Round 6');
                $sheet->setCellValue('M1','Round 7');
                $sheet->setCellValue('N1','Round 8');
                $sheet->setCellValue('O1','Qty Defect Endline');
                $sheet->setCellValue('P1','First Good');
                $sheet->setCellValue('Q1','Week');
                $sheet->setCellValue('R1','TTL Defect Inline/Defect');
                $sheet->setCellValue('S1','TTL Defect Inline/Line');
                $sheet->setCellValue('T1','TTL Defect Endline');
                $sheet->setCellValue('U1','Good QTY');
                $sheet->setCellValue('V1','WFT Inline');
                $sheet->setCellValue('W1','WFT Endline');
                $sheet->setCellValue('X1','Total WFT');
                $sheet->setCellValue('Y1','RFT');
                $sheet->setCellValue('Z1','Defect Rate');
                $position = 1;
                foreach(range('A','Z') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#2eceff');
                        $cells->setAlignment('center');
                        $cells->setFontColor('#000000');
                    });
                    $position++;
                }
                $index = 0;
                $array = array();
                $row   = 2;
                foreach ($styleUnion as $key => $a) {
                    $date        = $a->created_at;
                    $getDetail   = DB::select(db::Raw("select * from f_data_entry_line_2('$a->factory_id','$a->line_id','$date','$a->style')"));
                    $mayorDefect = ReportWftController::mayorDefectSumStyle($a->created_at,$a->line_id,$a->style);

                    if ($getDetail) {
                        foreach ($getDetail as $key => $detail) {
                            $goodQty    = $detail->first_good+$detail->total_defect_endline;
                            $rft        = $goodQty>0?$detail->first_good/$goodQty:0;
                            
                            $obj                       = new stdClass();
                            $obj->date                 = $date;
                            $obj->line_name            = $a->line_name;
                            $obj->style                = $a->style;
                            $obj->defect_id            = $detail->defect_id;
                            $obj->defect_name          = $detail->defect_name;
                            $obj->mayor_defect         = $mayorDefect;
                            $obj->qty_defect_inline1   = $detail->qty_defect_inline1;
                            $obj->qty_defect_inline2   = $detail->qty_defect_inline2;
                            $obj->qty_defect_inline3   = $detail->qty_defect_inline3;
                            $obj->qty_defect_inline4   = $detail->qty_defect_inline4;
                            $obj->qty_defect_inline5   = $detail->qty_defect_inline5;
                            $obj->qty_defect_inline6   = $detail->qty_defect_inline6;
                            $obj->qty_defect_inline7   = $detail->qty_defect_inline7;
                            $obj->qty_defect_inline8   = $detail->qty_defect_inline8;
                            $obj->qty_defect_endline   = $detail->qty_defect_endline;
                            $obj->first_good           = $detail->first_good;
                            $obj->week                 = $week;
                            $obj->ttl_defect_inline    = $detail->ttl_defect_inline;
                            $obj->total_defect_inline  = $detail->total_defect_inline;
                            $obj->total_defect_endline = $detail->total_defect_endline;
                            $obj->goodQty              = $goodQty;
                            $obj->wft_inline           = $detail->wft_inline;
                            $obj->wft_endline          = $detail->wft_endline;
                            $obj->totalWft             = $detail->wft_inline+$detail->wft_endline;
                            $obj->rft                  = $rft;
                                
                            $array [] = $obj;
                        }
                    }else{
                        $firstGood                 = ReportWftController::getDefect($a->style,$a->created_at,'output_qc_endline',$a->line_id);
                        $defectInline              = ReportWftController::getDefect($a->style,$a->created_at,'output_defect_inline',$a->line_id);
                        $defectEndline             = ReportWftController::getDefect($a->style,$a->created_at,'output_defect_endline',$a->line_id);
                        $goodQty                   = ($firstGood>0?$firstGood+$defectEndline:0);
                        $wftInline                 = ($goodQty>0?($defectInline/$goodQty):0);
                        $wftEndline                = ($goodQty>0?($defectEndline/$goodQty):0);
                        $totalWft                  = ($goodQty>0?($defectEndline+$defectInline/$goodQty):0);
                        $rft                       = ($goodQty>0?($firstGood/$goodQty):0);

                        $obj                       = new stdClass();
                        $obj->date                 = $date;
                        $obj->line_name            = $a->line_name;
                        $obj->style                = $a->style;
                        $obj->defect_id            = '-';
                        $obj->defect_name          = '-';
                        $obj->mayor_defect         = $mayorDefect;
                        $obj->qty_defect_inline1   = 0;
                        $obj->qty_defect_inline2   = 0;
                        $obj->qty_defect_inline3   = 0;
                        $obj->qty_defect_inline4   = 0;
                        $obj->qty_defect_inline5   = 0;
                        $obj->qty_defect_inline6   = 0;
                        $obj->qty_defect_inline7   = 0;
                        $obj->qty_defect_inline8   = 0;
                        $obj->qty_defect_endline   = 0;
                        $obj->first_good           = $firstGood;
                        $obj->week                 = $week;
                        $obj->ttl_defect_inline    = 0;
                        $obj->total_defect_inline  = 0;
                        $obj->total_defect_endline = $defectEndline;
                        $obj->goodQty              = $goodQty;
                        $obj->wft_inline           = $wftInline;
                        $obj->wft_endline          = $wftEndline;
                        $obj->totalWft             = $totalWft;
                        $obj->rft                  = $rft;

                        $array [] = $obj;
                    }
                        
                    
                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $a->style);
                    $sheet->setCellValue('D'.$row, $a->defect_id);
                    $sheet->setCellValue('E'.$row, $a->defect_name);
                    $sheet->setCellValue('F'.$row, $a->mayor_defect);
                    $sheet->setCellValue('G'.$row, $a->qty_defect_inline1);
                    $sheet->setCellValue('H'.$row, $a->qty_defect_inline2);
                    $sheet->setCellValue('I'.$row, $a->qty_defect_inline3);
                    $sheet->setCellValue('J'.$row, $a->qty_defect_inline4);
                    $sheet->setCellValue('K'.$row, $a->qty_defect_inline5);
                    $sheet->setCellValue('L'.$row, $a->qty_defect_inline6);
                    $sheet->setCellValue('M'.$row, $a->qty_defect_inline7);
                    $sheet->setCellValue('N'.$row, $a->qty_defect_inline8);
                    $sheet->setCellValue('O'.$row, $a->qty_defect_endline);
                    $sheet->setCellValue('P'.$row, $a->first_good);
                    $sheet->setCellValue('Q'.$row, $a->week);
                    $sheet->setCellValue('R'.$row, $a->ttl_defect_inline);
                    $sheet->setCellValue('S'.$row, $a->total_defect_inline);
                    $sheet->setCellValue('T'.$row, $a->total_defect_endline);
                    $sheet->setCellValue('U'.$row, $a->goodQty);
                    $sheet->setCellValue('V'.$row, $a->wft_inline);
                    $sheet->setCellValue('W'.$row, $a->wft_endline);
                    $sheet->setCellValue('X'.$row, $a->totalWft);
                    $sheet->setCellValue('Y'.$row, $a->rft);
                    $sheet->setCellValue('Z'.$row, '=(1-Y'.$row.')');
                    
                    $sheet->setColumnFormat(
                        array(
                            'G'.$row=>'0',
                            'h'.$row=>'0',
                            'I'.$row=>'0',
                            'J'.$row=>'0',
                            'K'.$row=>'0',
                            'L'.$row=>'0',
                            'M'.$row=>'0',
                            'N'.$row=>'0',
                            'O'.$row=>'0',
                            'P'.$row=>'0',
                            'R'.$row=>'0',
                            'S'.$row=>'0',
                            'T'.$row=>'0',
                            'U'.$row=>'0',
                            'V'.$row=>'0.00%',
                            'W'.$row=>'0.00%',
                            'X'.$row=>'0.00%',
                            'Y'.$row=>'0.00%',
                            'Z'.$row=>'0.00%',
                        )
                    );
                    foreach(range('A','Z') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setBackground('#fdf0e9');
                            $cells->setFontColor('#000000');
                        });
                    }
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    static function week_of_today($date)
    {
        $today = $date;
        $month = Carbon::createFromFormat('Y-m-d',$date)->format('m');
        $month = str_pad($month,2,'0',STR_PAD_LEFT);

        $minggu = 0;
        $week_end = 0;

        $last_date =  ReportWftController::last_date_ofthe_month();

        for($i = 1; $i<=$last_date; $i++)
        {
            $i = str_pad($i,2,'0',STR_PAD_LEFT);
            $date =  date("Y-{$month}-{$i}");
            $day  =  date('D', strtotime($date));

            if($day == 'Sat')
            {
            $minggu = $minggu + 1;
            }
            if($date == $today)
            {
            $minggu = $minggu + 1;
            break;

            }
        }
        return $minggu;
    }
    static function last_date_ofthe_month($month='', $year='')
    {
        if(!$year)   $year   = date('Y');
        if(!$month)  $month  = date('m');
        $date = $year.'-'.$month.'-01';

        $next_month = strtotime('+ 1 month', strtotime($date));

        $last_date  = date('d', strtotime('-1 minutes',  $next_month));
        return $last_date;

    }
    static function getDefect($style,$date,$name,$line_id)
    {
        $dataSum = SummaryProduction::where('name',$name)
        ->where('style',$style)
        ->where('date',$date)
        ->where('line_id',$line_id)
        ->sum('output');
        return $dataSum;
    }
    static function exportWeek()
    {
        $location = Config::get('storage.data_entry');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);
        $date       = Carbon::now();
        $startDate  = $date->startOfWeek()->format('Y-m-d');
        $endDate    = $date->endOfWeek()->format('Y-m-d');
        $factory_id = Factory::first()->id;
        $data       = DB::select(db::Raw("select * from f_wft('$startDate','$endDate') where factory_id='$factory_id'"));
        $styleUnion = QcInline::getStyleUnion($factory_id,$startDate,$endDate);
        $week       = ReportWftController::week_of_today($startDate);
        $file_name  = "REPORT QC ENDLINE & IN LINE BC WEEK ".$week.' '.Carbon::createFromFormat('Y-m-d',  $startDate)->format('F, Y');

        return Excel::create($file_name,function ($excel)use($styleUnion,$data,$week){
            $excel->sheet('Summary Per Line', function($sheet) use($data){
                
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line Name');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Major Defect');
                $sheet->setCellValue('E1','Sum of TTL Defect Inline/Defect');
                $sheet->setCellValue('F1','Sum of QTY Defect Endline');
                $sheet->setCellValue('G1','Total Inspection');
                $sheet->setCellValue('H1','WFT Inline');
                $sheet->setCellValue('I1','WFT Endline');
                $sheet->setCellValue('J1','Total WFT');
                $sheet->setCellValue('K1','RFT');

                $position = 1;
                foreach(range('A','K') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#2eceff');
                        $cells->setFontColor('#000000');
                    });
                    $position++;
                }

                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data as $key => $a) {
                    $row++;
                    $mayorDefect = ReportWftController::mayorDefectSumStyle($a->date,$a->line_id,$a->style);
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, strtoupper($a->style));
                    $sheet->setCellValue('D'.$row, ucwords($mayorDefect));
                    $sheet->setCellValue('E'.$row, $a->output_defect_inline);
                    $sheet->setCellValue('F'.$row, $a->output_defect_endline); 
                    $sheet->setCellValue('G'.$row, $a->output_qc_endline+$a->output_defect_endline); 
                    $sheet->setCellValue('H'.$row, $a->wft_inline); 
                    $sheet->setCellValue('I'.$row, $a->wft_endline);
                    $sheet->setCellValue('J'.$row, '=(H'.$row.'+I'.$row.')');
                    $sheet->setCellValue('K'.$row, $a->rft);
                    $index++;
                    $sheet->setColumnFormat(
                        array(
                            'E'.$row=>'0',
                            'F'.$row=>'0',
                            'G'.$row=>'0',
                            'H'.$row=>'0.00%',
                            'I'.$row=>'0.00%',
                            'J'.$row=>'0.00%',
                            'K'.$row=>'0.00%',
                        )
                    );
                    foreach(range('A','K') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setBackground('#fdf0e9');
                            $cells->setFontColor('#000000');
                        });
                    }
                }       
            });
            
            $excel->sheet('Data Entry', function($sheet) use($styleUnion,$week){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Nama Line');
                $sheet->setCellValue('C1','Style');
                $sheet->setCellValue('D1','Code Defect');
                $sheet->setCellValue('E1','Defect');
                $sheet->setCellValue('F1','Mayor Defect');
                $sheet->setCellValue('G1','Round 1');
                $sheet->setCellValue('H1','Round 2');
                $sheet->setCellValue('I1','Round 3');
                $sheet->setCellValue('J1','Round 4');
                $sheet->setCellValue('K1','Round 5');
                $sheet->setCellValue('L1','Round 6');
                $sheet->setCellValue('M1','Round 7');
                $sheet->setCellValue('N1','Round 8');
                $sheet->setCellValue('O1','Qty Defect Endline');
                $sheet->setCellValue('P1','First Good');
                $sheet->setCellValue('Q1','Week');
                $sheet->setCellValue('R1','TTL Defect Inline/Defect');
                $sheet->setCellValue('S1','TTL Defect Inline/Line');
                $sheet->setCellValue('T1','TTL Defect Endline');
                $sheet->setCellValue('U1','Good QTY');
                $sheet->setCellValue('V1','WFT Inline');
                $sheet->setCellValue('W1','WFT Endline');
                $sheet->setCellValue('X1','Total WFT');
                $sheet->setCellValue('Y1','RFT');
                $sheet->setCellValue('Z1','Defect Rate');
                $position = 1;
                foreach(range('A','Z') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#2eceff');
                        $cells->setAlignment('center');
                        $cells->setFontColor('#000000');
                    });
                    $position++;
                }
                $index = 0;
                $array = array();
                $row   = 2;
                foreach ($styleUnion as $key => $a) {
                    $date        = $a->created_at;
                    $getDetail   = DB::select(db::Raw("select * from f_data_entry_line_2('$a->factory_id','$a->line_id','$date','$a->style')"));
                    $mayorDefect = ReportWftController::mayorDefectSumStyle($a->created_at,$a->line_id,$a->style);

                    if ($getDetail) {
                        foreach ($getDetail as $key => $detail) {
                            $goodQty    = $detail->first_good+$detail->total_defect_endline;
                            $rft        = $goodQty>0?$detail->first_good/$goodQty:0;
                            
                            $obj                       = new stdClass();
                            $obj->date                 = $date;
                            $obj->line_name            = $a->line_name;
                            $obj->style                = $a->style;
                            $obj->defect_id            = $detail->defect_id;
                            $obj->defect_name          = $detail->defect_name;
                            $obj->mayor_defect         = $mayorDefect;
                            $obj->qty_defect_inline1   = $detail->qty_defect_inline1;
                            $obj->qty_defect_inline2   = $detail->qty_defect_inline2;
                            $obj->qty_defect_inline3   = $detail->qty_defect_inline3;
                            $obj->qty_defect_inline4   = $detail->qty_defect_inline4;
                            $obj->qty_defect_inline5   = $detail->qty_defect_inline5;
                            $obj->qty_defect_inline6   = $detail->qty_defect_inline6;
                            $obj->qty_defect_inline7   = $detail->qty_defect_inline7;
                            $obj->qty_defect_inline8   = $detail->qty_defect_inline8;
                            $obj->qty_defect_endline   = $detail->qty_defect_endline;
                            $obj->first_good           = $detail->first_good;
                            $obj->week                 = $week;
                            $obj->ttl_defect_inline    = $detail->ttl_defect_inline;
                            $obj->total_defect_inline  = $detail->total_defect_inline;
                            $obj->total_defect_endline = $detail->total_defect_endline;
                            $obj->goodQty              = $goodQty;
                            $obj->wft_inline            = $detail->wft_inline;
                            $obj->wft_endline           = $detail->wft_endline;
                            $obj->totalWft             = $detail->wft_inline+$detail->wft_endline;
                            $obj->rft                  = $rft;
                                
                            $array [] = $obj;
                        }
                    }else{
                        $firstGood                 = ReportWftController::getDefect($a->style,$a->created_at,'output_qc_endline',$a->line_id);
                        $defectInline              = ReportWftController::getDefect($a->style,$a->created_at,'output_defect_inline',$a->line_id);
                        $defectEndline             = ReportWftController::getDefect($a->style,$a->created_at,'output_defect_endline',$a->line_id);
                        $goodQty                   = ($firstGood>0?$firstGood+$defectEndline:0);
                        $wftInline                 = ($goodQty>0?($defectInline/$goodQty):0);
                        $wftEndline                = ($goodQty>0?($defectEndline/$goodQty):0);
                        $totalWft                  = ($goodQty>0?($defectEndline+$defectInline/$goodQty):0);
                        $rft                       = ($goodQty>0?($firstGood/$goodQty):0);

                        $obj                       = new stdClass();
                        $obj->date                 = $date;
                        $obj->line_name            = $a->line_name;
                        $obj->style                = $a->style;
                        $obj->defect_id            = '-';
                        $obj->defect_name          = '-';
                        $obj->mayor_defect         = $mayorDefect;
                        $obj->qty_defect_inline1   = 0;
                        $obj->qty_defect_inline2   = 0;
                        $obj->qty_defect_inline3   = 0;
                        $obj->qty_defect_inline4   = 0;
                        $obj->qty_defect_inline5   = 0;
                        $obj->qty_defect_inline6   = 0;
                        $obj->qty_defect_inline7   = 0;
                        $obj->qty_defect_inline8   = 0;
                        $obj->qty_defect_endline   = 0;
                        $obj->first_good           = $firstGood;
                        $obj->week                 = $week;
                        $obj->ttl_defect_inline    = 0;
                        $obj->total_defect_inline  = 0;
                        $obj->total_defect_endline = $defectEndline;
                        $obj->goodQty              = $goodQty;
                        $obj->wft_inline           = $wftInline;
                        $obj->wft_endline          = $wftEndline;
                        $obj->totalWft             = $totalWft;
                        $obj->rft                  = $rft;

                        $array [] = $obj;
                    }
                        
                    
                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $a->style);
                    $sheet->setCellValue('D'.$row, $a->defect_id);
                    $sheet->setCellValue('E'.$row, $a->defect_name);
                    $sheet->setCellValue('F'.$row, $a->mayor_defect);
                    $sheet->setCellValue('G'.$row, $a->qty_defect_inline1);
                    $sheet->setCellValue('H'.$row, $a->qty_defect_inline2);
                    $sheet->setCellValue('I'.$row, $a->qty_defect_inline3);
                    $sheet->setCellValue('J'.$row, $a->qty_defect_inline4);
                    $sheet->setCellValue('K'.$row, $a->qty_defect_inline5);
                    $sheet->setCellValue('L'.$row, $a->qty_defect_inline6);
                    $sheet->setCellValue('M'.$row, $a->qty_defect_inline7);
                    $sheet->setCellValue('N'.$row, $a->qty_defect_inline8);
                    $sheet->setCellValue('O'.$row, $a->qty_defect_endline);
                    $sheet->setCellValue('P'.$row, $a->first_good);
                    $sheet->setCellValue('Q'.$row, $a->week);
                    $sheet->setCellValue('R'.$row, $a->ttl_defect_inline);
                    $sheet->setCellValue('S'.$row, $a->total_defect_inline);
                    $sheet->setCellValue('T'.$row, $a->total_defect_endline);
                    $sheet->setCellValue('U'.$row, $a->goodQty);
                    $sheet->setCellValue('V'.$row, $a->wft_inline);
                    $sheet->setCellValue('W'.$row, $a->wft_endline);
                    $sheet->setCellValue('X'.$row, $a->totalWft);
                    $sheet->setCellValue('Y'.$row, $a->rft);
                    $sheet->setCellValue('Z'.$row, '=(1-Y'.$row.')');
                    
                    $sheet->setColumnFormat(
                        array(
                            'G'.$row=>'0',
                            'h'.$row=>'0',
                            'I'.$row=>'0',
                            'J'.$row=>'0',
                            'K'.$row=>'0',
                            'L'.$row=>'0',
                            'M'.$row=>'0',
                            'N'.$row=>'0',
                            'O'.$row=>'0',
                            'P'.$row=>'0',
                            'R'.$row=>'0',
                            'S'.$row=>'0',
                            'T'.$row=>'0',
                            'U'.$row=>'0',
                            'V'.$row=>'0.00%',
                            'W'.$row=>'0.00%',
                            'X'.$row=>'0.00%',
                            'Y'.$row=>'0.00%',
                            'Z'.$row=>'0.00%',
                        )
                    );
                    foreach(range('A','Z') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setBackground('#fdf0e9');
                            $cells->setFontColor('#000000');
                        });
                    }
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->save('xlsx', $location);
    }
    public function downloadWeek(Request $request){
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);

        $startDate = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate   = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $week      = ReportWftController::week_of_today($startDate);
        $file_name = "REPORT QC ENDLINE & IN LINE BC WEEK ".$week.' '.Carbon::createFromFormat('Y-m-d',  $startDate)->format('F, Y').'.xlsx';
        // dd($file_name);
        $file       = Config::get('storage.data_entry') . '/' . $file_name;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
