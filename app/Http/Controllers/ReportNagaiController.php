<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Defect;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\SummaryProduction;
use App\Models\ProductionSizeHistory;

Config::set(['excel.export.calculate' => true]);
class ReportNagaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('report_nagai_endline.index');
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function exportFormFilter(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        $startDate = Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d');
        $endDate   = Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d');
        $data      = Line::where('buyer','nagai')
        ->whereNull('delete_at')
        ->orderBy('order','asc')
        ->get();
        
        return Excel::create('F01. Nagai-'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data,$startDate,$endDate){
            foreach ($data as $key => $d) {
                $excel->sheet($d->name, function($sheet) use($d,$startDate,$endDate){
                    $sheet->setCellValue('B1',strtoupper($d->name));
                    $sheet->setCellValue('B6','NO.');
                    $sheet->setCellValue('C6','KATEGORI');
                    $sheet->setCellValue('D6','DEFECT');
                    $sheet->setCellValue('E6','TANGGAL');
                    $sheet->setCellValue('E7','STYLE');
                    $sheet->setCellValue('E8','LOT');
                    $sheet->setCellValue('E9','COLOR');
                    $sheet->mergeCells('B1:C2');
                    $sheet->mergeCells('B6:B9');
                    $sheet->mergeCells('C6:C9');
                    $sheet->mergeCells('D6:D9');

                    $sheet->setWidth('A',2);
                    $sheet->setWidth('B',4);
                    $sheet->setWidth('C',12);
                    $sheet->setWidth('D',30);

                    $sheet->cell('B1', function($cell) {
                        $cell->setFontSize(16);
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cells('B1:C2', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('B6:B9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('C6:C9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('D6:D9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E6', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E7', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E8', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E9', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $current_colum = 'F';
                    $currenRow     = 10;
                    $no            = 1;
                    $period        = CarbonPeriod::create($startDate,$endDate);
                    $defects       = DB::table('get_defect_nagai')
                    ->select('defect_id','root_cause','position','name')
                    ->where('line_id',$d->id)
                    ->whereBetween('tanggal',[$startDate,$endDate])
                    ->groupBy('defect_id','root_cause','position','name')
                    
                    ->orderby('root_cause','asc')
                    ->orderby('defect_id','asc')
                    ->get();

                    foreach ($defects as $key => $defect) {
                        $sheet->setCellValue('B'.$currenRow, $no);
                        $sheet->setCellValue('C'.$currenRow, $defect->root_cause);
                        $sheet->setCellValue('D'.$currenRow, ucwords($defect->position)." ".ucwords($defect->name));

                        $sheet->cells('B'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->cells('C'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->cells('D'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->mergeCells('D'.$currenRow.':E'.$currenRow);
                        $currenRow++;
                        $no++;
                    }

                    foreach ($period as $date) {
                        
                        
                        $details = DB::table('qc_history_output_v')
                        ->select('date','style','poreference','article')
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->groupBy('date','style','poreference','article')->get();
                        foreach ($details as $key => $detail) 
                        {
                            $_currenRow     = 10;
                            $sheet->setCellValue($current_colum.'6',Carbon::createFromFormat('Y-m-d',  $detail->date)->format('j F, Y'));
                            $sheet->setCellValue($current_colum.'7',$detail->style);
                            $sheet->setCellValue($current_colum.'8',$detail->poreference);
                            $sheet->setCellValue($current_colum.'9',$detail->article);

                            $sheet->cells($current_colum.'6', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'7', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'8', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'9', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });

                            $sheet->setWidth($current_colum,25);
                            foreach ($defects as $key => $defect) 
                            {
                                $total = DB::table('get_defect_nagai')
                                ->where([
                                    ['tanggal',$detail->date],
                                    ['line_id',$d->id],
                                    ['style',$detail->style],
                                    ['poreference',$detail->poreference],
                                    ['article',$detail->article],
                                    ['defect_id',$defect->defect_id],
                                ])
                                ->sum('qty');
                                
                                $sheet->setCellValue($current_colum.$_currenRow,$total);  
                                $sheet->cells($current_colum.$_currenRow, function($cells) 
                                {
                                    $cells->setBorder('thin','thin','thin','thin');
                                    $cells->setAlignment('center');
                                });
                                $_currenRow++;
                            }
                            $totalOutput = DB::table('qc_history_output_v')
                            ->where([
                                ['date',$detail->date],
                                ['line_id',$d->id],
                                ['style',$detail->style],
                                ['poreference',$detail->poreference],
                                ['article',$detail->article]
                            ])
                            ->sum('counter_day');
                            $sumKotorGarment = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Kotor Garment');
                            $sumRejectFabric = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Reject Fabric');
                            $sumShading      = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Shading');
                            $sumKotorFabric  = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Kotor Fabric');
                            $sumRepair       = ReportNagaiController::getSumDefect($d->id,$detail->date,'',$detail->style,$detail->poreference,$detail->article,'');
                            $sheet->setCellValue('B'.$_currenRow,'QTY REPAIR / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+1),'KOTOR GARMENT (DEBU)/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+2),'REJECT PABRIK/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+3),'SHADING/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+4),'KOTOR FABRIC/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+5),'OUTPUT / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+6),'% REPAIR / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+7),'% KOTOR  / STYLE');
                            $sheet->setCellValue($current_colum.($_currenRow),$sumRepair);
                            $sheet->setCellValue($current_colum.($_currenRow+1),$sumKotorGarment);
                            $sheet->setCellValue($current_colum.($_currenRow+2),$sumRejectFabric);
                            $sheet->setCellValue($current_colum.($_currenRow+3),$sumShading);
                            $sheet->setCellValue($current_colum.($_currenRow+4),$sumKotorFabric);
                            $sheet->setCellValue($current_colum.($_currenRow+5),$totalOutput);
                            $sheet->setCellValue($current_colum.($_currenRow+6),($totalOutput!=0)?$sumRepair/$totalOutput:0);
                            $sheet->setCellValue($current_colum.($_currenRow+7),($totalOutput!=0)?$sumKotorFabric/$totalOutput:0);
                            $sheet->setColumnFormat(
                                array(
                                    $current_colum.($_currenRow+6)=>'0.00%',
                                    $current_colum.($_currenRow+7)=>'0.00%',
                                )
                            );
            
                            $sheet->cells($current_colum.$_currenRow, function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#0aa633');
                            });
                            $sheet->cells($current_colum.($_currenRow+1), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+2), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+3), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+4), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+5), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+6), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+7), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            
                            $sheet->cells('B'.$_currenRow.':E'.$_currenRow, function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#0aa633');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+1).':E'.($_currenRow+1), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+2).':E'.($_currenRow+2), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+3).':E'.($_currenRow+3), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+4).':E'.($_currenRow+4), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+5).':E'.($_currenRow+5), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+6).':E'.($_currenRow+6), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+7).':E'.($_currenRow+7), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            
                            $sheet->mergeCells('B'.$_currenRow.':E'.$_currenRow);
                            $sheet->mergeCells('B'.($_currenRow+1).':E'.($_currenRow+1));
                            $sheet->mergeCells('B'.($_currenRow+2).':E'.($_currenRow+2));
                            $sheet->mergeCells('B'.($_currenRow+3).':E'.($_currenRow+3));
                            $sheet->mergeCells('B'.($_currenRow+4).':E'.($_currenRow+4));
                            $sheet->mergeCells('B'.($_currenRow+5).':E'.($_currenRow+5));
                            $sheet->mergeCells('B'.($_currenRow+6).':E'.($_currenRow+6));
                            $sheet->mergeCells('B'.($_currenRow+7).':E'.($_currenRow+7));
                            
                            $current_colum++;
                        }
                    }
                });
                
            }
            $excel->sheet("REKAP OUTPUT VS REPAIR & KOTOR", function($sheet) use($data,$startDate,$endDate){
                
                $row  = 1;
                foreach ($data as $key => $d) {
                    $_row = $row+6;
                    $sheet->setWidth('B',25);
                    $sheet->setCellValue('A'.$row,strtoupper($d->name));
                    $sheet->setCellValue('B'.$row,'');
                    $sheet->setCellValue('B'.($row+1),'Kotor(%)');
                    $sheet->setCellValue('B'.($row+2),'Repair Sew(%)');
                    $sheet->setCellValue('B'.($row+3),'Target(%)');
                    $sheet->setCellValue('B'.($row+4),'KOTOR');
                    $sheet->setCellValue('B'.($row+5),'REPAIR SEW');
                    $sheet->setCellValue('B'.($row+6),'Total Output');
                    $current_colum = 'C';
                    $currenRow     = 1;
                    $period        = CarbonPeriod::create($startDate,$endDate);
                    foreach ($period as $date) {
                        $sheet->setCellValue($current_colum.$currenRow,Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d'));
                        $outputQc = DB::table('qc_history_output_v')
                        ->select('line_id','date',db::raw('sum(counter_day)as output_qc'))
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->groupBy('line_id','date')->get();
                        foreach ($outputQc as $key => $output) {
                            $target             = $d->wft;
                            $kotor              = ReportNagaiController::getSumDefect($output->line_id,$output->date,'Kotor','','','','');
                            $repairSewing       = ReportNagaiController::getSumDefect($output->line_id,$output->date,'','','','','');
                            $kotorPersen        = ($output->output_qc!=0)?($kotor/$output->output_qc)*100:0;
                            $repairSewingPersen = ($output->output_qc!=0)?($repairSewing/$output->output_qc)*100:0;
                            $sheet->setCellValue($current_colum.($row+1),($kotor!=0)?number_format($kotorPersen,1,',',','):0);
                            $sheet->setCellValue($current_colum.($row+2),($repairSewing!=0)?number_format($repairSewingPersen,1,',',','):0);
                            $sheet->setCellValue($current_colum.($row+3),number_format($target,1,',',','));
                            $sheet->setCellValue($current_colum.($row+4),$kotor);
                            $sheet->setCellValue($current_colum.($row+5),$repairSewing);
                            $sheet->setCellValue($current_colum.($row+6),$output->output_qc);

                            $sheet->cells($current_colum.($row+1), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+2), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+3), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+4), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+5), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+6), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                        }
                        $sheet->cells($current_colum.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('center');
                            $cells->setBackground('#f28225');
                            $cells->setFontColor('#000000');
                            $cells->setFontWeight('bold');
                        });
                        $current_colum++;
                    }
                    $sheet->cells('A'.$row.':A'.$_row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#a4ea96');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('B'.($row+1), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+2), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+3), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+4), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+5), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+6), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->mergeCells('A'.$row.':A'.$_row);

                    $row = $row+6;
                    $row++;
                    $_row++;
                }
            });
            $excel->sheet("TOP 3 DEFECT HARIAN", function($sheet) use($data,$startDate,$endDate){
                $period        = CarbonPeriod::create($startDate,$endDate);
                foreach ($data as $key => $d) {
                    foreach ($period as $date) {
                        $row           = 2;
                        $detailDefects = DB::table('get_top_3_nagai')
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->limit(3)
                        ->get();
                        $sheet->setCellValue('A1','Tanggal');
                        $sheet->setCellValue('B1','Line');
                        $sheet->setCellValue('C1','Style');
                        $sheet->setCellValue('D1','% Repair Sewing');
                        $sheet->setCellValue('E1','% Kotor Sewing');
                        $sheet->setCellValue('F1','Qty Output');
                        $sheet->setCellValue('G1','Repair');
                        $sheet->setCellValue('H1','Kotor');
                        $sheet->setCellValue('I1','Jenis Repair Tertinggi');
                        $sheet->setCellValue('J1','QTY');
                        $sheet->setCellValue('K1',' ');
                        $sheet->cells('A1:K1', function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setBackground('#fbd3bf');
                            $cells->setFontColor('#000000');
                            $cells->setFontWeight('bold');
                        });
                        foreach ($detailDefects as $key => $detail) {
                            $obj                = new stdClass();
                            $obj->date          = $detail->date;
                            $obj->name          = $detail->name;
                            $obj->style         = $detail->style;
                            $obj->repair_persen = ($detail->qty_output)?$detail->qty_repair/$detail->qty_output:0;
                            $obj->kotor_persen  = ($detail->qty_output)?$detail->qty_kotor/$detail->qty_output:0;
                            $obj->qty_output    = ($detail->qty_output)?$detail->qty_output:0;
                            $obj->qty_repair    = ($detail->qty_repair)?$detail->qty_repair:0;
                            $obj->qty_kotor     = ($detail->qty_kotor)?$detail->qty_kotor:0;
                            $obj->defect_name   = $detail->defect_name;
                            $obj->qty_defect    = ($detail->qty_defect)?$detail->qty_defect:0;
                            $obj->pcs           = "pcs";

                            $array [] = $obj;
                        }

                    } 
                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row,$a->date);
                    $sheet->setCellValue('B'.$row,strtoupper($a->name));
                    $sheet->setCellValue('C'.$row,$a->style);
                    $sheet->setCellValue('D'.$row,$a->repair_persen);
                    $sheet->setCellValue('E'.$row,$a->kotor_persen);
                    $sheet->setCellValue('F'.$row,$a->qty_output);
                    $sheet->setCellValue('G'.$row,$a->qty_repair);
                    $sheet->setCellValue('H'.$row,$a->qty_kotor);
                    $sheet->setCellValue('I'.$row,$a->defect_name);
                    $sheet->setCellValue('J'.$row,$a->qty_defect);
                    $sheet->setCellValue('K'.$row,"pcs");
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('B'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('C'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('D'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('E'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('F'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('G'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('H'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('I'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('J'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('K'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->setColumnFormat(
                        array(
                            'D'.($row)=>'0.00%'
                        )
                    );
                    $sheet->setColumnFormat(
                        array(
                            'E'.($row)=>'0.00%'
                        )
                    );
                    $row++;
                }
            });
            $excel->sheet("TOP 5 DEFECT", function($sheet) use($data,$startDate,$endDate){
                $period        = CarbonPeriod::create($startDate,$endDate);
                foreach ($period as $date) {
                    $row           = 2;
                    $detailDefects = DB::table('get_defect_nagai')
                    ->select('tanggal','defect_id','root_cause','name',db::raw('sum(qty) as qty_defect'))
                    ->where('tanggal',$date->format('Y-m-d'))
                    ->groupBy('tanggal','defect_id','root_cause','name')
                    ->orderByRaw('tanggal,SUM (qty) desc')
                    ->limit(5)
                    ->get();
                    $sheet->setCellValue('A1','Date');
                    $sheet->setCellValue('B1','Ranking');
                    $sheet->setCellValue('C1','Defect');
                    $sheet->setCellValue('D1','Qty Defect');
                    $sheet->cells('A1:D1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#00331A');
                        $cells->setFontColor('#ffffff');
                        $cells->setFontWeight('bold');
                    });
                    $ranking = 1;
                    foreach ($detailDefects as $key => $detail) {
                        $ranking+1;
                        if ($ranking>5) {
                            $ranking=0;
                        }
                        $obj              = new stdClass();
                        $obj->date        = $detail->tanggal;
                        $obj->rangking    = $ranking;
                        $obj->defect_name = $detail->root_cause." ".$detail->name;
                        $obj->qty_defect  = $detail->qty_defect;

                        $array [] = $obj;
                        $ranking++;
                    }

                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row,$a->date);
                    $sheet->setCellValue('B'.$row,$a->rangking);
                    $sheet->setCellValue('C'.$row,$a->defect_name);
                    $sheet->setCellValue('D'.$row,$a->qty_defect);
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('B'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('C'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('D'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    static function getSumDefect($line_id,$date,$root_cause,$style,$poreference,$article,$kategori)
    {
        $sumDefect = DB::table('get_defect_nagai')
        ->where([
            ['tanggal',$date],
            ['line_id',$line_id],
        ]);
        if ($style!='') {
            $sumDefect->where([
                ['style',$style],
                ['poreference',$poreference],
                ['article',$article],
            ]);
        }
        if($root_cause){
            if($kategori =='Kotor Garment'){
                $sumDefect->whereIn('name',['Kotor Garment','Benda Asing']);
            }else if($kategori=='Reject Fabric'){
                $sumDefect->where('name','Reject Fabric');
            }else if($kategori=='Shading'){
                $sumDefect->where('name','Shading');
            }else if($kategori=='Kotor Fabric'){
                $sumDefect->where('name','Kotor Fabric');
            }
        }else{
            $sumDefect->where('root_cause','<>','Kotor');
        }
        
        
        return $sumDefect->sum('qty');;
    }
    
    public function downloadFilterMont(Request $request){
        $this->validate($request,[
            'date' => 'required'
        ]);
        $filename   = "F01. Nagai ".Carbon::createFromFormat('j F, Y',  $request->date)->format('F, Y').'.xlsx';
        $file       = Config::get('storage.nagai') . '/' . $filename;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
    static function exportFormMont()
    {
        $date      = Carbon::now();          //returns current day
        $startDate = $date->firstOfMonth();
        $endDate   = Carbon::now();
        // dd($startDate."  ".$endDate);
        $data      = Line::where('buyer','nagai')
        ->whereNull('delete_at')
        ->orderBy('order','asc')
        ->get();
        $location = Config::get('storage.nagai');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);
            
        $file_name = "F01. Nagai ".Carbon::createFromFormat('Y-m-d H:i:s',  $startDate)->format('F, Y');

        
        $file = Excel::create($file_name,function($excel) use($data,$startDate,$endDate){
            foreach ($data as $key => $d) {
                $excel->sheet($d->name, function($sheet) use($d,$startDate,$endDate){
                    $sheet->setCellValue('B1',strtoupper($d->name));
                    $sheet->setCellValue('B6','NO.');
                    $sheet->setCellValue('C6','KATEGORI');
                    $sheet->setCellValue('D6','DEFECT');
                    $sheet->setCellValue('E6','TANGGAL');
                    $sheet->setCellValue('E7','STYLE');
                    $sheet->setCellValue('E8','LOT');
                    $sheet->setCellValue('E9','COLOR');
                    $sheet->mergeCells('B1:C2');
                    $sheet->mergeCells('B6:B9');
                    $sheet->mergeCells('C6:C9');
                    $sheet->mergeCells('D6:D9');

                    $sheet->setWidth('A',2);
                    $sheet->setWidth('B',4);
                    $sheet->setWidth('C',12);
                    $sheet->setWidth('D',30);

                    $sheet->cell('B1', function($cell) {
                        $cell->setFontSize(16);
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cells('B1:C2', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('B6:B9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('C6:C9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('D6:D9', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E6', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E7', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E8', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('E9', function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ff0000');
                        $cells->setFontColor('#ffffff');
                    });
                    $current_colum = 'F';
                    $currenRow     = 10;
                    $no            = 1;
                    $period        = CarbonPeriod::create($startDate,$endDate);
                    $defects       = DB::table('get_defect_nagai')
                    ->select('defect_id','root_cause','position','name')
                    ->where('line_id',$d->id)
                    ->whereBetween('tanggal',[$startDate,$endDate])
                    ->groupBy('defect_id','root_cause','position','name')
                    
                    ->orderby('root_cause','asc')
                    ->orderby('defect_id','asc')
                    ->get();

                    foreach ($defects as $key => $defect) {
                        $sheet->setCellValue('B'.$currenRow, $no);
                        $sheet->setCellValue('C'.$currenRow, $defect->root_cause);
                        $sheet->setCellValue('D'.$currenRow, ucwords($defect->position)." ".ucwords($defect->name));

                        $sheet->cells('B'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->cells('C'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->cells('D'.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('left');
                        });
                        $sheet->mergeCells('D'.$currenRow.':E'.$currenRow);
                        $currenRow++;
                        $no++;
                    }

                    foreach ($period as $date) {
                        
                        
                        $details = DB::table('qc_history_output_v')
                        ->select('date','style','poreference','article')
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->groupBy('date','style','poreference','article')->get();
                        foreach ($details as $key => $detail) 
                        {
                            $_currenRow     = 10;
                            $sheet->setCellValue($current_colum.'6',Carbon::createFromFormat('Y-m-d',  $detail->date)->format('j F, Y'));
                            $sheet->setCellValue($current_colum.'7',$detail->style);
                            $sheet->setCellValue($current_colum.'8',$detail->poreference);
                            $sheet->setCellValue($current_colum.'9',$detail->article);

                            $sheet->cells($current_colum.'6', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'7', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'8', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });
                            $sheet->cells($current_colum.'9', function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ff0000');
                                $cells->setFontColor('#ffffff');
                            });

                            $sheet->setWidth($current_colum,25);
                            foreach ($defects as $key => $defect) 
                            {
                                $total = DB::table('get_defect_nagai')
                                ->where([
                                    ['tanggal',$detail->date],
                                    ['line_id',$d->id],
                                    ['style',$detail->style],
                                    ['poreference',$detail->poreference],
                                    ['article',$detail->article],
                                    ['defect_id',$defect->defect_id],
                                ])
                                ->sum('qty');
                                
                                $sheet->setCellValue($current_colum.$_currenRow,$total);  
                                $sheet->cells($current_colum.$_currenRow, function($cells) 
                                {
                                    $cells->setBorder('thin','thin','thin','thin');
                                    $cells->setAlignment('center');
                                });
                                $_currenRow++;
                            }
                            $totalOutput = DB::table('qc_history_output_v')
                            ->where([
                                ['date',$detail->date],
                                ['line_id',$d->id],
                                ['style',$detail->style],
                                ['poreference',$detail->poreference],
                                ['article',$detail->article]
                            ])
                            ->sum('counter_day');

                            $sumKotorGarment = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Kotor Garment');
                            $sumRejectFabric = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Reject Fabric');
                            $sumShading      = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Shading');
                            $sumKotorFabric  = ReportNagaiController::getSumDefect($d->id,$detail->date,'Kotor',$detail->style,$detail->poreference,$detail->article,'Kotor Fabric');
                            $sumRepair       = ReportNagaiController::getSumDefect($d->id,$detail->date,'',$detail->style,$detail->poreference,$detail->article,'');
                            $sheet->setCellValue('B'.$_currenRow,'QTY REPAIR / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+1),'KOTOR GARMENT (DEBU)/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+2),'REJECT PABRIK/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+3),'SHADING/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+4),'KOTOR FABRIC/ STYLE');
                            $sheet->setCellValue('B'.($_currenRow+5),'OUTPUT / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+6),'% REPAIR / STYLE');
                            $sheet->setCellValue('B'.($_currenRow+7),'% KOTOR  / STYLE');
                            $sheet->setCellValue($current_colum.($_currenRow),$sumRepair);
                            $sheet->setCellValue($current_colum.($_currenRow+1),$sumKotorGarment);
                            $sheet->setCellValue($current_colum.($_currenRow+2),$sumRejectFabric);
                            $sheet->setCellValue($current_colum.($_currenRow+3),$sumShading);
                            $sheet->setCellValue($current_colum.($_currenRow+4),$sumKotorFabric);
                            $sheet->setCellValue($current_colum.($_currenRow+5),$totalOutput);
                            $sheet->setCellValue($current_colum.($_currenRow+6),($totalOutput!=0)?$sumRepair/$totalOutput:0);
                            $sheet->setCellValue($current_colum.($_currenRow+7),($totalOutput!=0)?$sumKotorFabric/$totalOutput:0);
                            $sheet->setColumnFormat(
                                array(
                                    $current_colum.($_currenRow+6)=>'0.00%',
                                    $current_colum.($_currenRow+7)=>'0.00%',
                                )
                            );
            
                            $sheet->cells($current_colum.$_currenRow, function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#0aa633');
                            });
                            $sheet->cells($current_colum.($_currenRow+1), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+2), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+3), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+4), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+5), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+6), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            $sheet->cells($current_colum.($_currenRow+7), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('center');
                            });
                            
                            $sheet->cells('B'.$_currenRow.':E'.$_currenRow, function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#0aa633');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+1).':E'.($_currenRow+1), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+2).':E'.($_currenRow+2), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+3).':E'.($_currenRow+3), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+4).':E'.($_currenRow+4), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+5).':E'.($_currenRow+5), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+6).':E'.($_currenRow+6), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells('B'.($_currenRow+7).':E'.($_currenRow+7), function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#07c1de');
                                $cells->setFontWeight('bold');
                            });
                            
                            $sheet->mergeCells('B'.$_currenRow.':E'.$_currenRow);
                            $sheet->mergeCells('B'.($_currenRow+1).':E'.($_currenRow+1));
                            $sheet->mergeCells('B'.($_currenRow+2).':E'.($_currenRow+2));
                            $sheet->mergeCells('B'.($_currenRow+3).':E'.($_currenRow+3));
                            $sheet->mergeCells('B'.($_currenRow+4).':E'.($_currenRow+4));
                            $sheet->mergeCells('B'.($_currenRow+5).':E'.($_currenRow+5));
                            $sheet->mergeCells('B'.($_currenRow+6).':E'.($_currenRow+6));
                            $sheet->mergeCells('B'.($_currenRow+7).':E'.($_currenRow+7));
                            
                            $current_colum++;
                        }
                    }
                });
                
            }
            $excel->sheet("REKAP OUTPUT VS REPAIR & KOTOR", function($sheet) use($data,$startDate,$endDate){
                
                $row  = 1;
                foreach ($data as $key => $d) {
                    $_row = $row+6;
                    $sheet->setWidth('B',25);
                    $sheet->setCellValue('A'.$row,strtoupper($d->name));
                    $sheet->setCellValue('B'.$row,'');
                    $sheet->setCellValue('B'.($row+1),'Kotor(%)');
                    $sheet->setCellValue('B'.($row+2),'Repair Sew(%)');
                    $sheet->setCellValue('B'.($row+3),'Target(%)');
                    $sheet->setCellValue('B'.($row+4),'KOTOR');
                    $sheet->setCellValue('B'.($row+5),'REPAIR SEW');
                    $sheet->setCellValue('B'.($row+6),'Total Output');
                    $current_colum = 'C';
                    $currenRow     = 1;
                    $period        = CarbonPeriod::create($startDate,$endDate);
                    foreach ($period as $date) {
                        $sheet->setCellValue($current_colum.$currenRow,Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d'));
                        $outputQc = DB::table('qc_history_output_v')
                        ->select('line_id','date',db::raw('sum(counter_day)as output_qc'))
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->groupBy('line_id','date')->get();
                        foreach ($outputQc as $key => $output) {
                            $target             = $d->wft;
                            $kotor              = ReportNagaiController::getSumDefect($output->line_id,$output->date,'Kotor','','','','');
                            $repairSewing       = ReportNagaiController::getSumDefect($output->line_id,$output->date,'','','','','');
                            $kotorPersen        = ($output->output_qc!=0)?($kotor/$output->output_qc)*100:0;
                            $repairSewingPersen = ($output->output_qc!=0)?($repairSewing/$output->output_qc)*100:0;
                            $sheet->setCellValue($current_colum.($row+1),number_format($kotorPersen,1,',',','));
                            $sheet->setCellValue($current_colum.($row+2),number_format($repairSewingPersen,1,',',','));
                            $sheet->setCellValue($current_colum.($row+3),number_format($target,1,',',','));
                            $sheet->setCellValue($current_colum.($row+4),$kotor);
                            $sheet->setCellValue($current_colum.($row+5),$repairSewing);
                            $sheet->setCellValue($current_colum.($row+6),$output->output_qc);

                            $sheet->cells($current_colum.($row+1), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+2), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+3), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+4), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+5), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->cells($current_colum.($row+6), function($cells) 
                            {
                                $cells->setBorder('thin','thin','thin','thin');
                                $cells->setAlignment('left');
                                $cells->setBackground('#ffbeb1');
                                $cells->setFontColor('#000000');
                            });
                        }
                        $sheet->cells($current_colum.$currenRow, function($cells) 
                        {
                            $cells->setBorder('thin','thin','thin','thin');
                            $cells->setAlignment('center');
                            $cells->setBackground('#f28225');
                            $cells->setFontColor('#000000');
                            $cells->setFontWeight('bold');
                        });
                        $current_colum++;
                    }
                    $sheet->cells('A'.$row.':A'.$_row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#a4ea96');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('B'.($row+1), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+2), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+3), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+4), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+5), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('B'.($row+6), function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground('#ffbeb1');
                        $cells->setFontColor('#000000');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->mergeCells('A'.$row.':A'.$_row);

                    $row = $row+6;
                    $row++;
                    $_row++;
                }
            });
            $excel->sheet("TOP 3 DEFECT HARIAN", function($sheet) use($data,$startDate,$endDate){
                $period        = CarbonPeriod::create($startDate,$endDate);
                foreach ($data as $key => $d) {
                    foreach ($period as $date) {
                        $row           = 2;
                        $detailDefects = DB::table('get_top_3_nagai')
                        ->where('line_id',$d->id)
                        ->where('date',$date->format('Y-m-d'))
                        ->limit(3)
                        ->get();
                        $sheet->setCellValue('A1','Tanggal');
                        $sheet->setCellValue('B1','Line');
                        $sheet->setCellValue('C1','Style');
                        $sheet->setCellValue('D1','% Repair Sewing');
                        $sheet->setCellValue('E1','% Kotor Sewing');
                        $sheet->setCellValue('F1','Qty Output');
                        $sheet->setCellValue('G1','Repair');
                        $sheet->setCellValue('H1','Kotor');
                        $sheet->setCellValue('I1','Jenis Repair Tertinggi');
                        $sheet->setCellValue('J1','QTY');
                        $sheet->setCellValue('K1',' ');
                        $sheet->cells('A1:K1', function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setBackground('#fbd3bf');
                            $cells->setFontColor('#000000');
                            $cells->setFontWeight('bold');
                        });
                        foreach ($detailDefects as $key => $detail) {
                            $obj                = new stdClass();
                            $obj->date          = $detail->date;
                            $obj->name          = $detail->name;
                            $obj->style         = $detail->style;
                            $obj->repair_persen = ($detail->qty_output)?$detail->qty_repair/$detail->qty_output:0;
                            $obj->kotor_persen  = ($detail->qty_output)?$detail->qty_kotor/$detail->qty_output:0;
                            $obj->qty_output    = ($detail->qty_output)?$detail->qty_output:0;
                            $obj->qty_repair    = ($detail->qty_repair)?$detail->qty_repair:0;
                            $obj->qty_kotor     = ($detail->qty_kotor)?$detail->qty_kotor:0;
                            $obj->defect_name   = $detail->defect_name;
                            $obj->qty_defect    = ($detail->qty_defect)?$detail->qty_defect:0;
                            $obj->pcs           = "pcs";

                            $array [] = $obj;
                        }

                    } 
                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row,$a->date);
                    $sheet->setCellValue('B'.$row,strtoupper($a->name));
                    $sheet->setCellValue('C'.$row,$a->style);
                    $sheet->setCellValue('D'.$row,$a->repair_persen);
                    $sheet->setCellValue('E'.$row,$a->kotor_persen);
                    $sheet->setCellValue('F'.$row,$a->qty_output);
                    $sheet->setCellValue('G'.$row,$a->qty_repair);
                    $sheet->setCellValue('H'.$row,$a->qty_kotor);
                    $sheet->setCellValue('I'.$row,$a->defect_name);
                    $sheet->setCellValue('J'.$row,$a->qty_defect);
                    $sheet->setCellValue('K'.$row,"pcs");
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('B'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('C'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('D'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('E'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('F'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('G'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('H'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('I'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('J'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->cells('K'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#fdf0e9');
                        $cells->setFontColor('#000000');
                    });
                    $sheet->setColumnFormat(
                        array(
                            'D'.($row)=>'0.00%'
                        )
                    );
                    $sheet->setColumnFormat(
                        array(
                            'E'.($row)=>'0.00%'
                        )
                    );
                    $row++;
                }
            });
            $excel->sheet("TOP 5 DEFECT", function($sheet) use($data,$startDate,$endDate){
                $period        = CarbonPeriod::create($startDate,$endDate);
                foreach ($period as $date) {
                    $row           = 2;
                    $detailDefects = DB::table('get_defect_nagai')
                    ->select('tanggal','defect_id','root_cause','name',db::raw('sum(qty) as qty_defect'))
                    ->where('tanggal',$date->format('Y-m-d'))
                    ->groupBy('tanggal','defect_id','root_cause','name')
                    ->orderByRaw('tanggal,SUM (qty) desc')
                    ->limit(5)
                    ->get();
                    $sheet->setCellValue('A1','Date');
                    $sheet->setCellValue('B1','Ranking');
                    $sheet->setCellValue('C1','Defect');
                    $sheet->setCellValue('D1','Qty Defect');
                    $sheet->cells('A1:D1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#00331A');
                        $cells->setFontColor('#ffffff');
                        $cells->setFontWeight('bold');
                    });
                    $ranking = 1;
                    foreach ($detailDefects as $key => $detail) {
                        $ranking+1;
                        if ($ranking>5) {
                            $ranking=0;
                        }
                        $obj              = new stdClass();
                        $obj->date        = $detail->tanggal;
                        $obj->rangking    = $ranking;
                        $obj->defect_name = $detail->root_cause." ".$detail->name;
                        $obj->qty_defect  = $detail->qty_defect;

                        $array [] = $obj;
                        $ranking++;
                    }

                }
                foreach ($array as $key => $a) {
                    $sheet->setCellValue('A'.$row,$a->date);
                    $sheet->setCellValue('B'.$row,$a->rangking);
                    $sheet->setCellValue('C'.$row,$a->defect_name);
                    $sheet->setCellValue('D'.$row,$a->qty_defect);
                    $sheet->cells('A'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('B'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('C'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->cells('D'.$row, function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setBackground('#a10000');
                        $cells->setFontColor('#ffffff');
                    });
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->save('xlsx', $location);
    }
}
