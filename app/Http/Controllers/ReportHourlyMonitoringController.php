<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Production;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\BreakDownSize;
use App\Models\SummaryProduction;
use App\Models\ProductionSizeHistory;
Config::set(['excel.export.calculate' => true]);

class ReportHourlyMonitoringController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $workingHours = WorkingHours::getWorkingHours();
        return view('report_monitoring_hourly.index',compact('workingHours'));
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);
            $factory_id = Auth::user()->factory_id;
            $buyer      = $this->Buyer();
            $marks      = array_fill(0, sizeof($buyer), '?');
            $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            $data       = DB::select(db::Raw("select * from f_hourly_output('$startDate','$endDate') where factory_id='$factory_id' and production in(" . implode(',', $marks) . ")"),$buyer);
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('buyer',function($data){
                return ucwords($data->buyer);
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('category',function($data){
                return $data->category;
            })
            ->editColumn('smv',function($data){
                return $data->smv;
            })
            ->editColumn('present_sewer',function($data){
                return $data->present_sewer;
            })
            ->editColumn('working_hours',function($data){
                return $data->working_hours;
            })
            ->editColumn('wip_sewing',function($data){
                $data = $this->getWipStyle($data->factory_id,$data->date,$data->style,$data->line_id);
                return $data;
            })
            ->editColumn('input_loading',function($data){
                
                return $this->getOutputLoading($data->date,$data->style,$data->line_id);
            })
            ->editColumn('target_daily',function($data){
                
                return $data->commitment_line;
            })
            ->editColumn('target_hourly',function($data){
                return $data->target_hourly;
            })
            ->editColumn('jam_ke',function($data){
                $jamke = $this->HoursTo($data->date,$data->line_id);
                
                return $jamke;
            })
            ->editColumn('I',function($data){
                
                return $data->i;
            })
            ->editColumn('II',function($data){
                
                return $data->ii;
            })
            ->editColumn('III',function($data){
                
                return $data->iii;
            })
            ->editColumn('III',function($data){
                
                return $data->iii;
            })
            ->editColumn('IV',function($data){
                
                return $data->iv;
            })
            ->editColumn('V',function($data){
                
                return $data->v;
            })
            ->editColumn('VI',function($data){
                
                return $data->vi;
            })
            ->editColumn('VII',function($data){
                
                return $data->vii;
            })
            ->editColumn('VIII',function($data){
                
                return $data->viii;
            })
            ->editColumn('IX',function($data){
                
                return $data->ix;
            })
            ->editColumn('X',function($data){
                
                return $data->x;
            })
            ->editColumn('XI',function($data){
                
                return $data->xi;
            })
            ->editColumn('XII',function($data){
                
                return $data->xii;
            })
            ->editColumn('XIII',function($data){
                
                return $data->xiii;
            })
            ->editColumn('total',function($data){
                
                return $data->total;
            })
            ->editColumn('total_output',function($data){
                $daily = DB::table('daily_line_v')->select(db::raw('date(created_at)as date'),'line_id')
                ->whereNull('delete_at')
                ->where(db::raw('date(created_at)'),'<',$data->date)
                ->where('line_id',$data->line_id)
                ->orderBy('created_at','desc')->first();
                
                $outputQc = ($daily)?$this->getSumProductionaStyle($data->style,$daily->date,'output_qc_endline',$data->line_id):0;
                return $outputQc;
            })
            ->editColumn('output_mnt',function($data){
                return number_format($data->output_mnt,1,',',',');
            })
            ->editColumn('eff_style',function($data){
                $jamke = $this->HoursTo($data->date,$data->line_id);
                
                $actual = ($data->total==0||$data->smv==null?0:(($data->qc_smv)/($data->present_sewer*$jamke*60))*100);
                return number_format($actual,1,',',',');
            })
            ->editColumn('actual_eff',function($data){
                $jamke = $this->HoursTo($data->date,$data->line_id);
                $actual = $this->dailyLine($data->date,$data->line_id,$jamke);
                return number_format($actual,1,',',',');
            })
            ->editColumn('total_wft',function($data){
                return number_format($data->wft_endline,1,',',',');
            })
            ->editColumn('eff_kemarin',function($data){
                $daily = DB::table('daily_line_v')->select(db::raw('date(created_at)as date'),'line_id')
                ->whereNull('delete_at')
                ->where(db::raw('date(created_at)'),'<',$data->date)
                ->where('line_id',$data->line_id)
                ->orderBy('created_at','desc')->first();
                
                $jamke  = ($daily)?$this->HoursTo($daily->date,$daily->line_id):0;
                
                $actual = ($daily?$this->dailyLine($daily->date,$daily->line_id,$jamke):'0');
                return number_format($actual,1,',',',');
            })
            ->setRowAttr([
                'style' => function($data) 
                {
                    if($data->smv) {
                        return  'background-color: #d9ffde';
                    }else{
                        return 'background-color: #e53f3f';
                    };
                },
            ])
            ->rawColumns(['style'])
            ->make(true);
        }
    }
    public function exportFormOutputHourly(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $factory_id = Auth::user()->factory_id;
        $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $data       = DB::select(db::Raw("select * from f_hourly_output('$startDate','$endDate') where factory_id='$factory_id'"));
        
        return Excel::create('Hourly Monitoring Output Daily-'.$request->date."_".Carbon::now()->format('u'),function ($excel)use($data,$startDate,$endDate,$factory_id){
            $dataSewing       = DB::select(db::Raw("select * from f_hourly_output_sewing('$startDate','$endDate') where factory_id='$factory_id'"));
            /* $excel->sheet('Output Sewing', function($sheet) use($dataSewing){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line');
                $sheet->setCellValue('C1','Buyer');
                $sheet->setCellValue('D1','Category');
                $sheet->setCellValue('E1','Style');
                $sheet->setCellValue('F1','Jam Ke-');
                $sheet->mergeCells('F1:R1');
                $sheet->cell('F1:R1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('F2','7');
                $sheet->setCellValue('G2','8');
                $sheet->setCellValue('H2','9');
                $sheet->setCellValue('I2','10');
                $sheet->setCellValue('J2','11');
                $sheet->setCellValue('K2','12');
                $sheet->setCellValue('L2','13');
                $sheet->setCellValue('M2','14');
                $sheet->setCellValue('N2','15');
                $sheet->setCellValue('O2','16');
                $sheet->setCellValue('P2','17');
                $sheet->setCellValue('Q2','18');
                $sheet->setCellValue('R2','19');
                $sheet->setCellValue('S1','TOTAL');
                $row = 3;
                foreach ($dataSewing as $key => $a) {
                    $getBuyer    = $this->getBuyer($a->style,$a->line_id,'buyer');
                    $getCategory = $this->getBuyer($a->style,$a->line_id,'category');
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $getBuyer);
                    $sheet->setCellValue('D'.$row, $getCategory);
                    $sheet->setCellValue('E'.$row, $a->style);
                    $sheet->setCellValue('F'.$row, $a->i);
                    $sheet->setCellValue('G'.$row, $a->ii);
                    $sheet->setCellValue('H'.$row, $a->iii);
                    $sheet->setCellValue('I'.$row, $a->iv);
                    $sheet->setCellValue('J'.$row, $a->v);
                    $sheet->setCellValue('K'.$row, $a->vi);
                    $sheet->setCellValue('L'.$row, $a->vii);
                    $sheet->setCellValue('M'.$row, $a->viii);
                    $sheet->setCellValue('N'.$row, $a->ix);
                    $sheet->setCellValue('O'.$row, $a->x);
                    $sheet->setCellValue('P'.$row, $a->xi);
                    $sheet->setCellValue('Q'.$row, $a->xii);
                    $sheet->setCellValue('R'.$row, $a->xiii);
                    $sheet->setCellValue('S'.$row, '=SUM(A'.$row.':R'.$row.')');
                    $sheet->cells('A'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('C'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('D'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('E'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('F'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('G'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('H'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('I'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('J'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('K'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('L'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('M'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('N'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('O'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('P'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('Q'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('R'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('S'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $row++;
                }
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->mergeCells('C1:C2');
                $sheet->mergeCells('D1:D2');
                $sheet->mergeCells('E1:E2');
                $sheet->mergeCells('S1:S2');
                $sheet->cells('A1:A2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('B1:B2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('C1:C2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('D1:D2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('E1:E2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F1:R1', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F2:R2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('S1:S2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
            }); */
            $excel->sheet('Output QC', function($sheet) use($data){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line');
                $sheet->setCellValue('C1','Buyer');
                $sheet->setCellValue('D1','Style');
                $sheet->setCellValue('E1','Cat');
                $sheet->setCellValue('F1','TS');
                $sheet->setCellValue('G1','MP');
                $sheet->setCellValue('H1','Plan Jam Kerja');
                $sheet->setCellValue('I1','WIP Sewing');
                $sheet->setCellValue('J1','Input Loading');
                $sheet->setCellValue('K1','Target per Hari');
                $sheet->setCellValue('L1','Target Output perJam');
                $sheet->setCellValue('M1','Jam Ke-');
                $sheet->mergeCells('M1:Y1');
                $sheet->cell('M1:Y1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('M2','7');
                $sheet->setCellValue('N2','8');
                $sheet->setCellValue('O2','9');
                $sheet->setCellValue('P2','10');
                $sheet->setCellValue('Q2','11');
                $sheet->setCellValue('R2','12');
                $sheet->setCellValue('S2','13');
                $sheet->setCellValue('T2','14');
                $sheet->setCellValue('U2','15');
                $sheet->setCellValue('V2','16');
                $sheet->setCellValue('W2','17');
                $sheet->setCellValue('X2','18');
                $sheet->setCellValue('Y2','19');
                $sheet->setCellValue('Z2','20');
                $sheet->setCellValue('AA2','21');
                $sheet->setCellValue('AB2','22');
                $sheet->setCellValue('AC2','23');
                $sheet->setCellValue('AD1','TOTAL');
                $sheet->setCellValue('AE1','TOTAL OUTPUT KEMARIN');
                $sheet->setCellValue('AF1','Output mnt prd');
                $sheet->setCellValue('AG1','Jam Ke');
                $sheet->setCellValue('AH1','EFF Style');
                $sheet->setCellValue('AI1','EFF Line');
                $sheet->setCellValue('AJ1','Total WFT');
                $sheet->setCellValue('AK1','EFF Kemarin');
                $index    = 0;
                $array    = array();
                $row      = 3;
                $totalAll = 0;
                $position = 1;
                foreach(range('A','AK') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#2eceff');
                        $cells->setFontColor('#000000');
                    });
                }
                foreach ($data as $key => $d) {
                    $wip_sewing    = $this->getWipStyle($d->factory_id,$d->date,$d->style,$d->line_id);
                    $outputLoading = $this->getOutputLoading($d->date,$d->style,$d->line_id);
                    $jamke         = $this->HoursTo($d->date,$d->line_id);
                    $EffLine       = $this->dailyLine($d->date,$d->line_id,$jamke);
                    if ($d->total==0) {
                        $EffStyle =0;
                    }else if($d->smv==null){
                        $EffStyle =0;
                    }else{
                        if ($jamke==0) {
                            $EffStyle =0;
                        } else {
                            $EffStyle = (($d->qc_smv)/($d->present_sewer*$jamke*60))*100;
                        }
                        
                    }
                    $daily         = DB::table('daily_line_v')->select(db::raw('date(created_at)as date'),'line_id')
                    ->whereNull('delete_at')
                    ->where(db::raw('date(created_at)'),'<',$d->date)
                    ->where('line_id',$d->line_id)
                    ->orderBy('created_at','desc')->first();
                        $EffYesterday         = ($daily?$this->dailyLine($daily->date,$daily->line_id,$jamke):'0');
                        $totalOutKemarin      = ($daily)?$this->getSumProductionaStyle($d->style,$daily->date,'output_qc_endline',$d->line_id):0;
                        $getBuyer             = $this->getBuyer($d->style,$d->line_id,'buyer');
                        $getCategory          = $this->getBuyer($d->style,$d->line_id,'category');
                        $obj                  = new stdClass();
                        $obj->date            = $d->date;
                        $obj->line_name       = $d->line_name;
                        $obj->buyer           = $getBuyer;
                        $obj->style           = $d->style;
                        $obj->category        = $getCategory;
                        $obj->smv             = $d->smv;
                        $obj->present_sewer   = $d->present_sewer;
                        $obj->working_hours   = $d->working_hours;
                        $obj->wip_sewing      = $wip_sewing;
                        $obj->outputLoading   = $outputLoading;
                        $obj->commitment_line = $d->commitment_line;
                        $obj->target_hourly   = $d->target_hourly;
                        $obj->i               = $d->i;
                        $obj->ii              = $d->ii;
                        $obj->iii             = $d->iii;
                        $obj->iv              = $d->iv;
                        $obj->v               = $d->v;
                        $obj->vi              = $d->vi;
                        $obj->vii             = $d->vii;
                        $obj->viii            = $d->viii;
                        $obj->ix              = $d->ix;
                        $obj->x               = $d->x;
                        $obj->xi              = $d->xi;
                        $obj->xii             = $d->xii;
                        $obj->xiii            = $d->xiii;
                        $obj->xiv             = $d->xiv;
                        $obj->xv              = $d->xv;
                        $obj->xvi             = $d->xvi;
                        $obj->xvii            = $d->xvii;
                        $obj->total           = $d->total;
                        $obj->totalOutKemarin = $totalOutKemarin;
                        $obj->output_mnt      = number_format($d->output_mnt,1, '.', ',');
                        $obj->EffStyle        = number_format($EffStyle,1, '.', ',');
                        $obj->jamke           = $jamke;
                        $obj->EffLine         = number_format($EffLine,1, '.', ',');
                        $obj->total_wft       = number_format($d->wft_endline,1, '.', ',');
                        $obj->EffYesterday    = number_format($EffYesterday,1, '.', ',');
                    $array []                    = $obj;
                }
                foreach ($array as $key => $a) {
                    $color = ($a->smv)?'#69F173':'#e53f3f';
                    
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $a->buyer);
                    $sheet->setCellValue('D'.$row, $a->style);
                    $sheet->setCellValue('E'.$row, $a->category);
                    $sheet->setCellValue('F'.$row, $a->smv);
                    $sheet->setCellValue('G'.$row, $a->present_sewer);
                    $sheet->setCellValue('H'.$row, $a->working_hours);
                    $sheet->setCellValue('I'.$row, $a->wip_sewing);
                    $sheet->setCellValue('J'.$row, $a->outputLoading);
                    $sheet->setCellValue('K'.$row, $a->commitment_line);
                    $sheet->setCellValue('L'.$row, $a->target_hourly);
                    $sheet->setCellValue('M'.$row, $a->i);
                    $sheet->setCellValue('N'.$row, $a->ii);
                    $sheet->setCellValue('O'.$row, $a->iii);
                    $sheet->setCellValue('P'.$row, $a->iv);
                    $sheet->setCellValue('Q'.$row, $a->v);
                    $sheet->setCellValue('R'.$row, $a->vi);
                    $sheet->setCellValue('S'.$row, $a->vii);
                    $sheet->setCellValue('T'.$row, $a->viii);
                    $sheet->setCellValue('U'.$row, $a->ix);
                    $sheet->setCellValue('V'.$row, $a->x);
                    $sheet->setCellValue('W'.$row, $a->xi);
                    $sheet->setCellValue('X'.$row, $a->xii);
                    $sheet->setCellValue('Y'.$row, $a->xiii);
                    $sheet->setCellValue('Z'.$row, $a->xiv);
                    $sheet->setCellValue('AA'.$row, $a->xv);
                    $sheet->setCellValue('AB'.$row, $a->xvi);
                    $sheet->setCellValue('AC'.$row, $a->xvii);
                    $sheet->setCellValue('AD'.$row, $a->total);
                    $sheet->setCellValue('AE'.$row, $a->totalOutKemarin);
                    $sheet->setCellValue('AF'.$row, $a->output_mnt);
                    $sheet->setCellValue('AG'.$row, $a->jamke);
                    $sheet->setCellValue('AH'.$row, $a->EffStyle);
                    $sheet->setCellValue('AI'.$row, $a->EffLine);
                    $sheet->setCellValue('AJ'.$row, $a->total_wft);
                    $sheet->setCellValue('AK'.$row, $a->EffYesterday);
                    foreach(range('A','AK1') as $v){
                        $sheet->cell($v.$row, function($cell)use($color) { 
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                            $cell->setBackground($color); 
                        });
                    }
                    $row++;
                }
                
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->mergeCells('C1:C2');
                $sheet->mergeCells('D1:D2');
                $sheet->mergeCells('E1:E2');
                $sheet->mergeCells('F1:F2');
                $sheet->mergeCells('G1:G2');
                $sheet->mergeCells('H1:H2');
                $sheet->mergeCells('I1:I2');
                $sheet->mergeCells('J1:J2');
                $sheet->mergeCells('K1:K2');
                $sheet->mergeCells('L1:L2');
                $sheet->mergeCells('AD1:AD2');
                $sheet->mergeCells('AE1:AE2');
                $sheet->mergeCells('AF1:AF2');
                $sheet->mergeCells('AG1:AG2');
                $sheet->mergeCells('AH1:AH2');
                $sheet->mergeCells('AI1:AI2');
                $sheet->mergeCells('AJ1:AJ2');
                $sheet->mergeCells('AK1:AK2');
                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            
            /* $outputDefect       = DB::select(db::Raw("select * from f_hourly_defect('$startDate','$endDate') where factory_id='$factory_id'"));
            $excel->sheet('Output Defect', function($sheet) use($outputDefect){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line');
                $sheet->setCellValue('C1','Buyer');
                $sheet->setCellValue('D1','Category');
                $sheet->setCellValue('E1','Style');
                $sheet->setCellValue('F1','Jam Ke-');
                $sheet->mergeCells('F1:R1');
                $sheet->cell('F1:R1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('F2','7');
                $sheet->setCellValue('G2','8');
                $sheet->setCellValue('H2','9');
                $sheet->setCellValue('I2','10');
                $sheet->setCellValue('J2','11');
                $sheet->setCellValue('K2','12');
                $sheet->setCellValue('L2','13');
                $sheet->setCellValue('M2','14');
                $sheet->setCellValue('N2','15');
                $sheet->setCellValue('O2','16');
                $sheet->setCellValue('P2','17');
                $sheet->setCellValue('Q2','18');
                $sheet->setCellValue('R2','19');
                $sheet->setCellValue('S1','TOTAL');
                $row = 3;
                foreach ($outputDefect as $key => $a) {
                    $getBuyer    = $this->getBuyer($a->style,$a->line_id,'buyer');
                    $getCategory = $this->getBuyer($a->style,$a->line_id,'category');
                    
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $getBuyer);
                    $sheet->setCellValue('D'.$row, $getCategory);
                    $sheet->setCellValue('E'.$row, $a->style);
                    $sheet->setCellValue('F'.$row, $a->i);
                    $sheet->setCellValue('G'.$row, $a->ii);
                    $sheet->setCellValue('H'.$row, $a->iii);
                    $sheet->setCellValue('I'.$row, $a->iv);
                    $sheet->setCellValue('J'.$row, $a->v);
                    $sheet->setCellValue('K'.$row, $a->vi);
                    $sheet->setCellValue('L'.$row, $a->vii);
                    $sheet->setCellValue('M'.$row, $a->viii);
                    $sheet->setCellValue('N'.$row, $a->ix);
                    $sheet->setCellValue('O'.$row, $a->x);
                    $sheet->setCellValue('P'.$row, $a->xi);
                    $sheet->setCellValue('Q'.$row, $a->xii);
                    $sheet->setCellValue('R'.$row, $a->xiii);
                    $sheet->setCellValue('S'.$row, '=SUM(A'.$row.':R'.$row.')');
                    $sheet->cells('A'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('C'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('D'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('E'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('F'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('G'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('H'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('I'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('J'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('K'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('L'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('M'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('N'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('O'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('P'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('Q'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('R'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('S'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $row++;
                }
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->mergeCells('C1:C2');
                $sheet->mergeCells('D1:D2');
                $sheet->mergeCells('E1:E2');
                $sheet->mergeCells('S1:S2');
                $sheet->cells('A1:A2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('B1:B2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('C1:C2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('D1:D2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('E1:E2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F1:R1', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F2:R2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('S1:S2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
            });
            $excel->sheet('Efficency', function($sheet) use($data){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line');
                $sheet->setCellValue('C1','Buyer');
                $sheet->setCellValue('D1','Category');
                $sheet->setCellValue('E1','Style');
                $sheet->setCellValue('F1','Jam Ke-');
                $sheet->mergeCells('F1:R1');
                $sheet->cell('F1:R1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('F2','1');
                $sheet->setCellValue('G2','2');
                $sheet->setCellValue('H2','3');
                $sheet->setCellValue('I2','4');
                $sheet->setCellValue('J2','5');
                $sheet->setCellValue('K2','6');
                $sheet->setCellValue('L2','7');
                $sheet->setCellValue('M2','8');
                $sheet->setCellValue('N2','9');
                $sheet->setCellValue('O2','10');
                $sheet->setCellValue('P2','11');
                $sheet->setCellValue('Q2','12');
                $sheet->setCellValue('R2','13');
                $sheet->setCellValue('S1','TOTAL');
                $row = 3;
                foreach ($data as $key => $a) {
                    $getBuyer    = $this->getBuyer($a->style,$a->line_id,'buyer');
                    $getCategory = $this->getBuyer($a->style,$a->line_id,'category');
                    $jamKe = $this->HoursTo($a->date,$a->line_id);
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $getBuyer);
                    $sheet->setCellValue('D'.$row, $getCategory);
                    $sheet->setCellValue('E'.$row, $a->style);
                    $sheet->setCellValue('F'.$row, ($a->i==0)?'0':(($a->smv==null)?'0':(($a->i*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('G'.$row, ($a->ii==0)?'0':(($a->smv==null)?'0':(($a->ii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('H'.$row, ($a->iii==0)?'0':(($a->smv==null)?'0':(($a->iii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('I'.$row, ($a->iv==0)?'0':(($a->smv==null)?'0':(($a->iv*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('J'.$row, ($a->v==0)?'0':(($a->smv==null)?'0':(($a->v*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('K'.$row, ($a->vi==0)?'0':(($a->smv==null)?'0':(($a->vi*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('L'.$row, ($a->vii==0)?'0':(($a->smv==null)?'0':(($a->vii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('M'.$row, ($a->viii==0)?'0':(($a->smv==null)?'0':(($a->viii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('N'.$row, ($a->ix==0)?'0':(($a->smv==null)?'0':(($a->ix*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('O'.$row, ($a->x==0)?'0':(($a->smv==null)?'0':(($a->x*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('P'.$row, ($a->xi==0)?'0':(($a->smv==null)?'0':(($a->xi*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('Q'.$row, ($a->xii==0)?'0':(($a->smv==null)?'0':(($a->xii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('R'.$row, ($a->xiii==0)?'0':(($a->smv==null)?'0':(($a->xiii*$a->smv)/($a->present_sewer*1*60))));
                    $sheet->setCellValue('S'.$row, ($a->total==0)?'0':(($a->smv==null)?'0':(($jamKe==0)?'0':(($a->total*$a->smv)/($a->present_sewer*$jamKe*60)))));
                    $sheet->cells('A'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('C'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('D'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('E'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('F'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('G'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('H'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('I'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('J'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('K'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('L'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('M'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('N'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('O'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('P'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('Q'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('R'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('S'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->setColumnFormat(
                        array(
                            'F'.$row=>'0.00%',
                            'G'.$row=>'0.00%',
                            'H'.$row=>'0.00%',
                            'I'.$row=>'0.00%',
                            'J'.$row=>'0.00%',
                            'K'.$row=>'0.00%',
                            'L'.$row=>'0.00%',
                            'M'.$row=>'0.00%',
                            'N'.$row=>'0.00%',
                            'O'.$row=>'0.00%',
                            'P'.$row=>'0.00%',
                            'Q'.$row=>'0.00%',
                            'R'.$row=>'0.00%',
                            'S'.$row=>'0.00%',
                        )
                    );
                    $row++;
                }
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->mergeCells('C1:C2');
                $sheet->mergeCells('D1:D2');
                $sheet->mergeCells('E1:E2');
                $sheet->mergeCells('S1:S2');
                $sheet->cells('A1:A2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('B1:B2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('C1:C2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('D1:D2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('E1:E2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F1:R1', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F2:R2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('S1:S2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
            }); */
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    private function getBuyer($style,$line_id,$remark){
        $production = Production::where(db::raw('style'),'LIKE',"$style%")
        ->where('line_id',$line_id)
        ->orderBy('created_at','desc')
        ->first();
        $breakDown = BreakDownSize::where('poreference',($production)?$production->poreference:'')
        ->first();
        if ($remark=='buyer') {
            if ($breakDown) {
                return $breakDown->buyer;
            } else {
                return '';
            }
            
        } else if ($remark=='category'){
            if ($breakDown) {
                return $breakDown->category;
            } else {
                return '';
            }
        }
    }
    private function HoursTo($date,$line_id)
    {
        
        $now          = Carbon::now()->format('Y-m-d H:i:s');
        $dateNow      = Carbon::createFromFormat('Y-m-d H:i:s',$now)->format('Y-m-d');
        if ($date==$dateNow) {
            $dailyWorking = DailyWorking::whereNull('delete_at')
            ->where(db::raw('date(created_at)'),$date)
            ->where('line_id',$line_id)
            ->first();
            $start = ($dailyWorking)?$dailyWorking->start:'24:00';
            // dd($start);
            $workingHours = WorkingHours::where('start',">=",$start)
            ->orderBy('start','ASC');
            $hourTo =0;
            $jamKe =0;
            foreach ($workingHours->get() as $key => $working) {
                
                if($working->start>=$dailyWorking->start)
                {
                    $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
                    $akhir = Carbon::now()->format('Y-m-d').' '.$working->end;
                    
                    
                    if($now >= $awal && $now < $akhir){
                        $start        = $awal;
                        $end          = $akhir;
                        $hourTo       = $key;
                        $current      = $working->name;
                        $awalCurrent  = $working->start;
                        $akhirCurrent = $working->end;
                    }
                }
            }
            if ($dailyWorking) {
                if ($hourTo==0) {
                    $jamKe = $hourTo+1;
                } else {
                    if($hourTo>5){
                        $jamKe = $hourTo-1;
                    }else{
                        $jamKe= $hourTo;
                    }
                }
                
            } else {
                $jamKe = 0;
            }
            
        } else {
            $jamKe = 8;
        }
        
        /* $jamKe = (!$dailyWorking)?'':(($hourTo==0)?$hourTo+1:$hourTo);
        $data = ($dailyWorking)?(($date!=$dateNow||$jamKe>=$dailyWorking->working_hours)?8:$jamKe):0; */
        return $jamKe;
    }
    private function dailyLine($date,$line_id,$jamke)
    {
        $actualOutput = 0;
        $targetOutput = 0;
        $outputQc     = 0;
        $data         = DB::table('daily_line_v')
        ->whereNull('delete_at')
        ->where(db::raw('date(created_at)'),$date)
        ->where('line_id',$line_id);
        foreach ($data->get() as $key => $d) {
            $outputQc = $this->getSumProductionaStyle($d->style,Carbon::createFromFormat('Y-m-d H:i:s',$d->created_at)->format('Y-m-d'),'output_qc_endline',$d->line_id);
            $actualOutput +=$outputQc*$d->smv;
            $targetOutput += $d->commitment_line*$d->smv;
        }
        if ($actualOutput!=0) {
            if ($data->first()) {
                if ($jamke!=0) {
                    $eff =  (($actualOutput)/($data->first()->present_sewer*$jamke*60))*100;
                    return $eff;
                }else{
                    return 0;
                }
                
            } else {
                return 0;
            }
            
        } else {
            return 0;
        }
    }
    private function TotalOutput($date,$line_id)
    {
        $outputQc     = 0;
        $actualOutput     = 0;
        $data         = DB::table('daily_line_v')
        ->whereNull('delete_at')
        ->where(db::raw('date(created_at)'),$date)
        ->where('line_id',$line_id);
        foreach ($data->get() as $key => $d) {
            $outputQc = $this->getSumProductionaStyle($d->style,Carbon::createFromFormat('Y-m-d H:i:s',$d->created_at)->format('Y-m-d'),'output_qc_endline',$d->line_id);
            $actualOutput +=$outputQc;
        }
        return $actualOutput;
    }
    private function getSumProductionaStyle($style,$date,$name,$line_id)
    {
        $dataSum = SummaryProduction::where('summary_productions.name',$name)
        ->leftJoin('styles','styles.erp','summary_productions.style')
        ->where('styles.default',$style)
        ->where('summary_productions.date',$date)
        ->where('summary_productions.line_id',$line_id)
        ->sum('summary_productions.output');
        return $dataSum;
    }
    private function getWipStyle($factory_id,$date,$style,$line_id)
    {
        $data      = DB::select(db::Raw("select COALESCE(sum(qty_order)-sum(counter_day),0) as wip from f_qc_history_output('$factory_id','$date')where style_merge='$style' and line_id='$line_id'"));
        return ($data?$data[0]->wip:0);
    }
    private function getOutputLoading($date,$style,$line_id)
    {
        $data = ProductionSizeHistory::select('styles.default',db::raw('sum(production_size_histories.qty_new) as qty'))
        ->leftJoin('production_sizes','production_sizes.id','production_size_histories.production_size_id')
        ->leftJoin('productions','productions.id','production_sizes.production_id')
        ->leftJoin('styles','styles.erp','productions.style')
        ->where(db::raw('date(production_size_histories.created_at)'),$date)
        ->where('styles.default',$style)
        ->where('productions.line_id',$line_id)
        ->groupBy('styles.default')
        ->first();
        return ($data?$data->qty:0);
    }
}