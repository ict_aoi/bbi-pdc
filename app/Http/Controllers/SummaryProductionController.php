<?php namespace App\Http\Controllers;

use DB;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Sewing;
use App\Models\QcEndline;
use App\Models\SewingDetail;
use App\Models\WorkingHours;
use App\Models\DailyLineView;
use App\Models\QcInlineDetail;
use App\Models\QcEndlineDetail;
use App\Models\QcEndlineDefect;
use App\Models\SummaryProduction;
class SummaryProductionController extends Controller
{
    static function CronInserProductionHourly()
    {
        $workingHours = DB::table('working_hours2')->OrderBy('start','asc')->get();
        $previousValue = null;
        foreach ($workingHours as $key => $working) {
            $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
            $akhir  = Carbon::now()->format('Y-m-d').' '.$working->end;
			
            $now   = Carbon::now()->format('Y-m-d H:i:s');
            // dd($awal."-".$akhir);
		  	if($now > $awal && $now < $akhir){
                try {
                    DB::beginTransaction();
                    if (Carbon::createFromFormat('Y-m-d H:i:s',$now)->format('i')=='00') {
                        if($previousValue) {
                            // $previous = WorkingHours::find($previousValue);
                            $previous = DB::table('working_hours2')->where('id',$previousValue)->first();
                            if ($previous) {
                                $previousAwal  = Carbon::now()->format('Y-m-d').' '.$previous->start;
                                $previousAkhir = Carbon::now()->format('Y-m-d').' '.$previous->end;
                            }
                            SummaryProductionController::sumSewing($previousAwal,$previousAkhir);
                            SummaryProductionController::sumQcEndline($previousAwal,$previousAkhir);
                            SummaryProductionController::sumDefectQcEndline($previousAwal,$previousAkhir);
                            SummaryProductionController::defectQcEndlineOutStanding($previousAwal,$previousAkhir);
                            SummaryProductionController::sumDefectQcInline($previousAwal,$previousAkhir);
                        }
    
                    }
                    SummaryProductionController::sumSewing($awal,$akhir);
                    SummaryProductionController::sumQcEndline($awal,$akhir);
                    SummaryProductionController::sumDefectQcEndline($awal,$akhir);
                    SummaryProductionController::defectQcEndlineOutStanding($awal,$akhir);
                    SummaryProductionController::sumDefectQcInline($awal,$akhir);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
				
				

			}
		  
		  $previousValue = $working->id;
        }
    }
    static function CronInserProductionDaily()
    {
        // $workingHours = WorkingHours::OrderBy('start','asc')->get();
        $workingHours = DB::table('working_hours2')->orderBy('start','asc')->get();
        $date = Carbon::now();
        $startDate    = Carbon::createFromFormat('Y-m-d H:i:s',  $date)->format('Y-m-d');
        $endDate      = Carbon::createFromFormat('Y-m-d H:i:s',  $date)->format('Y-m-d');
        // $startDate    = '2023-07-18';
        // $endDate      = '2023-07-18';
        try {
            DB::beginTransaction();
            for ($i=$startDate; $i <= $endDate; $i++) { 
                SummaryProduction::where('date',$i)
                ->delete();
                foreach ($workingHours as $key => $working) {
                    $awal  = $i.' '.$working->start;
                    $akhir = $i.' '.$working->end;
                    SummaryProductionController::sumSewing($awal,$akhir);
                    SummaryProductionController::sumQcEndline($awal,$akhir);
                    SummaryProductionController::sumDefectQcEndline($awal,$akhir);
                    SummaryProductionController::defectQcEndlineOutStanding($awal,$akhir);
                    SummaryProductionController::sumDefectQcInline($awal,$akhir);
                }
            }
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }
    static function CronInserProductionRepeat()
    {
        $workingHours = WorkingHours::OrderBy('start','asc')->get();
        $date = Carbon::now();
        $startDate    = Carbon::createFromFormat('Y-m-d H:i:s',  $date)->format('Y-m-d');
        $endDate      = Carbon::createFromFormat('Y-m-d H:i:s',  $date)->format('Y-m-d');
        try {
            DB::beginTransaction();
            for ($i=$startDate; $i <= $endDate; $i++) { 
                SummaryProduction::where('date',$i)
                ->delete();
                foreach ($workingHours as $key => $working) {
                    $awal  = $i.' '.$working->start;
                    $akhir = $i.' '.$working->end;
                    SummaryProductionController::sumSewing($awal,$akhir);
                    SummaryProductionController::sumQcEndline($awal,$akhir);
                    SummaryProductionController::sumDefectQcEndline($awal,$akhir);
                    SummaryProductionController::defectQcEndlineOutStanding($awal,$akhir);
                    SummaryProductionController::sumDefectQcInline($awal,$akhir);
                }
            }
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }
    static function sumSewing($awal,$akhir)
    {
        $getSumSewings = SewingDetail::select('sewings.line_id','sewings.style','sewings.article',db::raw('count(*) as output_sewing'),db::raw('date(sewing_details.created_at) as created_at'))
        ->leftJoin('sewings','sewings.id','sewing_details.sewing_id')
        ->where('sewing_details.created_at','>=',$awal)
        ->where('sewing_details.created_at','<',$akhir)
        ->groupBy('sewings.line_id','sewings.style','sewings.article',db::raw('date(sewing_details.created_at)'))->get();
        foreach ($getSumSewings as $key => $sewing) {
            $name                 = 'sewing';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where([
                ['date',$date],
                ['end_hours',$akhir],
                ['line_id',$sewing->line_id],
                ['style',$sewing->style],
                ['article',$sewing->article],
                ['name',$name],
            ])->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $sewing->line_id,
                'name'        => $name,
                'style'       => $sewing->style,
                'article'     => $sewing->article,
                'output'      => $sewing->output_sewing,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $sewing->created_at
            ]);
        }
    }
    static function sumQcEndline($awal,$akhir)
    {
        $getSumQcEndline = QcEndlineDetail::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_details.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_details.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_details.counter','<>','0')
        ->where('qc_endline_details.created_at','>=',$awal)
        ->where('qc_endline_details.created_at','<',$akhir)
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_details.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'output_qc_endline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where([
                ['date',$date],
                ['end_hours',$akhir],
                ['line_id',$qc->line_id],
                ['style',$qc->style],
                ['poreference',$qc->poreference],
                ['size',$qc->size],
                ['article',$qc->article],
                ['name',$name],
            ])->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    }
    static function sumDefectQcEndline($awal,$akhir)
    { 
        $getSumQcEndline = QcEndlineDefect::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_defects.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_defects.created_at','>=',$awal)
        ->where('qc_endline_defects.created_at','<',$akhir)
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_defects.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'output_defect_endline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where([
                ['date',$date],
                ['end_hours',$akhir],
                ['line_id',$qc->line_id],
                ['style',$qc->style],
                ['poreference',$qc->poreference],
                ['size',$qc->size],
                ['article',$qc->article],
                ['name',$name],
            ])->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    }
    static function defectQcEndlineOutStanding($awal,$akhir)
    { 
        $getSumQcEndline = QcEndlineDefect::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_defects.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_defects.created_at','>=',$awal)
        ->where('qc_endline_defects.created_at','<',$akhir)
        ->where(db::raw("date_part('hour', qc_endline_defects.update_defect_date)"),'<>',db::raw("date_part('hour', qc_endline_defects.created_at)"))
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_defects.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'defect_endline_outstanding';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                 = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where([
                ['date',$date],
                ['end_hours',$akhir],
                ['line_id',$qc->line_id],
                ['style',$qc->style],
                ['poreference',$qc->poreference],
                ['size',$qc->size],
                ['article',$qc->article],
                ['name',$name],
            ])->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    }
    static function sumDefectQcInline($awal,$akhir)
    {
        $getSumQcInline = QcInlineDetail::select('productions.line_id','productions.style','productions.poreference','productions.article',db::raw('count(*) as output_defect_inline'),db::raw('date(qc_inline_details.created_at) as created_at'))
        ->leftJoin('qc_inlines','qc_inlines.id','qc_inline_details.qc_inline_id')
        ->leftJoin('productions','productions.id','qc_inlines.production_id')
        ->where('defect_id','!=','0')
        ->where('qc_inline_details.created_at','>=',$awal)
        ->where('qc_inline_details.created_at','<',$akhir)
        ->groupBy(db::raw('date(qc_inline_details.created_at)'),'productions.line_id','productions.style','productions.poreference','productions.article');
        foreach ($getSumQcInline->get() as $key => $qc) {
            $name                 = 'output_defect_inline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where([
                ['date',$date],
                ['end_hours',$akhir],
                ['line_id',$qc->line_id],
                ['style',$qc->style],
                ['poreference',$qc->poreference],
                ['size',$qc->size],
                ['article',$qc->article],
                ['name',$name],
            ])->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'article'     => $qc->article,
                'output'      => $qc->output_defect_inline,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }

    }
    static function sendAllertProduction()
    {
        $now       = Carbon::now();
        $hours     = $now->format('H:i:s');
        $dateNow   = $now->format('Y-m-d');

        $dailyLines = DailyLineView::select('line_id','start','end','name','working_hours',db::raw('sum(commitment_line) as commitment_line'),'urut')
        ->whereNull('delete_at')
        ->where(db::raw('date(created_at)'),$dateNow)
        ->groupBy('line_id','start','end','working_hours','urut','name')
        ->orderBy('urut','asc');
        $pesan      = '';
        $count        = 0;
        $no            = 1;
        foreach ($dailyLines->get() as $key => $daily) {
            
            $workingHours = WorkingHours::OrderBy('start','asc')->get();
            
            $previousValue = null;
            
            foreach ($workingHours as $key => $working) {
                $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
                $akhir = Carbon::now()->format('Y-m-d').' '.$working->end;
                
                $nows          = $now->format('Y-m-d H:i:s');
               
                
                if($now > $awal && $now < $akhir){
                    if($previousValue) {
                        // $previous = WorkingHours::find($previousValue);
                        $previous   = WorkingHours::where('id',$previousValue)->first();
                        $achivement = 0;
                        
                        
                        if ($previous) {
                            
                            $finish        = Carbon::parse($hours);
                            $hourTo        = $finish->diffInHours($daily->start)-1;
                            $jam           = (($finish->diffInHours($daily->start))>5?($finish->diffInHours($daily->start)-1):($finish->diffInHours($daily->start)));
                            $target        = floor($daily->commitment_line/$daily->working_hours);
                            $previousAwal  = Carbon::now()->format('Y-m-d').' '.$previous->start;
                            $previousAkhir = Carbon::now()->format('Y-m-d').' '.$previous->end;
                            $data          = SummaryProduction::select(db::raw('sum(output) as output'))
                            ->where([
                                ['line_id',$daily->line_id],
                                ['start_hours',$previous->start],
                                ['end_hours',$previous->end],
                                ['date',$dateNow],
                                ['name','output_qc_endline']
                            ])
                            ->first();
                            $achivement = ($data->output==null)?0:$data->output;
                            $keterangan = ($hourTo==5)?'(istirahat)':'';
                            if ($jam>0) {
                                if ($daily->working_hours>=$jam) {
                                    if ($achivement<$target) {
                                        $pesan .= "*".$no.".".ucwords($daily->name)."*_(jam ke-".$jam.")_|*Target*  = ".$target."|*Tercapai*  = ".$achivement.$keterangan."|--------------------------------------------------------- |";
                                        $no++;
                                        $count++;
                                    }
                                }
                            }
                            
                            
                        }
                    }
                   
                }
                $previousValue = $working->id;
            }
            

        }
        if ($count>0) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';

            DB::connection($connection_mid)
            ->table('whatsapp')
            ->insert([
                'contact_number' => 'Digitalisasi PDC AOI 3',
                'is_group' => 't',
                'message' => '*Perhatian*|Berikut ini line yang tidak mencapai target :|'.$pesan.'|Atas kerjasamanya.|Terimakasih.|_*info lebih lengkap silahkan kunjungi_|http://dashboard.bbigarment.co.id/pdc-efficiency'
            ]);
        }
    }

}