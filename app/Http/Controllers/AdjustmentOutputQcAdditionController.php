<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Style;
use App\Models\Sewing;
use App\Models\Factory;
use App\Models\QcEndline;
use App\Models\Production;
use App\Models\SewingDetail;
use App\Models\WorkingHours;
use App\Models\ProductionSize;
use App\Models\QcEndlineDetail;
use App\Models\QcEndlineDefect;
use App\Models\SummaryProduction;
use App\Models\AdjustmentOutputQcAddition;

class AdjustmentOutputQcAdditionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        //return Carbon::now()->format('j F, Y');
        /*  */
        return view('adjustment_output_qc_addition.index');
    }
    public function data(Request $request)
    {
    	if(request()->ajax())
        {
            
            $buyer   = $this->Buyer();
            $data    = AdjustmentOutputQcAddition::select(
                db::raw('date(adjustment_output_qc_additions.created_at) as date'),
                'lines.name',
                'lines.code',
                'productions.style',
                'productions.poreference',
                'productions.article',
                'production_sizes.size',
                'adjustment_output_qc_additions.qty_revision',
                'adjustment_output_qc_additions.request_by_name',
                'adjustment_output_qc_additions.request_by_nik',
                'adjustment_output_qc_additions.reason'
                )
            ->leftJoin('qc_endlines','qc_endlines.id','adjustment_output_qc_additions.qc_endline_id')
            ->leftJoin('productions','productions.id','qc_endlines.production_id')
            ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
            ->leftJoin('lines','lines.id','productions.line_id')
            ->whereIn('lines.buyer',$buyer)
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->orderby('adjustment_output_qc_additions.created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('tanggal',function($data){
            	// return Carbon::createFromFormat('Y-m-d H:i:s',$data->date)->format('d-m-Y H:i:s');
                return $data->date;
            })
            ->editColumn('code',function($data){
            	return $data->code;
            })
            ->editColumn('line_name',function($data){
            	return strtoupper($data->name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('poreference',function($data){
            	return $data->poreference;
            })
            ->editColumn('article',function($data){
            	return $data->article;
            })
            ->editColumn('size',function($data){
            	return $data->size;
            })
            ->editColumn('qty_revision',function($data){
            	return $data->qty_revision;
            })
            ->editColumn('qc_name',function($data){
            	return ucwords($data->request_by_nik)."-".$data->request_by_name;
            })
            ->editColumn('reason',function($data){
            	return ucwords($data->reason);
            })
            
            ->make(true);
        }
        
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function create(Request $request)
    {
        
        return view('adjustment_output_qc_addition.create');
    }
    function linePicklist(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $buyer = $this->Buyer();
            $lists = Line::whereNull('delete_at')
            ->whereIn('buyer',$buyer)
            ->where([
                ['factory_id',Auth::user()->factory_id],
                [db::raw('lower(alias)'), 'LIKE', "%$q%"],
                ['code','<>','all'],

            ])
            ->orderBy('order','asc')
            ->paginate(10);

            return view('adjustment_output_qc_addition._line_picklist', compact('lists'));
        }
    }
    public function poreferencePickList(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $line_id = $request->line_id;
            $lists = DB::table('qc_output_v')
            ->distinct()
            ->select('production_id','style','poreference','article','start_date')
            ->where('line_id', $line_id)
            ->where(db::raw('lower(poreference)'), 'LIKE', "%$q%")
            ->whereRaw('balance<0')
            ->orderBy('start_date','desc')
            ->paginate(10);

            return view('adjustment_output_qc_addition._poreference_picklist', compact('lists'));
        }
    }
    public function QcOutputData(Request $request)
    {
        if(request()->ajax())
        {
            $date          = ($request->date)?Carbon::createFromFormat('m/d/Y',$request->date)->format('Y-m-d'):Carbon::now()->format('Y-m-d');
            $production_id = $request->production_id;
            $line_id       = $request->line_id;
            $data          = DB::table('qc_output_v')
            ->where('production_id',$production_id)
            ->where('line_id',$line_id);
            
            return datatables()->of($data)
            ->editColumn('line_name',function($data){
            	return strtoupper($data->line_name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('poreference',function($data){
            	return $data->poreference;
            })
            ->editColumn('article',function($data){
            	return $data->article;
            })
            ->editColumn('size',function($data){
            	return $data->size;
            })
            ->editColumn('total_output',function($data){
            	return $data->total_output;
            })
            ->editColumn('balance',function($data){
            	return $data->balance;
            })
            ->addColumn('action', function($data) {
                
                return view('adjustment_output_qc_addition._action', [
                    'model' => $data,
                    'good' => route('adjustment_output_qc_addition.add_output',[$data->production_id,$data->production_size_id]),
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function addOutput(Request $request,$production_id,$production_size_id)
    {
        $startNow = Carbon::now()->format('H').':00:00';
        
        $data = DB::table('qc_output_v')
        ->where('production_id',$production_id)
        ->where('production_size_id',$production_size_id)
        ->first();
        $workingHours = WorkingHours::where('start','<=',$startNow)
        ->OrderBy('start','asc')->pluck('name', 'id')->all();
        
        return view('adjustment_output_qc_addition.add_output', compact('data','workingHours'));
    }
    static function getSewer(Request $request)
    {
        if ($request->ajax()) {
            $factory_id = Auth::user()->factory_id;
            $nik        = $request->nik;
            $factory    = Factory::find($factory_id);
            
            if($factory)
            {
                $absences = DB::connection('bbi_apparel')
                ->table('get_employee') 
                ->select('nik','name','department_name','subdept_name')
                ->where('nik',$nik)
                ->first();

                if (!$absences) {
                    return response()->json('Nik Tidak ada.', 422);
                } else {
                    $obj = new StdClass();
                    $obj->name = $absences->name;
                    $obj->nik = $absences->nik;
                    return  response()->json($obj, 200);
                }
                
            }else
            {
                return response()->json('Ada Kesalahan silahkan refresh halaman anda', 422);
            }
        }
    }
    public function storeRevision(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $now                = Carbon::now()->format('Y-m-d');
            $date               = ($request->date? Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d') :Carbon::now()->format('Y-m-d'));
            $working_hours_id   = $request->hours;
            $qc_endline_id      = $request->qc_endline_id;
            $revisi_output      = (int)$request->revisi_output;
            $production_id      = $request->production_id;
            $production_size_id = $request->production_size_id;
            $productionSize     = ProductionSize::find($production_size_id);
            $Production         = Production::find($production_id);
            $qcEndline          = QcEndline::where([
                ['production_id',$production_id],
                ['production_size_id',$production_size_id],
            ])->first();
            $counter            = ($qcEndline?$qcEndline->counter:0)+$revisi_output;
            $balance            = $productionSize->qty-$counter;
            if ($working_hours_id) {
                $working    = WorkingHours::find($working_hours_id);
                $hours      = Carbon::createFromFormat('H:i:s', $working->start)->format('H').":30:00";
                $created_at = $date.' '.$hours;
            } else {
                $hours      = Carbon::now()->format('H:i:s');
                $created_at = $date.' '.$hours;
            }
            
            if ($balance<0) {
                return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
            }
            // dd(!$qcEndline);
            if (!$qcEndline) {
                $qc_endlines = QcEndline::FirstOrCreate([
                    'production_id'      => $production_id,
                    'production_size_id' => $production_size_id,
                    'created_at'         => $created_at,
                    'updated_at'          => $created_at
                ]);
                $qcEndline     = $qc_endlines;
                $qc_endline_id = $qc_endlines->id;
            }
            try {
                DB::beginTransaction();
                $this->getWip($Production,$revisi_output,$now,$created_at);
                
                
                for ($i=0; $i < $revisi_output; $i++) { 
                    QcEndlineDetail::create([
                        'qc_endline_id'   => $qc_endline_id,
                        'counter'         => 1,
                        'ip_address'      => \Request::ip(),
                        'created_at'      => $created_at,
                        'updated_at'       => $created_at,
                        'created_by_name' => $request->nik,
                        'created_by_nik'  => $request->name_label
                    ]);
                }
                $date = Carbon::now()->format('Y-m-d');
                DB::select(db::Raw("select * from update_counter('$qc_endline_id')"));
                DB::select(db::Raw("select * from update_counter_qc('$qc_endline_id','$date')"));
                DB::select(db::Raw("select * from update_repair_qc('$qc_endline_id')"));
                
                AdjustmentOutputQcAddition::create([
                    'qc_endline_id'   => $qc_endline_id,
                    'qty_revision'    => $request->revisi_output,
                    'request_by_name' => $request->name_label,
                    'request_by_nik'  => $request->nik,
                    'reason'          => $request->reason,
                    'date_output'     => $request->date,
                    'create_user_id'  => Auth::user()->id
                ]);
                $this->pullBackSummaryProduction($created_at,$Production->line_id,$Production->style,$Production->poreference,$Production->article,$productionSize->size);
                
                DB::commit();
                return response()->json(['success','qc_endline_id'=>$qc_endline_id],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    private function getWip($production,$revisi_output,$now,$created_at){
        $styles          = Style::where('erp',$production->style)->first();
        $styleDefault    = ($styles)?$styles->default:$production->style;
        $sewing          = SewingDetail::getCounterSewing($styleDefault,$production->line_id,$created_at,$production->article);
        $counterSewing   = $sewing?$sewing[0]->total_counter:0;
        $counterEndStyle = QcEndlineDetail::getCounterQcDetail($styleDefault,$production->line_id,$created_at,$production->article);
        $wip             = ($counterSewing)-($counterEndStyle?$counterEndStyle[0]->total_counter:0);
        $available       = $revisi_output-$wip;
        $cekSewing = Sewing::where('line_id',$production->line_id)
                    ->where('factory_id',$production->factory_id)
                    ->where('style',$styleDefault)
                    ->where('article',$production->article)
                    ->first();
            
            if (!$cekSewing) {
                $sewing = Sewing::Create([
                    'factory_id' => $production->factory_id,
                    'line_id'    => $production->line_id,
                    'style'      => $production->style,
                    'article'    => $production->article
                ]);
                $sewing_id = $sewing->id;
            }else{
                $sewing_id = $cekSewing->id;
            }
            
            for ($i=0; $i < $available ; $i++) { 
                SewingDetail::Create([
                    'sewing_id'  => $sewing_id,
                    'counter'    => '1',
                    'created_at' => $created_at,
                    'update_at'  => $created_at,
                    'ip_address' => \Request::ip()
                ]);
            }
            DB::select(db::Raw("select * from update_counter_sewing_detail('$sewing_id')"));
            DB::select(db::Raw("select * from update_counter_sewing('$sewing_id','$now')"));
    }
    private function pullBackSummaryProduction($created_at,$line_id,$style,$poreference,$article,$size){
        // dd($date,$line_id,$style,$poreference,$article,$size);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $created_at)->format('Y-m-d');
        $workingHours = WorkingHours::OrderBy('start','asc')->get();
        for ($i=$date; $i <= $date; $i++) { 
            SummaryProduction::where('date',$i)
            ->where('line_id',$line_id)
            ->where('style',$style)
            ->where('poreference',$poreference)
            ->where('article',$article)
            ->where('size',$size)
            ->whereIn('name',['output_qc_endline','output_defect_endline'])
            ->delete();
            foreach ($workingHours as $key => $working) {
                $awal  = $i.' '.$working->start;
                $akhir = $i.' '.$working->end;
                $this->sumQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size);
                $this->sumSewing($awal,$akhir,$line_id,$style,$article);
            }
        }
    }
    private function sumQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size)
    {
        $getSumQcEndline = QcEndlineDetail::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_details.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_details.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_details.counter','<>','0')
        ->where('qc_endline_details.created_at','>=',$awal)
        ->where('qc_endline_details.created_at','<',$akhir)
        ->where('productions.line_id',$line_id)
        ->where('productions.style',$style)
        ->where('productions.poreference',$poreference)
        ->where('productions.article',$article)
        ->where('production_sizes.size',$size)
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_details.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'output_qc_endline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where('end_hours',$akhir)
            ->where('line_id',$qc->line_id)
            ->where('style',$qc->style)
            ->where('poreference',$qc->poreference)
            ->where('size',$qc->size)
            ->where('article',$qc->article)
            ->where('name',$name)
            ->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    }
    static function sumSewing($awal,$akhir,$line_id,$style,$article)
    { 
        // echo $awal."-".$akhir."-".$line_id."-".$style."-".$article;
        $getSumSewings = SewingDetail::select('sewings.line_id','sewings.style','sewings.article',db::raw('count(*) as output_sewing'),db::raw('date(sewing_details.created_at) as created_at'))
        ->leftJoin('sewings','sewings.id','sewing_details.sewing_id')
        ->where('sewing_details.created_at','>=',$awal)
        ->where('sewing_details.created_at','<',$akhir)
        ->where('sewings.line_id',$line_id)
        ->where('sewings.style',$style)
        ->where('sewings.article',$article)
        ->groupBy('sewings.line_id','sewings.style','sewings.article',db::raw('date(sewing_details.created_at)'))->get();
        
        foreach ($getSumSewings as $key => $sewing) {
            $name                 = 'sewing';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where('end_hours',$akhir)
            ->where('line_id',$sewing->line_id)
            ->where('style',$sewing->style)
            ->where('article',$sewing->article)
            ->where('name',$name)
            ->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $sewing->line_id,
                'name'        => $name,
                'style'       => $sewing->style,
                'article'     => $sewing->article,
                'output'      => $sewing->output_sewing,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $sewing->created_at
            ]);
        }
    }
}