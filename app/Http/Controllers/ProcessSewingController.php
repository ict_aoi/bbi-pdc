<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use App\Models\Factory;
use Illuminate\Http\Request;
use App\Models\ProcessSewing;

use Yajra\Datatables\Datatables;

class ProcessSewingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        $factory = Factory::active()->pluck('name','id')->all();
        return view('process.index',compact('msg','factory'));
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $factory_id = $request->factory_id;
        return view('process.create',compact('factory_id'));
    }

    public function data(Request $request)
    {
    	if(request()->ajax())
        {
            $data = ProcessSewing::whereNull('delete_at')
            ->where('factory_id',$request->factory)
            ->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('component',function($data){
            	return ucwords($data->component);
            })
            ->editColumn('smv',function($data){
            	return ucwords($data->smv);
            })
            ->addColumn('action', function($data) {
                return view('process._action', [
                    'model' => $data,
                    'edit' => route('process_sewing.edit',$data->id),
                    'delete' => route('process_sewing.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required|string',
            'component' => 'required|string',
            'smv'       => 'required',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $component  = strtolower(trim($request->component));
        $name       = strtolower(trim($request->name));
        $factory_id = $request->factory_id;
        $smv        = $request->smv;
        $is_exists  = ProcessSewing::whereNull('delete_at')
                    ->where([
                        ['factory_id',$factory_id],
                        [db::raw("regexp_replace(component, '[\s+]', '', 'g')"),str_replace(" ","",$component)],
                        [db::raw("regexp_replace(name, '[\s+]', '', 'g')"),str_replace(" ","",$name)],
                    ])
                    ->exists();
        if ($is_exists) {
            return response()->json(['message' => 'Process Sudah Ada'], 422);
        }
    	try {
            DB::beginTransaction();
            ProcessSewing::FirstOrCreate([
                'factory_id'     => $factory_id,
                'name'           => $name,
                'component'      => $component,
                'smv'            => $smv,
                'create_user_id' => Auth::user()->id
            ]);
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    public function edit(Request $request,$id)
    {
		if ($request->ajax()) {
            $data = ProcessSewing::find($id);
            return view('process.edit',compact('data'));
        }
    }

    public function update(Request $request,$id)
    {
        if ($request->ajax()) {
            $this->validate($request,[
                'name'      => 'required',
                'component' => 'required',
                'smv'       => 'required',
            ]);
            $component  = strtolower(trim($request->component));
            $name       = strtolower(trim($request->name));
            $factory_id = $request->factory_id;
            $smv        = $request->smv;
            if (ProcessSewing::where([
                ['factory_id',$factory_id],
                [db::raw("regexp_replace(component, '[\s+]', '', 'g')"),str_replace(" ","",$component)],
                [db::raw("regexp_replace(name, '[\s+]', '', 'g')"),str_replace(" ","",$name)],
                ['id','!=',$id],
            ])
            ->whereNull('delete_at')
            ->exists())
            {
                return response()->json(['message'=>'Nama Proses Sudah ada'],422);
            }
    
    
            try 
            {
                DB::beginTransaction();
                $updateProcess                 = ProcessSewing::findorFail($id);
                $updateProcess->name           = strtolower(trim($request->name));
                $updateProcess->component      = strtolower(trim($request->component));
                $updateProcess->smv            = strtolower(trim($request->smv));
                $updateProcess->update_at      = carbon::now();
                $updateProcess->update_user_id = Auth::user()->id;
                
                $updateProcess->save();
                DB::commit();
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $update                 = ProcessSewing::findorFail($id);
            $update->delete_at      = carbon::now();
            $update->delete_user_id = Auth::user()->id;
            $update->save();

            return response()->json('success',200);
        }
    }
}