<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Factory;
use App\Models\Machine;

class MachineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        $factory = Factory::active()->pluck('name','id')->all();
        return view('mesin.index',compact('msg','factory'));
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $factory = $request->factory;
        return view('mesin.create',compact('factory'));
    }

    public function data(Request $request)
    {
    	if(request()->ajax())
        {
            $factory=$request->factory;
            $data = Machine::whereNull('delete_at')
            ->where('factory_id',$factory)
            ->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('category_machine',function($data){
                if ($data->is_special=='t') {
                    return '<span class="label label-info">Special</span>';
                }else{
                    return '<span class="label label-primary">Normal</span>';
                }
            })
            ->addColumn('action', function($data) {
                return view('mesin._action', [
                    'model' => $data,
                    'edit' => route('mesin.edit',$data->id),
                    'delete' => route('mesin.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action','category_machine'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
    	if ($request->ajax()) {
            $code       = strtolower(trim($request->code));
            $name       = strtolower(trim($request->name));
            $factory_id = $request->factory_id;
            $is_special = isset($request->is_special);
            try {
                DB::beginTransaction();
                $is_exists = Machine::whereNull('delete_at')
                ->where([
                    ['factory_id',$factory_id],
                    [db::raw("regexp_replace(name, '[\s+]', '', 'g')"),str_replace(" ","",$name)],
                    [db::raw("regexp_replace(code, '[\s+]', '', 'g')"),str_replace(" ","",$code)],
                    ['is_special',$is_special],
                ])
                ->exists();
                if ($is_exists) {
                    return response()->json(['message' => 'Machine Sudah Ada'], 422);
                }
                Machine::Create([
                    'factory_id'     => $factory_id,
                    'name'           => $name,
                    'code'           => $code,
                    'is_special'     => $is_special,
                    'create_user_id' => Auth::user()->id
                ]);
                DB::commit();
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }
    public function edit(Request $request,$id)
    {
		if ($request->ajax()) {
            $data = Machine::find($id);
            return view('mesin.edit',compact('data'));
        }
    }

    public function update(Request $request,$id)
    {
        if ($request->ajax()) {
            $this->validate($request,[
                'name'=>'required',
                'code'=>'required'
            ]);
            
            $code       = strtolower(trim($request->code));
            $name       = strtolower(trim($request->name));
            $factory_id = $request->factory_id;
            $is_special = isset($request->is_special);
            
            if (Machine::where([
                ['factory_id',$factory_id],
                [db::raw("regexp_replace(name, '[\s+]', '', 'g')"),str_replace(" ","",$name)],
                [db::raw("regexp_replace(code, '[\s+]', '', 'g')"),str_replace(" ","",$code)],
                ['is_special',$is_special],
                ['id','!=',$id],
            ])
            ->whereNull('delete_at')
            ->exists())
            {
                return response()->json(['message'=>'Nama Mesin Sudah ada'],422);
            }
    
    
            try 
            {
                DB::beginTransaction();
                $updateMesin                 = Machine::findorFail($id);
                $updateMesin->name           = $name;
                $updateMesin->code           = $name;
                $updateMesin->is_special     = $is_special;
                $updateMesin->update_at      = carbon::now();
                $updateMesin->update_user_id = Auth::user()->id;
                
                $updateMesin->save();
                DB::commit();
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $update                 = Machine::findorFail($id);
            $update->delete_at      = carbon::now();
            $update->delete_user_id = Auth::user()->id;
            $update->save();

            return response()->json('success',200);
        }
    }
}