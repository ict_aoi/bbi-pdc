<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;

use App\Models\Line;
use App\Models\Factory;
use App\Models\Production;
use App\Models\HistoryLine;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FactoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('factory.index');
    }
    public function data()
    {
    	if(request()->ajax())
        {

            $data = Factory::whereNull('delete_at')
            ->orderby('name','asc');
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return strtoupper($data->code);
            })
            ->editColumn('name',function($data){
                return ucwords($data->name);
            })
            ->editColumn('alamat',function($data){
                return ucwords($data->alamat);
            })
            ->editColumn('description',function($data){
                return ucwords($data->description);
            })
            
            ->addColumn('action', function($data) {
                return view('factory._action', [
                    'model'  => $data,
                    'edit'   => route('master_factory.edit',$data->id),
                    'delete' => route('master_factory.destroy',$data->id)
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function create(Request $request)
    {
        return view('factory.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $rules = [
            'code'   => 'required|string',
            'name' => 'required|string',
            'address' => 'required|string'
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $code        = strtolower($request->code);
        $name        = strtolower($request->name);
        $address     = strtolower($request->address);
        $description = strtolower($request->description);
        $dataFactory = Factory::Active()
        ->where('code',$code)->first();
        if ($dataFactory) {
            return response()->json('Code sudah ada.', 422);
        }
    	try {
    		DB::beginTransaction();
            Factory::FirstOrCreate([
                'code'        => $code,
                'name'        => $name,
                'alamat'      => $address,
                'description' => $description,
            ]);
            DB::commit();
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}

    }
    public function edit(Request $request,$id)
    {
        $factory = Factory::find($id);
        return view('factory.edit',compact('factory'));
    }
    public function update(Request $request,$id)
    {
        $rules = [
            'code'    => 'required|string',
            'name'    => 'required|string',
            'address' => 'required|string'
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $code        = strtolower($request->code);
        $name        = strtolower($request->name);
        $address     = strtolower($request->address);
        $description = strtolower($request->description);
    
        try 
        {
            DB::beginTransaction();
            $dataFactory              = Factory::Active()->find($id);
            if (!$dataFactory) {
                return response()->json('Data tidak ada.', 422);
            }
            $dataFactory->name        = $name;
            $dataFactory->code        = $code;
            $dataFactory->description = $description;
            $dataFactory->alamat      = $address;
            $dataFactory->save();
            
            DB::commit();
            return response()->json('success',200);
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $update = Factory::find($id);
            if ($update) {
                $update->delete_at      = Carbon::now();
                $update->save();
            }
        }
    }
}