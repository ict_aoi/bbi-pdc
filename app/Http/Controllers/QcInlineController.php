<?php

namespace App\Http\Controllers;

use DB;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\Line;
use App\Models\Style;
use App\Models\Defect;
use App\Models\Factory;
use App\Models\Machine;
use App\Models\Workflow;
use App\Models\QcInline;
use App\Models\Production;
use App\Models\StatusRound;
use App\Models\WorkflowDetail;
use App\Models\QcInlineDetail;
use App\Models\ResponseWorkflow;
use App\Models\Absensi\AbsensiBbi;
use App\Models\HistoryProcessSewing;


class QcInlineController extends Controller
{
    public function index(Request $request, $factory, $b)
    {
        $name          = null;
        $nik         = null;
        $subdept_name = null;
        $buyer        = ($b=='all')?'other':(($b=='other')?'other':'nagai');
        $master_line  = Line::where('buyer', $buyer)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();
        
        if (($request->session()->has('nik'))&&($request->session()->get('buyer')==$buyer)) {
            $nik            = $request->session()->get('nik');
            $name           = $request->session()->get('name');
            $subdept_name   = $request->session()->get('subdept_name');
        }
        // dd($master_line->factory_id);
        $obj                    = new stdClass();
        $obj->nik               = $nik;
        $obj->name              = $name;
        $obj->factory_id        = $master_line->factory_id;
        $obj->line_id           = $master_line->id;
        $obj->buyer             = $buyer;
        $obj->factory           = $factory;
        $obj->line              = 'all';
        $obj->subdept_name      = $subdept_name;
        $obj->header_name       = 'QC INLINE (LINE ' . strtoupper($buyer) . ')';

        return view('qc_inline.index', compact('obj'));
    }
    static function getAbsence($nik)
    {
        $master = AbsensiBbi::where('nik', $nik)->first();
        if ($master) {
            $obj                = new stdClass();
            $obj->nik           = $master->nik;
            $obj->name          = strtolower($master->name);
            $obj->subdept_name  = strtolower($master->subdept_name);

            return $obj;
        } else return null;
    }
    
    public function login(Request $request)
    {
        $nik     = trim($request->nik);
        $line    = $request->line;
        $buyer   = ($request->buyer=='all')?'other':(($request->buyer=='other')?'other':'nagai');
        $factory = $request->factory;
        $user    = $this->getAbsence($nik);

        if ($user) {
            $master_line    = Line::where('buyer', $buyer)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();
            if (!$master_line) {
                return response()->json(['message'=>'Code Line Tidak di Temukan'],422);
            }
            $request->session()->put('nik', $user->nik);
            $request->session()->put('name', $user->name);
            $request->session()->put('factory', $factory);
            $request->session()->put('factory_id', $master_line->factory_id);
            $request->session()->put('subdept_name', $user->subdept_name);
            $request->session()->put('line', $line);
            $request->session()->put('buyer', $buyer);
            $request->session()->put('line_id', $master_line->id);

            $url_qc_inline = route('qc_inline.index', [$factory, $buyer]);
            return response()->json($url_qc_inline, 200);
        } else return response()->json('Nik tidak ditemukan di absensi', 422);
    }

    public function logout(Request $request)
    {
        $factory = $request->factory;
        $line    = $request->line;
        $buyer   = $request->buyer;

        $master_line    = Line::where('buyer', $buyer)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();

        $request->session()->forget(['nik', 'nama', 'factory', 'line','buyer', 'job_desc']);

        if ($master_line->buyer == 'other') return redirect()->route('qc_inline.index', [$factory, $buyer]);
        else return redirect()->route('qc_inline.index', [$factory, $buyer]);
    }
    static function linePicklist(Request $request)
    {
        $q          = trim(strtolower($request->q));
        $factory_id = $request->factory_id;
        $buyer      = $request->session()->get('buyer');

        $lists      = Line::where('factory_id', $factory_id)
            ->where([
                ['alias', 'LIKE', "%$q%"],
                ['code','<>','all'],
                ['buyer',$buyer]
            ])
            ->orderBy('order','asc')
            ->paginate(10);

        return view('qc_inline._line_picklist', compact('lists'));
    }
    static function poreferencePickList(Request $request)
    {
        $q     = trim(strtolower($request->q));
        $buyer = $request->session()->get('buyer');
        $lists = DB::table('qc_output_v')
            ->distinct()
            ->select('production_id','style','style_merge','poreference','article','start_date','workflow_id')
            ->where('line_id', $request->line_id)
            ->where(db::raw('lower(poreference)'), 'LIKE', "%$q%")
            ->whereRaw('balance<0')
            ->orderBy('start_date','desc')
            ->paginate(10);

        return view('qc_inline._poreference_picklist', compact('lists','buyer'));
    }
    public function getStatus(Request $request)
    {
        $line              = Line::find($request->line_id);
        $is_infinity_round = $line->is_infinity_round;
        $line_id           = $request->line_id;
        $factory_id        = $request->session()->get('factory_id');
        $nik               = $request->session()->get('nik');
        $statusRound       = $this->getStatusRound($line_id,$factory_id,$is_infinity_round,$nik);
        $countComponent    = WorkflowDetail::whereNull('workflow_details.delete_at')
        ->whereNotNull('process_sewings.component')
        ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
        ->where('workflow_details.workflow_id',$request->workflow_id)
        ->count();
        if ($statusRound) {
            $round = $statusRound->round;
        } else {
            $data  = $this->insertStatusRound(1,$line_id,$factory_id,$nik);
            $round = $data;
        }
        
        return response()->json(['round'=>$round,'countComponent'=>$countComponent], 200);
    }
    private function getStatusRound($line_id,$factory_id,$is_infinity_round,$nik){
        $statusRound = StatusRound::whereNull('delete_at')
        ->where([
            ['line_id', $line_id],
            [db::raw('date(created_at)'), Carbon::now()->format('Y-m-d')],
            ['factory_id', $factory_id],
            ['qc_nik',$nik]
        ])
        ->orderBy('created_at', 'desc')->first();

        return $statusRound;
    }
    private function insertStatusRound($round,$line_id,$factory_id,$nik){

        $data = StatusRound::Create([
            'factory_id' => $factory_id,
            'line_id'    => $line_id,
            'round'      => $round,
            'qc_nik'     => $nik
        ]);
        return $data->round;
    }
    static function getProcess(Request $request)
    {
        if ($request->ajax()) {
            $q      = trim(strtolower($request->q));
            $search = str_replace(" ","%",$q);
            $round  = $request->round;
            
            $date        = Carbon::now()->format('Y-m-d');
            $production  = Production::find($request->production_id);
            $roundBefore = StatusRound::whereNotNull('delete_at')->where('line_id',$production->line_id)->orderBy('delete_at','desc')->first();
            $dbefore     = ($roundBefore)?$roundBefore->created_at:'9999-01-01 07:00:00';
            $rBefore     = ($roundBefore)?$roundBefore->round:'0';
            $dateBefore  = Carbon::createFromFormat('Y-m-d H:i:s',$dbefore)->format('Y-m-d');
            $id          = $request->workflow_id;
            $component   = ($request->component<>'')?$request->component:null;
            $nik         = $request->session()->get('nik');
            
            $listProcess = WorkflowDetail::whereNull('workflow_details.delete_at')
                ->select('workflow_details.id as workflow_detail_id', 'workflow_details.process_sewing_id as process_id', 'process_sewings.name as proses_name', 'workflow_details.last_process', db::raw('COALESCE(total_inline.count_inline,0) as count_inline'),'machines.name as machine_name','workflow_details.machine_id')
                ->leftjoin('process_sewings', 'process_sewings.id', 'workflow_details.process_sewing_id')
                ->leftjoin('machines', 'machines.id', 'workflow_details.machine_id')
                ->leftjoin(
                    DB::raw('(select workflow_detail_id,date(qi.created_at),count(*)as count_inline from qc_inlines qi left join productions pr on pr.id=qi.production_id  where date(qi.created_at)=' . "'$date'" . ' and qi.qc_nik=' . "'$nik'" . ' and round='.$round.' and pr.style=' . "'$production->style'" . ' GROUP BY production_id,workflow_detail_id,date(qi.created_at))
            total_inline'),
                    function ($leftjoin) {
                        $leftjoin->on('workflow_details.id', '=', 'total_inline.workflow_detail_id');
                    }
                )
                ->leftjoin(
                    DB::raw('(select qi.process_sewing_id,pr.line_id,pr.style,max(qi.created_at) as max_created from qc_inlines qi
                    LEFT JOIN productions pr on pr.id=qi.production_id  where date(qi.created_at)=' . "'$dateBefore'" . ' and round='.$rBefore.' and pr.line_id=' . "'$production->line_id'" . ' and pr.style=' . "'$production->style'" . ' and qi.qc_nik=' . "'$nik'" . ' GROUP BY qi.process_sewing_id,pr.line_id,pr.style)
                    round_before'),
                    function ($leftjoin) {
                        $leftjoin->on('process_sewings.id', '=', 'round_before.process_sewing_id');
                    }
                )
                ->where(db::raw('lower(process_sewings.name)'), 'LIKE', "%$search%")
                ->where('workflow_details.workflow_id', $id)
                ->where(function($query) use($component)
                {
                    if ($component<>'') {
                        $query = $query->Where('process_sewings.component',$component);
                    }
                })
                ->orderBy(db::raw('COALESCE(total_inline.count_inline,0)'), 'asc')
                ->orderBy('round_before.max_created', 'asc')
                ->orderBy('process_sewings.created_at', 'asc')
                ->paginate(10);
            return view('qc_inline._process_picklist', compact('listProcess'));
        }
    }
    public function getProcessAll(Request $request){
        if ($request->ajax()) {
            $workflow_id = $request->workflow_id;
            $listProcess = Workflow::leftJoin('workflow_details','workflow_details.workflow_id','workflows.id')
                ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
                ->where('workflows.id',$workflow_id)
                ->pluck('process_sewings.id', 'process_sewings.name','workflow_details.last_process')->all();
            return response()->json(['list' => $listProcess]);
        }
    }
    static function getMachine(Request $request)
    {
        if ($request->ajax()) {
            $listMachine = Machine::whereNull('delete_at')
            ->select('id','name')
            ->where('factory_id', $request->session()->get('factory_id'))->get();
            
            return response()->json(['list' => $listMachine]);
        }
    }
    static function getSewer(Request $request)
    {
        if ($request->ajax()) {
            $factory_id = $request->factory_id;
            $factory    = Factory::find($factory_id);
            
            if($factory)
            {
                $absences = DB::connection('bbi_apparel')
                    ->table('get_employee') 
                    ->select('nik','name','department_name','subdept_name')
                    ->get();

                $array = [];
                foreach ($absences as $key => $value) 
                {
                    $array [] = $value->nik.':'.$value->name;
                }
                
                return response()->json($array,200);
            }else
            {
                return response()->json(200);
            }
        }
    }
    static function showHistory(Request $request)
    {
        if ($request->ajax()) {
            list($nik,$nama) = explode(":",$request->nik);
            $factory_id = $request->session()->get('factory_id');
            $line_id = $request->session()->get('line_id');
            $list       = HistoryProcessSewing::whereNull('history_process_sewings.delete_at')
            ->leftjoin('workflow_details','workflow_details.id','history_process_sewings.workflow_detail_id')
            ->leftJoin('workflows','workflows.id','workflow_details.workflow_id')
            ->whereNull('workflow_details.delete_at')
            ->whereNull('workflows.delete_at')
            ->where([
                ['history_process_sewings.factory_id',$request->factory_id],
                ['line_id', $request->line_id],
                ['sewer_nik', $nik],
                ['history_process_sewings.style', $request->style_merge]
            ]);
            $listCount          = $list->count();
            $workflow_detail_id = '';
            $machine_id         = '';
            $machine_name       = '';
            $process_id         = '';
            $process_name       = '';
            $urlmesin           = '';
            $urlsewer           = '';
            $urlprocess         = '';
            $lastProcess        = '';
            if ($listCount==1) {
                $workflowDetail = WorkflowDetail::whereNull('delete_at')
                ->where([
                    ['workflow_id',$request->workflow_id],
                    ['id',$list->first()->workflow_detail_id],
                ])->first();
                if ($workflowDetail) {
                    $machine_id         = $workflowDetail->machine_id;
                    $workflow_detail_id = $list->first()->workflow_detail_id;
                    $machine_name       = $workflowDetail->machine->name;
                    $process_id         = $workflowDetail->process_sewing_id;
                    $process_name       = $workflowDetail->processSewing->name;
                    $lastProcess        = ($workflowDetail->last_process)?1:0;
                }
            }else{
                $urlmesin = route('qc_inline.history_machine', [$request->factory_id, $request->line_id, $nik,$request->style_merge]);
                $urlprocess = route('qc_inline.history_process', [$request->factory_id, $request->line_id,$request->workflow_id, $nik,$request->style_merge]);
            }

            
            $data = array(
                'list'               => $listCount,
                'urlmesin'           => $urlmesin,
                'urlprocess'         => $urlprocess,
                'machine_id'         => $machine_id,
                'workflow_detail_id' => $workflow_detail_id,
                'machine_name'       => $machine_name,
                'process_id'         => $process_id,
                'process_name'       => $process_name,
                'last_process'       => $lastProcess,
            );
            return  response()->json($data, 200);
        }
    }
   
    static function HistoryProcess(Request $request, $factory_id, $line_id,$workflow_id, $nik,$style)
    {
        $q           = trim(strtolower($request->q));
        $listProcess = HistoryProcessSewing::select(
                'history_process_sewings.workflow_detail_id',
                'workflow_details.process_sewing_id',
                'workflow_details.machine_id',
                db::raw('process_sewings.name as proses_name'),
                db::raw('machines.name as machine_name'),
                db::raw('0 as count_inline'),
                'workflow_details.last_process'
            )
            ->leftJoin('workflow_details','workflow_details.id','history_process_sewings.workflow_detail_id')
            ->leftJoin('workflows','workflows.id','workflow_details.workflow_id')
            ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
            ->leftJoin('machines','machines.id','workflow_details.machine_id')
            ->where([
                ['history_process_sewings.line_id',$line_id],
                ['history_process_sewings.factory_id',$factory_id],
                ['history_process_sewings.sewer_nik',$nik],
                ['history_process_sewings.style',$style],
                ['workflow_details.workflow_id',$workflow_id],
            ])
            ->whereNull('history_process_sewings.delete_at')
            ->whereNull('workflow_details.delete_at')
            ->whereNull('workflows.delete_at')
            ->groupBy('history_process_sewings.workflow_detail_id',
            'workflow_details.process_sewing_id',
            'workflow_details.machine_id','process_sewings.name','machines.name',
            'workflow_details.last_process')
            ->paginate(10);
        return view('qc_inline._process_picklist', compact('listProcess'));
    }
    static function HistoryMachine(Request $request, $factory_id, $line_id, $nik,$style)
    {
        $q     = trim(strtolower($request->q));
        $lists = HistoryProcessSewing::select('machines.id as machine_id', 'machines.code', 'machines.name as machine_name')
                ->leftjoin('machines', 'machines.id', 'history_process_sewings.machine_id')
                ->where([
                    ['history_process_sewings.factory_id', $factory_id],
                    ['line_id', $line_id],
                    ['sewer_nik', $nik],
                    ['style', $style],
                ])
                ->whereNull('history_process_sewings.delete_at')
                ->groupBy('machines.id', 'machines.code', 'machines.name')
                ->paginate(10);
        return view('qc_inline._machine_picklist', compact('lists'));
    }
    static function HistorySewer(Request $request, $factory_id, $line_id, $process_id,$style)
    {
        if ($request->ajax()) {
            $lists       = HistoryProcessSewing::where('history_process_sewings.factory_id', $factory_id)
                ->whereNull('delete_at')
                ->where([
                    ['line_id', $line_id],
                    ['process_sewing_id', $process_id],
                    ['style', $style],
                ])
                ->paginate(10);
            return view('qc_inline._sewer_picklist', compact('lists'));
        }
    }
    static function getDefect(Request $request)
    {
        if ($request->ajax()) {
            $q       = trim(strtolower($request->q));
            $search  = str_replace(" ","%",$q);
            $buyer   = $request->session()->get('buyer');
            $line_id = ($request->line_id)?$request->line_id:$request->session()->get('line_id');
            $line    = Line::find($line_id);
            $lists   = Defect::select('defects.id as id_defect','defects.code','defects.position','defects.name as name_defect', 'grade_defects.color', 'grade_defects.id as grade_defect_id')
                ->where(function ($query) use ($q,$search,$buyer) {
                    if ($buyer=="other") {
                        $query->where('defects.id', 'LIKE', "%$q%")
                        ->orWhere('defects.name', 'LIKE', "%$q%");
                    } else if($buyer=="nagai") {
                        $query->where('defects.code', 'LIKE', "$search%")
                        ->orWhere(db::raw('lower(defects.name)'), 'LIKE', "%$search%")
                        ->orWhere(db::raw('lower(defects.position)'), 'LIKE', "%$search%");
                    }
                })
                ->where('defects.id', '!=', '0')
                ->whereNull('delete_at')
                ->where('buyer',$buyer)  
                ->leftjoin('grade_defects', 'grade_defects.id', 'defects.grade_defect_id')
                ->orderBy('defects.id','asc');
                
                if($q) $lists = $lists->paginate(100);
                else $lists = $lists->paginate(31);

            return view('qc_inline._defect_picklist', compact('lists','buyer'));
        }
    }
    static function getGradeBefore(Request $request)
    {
        $lastGrade =0;
        if ($request->workflow_detail_id) {
            $last = QcInline::whereNull('delete_at')
            ->where('factory_id', $request->session()->get('factory_id'))
            ->where('workflow_detail_id', $request->workflow_detail_id)
            ->where('sewer_nik', $request->sewer_nik)
            ->orderBy('created_at', 'DESC')
            ->first();
            $lastGrade = $last ? $last->grade_defect_id : 0;
        }
        return response()->json(['lastGrade' => $lastGrade], 200);
    }
    static function getHistoryGrade(Request $request)
    {
        $detailGrade ='';
        $beforeGrade ='';
        if ($request->workflow_detail_id) {
            $beforeGrade = QcInline::whereNull('delete_at')
            ->select(db::raw("(CASE WHEN color='LawnGreen' THEN 'success' WHEN color='Gold' THEN 'warning' ELSE 'danger' END) AS warna"), 'qc_inlines.round')
            ->leftjoin('grade_defects', 'grade_defects.id', 'qc_inlines.grade_defect_id')
            ->where('factory_id', $request->session()->get('factory_id'))
            ->where('workflow_detail_id', $request->workflow_detail_id)
            ->where('sewer_nik', $request->sewer_nik)
            ->where(db::raw('date(qc_inlines.created_at)'), '<', Carbon::now()->format("Y-m-d"))
            ->orderBy('qc_inlines.created_at', 'DESC')
            ->limit(1)
            ->get()->toArray();
        $detailGrade = QcInline::whereNull('delete_at')
            ->select(db::raw("(CASE WHEN color='LawnGreen' THEN 'success' WHEN color='Gold' THEN 'warning' ELSE 'danger' END) AS warna"), 'qc_inlines.round')
            ->leftjoin('grade_defects', 'grade_defects.id', 'qc_inlines.grade_defect_id')
            ->where('factory_id', $request->session()->get('factory_id'))
            ->where('workflow_detail_id', $request->workflow_detail_id)
            ->where('sewer_nik', $request->sewer_nik)
            ->where(db::raw('date(qc_inlines.created_at)'), Carbon::now()->format("Y-m-d"))
            ->orderBy('round', 'ASC')
            ->limit(10)
            ->get()->toArray();
        }
        return response()->json(['detailGrade' => $detailGrade,'beforeGrade' => $beforeGrade], 200);
    }
    public function store(Request $request)
    {   
        if ($request->ajax()) {
            $production_id      = $request->production_id;
            $line_id            = $request->line_id;
            $factory_id         = $request->session()->get('factory_id');
            $qc_nik             = $request->session()->get('nik');
            $style              = $request->style;
            $style_merge        = $request->style_merge;
            $machine_id         = $request->machine_id;
            $sewer_nik          = $request->sewer_nik;
            $sewer_name         = $request->sewer_name;
            $workflow_detail_id = $request->workflow_detail_id;
            $grade              = $request->grade;
            $last_process       = $request->last_process;
            $itemSamples        = json_decode($request->items);
            $lines              = Line::find($request->line_id);
            $is_infinity_round  = $lines->is_infinity_round;
            $maxRound           = ($is_infinity_round)?10:5;
            $statusRound        = $this->getStatusRound($line_id,$factory_id,$is_infinity_round,$qc_nik);
            $round              = ($statusRound ? $statusRound->round : 1);
            if (!$production_id && !$machine_id && !$sewer_nik && !$workflow_detail_id && !$grade && !$line_id) {
                return response()->json(['message' => 'Data Tidak Lengkap silahkan Refresh halaman anda'], 422);
            }
            $existSampling = QcInline::where('qc_inlines.delete_at')
                ->leftJoin('productions','productions.id','qc_inlines.production_id')
                ->where(db::raw('date(qc_inlines.created_at)'), Carbon::now()->format('Y-m-d'))
                ->where('qc_inlines.round', $round)
                ->where('qc_inlines.factory_id', $factory_id)
                ->where('qc_inlines.workflow_detail_id', $workflow_detail_id)
                ->where('productions.line_id', $line_id)
                ->where('qc_inlines.sewer_nik', $sewer_nik)
                ->exists();
            $existHistory = HistoryProcessSewing::whereNull('delete_at')
            ->where([
                ['sewer_nik',$sewer_nik],
                ['style',$style_merge],
                ['factory_id', $factory_id],
                ['workflow_detail_id', $workflow_detail_id],
                ['line_id',$line_id]
            ])->exists();
            if ($existSampling) {
                return response()->json(['message' => 'Data Sudah Ada'], 422);
            }
            try {
                DB::beginTransaction();
                    $roundInsert = $round;
                    if (!$statusRound) {
                        // $roundInsert = $round;
                        $RoundUpdate = StatusRound::whereNull('delete_at')
                            ->where('line_id', $request->line_id)
                            ->where('factory_id', $factory_id)
                            ->orderBy('created_at', 'desc')
                            ->first();
                        if ($roundInsert<=$maxRound) {
                            
                            if ($statusRound) {
                                $statusRound->delete_at = carbon::now();
                                $statusRound->save();
                            }

                            StatusRound::Create([
                                'factory_id' => $factory_id,
                                'line_id'    => $request->line_id,
                                'round'      => $roundInsert,
                                'qc_nik'     => $qc_nik
                            ]);
                        }
                        if ($statusRound) {
                            $statusRound->delete_at = carbon::now();
                            $statusRound->save();
                        }
                        
                    }
                    if (!$existHistory) {
                        HistoryProcessSewing::Create([
                            'factory_id'         => $factory_id,
                            'workflow_detail_id' => $workflow_detail_id,
                            'line_id'            => $line_id,
                            'sewer_nik'          => $sewer_nik,
                            'style'              => $style_merge,
                            'sewer_name'         => strtolower($sewer_name),
                        ]);
                    }
                    $checkWorkflowDetail = WorkflowDetail::whereNull('delete_at')
                    ->where('id',$workflow_detail_id)->first();
                    if ($checkWorkflowDetail->machine_id==null) {
                        $checkWorkflowDetail->machine_id = $machine_id;
                        $checkWorkflowDetail->save();
                    }
                    $qcInline = QcInline::Create([
                        'factory_id'         => $factory_id,
                        'production_id'      => $production_id,
                        'workflow_detail_id' => $workflow_detail_id,
                        'grade_defect_id'    => $grade,
                        'round'              => $round,
                        'sewer_name'         => strtolower($sewer_name),
                        'sewer_nik'          => $sewer_nik,
                        'qc_nik'             => $request->session()->get('nik'),
                        'qc_name'            => $request->session()->get('name'),
                    ]);
                    foreach ($itemSamples as $key => $item) {
                        QcInlineDetail::Create([
                            'qc_inline_id' => $qcInline->id,
                            'defect_id'    => $item->defect_id
                        ]);
                    }
                    // $round = ($last_process==1&&$round<5?$round+1:$round);
                DB::commit();
                return response()->json(['success'=>'success','round'=>$roundInsert],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function ReportSampleRound(Request $request)
    {
        $line_id = $request->line_id;
        $is_infinity_round = Line::find($line_id)->is_infinity_round;
        return view('qc_inline.report._report_sample_round', compact('line_id','is_infinity_round'));
    }
    public function getDashboard(Request $request)
    {
        if ($request->ajax()) {
            $date              = Carbon::now()->format('Y-m-d');
            $line_id           = $request->line_id;
            $optSewing         = QcInline::getOptSewing($line_id,$date);
            $totalDefect       = QcInline::getTotalDefect($line_id,$date);
            $totalDefectInline = $totalDefect[0]->total;
            $wft               = ($totalDefect[0]->total>0)?($totalDefect[0]->total)/($optSewing[0]->total_operator*5)*100:0;
            
            $obj              = new stdClass();
            $obj->total_defect = $totalDefectInline;
            $obj->total_proses = $optSewing[0]->total_operator;
            $obj->wft         = number_format($wft,1,',',',').'%';

            return response()->json(['data'=>$obj],200);
        }
    }
    public function dataReportSampleRound(Request $request)
    {
    
    	if(request()->ajax())
        { 
            $factory_id = $request->session()->get('factory_id');
            $date = Carbon::now()->format('Y-m-d');
            $data = DB::select(db::Raw("select * from f_report_sample_round2('$request->line_id','$factory_id','$date')")); 
            return datatables()->of($data)
            
                ->editColumn('process_name',function($data){
                    return strtoupper($data->process_name);
                })
                ->editColumn('sewer_name',function($data){
                    return strtoupper($data->sewer_name);
                })
                ->editColumn('sewer_nik',function($data){
                    return strtoupper($data->sewer_nik);
                })
                ->editColumn('round1',function($data){
                    // return '<button class="button" style="background-color: '.$data->grade_round1.';border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin:4px 2px;cursor: pointer;">'.$data->round1.'</button>';
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round1.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round1.'</span>';
                })
                ->editColumn('round2',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round2.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round2.'</span>';
                })
                ->editColumn('round3',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round3.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round3.'</span>';
                })
                ->editColumn('round4',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round4.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round4.'</span>';
                })
                ->editColumn('round5',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round5.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round5.'</span>';
                })
                ->editColumn('round6',function($data){
                    // return '<button class="button" style="background-color: '.$data->grade_round6.';border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin:4px 2px;cursor: pointer;">'.$data->round6.'</button>';
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round6.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round6.'</span>';
                })
                ->editColumn('round7',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round7.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round7.'</span>';
                })
                ->editColumn('round8',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round8.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round8.'</span>';
                })
                ->editColumn('round9',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round9.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round9.'</span>';
                })
                ->editColumn('round10',function($data){
                    return '<span class="badge badge-default position-center" style="background-color: '.$data->grade_round10.';cursor: pointer;font-size: 16px;border: none;padding: 15px 32px;text-decoration: none;display: inline-block;">'.$data->round10.'</span>';
                })
                ->rawColumns(['round1','round2','round3','round4','round5','round6','round7','round8','round9','round10'])
                ->make(true);
        }
    }
    public function insertHistorySewing(Request $request)
    {
        HistoryProcessSewing::truncate();

        $fetchProcess = QcInline::select(
            "qc_inlines.factory_id","qc_inlines.process_sewing_id","productions.line_id",
            db::raw("styles.default as style"),"qc_inlines.machine_id","qc_inlines.sewer_nik",
            "qc_inlines.sewer_name"
            )
        ->distinct()
        ->leftJoin('productions','productions.id','qc_inlines.production_id')
        ->leftJoin('styles','styles.erp','productions.style')
        ->get();
        foreach ($fetchProcess as $key => $fetch) {
            HistoryProcessSewing::Create([
                'factory_id'        => $fetch->factory_id,
                'process_sewing_id' => $fetch->process_sewing_id,
                'machine_id'        => $fetch->machine_id,
                'line_id'           => $fetch->line_id,
                'sewer_nik'         => $fetch->sewer_nik,
                'style'             => $fetch->style,
                'sewer_name'        => strtolower($fetch->sewer_name)
            ]);
        }
    }
    public function getHistoryProcess(Request $request)
    {
        if ($request->ajax()) {
            $array =[];
            $historyProcess = HistoryProcessSewing::whereNull('delete_at')
            ->where([
                ['sewer_nik',$request->nik],
                ['style',$request->style],
                ['line_id',$request->line_id],
            ])->get();
            foreach ($historyProcess as $p) {
                $obj                    = new stdClass();
                $obj->history_id        = $p->id;
                $obj->style             = $p->style;
                $obj->process_sewing_id = $p->process_sewing_id;
                $obj->machine_id        = $p->machine_id;
                $obj->factory_id        = $p->factory_id;
                $obj->line_id           = $p->line_id;
                $obj->proses_name       = ucwords($p->processSewing->name);
                $obj->machine_name      = ucwords($p->machine->name  );

                $array [] = $obj;
            }
            return response()->json(['data'=>$array],200);
        }
    }
    public function storeHistoryProcess(Request $request)
    {
        if ($request->ajax()) {
            $itemSamples       = json_decode($request->items);
            foreach ($itemSamples as $key => $i) {
                if ($i->id=='-1') {
                    $existHistory = HistoryProcessSewing::whereNull('delete_at')
                    ->where([
                        ['sewer_nik',$request->sewer_nik],
                        ['style',$i->style],
                        ['factory_id', $i->factory_id],
                        ['machine_id', $i->machine_id],
                        ['line_id',$i->line_id],
                        ['process_sewing_id',$i->process_sewing_id],
                    ])->exists();
                    if (!$existHistory) {
                        HistoryProcessSewing::Create([
                            'factory_id'        => $i->factory_id,
                            'process_sewing_id' => $i->process_sewing_id,
                            'machine_id'        => $i->machine_id,
                            'line_id'           => $i->line_id,
                            'sewer_nik'         => $request->sewer_nik,
                            'style'             => $i->style,
                            'sewer_name'        => strtolower($request->sewer_name),
                        ]);
                    }
                }
                
            }
            return response()->json('success',200);
        }
    }
    public function destroyProcessSewing(Request $request)
    {
        if ($request->ajax()) {
            $historySewing = HistoryProcessSewing::findOrFail($request->id);
            $historySewing->delete_at = Carbon::now();
            $historySewing->save();
        }
    }
    public function getComponentProcess(Request $request)
    {
        if ($request->ajax()) {
            $q           = trim(strtolower($request->q));
            $lists = WorkflowDetail::select('process_sewings.component')
            ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
            ->whereNull('workflow_details.delete_at')
            ->where([
                ['workflow_details.workflow_id',$request->workflow_id],
                ['process_sewings.name', 'LIKE', "%$q%"],
            ])
            ->groupBy('process_sewings.component')
            ->paginate(10); 
            return view('qc_inline._component_picklist', compact('lists'));
        }
    }
    public function storeSwitchRound(Request $request)
    {
        if ($request->ajax()) {
            $factory_id        = $request->session()->get('factory_id');
            $qc_nik            = $request->session()->get('nik');
            $line_id           = $request->line_id;
            $lines             = Line::find($line_id);
            $is_infinity_round = $lines->is_infinity_round;
            $maxRound          = ($is_infinity_round)?8:5;
            $statusRound       = $this->getStatusRound($line_id,$factory_id,$is_infinity_round,$qc_nik);
            $round             = ($statusRound ? $statusRound->round : 1);
            $roundInsert = (int)$round+1;
            try {
                
                if($roundInsert>$maxRound){
                    return response()->json(['message'=>'Line Telah Mencapai Batas Round Hari ini'],422);
                }
                DB::beginTransaction();
                
                StatusRound::Create([
                    'factory_id' => $factory_id,
                    'line_id'    => $request->line_id,
                    'round'      => $roundInsert,
                    'qc_nik'     => $qc_nik
                ]);
                $statusRound->delete_at = carbon::now();
                $statusRound->save();
                DB::commit();
                return response()->json(['success'=>'success','round'=>$roundInsert],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function showHistoryOperatorSewer(Request $request)
    {
        if ($request->ajax()) {
            $line_id     = $request->line_id;
            $workflow_id = $request->workflow_id;
            $style       = $request->style;
            $roundNow    = $request->round;
            $q           = trim(strtolower($request->q));
            $qc_nik      = $request->session()->get('nik');
            $dateNow     = Carbon::now()->format('Y-m-d');
            $Before      = QcInline::getRoundBefore($line_id,$qc_nik);
            $dateBefore  = $Before?Carbon::createFromFormat('Y-m-d H:i:s',$Before->created_at)->format('Y-m-d'):null;
            $roundBefore = $Before?$Before->round:null;

            $lists = QcInline::select(
                'workflow_details.process_sewing_id',
                'workflow_details.machine_id',
                'qc_inlines.workflow_detail_id',
                'qc_inlines.sewer_nik',
                'qc_inlines.sewer_name',
                'workflow_details.last_process',
                db::raw('process_sewings.name as process_name'),
                db::raw('machines.name as machine_name'),
                db::raw('COALESCE(qc_check.total,0) as check_qc')
            )
            ->leftJoin('productions','productions.id','qc_inlines.production_id')
            ->leftJoin('workflow_details','workflow_details.id','qc_inlines.workflow_detail_id')
            ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
            ->leftJoin('machines','machines.id','workflow_details.machine_id')
            ->leftjoin(
                DB::raw('(select qci.workflow_detail_id,qci.sewer_nik,s.default,count(*) as total from qc_inlines qci LEFT JOIN productions pr on pr.id=qci.production_id LEFT JOIN styles s on s.erp=pr.style
                where date(qci.created_at)=' . "'$dateNow'" . ' and qci.qc_nik=' . "'$qc_nik'" . ' and round='.$roundNow.' and s.default=' . "'$style'" . ' GROUP BY qci.workflow_detail_id,qci.sewer_nik,s.default)
                qc_check'),
                function ($leftjoin) {
                    $leftjoin->on('qc_inlines.sewer_nik', '=', 'qc_check.sewer_nik');
                    $leftjoin->on('qc_inlines.workflow_detail_id', '=', 'qc_check.workflow_detail_id');
                }
            )
            ->where([
                ['qc_inlines.qc_nik',$qc_nik],
                [db::raw('date(qc_inlines.created_at)'),$dateBefore],
                ['qc_inlines.round',$roundBefore],
                ['productions.line_id',$line_id],
                ['workflow_details.workflow_id',$workflow_id],
            ])
            ->whereNull('workflow_details.delete_at')
            ->where(function($query) use ($q){
                $query->where(db::raw('lower(qc_inlines.sewer_nik)'),'LIKE',"%$q%")
                ->orWhere(db::raw('lower(qc_inlines.sewer_name)'),'LIKE',"%$q%");
            })
            ->orderBy(db::raw('COALESCE(qc_check.total,0)'),'asc')
            ->orderBy('qc_inlines.created_at','asc')
            ->paginate(10);

            // $lists = QcInline::select(
            //     'productions.style',
            //     'workflow_details.last_process',
            //     'qc_inlines.sewer_nik',
            //     'qc_inlines.sewer_name',
            //     db::raw('workflow_details.id as workflow_detail_id'),
            //     db::raw('process_sewings.id as process_sewing_id'),
            //     db::raw('process_sewings.name as process_name'),
            //     db::raw('machines.id as machine_id'),
            //     db::raw('machines.name as machine_name')
            // )
            // ->join('productions','qc_inlines.production_id','productions.id')
            // ->join('workflow_details','qc_inlines.workflow_detail_id','workflow_details.id')
            // ->join('process_sewings','workflow_details.process_sewing_id','process_sewings.id')
            // ->join('machines','workflow_details.machine_id','machines.id')
            // ->whereNull('workflow_details.delete_at')
            // ->where([
            //     ['productions.line_id',$line_id],
            //     ['productions.style',$style],
            //     ['workflow_details.workflow_id',$workflow_id]
            // ])
            // ->groupBy([
            //     'productions.style',
            //     'workflow_details.last_process',
            //     'workflow_details.id',
            //     'process_sewings.id',
            //     'process_sewings.name',
            //     'machines.id',
            //     'machines.name',
            //     'qc_inlines.sewer_nik',
            //     'qc_inlines.sewer_name'
            // ])
            // ->orderBy('process_sewings.name','asc')
            // ->paginate(10);
            
            return view('qc_inline._sewer_picklist', compact('lists'));
        }
    }
    public function sendMessage(Request $request)
    {
        $line_id = $request->line_id;
        $is_infinity_round = Line::find($line_id)->is_infinity_round;

        $listStyle = DB::table('list_style_sewing_v')
                    ->where([
                        ['line_id',$request->line_id],
                    ])
                    ->whereRaw('total_qty>total_counter')
                    ->orderBy('max_date','desc')
                    ->pluck('style','style')->all();
        $countComplainClose = $this->getComplainClose($line_id)->count();
        $totComplain        = ResponseWorkflow::whereNull('delete_at')
        ->where('line_id',$line_id)
        ->where('status','open')->get()->count();
        return view('qc_inline._lov_send_message', compact('line_id','listStyle','countComplainClose','totComplain'));
    }
    private function getComplainClose($line_id)
    {
        $data = ResponseWorkflow::select('response_workflow_details.message','response_workflow_details.id')
        ->leftJoin('response_workflow_details','response_workflow_details.response_workflow_id','response_workflows.id')
        ->whereNull('response_workflows.delete_at')
        ->whereNull('response_workflow_details.delete_at')
        ->where([
            ['response_workflows.line_id',$line_id],
            ['response_workflow_details.is_read_qc','f'],
        ])->get();
        return $data;
    }
    public function storeMessage(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';
            
            $line_id   = $request->line_id;
            $line = Line::find($line_id);
            $style     = $request->style;
            $message   = $request->message;
            $noLaporan = Carbon::now()->format('u');
            $qc_name   = $request->session()->get('name');
            $qc_nik   = $request->session()->get('nik');

            $workflows = Workflow::whereNull('delete_at')
            ->where('style',$style)->first();
            
            if ($workflows) {
                $workflow_id = $workflows->id;
            }else{
                $workflow_id = null;
            }
            $group = ($line->buyer=='other')?'Digitalisasi P. Akurasi':'Nagai Digitalisasi';
            ResponseWorkflow::create([
                'line_id'        => $line_id,
                'workflow_id'    => $workflow_id,
                'nomor'          => $noLaporan,
                'style'          => $style,
                'message'        => $message,
                'create_qc_nik'  => $qc_nik,
                'create_qc_name' => $qc_name,
                'status'         => 'open',
            ]);
            DB::connection($connection_mid)
            ->table('whatsapp')
            ->insert([
                'contact_number' => $group,
                'is_group' => 't',
                'message' => '*Mohon Bantuannya*|No. Lapor : '.$noLaporan.'|Line :'.$line->alias.'|Style : '.$style.'|Nama QC  :'.ucwords($qc_name).'|Pesan : '.ucwords($message).'|Terimakasih.'
            ]);
        }
    }
    public function getNotification(Request $request)
    {
        if ($request->ajax()) {
            $line_id = $request->line_id;
            $data = $this->getComplainClose($line_id)->count();
           
            return response()->json($data,200);
        }
    }
    public function getComplainWorkflowData(Request $request)
    {
        $q          = trim(strtolower($request->q));
        $line_id = $request->line_id;
        $data = $this->getComplainClose($line_id);
        foreach ($data as $key => $d) {
            DB::table('response_workflow_details')
            ->where([
                ['id',$d->id],
            ])
            ->update([
                'is_read_qc' => 't'
            ]);
        }

        $lists      = ResponseWorkflow::select('response_workflows.line_id','response_workflows.message as message_ie','response_workflow_details.message as message_qc','response_workflows.style','response_workflows.nomor','create_qc_name','create_qc_nik',db::raw('COALESCE(response_workflow_details.status,response_workflows.status) as status'),'lines.name')
        ->leftJoin('response_workflow_details','response_workflow_details.response_workflow_id','response_workflows.id')
        ->leftJoin('lines','lines.id','response_workflows.line_id')
        ->whereNull('response_workflows.delete_at')
        ->whereNull('response_workflow_details.delete_at')
        ->where('response_workflows.line_id',$line_id)
        ->where(function($query) use ($q){
            $query->where(db::raw('lower(style)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(response_workflow_details.status)'),'LIKE',"%$q%");
        })
        ->orderBy('response_workflows.created_at','desc')
        ->paginate(10);

        return view('qc_inline._lov_complain_workflow', compact('lists'));
    }
}