<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\WorkingHours;
use App\Models\QcOutputView;
use App\Models\FoldingOutput;
use App\Models\QcEndlineDetail;
use App\Models\QcEndlineDefect;
use App\Models\SummaryProduction;
use App\Models\AdjustmentQcSubtraction;

class AdjustmentOutputQcSubtractionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('adjustment_output_qc_subtraction.index');
    }
    public function data(Request $request)
    {
    	if(request()->ajax())
        {
            
            $buyer   = $this->Buyer();
            $data    = AdjustmentQcSubtraction::select(
                db::raw('date(adjustment_qc_subtractions.created_at) as date'),
                'lines.name',
                'lines.code',
                'adjustment_qc_subtractions.style',
                'adjustment_qc_subtractions.poreference',
                'adjustment_qc_subtractions.article',
                'adjustment_qc_subtractions.size',
                'adjustment_qc_subtractions.size',
                'adjustment_qc_subtractions.qty_revision',
                'adjustment_qc_subtractions.request_by_name',
                'adjustment_qc_subtractions.request_by_nik',
                'adjustment_qc_subtractions.reason'
                )
            ->leftJoin('lines','lines.id','adjustment_qc_subtractions.line_id')
            ->whereIn('lines.buyer',$buyer)
            ->where('adjustment_qc_subtractions.factory_id',Auth::user()->factory_id)
            ->orderby('adjustment_qc_subtractions.created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('tanggal',function($data){
            	// return Carbon::createFromFormat('Y-m-d H:i:s',$data->date)->format('d-m-Y H:i:s');
                return $data->date;
            })
            ->editColumn('code',function($data){
            	return $data->code;
            })
            ->editColumn('line_name',function($data){
            	return strtoupper($data->name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('poreference',function($data){
            	return $data->poreference;
            })
            ->editColumn('article',function($data){
            	return $data->article;
            })
            ->editColumn('size',function($data){
            	return $data->size;
            })
            ->editColumn('qty_revision',function($data){
            	return $data->qty_revision;
            })
            ->editColumn('qc_name',function($data){
            	return ucwords($data->request_by_nik)."-".$data->request_by_name;
            })
            ->editColumn('reason',function($data){
            	return ucwords($data->reason);
            })
            
            ->make(true);
        }
        
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function create(Request $request)
    {
        
        return view('adjustment_output_qc_subtraction.create');
    }
    function linePicklist(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $buyer = $this->Buyer();
            $lists = Line::whereNull('delete_at')
            ->whereIn('buyer',$buyer)
            ->where([
                ['factory_id',Auth::user()->factory_id],
                [db::raw('lower(alias)'), 'LIKE', "%$q%"],
                ['code','<>','all'],

            ])
            ->orderBy('order','asc')
            ->paginate(10);

            return view('adjustment_output_qc_subtraction._line_picklist', compact('lists'));
        }
    }
    public function QcOutputData(Request $request)
    {
        if(request()->ajax())
        {
            $date       = ($request->date)?Carbon::createFromFormat('m/d/Y',$request->date)->format('Y-m-d'):Carbon::now()->format('Y-m-d');
            $line_id    = $request->line_id;
            $factory_id = Auth::user()->factory_id;
            $data       = DB::select(db::Raw("select * from f_qc_history_output('$factory_id','$date') where line_id='$line_id'"));
            
            return datatables()->of($data)
            ->editColumn('tanggal',function($data){
            	return $data->date;
            })
            ->editColumn('line_name',function($data){
            	return strtoupper($data->name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('poreference',function($data){
            	return $data->poreference;
            })
            ->editColumn('article',function($data){
            	return $data->article;
            })
            ->editColumn('size',function($data){
            	return $data->size;
            })
            ->editColumn('qty_order',function($data){
            	return $data->qty_order;
            })
            ->editColumn('total_counter',function($data){
            	return $data->total_counter;
            })
            ->editColumn('counter_day',function($data){
            	return $data->counter_day;
            })
            ->editColumn('balance_per_day',function($data){
            	return $data->balance_per_day;
            })
            ->addColumn('action', function($data) {
                return view('adjustment_output_qc_subtraction._action', [
                    'model' => $data,
                    'edit' => route('adjustment_output_qc_subtraction.edit',[$data->qc_endline_id,$data->date]),
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function edit(Request $request,$id,$date)
    {
        $data = DB::table('qc_history_output_v')
        ->where('qc_endline_id',$id)
        ->where('date',$date)
        ->first();
        
        return view('adjustment_output_qc_subtraction._form_revision_qc_endline', compact('data'));
    }
    static function getSewer(Request $request)
    {
        if ($request->ajax()) {
            $factory_id = Auth::user()->factory_id;
            $nik        = $request->nik;
            $factory    = Factory::find($factory_id);
            
            if($factory)
            {
                $absences = DB::connection('bbi_apparel')
                ->table('get_employee') 
                ->select('nik','name','department_name','subdept_name')
                ->where('nik',$nik)
                ->first();

                if (!$absences) {
                    return response()->json('Nik Tidak ada.', 422);
                } else {
                    $obj = new StdClass();
                    $obj->name = $absences->name;
                    $obj->nik = $absences->nik;
                    return  response()->json($obj, 200);
                }
                
            }else
            {
                return response()->json('Ada Kesalahan silahkan refresh halaman anda', 422);
            }
        }
    }
    public function storeRevision(Request $request)
    {
        if ($request->ajax()) {
            $qcOutput = QcOutputView::where('qc_endline_id',$request->id)->first();
            if (!$qcOutput) {
                return response()->json('Ada Kesalahan silahkan refresh halaman anda', 422);
            }
            $dataFolding = FoldingOutput::where([
                ['production_id',$qcOutput->production_id],
                ['production_size_id',$qcOutput->production_size_id],
            ])->first();
            $maxFolding = $dataFolding?$dataFolding->counter:0;
            $legalQty   = (int)$qcOutput->total_output - (int)$maxFolding;
            // dd($legalQty<=$request->revisi_output);
            if ($legalQty < $request->revisi_output) {
                return response()->json('Tidak bisa di hapus karna sudah scan folding', 422);
            }
            try {
                DB::beginTransaction();
                $qcEndlineDetail = QcEndlineDetail::where('qc_endline_id',$request->id)
                ->where(db::raw('date(created_at)'),$request->date)
                ->orderBy('counter','desc')
                ->limit($request->revisi_output);
                foreach ($qcEndlineDetail->get() as $key => $qc) {
                   QcEndlineDetail::find($qc->id)->delete();
                }

                $date = Carbon::now()->format('Y-m-d');
                DB::select(db::Raw("select * from update_counter('$request->id')"));
                DB::select(db::Raw("select * from update_counter_qc('$request->id','$date')"));
                DB::select(db::Raw("select * from update_repair_qc('$request->id')"));

                AdjustmentQcSubtraction::create([
                    'factory_id'      => Auth::user()->factory_id,
                    'line_id'         => $request->line_id,
                    'style'           => $request->style,
                    'poreference'     => $request->poreference,
                    'article'         => $request->article,
                    'size'            => $request->size,
                    'qty_revision'    => $request->revisi_output,
                    'request_by_name' => $request->name_label,
                    'request_by_nik'  => $request->nik,
                    'reason'          => $request->reason,
                    'date_output'     => $request->date,
                    'create_user_id'  => Auth::user()->id
                ]);
                $this->pullBackSummaryProduction($request->date,$request->line_id,$request->style,$request->poreference,$request->article,$request->size);
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    private function pullBackSummaryProduction($date,$line_id,$style,$poreference,$article,$size){
        $workingHours = WorkingHours::OrderBy('start','asc')->get();
        for ($i=$date; $i <= $date; $i++) { 
            SummaryProduction::where('date',$i)
            ->where('line_id',$line_id)
            ->where('style',$style)
            ->where('poreference',$poreference)
            ->where('article',$article)
            ->where('size',$size)
            ->whereIn('name',['output_qc_endline','output_defect_endline'])
            ->delete();
            foreach ($workingHours as $key => $working) {
                $awal  = $i.' '.$working->start;
                $akhir = $i.' '.$working->end;
                $this->sumQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size);
                $this->sumDefectQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size);
            }
        }
    }
    private function sumQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size)
    {
        $getSumQcEndline = QcEndlineDetail::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_details.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_details.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_details.counter','<>','0')
        ->where('qc_endline_details.created_at','>=',$awal)
        ->where('qc_endline_details.created_at','<',$akhir)
        ->where('productions.line_id',$line_id)
        ->where('productions.style',$style)
        ->where('productions.poreference',$poreference)
        ->where('productions.article',$article)
        ->where('production_sizes.size',$size)
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_details.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'output_qc_endline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where('date',$date)
            ->where('end_hours',$akhir)
            ->where('line_id',$qc->line_id)
            ->where('style',$qc->style)
            ->where('poreference',$qc->poreference)
            ->where('size',$qc->size)
            ->where('article',$qc->article)
            ->where('name',$name)
            ->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    }
    private function sumDefectQcEndline($awal,$akhir,$line_id,$style,$poreference,$article,$size)
    { 
        $getSumQcEndline = QcEndlineDefect::select('productions.line_id','productions.style','productions.poreference','production_sizes.size','productions.article',db::raw('count(*) as output_qc'),db::raw('date(qc_endline_defects.created_at) as created_at'))
        ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
        ->leftJoin('productions','productions.id','qc_endlines.production_id')
        ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
        ->where('qc_endline_defects.created_at','>=',$awal)
        ->where('qc_endline_defects.created_at','<',$akhir)
        ->where('productions.line_id',$line_id)
        ->where('productions.style',$style)
        ->where('productions.poreference',$poreference)
        ->where('productions.article',$article)
        ->where('production_sizes.size',$size)
        ->groupBy('productions.line_id','productions.style','productions.article','productions.poreference','production_sizes.size',db::raw('date(qc_endline_defects.created_at)'));
        foreach ($getSumQcEndline->get() as $key => $qc) {
            $name                 = 'output_defect_endline';
            $start                = Carbon::createFromFormat('Y-m-d H:i:s',$awal)->format('H:i:s');
            $end                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('H:i:s');
            $date                  = Carbon::createFromFormat('Y-m-d H:i:s',$akhir)->format('Y-m-d');
            $cekSummaryProduction = SummaryProduction::where('start_hours',$awal)
            ->where('date',$date)
            ->where('end_hours',$akhir)
            ->where('line_id',$qc->line_id)
            ->where('style',$qc->style)
            ->where('poreference',$qc->poreference)
            ->where('size',$qc->size)
            ->where('article',$qc->article)
            ->where('name',$name)
            ->first();
            if ($cekSummaryProduction) {
                SummaryProduction::where('id',$cekSummaryProduction->id)
                ->delete();
            }
            SummaryProduction::create([
                'line_id'     => $qc->line_id,
                'name'        => $name,
                'style'       => $qc->style,
                'poreference' => $qc->poreference,
                'size'        => $qc->size,
                'article'     => $qc->article,
                'output'      => $qc->output_qc,
                'start_hours' => $start,
                'end_hours'   => $end,
                'date'        => $qc->created_at
            ]);
        }
    } 
    
}