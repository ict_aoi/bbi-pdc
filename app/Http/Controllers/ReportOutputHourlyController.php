<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\BreakDownSize;
Config::set(['excel.export.calculate' => true]);
class ReportOutputHourlyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('report_output_perjam.index');
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function exportFormFilter(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        $date = Carbon::createFromFormat('j F, Y',  $request->date)->format('Y-m-d');
        $buyer = $this->Buyer();
        $factory_id = Auth::user()->factory_id;
        $b = Auth::user()->buyer;
        return Excel::create('Report Hourly Output Qc-'.$date."_".Carbon::now()->format('u'),function ($excel)use($date,$factory_id,$buyer){
            $excel->sheet("active", function($sheet) use($date,$factory_id,$buyer){
                $marks = array_fill(0, sizeof($buyer), '?');
                $data  = DB::select(db::Raw("select * from f_hourly_output_qc('$date') where factory_id='$factory_id' and buyer in(" . implode(',', $marks) . ")"),$buyer);
                $row   = 2;
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Buyer');
                $sheet->setCellValue('C1','Line');
                $sheet->setCellValue('D1','Hour');
                $sheet->setCellValue('E1','Style');
                $sheet->setCellValue('F1','Style Ori');
                $sheet->setCellValue('G1','SMV');
                $sheet->setCellValue('H1',(Auth::user()->production=='nagai')?'No. Lot':'Po Buyer');
                $sheet->setCellValue('I1','Article');
                $sheet->setCellValue('J1','Size');
                $sheet->setCellValue('K1','Qty Output');
                foreach(range('A','K') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#Ca8e14');
                        $cells->setFontColor('#FFFFFF');
                    });
                }
                foreach ($data as $d) {
                    $byr = BreakDownSize::getBuyer($d->style,$d->poreference,$d->article,$d->size);
                    $sheet->setCellValue('A'.$row,$d->date);
                    $sheet->setCellValue('B'.$row,strtoupper($byr));
                    $sheet->setCellValue('C'.$row,strtoupper($d->line_name));
                    $sheet->setCellValue('D'.$row,$d->hours);
                    $sheet->setCellValue('E'.$row,$d->style);
                    $sheet->setCellValue('F'.$row,$d->style_merge);
                    $sheet->setCellValue('G'.$row,$d->smv);
                    $sheet->setCellValue('H'.$row,$d->poreference);
                    $sheet->setCellValue('I'.$row,$d->article);
                    $sheet->setCellValue('J'.$row,$d->size);
                    $sheet->setCellValue('K'.$row,$d->qty);
                    foreach(range('A','K') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                    }
                    $sheet->setColumnFormat(
                        array(
                            'H'.$row => '@',
                            'J'.$row => '@',
                            'K'.$row => '0',
                        )
                    );
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

}