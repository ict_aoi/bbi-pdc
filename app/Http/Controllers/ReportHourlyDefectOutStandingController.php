<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Production;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\BreakDownSize;
use App\Models\SummaryProduction;
use App\Models\ProductionSizeHistory;

class ReportHourlyDefectOutStandingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $workingHours = WorkingHours::getWorkingHours();
        return view('report_monitoring_hourly_defect_outstanding.index',compact('workingHours'));
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);
            $factory_id = Auth::user()->factory_id;
            $buyer      = $this->Buyer();
            $marks      = array_fill(0, sizeof($buyer), '?');
            $startDate  = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
            $endDate    = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
            $data       = DB::select(db::Raw("select * from f_hourly_defect_outstanding('$startDate','$endDate') where factory_id='$factory_id' and type_buyer in(" . implode(',', $marks) . ")"),$buyer);
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('buyer',function($data){
                $getBuyer    = $this->getBuyer($data->style,$data->line_id,'buyer');
                return $getBuyer;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('I',function($data){
                return $data->i;
            })
            ->editColumn('II',function($data){
                return $data->ii;
            })
            ->editColumn('III',function($data){
                return $data->iii;
            })
            ->editColumn('IV',function($data){
                return $data->iv;
            })
            ->editColumn('V',function($data){
                return $data->v;
            })
            ->editColumn('VI',function($data){
                return $data->vi;
            })
            ->editColumn('VII',function($data){
                return $data->vii;
            })
            ->editColumn('VIII',function($data){
                return $data->viii;
            })
            ->editColumn('IX',function($data){
                return $data->ix;
            })
            ->editColumn('X',function($data){
                return $data->x;
            })
            ->editColumn('XI',function($data){
                return $data->xi;
            })
            ->editColumn('XII',function($data){
                return $data->xii;
            })
            ->editColumn('XIII',function($data){
                return $data->xiii;
            })
            ->editColumn('total_defect',function($data){
                return $data->total_defect;
            })
            ->make(true);
        }
    }
    private function getTotalStyle($style,$line_id,$date){
        $data = SummaryProduction::where('line_id',$line_id)
        ->where('style',$style)
        ->where('date',$date)
        ->where('name','sewing')
        ->sum('output');
        return $data;
    }
    private function getBuyer($style,$line_id,$remark){
        $production = Production::where(db::raw('style'),'LIKE',"$style%")
        ->where('line_id',$line_id)
        ->orderBy('created_at','desc')
        ->first();
        $breakDown = BreakDownSize::where('poreference',($production)?$production->poreference:'')
        ->first();
        if ($remark=='buyer') {
            if ($breakDown) {
                return $breakDown->buyer;
            } else {
                return '';
            }
            
        } else if ($remark=='category'){
            if ($breakDown) {
                return $breakDown->category;
            } else {
                return '';
            }
        }
    }
    public function exportFormOutputHourly(Request $request)
    {
        $this->validate($request,[
            'date' => 'required'
        ]);
        list($start,$end) = explode("-",$request->date);
        $buyer       = $this->Buyer();
        $marks       = array_fill(0, sizeof($buyer), '?');
        $factory_id  = Auth::user()->factory_id;
        $startDate   = Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d');
        $endDate     = Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d');
        $data        = DB::select(db::Raw("select * from f_hourly_defect_outstanding('$startDate','$endDate') where factory_id='$factory_id' and type_buyer in(" . implode(',', $marks) . ")"),$buyer);
        
        return Excel::create('Hourly Monitoring Defect Outstanding-'.Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('Output Defect Perjam', function($sheet) use($data){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line');
                $sheet->setCellValue('C1','Buyer');
                $sheet->setCellValue('D1','Category');
                $sheet->setCellValue('E1','Style');
                $sheet->setCellValue('F1','Jam Ke-');
                $sheet->mergeCells('F1:R1');
                $sheet->cell('F1:R1',function($cell)
                {
                    $cell->setAlignment('center');
                });
                $sheet->setCellValue('F2','7');
                $sheet->setCellValue('G2','8');
                $sheet->setCellValue('H2','9');
                $sheet->setCellValue('I2','10');
                $sheet->setCellValue('J2','11');
                $sheet->setCellValue('K2','12');
                $sheet->setCellValue('L2','13');
                $sheet->setCellValue('M2','14');
                $sheet->setCellValue('N2','15');
                $sheet->setCellValue('O2','16');
                $sheet->setCellValue('P2','17');
                $sheet->setCellValue('Q2','18');
                $sheet->setCellValue('R2','19');
                $sheet->setCellValue('S1','TOTAL');
                $row = 3;
                foreach ($data as $key => $a) {
                    $getBuyer    = $this->getBuyer($a->style,$a->line_id,'buyer');
                    $getCategory = $this->getBuyer($a->style,$a->line_id,'category');
                    $sheet->setCellValue('A'.$row, $a->date);
                    $sheet->setCellValue('B'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('C'.$row, $getBuyer);
                    $sheet->setCellValue('D'.$row, $getCategory);
                    $sheet->setCellValue('E'.$row, $a->style);
                    $sheet->setCellValue('F'.$row, $a->i);
                    $sheet->setCellValue('G'.$row, $a->ii);
                    $sheet->setCellValue('H'.$row, $a->iii);
                    $sheet->setCellValue('I'.$row, $a->iv);
                    $sheet->setCellValue('J'.$row, $a->v);
                    $sheet->setCellValue('K'.$row, $a->vi);
                    $sheet->setCellValue('L'.$row, $a->vii);
                    $sheet->setCellValue('M'.$row, $a->viii);
                    $sheet->setCellValue('N'.$row, $a->ix);
                    $sheet->setCellValue('O'.$row, $a->x);
                    $sheet->setCellValue('P'.$row, $a->xi);
                    $sheet->setCellValue('Q'.$row, $a->xii);
                    $sheet->setCellValue('R'.$row, $a->xiii);
                    $sheet->setCellValue('S'.$row, '=SUM(A'.$row.':R'.$row.')');
                    $sheet->cells('A'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('B'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('C'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('D'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('E'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('F'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('G'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('H'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('I'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('J'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('K'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('L'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('M'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('N'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('O'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('P'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('Q'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('R'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $sheet->cells('S'.$row, function($cells) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setBackground('#b0d2ff');
                    });
                    $row++;
                }
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('B1:B2');
                $sheet->mergeCells('C1:C2');
                $sheet->mergeCells('D1:D2');
                $sheet->mergeCells('E1:E2');
                $sheet->mergeCells('S1:S2');
                $sheet->cells('A1:A2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('B1:B2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('C1:C2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('D1:D2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('E1:E2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F1:R1', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('F2:R2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });
                $sheet->cells('S1:S2', function($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setBackground('#3b90ff');
                });         
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
}