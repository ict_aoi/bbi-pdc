<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\AndonSupply;
use App\Models\ProductionSizeHistory;


class ReportAndonSupplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $factory        = Factory::active()->pluck('name','id')->all();
        $factory_id     = Auth::user()->factory_id;
        return view('report_andon_supply.index',compact('factory','factory_id'));
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    public function data(Request $request)
    {   
        if(request()->ajax())
        {
            list($start,$end) = explode("-",$request->date);

            $buyer     = $this->Buyer();
            $factory   = Auth::user()->is_super_admin?$request->factory:Auth::user()->factory_id;

            $startDate = $start?Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d'):Carbon::now()->format('Y-m-d');
            $endDate   = $end?Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d'):Carbon::now()->format('Y-m-d');
            $dataClostimeNull      = AndonSupply::getAndonSupplyView($factory,$buyer,$startDate,$endDate,true);
            foreach ($dataClostimeNull->get() as $key => $dt) {
                $dataCloseTime = ProductionSizeHistory::getHistoryDate($dt->transdate,$dt->line_id);
                $closeTime     = $dt->close_time?$dt->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$dt->close_time);

                if ($closeTime<>'') {
                    DB::table('andon_supplies')
                    ->where('id',$dt->id)
                    ->update([
                        'close_time'=> $closeTime,
                    ]);
                }
            }
            $data      = AndonSupply::getAndonSupplyView($factory,$buyer,$startDate,$endDate,false);
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->alias;
            })
            ->editColumn('date',function($data){
                return $data->transdate;
            })
            ->editColumn('name',function($data){
                return ucwords($data->name);
            })
            ->editColumn('working_hours',function($data){
                return $data->start.'-'.$data->end;
            })
            ->editColumn('start_time',function($data){
                return $data->start_time;
            })
            ->editColumn('close_time',function($data){
                $closeTime = '';
                if ($data->close_time) {
                    return $data->close_time;
                } else {
                    $dataCloseTime = ProductionSizeHistory::getHistoryDate($data->transdate,$data->line_id);
                    $closeTime     = $data->close_time?$data->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$data->close_time);

                    if ($closeTime<>'') {
                        DB::table('andon_supplies')
                        ->where('id',$data->id)
                        ->update([
                            'close_time'=> $closeTime,
                        ]);
                    }
                    
                    return $closeTime;
                }
                
            })
            ->editColumn('duration',function($data){
                $dataCloseTime = ProductionSizeHistory::getHistoryDate($data->transdate,$data->line_id);
                $closeDateTime = $data->close_time?$data->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$data->close_time);
                
                if ($closeDateTime) {
                    $closeDate     = Carbon::createFromFormat('Y-m-d H:i:s',$closeDateTime)->format('Y-m-d');
                    $startDate     = $data->transdate;
                    if ($startDate==$closeDate) {
                        return (Carbon::parse( $data->start_time )->diffInMinutes($data->close_time));
                    } else {
                        if ($closeDateTime) {
                            $startTime    = Carbon::createFromFormat('Y-m-d H:i:s', $data->start_time)->format('H:i:s');
                            $closeTime    = Carbon::createFromFormat('Y-m-d H:i:s', $closeDateTime)->format('H:i:s');
                            $dateFirst    = $data->start;
                            $dateEnd      = $data->end;
                            $workingStart = Carbon::parse("$dateFirst");
                            $workingEnd   = Carbon::parse("$dateEnd");
                            $dif1         = Carbon::parse( $startTime )->diffInMinutes($workingEnd);
                            $dif2         = Carbon::parse( $workingStart )->diffInMinutes($closeTime);
                            return $dif1+$dif2;
                        } else {
                            return '';
                        }
                    }
                } else {
                    return '';
                }
            })
            ->make(true);
        }
    }
    
    public function downloadAndonExcel(Request $request)
    {
        // dd($request->all());
        if (!$request->date) {
            return response()->json('date not selected', 422);
        }
        if (Auth::user()->is_super_admin) {
            if (!$request->factory) {
                return response()->json('factory not selected', 422);
            }
        }
        
        list($start,$end) = explode("-",$request->date);

        $buyer     = $this->Buyer();
        $factory   = Auth::user()->is_super_admin?$request->factory:Auth::user()->factory_id;

        $startDate = $start?Carbon::createFromFormat('m/d/Y',trim($start))->format('Y-m-d'):Carbon::now()->format('Y-m-d');
        $endDate   = $end?Carbon::createFromFormat('m/d/Y',trim($end))->format('Y-m-d'):Carbon::now()->format('Y-m-d');
        $dataClostimeNull      = AndonSupply::getAndonSupplyView($factory,$buyer,$startDate,$endDate,true);
        foreach ($dataClostimeNull->get() as $key => $dt) {
            $dataCloseTime = ProductionSizeHistory::getHistoryDate($dt->transdate,$dt->line_id);
            $closeTime     = $dt->close_time?$dt->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$dt->close_time);

            if ($closeTime<>'') {
                DB::table('andon_supplies')
                ->where('id',$dt->id)
                ->update([
                    'close_time'=> $closeTime,
                ]);
            }
        }
        $data      = AndonSupply::getAndonSupplyView($factory,$buyer,$startDate,$endDate,false);
        // dd($data);
        return Excel::create('Report Andon Supply '.Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet("active", function($sheet) use($data){
                $sheet->setCellValue('A1','Date');
                $sheet->setCellValue('B1','Line Name');
                $sheet->setCellValue('C1','Working Hour');
                $sheet->setCellValue('D1','Start');
                $sheet->setCellValue('E1','End');
                $sheet->setCellValue('F1','Duration');
                foreach(range('A','F') as $v){
                    $sheet->cells($v.'1', function($cells) {
                        $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        $cells->setAlignment('center');
                        $cells->setBackground('#Ca8e14');
                        $cells->setFontColor('#FFFFFF');
                    });
                }
                $row = 2;
                foreach ($data->get() as $key => $d) {
                    $dataCloseTime = ProductionSizeHistory::getHistoryDate($d->transdate,$d->line_id);
                    $closeDateTime = $d->close_time?$d->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$d->close_time);
                    
                    if ($closeDateTime) {
                        $closeDate     = Carbon::createFromFormat('Y-m-d H:i:s',$closeDateTime)->format('Y-m-d');
                        $startDate     = $d->transdate;
                        if ($startDate==$closeDate) {
                            $duration = (Carbon::parse( $d->start_time )->diffInMinutes($d->close_time));
                        } else {
    
                            if ($closeDateTime) {
                                $startTime    = Carbon::createFromFormat('Y-m-d H:i:s', $d->start_time)->format('H:i:s');
                                $closeTime    = Carbon::createFromFormat('Y-m-d H:i:s', $closeDateTime)->format('H:i:s');
                                $dateFirst    = $d->start;
                                $dateEnd      = $d->end;
                                $workingStart = Carbon::parse("$dateFirst");
                                $workingEnd   = Carbon::parse("$dateEnd");
                                $dif1         = Carbon::parse( $startTime )->diffInMinutes($workingEnd);
                                $dif2         = Carbon::parse( $workingStart )->diffInMinutes($closeTime);
                                $duration = $dif1+$dif2;
                            } else {
                                $duration = '';
                            }
                        }
                    } else {
                        $duration='';
                    }
                    

                    
                    $sheet->setCellValue('A'.$row,$d->transdate);
                    $sheet->setCellValue('B'.$row,strtoupper($d->name));
                    $sheet->setCellValue('C'.$row,$d->start.'-'.$d->end);
                    $sheet->setCellValue('D'.$row,$d->start_time);
                    $sheet->setCellValue('E'.$row,$closeDateTime);
                    $sheet->setCellValue('F'.$row,$duration);
                    foreach(range('A','F') as $v){
                        $sheet->cells($v.$row, function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                        });
                    }
                    $row++;
                }
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function dashboardAndonSupply(Request $request)
    {
        
        return view('dashboard_andon_supply.index',compact('line_other','line_nagai'));
    }
    public function dataDashboardAndonSupply(Request $request)
    {
        $buyer     = $this->Buyer();
        $factory   = Auth::user()->factory_id;

        
        $date             = '2022-07-27';
        $dataClostimeNull = AndonSupply::getAndonSupplyView($factory,$buyer,$date,$date,true);
        foreach ($dataClostimeNull->get() as $key => $dt) {
            $dataCloseTime = ProductionSizeHistory::getHistoryDate($dt->transdate,$dt->line_id);
            $closeTime     = $dt->close_time?$dt->close_time:((count($dataCloseTime)>0)?$dataCloseTime[0]->date:$dt->close_time);

            if ($closeTime<>'') {
                DB::table('andon_supplies')
                ->where('id',$dt->id)
                ->update([
                    'close_time'=> $closeTime,
                ]);
            }
        }
        $data      = AndonSupply::getAndonSupplyView($factory,$buyer,$date,$date,false);
        $line_other = array();
        $line_nagai = array();
        foreach ($data->get() as $key => $dt) {
            if ($dt->buyer=='nagai') {
                array_push($line_nagai,strtoupper($dt->name));
            } else {
                array_push($line_other,strtoupper($dt->name));
            }
        }
        return response()->json([
            'line_other'       => $line_other,
            'line_nagai'       => $line_nagai,
        ], 200);
    }
}