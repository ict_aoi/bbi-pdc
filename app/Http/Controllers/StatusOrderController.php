<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class StatusOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('report_status_order.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $dateSub = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->subMonths(1)->format('Y-m'):Carbon::now()->subMonths(1)->format('Y-m');
            $dateAdd = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->addMonth(1)->format('Y-m'):Carbon::now()->addMonth(1)->format('Y-m');
            $data       = DB::select(db::Raw("select * from f_status_order('$dateSub','$dateAdd')"));
            
            return datatables()->of($data)
            ->editColumn('line',function($data){
                return strtoupper($data->line);
            })
            ->editColumn('date_order',function($data){
                return $data->dateorder;
            })
            ->editColumn('date_finish',function($data){
                return $data->date_finish_schedule;
            })
            ->editColumn('last_update',function($data){
                return strtoupper($data->last_update);
            })
            ->editColumn('buyer',function($data){
                return strtoupper($data->buyer);
            })
            ->editColumn('category',function($data){
                return strtoupper($data->category);
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('qty_ordered',function($data){
                return $data->qtyordered;
            })
            ->editColumn('qty_receive',function($data){
                return $data->qty_receive;
            })
            ->editColumn('total_output',function($data){
                return $data->total_output;
            })
            ->editColumn('wip_sewing',function($data){
                return $data->wip_sewing;
            })
            ->editColumn('balance',function($data){
                return $data->balance_qc;
            })
            ->setRowAttr([
                'style' => function($data) 
                {
                    if ($data->last_update!=null) {
                        return  'background-color: #4aff3d';
                    } else if($data->qty_receive==0){
                        return  'background-color: #ff3d3d;color: #ffffff;';
                    }
                },
            ])
            ->rawColumns(['style'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    public function exportFormFilter(Request $request)
    {
        // dd($request->all());
        $dateSub = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->subMonths(1)->format('YYYY-mm'):Carbon::now()->subMonths(1)->format('YYYY-mm');
        $dateAdd = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->addMonth(1)->format('YYYY-mm'):Carbon::now()->addMonth(1)->format('YYYY-mm');

        $dateSub = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->subMonths(1)->format('Y-m'):Carbon::now()->subMonths(1)->format('Y-m');
            $dateAdd = ($request->date)?Carbon::createFromFormat('j F, Y',  $request->date)->addMonth(1)->format('Y-m'):Carbon::now()->addMonth(1)->format('Y-m');
            $data       = DB::select(db::Raw("select * from f_status_order('$dateSub','$dateAdd')"));
            
        return Excel::create('Status Order(Filter) -'.Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','LINE NAME');
                $sheet->cells('A1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('B1','DATE ORDER');
                $sheet->cells('B1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('C1','DATE FINISH SEWING');
                $sheet->cells('C1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('D1','LAST UPDATE OUTPUT');
                $sheet->cells('D1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('E1','BUYER');
                $sheet->cells('E1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('F1','CATEGORY');
                $sheet->cells('F1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('G1','PO BUYER');
                $sheet->cells('G1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('H1','STYLE');
                $sheet->cells('H1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('I1','ARTICLE');
                $sheet->cells('I1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('J1','SIZE');
                $sheet->cells('J1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('K1','QTY ORDER');
                $sheet->cells('K1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('L1','QTY LOADING');
                $sheet->cells('L1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('M1','TOTAL QC OUTPUT');
                $sheet->cells('M1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('N1','WIP SEWING');
                $sheet->cells('N1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $sheet->setCellValue('O1','BALANCE OUTPUT');
                $sheet->cells('O1', function($cells) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('center');
                    $cells->setBackground('#0080ff');
                });
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data as $key => $a) {
                    if ($a->qtyordered==$a->total_output) {
                        $background = '#4aff3d';
                    } else if($a->qty_receive==0){
                        $background = '#ff3d3d';
                    }else {
                        $background = '#ecff40';
                    }
                    $row++;
                    $sheet->setCellValue('A'.$row, strtoupper($a->line));
                    $sheet->cells('A'.$row, function($cells)use($background) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground($background);
                    });
                    $sheet->setCellValue('B'.$row, $a->dateorder);
                    $sheet->cells('B'.$row, function($cells)use($background) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground($background);
                    });
                    $sheet->setCellValue('C'.$row, $a->date_finish_schedule);
                    $sheet->cells('C'.$row, function($cells)use($background) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground($background);
                    });
                    $sheet->setCellValue('D'.$row, $a->last_update);
                    $sheet->cells('D'.$row, function($cells)use($background) 
                    {
                        $cells->setBorder('thin','thin','thin','thin');
                        $cells->setAlignment('left');
                        $cells->setBackground($background);
                    });
                    $sheet->setCellValue('E'.$row, $a->buyer);
                    $sheet->cells('E'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('F'.$row, $a->category);
                    $sheet->cells('F'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('G'.$row, $a->poreference);
                    $sheet->cells('G'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('H'.$row, $a->style);
                    $sheet->cells('H'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('I'.$row, $a->article);
                    $sheet->cells('I'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                }); 
                    $sheet->setCellValue('J'.$row, $a->size);
                    $sheet->cells('J'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                }); 
                    $sheet->setCellValue('K'.$row, $a->qtyordered);
                    $sheet->cells('K'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('L'.$row, $a->qty_receive);
                    $sheet->cells('L'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('M'.$row, $a->total_output);
                    $sheet->cells('M'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('N'.$row, $a->wip_sewing);
                    $sheet->cells('N'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $sheet->setCellValue('O'.$row, $a->balance_qc);
                    $sheet->cells('O'.$row, function($cells)use($background) 
                {
                    $cells->setBorder('thin','thin','thin','thin');
                    $cells->setAlignment('left');
                    $cells->setBackground($background);
                });
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'F' => '@',
                    'H' => '0',
                    'I' => '0',
                    'F' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
}
