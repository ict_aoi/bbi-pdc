<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Line;
use App\Models\Sewing;
use App\Models\Factory;
use App\Models\DailyLine;
use App\Models\SewingDetail;
use App\Models\DailyWorking;
use App\Models\WorkingHours;
use App\Models\DailyLineView;
use App\Models\SummaryProduction;


class DisplayController extends Controller
{
    public function index(Request $request,$factory,$line)
    {
        $now             = Carbon::now()->format('Y-m-d H:i:s');
        $master_line = Line::where('code', $line)
        ->whereHas('factory', function ($query) use ($factory) {
            $query->where('code', $factory);
        })
        ->whereNull('delete_at')
        ->first();
        
        $dailyLine = DailyLIneView::whereNull('delete_at')
        ->where('line_id',$master_line->id)
        ->where(db::raw('date(created_at)'),Carbon::now()->format('Y-m-d'))
        ->orderBy('created_at','desc');
        if ($dailyLine->exists()) {
            $hourTo       = 0;
            $dailyWorking = DailyWorking::whereNull('delete_at')
            ->where(db::raw('date(created_at)'),Carbon::createFromFormat('Y-m-d H:i:s', $now)->format('Y-m-d'))
            ->where('line_id',$master_line->id)
            ->first();
            $workingHours = WorkingHours::where('start',">=",$dailyWorking->start)
            ->orderBy('start','ASC');
            $start = "";
            foreach ($workingHours->get() as $key => $working) {
                
                if($working->start>=$dailyWorking->start)
                {
                    
                    $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
                    $akhir = Carbon::now()->format('Y-m-d').' '.$working->end;
                    
                    
                    if($now >= $awal && $now < $akhir){
                        $start = $working->start;
                        $hourTo       = $key;
                    }
                }
            }
            $active = Carbon::createFromFormat('Y-m-d H:i:s', $now)->format('H:i:s')>$dailyWorking->start;
            // dd($hourTo."-".$dailyWorking->working_hours);
            if ($active) {
                // dd($dailyWorking->working_hours."-".$hourTo);
                if ($dailyWorking->working_hours>=$hourTo) {
                    
                    // return view('display.graph_performance',compact('obj'));
                    // return view('display.line_performance',compact('obj'));
                    // dd($obj);
                    return redirect()->route('display.line_performance',[$factory,$line]);
                }else{
                    return view('display.index',compact('master_line','factory','line'));
                }
                
            } else {
                return view('display.index',compact('master_line','factory','line'));
            }
        } else 
        {
            return view('display.index',compact('master_line','factory','line'));
        }
    }
    public function linePerformance(Request $request,$factory,$line)
    {
        $master_line = Line::where('code', $line)
        ->whereHas('factory', function ($query) use ($factory) {
            $query->where('code', $factory);
        })
        ->whereNull('delete_at')
        ->first();
        $dailyLine = 
        $obj             = new StdClass();
        $obj->line_name  = strtoupper($master_line->name);
        $obj->line_id    = $master_line->id;
        $obj->line       = $line;
        $obj->factory_id = $master_line->factory_id;
        $obj->factory    = $factory;
        return view('display.line_performance',compact('obj'));
    }
    public function graphPerformance(Request $request,$factory,$line)
    {
        $now = Carbon::now();
        $master_line = Line::where('code', $line)
        ->whereHas('factory', function ($query) use ($factory) {
            $query->where('code', $factory);
        })
        ->whereNull('delete_at')
        ->first();
        if ($master_line) {
            $labelWorkingHours = '';$targetOutput= 0;$targetPcs= '';$commitment_line= 0;$targetEff= '';$outputQc='';$actual_eff='';$styleIn= array();$qcOutput=0;$qc=0;$defectOutput=0;
            $dailyLine         = DailyLineView::whereNull('delete_at')
            ->whereNotNull('commitment_line')
            ->where('line_id',$master_line->id)
            ->where(db::raw('date(created_at)'),$now)
            ->orderBy('created_at','asc');
            
            foreach ($dailyLine->get() as $key => $s) {
                $ob                  = new StdClass();
                $ob->styles          = $s->style;
                $ob->smv             = $s->smv;
                $ob->commitment_line = $s->commitment_line;
                $styleIn             = $s->style;
                $present_sewer       = $s->present_sewer;
                $working_hours       = $s->working_hours;
                $commitment_line += $s->commitment_line;
                $targetOutput += $s->commitment_line*$s->smv;
                $arrayStyle []        = $ob;
            }
            $jam        = 1;
            $target_eff = (($targetOutput)/($present_sewer*$working_hours*60))*100;
            $target_pcs = $commitment_line/$working_hours;
            $workingHours = WorkingHours::where('start',">=",$dailyLine->first()->start)
            ->where('start',"<=",$dailyLine->first()->end)
            ->orderBy('start','ASC')
            ->get();

            $actual_eff    = array();
            $qc_output     = array();
            $defect_output = array();
            $wft_output    = array();
            $target_wft    = array();
            foreach ($workingHours as $index => $working) {
                // $qcOutput =0;
                $outputQc = 0;
                $defect = 0;
                foreach ($arrayStyle as $key => $style) {
                    
                    $outputQc += $this->getSumProductionall($style->styles,$now,'output_qc_endline',$working->start,$working->end,$master_line->id);
                    $defect    += $this->getSumProductionall($style->styles,$now,'output_defect_endline',$working->start,$working->end,$master_line->id);
                    $totalOutputSmv = $outputQc*$style->smv;
                }
                $jam       = $index+1;
                
                $actualEff = ($totalOutputSmv>0?(($totalOutputSmv)/($present_sewer*1*60))*100:0);
                $wft = ($totalOutputSmv>0?100*($defect/$outputQc):0);
                array_push($actual_eff,round($actualEff));
                array_push($qc_output,$outputQc);
                array_push($wft_output,round($wft));
                array_push($target_wft,$master_line->wft);
                $labelWorkingHours .= $jam.",";
                $targetEff .= round($target_eff).",";
                $targetPcs .= round($target_pcs).",";
                
            }
            $data                    = new StdClass();
            $data->labelWorkingHours = rtrim($labelWorkingHours,",");
            $data->target_eff        = rtrim($targetEff,",");
            $data->target_pcs        = rtrim($targetPcs,",");
            $data->target_wft        = json_encode($target_wft);
            $data->qc_output         = json_encode($qc_output);
            $data->actual_eff         = json_encode($actual_eff);
            $data->wft_output         = json_encode($wft_output);
            $data->line_name          = strtoupper($master_line->name);
            $data->line_id            = $master_line->id;
            $data->line               = $line;
            $data->factory_id         = $master_line->factory_id;
            $data->factory            = $factory;
            return view('display.graph_performance',compact('data'));
        }
        
    }
    public function getDataPerformance(Request $request)
    {
        if ($request->ajax()) {
            $line         = $request->line;
            $line_id      = $request->line_id;
            $factory_id   = $request->factory_id;
            $factory_name = $request->factory;
            $date         = Carbon::now()->format('Y-m-d');
            $line         = Line::find($line_id);
            $now          = Carbon::now()->format('Y-m-d H:i:s');
            $dailyLine    = DailyLineView::whereNull('delete_at')
            ->where('line_id',$line_id)
            ->where(db::raw('date(created_at)'),$date)
            ->orderBy('created_at','asc');
            $lastUpdate = SummaryProduction::select('created_at')
            ->where('date',Carbon::createFromFormat('Y-m-d H:i:s', $now)->format('Y-m-d'))
            ->where('line_id',$line_id)
            ->orderBy('created_at','desc')
            ->first();
            
            $styleIn         = array();
            $commitment_line = 0 ;
            $hourTo          = 0;
            $concateStyle           = '';$smv = '';$cumulative_day = '';$start = '';$end = '';$awal = '';$akhir = '';$current = '';
            
            foreach ($dailyLine->get() as $key => $daily) {
                $styleIn[] = $daily->style;
                $concateStyle .= $daily->style.",";
                $smv .= $daily->smv.",";
                $cumulative_day .= $daily->day.",";
                $commitment_line+=$daily->commitment_line;
                $ob                  = new StdClass();
                $ob->styles           = $daily->style;
                $ob->smv             = $daily->smv;
                $ob->commitment_line = $daily->commitment_line;
                $arrayStyle []        = $ob;
            }
            
            $dailyWorking = DailyWorking::whereNull('delete_at')
            ->where(db::raw('date(created_at)'),$date)
            ->where('line_id',$line_id)
            ->first();
            // dd($dailyWorking);
            $workingHours = WorkingHours::where('start',">=",$dailyWorking->start)
            ->orderBy('start','ASC');
            
            foreach ($workingHours->get() as $key => $working) {
                
                if($working->start>=$dailyWorking->start)
                {
                    
                    $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
                    $akhir = Carbon::now()->format('Y-m-d').' '.$working->end;
                    
                    
                    if($now >= $awal && $now < $akhir){
                        $start        = $awal;
                        $end          = $akhir;
                        $hourTo       = $key+1;
                        $current      = $working->name;
                        $awalCurrent  = $working->start;
                        $akhirCurrent = $working->end;
                    }
                }
            }
            
            // dd($start);
            $targetOutput = 0;
            $outputQc     = 0;
            $actualOutput = 0;
            foreach ($arrayStyle as $key => $s) {
               $outputQc = $this->getSumProductionall($s->styles,$date,'output_qc_endline',$dailyWorking->start,$dailyWorking->end,$line_id);
               $actualOutput +=$outputQc*$s->smv;
               $targetOutput += $s->commitment_line*$s->smv;
            }
            $daily                         = $dailyLine->first();
            $jamke                         = ($hourTo>5)?$hourTo-1:$hourTo;
            $date                          = Carbon::createFromFormat('Y-m-d H:i:s',$now)->format('Y-m-d');
            $startWorking                  = Carbon::createFromFormat('H:i:s',$dailyWorking->start)->format('H:i');
            $endWorking                    = Carbon::createFromFormat('Y-m-d H:i:s', $start)->format('H:i:s');
            $targetHourly                  = round($commitment_line/$dailyWorking->working_hours);
            $targetAccumulation            = $targetHourly*($hourTo-1);
            $outputCurrentSewing           = $this->getSumProduction($styleIn,$date,'sewing',$awalCurrent,$akhirCurrent,$line_id);
            $outputCurrentQc               = $this->getSumProduction($styleIn,$date,'output_qc_endline',$awalCurrent,$akhirCurrent,$line_id);
            $outputAccumulationQc          = $this->getSumProduction($styleIn,$date,'output_qc_endline',$startWorking,$endWorking,$line_id);
            $outputAccumulationSewing      = $this->getSumProduction($styleIn,$date,'sewing',$startWorking,$endWorking,$line_id);
             $outputDefectInline            = $this->getDefect($styleIn,$date,'output_defect_inline',$line_id);
            $outputDefectEndline           = $this->getDefect($styleIn,$date,'output_defect_endline',$line_id);
            $totalOuput                    = ($outputCurrentQc?$outputCurrentQc:0)+($outputAccumulationQc?$outputAccumulationQc:0);
            $wft                           = ($totalOuput>0?100*(($outputDefectEndline)/$totalOuput):0);
            $targetEff                     = (($targetOutput)/($daily->present_sewer*$dailyWorking->working_hours*60))*100;
            $actualEff                     = ($totalOuput>0?(($actualOutput)/($daily->present_sewer*$jamke*60))*100:0);
            $colorEff                      = (round($actualEff)<round($targetEff)?'#ff2e42':'#008c0c');
            $colorWft                      = ($wft>$line->wft?'#ff2e42':'#008c0c');
            $active                        = ($dailyWorking->working_hours>=$jamke?1:Route('display.index',[$factory->code,$line]));
            $runningText                   = $this->runningText($line_id);
            $obj                           = new StdClass();
            $obj->last_update              = ($lastUpdate?Carbon::createFromFormat('Y-m-d H:i:s',$lastUpdate->created_at)->format('d-m-Y H:i:s'):"-");
            $obj->style                    = rtrim($concateStyle,",");
            $obj->smv                      = rtrim($smv,",");
            $obj->cumulative_day           = rtrim($cumulative_day,",");
            $obj->commitment_line          = $commitment_line;
            $obj->man_power                = $daily->present_sewer;
            $obj->current                  = $current;
            $obj->accumulation             = "AKUMULASI (".$startWorking."-".Carbon::createFromFormat('Y-m-d H:i:s', $start)->format('H:i').")";
            $obj->outputCurrentSewing      = ($outputCurrentSewing?$outputCurrentSewing:0);
            $obj->outputCurrentQc          = ($outputCurrentQc?$outputCurrentQc:0);
            $obj->outputAccumulationQc     = ($outputAccumulationQc?$outputAccumulationQc:0);
            $obj->outputAccumulationSewing = ($outputAccumulationSewing?$outputAccumulationSewing:0);
            $obj->targetHourly             = $targetHourly;
            $obj->targetAccumulation       = $targetAccumulation;
            $obj->targetEff                = round($targetEff). " %";
            $obj->actualEff                = "<b style='color:".$colorEff."'>".round($actualEff)." %"."</b>";
            $obj->active                   = $active;
            $obj->targetWft                = number_format($line->wft,1,',',',')." %";
            $obj->runningText              = rtrim($runningText,",");
            $obj->wft                      = "<b style='color:".$colorWft."'>".number_format($wft,1,',',',')." %"."</b>";
            return response()->json($obj, 200);
        }
    }
    private function getSumProduction($style,$date,$name,$awal,$akhir,$line_id)
    {
        $dataSum = SummaryProduction::where('summary_productions.name',$name)
        ->leftJoin('styles','styles.erp','summary_productions.style')
        ->whereIn('styles.default',$style)
        ->where('summary_productions.date',$date)
        ->where('summary_productions.start_hours','>=',$awal)
        ->where('summary_productions.end_hours','<=',$akhir)
        ->where('summary_productions.line_id',$line_id)
        ->sum('summary_productions.output');
        return $dataSum;
    }
    private function getSumProductionall($style,$date,$name,$awal,$akhir,$line_id)
    {
        $dataSum = SummaryProduction::where('summary_productions.name',$name)
        ->leftJoin('styles','styles.erp','summary_productions.style')
        ->where('styles.default',$style)
        ->where('summary_productions.date',$date)
        ->where('summary_productions.start_hours','>=',$awal)
        ->where('summary_productions.end_hours','<=',$akhir)
        ->where('summary_productions.line_id',$line_id)
        ->sum('summary_productions.output');
        return $dataSum;
    }
    private function getDefect($style,$date,$name,$line_id)
    {
        $dataSum = SummaryProduction::where('summary_productions.name',$name)
        ->leftJoin('styles','styles.erp','summary_productions.style')
        ->whereIn('styles.default',$style)
        ->where('summary_productions.date',$date)
        ->where('summary_productions.line_id',$line_id)
        ->sum('summary_productions.output');
        return $dataSum;
    }
    private function runningText($line_id)
    {
        $poreference = '';
        $data = DB::select(db::raw(
            "SELECT DISTINCT
                line_id,
                qo.style,
                qo.poreference,
                qo.article,
                brs.datepromise
            FROM
                qc_output_v qo
            LEFT JOIN(
            select distinct style,poreference,COALESCE(COALESCE(article,color),'-') as article,dateorder,datepromise  from breakdown_size 
            )brs on brs.style=qo.style and brs.poreference=qo.poreference and brs.article=qo.article
            WHERE
                balance <0 and is_show_folding=true and line_id='".$line_id."' "));
        foreach ($data as $key => $data) {
            $poreference.='<b style="color: white"> TGL EXPORT '.$data->datepromise.' : '.$data->style."|".$data->poreference."|".$data->article.",";
        }
        return $poreference;

    }
    
    
}