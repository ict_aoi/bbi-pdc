<?php namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Factory;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('user.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = User::whereNull('delete_at')
            ->where('factory_id',Auth::user()->factory_id);            

            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('factory_name',function($data){
                return ucwords($data->factory->name);
            })
            ->editColumn('department_name',function($data){
            	return ucwords($data->department_name);
            })
            
            ->editColumn('sex',function($data){
            	return ucwords($data->sex);
            })
            ->addColumn('action', function($data) {
                return view('user._action', [
                    'model' => $data,
                    'edit' => route('user.edit',$data->id),
                    'delete' => route('user.destroy',$data->id),
                    'reset' => route('user.resetPassword',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $factory_id = Auth::user()->factory_id;
        $roles      = Role::pluck('display_name', 'id')->all();
        $factory    = Factory::active()->pluck('name','id')->all();
        return view('user.create',compact('roles','factory_id','factory'));
    }

    public function store(Request $request)
    {
        
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);

        if($request->nik)
        {
            if(User::where('nik',$request->nik)->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file         = $request->file('photo');
                $now          = Carbon::now()->format('u');
                $filename     = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image        = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();
                
                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height       = $image_resize->height();
                $width        = $image_resize->width();
                
                $newWidth     = 130;
                $newHeight    = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $factory = factory::find($request->factory);
        
        try
        {
            DB::beginTransaction();
            
            $user = User::firstorCreate([
                'name'              => strtolower($request->name),
                'nik'               => $request->nik,
                'sex'               => $request->sex,
                'production'        => $request->production,
                'factory_id'        => isset($request->factory)?$request->factory:$request->factory_id,
                'department_name'   => strtolower($request->department),
                'photo'             => $image,
                'email'             => $request->nik.'.dummy@'.$factory->code.'.co.id',
                'email_verified_at' => carbon::now(),
                'password'          => bcrypt('password1'),
                'create_user_id'    => auth::user()->id,
                'is_super_admin'    => isset($request->is_super_admin),
            ]);

            $mappings =  json_decode($request->mappings);
            $array = array();

            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
            
            if($user->save()) $user->attachRoles($array);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles      = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles      = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        } 

        $user           = User::find($id);
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role) 
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        return view('user.edit',compact('roles','factory_id','user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
            'sex' => 'required',
        ]);
        
        if($request->nik)
        {
            if(User::where([
                ['nik',$request->nik],
                ['id','!=',$id],
            ])->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $user  = User::find($id);
        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                if(File::exists($avatar)) File::delete($old_file);

                $file         = $request->file('photo');
                $now          = Carbon::now()->format('u');
                $filename     = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image        = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();
                
                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height       = $image_resize->height();
                $width        = $image_resize->width();
                
                $newWidth     = 130;
                $newHeight    = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $factory = factory::find($request->factory);

        $user->nik             = $request->nik;
        $user->name            = strtolower($request->name);
        $user->sex             = $request->sex;
        $user->production      = $request->production;
        $user->factory_id      = Auth::user()->factory_id;
        $user->is_super_admin  = isset($request->is_super_admin);
        $user->department_name = strtolower($request->department);

        if ($image) $user->photo  = $image;
        $user->save();

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $user                 = User::findorFail($id);
        $user->delete_at      = carbon::now();
        $user->delete_user_id = auth::user()->id;
        $user->save();
        return response()->json(200);
    }

    public function resetPassword($id)
    {
        $user           = User::findorFail($id);
        $user->password = bcrypt('password1');
        $user->save();
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user  = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }

    public function getAbsence(Request $request)
    {
        $factory_id = $request->factory_id;
        $factory    = Factory::find($factory_id);
        
        if($factory)
        {
            $absences = DB::connection('bbi_apparel')
            ->table('get_employee') 
            ->select('nik','name','department_name','subdept_name')
            ->get();

            $array = [];
            foreach ($absences as $key => $value) 
            {
                $array [] = $value->nik.':'.$value->name.':'.($value->department_name != ''? $value->department_name : $value->subdept_name);
            }
            
            return response()->json($array,200);
        }else
        {
            return response()->json(200);
        }
        
    }

    public function getInformationUser(Request $request)
    {
        $nik = $request->nik;
        $factory_id = $request->factory_id;
        $factory = Factory::find($factory_id);
        
        if($factory)
        {
            $user_information = DB::connection('bbi_apparel')
                ->table('get_employee') 
                ->where('nik',$nik)
                ->first();

            return response()->json($user_information,200);
        }else
        {
            return response()->json(200);
        }
    }
}