<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Carbon\Carbon;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Factory;
use App\Models\LogLogin;
use App\Models\QtyCarton;
use App\Models\QcEndline;
use App\Models\Production;
use App\Models\AlertFolding;
use App\Models\HangtagOutput;
use App\Models\FoldingOutput;
use App\Models\FoldingPackage;
use App\Models\BarcodeGarment;
use App\Models\Absensi\AbsensiBbi;
use App\Models\HangtagOutputDetail;
use App\Models\FoldingPackageDetail;
use App\Models\FoldingPackageMovement;
use App\Models\FoldingPackageDetailSize;

class AdjustmentFoldingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('adjustment_folding.index');
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    public function breakDownSizeScan(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_fgms = 'fgms_live';
            else $connection_fgms = 'fgms_dev';
            $barcode_id    = $request->barcode;
            $packageDetail = DB::connection($connection_fgms)
            ->table('v_package_detail_split_detail')
            ->where('barcode_id',$barcode_id)
            ->get();
            if (count($packageDetail)==0) {
                return response()->json(['message' => 'Barcode tidak di temukan, Info Packing'], 422);
            }

            $current_department = $packageDetail[0]->current_department;
            $current_status = $packageDetail[0]->current_status;

            if (!($current_department=='preparation'&&$current_status=='completed')&&!($current_department=='sewing'&&$current_status=='onprogress')) {
                $checkScan = DB::connection($connection_fgms)
                ->table('v_check_scan')
                ->where('barcode_id',$barcode_id)
                ->get();
                $message = (count($checkScan))?$checkScan[0]->description:null;
                return response()->json(['message' => "Info Packing, ".$message], 422);
            }
            $style         = array();
            $poreference   = array();
            $article       = array();
            $customer_size = array();
            foreach ($packageDetail as $key => $p) {
                array_push($style,$p->style);
                array_push($poreference,$p->po_number);
                array_push($article,$p->article);
                array_push($customer_size,$p->customer_size);
            }
            $buyer          = $this->Buyer();
            $inner_pack_all = $packageDetail[0]->inner_pack_all;
            $poName         = ($buyer=='nagai')?"NO LOT :":"Po Buyer :";
            $production     = Production::whereNull('productions.delete_at')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->whereIn('productions.poreference',$poreference)
            ->whereIn('productions.style',$style)
            ->whereIn('productions.article',$article)
            ->whereIn('production_sizes.size',$customer_size)->get();
            if (!count($production)>0) {
                return response()->json(['message' => $poName.implode(",",$poreference).", Style :".implode(",",$style)." ,Article :".implode(",",$article).", Size :".implode(",",$customer_size)." Tidak ada"], 422);
            }
            try {
                DB::beginTransaction();
                $array = [];
                foreach ($packageDetail as $key => $value) {
                    $qc_outputs = DB::table('qc_output_v')
                    ->where('poreference',$value->po_number)
                    ->where('style',$value->style)
                    ->where('article',$value->article)
                    ->where('size',$value->customer_size);
                    
                    $obj= new stdClass();
                    foreach ($qc_outputs->get() as $key => $qc) {
                        $foldingPackage = FoldingPackage::whereNull('delete_at')
                        ->where('barcode_id',$value->barcode_id);
                        $folding_package_id = ($foldingPackage->first())?$foldingPackage->first()->id:null;

                        if (!$foldingPackage->exists()) {
                            $foldingPackage = FoldingPackage::FirstOrCreate([
                                'scan_id'            => $value->scan_id,
                                'barcode_id'         => $value->barcode_id,
                                'pkg_count'          => $value->pkg_count?$value->pkg_count:0,
                                'item_qty'           => $value->inner_pack_all,
                                'current_department' => "sewing",
                                'current_status'     => "onprogress"
                            ]);
                            $packageDetail = DB::connection($connection_fgms)
                            ->table('packages')
                            ->where('barcode_id',$barcode_id)
                            ->update([
                                'current_department' => 'sewing',
                                'current_status'     => 'onprogress',
                                'status_sewing'      => 'onprogress',
                                'updated_at'         => Carbon::now()
                            ]);
                            $folding_package_id = $foldingPackage->id;
                        }
                        $foldingPackageDetailSize = FoldingPackageDetailSize::whereNull('delete_at')
                        ->where('folding_package_id',$folding_package_id)
                        ->where([
                            ['poreference',$value->po_number],
                            ['style',$value->style],
                            ['size',$value->customer_size],
                            ['article',$value->article],
                            [ 'folding_package_id',$folding_package_id],
                        ]);

                        $folding_package_detail_size_id = ($foldingPackageDetailSize->first())?$foldingPackageDetailSize->first()->id:null;
                        $folding_inner_pack = ($foldingPackageDetailSize->first())?$foldingPackageDetailSize->first()->inner_pack:null;
                        if (!$foldingPackageDetailSize->exists()) {
                            $foldingPackageDetailSize = FoldingPackageDetailSize::Create([
                                'folding_package_id' => $folding_package_id,
                                'style'              => $value->style,
                                'poreference'        => $value->po_number,
                                'size'               => $value->customer_size,
                                'article'            => $value->article,
                                'inner_pack'         => $value->inner_pack
                            ]);
                            $folding_package_detail_size_id = $foldingPackageDetailSize->id;
                            $folding_inner_pack             = $foldingPackageDetailSize->inner_pack;
                        }
                        $packageDetail = DB::connection($connection_fgms)
                        ->table('package_movements')
                        ->where('barcode_package',$barcode_id)
                        ->whereNull('deleted_at')
                        ->update([
                            'deleted_at'         => Carbon::now()
                        ]);
                        $line_fgms = DB::connection($connection_fgms)
                        ->table('lines')
                        ->where('name',$qc->code)
                        ->get();
                        $line_id_fgms = (count($line_fgms))>0?$line_fgms[0]->id:null;
                        
                        $packageDetail = DB::connection($connection_fgms)
                        ->table('package_movements')
                        ->insert([
                            'barcode_package' => $barcode_id,
                            'department_from' => 'preparation',
                            'department_to'   => 'sewing',
                            'status_from'     => 'sewing',
                            'status_to'       => 'onprogress',
                            'user_id'         => ($buyer=='other')?41:42,
                            'created_at'      => Carbon::now(),
                            'description'     => 'accepted on checkin sewing',
                            'line_id'         => $line_id_fgms,
                            'ip_address'      => \Request::ip()
                        ]);
                        $foldingOutput = FoldingOutput::whereNull('delete_at')
                        ->where('production_id',$qc->production_id)
                        ->where('production_size_id',$qc->production_size_id)
                        ->first();

                        $foldingPackageDetail = FoldingPackageDetail::whereNull('delete_at')
                        ->where([
                            ['folding_package_detail_size_id',$folding_package_detail_size_id],
                            ['folding_package_id',$folding_package_id],
                        ]);
                        $dataQtyCarton = QtyCarton::where([
                            ['style',$qc->style],
                            ['size',$qc->size],
                        ])->first();
                        $qtyPolibag = $dataQtyCarton?$dataQtyCarton->qty:1;
                        
                        $qty_available                       = (int)$folding_inner_pack -  (int)$foldingPackageDetail->count();
                        $foldingCounter                      = ($foldingOutput)?$foldingOutput->counter:0;
                        $balance                             = (int)$qc->qty_order - (int)$foldingCounter;
                        $obj->folding_package_id             = $folding_package_id;
                        $obj->line_name                      = $qc->line_name;
                        $obj->production_id                  = $qc->production_id;
                        $obj->production_size_id             = $qc->production_size_id;
                        $obj->folding_package_detail_size_id = $folding_package_detail_size_id;
                        $obj->poreference                    = $value->po_number;
                        $obj->style                          = $value->style;
                        $obj->article                        = $value->buyer_item;
                        $obj->size                           = $value->customer_size;
                        $obj->inner_pack                     = $value->inner_pack;
                        $obj->qty_order                      = $qc->qty_order;
                        $obj->qty_input                      = $foldingPackageDetail->count();
                        $obj->wip                            = (int)$qc->total_output- (int)$foldingCounter;
                        $obj->balance                        = $balance;
                        $obj->qty_available                  = $qty_available;
                        $obj->status                         = 0;
                        $obj->qty_polibag                    = $qtyPolibag;
                        
                    }
                    $array []              = $obj;
                }
                DB::commit();
                return response()->json(['data'=>$array,'inner_pack'=>$inner_pack_all,'status'=>0],200);

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_fgms = 'fgms_live';
            else $connection_fgms = 'fgms_dev';

            $status                     = '0';
            $qtyInput                   = $request->qty_input;
            $productionId               = $request->production_id;
            $productionSizeId           = $request->production_size_id;
            $foldingPackageId           = $request->folding_package_id;
            $foldingPackageDetailSizeId = $request->folding_package_detail_size_id;

            try {
                DB::beginTransaction();
                $folding_package_id = '';
                $dataPackage = FoldingPackageDetailSize::dataPackageDetailSize($foldingPackageDetailSizeId);
                $qtyInner    = (int)$dataPackage->qty_inner + (int)$qtyInput;
                $qtyPackage  = (int)$dataPackage->qty_package + (int)$qtyInput;
                
                if ($qtyInput==0) {
                    return response()->json('Qty tidak boleh 0', 422);
                }
                if ($qtyInner>$dataPackage->inner_pack) {
                    return response()->json('Qty karton sudah terpenuhi.', 422);
                }
                if ($qtyPackage>$dataPackage->item_qty) {
                    return response()->json('Qty Item sudah terpenuhi.', 422);
                }
                $barcodeGarment = BarcodeGarment::whereNull('delete_at')
                ->where([
                    ['style','adjustment'],
                ])->first();
                $barcode_garment_id = $barcodeGarment?$barcodeGarment->id:null;
                if (!$barcodeGarment) {
                    $insert = BarcodeGarment::create([
                        'style'      => 'adjustment',
                        'barcode_id' => '1111111111'
                    ]);
                    $barcode_garment_id = $insert->id;
                }
                $dataFoldingOutput = FoldingOutput::whereNull('delete_at')
                                    ->where('production_id',$productionId)
                                    ->where('production_size_id',$productionSizeId)
                                    ->first();
                $folding_output_id = ($dataFoldingOutput)?$dataFoldingOutput->id:null;
                $foldingOutput     = ($dataFoldingOutput)?$dataFoldingOutput->counter:0;

                if(!$dataFoldingOutput){
                    $dataFoldingOutput = FoldingOutput::create([
                        'production_id'      => $productionId,
                        'production_size_id' => $productionSizeId,
                        'counter'            => 0,
                        'scan_from'          => 'folding'
                    ]);
                    $folding_output_id = $dataFoldingOutput->id;
                    $foldingOutput     = $dataFoldingOutput->counter;
                }
                $dataFoldingOutput->counter    = $foldingOutput+$qtyInput;
                $dataFoldingOutput->updated_at = Carbon::now();
                $dataFoldingOutput->save();
                
                for ($i=0; $i < $qtyInput; $i++) { 
                    FoldingPackageDetail::create([
                        'folding_output_id'              => $folding_output_id,
                        'folding_package_id'             => $foldingPackageId,
                        'folding_package_detail_size_id' => $foldingPackageDetailSizeId,
                        'barcode_garment_id'             => $barcode_garment_id,
                        'ip_address'                     => \Request::ip(),
                        'created_by_name'                => Auth::user()->name,
                        'created_by_nik'                 => Auth::user()->nik
                    ]);
                }
                $dataFoldingPackage          = FoldingPackage::find($foldingPackageId);
                $dataFoldingPackage->counter = $dataFoldingPackage->counter+$qtyInput;
                $dataFoldingPackage->save();

                $production = Production::find($productionId);
                $line       = $production->line->alias;
                $buyer      = $production->line->buyer;
                $line_fgms  = DB::connection($connection_fgms)
                                    ->table('lines')
                                    ->where('name',$line)
                                    ->get();
                $line_id_fgms   = (count($line_fgms))>0?$line_fgms[0]->id:null;
                $foldingPackage = FoldingPackage::find($foldingPackageId);
                $cekComplete    = ($foldingPackage->item_qty==$foldingPackage->counter);

                if ($cekComplete) {

                    $foldingPackage->current_department = 'sewing';
                    $foldingPackage->current_status     = 'completed';
                    $foldingPackage->updated_at         = Carbon::now();
                    $foldingPackage->save();

                    DB::connection($connection_fgms)
                    ->table('packages')
                    ->where('barcode_id',$foldingPackage->barcode_id)
                    ->update([
                        'current_department' => 'sewing',
                        'current_status'     => 'completed',
                        'status_sewing'      => 'completed',
                        'updated_at'         => Carbon::now()
                    ]);
                    $packageDetail = DB::connection($connection_fgms)
                    ->table('package_movements')
                    ->where('barcode_package',$foldingPackage->barcode_id)
                    ->whereNull('deleted_at')
                    ->update([
                        'deleted_at'         => Carbon::now()
                    ]);
                    $packageDetail = DB::connection($connection_fgms)
                    ->table('package_movements')
                    ->insert([
                        'barcode_package' => $foldingPackage->barcode_id,
                        'department_from' => 'sewing',
                        'department_to'   => 'sewing',
                        'status_from'     => 'onprogress',
                        'status_to'       => 'completed',
                        'user_id'         => ($buyer=='other')?41:42,
                        'created_at'      => Carbon::now(),
                        'description'     => 'set as completed on sewing',
                        'line_id'         => $line_id_fgms,
                        'ip_address'      => \Request::ip()
                    ]);
                    $status = '1';
                }
                DB::commit();
                return response()->json(['status' => $status],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
}