<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use View;
use Excel;
use Config;
use DateTime;
use StdClass;
use Validator;
use Carbon\Carbon;
use App\Models\Line;

use App\Models\User;
use App\Models\Style;
use App\Models\Factory;
use App\Models\Workflow;
use App\Models\CycleTime;
use App\Models\StatusRound;
use Illuminate\Http\Request;
use App\Models\CycleTimeBatch;
use App\Models\WorkflowDetail;
use App\Models\CycleTimeDetail;
use App\Models\ResponseWorkflow;
use Yajra\Datatables\Datatables;
use App\Models\CycleWorkflowDetail;
use App\Models\HistoryProcessSewing;
Config::set(['excel.export.calculate' => true]);

class CycleTimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function Index()
    {
        $buyer = Auth::user()->production;
        $b = $this->Buyer();
        $lines = Line::whereNull('delete_at')
        ->whereIn('buyer',$b)
        ->orderBy('order','asc')
        ->pluck('name','id')
        ->all();
        $factory = Factory::active()->pluck('name','id')->all();
        return view('cycle_time.index',compact('buyer','lines','factory'));
    }
    public function getDataLine(Request $request)
    {
        $this->validate($request,[
            'factory_id'=>'required',
        ]);
        $factory = Factory::find($request->factory_id);
        if (!$factory) {
            return response()->json('Factory tidak di temukan', 422);
        }
        $b = $this->Buyer();
        
        $lines = Line::whereNull('delete_at')
        ->whereIn('buyer',$b)
        ->where('factory_id',$request->factory_id)
        ->orderBy('order','asc')
        ->pluck('name','id')
        ->all();
        return response()->json(['lines' => $lines]);
    }
    public function data()
    {
    	if(request()->ajax())
        {
            $data = CycleTimeBatch::whereNull('delete_at')
            ->where('create_user_id',Auth::user()->id)
            ->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('line',function($data){
            	return ucwords($data->line->name);
            })
            ->editColumn('style',function($data){
            	return $data->style;
            })
            ->editColumn('condition_day',function($data){
            	return $data->day;
            })
            ->editColumn('nik',function($data){
            	return $data->user->nik;
            })
            ->editColumn('name',function($data){
            	return ucwords($data->user->name);
            })
            ->editColumn('start_job',function($data){
            	return $data->start_job;
            })
            ->editColumn('end_job',function($data){
            	return $data->end_job;
            })
            ->editColumn('end_job',function($data){
            	return $data->end_job;
            })
            ->editColumn('status',function($data){
                if ($data->end_job==null) {
                    return '<span class="label label-danger">In progress</span>';
                }else{
                    return '<span class="label label-flat border-success text-success-600 position-right">Done</span>';
                }
            })
            
            ->addColumn('action', function($data) {
                return view('cycle_time._action', [
                    'model' => $data,
                    'edit' => route('cycle_time.create_cycle',$data->id),
                    'report' => route('cycle_time.report_time_study',$data->id),
                    'delete' => route('alur_kerja.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action','status'])
            ->make(true);
        }
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = array('nagai','other');
        }
        return $data;
    }
    static function getSewer(Request $request)
    {
        if ($request->ajax()) {
            $dataCycleTimeBatch = CycleTimeBatch::find($request->cycle_time_batch_id);
            if (!$dataCycleTimeBatch) {
                return response()->json(['message' => 'Ada Kesalahan Silahkan refresh halaman.'], 422);
            }
            $factory    = Factory::find($dataCycleTimeBatch->factory_id);
            if (!$factory) {
                return response()->json(['message' => 'Ada Kesalahan Silahkan refresh halaman.'], 422);
            }
            if($factory)
            {
                $absences = DB::connection('bbi_apparel')
                    ->table('get_employee') 
                    ->select('nik','name','department_name','subdept_name')
                    ->get();
                    

                $array = [];
                foreach ($absences as $key => $value) 
                {
                    $array [] = $value->nik.':'.$value->name;
                }
                
                return response()->json($array,200);
            }else
            {
                return response()->json(200);
            }
        }
    }
    public function linePicklist(Request $request)
    {
        $q          = trim(strtolower($request->q));
        $factory_id = Auth::user()->factory_id;
        $buyer      = $this->Buyer();

        $lists      = Line::where('factory_id', $factory_id)
        ->where([
            ['alias', 'LIKE', "%$q%"],
            ['code','<>','all']
        ])
        ->whereIn('buyer',$buyer)
        ->orderBy('order','asc')
        ->paginate(10);
        return view('qc_inline._line_picklist', compact('lists'));
    }
    public function stylePicklist(Request $request)
    {
        if ($request->ajax()) {
            $style = DB::table('list_style_sewing_v')
            ->where([
                ['line_id',$request->line_id],
            ])
            ->whereRaw('total_qty>total_counter')
            ->orderBy('max_date','desc')
            ->pluck('style','style')->all();;
            return response()->json(['style' => $style]);
        }
    }
    public function getDayHeader(Request $request)
    {
        if ($request->ajax()) {
            $line_id = $request->line_id;
            $style   = $request->value;
            if ($style!='') {
                $cekExist = CycleTimeBatch::whereNull('delete_at')
                ->where([
                    ['line_id',$line_id],
                    ['style',$style],
                    ['create_user_id',Auth::user()->id],
                    
                ])
                ->orderBy('created_at','desc')
                ->first();
                $day=1;
                if ($cekExist) {
                    if ($cekExist->end_job==null) {
                        return response()->json(['message' => 'Style sebelum nya blm di lock.'], 422);
                    }else{
                        $day= $cekExist->day+1;
                    }
                }
                return response()->json(['day'=>$day],200);
            }
        }
    }
    public function storeHeader(Request $request)
    {
        if ($request->ajax()) {
            $line_id  = $request->line;
            $style    = $request->style;
            $day      = $request->day;
            $cekExist = CycleTimeBatch::whereNull('delete_at')
            ->where([
                ['line_id',$line_id],
                ['style',$style],
                ['day',$day],
                ['create_user_id',Auth::user()->id],
                
            ])
            ->exists();
            if($cekExist) return response()->json(['message' => 'Style Sudah ada.'], 422);
            try {
                DB::beginTransaction();
                $cycle = CycleTimeBatch::firstorCreate([
                    'line_id'        => $line_id,
                    'style'          => trim($style),
                    'day'            => trim($day),
                    'start_job'      => Carbon::now(),
                    'factory_id'     => Auth::user()->factory_id,
                    'create_user_id' => Auth::user()->id
                ]);
                if ($day>1) {
                    $cekBatch = CycleTimeBatch::whereNull('delete_at')
                    ->where([
                        ['line_id',$line_id],
                        ['style',$style],
                        ['day','<',$day],
                        ['create_user_id',Auth::user()->id],
                    ])
                    ->orderBy('day','desc')
                    ->first();
                    $idBatch = $cekBatch->id;
                    $dataCycleTime = CycleTime::whereNull('delete_at')
                    ->where([
                        ['cycle_time_batch_id',$idBatch],
                        ['is_draft',false],
                    ])
                    ->orderBy('created_at','asc')
                    ->get();
                    foreach ($dataCycleTime as $key => $d) {
                        $cycleTime = CycleTime::Create([
                            'cycle_time_batch_id'  => $cycle->id,
                            'cycle_time_id_before' => $d->id,
                            'wip'                  => $d->wip,
                            'sewer_name'           => $d->sewer_name,
                            'sewer_nik'            => $d->sewer_nik,
                            'average'              => $d->average,
                            'is_draft'             => $d->is_draft,
                            'is_copied'            => true,
                        ]);
                        $dataCycleDetail = CycleTimeDetail::where('cycle_time_id',$d->id)->get();
                        foreach ($dataCycleDetail as $key => $detail) {
                            CycleTimeDetail::Create([
                                'cycle_time_id'      => $cycleTime->id,
                                'time'               => $detail->time,
                                'time_recorded'      => $detail->time_recorded,
                                'duration'           => $detail->duration,
                            ]);
                        }
                        $dataWorkflowDetail = CycleWorkflowDetail::where('cycle_time_id',$d->id)->get();
                        foreach ($dataWorkflowDetail as $key => $workflowDetail) {
                            CycleWorkflowDetail::Create([
                                'cycle_time_id'      => $cycleTime->id,
                                'workflow_detail_id' => $workflowDetail->workflow_detail_id
                            ]);
                        }
                    }
                }
                DB::commit();
                $url = route('cycle_time.create_cycle',$cycle->id);
                
                return response()->json($url,200);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function createCycle(Request $request,$id)
    {
        $checkCycleBatch = CycleTimeBatch::whereNull('delete_at')
        ->where('id',$id)
        ->whereNull('end_job')->first();
        
        if($checkCycleBatch){
            $line           = $checkCycleBatch->line->name;
            $styleErp       = Style::where('erp',$checkCycleBatch->style)->first();
            $style          = $styleErp?$styleErp->default:$checkCycleBatch->style;
            $data           = CycleTimeBatch::getWorkflow($style);
            $workflowId     = $data?$data->id:null;
            $isCopied       = $checkCycleBatch->day>1?1:0;
            $conditionalDay = $checkCycleBatch->day;
            
            return view('cycle_time.create_cycle',compact('id','line','style','workflowId','isCopied','conditionalDay'));
        }else{
            return redirect()->back();
        }
    }
    public function getProcess(Request $request)
    {
        // dd($request->all());
            $batchId     = CycleTimeBatch::findorFail($request->id);
            $workflow_id = $request->workflow_id;
            $component   = $request->component;
            $workflow    = Workflow::find($workflow_id);
            $style       = $workflow?$workflow->style:'';
            $q           = trim(strtolower($request->q));
            $search      = str_replace(" ","%",$q);
            
            $date        = Carbon::now()->format('Y-m-d');
            $roundBefore = StatusRound::whereNotNull('delete_at')->where('line_id',$batchId->line_id)->orderBy('delete_at','desc')->first();
            $dataRound   = StatusRound::whereNull('delete_at')->where('line_id',$batchId->line_id)->orderBy('delete_at','desc')->first();
            $round       = $dataRound?$dataRound->round:null;
            $dbefore     = ($roundBefore)?$roundBefore->created_at:'9999-01-01 07:00:00';
            $rBefore     = ($roundBefore)?$roundBefore->round:'0';
            $dateBefore  = Carbon::createFromFormat('Y-m-d H:i:s',$dbefore)->format('Y-m-d');
            
            $listProcess = WorkflowDetail::whereNull('workflow_details.delete_at')
                ->select('workflow_details.id as workflow_detail_id', 'workflow_details.process_sewing_id as process_id', 'process_sewings.name as proses_name','process_sewings.component', 'workflow_details.last_process', db::raw('COALESCE(total_cycle.total_cycle,0) as count_inline'),'machines.name as machine_name','workflow_details.machine_id')
                ->leftjoin('process_sewings', 'process_sewings.id', 'workflow_details.process_sewing_id')
                ->leftjoin('machines', 'machines.id', 'workflow_details.machine_id')
                ->leftjoin(
                    DB::raw('(select ctb.line_id,cwd.workflow_detail_id,max(ct.created_at) as created_at,count(*) as total_cycle from cycle_times ct LEFT JOIN cycle_time_batches ctb on ctb.id=ct.cycle_time_batch_id
                    LEFT JOIN cycle_workflow_details cwd on cwd.cycle_time_id=ct.id
                    where ctb.line_id='."'$batchId->line_id'".' GROUP BY ctb.style,ctb.line_id,cwd.workflow_detail_id)total_cycle'),
                    function ($leftjoin) {
                        $leftjoin->on('workflow_details.id', '=', 'total_cycle.workflow_detail_id');
                    }
                )
                ->leftjoin(
                    DB::raw('(select qi.workflow_detail_id,pr.line_id,s.default,max(qi.created_at) as max_created from qc_inlines qi
                    LEFT JOIN productions pr on pr.id=qi.production_id left join styles s on s.erp=pr.style  where date(qi.created_at)=' . "'$dateBefore'" . ' and round='.$rBefore.' and pr.line_id=' . "'$batchId->line_id'" . ' and s.default=' . "'$style'" . '  GROUP BY qi.workflow_detail_id,pr.line_id,s.default)
                    round_before'),
                    function ($leftjoin) {
                        $leftjoin->on('workflow_details.id', '=', 'round_before.workflow_detail_id');
                    }
                )
                ->where(function($query) use ($search){
                    $query->where(db::raw('lower(process_sewings.component)'), 'LIKE', "%$search%")
                    ->orWhere(db::raw('lower(process_sewings.name)'), 'LIKE', "%$search%");
                })
                ->where('workflow_details.workflow_id', $workflow_id)
                ->where('process_sewings.component', $component)
                ->orderBy(db::raw('COALESCE(total_cycle.total_cycle,0)'), 'asc')
                ->orderBy('round_before.max_created', 'asc')
                ->orderBy('process_sewings.created_at', 'asc')
                ->paginate(10);
            // return view('cycle_time.lov._lov_process', compact('listProcess'));
            $data = [
                'view'          => View::make('cycle_time.lov._lov_process',compact('listProcess'))->render(),
                'data'          => $listProcess
            ];

            return response()->json($data);
    }
    public function storeDetailCycle(Request $request,$params)
    {
        if ($request->ajax()) {
            $this->validate($request, [
                'pick_time_data'      => 'required',
                'cycle_time_batch_id' => 'required',
                'sewer_nik'           => 'required',
                'sewer_name'          => 'required',
            ]);
            // dd($request->all());
            $itemCycle           = json_decode($request->pick_time_data);
            $pickProses          = json_decode($request->process_pick_list);
            $cycle_time_batch_id = $request->cycle_time_batch_id;
            $workflow_detail_id  = $request->workflow_detail_id;
            $cycle_time_id       = $request->cycle_time_id;
            $sewer_nik           = $request->sewer_nik;
            $sewer_name          = $request->sewer_name;
            $wip                 = $request->wip;
            $stiches_burst       = $request->stiches_burst;
            $isDraft             = ($params=='draft')?true:false;
            $is_assembly         = isset($request->is_assembly);
            // $created_at          = (!$params)?Carbon::now():null;
            $checkLock           = CycleTimeBatch::find($cycle_time_batch_id);
            if ($checkLock->end_job!=null) {
                return response()->json(['message' => 'Data Sudah di Lock, Cycle time tidak bisa di simpan'], 422);
            }
            $arrayWorkflowDetail = array();
            foreach ($pickProses as $key => $proses) {
                $arrayWorkflowDetail[] = $proses->workflow_detail_id;
            }

            if ($cycle_time_id==0) {
                // dd($arrayWorkflowDetail);
                $existCycle = CycleTime::whereNull('cycle_times.delete_at')
                ->select('cycle_times.id')
                ->leftjoin('cycle_workflow_details','cycle_workflow_details.cycle_time_id','cycle_times.id')
                ->where([
                    ['cycle_times.cycle_time_batch_id',$cycle_time_batch_id],
                    ['cycle_times.sewer_nik',$sewer_nik],
                ])
                ->whereIn('cycle_workflow_details.workflow_detail_id',$arrayWorkflowDetail)
                ->first();

                if ($existCycle) {
                    return response()->json(['message' => 'Data Sudah Ada,Apakah data akan di update?','id' => $existCycle->id], 433);
                }
            }else{
                CycleTime::where('id', $cycle_time_id)->update(array('delete_at' => Carbon::now()));
            }

            try {
                DB::beginTransaction();
                $cycleTime = CycleTime::Create([
                    'cycle_time_batch_id' => $cycle_time_batch_id,
                    'workflow_detail_id'  => $workflow_detail_id,
                    'wip'                 => $wip,
                    'burst'               => $stiches_burst,
                    'is_assembly'         => $is_assembly,
                    'sewer_name'          => strtolower($sewer_name),
                    'sewer_nik'           => $sewer_nik,
                    'is_draft'            => $isDraft
                ]);
                foreach ($pickProses as $key => $proses) {
                    CycleWorkflowDetail::Create([
                        'cycle_time_id'      => $cycleTime->id,
                        'workflow_detail_id' => $proses->workflow_detail_id
                    ]);
                }
                $sum   = 0;
                $count = 0;
                foreach ($itemCycle as $key => $item) {
                    $time    = $this->parts($item->interval);
                    // dd($time);
                    
                    $duration = $time[0] * 60 + $time[1]+($time[2]*0.001);
                    CycleTimeDetail::Create([
                        'cycle_time_id' => $cycleTime->id,
                        'time'          => '00:'.$time[0].':'.$time[1],
                        'time_recorded' => $item->time_recorded,
                        'duration'      => $duration,
                    ]);
                    $sum+=$duration;
                    $count++;
                }
                $CycleDetail = CycleTimeDetail::select(db::raw('sum(duration) as duration'),db::raw('count(*) as total'))
                ->where('cycle_time_id',$cycleTime->id)->first();
                $average = ($CycleDetail)?$CycleDetail->duration/$CycleDetail->total:0;
                CycleTime::where('id', $cycleTime->id)->update(array('average' => $average));
                DB::commit();
                return response()->json(['success'=>'success'],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function storeDraftCycle(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $cycle_id      = $request->cycle_time_id;
                $itemCycle     = json_decode($request->pick_time_data);
                $now           = Carbon::now()->format('Y-m-d');
                $dataCycleTime = CycleTime::find($cycle_id);
                $dateDraft     = Carbon::createFromFormat('Y-m-d H:i:s', $dataCycleTime->created_at)->format('Y-m-d');
                $date          = ($dateDraft==$now)?$dataCycleTime->created_at:Carbon::now();
                // dd($request->all());
                $sum   = 0;
                $count = 0;
                foreach ($itemCycle as $key => $item) {
                    $time    = $this->parts($item->interval);
                    $duration = $time[0] * 60 + $time[1]+($time[2]*0.001);
                    if ($item->id=='-1') {
                        
                        
                        CycleTimeDetail::Create([
                            'cycle_time_id' => $cycle_id,
                            'time'          => '00:'.$time[0].':'.$time[1],
                            'time_recorded' => $item->time_recorded,
                            'duration'      => $duration,
                        ]);
                    }

                    $sum+=$duration;
                    $count++;
                    
                }
                $CycleDetail = CycleTimeDetail::select(db::raw('sum(duration) as duration'),db::raw('count(*) as total'))
                ->where('cycle_time_id',$cycle_id)->first();
                $average = ($CycleDetail)?$CycleDetail->duration/$CycleDetail->total:0;
                $dataCycleTime->created_at = $date;
                $dataCycleTime->average = $average;
                $dataCycleTime->is_draft = false;
                $dataCycleTime->save();
                DB::commit();
                return response()->json(['success'=>'success'],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }
    }
    public function destroyDraftCycleTimeDetail(Request $request,$id)
    {
        if ($request->ajax()) {
            CycleTimeDetail::where('id',$id)
            ->delete();
        }
    }
    private function parts($time)
    {
        if (empty($time)) {
            return 0;
        }
        return explode(':', $time);
    }
    public function showHistory(Request $request)
    {
        if ($request->ajax()) {
            list($nik,$nama) = explode(":",$request->nik);
            
            $id                 = $request->cycle_time_batch_id;
            $workflowId         = $request->workflowId;
            $dataCycleTimeBatch = CycleTimeBatch::find($id);
            $line_id            = $dataCycleTimeBatch->line_id;
            $style              = $dataCycleTimeBatch->style;


            $list       = HistoryProcessSewing::whereNull('delete_at')
            ->whereNotNull('workflow_detail_id')
            ->where([
                ['line_id', $line_id],
                ['sewer_nik', $nik],
                ['style', $style]
            ])->get();
            $workflow = Workflow::find($workflowId);
            if ($workflow==null) {
                return response()->json(['message' => 'Data Proses belum ada, info Development'], 422);
            }
            $listCount          = $list->count();
            $workflow_detail_id = ($listCount>0)?$list[0]->workflow_detail_id:null;
            $urlprocess         = null;
            $machine_name       = null;
            $machine_id         = null;
            $process_name       = null;
            if ($workflow->delete_at==null) {
                if ($listCount==1) {
                    $dataWorkflowDetail = WorkflowDetail::whereNull('delete_at')
                                            ->whereNotNull('machine_id')
                                            ->where('id',$list[0]->workflow_detail_id)->first();
                    if (!$dataWorkflowDetail) {
                        $listCount=0;
                    }
                    
                    $machine_name = ($dataWorkflowDetail->machine->name)?$dataWorkflowDetail->machine->name:null;
                    $machine_id   = ($dataWorkflowDetail->machine->id)?$dataWorkflowDetail->machine->id:null;
                    $process_name = $dataWorkflowDetail->processSewing->name;
                }else if($listCount>1){
                    $urlprocess = route('cycle_time.history_process', [$line_id,$nik,$style]);
                }
            }

            $data = array(
                'list'               => $listCount,
                'urlprocess'         => $urlprocess,
                'workflow_detail_id' => $workflow_detail_id,
                'machine_name'       => $machine_name,
                'machine_id'         => $machine_id,
                'process_name'       => $process_name
            );
            return  response()->json($data, 200);

        }
    }
    public function HistoryProcess(Request $request,$line_id,$nik,$style)
    {
        $q           = trim(strtolower($request->q));
        $workflow_id = $request->workflow_id;
        $listProcess = HistoryProcessSewing::select(
            'history_process_sewings.workflow_detail_id',
            'workflow_details.process_sewing_id',
            'workflow_details.machine_id',
            'process_sewings.component',
            db::raw('process_sewings.name as proses_name'),
            db::raw('machines.name as machine_name'),
            db::raw('0 as count_inline'),
            'workflow_details.last_process'
        )
        ->leftJoin('workflow_details','workflow_details.id','history_process_sewings.workflow_detail_id')
        ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
        ->leftJoin('machines','machines.id','workflow_details.machine_id')
        ->where([
            ['history_process_sewings.line_id',$line_id],
            ['history_process_sewings.sewer_nik',$nik],
            ['history_process_sewings.style',$style],
            ['workflow_details.workflow_id',$workflow_id],
        ])
        ->whereNull('history_process_sewings.delete_at')
        ->whereNull('workflow_details.delete_at')
        ->paginate(10);

        $data = [
            'view'          => View::make('cycle_time.lov._lov_process',compact('listProcess'))->render(),
            'data'          => $listProcess
        ];

        return response()->json($data);
    }
    public function HistoryDayProcess(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $dataBatch = CycleTimeBatch::find($request->batch_id);
            $batchBefore = CycleTimeBatch::whereNull('delete_at')
                            ->where([
                                ['line_id',$dataBatch->line_id],
                                ['style',$dataBatch->style],
                                ['day','<',$dataBatch->day],
                                ['create_user_id',Auth::user()->id],
                            ])
                            ->first();
            $idBatchBefore = $batchBefore?$batchBefore->id:null;
            // dd($idBatchBefore);
            $listProcess = DB::table('report_data_entry_cycle')
            ->leftjoin(
                DB::raw('(select  
                cycle_time_id_before,
                count(*) as count_now,
                total_update
                from cycle_times where cycle_time_batch_id=' . "'$dataBatch->id'" . '
                and total_update!=0
                GROUP BY cycle_time_id_before,total_update)
        cycle_now'),
                function ($leftjoin) {
                    $leftjoin->on('report_data_entry_cycle.id', '=', 'cycle_now.cycle_time_id_before');
                }
            )
            ->where('cycle_time_batch_id',$idBatchBefore)
            ->orderBy(db::raw('COALESCE(cycle_now.count_now,0)'), 'asc')
            ->orderBy('report_data_entry_cycle.date', 'asc')
            ->paginate(10);
            return view('cycle_time.lov._lov_history_process', compact('listProcess'));
        }
    }
    public function ReportDataEntry(Request $request)
    {
        $search           = trim(strtolower($request->q));
        $cycle_time_batch_id = $request->cycle_time_batch_id;
        $workflow_id = $request->workflow_id;
        $listProcess = CycleTime::fetchDataEntry(false,$cycle_time_batch_id,$search);

        return view('cycle_time.lov._lov_lap_data_entry', compact('listProcess'));
    }
    public function listDraft(Request $request)
    {
        $search           = trim(strtolower($request->q));
        $cycle_time_batch_id = $request->cycle_time_batch_id;
        $workflow_id = $request->workflow_id;
        $listProcess = CycleTime::fetchDataEntry(true,$cycle_time_batch_id,$search);

        return view('cycle_time.lov._lov_lap_draft', compact('listProcess'));
    }
    public function detailDraft(Request $request)
    {
        if ($request->ajax()) {
            $cycle_id   = $request->cycle_id;
            $checkCycle = CycleTime::where('id',$cycle_id)->where('is_draft','t')->first();
            if(!$checkCycle) return response()->json(['message' => 'data draft sudah tidak ada.'], 422);            
            $dataDetail = CycleTimeDetail::select(db::raw('id'),'time','time_recorded')
            ->whereNull('delete_at')
            ->where('cycle_time_id',$cycle_id)->get();
            $array = array();
            $time ='';
            foreach ($dataDetail as $key => $d) {
                $obj                = new stdClass();
                if ($time!='') {
                    
                    $time2 = strtotime($d->time);
                    $total_time = date('i:s', $time+$time2);
                    
                    $obj->id = $d->id;
                    $obj->time = $d->time;
                    $obj->total_time = $total_time;
                    $obj->time_recorded = $d->time_recorded;
    
                    
                }else{
                    $obj->id = $d->id;
                    $obj->time = $d->time;
                    $obj->total_time = date('i:s',strtotime($d->time));
                    $obj->time_recorded = $d->time_recorded;
                }
                $array [] = $obj;
               $time=strtotime($d->time);
            }
            return response()->json($array,200);
        }
    }
    public function lockCycle(Request $request)
    {
        $id = $request->cycle_batch_id;
        $data = CycleTimeBatch::findorFail($id);
        $data->end_job = Carbon::now();
        $data->save();
        return redirect()->route('cycle_time.index');
    }
    /* public function reportTimeStudy(Request $request,$id)
    {
        $data = CycleTimeBatch::whereNull('delete_at')
                            ->where('id',$id)
                            ->first();
        $line = $data->line->alias;
        if ($data) {
            return Excel::create($data->style."-L.".$line."-".Carbon::now()->format('u'),function ($excel)use($data,$line,$id){
                $excel->sheet('timestudy', function($sheet)use($data,$line,$id){
                    // dd($sheet);
                    $dataBatch = DB::table('report_time_study')
                    ->where('cycle_time_batch_id',$id)
                    ->orderBy('no_op','desc')
                    ->first();
                    $dataWorkflow = Workflow::whereNull('delete_at')
                    ->where('style',$data->style)
                    ->first();
                    $ts = ($dataWorkflow)?$dataWorkflow->smv:0;
                    if ($dataBatch) {
                        $maxOp       = $dataBatch->no_op;
                        $sheet->setCellValue('A1',"ACTUAL PRODUCTION TIME REPORT");
                        $sheet->setCellValue('C3',"Style :");
                        $sheet->setCellValue('C4',"Line :");
                        $sheet->setCellValue('C5',"Date :");
                        $sheet->setCellValue('C6',"Second :");
                        $sheet->setCellValue('C7',"PPH :");
                        $sheet->setCellValue('C8',"Minute :");
                        $sheet->setCellValue('H3',"Condition Day :");
                        $sheet->setCellValue('H4',"Actual Output :");
                        $sheet->setCellValue('H5',"PIC :");
                        $sheet->setCellValue('H6',"Deferention SMV :");
                        $sheet->setCellValue('H7',"PPH Estimasi :");
                        $sheet->setCellValue('H8',"Target :");
                        $sheet->setCellValue('L3',"TS:");
                        $sheet->setCellValue('L4',"Max TS Process:");
                        $sheet->setCellValue('L5',"Takt Time:");
                        $sheet->setCellValue('L6',"No Of Process:");
                        $sheet->setCellValue('L7',"TS Actual:");
                        $sheet->setCellValue('L8',"Line Balancing Rate:");

                        $sheet->setCellValue('D3',$data->style);
                        $sheet->setCellValue('D4',$line);
                        $sheet->setCellValue('D5',Carbon::createFromFormat('Y-m-d H:i:s',  $data->end_job)->format('j F, Y'));
                        $sheet->setCellValue('I5',ucwords($data->user->name));
                        $sheet->setCellValue('M3',$ts);
                        $sheet->setCellValue('I6','=M3-D8');
                        $sheet->setCellValue('I7','=60/M3');

                        $sheet->cell('A1', function($cell) {
                            $cell->setFontSize(18);
                            $cell->setFontWeight('bold');
                            $cell->setAlignment('center');
                        });
                        for ($i=3; $i < 9; $i++) { 
                            $sheet->cell('C'.$i, function($cell) {
                                $cell->setAlignment('right');
                            });
                            $sheet->cell('H'.$i, function($cell) {
                                $cell->setAlignment('right');
                            });
                            $sheet->cell('L'.$i, function($cell) {
                                $cell->setAlignment('right');
                            });
                        }

                        $sheet->setCellValue('A11','No');
                        $sheet->setCellValue('B11','Machine Name');
                        $sheet->setCellValue('C11','Process Name');
                        $sheet->setCellValue('D11','Target 80%');
                        $sheet->setCellValue('E11','Target 100%');
                        $sheet->setCellValue('F11','Alocated time');
                        $sheet->setCellValue('G11','Harmonic ave');
                        $sheet->setCellValue('H11','TT');
                        $sheet->setCellValue('I11','UCL');
                        $sheet->setCellValue('J11','LCL');
                        $sheet->setCellValue('K11','SMV OB');
                        $current_colum_p      = 'L';
                        $current_colum_p_end  = chr(ord($current_colum_p)+$maxOp);
                        $current_colum_op     = $current_colum_p_end;
                        $current_colum_op_end = chr(ord($current_colum_op)+$maxOp);
                        $sheet->setCellValue($current_colum_op_end.'11','No. Of OP');

                        $no_op      = 1;
                        $no_p       = 1;
                        $currentRow = 13;
                        $no         = 1;
                        for ($i=$current_colum_op; $i <$current_colum_op_end; $i++) {
                            $sheet->setCellValue($i.'11','OP name'.$no_op++);
                        }

                        for ($i=$current_colum_p; $i <$current_colum_p_end; $i++) {                        
                            $sheet->setCellValue($i.'11','Process time'.$no_p++);
                        }

                        foreach(range('A',$current_colum_op_end) as $v){
                            $sheet->cells($v.'11', function($cells) {
                                $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                $cells->setAlignment('center');
                                $cells->setBackground('#ffc014');
                                $cells->setFontColor('#000000');
                            });
                            $sheet->mergeCells($v.'11:'.$v.'12');
                        }
                        $sheet->mergeCells('A1:'.$current_colum_op_end.'1');
                        $dataCycle = DB::table('report_time_study')
                        ->where('cycle_time_batch_id',$id)
                        ->get();
                        foreach ($dataCycle as $key => $d) {
                            $sheet->setCellValue('A'.$currentRow, $no++);
                            $sheet->setCellValue('B'.$currentRow, strtoupper($d->machine_name));
                            $sheet->setCellValue('C'.$currentRow, strtoupper($d->process_name));
                            $operators    = explode(",",$d->operator);
                            $averages     = explode(",",round($d->average,2));
                            $kolom        = "L";
                            $kolom_p_end  = chr(ord($kolom)+$maxOp);
                            $kolom_op_end = chr(ord($kolom_p_end)+$maxOp);
                            foreach ($averages as $av) {
                                $sheet->setCellValue($kolom.$currentRow, $av);
                                $kolom++;
                            }
                            foreach ($operators as $op) {
                                $sheet->setCellValue($kolom_p_end.$currentRow, strtoupper($op));
                                $kolom_p_end++;
                            }
                            $sheet->setCellValue($kolom_op_end.$currentRow, $d->no_op);
                            $sheet->setCellValue('G'.$currentRow, '=ROUND(HARMEAN(L'.$currentRow.':'.(chr(ord('L')+$d->no_op-1)).$currentRow.');2)');
                            $sheet->setCellValue('H'.$currentRow, '=M5');
                            $sheet->setCellValue('F'.$currentRow, '=G'.$currentRow.'/'.$kolom_op_end.$currentRow);
                            $sheet->setCellValue('E'.$currentRow, '=ROUNDUP((3600/F'.$currentRow.'),0)');
                            $sheet->setCellValue('D'.$currentRow, '=E'.$currentRow.'*0.8');
                            $sheet->setColumnFormat(
                                array(
                                    'D'.$currentRow=>'0',
                                    'E'.$currentRow=>'0',
                                    'F'.$currentRow=>'0.0',
                                    'G'.$currentRow=>'0.0',
                                )
                            );
                            foreach(range('A',$kolom_op_end) as $v){
                                $sheet->cells($v.$currentRow, function($cells) {
                                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                            }
                            $currentRow++;
                        }
                        $sheet->setCellValue('I4', '=MIN(D13:D'.($currentRow-1).')');
                        $sheet->setCellValue('C'.($currentRow), '=COUNTA(C13:C'.($currentRow-1).')');
                        $sheet->setCellValue('D'.$currentRow,'Total TS');
                        $sheet->setCellValue('D'.($currentRow+1),'Max TS Process');
                        $sheet->setCellValue('F'.$currentRow, '=SUM(F13:F'.($currentRow-1).')');
                        $sheet->setCellValue('F'.($currentRow+1),'=MAX(F13:F'.($currentRow-1).')');
                        $sheet->setCellValue('G'.$currentRow,'=SUM(G13:G'.($currentRow-1).')');
                        $sheet->setCellValue('G'.($currentRow+1),'=MAX(G13:G'.($currentRow-1).')');
                        $sheet->setCellValue('D6', '=SUM(G13:G'.($currentRow-1).')');
                        $sheet->setCellValue('D7', '=3600/D6');
                        $sheet->setCellValue('D8', '=60/D7');
                        $sheet->setCellValue('M4','=MAX(F13:F'.($currentRow-1).')');
                        $sheet->setCellValue('M6','=COUNTA(C13:C'.($currentRow-1).')');
                        $sheet->setCellValue('M5', '=(M3/M6)*60');
                        $sheet->setCellValue('M7', '=SUM(F13:F'.($currentRow-1).')');
                        $sheet->setCellValue('M8', '=M7/(M6*M4)');
                        $sheet->cells('C'.($currentRow), function($cells) {
                            $cells->setBorder('thin', 'thin', 'thin', 'thin');
                            $cells->setAlignment('center');
                            $cells->setFontSize(18);
                            $cells->setFontWeight('bold');
                            $cells->setAlignment('center');
                            $cells->setBackground('#4ca64c');
                        });
                        $sheet->cells('D'.($currentRow), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setFontSize(18);
                            $cells->setFontWeight('bold');
                            $cells->setAlignment('center');
                        });
                        $sheet->cells('G'.($currentRow), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setFontWeight('bold');
                            $cells->setAlignment('center');
                        });
                        $sheet->mergeCells('D'.$currentRow.':E'.$currentRow);
                        $sheet->mergeCells('D'.($currentRow+1).':E'.($currentRow+1));
                        $sheet->setColumnFormat(
                            array(
                                'D6'=>'0.0',
                                'D7'=>'0.0',
                                'I7'=>'0.0',
                                'I6'=>'0.0',
                                'D8'=>'0.0',
                                'M7'=>'0.0',
                                'M8'=>'0.0',
                                'F'.$currentRow=>'0.0',
                                'F'.($currentRow+1)=>'0.0',
                                'G'.$currentRow=>'0.0',
                                'G'.($currentRow+1)=>'0.0',
                            )
                        );
                        
                    }
                });
            })->export('xlsx');
        }
    } */

    public function reportTimeStudyChart(Request $request,$id){
        $data = CycleTimeBatch::whereNull('delete_at')
                            ->where('id',$id)
                            ->first();
        $line = $data->line->alias;
        if ($data) {
            $excel = new \PHPExcel();

            $dataBatch     = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY no_op desc limit 1"));
            $dataBreakdown = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY count_duration desc limit 1"));
            if ($dataBatch) {
                $dataWorkflow = Workflow::whereNull('delete_at')
                    ->where('style',$data->style)
                    ->first();
                    
                $ts = ($dataWorkflow)?$dataWorkflow->smv:0;

                $excel->getProperties()->setCreator('ICT Developer')
                ->setLastModifiedBy('ICT Developer')
                ->setTitle($data->style."-L.".$line."-".Carbon::now()->format('u'))
                ->setSubject($data->style."-L.".$line)
                ->setDescription($data->style."-L.".$line)
                ->setKeywords($data->style."-L.".$line);
                
                $sheet = $excel->setActiveSheetIndex(0);
                $excel->getActiveSheet(0)->setTitle("timestudy");

                $style_col = array(
                    'font' => array('bold' => true),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'top'    => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'right'  => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'bottom' => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'left'   => array('style'  => \PHPExcel_Style_Border::BORDER_THIN)
                    )
                );
        
                $style_row = array(
                    'alignment' => array(
                        'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ),
                    'borders' => array(
                        'top'    => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'right'  => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'bottom' => array('style'  => \PHPExcel_Style_Border::BORDER_THIN),
                        'left'   => array('style'  => \PHPExcel_Style_Border::BORDER_THIN)
                    )
                );

                $maxOp                    = $dataBatch[0]->no_op;
                $countBreakdown           = $dataBreakdown[0]->count_duration;
                $current_colum_p          = 'L';
                $current_colum_p_end      = chr(ord($current_colum_p)+$maxOp);
                $current_colum_op         = $current_colum_p_end;
                $current_colum_op_end     = chr(ord($current_colum_op)+$maxOp);
                $current_column_breakdown = self::getNameFromNumber(self::alpha2num($current_colum_op_end));
                

                $sheet->setCellValue('A1',"ACTUAL PRODUCTION TIME REPORT");
                $sheet->mergeCells('A1:'.$current_colum_op_end.'1');
                $sheet->mergeCells('A1:Q1');
                $sheet->getStyle('A1')->getFont()->setBold(TRUE);
                $sheet->getStyle('A1')->getFont()->setSize(18);
                $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                for ($i=3; $i <= 9; $i++) { 
                    $sheet->getStyle('C'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('H'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('L'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $sheet->getStyle('D'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $sheet->getStyle('I'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $sheet->getStyle('M'.$i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                }

                $no_op      = 1;
                $no_op2      = 1;
                $no_p       = 1;
                $currentRow = 13;
                $no         = 1;
                for ($i=$current_colum_op; $i <$current_colum_op_end; $i++) {
                    $sheet->setCellValue($i.'11','OP name'.$no_op++);
                }

                for ($i=$current_colum_p; $i <$current_colum_p_end; $i++) {                        
                    $sheet->setCellValue($i.'11','Process time'.$no_p++);
                }
                foreach(self::excelColumnRange('A',$current_colum_op_end) as $v){
                    $sheet->mergeCells($v.'11:'.$v.'12');
                    $sheet->getColumnDimension($v)->setAutoSize(true);
                    $sheet->getStyle($v.'11')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle($v.'12')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                        )
                    );
                
                }
                $sheet->setCellValue('C3',"Style :");
                $sheet->setCellValue('C4',"Line :");
                $sheet->setCellValue('C5',"Date :");
                $sheet->setCellValue('C6',"Second :");
                $sheet->setCellValue('C7',"PPH :");
                $sheet->setCellValue('C8',"Minute :");
                $sheet->setCellValue('H3',"Condition Day :");
                $sheet->setCellValue('H4',"Actual Output :");
                $sheet->setCellValue('H5',"PIC :");
                $sheet->setCellValue('H6',"Deferention SMV :");
                $sheet->setCellValue('H7',"PPH Estimasi :");
                $sheet->setCellValue('H8',"Target :");
                $sheet->setCellValue('L3',"TS:");
                $sheet->setCellValue('L4',"Max TS Process:");
                $sheet->setCellValue('L5',"Takt Time:");
                $sheet->setCellValue('L6',"No Of Process:");
                $sheet->setCellValue('L7',"TS Actual:");
                $sheet->setCellValue('L8',"Line Balancing Rate:");
                $sheet->setCellValue('D3',$data->style);
                $sheet->setCellValue('D4',$line);
                $sheet->setCellValue('D5',Carbon::createFromFormat('Y-m-d H:i:s',  $data->end_job)->format('j F, Y'));
                $sheet->setCellValue('I3',$data->day);
                $sheet->setCellValue('I5',ucwords($data->user->name));
                $sheet->setCellValue('M3',$ts);

                $sheet->setCellValue('A11','No');
                $sheet->setCellValue('B11','Machine Name');
                $sheet->setCellValue('C11','Process Name');
                $sheet->setCellValue('D11','Target 80%');
                $sheet->setCellValue('E11','Target 100%');
                $sheet->setCellValue('F11','Alocated time');
                $sheet->setCellValue('G11','Harmonic ave');
                $sheet->setCellValue('H11','TT');
                $sheet->setCellValue('I11','UCL');
                $sheet->setCellValue('J11','LCL');
                $sheet->setCellValue('K11','SMV OB');
                $sheet->setCellValue($current_colum_op_end.'11','No. Of OP');
                $column    = self::alpha2num($current_colum_op_end);
                $column2   = self::alpha2num($current_colum_op_end);
                
                $no        = 1;
                for ($i=1; $i <= $maxOp ; $i++) {
                    $sheet->setCellValue(self::getNameFromNumber($column).'11','Pick Time OP '.$no_op2++);
                    $sheet->mergeCells(self::getNameFromNumber($column).'11:'.self::getNameFromNumber(($column+$countBreakdown)-1).'11');
                    
                    $sheet->getStyle(self::getNameFromNumber($column).'11:'.self::getNameFromNumber(($column+$countBreakdown)-1).'11')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle(self::getNameFromNumber($column).'11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $column+=$countBreakdown;
                }
                $maxCycle  = $countBreakdown*$maxOp;
                $columnMax = self::getNameFromNumber(self::alpha2num($current_colum_op_end)+$maxCycle-1);
                $no        = 1;
                for ($k=1; $k <=$maxCycle ; $k++) { 
                    $sheet->setCellValue(self::getNameFromNumber($column2).'12',$no++);
                    if ($no>$countBreakdown) {
                        $no=1;
                    }
                    $sheet->getStyle(self::getNameFromNumber($column2).'12')->applyFromArray(
                        array(
                            'borders' => array(
                                'allborders' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                                )
                                ),
                            'fill' => array(
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'FFC014')
                            )
                        )
                    );
                    $sheet->getStyle($column2.'12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $column2++;
                }

                $dataCycle = DB::select(db::Raw("select * from f_report_time_study('$id') ORDER BY created_at"));
                $noUrut =1;
                foreach ($dataCycle as $key => $d) {
                    $sheet->setCellValue('A'.$currentRow, $noUrut++);
                    $sheet->setCellValue('B'.$currentRow, strtoupper($d->machine_name));
                    $sheet->setCellValue('C'.$currentRow, strtoupper($d->process_name));
                    $sheet->setCellValue('K'.$currentRow, $d->smv);
                    $operators       = explode(",",$d->operator);
                    $averages        = explode(",",$d->average);
                    $kolom           = "L";
                    $kolom_p_end     = chr(ord($kolom)+$maxOp);
                    $kolom_op_end    = chr(ord($kolom_p_end)+$maxOp);
                    $column          = self::alpha2num($kolom_op_end);
                    $kolom_breakdown = self::getNameFromNumber(self::alpha2num($kolom_op_end));
                    
                    foreach ($averages as $av) {
                        $sheet->setCellValue($kolom.$currentRow, round($av,2));
                        $kolom++;
                    }
                    foreach ($operators as $op) {
                        $sheet->setCellValue($kolom_p_end.$currentRow, strtoupper($op));
                        $kolom_p_end++;
                    }
                    $durations = json_decode($d->duration);
                    $maxCycle  = $countBreakdown;
                    $no        = 1;
                    $next      = 1;
                    foreach ($durations as $duration) {
                        foreach (json_decode($duration) as $value) {
                            $drt           = explode(",",$value);
                            $countDuration = count($drt);
                            foreach ($drt as $drt) {
                                $sheet->setCellValue(self::getNameFromNumber($column).$currentRow, $drt);
                                if ($no==$countDuration) {
                                    $next=$maxCycle-$countDuration;
                                    $column = self::alpha2num(self::getNameFromNumber(($column+$next)-1));
                                    $no     = 1;
                                    $next   = 1;
                                }else{

                                    $no++;
                                }
                                
                                $column++;
                            }
                        }
                    }
                    // die();
                    $sheet->setCellValue($kolom_op_end.$currentRow, $d->no_op);
                    $sheet->setCellValue('G'.$currentRow, '=ROUND(HARMEAN(L'.$currentRow.':'.(chr(ord('L')+$d->no_op-1)).$currentRow.'),2)');
                    $sheet->setCellValue('H'.$currentRow, '=M5');
                    $sheet->setCellValue('F'.$currentRow, '=ROUND(G'.$currentRow.'/'.$kolom_op_end.$currentRow.',2)');
                    $sheet->setCellValue('E'.$currentRow, '=ROUNDUP((3600/F'.$currentRow.'),0)');
                    $sheet->setCellValue('D'.$currentRow, '=E'.$currentRow.'*0.8');
                    
                    // $lRow = 13;
                    $lRow=13;
                    foreach(self::excelColumnRange('A', $columnMax) as $v){
                        if ($d->is_copied==false&&$d->total_update!=0) {
                            $sheet->getStyle($v.$currentRow)->applyFromArray(
                                array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    ),
                                    'fill' => array(
                                        'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '14FF24')
                                    )
                                )
                            );
                        }else{
                            $sheet->getStyle($v.$currentRow)->applyFromArray(
                                array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => \PHPExcel_Style_Border::BORDER_THIN
                                        )
                                    )
                                )
                            );
                        }
                        $sheet->getColumnDimension($v)->setAutoSize(true);
                        // $sheet->getStyle($v.$currentRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                    $currentRow++;
                }
                $sheet->setCellValue('I4', '=MIN(D13:D'.($currentRow-1).')');
                $sheet->setCellValue('D6', '=SUM(G13:G'.($currentRow-1).')');
                $sheet->setCellValue('D7', '=ROUND(3600/D6,2)');
                $sheet->setCellValue('D8', '=ROUND(60/D7,2)');
                $sheet->setCellValue('M4','=MAX(F13:F'.($currentRow-1).')');
                $sheet->setCellValue('M6','=COUNTA(C13:C'.($currentRow-1).')');
                $sheet->setCellValue('M5', '=ROUND((M3/M6)*60,2)');
                $sheet->setCellValue('M7', '=ROUND(SUM(F13:F'.($currentRow-1).'),2)');
                $sheet->setCellValue('M8', '=ROUND(M7/(M6*M4),2)');

                $dataseriesLabels1  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$F$11', NULL, 1),
                    // 'Allocated'
                    // new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$H$11', NULL, 1),
                );
                $dataseriesLabels2  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$H$11', NULL, 1),
                );
        
                $xAxisTickValues  = array(
                    new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$C$13:$C$' . ($currentRow-1), NULL, ($currentRow-1)),
                    // new \PHPExcel_Chart_DataSeriesValues('String', 'timestudy!$C$13:$C$' . ($currentRow-1), NULL, ($currentRow-1)),
                );

                $dataSeriesValues1  = array(
                    new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$F$13:$F$' . ($currentRow-1), NULL, ($currentRow-1)),
                    // new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$H$13:$H$' . ($currentRow-1), NULL, ($currentRow-1))
                );
                $dataSeriesValues2  = array(
                    // new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$F$13:$F$' . ($currentRow-1), NULL, ($currentRow-1)),
                    new \PHPExcel_Chart_DataSeriesValues('Number', 'timestudy!$H$13:$H$' . ($currentRow-1), NULL, ($currentRow-1))
                );
                
                $series1 = new \PHPExcel_Chart_DataSeries(
                    \PHPExcel_Chart_DataSeries::TYPE_BARCHART,
                    \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,
                    range(0, count($dataSeriesValues1) - 1),
                    $dataseriesLabels1 ,
                    $xAxisTickValues ,
                    $dataSeriesValues1 
                );
                $series1->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);
                
                $series2 = new \PHPExcel_Chart_DataSeries(
                    \PHPExcel_Chart_DataSeries::TYPE_LINECHART, // plotType
                    \PHPExcel_Chart_DataSeries::GROUPING_STANDARD, // plotGrouping
                    range(0, count($dataSeriesValues2) - 1), // plotOrder
                    $dataseriesLabels2, // plotLabel
                    $xAxisTickValues, // plotCategory
                    $dataSeriesValues2                              // plotValues
                );
                // dd($series2);
                $title  = new \PHPExcel_Chart_Title('VISUAL KEMAMPUAN LINE (AFTER)');
                $plotarea = new \PHPExcel_Chart_PlotArea(NULL, array($series1, $series2));
                $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);
                $chart  = new \PHPExcel_Chart(
                    'timestudy',
                    $title,
                    $legend,
                    $plotarea,
                    true,
                    0,
                    NULL, 
                    NULL
                );
                $chart->setTopLeftPosition("A".($currentRow+5));
                $chart->setBottomRightPosition($columnMax.($currentRow+17));

                $excel->getActiveSheet()->addChart($chart);
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="'.$data->style."-L.".$line."-".Carbon::now()->format('u').'.xlsx"'); // Set nama file excel nya
                header('Cache-Control: max-age=0');
                $write = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $write->setIncludeCharts(TRUE);
                $write->save('php://output');
            }
        }else{
            return response()->json('tidak ada data');
        }
        
    }

    static function updateCycleWorkflowDetail()
    {
        $data = CycleTime::whereNotNull('workflow_detail_id')
        ->get();
        foreach ($data as $key => $d) {
            CycleWorkflowDetail::Create([
                'cycle_time_id'      => $d->id,
                'workflow_detail_id' => $d->workflow_detail_id
            ]);
        }
    }
    public function destroyCycleTime(Request $request,$id)
    {
        if ($request->ajax()) {
            $cycle = CycleTime::find($id);
            $cycle->delete_at = Carbon::now();
            $cycle->save();
        }
    }
    public function sendMessage(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $workflow           = Workflow::find($request->workflowId);
            $cycleTimeBatch     = CycleTimeBatch::find($request->cycle_batch_id);
            $nik                = $cycleTimeBatch->user->nik;
            $name               = $cycleTimeBatch->user->name;
            $style              = $workflow->style;
            $line_id            = $cycleTimeBatch->line_id;
            $countComplainClose = $this->getComplainClose($line_id,$nik)->count();
            $totComplain        = ResponseWorkflow::whereNull('delete_at')
            ->where('line_id',$line_id)
            ->where('create_qc_nik',$nik)
            ->where('status','open')->get()->count();
            return view('cycle_time.lov._lov_send_message', compact('style','line_id','totComplain','countComplainClose','name','nik'));
        }
    }
    private function getComplainClose($line_id,$nik)
    {
        $data = ResponseWorkflow::select('response_workflow_details.message','response_workflow_details.id')
        ->leftJoin('response_workflow_details','response_workflow_details.response_workflow_id','response_workflows.id')
        ->whereNull('response_workflows.delete_at')
        ->whereNull('response_workflow_details.delete_at')
        ->where([
            ['response_workflows.line_id',$line_id],
            ['response_workflows.create_qc_nik',$nik],
            ['response_workflow_details.is_read_qc','f'],
        ])->get();
        return $data;
    }
    public function storeMessage(Request $request)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';
            
            $line_id   = $request->line_id;
            $line      = Line::find($line_id);
            $style     = $request->style;
            $message   = $request->message;
            $noLaporan = Carbon::now()->format('u');
            $qc_name   = $request->name;
            $qc_nik    = $request->name;

            $workflows = Workflow::whereNull('delete_at')
            ->where('style',$style)->first();
            
            if ($workflows) {
                $workflow_id = $workflows->id;
            }else{
                $workflow_id = null;
            }

            ResponseWorkflow::create([
                'line_id'        => $line_id,
                'workflow_id'    => $workflow_id,
                'nomor'          => $noLaporan,
                'style'          => $style,
                'message'        => $message,
                'create_qc_nik'  => $qc_nik,
                'create_qc_name' => $qc_name,
                'status'         => 'open',
            ]);
            DB::connection($connection_mid)
            ->table('whatsapp')
            ->insert([
                'contact_number' => 'Digitalisasi P. Akurasi',
                'is_group' => 't',
                'message' => '*Mohon Bantuannya*|No. Lapor : '.$noLaporan.'|Line :'.$line->alias.'|Style : '.$style.'|Nama IE APT  :'.ucwords($qc_name).'|Pesan :'.ucwords($message).'|Terimakasih.'
            ]);
        }
    }
    static function updateAverage()
    {
        $data = DB::table('report_data_entry_cycle');

        foreach ($data->get() as $key => $d) {
            
            CycleTime::where('id', $d->id)->update(array('average' => $d->average));
            
        }
    }
    public function storeCycleHistory(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $this->validate($request, [
                'pick_time_data'      => 'required',
                'cycle_time_batch_id' => 'required',
                'sewer_nik'           => 'required',
                'sewer_name'          => 'required',
            ]);
            // dd($request->all());
            $itemCycle           = json_decode($request->pick_time_data);
            $pickProses          = json_decode($request->process_pick_list);
            $cycle_time_batch_id = $request->cycle_time_batch_id;
            $cycle_time_id       = $request->cycle_time_id;
            $sewer_nik           = $request->sewer_nik;
            $sewer_name          = $request->sewer_name;
            $wip                 = $request->wip;
            // $created_at          = (!$params)?Carbon::now():null;
            $checkLock           = CycleTimeBatch::find($cycle_time_batch_id);
            if ($checkLock->end_job!=null) {
                return response()->json(['message' => 'Data Sudah di Lock, Cycle time tidak bisa di simpan'], 422);
            }
            $arrayWorkflowDetail = array();
            foreach ($pickProses as $key => $proses) {
                $arrayWorkflowDetail[] = $proses->workflow_detail_id;
            }

            $existCycle = CycleTime::whereNull('cycle_times.delete_at')
                        ->select('cycle_times.id')
                        ->leftjoin('cycle_workflow_details','cycle_workflow_details.cycle_time_id','cycle_times.id')
                        ->where([
                            ['cycle_times.cycle_time_id_before',$cycle_time_id],
                        ])->first();

            if (!$existCycle) {
                return response()->json(['message' => 'ada kesalahan, silahkan refresh halaman anda'], 422);
            }

            try {
                DB::beginTransaction();
                
                $existCycle->created_at   = Carbon::now();
                $existCycle->updated_at   = Carbon::now();
                $existCycle->total_update = $existCycle->total_update+1;
                $existCycle->is_copied    = false;
                $existCycle->save();

                foreach ($pickProses as $key => $proses) {
                    $cekWorkflow = CycleWorkflowDetail::whereNull('delete_at')
                                    ->where([
                                        ['cycle_time_id',$existCycle->id],
                                        ['workflow_detail_id',$proses->workflow_detail_id],
                                    ])->first();
                    if (!$cekWorkflow) {
                        CycleWorkflowDetail::Create([
                            'cycle_time_id'      => $existCycle->id,
                            'workflow_detail_id' => $proses->workflow_detail_id
                        ]);
                    }
                    
                }
                $sum   = 0;
                $count = 0;
                CycleTimeDetail::where('cycle_time_id',$existCycle->id)->delete();
                foreach ($itemCycle as $key => $item) {
                    $time    = $this->parts($item->interval);
                    $duration = $time[0] * 60 + $time[1]+($time[2]*0.001);
                    CycleTimeDetail::Create([
                        'cycle_time_id' => $existCycle->id,
                        'time'          => '00:'.$time[0].':'.$time[1],
                        'time_recorded' => $item->time_recorded,
                        'duration'      => $duration,
                    ]);
                    $sum+=$duration;
                    $count++;
                }
                $CycleDetail = CycleTimeDetail::select(db::raw('sum(duration) as duration'),db::raw('count(*) as total'))
                ->where('cycle_time_id',$existCycle->id)->first();
                $average = ($CycleDetail)?$CycleDetail->duration/$CycleDetail->total:0;
                CycleTime::where('id', $existCycle->id)->update(array('average' => $average));
                DB::commit();
                return response()->json(['success'=>'success'],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function getComponent(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $q           = trim(strtolower($request->q));
            $lists = WorkflowDetail::select('process_sewings.component')
            ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
            ->whereNull('workflow_details.delete_at')
            ->where([
                ['workflow_details.workflow_id',$request->workflowId],
                ['process_sewings.component', 'LIKE', "%$q%"],
            ])
            ->groupBy('process_sewings.component')
            ->paginate(10); 
            return view('cycle_time.lov._lov_component_picklist', compact('lists'));
        }
    }
    static function getNameFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return self::getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
    static function excelColumnRange($lower, $upper) {
        ++$upper;
        for ($i = $lower; $i !== $upper; ++$i) {
            yield $i;
        }
    }
    static function alpha2num($column) {
        $number = 0;
        foreach(str_split($column) as $letter){
            $number = ($number * 26) + (ord(strtolower($letter)) - 96);
        }
        return $number;
    }
}