<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\Production;
use App\Models\ProductionSizeHistory;

class ReportAdmSewingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('report_adm_sewing.index');
    }
    public function data(Request $request)
    {
        if ($request->ajax()) {
            $start_date  = ($request->start_date)?Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d'):null;
            $end_date    = ($request->end_date)?Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d'):null;
            $poreference = $request->poreference;
            $size        = $request->size;
            $style       = null;
            $article     = null;
            $buyer       = $this->Buyer();
            $marks       = array_fill(0, sizeof($buyer), '?');
            if ($poreference) {
                list($poreference,$style,$article)= explode("|",$request->poreference);
            }
            if ($start_date==null&&$end_date==null&&$request->poreference==null) {
                $date = Carbon::now()->format('Y-m-d');
                $data    = DB::select(db::Raw("select * from f_wip('$date','$date') where buyer in(" . implode(',', $marks) . ")"),$buyer);
            } else if($request->poreference!=''&&$start_date==null&&$end_date==null){
                $data    = DB::select(db::Raw("select * from f_wip_style('$style','$poreference','$article')where size like '$request->size%' and buyer in(" . implode(',', $marks) . ")"),$buyer);
            }else if($start_date!=null&&$end_date!=null&&$poreference!=''){
                $data    = DB::select(db::Raw("select * from f_wip('$start_date','$end_date') where style='$style' and style='$poreference' and style='$article' and size like '$request->size%' and buyer in(" . implode(',', $marks) . ")"),$buyer);
            }else if($request->date!=null||$request->poreference==null){
                $data    = DB::select(db::Raw("select * from f_wip('$start_date','$end_date')where buyer in(" . implode(',', $marks) . ")"),$buyer);
            }
            
            return datatables()->of($data)

            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('line_name',function($data){
                return strtoupper($data->line_name);
            })
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('total_qty_loading',function($data){
                $total_loading = $this->getTotalLoading($data->production_size_id,$data->date);
                return $total_loading;
            })
            ->editColumn('qty_loading',function($data){
                return $data->qty_input_loading;
            })
            ->editColumn('total_qc_output',function($data){
                return $data->total_counter;
            })
            ->editColumn('qc_output',function($data){
                return $data->output_qc;
            })
            ->editColumn('balance',function($data){
                $total_loading = $this->getTotalLoading($data->production_size_id,$data->date);
                $balance = $total_loading - $data->total_counter;
                return $balance;
            })
            ->make(true);
        }
    }
    private function getTotalLoading($production_size_id,$date)
    {
        $data = ProductionSizeHistory::where('production_size_id',$production_size_id)
        ->where(db::raw('date(created_at)'),'<=',$date)
        ->orderBy('created_at','desc')->first();
        
        return ($data)?$data->qty_new:0;
    }
    private function getOutPutQcDaily($production_id,$production_size_id,$date)
    {
        $data = DB::select(db::Raw("select * from qc_history_output_v where production_id='$production_id' and production_size_id='$production_size_id' and date<='$date' order by date desc limit 1"));
        
        return ($data==array())?0:$data[0]->total_counter;
    }
    public function poBuyerPicklist(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtoupper($request->q));
            $buyer = $this->Buyer();
            $data  = Production::select('productions.factory_id','lines.buyer','productions.style','productions.poreference','productions.article')
            ->leftJoin('lines','lines.id','productions.line_id')
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->where(function($query) use ($q){
                $query->where(db::raw('upper(poreference)'),'LIKE',"%$q%")
                ->orWhere(db::raw("upper(article)"),'LIKE',"%$q%")
                ->orWhere(db::raw('upper(style)'),'LIKE',"%$q%");
            })
            ->whereIn('lines.buyer',$buyer)
            ->groupBy('lines.buyer','productions.factory_id','productions.style','productions.poreference','productions.article')
            ->paginate(10);
            return view('report_qc_endline_output._poreference_picklist', compact('data'));
        }
    }
    static function getSize(Request $request)
    {
        if ($request->ajax()) {
            list($poreference,$style,$article)= explode("|",$request->poreference);

            $size = Production::select('production_sizes.size')
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->where('productions.poreference',$poreference)
            ->where('productions.style',$style)
            ->where('productions.article',$article)
            ->where('productions.factory_id',Auth::user()->factory_id)
            ->pluck('production_sizes.size','production_sizes.size');
            return response()->json(['size' => $size]);
        }
    }
    public function exportFormAdminSewing(Request $request)
    {
        $start_date  = ($request->start_date)?Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d'):null;
        $end_date    = ($request->end_date)?Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d'):null;
        $poreference = $request->poreference;
        $size        = $request->size;
        $style       = null;
        $article     = null;
        $buyer       = $this->Buyer();
        $marks       = array_fill(0, sizeof($buyer), '?');
        if ($poreference) {
            list($poreference,$style,$article)= explode("|",$request->poreference);
        }
        if ($start_date==null&&$end_date==null&&$request->poreference==null) {
            $date = Carbon::now()->format('Y-m-d');
            $data    = DB::select(db::Raw("select * from f_wip('$date','$date') where buyer in(" . implode(',', $marks) . ")"),$buyer);
        } else if($request->poreference!=''&&$start_date==null&&$end_date==null){
            $data    = DB::select(db::Raw("select * from f_wip_style('$style','$poreference','$article')where size like '$request->size%' and buyer in(" . implode(',', $marks) . ")"),$buyer);
        }else if($start_date!=null&&$end_date!=null&&$poreference!=''){
            $data    = DB::select(db::Raw("select * from f_wip('$date','$date') where style='$style' and style='$poreference' and style='$article' and size like '$request->size%' and buyer in(" . implode(',', $marks) . ")"),$buyer);
        }else if($request->date!=null||$request->poreference==null){
            $data    = DB::select(db::Raw("select * from f_wip('$start_date','$end_date')where buyer in(" . implode(',', $marks) . ")"),$buyer);
        }
        return Excel::create('Report Adm Sewing-'.$start_date."_".Carbon::now()->format('u'),function ($excel)use($data){
            $excel->sheet('active', function($sheet) use($data){
                
                $sheet->setCellValue('A1','LINE NAME');
                $sheet->setCellValue('B1','TANGGAL');
                $sheet->setCellValue('C1','STYLE');
                $sheet->setCellValue('D1','PO BUYER');
                $sheet->setCellValue('E1','ARTICLE');
                $sheet->setCellValue('F1','SIZE');
                $sheet->setCellValue('G1','TOTAL QTY LOADING');
                $sheet->setCellValue('H1','QTY LOADING');
                $sheet->setCellValue('I1','TOTAL QC OUTPUT');
                $sheet->setCellValue('J1','QC OUTPUT');
                $sheet->setCellValue('K1','WIP SEWING');
                $index = 0;
                $array = array();
                $row   = 1;
                foreach ($data as $key => $a) {
                    $totalLoading = $this->getTotalLoading($a->production_size_id,$a->date);
                    // $output_qc = $this->getOutPutQcDaily($a->production_id,$a->production_size_id,$a->date);
                    $balance = $totalLoading-$a->total_counter;
                    $row++;
                    $sheet->setCellValue('A'.$row, strtoupper($a->line_name));
                    $sheet->setCellValue('B'.$row, $a->date);
                    $sheet->setCellValue('C'.$row, $a->style);
                    $sheet->setCellValue('D'.$row, $a->poreference);
                    $sheet->setCellValue('E'.$row, $a->article); 
                    $sheet->setCellValue('F'.$row, $a->size); 
                    $sheet->setCellValue('G'.$row, $totalLoading); 
                    $sheet->setCellValue('H'.$row, $a->qty_input_loading);
                    $sheet->setCellValue('I'.$row, $a->total_counter);
                    $sheet->setCellValue('J'.$row, $a->output_qc);
                    $sheet->setCellValue('K'.$row, $balance);
                    $index++;
                }

                $sheet->setColumnFormat(array(
                    'B' => 'yyyy-mm-dd',
                    'C' => '@',
                    'D' => '@',
                    'F' => '@',
                    'G' => '0',
                    'H' => '0',
                    'I' => '0',
                    'J' => '0',
                    'K' => '0',
                ));             
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }
    private function Buyer()
    {
        if (Auth::user()->production=='other') {
            $data = array('other');
        } else if(Auth::user()->production=='nagai') {
            $data = array('nagai'); 
        }else{
            $data = ['other','nagai'];
        }
        return $data;
    }
}
