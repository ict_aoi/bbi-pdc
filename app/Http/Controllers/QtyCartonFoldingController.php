<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\QtyCarton;
use App\Models\BreakDownSize;

class QtyCartonFoldingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('qty_carton.index');
    }
    public function data()
    {
    	if(request()->ajax())
        {

            $data = QtyCarton::select('style')
            ->groupBy('style');
            
            return datatables()->of($data)
            ->editColumn('style',function($data){
                return $data->style;
            })
            
            ->addColumn('action', function($data) {
                return view('qty_carton._action', [
                    'model'  => $data,
                    'edit'   => route('qty_carton.edit',$data->style),
                    'delete' => route('qty_carton.destroy',$data->style)
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function create(Request $request)
    {
        return view('qty_carton.create');
    }
    public function stylePicklist(Request $request)
    {
        if ($request->ajax()) {
            $q          = trim(strtolower($request->q));
       
            $lists      = BreakDownSize::select('style',db::raw("max(dateorder) as dateorder"),db::raw("json_agg(ROW(size)) as size"))
            ->where(function($query) use ($q){
                $query->where(db::raw('lower(style)'),'LIKE',"%$q%");
            })
            ->groupby('style')
            ->orderBy('dateorder','desc')
            ->paginate(10);
            
            return view('qty_carton.lov._lov_style_picklist',compact('lists'));
        }
    }
    public function storeQtyCarton(Request $request)
    {
        if ($request->ajax()) {
            $style = $request->style;
            $data  = json_decode($request->breakdown_size_data);
            try {
                DB::beginTransaction();
                foreach ($data as $key => $item) {
                    if ($item->id=='-1') {
                        $check = QtyCarton::where([
                            ['style',$style],
                            ['size',$item->size],
                        ])->exists();
                        
                        if (!$check) {
                            QtyCarton::Create([
                                'style'          => $style,
                                'size'           => $item->size,
                                'qty'            => $item->qty,
                                'create_user_id' => Auth::user()->id,
                            ]);
                        }
                    }else{
                        QtyCarton::where('id',$item->id)
                        ->update(array('qty' => $item->qty));
                    }
                }
                DB::commit();
                return response()->json(['success'=>'success'],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function edit(Request $request,$style)
    {
        $detail = QtyCarton::select('id','style','size','qty')
        ->where('style',$style)->get();
        return view('qty_carton.edit',compact('detail','style'));
    }
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $qtyCarton = QtyCarton::find($id);
            $qtyCarton->delete();
            return response()->json(['success'=>'success'],200);
        }
    }
}