<?php namespace App\Http\Controllers;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\Line;
use App\Models\Style;
use App\Models\Sewing;
use App\Models\SewingDetail;
use App\Models\HistoryChangeStyle;


class SewingController extends Controller
{
    public function index(Request $request,$factory,$line)
    {
        $style = null;
        $sewing_id = null;
        $style_default = null;
        $master_line = Line::where('code', $line)
        ->whereHas('factory', function ($query) use ($factory) {
            $query->where('code', $factory);
        })
        ->whereNull('delete_at')
        ->first();
        $sewing      = Sewing::where('status','active')
        ->where('line_id',$master_line->id)
        ->first();
        if ($sewing) {
            $changeStyle = HistoryChangeStyle::leftJoin('styles','styles.id','history_change_styles.style_id')
            ->where('styles.erp',$sewing->style);
            if($changeStyle->count()>0){
                $sewingCounter = Sewing::where('style',$changeStyle->first()->default)
                ->where('line_id',$master_line->id)->first();
                // $sewing_id = $sewingCounter->id;    
                if($sewingCounter){
                    if($sewing->style!=$sewingCounter->style){
                        DB::table('sewings')
                        ->where('id',$sewing->id)
                        ->update(['status' => null]);
                        $sewingCounter->status = 'active';
                        $sewingCounter->save();
                    }
                    $style = $changeStyle->first()->default;
                    $sewing_id = $changeStyle->first()->id;
                }else{
                    $sewing_id = $this->InsertSewing($master_line->id,$master_line->factory_id,$changeStyle->first()->default,$sewing->article);
                    $style = $changeStyle->first()->default;
                }
                
            }

        }
        if ($sewing) {
            $styles = Style::where('erp',$sewing->style)->first();
            if ($styles) {
                $style_default = $styles->default;
            } else {
                $style_default = $sewing->style;
            }
            
        } else {
            $style_default = null;
        }
        
        $obj                = new StdClass();
        $obj->factory_id    = ($master_line?$master_line->factory_id:null);
        $obj->line_id       = ($master_line?$master_line->id:null);
        $obj->factory       = $factory;
        $obj->line          = $line;
        $obj->line_name     = $master_line->name;
        $obj->style         = ($style)?(($sewing)?$sewing->style:$style):$style_default;
        $obj->style_default = $style_default;
        $obj->article       = ($sewing?$sewing->article:null);
        $obj->sewing_id     = ($sewing?$sewing->id:null);
        return view('sewing.index',compact('obj'));
    }
    public function getStyle(Request $request)
    {
        $lists = DB::table('list_style_sewing_v')
        ->select('line_id','factory_id','style','article',db::raw('total_qty-total_counter as balance'))
        ->where([
            ['line_id',$request->line_id],
            ['factory_id',$request->factory_id]
        ])
        ->whereRaw('total_qty>total_counter')
        ->orderBy('max_date','desc')
        ->get();
        return view('sewing._style_picklist', compact('lists'));
    }
    static function getCounter(Request $request)
    {
        if ($request->ajax()) {
            $line_id       = $request->line_id;
            $style         = $request->style;
            $style_default = $request->style_default;
            $article       = $request->article;
            $counter_day   = 0;
            $now           = Carbon::createFromFormat('Y-m-d H:i:s',now())->format('Y-m-d');
            $balance       = 0;
            if ($style) {
                $styles = Style::where('default',$style_default);
                foreach ($styles->get() as $key => $s) {
                    $styleErp[] = $s->erp;
                }
                $sewing      = DB::table('list_style_sewing_v')
                ->where([
                    ['line_id',$line_id],
                    ['style',$style_default],
                    ['article',$article]

                ])
                ->first();
                $counter_day = Sewing::where('update_counter_day',$now)
                ->where([
                    ['line_id',$line_id],
                    ['article',$article]

                ])
                ->whereIn('style',$styleErp)
                ->sum('counter_day');
                $totalQty = ($sewing)?$sewing->total_qty:0;
                $totalCounter = ($sewing)?$sewing->total_counter:0;
                $balance     = $totalQty-$totalCounter;
            }
            return response()->json(['counter_day' => $counter_day,'balance'=>$balance], 200);
        }
    }
    public function selectStyle(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $sewing = Sewing::where([
                ['line_id',$request->line_id],
                ['factory_id',$request->factory_id],
                ['status','active']
            ])->first();
            if ($sewing) {
                DB::table('sewings')
                ->where('id',$sewing->id)
                ->update(['status' => null]);
                $sewing_id = $this->InsertSewing($request->line_id,$request->factory_id,$request->style,$request->article);
                $styles = Style::where('erp',$request->style)->first();
                if ($styles) {
                    $style_default = $styles->default;
                } else {
                    $style_default = $request->style;
                }
            } else {
                $sewing_id = $this->InsertSewing($request->line_id,$request->factory_id,$request->style,$request->article);
                $style_default = null;
            }
            return response()->json(['sewing_id' => $sewing_id,'style_default'=>$style_default],200);
        }
    }
    private function InsertSewing($line_id,$factory_id,$style,$article)
    {
        $cekSewing = Sewing::where('line_id',$line_id)
        ->where([
            ['factory_id',$factory_id],
            ['style',$style],
            ['article',$article]
        ]);
        if (!$cekSewing->exists()) {
            $sewing = Sewing::Create([
                'factory_id' => $factory_id,
                'line_id'    => $line_id,
                'style'      => $style,
                'article'    => $article,
                'status'     => 'active'
            ]);
        }else{
            $sewing = $cekSewing->first();
            DB::table('sewings')
            ->where('id',$sewing->id)
            ->update(['status' => 'active']);
        }
        return $sewing->id;
    }
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $sewing_id = $request->sewing_id;
            $qtyIdle   = $request->qtyIdle;
            $cekSewing = Sewing::findOrFail($sewing_id);
            if(!$cekSewing){
                return response()->json(['message' => 'Silahkan pilih Style'], 422);
            }
            
            try {
                DB::beginTransaction();
                $counterHeader = $cekSewing->counter+1;
                for ($i=0; $i < $qtyIdle ; $i++) { 
                    SewingDetail::Create([
                        'sewing_id' => $sewing_id,
                        'counter' => $counterHeader,
                        'ip_address' => \Request::ip()
                    ]);
                    $counterHeader++;
                }
                $cekSewing->counter = $cekSewing->counter+$qtyIdle;
                if ($cekSewing->update_counter_day==Carbon::now()->format('Y-m-d')) {
                    $cekSewing->counter_day = $cekSewing->counter_day+$qtyIdle;
                }else{
                    $cekSewing->counter_day = $qtyIdle;
                    $cekSewing->update_counter_day = Carbon::now()->format('Y-m-d');
                }
                $cekSewing->save();
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
}
