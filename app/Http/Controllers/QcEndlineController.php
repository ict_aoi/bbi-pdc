<?php namespace App\Http\Controllers;

use DB;
use View;
use Mqtt;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;
use Illuminate\Support\Facades\Validator;

use App\Models\Line;
use App\Models\Style;
use App\Models\Sewing;
use App\Models\Defect;
use App\Models\Setting;
use App\Models\Factory;
use App\Models\LogLogin;
use App\Models\QcEndline;
use App\Models\Production;
use App\Models\AndonSupply;
use App\Models\QcOutputView;
use App\Models\WorkingHours;
use App\Models\DailyWorking;
use App\Models\DailyLineView;
use App\Models\ProductionSize;
use App\Models\QcEndlineDetail;
use App\Models\QcEndlineDefect;
use App\Models\Absensi\AbsensiBbi;

class QcEndlineController extends Controller
{
    public function index(Request $request,$factory,$line)
    {
        $nik          = null;
        $name         = null;
        $subdept_name = null;
        $code         = null;
        $line_id      = null;
        $factory_id   = null;
        $buyer        = null;

        $master_line    = Line::where('code', $line)
        ->whereHas('factory', function ($query) use ($factory) {
            $query->where('code', $factory);
        })
        ->whereNull('delete_at')
        ->first();
        if($master_line){
            $line_id    = $master_line->id;
            $factory_id = $master_line->factory_id;
            $buyer      = $master_line->buyer;
            $dataLog    = LogLogin::whereNull('delete_at')
            ->where('line_id',$line_id)
            ->where('name','qc_endline')->orderBy('created_at','desc')->first();
            
            $date         = Carbon::now()->format('Y-m-d');
            $dailyWorking = DailyLineView::getStyle($date,$master_line->id);
            
            $style               = $dailyWorking?explode(",",$dailyWorking->style):[];
            // dd($style);
            $setting             = Setting::whereNull('delete_at')->where('name','websocket')->first();
            $start               = ($dailyWorking?$dailyWorking->start:null);
            $end                 = ($dailyWorking?$dailyWorking->end:null);
            $commitment_line     = ($dailyWorking?$dailyWorking->commitment_line:null);
            $commitment_line_jam = ($dailyWorking?$dailyWorking->commitment_line/$dailyWorking->working_hours:null);
            // $topicTarget         = "cmd/$factory/qc_endline/commitment/$line_id";
            
            $topic             = [];
            $objTopic          = new stdClass();
            $objTopic->andon   = "cmd/$factory/$buyer/dashboard/andon-supply";
            $objTopic->balance = "cmd/$factory/qc_endline/balance/$line_id";
            $objTopic->target  = "cmd/$factory/qc_endline/commitment/$line_id";
            
            $topic[] = $objTopic;
            
            
            if ($dataLog) {
                $factory = Factory::find($dataLog->factory_id);
                $line_id = Line::find($dataLog->line_id);
                
                if($dataLog->expired_date>Carbon::now()){
                    $request->session()->put('nik', $dataLog->created_by_nik);
                    $request->session()->put('name', $dataLog->created_by_name);
                    $request->session()->put('factory', $factory->code);
                    $request->session()->put('factory_id', $dataLog->factory_id);
                    $request->session()->put('subdept_name', $dataLog->subdept_name);
                    $request->session()->put('buyer', $buyer);
                    $request->session()->put('line', $line_id->code);
                    $request->session()->put('line_id', $line_id->id);

                    $nik          = $request->session()->get('nik');
                    $name         = $request->session()->get('name');
                    $subdept_name = $request->session()->get('subdept_name');
                    $code         = $request->session()->get('line');


                    $line_id                  = $dataLog->line_id;
                    $factory_id               = $dataLog->factory_id;
                    $obj                      = new stdClass();
                    $obj->nik                 = $nik;
                    $obj->name                = $name;
                    $obj->factory_id          = $factory_id;
                    $obj->line_id             = $line_id;
                    $obj->buyer               = $buyer;
                    $obj->factory             = $factory->code;
                    $obj->line                = $line;
                    $obj->subdept_name        = $subdept_name;
                    $obj->start               = ($dailyWorking?$dailyWorking->start:null);
                    $obj->end                 = ($dailyWorking?$dailyWorking->end:null);
                    $obj->commitment_line_jam = $commitment_line_jam;
                    $obj->commitment_line     = $commitment_line;
                    $obj->ip_websocket        = $setting?$setting->value1:0;
                    $obj->port_websocket      = $setting?$setting->value2:0;
                    $obj->topic               = json_encode($topic);
                    $obj->style_daily         = json_encode($style);
                    $obj->header_name         = 'QC ENDLINE (' . strtoupper($master_line->name) . ')';

                    return view('qc_endline.index', compact('obj'));
                }else{
                    $request->session()->forget(['nik', 'nama', 'factory', 'line', 'job_desc']);
                    $obj                      = new stdClass();
                    $obj->nik                 = $nik;
                    $obj->name                = $name;
                    $obj->factory_id          = $factory_id;
                    $obj->line_id             = $line_id;
                    $obj->buyer               = $buyer;
                    $obj->factory             = $factory->code;
                    $obj->line                = $line;
                    $obj->start               = $start;
                    $obj->end                 = $end;
                    $obj->commitment_line_jam = $commitment_line_jam;
                    $obj->commitment_line     = $commitment_line;
                    $obj->subdept_name        = $subdept_name;
                    $obj->topic               = json_encode($topic);
                    $obj->style_daily         = json_encode($style);
                    $obj->header_name         = 'QC ENDLINE (' . strtoupper($master_line->name) . ')';

                    return view('qc_endline.index', compact('obj'));
                }
            }else{
                $obj                      = new stdClass();
                $obj->nik                 = $nik;
                $obj->name                = $name;
                $obj->factory_id          = $factory_id;
                $obj->line_id             = $line_id;
                $obj->buyer               = $buyer;
                $obj->factory             = $factory;
                $obj->line                = $line;
                $obj->start               = $start;
                $obj->end                 = $end;
                $obj->commitment_line_jam = $commitment_line_jam;
                $obj->commitment_line     = $commitment_line;
                $obj->subdept_name        = $subdept_name;
                $obj->topic               = json_encode($topic);
                $obj->style_daily         = json_encode($style);
                $obj->header_name         = 'QC ENDLINE (' . strtoupper($master_line->name) . ')';
                return view('qc_endline.index', compact('obj'));
            }
        }
    }

    public function createEditSession(Request $request)
    {
        $nik        = $request->nik;
        $name       = $request->name;
        $factory    = $request->factory;
        $line       = $request->line;

        if($request->session()->has('nik'))
        {
            $request->session()->forget(['nik', 'nama','factory','line']);
        }else
        {
            $request->session()->put('nik',$nik);
            $request->session()->put('nama',$name);
            $request->session()->put('factory',$factory);
            $request->session()->put('line',$line);
        }
        
        return response()->json(200);
    }

    public function deleteSession(Request $request)
    {
        $request->session()->forget(['nik', 'nama','factory','line']);
        return response()->json(200);
    }
    static function getAbsence($nik)
    {
        $master = AbsensiBbi::where('nik', $nik)->first();
        if ($master) {
            $obj                = new stdClass();
            $obj->nik           = $master->nik;
            $obj->name          = strtolower($master->name);
            $obj->subdept_name  = strtolower($master->subdept_name);

            return $obj;
        } else return null;
    }
    public function login(Request $request)
    {
        $nik        = trim($request->nik);
        $line       = $request->line;
        $factory    = $request->factory;
        $user       = $this->getAbsence($nik);

        if ($user) {
            
            $master_line    = Line::where('code', $line)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();
            if (!$master_line) {
                return response()->json(['message'=>'Code Line Tidak di Temukan'],422);
            }

            $request->session()->put('nik', $user->nik);
            $request->session()->put('name', $user->name);
            $request->session()->put('factory', $factory);
            $request->session()->put('factory_id', $master_line->factory_id);
            $request->session()->put('subdept_name', $user->subdept_name);
            $request->session()->put('line', $line);
            $request->session()->put('buyer', $master_line->buyer);
            $request->session()->put('line_id', $master_line->id);
            $dataLogin = LogLogin::whereNull('delete_at')
            ->where('line_id',$master_line->id)
            ->where('name','qc_endline')
            ->orderBy('created_at','desc')
            ->first();
            if($dataLogin){
                LogLogin::create([
                    'line_id'         => $master_line->id,
                    'name'            => 'qc_endline',
                    'created_by_nik'  => $user->nik,
                    'created_by_name' => $user->name,
                    'subdept_name'    => $user->subdept_name,
                    'factory_id'      => $master_line->factory_id,
                    'expired_date'    => Carbon::parse(now())->addHours(9),
                ]);
                $dataLogin  ->delete_at = Carbon::now();
                $dataLogin->save();
            }else{
                LogLogin::create([
                    'line_id'         => $master_line->id,
                    'name'            => 'qc_endline',
                    'created_by_nik'  => $user->nik,
                    'created_by_name' => $user->name,
                    'subdept_name'    => $user->subdept_name,
                    'factory_id'      => $master_line->factory_id,
                    'expired_date'    => Carbon::parse(now())->addHours(9),
                ]);
            }
            $url_qc_endline = route('endLine.index', [$factory, $line]);
            return response()->json($url_qc_endline, 200);
        } else return response()->json('Nik tidak ditemukan di absensi', 422);
    }

    public function logout(Request $request)
    {
        $factory        = $request->factory;
        $line           = $request->line;

        $master_line    = Line::where('code', $line)
            ->whereHas('factory', function ($query) use ($factory) {
                $query->where('code', $factory);
            })
            ->whereNull('delete_at')
            ->first();
        $dataLog    = LogLogin::whereNull('delete_at')
        ->where('line_id',$master_line->id)
        ->where('name','qc_endline')->orderBy('created_at')->first();
        
        $request->session()->forget(['nik', 'nama', 'factory', 'line', 'job_desc']);
        $dataLog->delete_at = Carbon::now();
        $dataLog->save();

        return redirect()->route('endLine.index', [$factory, $line]);
    }
    static function poreferencePickList(Request $request)
    {
        if ($request->ajax()) {
            $q     = trim(strtolower($request->q));
            $buyer = $request->buyer;
            $lists = DB::table('qc_output_v')
            ->distinct()
            ->select('production_id','style','poreference','article','start_date')
            ->where('line_id', $request->session()->get('line_id'))
            ->where('is_show_qc','t')
            ->where(db::raw('lower(poreference)'), 'LIKE', "%$q%")
            ->whereRaw('balance<0')
            ->whereRaw('qty_order>0')
            ->orderBy('start_date','desc')
            ->paginate(10);

            return view('qc_endline._poreference_picklist', compact(['buyer','lists']));
        }
    }
    static function sizePickList(Request $request)
    {
        if ($request->ajax()) {
            $q          = trim(strtolower($request->q));
            
            $lists      = ProductionSize::select('production_sizes.id','production_sizes.size','production_sizes.qty','qc_endlines.counter')
                ->leftJoin('qc_endlines', function($join){
                    $join->on('qc_endlines.production_id', '=', 'production_sizes.production_id');
                    $join->on('qc_endlines.production_size_id', '=', 'production_sizes.id');
                })
                ->whereNull('production_sizes.delete_at')
                ->whereRaw('production_sizes.qty>0')
                ->where([
                    ['production_sizes.production_id', $request->production_id],
                    [db::raw('lower(production_sizes.size)'), 'LIKE', "%$q%"]
                ])
                ->paginate(10);

            return view('qc_endline._size_picklist', compact('lists'));
        }
    }
    public function getDataQc(Request $request)
    {
        if ($request->ajax()) {
            
            $id                 = $request->production_id;
            $production_size_id = $request->production_size_id;
            $date               = Carbon::now()->format('Y-m-d');
            $production         = Production::find($id);
            $productionSize     = ProductionSize::find($production_size_id);
            $dataQcEndline      = QcEndline::whereNull('delete_at')
            ->where('production_id',$id)
            ->where('production_size_id',$production_size_id)
            ->first();
            if($dataQcEndline){
                DB::select(db::Raw("select * from update_counter('$dataQcEndline->id')"));
                DB::select(db::Raw("select * from update_counter_qc('$dataQcEndline->id','$date')"));
                DB::select(db::Raw("select * from update_repair_qc('$dataQcEndline->id')"));
            }
            $styles = Style::where('erp',$production->style)->first();
            $styleDefault = ($styles)?$styles->default:$production->style;
            if($styles){
                $sewing = DB::table('list_style_sewing_v')
                ->where('style',$styleDefault)
                ->where('article',$production->article)
                ->where('line_id',$production->line_id)
                ->first();
            }else{
                $sewing = Sewing::where('style',$styleDefault)
                ->where('article',$production->article)
                ->where('line_id',$production->line_id)
                ->first();
            }
            $counterEndStyle = QcEndline::select(db::raw('sum(counter)+sum(repairing) as counter'))
            ->whereIn(db::raw('(production_id)'),function($query) use ($production,$styles,$styleDefault){
                if($styles){
                    $query->select('productions.id')
                    ->from('productions')
                    ->leftJoin('styles','styles.erp','productions.style')
                    ->whereNull('productions.delete_at')
                    ->where('line_id',$production->line_id)
                    ->where('styles.default',$styleDefault)
                    ->where('article',$production->article);
                }else{
                    $query->select('id')
                    ->from('productions')
                    ->whereNull('delete_at')
                    ->where('line_id',$production->line_id)
                    ->where('style',$production->style)
                    ->where('article',$production->article);
                }
            })->first();
            $dailyWorking      = DailyWorking::where('line_id',$production->line_id)->where(db::raw('date(created_at)'),Carbon::now())->first();
            $counter           = ($styles)?($sewing?$sewing->total_counter:0):($sewing?$sewing->counter:0);
            $obj               = new StdClass();
            $obj->wip          = ($counter)-($counterEndStyle?$counterEndStyle->counter:0);
            $obj->qcEndlineId  = ($dataQcEndline?$dataQcEndline->id:null);
            $obj->repairing    = ($dataQcEndline?$dataQcEndline->repairing:0);
            $obj->totRepairing = ($dataQcEndline?$dataQcEndline->total_repairing:0);
            $obj->totRepairing = ($dataQcEndline?$dataQcEndline->total_repairing:0);
            $obj->counter      = ($dataQcEndline?$dataQcEndline->counter:0);
            $obj->counterDay   = ($dataQcEndline?($dataQcEndline->counter_day_at==Carbon::now()->format('Y-m-d')?$dataQcEndline->counter_day:0):0);
            $obj->balance      = ($dataQcEndline?$dataQcEndline->counter:0)-$productionSize->qty;
            $obj->start        = ($dailyWorking?$dailyWorking->start:null);
            $obj->end          = ($dailyWorking?$dailyWorking->end:null);
            $obj->style_merge  = ($styles)?$styles->default:$production->style;
            return response()->json($obj, 200);
        }
    }
    public function store(Request $request)
    {
       if ($request->ajax()) {
            $qc_endline_id      = $request->qc_endline_id;
            $qtyIdle            = (int)$request->qtyIdle;
            $production_id      = $request->production_id;
            $production_size_id = $request->production_size_id;
            $date = Carbon::now()->format('Y-m-d');
            // $production         = Production::find($production_id);
            $productionSize     = ProductionSize::find($production_size_id);
            
            $qcEndline = QcEndline::where([
                ['production_id',$production_id],
                ['production_size_id',$production_size_id],
            ])->first();
            
            $counter = ($qcEndline?($qcEndline->counter+$qcEndline->repairing):0)+$qtyIdle;
            $balance = $productionSize->qty-$counter;
            if ($productionSize->production_id!=$production_id) {
                return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
            }
            if ($balance<0) {
                return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
            }
            if (!$qcEndline) {
                $qc_endlines = QcEndline::FirstOrCreate([
                    'production_id' => $production_id,
                    'production_size_id' => $production_size_id
                ]);
                $qcEndline     = $qc_endlines;
                $qc_endline_id = $qc_endlines->id;
            }
            try {
                DB::beginTransaction();
                
                for ($i=0; $i < $qtyIdle; $i++) { 
                    QcEndlineDetail::create([
                        'qc_endline_id'   => $qc_endline_id,
                        'counter'         => 1,
                        'ip_address'      => \Request::ip(),
                        'created_by_name' => $request->session()->get('nik'),
                        'created_by_nik'  => $request->session()->get('name')
                    ]);
                }
                
                DB::select(db::Raw("select * from update_counter('$qc_endline_id')"));
                DB::select(db::Raw("select * from update_counter_qc('$qc_endline_id','$date')"));
                DB::commit();
                return response()->json(['success','qc_endline_id'=>$qc_endline_id],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
       }
    }
    public function storeDefect(Request $request)
    {
        if ($request->ajax()) {
            $qc_endline_id      = $request->qc_endline_id;
            $defect_id          = $request->defect_id;
            $production_id      = $request->production_id;
            $production_size_id = $request->production_size_id;
            $productionSize     = ProductionSize::find($production_size_id);
            $qcEndline = QcEndline::find($qc_endline_id);

            $counter = ($qcEndline?($qcEndline->counter+$qcEndline->repairing):0)+1;
            $balance = $productionSize->qty-$counter;
            

            if ($productionSize->production_id!=$production_id) {
                return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
            }

            if ($balance<0) {
                return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
            }
            try {
                DB::beginTransaction();
                $qcEndline     = QcEndline::find($qc_endline_id);
                
            if (!$qcEndline) 
            {
                $qc_endlines = QcEndline::FirstOrCreate([
                    'production_id' => $production_id,
                    'production_size_id' => $production_size_id
                ]);
                $qc_endline_id = $qc_endlines->id;
                $qcEndline = $qc_endlines;
            }
                $qc_endline_detail = QcEndlineDetail::create([
                    'qc_endline_id'   => $qc_endline_id,
                    'counter'         => '0',
                    'ip_address'      => \Request::ip(),
                    'created_by_name' => $request->session()->get('nik'),
                    'created_by_nik'  => $request->session()->get('name')
                ]);
                
                QcEndlineDefect::create([
                    'qc_endline_id'        => $qc_endline_id,
                    'qc_endline_detail_id' => $qc_endline_detail->id,
                    'defect_id'            => $defect_id
                ]);
                DB::select(db::Raw("select * from update_repair_qc('$qc_endline_id')"));
                DB::commit();
                return response()->json(['success','qc_endline_id'=>$qc_endline_id],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    static function storeRepair(Request $request)
    {
        if ($request->ajax()) {
            $id        = $request->id;
            
            
            try {
                DB::beginTransaction();
                
                $repair    = QcEndlineDefect::find($id);
                $repair->update_defect_date = Carbon::now();
                $repair->save();
                
                $QcDefect = QcEndlineDefect::whereNull('update_defect_date')->where('qc_endline_detail_id',$repair->qc_endline_detail_id)->count();

                if ($QcDefect==0) {
                    $endlineDetail             = QcEndlineDetail::find($repair->qc_endline_detail_id);
                    $endlineDetail->counter    = 1;
                    $endlineDetail->ip_address = \Request::ip();
                    $endlineDetail->created_at = Carbon::now();
                    $endlineDetail->save();
                }
                $date = Carbon::now()->format('Y-m-d');
                DB::select(db::Raw("select * from update_counter('$repair->qc_endline_id')"));
                DB::select(db::Raw("select * from update_counter_qc('$repair->qc_endline_id','$date')"));
                DB::select(db::Raw("select * from update_repair_qc('$repair->qc_endline_id')"));
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }
    }
    public function getRepairing(Request $request)
    {
        /* if ($request->ajax()) {
            
        } */
        $qc_endline_id          = $request->qc_endline_id;
            return view('qc_endline._repairing_lov', compact('qc_endline_id'));
    }
    public function getRepairingAll(Request $request)
    {
        $qc_endline_id          = $request->qc_endline_id;
            return view('qc_endline._repairing_all_lov', compact('qc_endline_id'));
    }
    public function getDataRepairing(Request $request)
    {
        if ($request->ajax()) {
            $data = QcEndlineDefect::whereNull('update_defect_date')
            ->where('qc_endline_id',$request->qc_endline_id)
            ->orderBy('created_at');
            
            return datatables()->of($data)
            
            ->editColumn('tanggal',function($data){
                return $data->created_at;
            })
            ->editColumn('code',function($data){
                return $data->defect_id;
            })
            ->editColumn('name',function($data){
                return ucwords($data->defect->name);
            })
            ->addColumn('aksi',function($data){
                $url = route('endLine.storeRepair','&id='.$data->id);
                return '<button class="btn btn-info btn-xs btn-choose" type="button" onclick="storeRepair(\''.$url.'\')">Perbaiki</button>';
            })
            ->rawColumns(['aksi'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    public function getDataRepairingAll(Request $request)
    {
        if ($request->ajax()) {
            $data = QcEndlineDefect::whereNull('update_defect_date')
            ->select(db::raw('qc_endline_defects.id as qc_endline_defect_id'),db::raw("defects.name as defect_name"),'defects.root_cause','productions.line_id','productions.style','productions.poreference','productions.article','production_sizes.size','qc_endline_defects.defect_id','qc_endline_defects.created_at')
            ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
            ->leftJoin('productions','productions.id','qc_endlines.production_id')
            ->leftJoin('production_sizes','production_sizes.id','qc_endlines.production_size_id')
            ->leftJoin('defects','defects.id','qc_endline_defects.defect_id')
            ->where([
                ['productions.line_id',$request->session()->get('line_id')],
                ['productions.is_show_qc','t']
            ])
            ->orderBy('qc_endline_defects.created_at');
            
            return datatables()->of($data)
            
            ->editColumn('tanggal',function($data){
                return $data->created_at;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('code',function($data){
                return $data->defect_id;
            })
            ->editColumn('defect_name',function($data){
                return ($data->root_cause!=null)?$data->defect_name:$data->root_cause." ".$data->defect_name;
            })
            ->addColumn('aksi',function($data){
                $url = route('endLine.storeRepair','&id='.$data->qc_endline_defect_id);
                return '<button class="btn btn-info btn-xs btn-choose" type="button" onclick="storeRepair(\''.$url.'\')">Perbaiki</button>';
            })
            ->rawColumns(['aksi'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    public function reportQcCheck(Request $request)
    {
        if ($request->ajax()) {
            $qcEndline = DB::table('qc_history_output_v')
            ->select(db::raw('COALESCE(sum(counter_day),0) as counter_day'),db::raw('COALESCE(sum(total_defect_day),0) as total_defect_day'))
            ->where('date',Carbon::now()->format('Y-m-d'))
            ->where('line_id',$request->session()->get('line_id'))
            ->first();
            $repairing = DB::table('qc_output_v')
            ->where('line_id',$request->session()->get('line_id'))
            ->sum('repairing');
            $buyer = $request->session()->get('buyer');
            return view('qc_endline.report._daily_inspection',compact('qcEndline','repairing','buyer'));
        }
    }
    public function getDataOutputQc(Request $request)
    {
        if ($request->ajax()) {
            $date = ($request->date?$request->date:Carbon::now()->format('Y-m-d'));
            $data = DB::table('qc_history_output_v')
            ->where('date',$date)
            ->where('line_id',$request->session()->get('line_id'));
            
            return datatables()->of($data)
            
            ->editColumn('date',function($data){
                return $data->date;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('qty_order',function($data){
                return $data->qty_order;
            })
            ->editColumn('total_counter',function($data){
                return $data->total_counter;
            })
            ->editColumn('counter_day',function($data){
                return $data->counter_day;
            })
            ->editColumn('balance_per_day',function($data){
                return $data->balance_per_day;
            })
            ->editColumn('total_defect_day',function($data){
                return $data->total_defect_day;
            })
            
            // ->rawColumns(['aksi'])
            ->make(true);
        }
    }
    static function getDataSumQc(Request $request)
    {
        if ($request->ajax()) {
            $date = ($request->date?Carbon::createFromFormat('d/m/Y',$request->date)->format('Y-m-d'):Carbon::now()->format('Y-m-d'));
            $qcEndlineSum = DB::table('qc_history_output_v')
            ->select(db::raw('COALESCE(sum(counter_day),0) as counter_day'),db::raw('COALESCE(sum(total_defect_day),0) as total_defect_day'))
            ->where('date',$date)
            ->where('line_id',$request->session()->get('line_id'))
            ->first();
            return response()->json($qcEndlineSum, 200);
        }
    }
    static function getDetailDefect(Request $request){
        if ($request->ajax()) {
            $date = ($request->date?Carbon::createFromFormat('d/m/Y',$request->date)->format('Y-m-d'):Carbon::now()->format('Y-m-d'));
            $data = QcEndlineDefect::select('productions.line_id','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'),db::raw('count(*)as total_defect'))
            ->leftJoin('qc_endlines','qc_endlines.id','qc_endline_defects.qc_endline_id')
            ->leftJoin('productions','productions.id','qc_endlines.production_id')
            ->leftJoin('defects','defects.id','qc_endline_defects.defect_id')
            ->where(db::raw('date(qc_endline_defects.created_at)'),$date)
            ->where('line_id',$request->session()->get('line_id'))
            ->groupBy('productions.line_id','defects.name','qc_endline_defects.defect_id',db::raw('date(qc_endline_defects.created_at)'))
            ->orderBy(db::raw('count(*)'),'DESC')
            ->get();
            return view('qc_endline.report._detail_defect', compact('data'));
        }
    }
    static function getStatusPo(Request $request)
    {
        if ($request->ajax()) {
            $buyer = $request->session()->get('buyer');
            return view('qc_endline.report._status_po',compact('buyer'));
        }
    }
    public function getDetailOutput(Request $request)
    {
        if ($request->ajax()) {
            $buyer             = $request->session()->get('buyer');
            $line_id           = $request->session()->get('line_id');
            $now               = Carbon::now();
            $date              = Carbon::now()->format('Y-m-d');
            $workingHours = WorkingHours::orderBy('start','ASC')
            ->get();
            $data       = DB::select(db::Raw("select * from f_hourly_output('$date','$date') where line_id='$line_id'"));
            
            return view('qc_endline.report._detail_output_hourly',compact('workingHours','data'));
        }
    }
    public function getDataStatusPo(Request $request)
    {
        if ($request->ajax()) {
            $status    = $request->status;
            // dd($status);
            $poreference    = $request->poreference;
            $data = Production::select('qc_endlines.id as qc_endline_id','productions.line_id','productions.style','productions.poreference','productions.article','production_sizes.size','production_sizes.qty as qty_order',
            db::raw('COALESCE(qc_endlines.counter,0) as total_output'),db::raw('(COALESCE(qc_endlines.counter,0))-production_sizes.qty as balance'),db::raw('max(qc_endlines.counter_day_at) AS last_update'))
            ->leftJoin('production_sizes','production_sizes.production_id','productions.id')
            ->leftJoin('qc_endlines', function($q) {
                $q->on('qc_endlines.production_id', '=', 'productions.id');
                $q->on('qc_endlines.production_size_id', '=','production_sizes.id');
            })
            ->leftJoin('lines','lines.id','productions.line_id')
            ->where('productions.line_id',$request->session()->get('line_id'))
            ->where(function($query) use($status)
            {
                if ($status!=NULL) {
                    if($status=='outstanding')
                    {
                        $query = $query->whereRaw('production_sizes.qty>COALESCE(qc_endlines.counter,0)');
                    }else if($status=='balance'){
                        $query = $query->whereRaw('production_sizes.qty=COALESCE(qc_endlines.counter,0)');
                    }else if($status=='over'){
                        $query = $query->whereRaw('production_sizes.qty<COALESCE(qc_endlines.counter,0)');
                    }
                }
                
            })
            ->where(function($query) use($poreference)
            {
                if ($poreference!=NULL) {
                    $query = $query->where('productions.poreference','Like','%'.$poreference.'%');
                }
                
            })
            ->groupBy('qc_endlines.id','productions.style','productions.poreference','productions.article','production_sizes.size','production_sizes.qty','qc_endlines.counter','productions.line_id')
            ->orderBy(db::raw('max(qc_endlines.counter_day_at)','desc'));
            
            return datatables()->of($data)
            
            ->editColumn('date',function($data){
                return $data->last_update;
            })
            ->editColumn('style',function($data){
                return $data->style;
            })
            ->editColumn('poreference',function($data){
                return $data->poreference;
            })
            ->editColumn('article',function($data){
                return $data->article;
            })
            ->editColumn('size',function($data){
                return $data->size;
            })
            ->editColumn('qty_order',function($data){
                return $data->qty_order;
            })
            ->editColumn('total_output',function($data){
                return $data->total_output;
            })
            ->editColumn('balance',function($data){
                return $data->balance;
            })
            ->addColumn('aksi', function($data) {
                return view('qc_endline.report._action_status_po', [
                    'model'  => $data,
                    'history'   => route('endLine.getHistoryDataPo','&id='.$data->qc_endline_id),
                ]);
            })
            ->addColumn('status',function($data){
                if ($data->qty_order==$data->total_output) {
                    return '<span class="label bg-success-400">Balance</span>';
                } else if($data->qty_order>$data->total_output){
                    return '<span class="label bg-blue">Kurang Output</span>';
                }else if($data->qty_order<$data->total_output){
                    return '<span class="label bg-danger">Lebih Output</span>';
                }
                
            })
            ->rawColumns(['aksi','status'])
            //->rawColumns(['aksi'])
            ->make(true);
        }
    }
    public function getHistoryDataPo(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('qc_history_output_v')
            ->where('qc_endline_id',$request->id)
            ->orderBy('date','asc');

            return view('qc_endline.report._history_output', compact('data'));
        }
    }
    public function sshReboot(Request $request)
    {
        $ip = \Request::ip();
        $sftp = new SFTP($ip);
        $sftp_login = $sftp->login('pi', 'pi');
        if($sftp_login){
            $sftp->exec('sudo reboot');
        }
    }
    public function sshShutdown(Request $request)
    {
        $ip = \Request::ip();
        $sftp = new SFTP($ip);
        $sftp_login = $sftp->login('pi', 'pi');
        if($sftp_login){
            $sftp->exec('sudo shutdown now');
        }
    }
    static function getDefect(Request $request)
    {
        if ($request->ajax()) {
            $q       = trim(strtolower($request->q));
            $search  = str_replace(" ","%",$q);
            $line_id = ($request->line_id)?$request->line_id:$request->session()->get('line_id');
            $buyer   = $request->session()->get('buyer');
            $lists   = Defect::select('defects.id as id_defect','defects.code','defects.position','defects.name as name_defect', 'grade_defects.color', 'grade_defects.id as grade_defect_id')
                ->where(function ($query) use ($q,$search,$buyer) {
                    if ($buyer=="other") {
                        $query->where('defects.id', 'LIKE', "%$q%")
                        ->orWhere('defects.name', 'LIKE', "%$q%");
                    } else if($buyer=="nagai") {
                        $query->where('defects.code', 'LIKE', "$search%")
                        ->orWhere(db::raw('lower(defects.name)'), 'LIKE', "%$search%");
                    }
                    
                    
                })
                ->where('defects.id', '!=', '0')
                ->whereNull('delete_at')
                ->where('buyer',$buyer)  
                ->leftjoin('grade_defects', 'grade_defects.id', 'defects.grade_defect_id')
                ->orderBy('defects.id','asc');
                
                if($q) $lists = $lists->paginate(100);
                else $lists = $lists->paginate(10);

            // return view('qc_endline._defect_picklist', compact('lists','buyer'));
            $data = [
                'view'          => View::make('qc_endline._defect_picklist',compact('lists','buyer'))->render(),
                'data'          => $lists
            ];

            return response()->json($data);
        }
    }
    public function storeColectionDefect(Request $request){
        if ($request->ajax()) {
            try {
                $production_id      = $request->production_id_defect;
                $production_size_id = $request->production_size_id_defect;
                $qc_endline_id      = $request->qc_endline_id_defect;
                $defects            = json_decode($request->defect_confirmation_data);
                $productionSize     = ProductionSize::find($production_size_id);
                $qcEndline          = QcEndline::find($qc_endline_id);

                $counter = ($qcEndline?($qcEndline->counter+$qcEndline->repairing):0)+count($defects);
                $balance = $productionSize->qty-$counter;
                

                if ($productionSize->production_id!=$production_id) {
                    return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
                }

                if ($balance<0) {
                    return response()->json(['message' => 'Simpan Data Gagal Silahkan Refresh Halaman Anda'], 422);
                }
                DB::beginTransaction();
                if (!$qcEndline) 
                {
                    $qc_endlines = QcEndline::FirstOrCreate([
                        'production_id' => $production_id,
                        'production_size_id' => $production_size_id
                    ]);
                    $qc_endline_id = $qc_endlines->id;
                    $qcEndline = $qc_endlines;
                }
                    
                    foreach ($defects as $key => $d) {
                        for ($i=0; $i < $d->qty_defect; $i++) { 
                            $qcDetail = QcEndlineDetail::create([
                                'qc_endline_id'   => $qc_endline_id,
                                'counter'         => 0,
                                'ip_address'      => \Request::ip(),
                                'created_by_name' => $request->session()->get('nik'),
                                'created_by_nik'  => $request->session()->get('name')
                            ]);
                            QcEndlineDefect::create([
                                'qc_endline_id'        => $qc_endline_id,
                                'qc_endline_detail_id' => $qcDetail->id,
                                'defect_id'            => $d->defect_id
                            ]);
                        }
                        
                    }
                    DB::select(db::Raw("select * from update_repair_qc('$qc_endline_id')"));
                    DB::commit();
                    return response()->json(['success','qc_endline_id'=>$qc_endline_id],200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function getCommitmentLine(Request $request){
        if ($request->ajax()) {
            $now          = Carbon::now()->format('Y-m-d H:i:s');
            $line_id      = $request->line_id;
            $workingHours = WorkingHours::where('start',">=",$request->start)
            ->orderBy('start','ASC');
            $start = "";
            $end = "";
            $num = 0;
            foreach ($workingHours->get() as $key => $working) {
                
                if($working->start>=$request->start)
                {
                    
                    $awal  = Carbon::now()->format('Y-m-d').' '.$working->start;
                    $akhir = Carbon::now()->format('Y-m-d').' '.$working->end;
                    
                    
                    if($now >= $awal && $now < $akhir){
                        $start = Carbon::now()->format('Y-m-d').' '.$working->start;
                        $end   = Carbon::now()->format('Y-m-d').' '.$working->end;
                        $num = $key+1;
                    }
                }
            }
            $data       = DB::select(db::Raw("
                SELECT
                    count(*) as output
                FROM
                    qc_endline_details qed
                LEFT JOIN qc_endlines qe on qe.id=qed.qc_endline_id
                LEFT JOIN productions pr on pr.id=qe.production_id
                LEFT JOIN production_sizes ps on ps.id=qe.production_size_id
                where qed.created_at >= '$start'
                and qed.created_at < '$end'
                and pr.line_id='$line_id'
            "));
            $message = "Jam Ke - ".$num." Akan Segera Berakhir, Silahkan Input Good Agar Target Tercapai";
            $output = $data[0]->output;
            return response()->json(['output'=>$output,'message'=>$message],200);
        }
    }
    public function getAlertFolding(Request $request){
        if ($request->ajax()) {
            $now          = Carbon::now()->format('Y-m-d H:i:s');
            $line_id      = $request->line_id;
            
            $data = DB::table('alert_folding_v')
                    ->where('line_id',$line_id)
                    ->whereRaw('qc_output>=total_output')
                    ->whereRaw('balance<>0')
                    ->get();
            $totalData = $data?count($data):0;
            if ($totalData==0) {
                return response()->json(['total_data'=>$totalData],200);
            }
            $message = "Tolong input Nomor Order Berikut, Karena barang akan di kirim ke  Packing<br>";
            foreach ($data as $key => $d) {
                $message.="Style : ".$d->style."<br>";
                $message.="No. Lot : ".$d->poreference."<br>";
                $message.="Article : ".$d->article."<br>";
                $message.="size : ".$d->size."<br>";
            }
            return response()->json(['total_data'=>$totalData,'message'=>$message],200);
        }
    }

    public function checkAndon(Request $request)
    {
        $rules = [
            'line_id' => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $line_id = $request->line_id;
        $date    = Carbon::now()->format('Y-m-d');

        $dailyLine = DailyLineView::getStyle($date,$line_id);
        $style     = explode(",",$dailyLine->style);
        
        $data = QcOutputView::getBalance($date,$style,$line_id);
        $dataAndon = AndonSupply::active()
                    ->where([
                        ['line_id',$line_id],
                        [db::raw('date(created_at)'),$date]
                    ])
                    ->first();
                    
        if ($data) {
            $check    = $data->safety_stock > $data->balance;
            if ($check) {
                $codeLine = Line::find($line_id)->code;
                $topic   = 'cmd/andonbbis/group1/123';
                $message  = '{"type":"set_var","payload":{"'.$codeLine.'":"1"}}';
                $output   = Mqtt::ConnectAndPublish($topic, $message);
                if (!$dataAndon) {
                    AndonSupply::create([
                        'line_id'          => $line_id,
                        'balance'          => $data->balance,
                        'target_line'      => $data->commitment_line,
                        'safety_stock'     => $data->safety_stock,
                        'start_time'       => Carbon::now(),
                        'create_user_name' => $request->session()->get('name')??'system',
                        'create_user_nik'  => $request->session()->get('nik')??'system'
                    ]);
                }
            }
        }
    }
    public function getOutputStyle(Request $request)
    {
        $rules = [
            'line_id' => 'required|string',
            'style'   => 'required|string',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $line_id = $request->line_id;
        $style   = trim($request->style);
        $date    = Carbon::now()->format('Y-m-d');

        $data = QcOutputView::getBalanceStyle($date,$style,$line_id);

        $obj      = new StdClass();
        
        $obj->balance = $data?$data->balance:0;
        $obj->commitment_line_style = $data?$data->commitment_line:0;
        $obj->qty_loading = $data?$data->qty_loading:0;

        return response()->json($obj, 200);
    }
}