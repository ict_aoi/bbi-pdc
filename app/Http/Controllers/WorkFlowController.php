<?php namespace App\Http\Controllers;

use DB;

use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Line;
use App\Models\User;
use App\Models\Style;
use App\Models\Contact;
use App\Models\Machine;
use App\Models\QcInline;
use App\Models\Workflow;
use App\Models\BreakDownSize;
use App\Models\ProcessSewing;
use App\Models\WorkflowDetail;
use App\Models\ResponseWorkflow;
use App\Models\HistoryChangeStyle;
use App\Models\HistoryProcessSewing;
use App\Models\ResponseWorkflowDetail;


class WorkFlowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $process = ProcessSewing::whereNull('delete_at')
        ->where('factory_id',Auth::user()->factory_id)
        ->pluck('name','id')
        ->all();
        return view('workflow.index',compact('process'));
    }
    public function data()
    {
    	if(request()->ajax())
        {
            $data = Workflow::whereNull('delete_at')
            ->where('factory_id',Auth::user()->factory_id)
            ->orderby('created_at','desc');
            
            return datatables()->of($data)
            ->editColumn('style',function($data){
            	return ucwords($data->style);
            })
            ->editColumn('smv',function($data){
            	return ucwords($data->smv);
            })
            ->addColumn('action', function($data) {
                return view('workflow._action', [
                    'model' => $data,
                    'edit' => route('alur_kerja.create',$data->id),
                    'delete' => route('alur_kerja.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function dataDetailWorkflow(Request $request)
    {
    	if(request()->ajax())
        {
            $id   = $request->id;
            $data = WorkflowDetail::whereNull('workflow_details.delete_at')
                        ->select(
                            'workflow_details.id as id','workflow_details.process_sewing_id as proses_id',db::raw("process_sewings.name as proses_name"),"process_sewings.component","workflow_details.smv",'workflow_details.last_process as lastproses','workflow_details.critical_process as critical_process','machines.name as machine_name')
                        ->leftjoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
                        ->leftjoin('machines','machines.id','workflow_details.machine_id')
                        ->where('workflow_details.workflow_id',$id)
                        ->get();
            
            return datatables()->of($data)
            ->editColumn('proses_name',function($data){
            	return ucwords($data->proses_name);
            })
            ->editColumn('component',function($data){
            	return ucwords($data->component);
            })
            ->editColumn('machine_name',function($data){
            	return ucwords($data->machine_name);
            })
            ->editColumn('critical_process',function($data){
                if ($data->critical_process=='t') {
                    return '<span class="label label-danger">YES</span>';
                }else{
                    return '<span class="label label-primary">NO</span>';
                }
            })
            ->editColumn('lastproses',function($data){
                if ($data->lastproses=='t') {
                    return '<span class="label label-danger">YES</span>';
                }else{
                    return '<span class="label label-primary">NO</span>';
                }
            })
            ->editColumn('smv',function($data){
            	return ucwords($data->smv);
            })
            ->addColumn('action', function($data) {
                return view('workflow._action_create', [
                    'model'  => $data,
                    'edit'   => route('alur_kerja.edit',$data->id),
                    'delete' => route('alur_kerja.destroy_detail',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action','critical_process','lastproses'])
            ->make(true);
        }
    }
    public function storeHeader(Request $request)
    {
        $this->validate($request,[
            'style' => 'required',
            'gsd'   => 'required'
        ]);

        
        $cekExist = Workflow::whereNull('delete_at')->where('style',trim($request->style))->where('factory_id',Auth::user()->factory_id)->exists();
        if($cekExist) return response()->json(['message' => 'Style Sudah ada.'], 422);
        try {
            DB::beginTransaction();
            $workflow = Workflow::firstorCreate([
                'style'          => trim($request->style),
                'smv'            => trim($request->gsd),
                'factory_id'     => Auth::user()->factory_id,
                'create_user_id' => Auth::user()->id
            ]);
            DB::commit();
            $url = route('alur_kerja.create',$workflow->id);
            
            return response()->json($url,200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    public function updateHeader(Request $request,$id)
    {
        if ($request->ajax()) {
            try {
                $update                 = Workflow::findorFail($id);
                $update->smv            = $request->gsd;
                $update->update_user_id = Auth::user()->id;
                $update->save();
                
                return response()->json(200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function create(Request $request,$id)
    {
        $workflow = Workflow::find($request->id);
        
        if (!$workflow->delete_at) {
            $data   = Workflow::find($id);

            return view('workflow.create',compact('data','id'));
        }else{
            return redirect()->back();
        }
        
    }
    public function storeDetail(Request $request)
    {
        
        $workflow = Workflow::find($request->id);
        if (!$workflow->delete_at) {
            try
            {
                DB::beginTransaction();
                $detailworkflows     = json_decode($request->items);

                foreach ($detailworkflows as $key => $d) {
                    if($d->id == -1){
                        $is_exists = WorkflowDetail::where([
                            ['process_sewing_id',$d->proses_id],
                            ['workflow_id',$request->id],
                        ])
                        ->whereNull('delete_at')
                        ->exists();
                        if(!$is_exists){
                            WorkflowDetail::FirstOrCreate([
                                'workflow_id'       => $request->id,
                                'process_sewing_id' => $d->proses_id,
                                'last_process'      => $d->lastproses,
                                'create_user_id'    => Auth::user()->id
                            ]);
                        }
                        
                    }
                    
                    if ($workflow->smv!=$request->smv) {
                        $workflow->smv            = $request->smv;
                        $workflow->update_at      = carbon::now();
                        $workflow->update_user_id = Auth::user()->id;
                        
                        $workflow->save();
                    }
                }
                DB::commit();
                return response()->json('success',200);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }else{
            return redirect()->back();
        }
        
    }
    public function edit(Request $request,$id)
    {
        if ($request->ajax()) {
            $data = WorkflowDetail::find($id);
            // dd($data->last_process);
            return view('workflow._lov_edit_process', compact('id','data'));
        }
    }
    public function createProcess(Request $request,$id)
    {
        if ($request->ajax()) {
            return view('workflow._lov_create_process', compact('id'));
        }
    }
    public function mergeProcess(Request $request,$id)
    {
        if ($request->ajax()) {
            $lists = WorkflowDetail::whereNull('workflow_details.delete_at')
            ->leftJoin('process_sewings','process_sewings.id','workflow_details.process_sewing_id')
            ->leftJoin('machines','machines.id','workflow_details.machine_id')
            ->where('workflow_id',$id)
            ->pluck("process_sewings.name",'workflow_details.id')->all();
            return view('workflow.merge_process._lov_merge_process', compact('id','lists'));
        }
    }
    public function storeProcess(Request $request,$id)
    {
        if ($request->ajax()) {
            $name_proses       = trim(strtolower($request->name_process));
            $component         = trim(strtolower($request->component));
            $name_machine      = trim(strtolower($request->name_machine));
            $smv               = $request->smv;
            $workflow_id       = $request->workflow_id;
            $process_sewing_id = $request->process_id;
            $last_process      = ($request->last_process=='Y')?true:false;
            $critical_process  = ($request->critical_process=='Y')?true:false;
            try
            {
                DB::beginTransaction();
                
                $dataMachine = Machine::whereNull('delete_at')
                ->where([
                    ['factory_id',Auth::user()->factory_id],
                    ['name',$name_machine],
                ])->first();
                if ($dataMachine) {
                    $machine_id = $dataMachine->id;
                } else {
                    $machine = Machine::Create([
                        'code'               => $name_machine,
                        'name'               => $name_machine,
                        'factory_id'        => Auth::user()->factory_id,
                        'create_user_id'    => auth::user()->id
                    ]);
                    $machine_id = $machine->id;
                }
                
                $dataProcess = ProcessSewing::whereNull('delete_at')
                ->where([
                    ['name',$name_proses],
                    ['component',$component],
                ])
                ->first();
                if ($dataProcess) {
                    $process_id = $dataProcess->id;
                } else {
                    $process = ProcessSewing::Create([
                        'name'           => $name_proses,
                        'component'      => $component,
                        'factory_id'     => Auth::user()->factory_id,
                        'create_user_id' => auth::user()->id
                    ]);
                    $process_id = $process->id;
                }
                $cekWorkflow = WorkflowDetail::whereNull('delete_at')
                ->where([
                    ['workflow_id',$id],
                    ['process_sewing_id',$process_id],
                    ['machine_id',$machine_id],
                ])
                ->first();
                if ($cekWorkflow) {
                    return response()->json(['message' => 'Simpan Data Gagal, Proses Yang anda masukan sudah ada'], 422);
                }
                WorkflowDetail::create([
                    'workflow_id'       => $id,
                    'process_sewing_id' => $process_id,
                    'last_process'      => $last_process,
                    'critical_process'  => $critical_process,
                    'smv'               => $smv,
                    'machine_id'        => $machine_id,
                    'update_user_id'    => Auth::user()->id,
                ]);
                
                DB::commit();
                return response()->json('success',200);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function update(Request $request,$id)
    {
        if ($request->ajax()) {
            $name_proses       = trim(strtolower($request->name_process));
            $component         = trim(strtolower($request->component));
            $name_machine      = trim(strtolower($request->name_machine));
            $smv               = $request->smv;
            $workflow_id       = $request->workflow_id;
            $process_sewing_id = $request->process_id;
            $last_process      = ($request->last_process=='Y')?true:false;
            $critical_process  = ($request->critical_process=='Y')?true:false;
            try
            {
                DB::beginTransaction();
                
                $dataMachine = Machine::whereNull('delete_at')
                ->where([
                    ['factory_id',Auth::user()->factory_id],
                    ['name',$name_machine],
                ])->first();
                if ($dataMachine) {
                    $machine_id = $dataMachine->id;
                } else {
                    $machine = Machine::Create([
                        'code'               => $name_machine,
                        'name'               => $name_machine,
                        'factory_id'        => Auth::user()->factory_id,
                        'create_user_id'    => auth::user()->id
                    ]);
                    $machine_id = $machine->id;
                }
                
                $dataProcess = ProcessSewing::whereNull('delete_at')
                ->where([
                    ['name',$name_proses],
                    ['component',$component],
                ])
                ->first();
                if ($dataProcess) {
                    $process_id = $dataProcess->id;
                } else {
                    $process = ProcessSewing::Create([
                        'name'           => $name_proses,
                        'component'      => $component,
                        'factory_id'     => Auth::user()->factory_id,
                        'create_user_id' => auth::user()->id
                    ]);
                    $process_id = $process->id;
                }
                $cekWorkflow =null;
                if($process_id<>$process_sewing_id){
                    $cekWorkflow = WorkflowDetail::whereNull('delete_at')
                    ->where([
                        ['workflow_id',$workflow_id],
                        ['process_sewing_id',$process_id],
                        ['machine_id',$machine_id],
                    ])
                    ->first();
                    if ($cekWorkflow) {
                        $cekWorkflow =1;
                    } 
                }
                if ($cekWorkflow==1) {
                    return response()->json(['message' => 'Simpan Data Gagal, Proses Yang anda masukan sudah ada'], 422);
                }
                
                DB::table('workflow_details')
                ->where([
                    ['process_sewing_id',$id],
                    ['workflow_id',$workflow_id]
                ])
                ->update([
                    'process_sewing_id' => $process_id,
                    'last_process'      => $last_process,
                    'critical_process'  => $critical_process,
                    'smv'               => $smv,
                    'machine_id'        => $machine_id,
                    'updated_at'        => Carbon::now(),
                    'update_user_id'    => Auth::user()->id,
                ]);
                
                DB::commit();
                return response()->json('success',200);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    public function listOfStyle(Request $request)
    {
        $q = strtoupper(trim($request->q));

        $lists = Style::select(db::raw('"default" as style'))
        ->distinct()
        ->where(function($query)use ($q)
        {
            if ($q!='') {
                $query = $query->where('default','LIKE',"%$q%");
            }
        })
        ->whereNotIn(db::raw('"default"'),function($query){
            $query->select('style')
            ->from('workflows')
            ->whereNull('delete_at');
        })
        ->paginate(10);
        
        return view('workflow._lov_select_style',compact('lists'));
    }
    public function destroyHeader(Request $request,$id)
    {
        if ($request->ajax()) {
            $update                 = Workflow::findorFail($id);
            $update->delete_at      = carbon::now();
            $update->delete_user_id = Auth::user()->id;
            $update->save();

            return response()->json('success',200);
        }
    }
    public function destroyDetail(Request $request,$id)
    {
        if ($request->ajax()) {
            $update                 = WorkflowDetail::findorFail($id);
            $update->delete_at      = carbon::now();
            $update->delete_user_id = Auth::user()->id;
            $update->save();

            HistoryProcessSewing::where('workflow_detail_id', $id)
            ->update(array('delete_at' => Carbon::now()));

            return response()->json('success',200);
        }
    }
    public function import(Request $request,$id)
    {
        return view('workflow.import',compact('id'));
    }
    public function exportFormImport(Request $request,$id)
    {
        // dd($id);
        $Workflow = Workflow::whereNull('delete_at')
        ->where('id',$id)
        ->where('factory_id',Auth::user()->factory_id)->first();
        return Excel::create('template_style-'.$Workflow->style."_".Carbon::now()->format('u'),function ($excel)use($Workflow,$id){

            $excel->sheet('active', function($sheet) use($Workflow,$id) {
                $sheet->setCellValue('A1','workflow_id');
                for ($i=2; $i < 26 ; $i++) { 
                    $sheet->setCellValue('A'.$i,$id);
                    $sheet->setCellValue('B1','nama_proses');
                    $sheet->setCellValue('C1','komponen');
                    $sheet->setCellValue('D1','nama_mesin');
                    $sheet->setCellValue('E1','smv');
                    $sheet->setCellValue('F1','proses_terakhir');
                    $sheet->setCellValue('G1','critical_process');
                    $sheet->getComment('F'.$i)->getText()->createTextRun('Isi YA atau TIDAK');
                    $sheet->getComment('G'.$i)->getText()->createTextRun('Isi YA atau TIDAK');
                }
            
            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
        
    }
    public function uploadFormImport(Request $request,$id)
    {  
        $array              = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
            
            $path = $request->file('upload_file')->getRealPath();
           
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        $workflow              = Workflow::find($value->workflow_id);
                        $style                 = $workflow->style;
                        $workflow_id           = $value->workflow_id;
                        $nama_proses           = strtolower(trim($value->nama_proses));
                        $component             = strtolower(trim($value->komponen));
                        $nama_mesin            = strtolower(trim($value->nama_mesin));
                        $smv                   = strtolower(trim($value->smv));
                        $proses_terakhir       = strtoupper(trim($value->proses_terakhir));
                        $critical_process      = strtoupper(trim($value->critical_process));
                        $code_last_process     = ($proses_terakhir=='YA'?true:false);
                        $code_critical_process = ($critical_process=='YA'?true:false);
                        $validasiEndProses     = $this->cekProsesTerakhir($proses_terakhir);
                        if($workflow_id)
                        {
                            
                            if (!$workflow||($id!=$workflow_id)) {
                                $obj                   = new stdClass();
                                $obj->style            = $style;
                                $obj->nama_proses      = $nama_proses;
                                $obj->component        = $component;
                                $obj->nama_mesin       = $nama_mesin;
                                $obj->smv              = $smv;
                                $obj->style            = $style;
                                $obj->proses_terakhir  = $proses_terakhir;
                                $obj->critical_process = $critical_process;
                                $obj->is_error         = true;
                                $obj->system_log       = 'Workflow id tidak ada';

                                $array []             = $obj;
                            }else{
                                if(!$validasiEndProses){
                                    $obj                   = new stdClass();
                                    $obj->style            = $style;
                                    $obj->nama_proses      = $nama_proses;
                                    $obj->component        = $component;
                                    $obj->nama_mesin       = $nama_mesin;
                                    $obj->smv              = $smv;
                                    $obj->style            = $style;
                                    $obj->proses_terakhir  = $proses_terakhir;
                                    $obj->critical_process = $critical_process;
                                    $obj->is_error         = true;
                                    $obj->system_log       = 'Proses Terakhir Harus di isi YA atau TIDAK';
                                }else{
                                    if (!$nama_proses&&!$component&&!$smv&&!$proses_terakhir&&!$nama_mesin&&!$code_last_process&&!$code_critical_process) {
                                        $obj                   = new stdClass();
                                        $obj->style            = $style;
                                        $obj->nama_proses      = $nama_proses;
                                        $obj->component        = $component;
                                        $obj->nama_mesin       = $nama_mesin;
                                        $obj->smv              = $smv;
                                        $obj->style            = $style;
                                        $obj->proses_terakhir  = $proses_terakhir;
                                        $obj->critical_process = $critical_process;
                                        $obj->is_error         = true;
                                        $obj->system_log       = 'Nama Proses / Nama Component / Proses Terakhir kosong';
                                        
                                        $array []             = $obj;
                                    }else{
                                        if ($workflow->delete_at) {
                                            $obj                   = new stdClass();
                                            $obj->style            = $style;
                                            $obj->nama_proses      = $nama_proses;
                                            $obj->component        = $component;
                                            $obj->nama_mesin       = $nama_mesin;
                                            $obj->smv              = $smv;
                                            $obj->style            = $style;
                                            $obj->proses_terakhir  = $proses_terakhir;
                                            $obj->critical_process = $critical_process;
                                            $obj->is_error         = true;
                                            $obj->system_log       = 'Style Sudah di hapus';

                                            $array []             = $obj;
                                        }else{
                                            $processSewing = ProcessSewing::whereNull('delete_at')
                                            ->where([
                                                ['name',$nama_proses],
                                                ['component',$component],
                                            ]);
                                            if (!$processSewing->exists()) {
                                                $process = ProcessSewing::Create([
                                                    'factory_id'     => Auth::user()->factory_id,
                                                    'name'           => $nama_proses,
                                                    'component'      => $component,
                                                    'create_user_id' => Auth::user()->id
                                                ]);
                                            }else{
                                                $process =  $processSewing->first();
                                            }
                                            $dataMachine = Machine::whereNull('delete_at')
                                            ->where('name',$nama_mesin)
                                            ->first();
                                            if (!$dataMachine) {
                                                $mesin = Machine::Create([
                                                    'factory_id'     => Auth::user()->factory_id,
                                                    'code'           => $nama_mesin,
                                                    'name'           => $nama_mesin,
                                                    'update_user_id' => Auth::user()->id
                                                ]);
                                            }else{
                                                $mesin =  $dataMachine;
                                            }
                                            $checkProcessMachineNull = WorkflowDetail::where([
                                                ['workflow_id',$workflow_id],
                                                ['process_sewing_id',$process->id],
                                                ['last_process',$code_last_process],
                                            ])
                                            ->whereNull('delete_at')
                                            ->first();
                                            if ($checkProcessMachineNull) {
                                                if ($checkProcessMachineNull->machine_id==null) {
                                                    $checkProcessMachineNull->machine_id = $mesin->id;
                                                    $checkProcessMachineNull->save();
                                                }
                                            }
                                            $check_process = WorkflowDetail::where([
                                                ['workflow_id',$workflow_id],
                                                ['process_sewing_id',$process->id],
                                                ['last_process',$code_last_process],
                                                ['machine_id',$mesin->id],
                                            ])
                                            ->whereNull('delete_at')
                                            ->exists();
                                            if ($check_process) {
                                                $obj                   = new stdClass();
                                                $obj->style            = $style;
                                                $obj->nama_proses      = $nama_proses;
                                                $obj->component        = $component;
                                                $obj->nama_mesin       = $nama_mesin;
                                                $obj->smv              = $smv;
                                                $obj->style            = $style;
                                                $obj->proses_terakhir  = $proses_terakhir;
                                                $obj->critical_process = $critical_process;
                                                $obj->is_error         = true;
                                                $obj->system_log       = 'Proses Sudah ada';

                                                $array []             = $obj;
                                                
                                            }else{
                                                $is_exists = WorkflowDetail::where([
                                                    ['last_process','t'],
                                                    ['workflow_id',$workflow_id]
                                                ])
                                                ->whereNull('delete_at')
                                                ->exists();
                                                if ($is_exists && $code_last_process==true) {
                                                    $obj                   = new stdClass();
                                                    $obj->style            = $style;
                                                    $obj->nama_proses      = $nama_proses;
                                                    $obj->component        = $component;
                                                    $obj->nama_mesin       = $nama_mesin;
                                                    $obj->smv              = $smv;
                                                    $obj->style            = $style;
                                                    $obj->proses_terakhir  = $proses_terakhir;
                                                    $obj->critical_process = $critical_process;
                                                    $obj->is_error         = true;
                                                    $obj->system_log       = 'Proses Terakhir Lebih dari satu';
                                                    $array []                     = $obj;
                                                }else{
                                                        WorkflowDetail::create([
                                                            'workflow_id'       => $workflow_id,
                                                            'process_sewing_id' => $process->id,
                                                            'machine_id'        => $mesin->id,
                                                            'smv'               => $smv,
                                                            'last_process'      => $code_last_process,
                                                            'critical_process'  => $code_critical_process,
                                                            'create_user_id'    => Auth::user()->id
                                                        ]);
                                                        
                                                        $obj                   = new stdClass();
                                                        $obj->style            = $style;
                                                        $obj->nama_proses      = $nama_proses;
                                                        $obj->component        = $component;
                                                        $obj->nama_mesin       = $nama_mesin;
                                                        $obj->smv              = $smv;
                                                        $obj->style            = $style;
                                                        $obj->proses_terakhir  = $proses_terakhir;
                                                        $obj->critical_process = $critical_process;
                                                        $obj->is_error         = false;
                                                        $obj->system_log       = 'Berhasil';

                                                        $array []             = $obj;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
                        }

                    }  


                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }
    private function cekProsesTerakhir($endProses)
	{

		$withlist = array('YA','TIDAK');
        if (in_array($endProses, $withlist)) {
            return true;
        }else{
            return false;
        }
	}
    public function getWorkflow(Request $request)
    {
        if ($request->ajax()) {
            $dataActive           = $this->getWorkflowDetail(1);
            $dataOutstanding      = $this->getWorkflowDetail(0);
            $dataComplainWorkflow = $this->countComplainWorkflow();
            
            $obj                       = new StdClass();
            $obj->active               = $dataActive;
            $obj->outstanding          = $dataOutstanding;
            $obj->dataComplainWorkflow = $dataComplainWorkflow;
            
            return response()->json($obj, 200);

        }
    }
    public function getComplainWorkflowData(Request $request)
    {
        $q          = trim(strtolower($request->q));
        $factory_id = $request->factory_id;
        $buyer      = $request->session()->get('buyer');

        $lists      = ResponseWorkflow::whereNull('delete_at')
        ->where(function($query) use ($q){
            $query->where(db::raw('lower(style)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(status)'),'LIKE',"%$q%");
        })
        ->orderBy('created_at','desc')
        ->paginate(10);

        return view('workflow._lov_complain_workflow', compact('lists'));
    }
    public function sendMessage(Request $request)
    {
        $id = $request->id;
        return view('workflow._lov_send_message',compact('id'));
    }
    public function storeMessage(Request $request,$id)
    {
        if ($request->ajax()) {
            $app_env    = Config::get('app.env');
            if($app_env == 'live') $connection_mid = 'middleware_live';
            else $connection_mid = 'middleware_dev';
            try
            {
                DB::beginTransaction();
                $user = User::find(Auth::user()->id);
                $contact = Contact::where('name','akurasi')->first();
                $dataResponse= ResponseWorkflow::find($id);
                $dataResponse->status         = $request->status;
                $dataResponse->update_user_id = Auth::user()->id;
                $dataResponse->updated_at     = Carbon::now();
                $dataResponse->save();
                $dataResponseDetail = ResponseWorkflowDetail::whereNull('delete_at')
                ->where('response_workflow_id',$id)
                ->orderBy('created_at','desc')
                ->first();
                if ($dataResponseDetail) {
                    $dataResponseDetail->delete_at = Carbon::now();
                    $dataResponseDetail->save();
                }
                $line  = Line::find($dataResponse->line_id);
                $group = ($line->buyer=='other')?'Digitalisasi P. Akurasi':'Nagai Digitalisasi';

                ResponseWorkflowDetail::create([
                    'response_workflow_id' => $id,
                    'message'              => $request->message,
                    'status'               => $request->status,
                    'update_user_id'       => Auth::user()->id
                ]);
                
                DB::connection($connection_mid)
                ->table('whatsapp')
                ->insert([
                    'contact_number' => $group,
                    'is_group' => 't',
                    'message' => '*Response Dari*|No. Lapor : '.$dataResponse->nomor.'|Nama IE Development  :'.ucwords($user->name).'|Pesan : '.ucwords($request->message).'|Status : '.ucwords($request->status).'|Terimakasih.'
                ]);
                DB::commit();
                return response()->json('success',200);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
    private function getWorkflowDetail($active)
    {
        $data = DB::table('workflow_outstanding_v')
        ->where(function($query) use($active)
        {
            if ($active==1) {
                $query = $query->where('total_detail','>','1');
            }else{
                $query = $query->where('total_detail','<=','1');
            }
        })
        ->where('factory_id',Auth::user()->factory_id)
        ->count();
        return $data;
    }
    public function styleOutstanding(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('workflow_outstanding_v')
            ->where('total_detail','<=','1')
            ->where('factory_id',Auth::user()->factory_id)
            ->get();
            return view('workflow._lov_style_outstanding', compact('data'));
        }
    }
    private function countComplainWorkflow()
    {
        $data = ResponseWorkflow::whereNull('delete_at')
        ->where('status','open')->get()->count();
        return $data;
    }
    public function getStyle(Request $request)
    {
        if ($request->ajax()) {
            $style = trim(strtoupper($request->style));
            $data = Style::where('erp', 'LIKE', "$style%")->pluck('erp','default');
            return response()->json(['style' => $data]);
        }
    }
    public function getProcess(Request $request)
    {
        if ($request->ajax()) {
            // dd($request->all());
            $q      = trim(strtolower($request->q));
            $search = str_replace(" ","%",$q);
            $process = ProcessSewing::select(db::raw("CONCAT(name,'|',component,'|',smv) as name"),'id')
                    ->whereNull('delete_at')
                    ->where('name', 'LIKE', "$search%")
                    ->where('factory_id',Auth::user()->factory_id)->get();
            return response()->json(['process' => $process]);
        }
    }
    public function storeStyle(Request $request)
    {
        if ($request->ajax()) {
            $style = trim(strtoupper($request->style));
            $styleMerge = $request->styleMerge;
            try {
                DB::beginTransaction();
                $cekStyle = Style::where('erp',$style)->exists();
                if (!$cekStyle) {
                    $styleInsert = Style::create([
                        'erp'     => $style,
                        'edit'    => $style,
                        'default' => $style
                    ]);
                    HistoryChangeStyle::create([
                        'style_id'     => $styleInsert->id,
                        'change'       => "Style ".$style." di tambah",
                        'edit_user_id' => Auth::user()->id
                    ]);
                }
                foreach ($styleMerge as $key => $s) {
                    $styleUpdate = Style::where('erp',$s)->first();
                    if ($styleUpdate) {
                        $styleUpdate->default    = $style;
                        $styleUpdate->edit       = $style;
                        $styleUpdate->updated_at = Carbon::now();
                        $styleUpdate->save();
                        HistoryChangeStyle::create([
                            'style_id'     => $styleUpdate->id,
                            'change'       => "Style ".$s." di ubah menjadi ".$style,
                            'edit_user_id' => Auth::user()->id
                        ]);
                    }
                    
                }
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
        }
    }
    static function deleteDuplicate()
    {
        $data       = DB::select(db::Raw("select name,component,count(*) from process_sewings
                                        where delete_at is null
                                        GROUP BY 
                                        name,component
                                        HAVING count(*)>1;"));
        foreach ($data as $key => $data) {
            $processSewing = ProcessSewing::whereNull('delete_at')
            ->where([
                ['name',$data->name],
                ['component',$data->component],
            ])->first();
            if ($processSewing) {
                $processSewing->delete_at = Carbon::now();
                $processSewing->save();
            }
        }
    }
    static function updateSmvWorkflowDetail()
    {
        $data       = DB::select(db::Raw("
            select 
                wd.process_sewing_id,
                wd.id as workflow_detail_id,
                ps.name,
                ps.component,
                ps.smv
            from workflow_details wd
            LEFT JOIN process_sewings ps on ps.id=wd.process_sewing_id
            where wd.delete_at is null
        "));
        foreach ($data as $key => $data) {
            DB::table('workflow_details')
            ->where('id',$data->workflow_detail_id)
            ->update([
                'smv' => $data->smv
            ]);
        }
    }
    static function updateWorkflowDetail()
    {
        $data       = DB::select(db::Raw("
            select 
                distinct
                production_id,
                qi.process_sewing_id,
                qi.machine_id,
                s.default AS style,
                wd.id as workflow_detail_id
            from qc_inlines qi
            LEFT JOIN productions pr ON pr.id = qi.production_id
            LEFT JOIN lines li ON li.id = pr.line_id
            LEFT JOIN styles s ON s.erp = pr.style
            LEFT JOIN workflows wo on wo.style=s.default
            left join workflow_details wd on wd.workflow_id=wo.id and wd.process_sewing_id=qi.process_sewing_id
            where wd.delete_at is null and wo.delete_at is null
            and pr.line_id='da91c9f0-788e-11ea-98f6-df1e8bdeb186' 
        "));
        foreach ($data as $key => $data) {
            DB::table('workflow_details')
            ->where('id',$data->workflow_detail_id)
            ->update([
                'machine_id' => $data->machine_id
            ]);
            DB::table('qc_inlines')
            ->where('production_id',$data->production_id)
            ->where('process_sewing_id',$data->process_sewing_id)
            ->where('machine_id',$data->machine_id)
            ->update([
                'workflow_detail_id' => $data->workflow_detail_id
            ]);
        }
    }
    static function updateWorkflowDetailHistory()
    {
        $data       = HistoryProcessSewing::whereNull('delete_at')
        ->where('line_id','da91c9f0-788e-11ea-98f6-df1e8bdeb186')
        ->whereNull('workflow_detail_id')->get();
        foreach ($data as $key => $d) {
            $check = Workflow::select(db::raw('workflow_details.id as workflow_detail_id'),'workflows.style','workflow_details.process_sewing_id','workflow_details.machine_id')
            ->leftjoin('workflow_details','workflow_details.workflow_id','workflows.id')
            ->whereNull('workflows.delete_at')
            ->whereNull('workflow_details.delete_at')
            ->where([
                ['style',$d->style],
                ['process_sewing_id',$d->process_sewing_id],
            ])
            ->first();
            if ($check) {
                DB::table('history_process_sewings')
                ->where('process_sewing_id',$check->process_sewing_id)
                ->where('style',$check->style)
                ->update([
                    'workflow_detail_id' => $check->workflow_detail_id
                ]);
                DB::table('workflow_details')
                ->where('id',$check->workflow_detail_id)
                ->update([
                    'machine_id' => $d->machine_id
                ]);
            }
            
        }
    }
    static function updateQcInline()
    {
        $style = array(
            'HV210');
        $data       = WorkflowDetail::select('workflows.id as workflow_id',
        'workflow_details.id as workflow_detail_id',
        'workflow_details.process_sewing_id',
        'workflows.style')
        ->leftJoin('workflows','workflows.id','workflow_details.workflow_id')
        ->whereNull('workflow_details.delete_at')
        ->whereIn('style',$style)
        ->get();
        foreach ($data as $key => $d) {
            $dataQcInline = DB::select(db::Raw("select * from get_qc_inline_v where process_sewing_id='$d->process_sewing_id' and style_default='$d->style' and workflow_detail_id is null"));
            if(count($dataQcInline)>0){
                foreach ($dataQcInline as $key => $qc) {
                    DB::table('qc_inlines')
                    ->where('id',$qc->qc_inline_id)
                    ->update([
                        'workflow_detail_id' => $d->workflow_detail_id
                    ]);
                    DB::table('history_process_sewings')
                    ->where([
                        ['process_sewing_id',$qc->process_sewing_id],
                        ['style',$qc->style],
                        ['sewer_nik',$qc->sewer_nik],
                    ])
                    ->update([
                        'workflow_detail_id' => $d->workflow_detail_id
                    ]);
                    DB::table('workflow_details')
                    ->whereNull('machine_id')
                    ->where([
                        ['id',$d->workflow_detail_id],
                    ])
                    ->update([
                        'machine_id' => $qc->machine_id
                    ]);
    
                }
            }
        }

    }
    public function storeMergeProcess(Request $request,$id)
    {
        if ($request->ajax()) {
            $process        = array_unique($request->processMerge);
            $processNames   = array();
            $componentNames = array();
            $machineNames   = array();
            $smv            = 0;
            try
            {
                DB::beginTransaction();
                foreach ($process as $p) {
                    $workflowDetail = WorkflowDetail::findOrFail($p);
                    $workflowDetail->delete_at = Carbon::now();
                    $workflowDetail->delete_user_id = Auth::user()->id;
                    $workflowDetail->save();
                    
                    $historyProcess = HistoryProcessSewing::findOrFail($p);
                    $historyProcess->delete_at = Carbon::now();
                    $historyProcess->save();
                    array_push($processNames,$workflowDetail->processSewing->name);
                    array_push($componentNames,$workflowDetail->processSewing->component);
                    array_push($machineNames,$workflowDetail->machine->name);
                    $smv+=$workflowDetail->smv;
                }
                sort($processNames);
                sort($componentNames);
                sort($machineNames);

                $processName   = '';
                $machineName   = '';
                $componentName = '';
                foreach ($processNames as $key => $p) {
                    $processName.=$p.",";
                }
                foreach ($componentNames as $key => $p) {
                    $componentName.=$p.",";
                }
                foreach ($machineNames as $key => $p) {
                    $machineName.=$p.",";
                }
                
                $processName   = rtrim($processName,",");
                $machineName   = rtrim($machineName,",");
                $componentName = rtrim($componentName,",");
                
                $dataMachine = Machine::whereNull('delete_at')
                ->where([
                    ['factory_id',Auth::user()->factory_id],
                    ['name',$machineName],
                ])->first();
                if ($dataMachine) {
                    $machine_id = $dataMachine->id;
                } else {
                    $machine = Machine::Create([
                        'code'           => $machineName,
                        'name'           => $machineName,
                        'factory_id'     => Auth::user()->factory_id,
                        'create_user_id' => auth::user()->id
                    ]);
                    $machine_id = $machine->id;
                }
                
                $dataProcess = ProcessSewing::whereNull('delete_at')
                ->where([
                    ['name',$processName],
                    ['component',$componentName],
                ])
                ->first();
                if ($dataProcess) {
                    $process_id = $dataProcess->id;
                } else {
                    $process = ProcessSewing::Create([
                        'name'           => $processName,
                        'component'      => $componentName,
                        'factory_id'     => Auth::user()->factory_id,
                        'create_user_id' => auth::user()->id
                    ]);
                    $process_id = $process->id;
                }
                if ($cekWorkflow) {
                    return response()->json(['message' => 'Simpan Data Gagal, Proses Yang anda masukan sudah ada'], 422);
                }
                WorkflowDetail::create([
                    'workflow_id'       => $id,
                    'process_sewing_id' => $process_id,
                    'smv'               => $smv,
                    'machine_id'        => $machine_id,
                    'update_user_id'    => Auth::user()->id,
                ]);
                DB::commit();
                return response()->json('success',200);
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        }
    }
}