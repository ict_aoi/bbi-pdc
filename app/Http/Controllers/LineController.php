<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;

use App\Models\Line;
use App\Models\Factory;
use App\Models\Production;
use App\Models\HistoryLine;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class LineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('line.index');
    }
    public function data()
    {
    	if(request()->ajax())
        {

            $data = Line::active()
            ->orderby('order','asc');
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
                return $data->code;
            })
            ->editColumn('factory',function($data){
                return $data->factory->code;
            })
            ->editColumn('order',function($data){
                return $data->order;
            })
            ->editColumn('buyer',function($data){
                return strtoupper($data->buyer);
            })
            ->editColumn('name',function($data){
                return strtoupper($data->name);
            })
            ->editColumn('wft',function($data){
                return strtoupper($data->wft);
            })
            ->editColumn('alias',function($data){
                return strtoupper($data->alias);
            })
            
            ->addColumn('action', function($data) {
                return view('line._action', [
                    'model'  => $data,
                    'edit'   => route('master_line.edit',$data->id),
                    'delete' => route('master_line.destroy',$data->id)
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }
    public function create(Request $request)
    {
        $factory = Factory::active()->pluck('name','id')->all();
        return view('line.create',compact('factory'));
    }
    /* metode store lama */
    /* public function store(Request $request)
    {
        // dd($request->all());
    	try {
    		DB::beginTransaction();
            $lineDetails     = json_decode($request->items);
            foreach ($lineDetails as $key => $line) {
                $is_exists = Line::whereNull('delete_at')
                ->where('factory_id',Auth::user()->factory_id)
                ->where('code',$line->code);
                if ($line->id == -1) {
                    if (!$is_exists->exists()) {
                        Line::FirstOrCreate([
                            'factory_id'     => Auth::user()->factory_id,
                            'code'           => $line->code,
                            'buyer'          => $line->buyer,
                            'name'           => strtolower($line->name),
                            'order'          => $line->order,
                            'create_user_id' => Auth::user()->id
                        ]);
                    }
                }else{
                    $lineCek = Line::find($line->id);
                    if ($lineCek->code==$line->code) {
                        $lineCek->buyer = $line->buyer;
                        $lineCek->name = strtolower($line->name);
                        $lineCek->order = $line->order;
                        $lineCek->save();
                    }else{
                        $is_exists = Line::whereNull('delete_at')
                        ->where('factory_id',Auth::user()->factory_id)
                        ->where('code',$line->code);
                        if (!$is_exists->exists()) {
                            DB::table('lines')
                            ->where('id',$line->id)
                            ->update([
                                 'code'           => $line->code,
                            ]);
                         }
                    }
                    
                }
            }
            DB::commit();
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}

    } */
    public function store(Request $request)
    {
        $rules = [
            'factory' => 'required|string',
            'jenis'   => 'required|string',
            'name'    => 'required|string',
            'code'    => 'required|string',
            'order'   => 'required|integer',
            'alias'   => 'required|string',
            'wft'     => 'required|integer',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $codeExists = Line::active()
                ->where('factory_id',$request->factory)
                ->where('code',$request->code)->exists();
        if ($codeExists) {
            return response()->json('Code sudah ada.', 422);
        }
        $aliasExists = Line::active()
                ->where('factory_id',$request->factory)
                ->where('alias',trim(strtolower($request->alias)))->exists();
        if ($aliasExists) {
            return response()->json('Alias sudah ada.', 422);
        }
        $nameExists = Line::active()
                ->where('factory_id',$request->factory)
                ->where('name',trim(strtolower($request->name)))->exists();
        if ($nameExists) {
            return response()->json('Nama Line sudah ada.', 422);
        }
    	try {
    		DB::beginTransaction();
            Line::FirstOrCreate([
                'factory_id'     => $request->factory,
                'buyer'          => strtolower($request->jenis),
                'name'           => trim(strtolower($request->name)),
                'code'           => trim(strtolower($request->code)),
                'order'          => $request->order,
                'alias'          => trim(strtolower($request->alias)),
                'wft'            => $request->wft,
                'create_user_id' => Auth::user()->id,
            ]);
            DB::commit();
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }
    public function edit(Request $request,$id)
    {
        $line    = Line::find($id);
        $factory = Factory::active()->pluck('name','id')->all();
        return view('line.edit',compact('line','factory'));
    }
    public function update(Request $request,$id)
    {
        $rules = [
            'factory' => 'required|string',
            'jenis'   => 'required|string',
            'name'    => 'required|string',
            'code'    => 'required|string',
            'order'   => 'required|integer',
            'alias'   => 'required|string',
            'wft'     => 'required|integer',
        ];
        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ],400);
        }
        $buyer          = strtolower($request->jenis);
        $name           = strtolower($request->name);
        $code           = strtolower($request->code);
        $order          = strtolower($request->order);
        $alias          = strtolower($request->alias);
        $wft            = $request->wft;
        $factory        = $request->factory;
        $dataLineBefore = Line::find($id);
        if (!$dataLineBefore) {
            return response()->json(['message'=>'Ada Kesalahan silahkan refresh Halaman anda Sudah ada'],422);
        }
        if ($dataLineBefore->name<>$name) {
            if (Line::active()
            ->where([
                ['name',$name],
                ['factory_id',$factory],
            ])
            ->exists())
            {
                return response()->json(['message'=>'Nama Line Sudah ada'],422);
            }
        }
        if ($dataLineBefore->code<>$code) {
            if (Line::active()
            ->where([
                ['code',$code],
                ['factory_id',$factory],
            ])
            ->exists())
            {
                return response()->json(['message'=>'Code Line Sudah Ada'],422);
            }else{
                $checkHistory = HistoryLine::whereNull('delete_at')
                ->where('line_id',$id)->first();
                if ($checkHistory) {
                    HistoryLine::where('id',$checkHostory->id)
                    ->update(array(
                        'delete_at' => Carbon::now(),
                        'delete_user_id' => Auth::user()->id
                    ));
                }
                $historyLine = HistoryLine::FirstOrCreate([
                    'line_id'        => $id,
                    'name'           => $dataLineBefore->name,
                    'create_user_id' => Auth::user()->id
                ]);
                
                Production::where('line_id',$id)
                ->whereNull('history_line_id')
                ->update(array('history_line_id'=>$historyLine->id));
            }
            
        }
        if ($dataLineBefore->alias<>$alias) {
            if (Line::active()
            ->where([
                ['alias',$alias],
                ['factory_id',$factory],
            ])
            ->exists())
            {
                return response()->json(['message'=>'Alias Line Sudah Ada'],422);
            }
            
        }
        try 
        {
            DB::beginTransaction();
            
            $dataLineBefore->factory_id     = $factory;
            $dataLineBefore->name           = $name;
            $dataLineBefore->code           = $code;
            $dataLineBefore->order          = $order;
            $dataLineBefore->buyer          = $buyer;
            $dataLineBefore->alias          = $alias;
            $dataLineBefore->wft            = $wft;
            $dataLineBefore->update_user_id = Auth::user()->id;
            $dataLineBefore->save();
            
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            $update = Line::find($id);
            if ($update) {
                $update->delete_at      = Carbon::now();
                $update->delete_user_id = Auth::user()->id;
                $update->save();
            }
        }
    }
}